package scatter.scattergenerator.rest.config.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 数据源配置表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Component
@FeignClient(value = "ConfigDatasource-client")
public interface ConfigDatasourceClient {

}
