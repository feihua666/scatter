package scatter.scattergenerator.rest.history.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 生成表历史表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Component
@FeignClient(value = "TableGenHistory-client")
public interface TableGenHistoryClient {

}
