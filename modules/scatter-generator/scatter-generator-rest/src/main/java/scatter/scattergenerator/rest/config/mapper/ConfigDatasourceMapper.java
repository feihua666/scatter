package scatter.scattergenerator.rest.config.mapper;

import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 数据源配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
public interface ConfigDatasourceMapper extends IBaseMapper<ConfigDatasource> {

}
