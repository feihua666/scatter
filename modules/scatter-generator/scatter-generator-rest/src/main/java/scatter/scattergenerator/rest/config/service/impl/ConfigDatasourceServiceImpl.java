package scatter.scattergenerator.rest.config.service.impl;

import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.scattergenerator.rest.config.mapper.ConfigDatasourceMapper;
import scatter.scattergenerator.rest.config.service.IConfigDatasourceService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourcePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 数据源配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Service
@Transactional
public class ConfigDatasourceServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ConfigDatasourceMapper, ConfigDatasource, ConfigDatasourceAddForm, ConfigDatasourceUpdateForm, ConfigDatasourcePageQueryForm> implements IConfigDatasourceService {
    @Override
    public void preAdd(ConfigDatasourceAddForm addForm,ConfigDatasource po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ConfigDatasource::getName,false);
        }

    }

    @Override
    public void preUpdate(ConfigDatasourceUpdateForm updateForm,ConfigDatasource po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ConfigDatasource byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ConfigDatasource::getName,false);
            }
        }

    }
}
