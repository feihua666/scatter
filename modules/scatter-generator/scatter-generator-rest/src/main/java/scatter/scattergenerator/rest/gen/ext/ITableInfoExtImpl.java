package scatter.scattergenerator.rest.gen.ext;

import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.tools.ThreadContextTool;
import scatter.generator.core.CustomTableField;
import scatter.generator.core.CustomTableInfo;
import scatter.generator.ext.ITableInfoExt;
import scatter.scattergenerator.pojo.gen.form.GenForm;
import scatter.scattergenerator.pojo.gen.form.GenTableColumnForm;

import static scatter.scattergenerator.rest.gen.ScatterTableGenerator.GEN_FORM_IN_THREAD_CONTEXT_KEY;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-02-26 17:13
 */
@Component
public class ITableInfoExtImpl implements ITableInfoExt, InterfaceTool {
    @Override
    public void tableInfoExt(CustomTableInfo customTableInfo) {
        GenForm genForm = (GenForm)ThreadContextTool.get(GEN_FORM_IN_THREAD_CONTEXT_KEY);
        if (genForm != null) {
            if (!isStrEmpty(genForm.getTableInfo().getTableComment())) {
                customTableInfo.setCommentSimple(genForm.getTableInfo().getTableComment());
            }
            if (!isStrEmpty(genForm.getTableInfo().getTableCommentRel1())) {
                customTableInfo.setCommentRel1(genForm.getTableInfo().getTableCommentRel1());
            }
            if (!isStrEmpty(genForm.getTableInfo().getTableCommentRel2())) {
                customTableInfo.setCommentRel2(genForm.getTableInfo().getTableCommentRel2());
            }
        }
    }

    @Override
    public void tableColumnExt(CustomTableField customTableField) {
        GenForm genForm = (GenForm)ThreadContextTool.get(GEN_FORM_IN_THREAD_CONTEXT_KEY);
        if (genForm != null && isEmpty(genForm.getTableInfo().getTableColumns())) {
            GenTableColumnForm genTableColumnForm = genForm.getTableInfo().getTableColumns().stream().filter(c -> isEqual(c.getColumnName(), customTableField.getColumnName())).findFirst().get();
            if (genTableColumnForm.getIsForeignKey() != null) {
                customTableField.setForeignKey(genTableColumnForm.getIsForeignKey());
            }

            if (genTableColumnForm.getIsRequired() != null) {
                customTableField.setRequired(genTableColumnForm.getIsRequired());
            }

            if (genTableColumnForm.getIsUnique() != null) {
                customTableField.setUnique(genTableColumnForm.getIsUnique());
            }

            if (genTableColumnForm.getColumnComment() != null) {
                customTableField.setCommentSimple(genTableColumnForm.getColumnComment());
            }


            if (genTableColumnForm.getIsQueryLike() != null) {
                customTableField.setQueryLike(genTableColumnForm.getIsQueryLike());
            }


            if (genTableColumnForm.getIsOrderby() != null) {
                customTableField.setOrderBy(genTableColumnForm.getIsOrderby());
            }

            if (genTableColumnForm.getIsOrderbyAsc() != null) {
                customTableField.setOrderByAsc(genTableColumnForm.getIsOrderbyAsc());
            }


            if (genTableColumnForm.getLengthLimit() != null) {
                customTableField.setLength(genTableColumnForm.getLengthLimit());
            }


            if (genTableColumnForm.getFractionlengthLimit() != null) {
                customTableField.setFractionLength(genTableColumnForm.getFractionlengthLimit());
            }
        }
    }
}
