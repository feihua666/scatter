package scatter.scattergenerator.rest.history.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
/**
 * <p>
 * 生成表历史表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
public interface ITableGenHistoryService extends IBaseService<TableGenHistory> {


    /**
     * 根据名称查询
     * @param tableName
     * @return
     */
    default TableGenHistory getByTableName(String tableName) {
        Assert.hasText(tableName,"tableName不能为空");
        return getOne(Wrappers.<TableGenHistory>lambdaQuery().eq(TableGenHistory::getTableName, tableName));
    }

}
