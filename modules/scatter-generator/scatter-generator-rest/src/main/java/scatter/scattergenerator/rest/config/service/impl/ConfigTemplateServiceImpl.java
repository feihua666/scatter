package scatter.scattergenerator.rest.config.service.impl;

import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.rest.config.mapper.ConfigTemplateMapper;
import scatter.scattergenerator.rest.config.service.IConfigTemplateService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplatePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 模板配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Service
@Transactional
public class ConfigTemplateServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ConfigTemplateMapper, ConfigTemplate, ConfigTemplateAddForm, ConfigTemplateUpdateForm, ConfigTemplatePageQueryForm> implements IConfigTemplateService {
    @Override
    public void preAdd(ConfigTemplateAddForm addForm,ConfigTemplate po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ConfigTemplate::getName,false);
        }
        if (!isStrEmpty(addForm.getTemplateName())) {
            // 模板名称已存在不能添加
            assertByColumn(addForm.getTemplateName(),ConfigTemplate::getTemplateName,false);
        }

    }

    @Override
    public void preUpdate(ConfigTemplateUpdateForm updateForm,ConfigTemplate po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ConfigTemplate byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ConfigTemplate::getName,false);
            }
        }
        if (!isStrEmpty(updateForm.getTemplateName())) {
            ConfigTemplate byId = getById(updateForm.getId());
            // 如果模板名称有改动
            if (!isEqual(updateForm.getTemplateName(), byId.getTemplateName())) {
                // 模板名称已存在不能修改
                assertByColumn(updateForm.getTemplateName(),ConfigTemplate::getTemplateName,false);
            }
        }

    }
}
