package scatter.scattergenerator.rest.history.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseQueryFormServiceImpl;
import scatter.scattergenerator.pojo.history.form.TableGenHistoryPageQueryForm;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.rest.history.mapper.TableGenHistoryMapper;
import scatter.scattergenerator.rest.history.service.ITableGenHistoryService;
/**
 * <p>
 * 生成表历史表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Service
@Transactional
public class TableGenHistoryServiceImpl extends IBaseQueryFormServiceImpl<TableGenHistoryMapper, TableGenHistory, TableGenHistoryPageQueryForm> implements ITableGenHistoryService {

}
