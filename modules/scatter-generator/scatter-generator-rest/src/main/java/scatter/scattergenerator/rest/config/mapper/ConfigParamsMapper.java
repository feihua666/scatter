package scatter.scattergenerator.rest.config.mapper;

import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
public interface ConfigParamsMapper extends IBaseMapper<ConfigParams> {

}
