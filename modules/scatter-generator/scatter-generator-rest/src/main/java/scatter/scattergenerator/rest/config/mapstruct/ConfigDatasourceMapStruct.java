package scatter.scattergenerator.rest.config.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.scattergenerator.pojo.config.vo.ConfigDatasourceVo;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourcePageQueryForm;

/**
 * <p>
 * 数据源配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConfigDatasourceMapStruct extends IBaseVoMapStruct<ConfigDatasource, ConfigDatasourceVo>,
                                  IBaseAddFormMapStruct<ConfigDatasource,ConfigDatasourceAddForm>,
                                  IBaseUpdateFormMapStruct<ConfigDatasource,ConfigDatasourceUpdateForm>,
                                  IBaseQueryFormMapStruct<ConfigDatasource,ConfigDatasourcePageQueryForm>{
    ConfigDatasourceMapStruct INSTANCE = Mappers.getMapper( ConfigDatasourceMapStruct.class );

}
