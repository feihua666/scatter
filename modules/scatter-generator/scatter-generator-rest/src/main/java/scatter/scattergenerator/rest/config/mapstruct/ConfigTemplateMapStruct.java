package scatter.scattergenerator.rest.config.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.pojo.config.vo.ConfigTemplateVo;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplatePageQueryForm;

/**
 * <p>
 * 模板配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConfigTemplateMapStruct extends IBaseVoMapStruct<ConfigTemplate, ConfigTemplateVo>,
                                  IBaseAddFormMapStruct<ConfigTemplate,ConfigTemplateAddForm>,
                                  IBaseUpdateFormMapStruct<ConfigTemplate,ConfigTemplateUpdateForm>,
                                  IBaseQueryFormMapStruct<ConfigTemplate,ConfigTemplatePageQueryForm>{
    ConfigTemplateMapStruct INSTANCE = Mappers.getMapper( ConfigTemplateMapStruct.class );

}
