package scatter.scattergenerator.rest.config.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;
import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.scattergenerator.rest.config.service.IConfigParamsService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 参数配置表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/inner/config/config-params")
public class ConfigParamsInnerController extends BaseInnerController<ConfigParams> {
 @Autowired
 private IConfigParamsService configParamsService;

 public IConfigParamsService getService(){
     return configParamsService;
 }
}
