package scatter.scattergenerator.rest.config.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.common.rest.service.IBaseService;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplatePageQueryForm;
import java.util.List;
/**
 * <p>
 * 模板配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
public interface IConfigTemplateService extends IBaseService<ConfigTemplate> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ConfigTemplate getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ConfigTemplate>lambdaQuery().eq(ConfigTemplate::getName, name));
    }

    /**
     * 根据模板名称查询
     * @param templateName
     * @return
     */
    default ConfigTemplate getByTemplateName(String templateName) {
        Assert.hasText(templateName,"templateName不能为空");
        return getOne(Wrappers.<ConfigTemplate>lambdaQuery().eq(ConfigTemplate::getTemplateName, templateName));
    }

}
