package scatter.scattergenerator.rest.history.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.rest.history.service.ITableGenHistoryService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 生成表历史表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/inner/history/table-gen-history")
public class TableGenHistoryInnerController extends BaseInnerController<TableGenHistory> {
 @Autowired
 private ITableGenHistoryService tableGenHistoryService;

 public ITableGenHistoryService getService(){
     return tableGenHistoryService;
 }
}
