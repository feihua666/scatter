package scatter.scattergenerator.rest.history.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scattergenerator.pojo.history.form.TableGenHistoryPageQueryForm;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.pojo.history.vo.TableGenHistoryVo;

/**
 * <p>
 * 生成历史 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TableGenHistoryMapStruct extends IBaseVoMapStruct<TableGenHistory, TableGenHistoryVo>,
                                  IBaseQueryFormMapStruct<TableGenHistory,TableGenHistoryPageQueryForm>{
    TableGenHistoryMapStruct INSTANCE = Mappers.getMapper( TableGenHistoryMapStruct.class );

}
