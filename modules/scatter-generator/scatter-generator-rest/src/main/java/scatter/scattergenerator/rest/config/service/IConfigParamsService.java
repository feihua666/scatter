package scatter.scattergenerator.rest.config.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.common.rest.service.IBaseService;
import scatter.scattergenerator.pojo.config.form.ConfigParamsAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsPageQueryForm;
import java.util.List;
/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
public interface IConfigParamsService extends IBaseService<ConfigParams> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ConfigParams getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ConfigParams>lambdaQuery().eq(ConfigParams::getName, name));
    }

    /**
     * 根据标签名查询
     * @param label
     * @return
     */
    default ConfigParams getByLabel(String label) {
        Assert.hasText(label,"label不能为空");
        return getOne(Wrappers.<ConfigParams>lambdaQuery().eq(ConfigParams::getLabel, label));
    }

}
