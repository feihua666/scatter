package scatter.scattergenerator.rest.config.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.scattergenerator.pojo.config.vo.ConfigParamsVo;
import scatter.scattergenerator.pojo.config.form.ConfigParamsAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Api(tags = "参数配置相关接口")
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/config/config-params")
public class ConfigParamsController extends BaseAddUpdateQueryFormController<ConfigParams, ConfigParamsVo, ConfigParamsAddForm, ConfigParamsUpdateForm, ConfigParamsPageQueryForm> {


     @Override
	 @ApiOperation("添加参数配置")
     @PreAuthorize("hasAuthority('ConfigParams:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigParamsVo add(@RequestBody @Valid ConfigParamsAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询参数配置")
     @PreAuthorize("hasAuthority('ConfigParams:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ConfigParamsVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除参数配置")
     @PreAuthorize("hasAuthority('ConfigParams:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新参数配置")
     @PreAuthorize("hasAuthority('ConfigParams:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigParamsVo update(@RequestBody @Valid ConfigParamsUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询参数配置")
    @PreAuthorize("hasAuthority('ConfigParams:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ConfigParamsVo> getList(ConfigParamsPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询参数配置")
    @PreAuthorize("hasAuthority('ConfigParams:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ConfigParamsVo> getPage(ConfigParamsPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
