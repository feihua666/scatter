package scatter.scattergenerator.rest.history.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseQueryFormController;
import scatter.scattergenerator.pojo.history.form.TableGenHistoryPageQueryForm;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.pojo.history.vo.TableGenHistoryVo;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;

import java.util.List;
/**
 * <p>
 * 生成表历史表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Api(tags = "生成历史相关接口")
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/history/table-gen-history")
public class TableGenHistoryController extends BaseQueryFormController<TableGenHistory, TableGenHistoryVo, TableGenHistoryPageQueryForm> {


     @Override
	 @ApiOperation("根据ID查询生成历史")
     @PreAuthorize("hasAuthority('TableGenHistory:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public TableGenHistoryVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除生成历史")
     @PreAuthorize("hasAuthority('TableGenHistory:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }


    @Override
	@ApiOperation("不分页查询生成历史")
    @PreAuthorize("hasAuthority('TableGenHistory:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<TableGenHistoryVo> getList(TableGenHistoryPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询生成历史")
    @PreAuthorize("hasAuthority('TableGenHistory:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<TableGenHistoryVo> getPage(TableGenHistoryPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
