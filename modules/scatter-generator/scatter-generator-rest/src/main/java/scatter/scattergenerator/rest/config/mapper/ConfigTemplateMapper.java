package scatter.scattergenerator.rest.config.mapper;

import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 模板配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
public interface ConfigTemplateMapper extends IBaseMapper<ConfigTemplate> {

}
