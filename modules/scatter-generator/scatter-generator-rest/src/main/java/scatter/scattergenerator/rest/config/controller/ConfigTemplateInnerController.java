package scatter.scattergenerator.rest.config.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.rest.config.service.IConfigTemplateService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 模板配置表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/inner/config/config-template")
public class ConfigTemplateInnerController extends BaseInnerController<ConfigTemplate> {
 @Autowired
 private IConfigTemplateService configTemplateService;

 public IConfigTemplateService getService(){
     return configTemplateService;
 }
}
