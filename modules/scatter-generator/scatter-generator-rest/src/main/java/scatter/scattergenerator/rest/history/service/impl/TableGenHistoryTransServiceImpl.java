package scatter.scattergenerator.rest.history.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.rest.history.service.ITableGenHistoryService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 生成历史翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Component
public class TableGenHistoryTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ITableGenHistoryService tableGenHistoryService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,TableGenHistory.TRANS_TABLEGENHISTORY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,TableGenHistory.TRANS_TABLEGENHISTORY_BY_ID)) {
            TableGenHistory byId = tableGenHistoryService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,TableGenHistory.TRANS_TABLEGENHISTORY_BY_ID)) {
            return tableGenHistoryService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
