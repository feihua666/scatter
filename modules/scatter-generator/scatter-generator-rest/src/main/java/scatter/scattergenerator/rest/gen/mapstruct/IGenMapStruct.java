package scatter.scattergenerator.rest.gen.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * Created by yangwei
 * Created at 2021/2/24 13:09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IGenMapStruct {
    IGenMapStruct INSTANCE = Mappers.getMapper( IGenMapStruct.class );


}
