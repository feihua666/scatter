package scatter.scattergenerator.rest.history.mapper;

import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 生成表历史表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
public interface TableGenHistoryMapper extends IBaseMapper<TableGenHistory> {

}
