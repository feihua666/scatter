package scatter.scattergenerator.rest.config.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.scattergenerator.pojo.config.vo.ConfigDatasourceVo;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourcePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 数据源配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Api(tags = "数据源配置相关接口")
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/config/config-datasource")
public class ConfigDatasourceController extends BaseAddUpdateQueryFormController<ConfigDatasource, ConfigDatasourceVo, ConfigDatasourceAddForm, ConfigDatasourceUpdateForm, ConfigDatasourcePageQueryForm> {


     @Override
	 @ApiOperation("添加数据源配置")
     @PreAuthorize("hasAuthority('ConfigDatasource:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigDatasourceVo add(@RequestBody @Valid ConfigDatasourceAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询数据源配置")
     @PreAuthorize("hasAuthority('ConfigDatasource:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ConfigDatasourceVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除数据源配置")
     @PreAuthorize("hasAuthority('ConfigDatasource:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新数据源配置")
     @PreAuthorize("hasAuthority('ConfigDatasource:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigDatasourceVo update(@RequestBody @Valid ConfigDatasourceUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询数据源配置")
    @PreAuthorize("hasAuthority('ConfigDatasource:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ConfigDatasourceVo> getList(ConfigDatasourcePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询数据源配置")
    @PreAuthorize("hasAuthority('ConfigDatasource:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ConfigDatasourceVo> getPage(ConfigDatasourcePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
