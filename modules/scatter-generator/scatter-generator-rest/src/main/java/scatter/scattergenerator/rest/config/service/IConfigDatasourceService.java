package scatter.scattergenerator.rest.config.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import org.springframework.util.Assert;
import scatter.generator.core.CustomDataSourceConfig;
import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.common.rest.service.IBaseService;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourcePageQueryForm;
import java.util.List;
/**
 * <p>
 * 数据源配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
public interface IConfigDatasourceService extends IBaseService<ConfigDatasource> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ConfigDatasource getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ConfigDatasource>lambdaQuery().eq(ConfigDatasource::getName, name));
    }

    /**
     * 根据配置id生成配置对象
     * @param dataSourceId
     * @return
     */
    default CustomDataSourceConfig buildDataSourceConfigById(String dataSourceId) {
        Assert.hasText(dataSourceId,"dataSourceId不能为空");
        ConfigDatasource byId = getById(dataSourceId);
        // 数据源配置
        CustomDataSourceConfig dsc = new CustomDataSourceConfig();
        dsc.setUrl(byId.getUrl());
        dsc.setDriverName(byId.getDriverClassName());
        dsc.setUsername(byId.getUsername());
        dsc.setPassword(byId.getPassword());
        return dsc;
    }
}
