package scatter.scattergenerator.rest.config.service.impl;

import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.scattergenerator.rest.config.mapper.ConfigParamsMapper;
import scatter.scattergenerator.rest.config.service.IConfigParamsService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scattergenerator.pojo.config.form.ConfigParamsAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Service
@Transactional
public class ConfigParamsServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ConfigParamsMapper, ConfigParams, ConfigParamsAddForm, ConfigParamsUpdateForm, ConfigParamsPageQueryForm> implements IConfigParamsService {
    @Override
    public void preAdd(ConfigParamsAddForm addForm,ConfigParams po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ConfigParams::getName,false);
        }
        if (!isStrEmpty(addForm.getLabel())) {
            // 标签名已存在不能添加
            assertByColumn(addForm.getLabel(),ConfigParams::getLabel,false);
        }

    }

    @Override
    public void preUpdate(ConfigParamsUpdateForm updateForm,ConfigParams po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ConfigParams byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ConfigParams::getName,false);
            }
        }
        if (!isStrEmpty(updateForm.getLabel())) {
            ConfigParams byId = getById(updateForm.getId());
            // 如果标签名有改动
            if (!isEqual(updateForm.getLabel(), byId.getLabel())) {
                // 标签名已存在不能修改
                assertByColumn(updateForm.getLabel(),ConfigParams::getLabel,false);
            }
        }

    }
}
