package scatter.scattergenerator.rest.config.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.scattergenerator.pojo.config.vo.ConfigParamsVo;
import scatter.scattergenerator.pojo.config.form.ConfigParamsAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsPageQueryForm;

/**
 * <p>
 * 参数配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConfigParamsMapStruct extends IBaseVoMapStruct<ConfigParams, ConfigParamsVo>,
                                  IBaseAddFormMapStruct<ConfigParams,ConfigParamsAddForm>,
                                  IBaseUpdateFormMapStruct<ConfigParams,ConfigParamsUpdateForm>,
                                  IBaseQueryFormMapStruct<ConfigParams,ConfigParamsPageQueryForm>{
    ConfigParamsMapStruct INSTANCE = Mappers.getMapper( ConfigParamsMapStruct.class );

}
