package scatter.scattergenerator.rest.config.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.rest.config.service.IConfigTemplateService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 模板配置翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Component
public class ConfigTemplateTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IConfigTemplateService configTemplateService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ConfigTemplate.TRANS_CONFIGTEMPLATE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ConfigTemplate.TRANS_CONFIGTEMPLATE_BY_ID)) {
            ConfigTemplate byId = configTemplateService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ConfigTemplate.TRANS_CONFIGTEMPLATE_BY_ID)) {
            return configTemplateService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
