package scatter.scattergenerator.rest.config.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.pojo.config.vo.ConfigTemplateVo;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplatePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 模板配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Api(tags = "模板配置相关接口")
@RestController
@RequestMapping(ScatterGeneratorConfiguration.CONTROLLER_BASE_PATH + "/config/config-template")
public class ConfigTemplateController extends BaseAddUpdateQueryFormController<ConfigTemplate, ConfigTemplateVo, ConfigTemplateAddForm, ConfigTemplateUpdateForm, ConfigTemplatePageQueryForm> {


     @Override
	 @ApiOperation("添加模板配置")
     @PreAuthorize("hasAuthority('ConfigTemplate:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigTemplateVo add(@RequestBody @Valid ConfigTemplateAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询模板配置")
     @PreAuthorize("hasAuthority('ConfigTemplate:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ConfigTemplateVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除模板配置")
     @PreAuthorize("hasAuthority('ConfigTemplate:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新模板配置")
     @PreAuthorize("hasAuthority('ConfigTemplate:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigTemplateVo update(@RequestBody @Valid ConfigTemplateUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询模板配置")
    @PreAuthorize("hasAuthority('ConfigTemplate:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ConfigTemplateVo> getList(ConfigTemplatePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询模板配置")
    @PreAuthorize("hasAuthority('ConfigTemplate:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ConfigTemplateVo> getPage(ConfigTemplatePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
