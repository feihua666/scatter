DROP TABLE IF EXISTS scatter_generator_config_template;
CREATE TABLE `scatter_generator_config_template` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `template_name` varchar(255) NOT NULL COMMENT '模板名称，文件名',
  `template_content` text COMMENT '模板内容',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `template_name` (`template_name`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='模板配置表';
