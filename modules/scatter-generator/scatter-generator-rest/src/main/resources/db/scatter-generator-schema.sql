DROP TABLE IF EXISTS scatter_generator_config_template;
CREATE TABLE `scatter_generator_config_template` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `template_name` varchar(255) NOT NULL COMMENT '模板名称，文件名',
  `template_content` text COMMENT '模板内容',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `template_name` (`template_name`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='模板配置表';
DROP TABLE IF EXISTS scatter_generator_config_params;
CREATE TABLE `scatter_generator_config_params` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `label` varchar(255) NOT NULL COMMENT '标签名',
  `value` varchar(255) NOT NULL COMMENT '值',
  `value_type` varchar(255) NOT NULL COMMENT '值类型，boolean,number,string',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `template_name` (`label`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='参数配置表';
DROP TABLE IF EXISTS scatter_generator_config_datasource;
CREATE TABLE `scatter_generator_config_datasource` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `driver_class_name` varchar(255) NOT NULL COMMENT '驱动类名',
  `url` varchar(500) NOT NULL COMMENT '地址',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据源配置表';
DROP TABLE IF EXISTS scatter_design_component_backend_backend_rel;
CREATE TABLE `scatter_design_component_backend_backend_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_backend_id` varchar(20) NOT NULL COMMENT '后端组件id，外键',
  `depand_component_backend_id` varchar(20) NOT NULL COMMENT '依赖的后端组件id，后端组件依赖，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_backend_id`) USING BTREE,
  KEY `component_java_tag_id` (`depand_component_backend_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='后端组件和后端组件依赖关系表';
DROP TABLE IF EXISTS scatter_generator_table_gen_history;
CREATE TABLE `scatter_generator_table_gen_history` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `table_name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `table_comment` varchar(255) DEFAULT NULL COMMENT '注释',
  `table_type` varchar(255) NOT NULL COMMENT '类型，normal,rel,tree',
  `config_content` text COMMENT '配置内容json',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `table_name` (`table_name`) USING BTREE,
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='生成表历史表';
