DROP TABLE IF EXISTS scatter_generator_table_gen_history;
CREATE TABLE `scatter_generator_table_gen_history` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `table_name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `table_comment` varchar(255) DEFAULT NULL COMMENT '注释',
  `table_type` varchar(255) NOT NULL COMMENT '类型，normal,rel,tree',
  `config_content` text COMMENT '配置内容json',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `table_name` (`table_name`) USING BTREE,
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='生成表历史表';
