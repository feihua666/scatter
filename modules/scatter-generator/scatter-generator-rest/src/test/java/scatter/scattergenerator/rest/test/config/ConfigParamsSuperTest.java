package scatter.scattergenerator.rest.test.config;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.scattergenerator.pojo.config.form.ConfigParamsAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsPageQueryForm;
import scatter.scattergenerator.rest.config.service.IConfigParamsService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 参数配置 测试类基类
* </p>
*
* @author yw
* @since 2021-02-23
*/
public class ConfigParamsSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IConfigParamsService configParamsService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return configParamsService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return configParamsService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ConfigParams mockPo() {
        return JMockData.mock(ConfigParams.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ConfigParamsAddForm mockAddForm() {
        return JMockData.mock(ConfigParamsAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ConfigParamsUpdateForm mockUpdateForm() {
        return JMockData.mock(ConfigParamsUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ConfigParamsPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ConfigParamsPageQueryForm.class, mockConfig);
    }
}