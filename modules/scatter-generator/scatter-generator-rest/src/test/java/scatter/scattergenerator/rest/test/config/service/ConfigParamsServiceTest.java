package scatter.scattergenerator.rest.test.config.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scattergenerator.pojo.config.po.ConfigParams;
import scatter.scattergenerator.pojo.config.form.ConfigParamsAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigParamsPageQueryForm;
import scatter.scattergenerator.rest.test.config.ConfigParamsSuperTest;
import scatter.scattergenerator.rest.config.service.IConfigParamsService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 参数配置 服务测试类
* </p>
*
* @author yw
* @since 2021-02-23
*/
@SpringBootTest
public class ConfigParamsServiceTest extends ConfigParamsSuperTest{

    @Autowired
    private IConfigParamsService configParamsService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ConfigParams> pos = configParamsService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}