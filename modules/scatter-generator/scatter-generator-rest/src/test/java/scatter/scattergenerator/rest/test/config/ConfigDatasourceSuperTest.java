package scatter.scattergenerator.rest.test.config;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourcePageQueryForm;
import scatter.scattergenerator.rest.config.service.IConfigDatasourceService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 数据源配置 测试类基类
* </p>
*
* @author yw
* @since 2021-02-23
*/
public class ConfigDatasourceSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IConfigDatasourceService configDatasourceService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return configDatasourceService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return configDatasourceService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ConfigDatasource mockPo() {
        return JMockData.mock(ConfigDatasource.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ConfigDatasourceAddForm mockAddForm() {
        return JMockData.mock(ConfigDatasourceAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ConfigDatasourceUpdateForm mockUpdateForm() {
        return JMockData.mock(ConfigDatasourceUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ConfigDatasourcePageQueryForm mockPageQueryForm() {
        return JMockData.mock(ConfigDatasourcePageQueryForm.class, mockConfig);
    }
}