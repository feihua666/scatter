package scatter.scattergenerator.rest.test.history.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import scatter.scattergenerator.rest.test.history.TableGenHistorySuperTest;
/**
* <p>
* 生成历史 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-02-27
*/
@SpringBootTest
@AutoConfigureMockMvc
public class TableGenHistoryControllerTest extends TableGenHistorySuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

}