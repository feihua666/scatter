package scatter.scattergenerator.rest.test.config;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplatePageQueryForm;
import scatter.scattergenerator.rest.config.service.IConfigTemplateService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 模板配置 测试类基类
* </p>
*
* @author yw
* @since 2021-02-23
*/
public class ConfigTemplateSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IConfigTemplateService configTemplateService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return configTemplateService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return configTemplateService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ConfigTemplate mockPo() {
        return JMockData.mock(ConfigTemplate.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ConfigTemplateAddForm mockAddForm() {
        return JMockData.mock(ConfigTemplateAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ConfigTemplateUpdateForm mockUpdateForm() {
        return JMockData.mock(ConfigTemplateUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ConfigTemplatePageQueryForm mockPageQueryForm() {
        return JMockData.mock(ConfigTemplatePageQueryForm.class, mockConfig);
    }
}