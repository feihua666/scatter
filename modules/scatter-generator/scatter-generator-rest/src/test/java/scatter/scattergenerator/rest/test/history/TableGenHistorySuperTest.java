package scatter.scattergenerator.rest.test.history;

import cn.hutool.core.util.ReflectUtil;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BaseTreePo;
import scatter.scattergenerator.pojo.history.form.TableGenHistoryPageQueryForm;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.rest.history.service.ITableGenHistoryService;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
/**
* <p>
* 生成历史 测试类基类
* </p>
*
* @author yw
* @since 2021-02-27
*/
public class TableGenHistorySuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ITableGenHistoryService tableGenHistoryService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return tableGenHistoryService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return tableGenHistoryService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public TableGenHistory mockPo() {
        return JMockData.mock(TableGenHistory.class, mockConfig);
    }


    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public TableGenHistoryPageQueryForm mockPageQueryForm() {
        return JMockData.mock(TableGenHistoryPageQueryForm.class, mockConfig);
    }
}