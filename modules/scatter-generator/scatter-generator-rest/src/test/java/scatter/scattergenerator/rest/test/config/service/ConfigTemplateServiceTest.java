package scatter.scattergenerator.rest.test.config.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scattergenerator.pojo.config.po.ConfigTemplate;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplateUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigTemplatePageQueryForm;
import scatter.scattergenerator.rest.test.config.ConfigTemplateSuperTest;
import scatter.scattergenerator.rest.config.service.IConfigTemplateService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 模板配置 服务测试类
* </p>
*
* @author yw
* @since 2021-02-23
*/
@SpringBootTest
public class ConfigTemplateServiceTest extends ConfigTemplateSuperTest{

    @Autowired
    private IConfigTemplateService configTemplateService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ConfigTemplate> pos = configTemplateService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}