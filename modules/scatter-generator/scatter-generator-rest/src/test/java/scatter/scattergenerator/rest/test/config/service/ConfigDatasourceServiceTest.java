package scatter.scattergenerator.rest.test.config.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scattergenerator.pojo.config.po.ConfigDatasource;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceAddForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourceUpdateForm;
import scatter.scattergenerator.pojo.config.form.ConfigDatasourcePageQueryForm;
import scatter.scattergenerator.rest.test.config.ConfigDatasourceSuperTest;
import scatter.scattergenerator.rest.config.service.IConfigDatasourceService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 数据源配置 服务测试类
* </p>
*
* @author yw
* @since 2021-02-23
*/
@SpringBootTest
public class ConfigDatasourceServiceTest extends ConfigDatasourceSuperTest{

    @Autowired
    private IConfigDatasourceService configDatasourceService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ConfigDatasource> pos = configDatasourceService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}