package scatter.scattergenerator.rest.test.history.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scattergenerator.pojo.history.po.TableGenHistory;
import scatter.scattergenerator.rest.history.service.ITableGenHistoryService;
import scatter.scattergenerator.rest.test.history.TableGenHistorySuperTest;

import java.util.List;
/**
* <p>
* 生成历史 服务测试类
* </p>
*
* @author yw
* @since 2021-02-27
*/
@SpringBootTest
public class TableGenHistoryServiceTest extends TableGenHistorySuperTest{

    @Autowired
    private ITableGenHistoryService tableGenHistoryService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<TableGenHistory> pos = tableGenHistoryService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}