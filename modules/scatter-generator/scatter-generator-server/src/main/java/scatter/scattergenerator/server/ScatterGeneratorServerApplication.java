package scatter.scattergenerator.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.generator.GeneratorConfiguration;
import scatter.scattergenerator.rest.ScatterGeneratorConfiguration;

/**
 * Created by yangwei
 * Created at 2021/2/23 9:44
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@Import({CommonRestConfig.class, ScatterGeneratorConfiguration.class, GeneratorConfiguration.class})
public class ScatterGeneratorServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScatterGeneratorServerApplication.class, args);
    }
}
