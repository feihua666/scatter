package scatter.scattergenerator.pojo.gen.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/24 12:46
 */
@Setter
@Getter
@ApiModel(value="代码生成表单对象")
public class GenForm extends BaseForm {

    /**
     * 作者
     */
    @NotEmpty(message="作者不能为空")
    @ApiModelProperty(value = "作者",required = true)
    private String author;
    /**
     * 模块名称,分组名
     */
    @ApiModelProperty(value = "模块名称",required = true)
    private String moduleName;
    /**
     * 组件名称
     */
    @NotEmpty(message="组件名称不能为空")
    @ApiModelProperty(value = "组件名称",required = true)
    private String componentName;

    /**
     * 表类型
     */
    @NotEmpty(message="表类型不能为空")
    @ApiModelProperty(value = "表类型",required = true)
    private String tableType;
    /**
     * 表前缀
     */
    @ApiModelProperty(value = "表前缀")
    private String tablePrefix;

    /**
     * 是否覆盖已有文件
     */
    @ApiModelProperty(value = "是否覆盖已有文件")
    private Boolean isFileOverride = false;
    /**
     * 是否删除生成的文件
     */
    @ApiModelProperty(value = "是否删除生成的文件")
    private Boolean isFileDelete = false;

    /**
     * 位置，提供两个位置，一个是component，另一个是module
     */
    @NotEmpty(message="位置不能为空")
    @ApiModelProperty(value = "位置",required = true)
    private String position;

    @NotNull(message="表信息不能为空")
    @ApiModelProperty(value = "表信息",required = true)
    private GenTableForm tableInfo;
}
