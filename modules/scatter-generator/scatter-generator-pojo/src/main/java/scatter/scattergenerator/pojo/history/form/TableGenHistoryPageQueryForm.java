package scatter.scattergenerator.pojo.history.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 生成历史分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@OrderBy(value ="createAt",asc = false)
@Setter
@Getter
@ApiModel(value="生成历史分页表单对象")
public class TableGenHistoryPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "名称，模糊查询")
    private String tableName;

    @Like
    @ApiModelProperty(value = "注释")
    private String tableComment;

    @ApiModelProperty(value = "类型，normal,rel,tree")
    private String tableType;


}
