package scatter.scattergenerator.pojo.table.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * <p>
 * 生成table响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="生成table响应对象")
public class TableVo extends BaseIdVo {

    @ApiModelProperty(value = "名称，模糊查询")
    private String tableName;

    @ApiModelProperty(value = "注释")
    private String tableComment;



    // 对应 xxx
    @ApiModelProperty(value = "rel注释1")
    private String tableCommentRel1;
    // 对应 xxxx
    @ApiModelProperty(value = "rel注释2")
    private String tableCommentRel2;

    @ApiModelProperty(value = "类型，normal,rel,tree")
    private String tableType;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

    // 列信息
    @ApiModelProperty(value = "列信息")
    List<TableColumnVo> tableColumns;
}
