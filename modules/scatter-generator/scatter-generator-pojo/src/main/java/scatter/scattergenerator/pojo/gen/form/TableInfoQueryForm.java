package scatter.scattergenerator.pojo.gen.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 加载表信息表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="加载表信息表单对象")
public class TableInfoQueryForm extends BaseAddForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String tableName;

    @NotEmpty(message="数据源配置id不能为空")
    @ApiModelProperty(value = "数据源配置id",required = true)
    private String configDataSourceId;


}
