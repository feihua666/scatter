package scatter.scattergenerator.pojo.config.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 数据源配置分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="数据源配置分页表单对象")
public class ConfigDatasourcePageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "驱动类名")
    private String driverClassName;

    @ApiModelProperty(value = "地址")
    private String url;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
