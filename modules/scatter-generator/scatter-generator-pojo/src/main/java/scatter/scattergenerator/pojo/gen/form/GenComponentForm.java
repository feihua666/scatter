package scatter.scattergenerator.pojo.gen.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 项目/组件生成表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="项目/组件生成表单对象")
public class GenComponentForm extends BaseForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String componentName;

    @NotEmpty(message="位置不能为空")
    @ApiModelProperty(value = "位置",required = true)
    private String position;


}
