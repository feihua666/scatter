package scatter.scattergenerator.pojo.table.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 生成table列响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="生成table列响应对象")
public class TableColumnVo extends BaseIdVo {

    @ApiModelProperty(value = "表id")
    private String tableId;

    @ApiModelProperty(value = "名称，模糊查询")
    private String columnName;

    @ApiModelProperty(value = "注释")
    private String columnComment;

    @ApiModelProperty(value = "是否必须")
    private Boolean isRequired;

    @ApiModelProperty(value = "是否唯一")
    private Boolean isUnique;

    @ApiModelProperty(value = "是否模糊查询")
    private Boolean isQueryLike;

    @ApiModelProperty(value = "是否排序")
    private Boolean isOrderby;

    @ApiModelProperty(value = "是否排序升序")
    private Boolean isOrderbyAsc;

    @ApiModelProperty(value = "是否外键")
    private Boolean isForeignKey;

    @ApiModelProperty(value = "长度限制")
    private Integer lengthLimit;

    @ApiModelProperty(value = "小数位置限制")
    private Integer fractionlengthLimit;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
