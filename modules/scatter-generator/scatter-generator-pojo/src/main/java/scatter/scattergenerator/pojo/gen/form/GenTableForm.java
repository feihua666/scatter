package scatter.scattergenerator.pojo.gen.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/24 12:46
 */
@Setter
@Getter
@ApiModel(value="代码生成表信息表单对象")
public class GenTableForm extends BaseForm {

    @NotEmpty(message="表名称不能为空")
    @ApiModelProperty(value = "表名称",required = true)
    private String tableName;

    @ApiModelProperty(value = "注释",required = true)
    private String tableComment;

    @ApiModelProperty(value = "rel注释1")
    private String tableCommentRel1;

    @ApiModelProperty(value = "rel注释2")
    private String tableCommentRel2;

    @NotEmpty(message="数据源配置id不能为空")
    @ApiModelProperty(value = "数据源配置id",required = true)
    private String configDataSourceId;

    @Valid
    @ApiModelProperty(value = "列信息")
    private List<GenTableColumnForm> tableColumns;

    @Valid
    @ApiModelProperty(value = "模板信息")
    private List<GenTemplateForm> templates;
}
