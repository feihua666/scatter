package scatter.scattergenerator.pojo.config.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 模板配置添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="模板配置添加表单对象")
public class ConfigTemplateAddForm extends BaseAddForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @NotEmpty(message="模板名称不能为空")
    @ApiModelProperty(value = "模板名称，文件名",required = true)
    private String templateName;

    @ApiModelProperty(value = "模板内容")
    private String templateContent;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
