package scatter.scattergenerator.pojo.config.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据源配置表
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_generator_config_datasource")
@ApiModel(value="ConfigDatasource对象", description="数据源配置表")
public class ConfigDatasource extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_CONFIGDATASOURCE_BY_ID = "trans_configdatasource_by_id_scatter.scattergenerator.pojo.config.po";

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "驱动类名")
    private String driverClassName;

    @ApiModelProperty(value = "地址")
    private String url;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;


}
