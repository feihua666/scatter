package scatter.scattergenerator.pojo.config.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 参数配置更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="参数配置更新表单对象")
public class ConfigParamsUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @NotEmpty(message="标签名不能为空")
    @ApiModelProperty(value = "标签名",required = true)
    private String label;

    @NotEmpty(message="值不能为空")
    @ApiModelProperty(value = "值",required = true)
    private String value;

    @NotEmpty(message="值类型不能为空")
    @ApiModelProperty(value = "值类型，boolean,number,string",required = true)
    private String valueType;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
