package scatter.scattergenerator.pojo.config.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 参数配置响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="参数配置响应对象")
public class ConfigParamsVo extends BaseIdVo {

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "标签名")
    private String label;

    @ApiModelProperty(value = "值")
    private String value;

    @ApiModelProperty(value = "值类型，boolean,number,string")
    private String valueType;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
