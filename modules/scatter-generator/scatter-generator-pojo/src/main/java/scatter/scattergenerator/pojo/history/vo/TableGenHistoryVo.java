package scatter.scattergenerator.pojo.history.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * <p>
 * 生成历史响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Setter
@Getter
@ApiModel(value="生成历史响应对象")
public class TableGenHistoryVo extends BaseIdVo {

    @ApiModelProperty(value = "名称，模糊查询")
    private String tableName;

    @ApiModelProperty(value = "注释")
    private String tableComment;

    @ApiModelProperty(value = "类型，normal,rel,tree")
    private String tableType;

    @ApiModelProperty(value = "配置内容json")
    private String configContent;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createAt;
}
