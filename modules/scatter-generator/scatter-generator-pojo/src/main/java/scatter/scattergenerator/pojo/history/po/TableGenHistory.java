package scatter.scattergenerator.pojo.history.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 生成表历史表
 * </p>
 *
 * @author yw
 * @since 2021-02-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_generator_table_gen_history")
@ApiModel(value="TableGenHistory对象", description="生成表历史表")
public class TableGenHistory extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_TABLEGENHISTORY_BY_ID = "trans_tablegenhistory_by_id_scatter.scattergenerator.pojo.history.po";

    @ApiModelProperty(value = "名称，模糊查询")
    private String tableName;

    @ApiModelProperty(value = "注释")
    private String tableComment;

    @ApiModelProperty(value = "类型，normal,rel,tree")
    private String tableType;

    @ApiModelProperty(value = "配置内容json")
    private String configContent;


}
