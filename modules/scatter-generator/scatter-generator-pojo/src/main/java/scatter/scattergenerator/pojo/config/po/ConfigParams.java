package scatter.scattergenerator.pojo.config.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 参数配置表
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_generator_config_params")
@ApiModel(value="ConfigParams对象", description="参数配置表")
public class ConfigParams extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_CONFIGPARAMS_BY_ID = "trans_configparams_by_id_scatter.scattergenerator.pojo.config.po";

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "标签名")
    private String label;

    @ApiModelProperty(value = "值")
    private String value;

    @ApiModelProperty(value = "值类型，boolean,number,string")
    private String valueType;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;


}
