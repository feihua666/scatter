package scatter.scattergenerator.pojo.gen.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/24 12:46
 */
@Setter
@Getter
@ApiModel(value="代码生成表信息表单对象")
public class GenTemplateForm extends BaseForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String name;

    @NotEmpty(message="模板名称不能为空")
    @ApiModelProperty(value = "模板名称",required = true)
    private String templateName;

    @ApiModelProperty(value = "是否生成",required = true)
    private Boolean isGenerate = true;

}
