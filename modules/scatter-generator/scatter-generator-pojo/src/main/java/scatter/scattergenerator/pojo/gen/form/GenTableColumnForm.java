package scatter.scattergenerator.pojo.gen.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/24 12:46
 */
@Setter
@Getter
@ApiModel(value="代码生成表字段表单对象")
public class GenTableColumnForm extends BaseForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String columnName;

    @NotEmpty(message="注释不能为空")
    @ApiModelProperty(value = "注释",required = true)
    private String columnComment;

    @NotNull(message="是否必须不能为空")
    @ApiModelProperty(value = "是否必须",required = true)
    private Boolean isRequired;

    @NotNull(message="是否唯一不能为空")
    @ApiModelProperty(value = "是否唯一",required = true)
    private Boolean isUnique;

    @NotNull(message="是否模糊查询不能为空")
    @ApiModelProperty(value = "是否模糊查询",required = true)
    private Boolean isQueryLike;

    @NotNull(message="是否排序不能为空")
    @ApiModelProperty(value = "是否排序",required = true)
    private Boolean isOrderby;

    @NotNull(message="是否排序升序不能为空")
    @ApiModelProperty(value = "是否排序升序",required = true)
    private Boolean isOrderbyAsc;

    @ApiModelProperty(value = "长度限制")
    private Integer lengthLimit;

    @ApiModelProperty(value = "小数位置限制")
    private Integer fractionlengthLimit;

    @ApiModelProperty(value = "是否外键")
    private Boolean isForeignKey;

    @ApiModelProperty(value = "表单")
    private List<String> form;
}
