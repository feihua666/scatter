package scatter.scattergenerator.pojo.config.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 数据源配置添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Setter
@Getter
@ApiModel(value="数据源配置添加表单对象")
public class ConfigDatasourceAddForm extends BaseAddForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @NotEmpty(message="驱动类名不能为空")
    @ApiModelProperty(value = "驱动类名",required = true)
    private String driverClassName;

    @NotEmpty(message="地址不能为空")
    @ApiModelProperty(value = "地址",required = true)
    private String url;

    @NotEmpty(message="用户名不能为空")
    @ApiModelProperty(value = "用户名",required = true)
    private String username;

    @NotEmpty(message="密码不能为空")
    @ApiModelProperty(value = "密码",required = true)
    private String password;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
