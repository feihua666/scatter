package scatter.scattergenerator.pojo.config.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 模板配置表
 * </p>
 *
 * @author yw
 * @since 2021-02-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_generator_config_template")
@ApiModel(value="ConfigTemplate对象", description="模板配置表")
public class ConfigTemplate extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_CONFIGTEMPLATE_BY_ID = "trans_configtemplate_by_id_scatter.scattergenerator.pojo.config.po";

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "模板名称，文件名")
    private String templateName;

    @ApiModelProperty(value = "模板内容")
    private String templateContent;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;


}
