let packageName = require('./package.json').name
module.exports = {
    outputDir: '../../scatter-generator-server/src/main/resources/static',
    lintOnSave: false,
    devServer: {
        disableHostCheck: true,
        port: 9999,
        proxy: 'http://localhost:9998'
    }

}