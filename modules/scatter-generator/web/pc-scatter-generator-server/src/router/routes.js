import ConfigParamsRoute from '../../../../web/pc/config/ConfigParamsRoute.js'
import ConfigTemplateRoute from '../../../../web/pc/config/ConfigTemplateRoute.js'
import ConfigDatasourceRoute from '../../../../web/pc/config/ConfigDatasourceRoute.js'
import TableGenHistoryRoute from '../../../../web/pc/history/TableGenHistoryRoute.js'

import GenUrl from '../views/gen/GenUrl.js'

let indexChildren = []
    .concat(ConfigParamsRoute)
    .concat(ConfigTemplateRoute)
    .concat(ConfigDatasourceRoute)
    .concat(TableGenHistoryRoute)
    .concat([
        {
            path: GenUrl.router.gen,
            component: () => import('../views/gen/Gen.vue'),
        },
        {
            path:  GenUrl.router.genComponent,
            component: () => import('../views/gen/GenComponent.vue'),
        },
    ])
let routes = [

    {
        path: '/',
        component: () => import('../views/Index.vue'),
        children: indexChildren
    },
]

export default routes