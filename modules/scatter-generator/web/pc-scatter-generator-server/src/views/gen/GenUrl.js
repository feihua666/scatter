const basePath = '' + '/gen'
const GenUrl = {
    gen: basePath,
    genComponent: basePath + '/component',
    tableInfo: basePath + '/tableInfo',
    router: {
        gen: '/gen',
        genComponent: '/genComponent',
    }
}
export default GenUrl