import ConfigDatasourceForm from './ConfigDatasourceForm.js'
import ConfigDatasourceTable from './ConfigDatasourceTable.js'
import ConfigDatasourceUrl from './ConfigDatasourceUrl.js'
const ConfigDatasourceMixin = {
    computed: {
        ConfigDatasourceFormOptions() {
            return this.$stDynamicFormTools.options(ConfigDatasourceForm,this.$options.name)
        },
        ConfigDatasourceTableOptions() {
            return ConfigDatasourceTable
        },
        ConfigDatasourceUrl(){
            return ConfigDatasourceUrl
        }
    },
}
export default ConfigDatasourceMixin