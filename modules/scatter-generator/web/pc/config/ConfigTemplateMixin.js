import ConfigTemplateForm from './ConfigTemplateForm.js'
import ConfigTemplateTable from './ConfigTemplateTable.js'
import ConfigTemplateUrl from './ConfigTemplateUrl.js'
const ConfigTemplateMixin = {
    computed: {
        ConfigTemplateFormOptions() {
            return this.$stDynamicFormTools.options(ConfigTemplateForm,this.$options.name)
        },
        ConfigTemplateTableOptions() {
            return ConfigTemplateTable
        },
        ConfigTemplateUrl(){
            return ConfigTemplateUrl
        }
    },
}
export default ConfigTemplateMixin