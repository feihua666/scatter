import ConfigParamsForm from './ConfigParamsForm.js'
import ConfigParamsTable from './ConfigParamsTable.js'
import ConfigParamsUrl from './ConfigParamsUrl.js'
const ConfigParamsMixin = {
    computed: {
        ConfigParamsFormOptions() {
            return this.$stDynamicFormTools.options(ConfigParamsForm,this.$options.name)
        },
        ConfigParamsTableOptions() {
            return ConfigParamsTable
        },
        ConfigParamsUrl(){
            return ConfigParamsUrl
        }
    },
}
export default ConfigParamsMixin