const basePath = '' + '/config/config-datasource'
const ConfigDatasourceUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ConfigDatasourceSearchList',
        add: '/ConfigDatasourceAdd',
        update: '/ConfigDatasourceUpdate',
    }
}
export default ConfigDatasourceUrl