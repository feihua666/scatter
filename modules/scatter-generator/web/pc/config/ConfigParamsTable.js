const ConfigParamsTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'label',
        label: '标签名'
    },
    {
        prop: 'value',
        label: '值'
    },
    {
        prop: 'valueType',
        label: '值类型'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ConfigParamsTable