import ConfigParamsUrl from './ConfigParamsUrl.js'
const ConfigParamsRoute = [
    {
        path: ConfigParamsUrl.router.searchList,
        component: () => import('./element/ConfigParamsSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ConfigParamsSearchList',
            name: '参数配置管理'
        },
        children: [
            {
                path: ConfigParamsUrl.router.add,
                component: () => import('./element/ConfigParamsAdd'),
                meta: {
                    code:'ConfigParamsAdd',
                    showInDrawer: true,
                    name: '参数配置添加'
                }
            },

            {
                path: ConfigParamsUrl.router.update,
                component: () => import('./element/ConfigParamsUpdate'),
                meta: {
                    code:'ConfigParamsUpdate',
                    showInDrawer: true,
                    name: '参数配置修改'
                }
            },
        ]
    },

]
export default ConfigParamsRoute