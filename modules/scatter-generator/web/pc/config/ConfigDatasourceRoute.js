import ConfigDatasourceUrl from './ConfigDatasourceUrl.js'
const ConfigDatasourceRoute = [
    {
        path: ConfigDatasourceUrl.router.searchList,
        component: () => import('./element/ConfigDatasourceSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ConfigDatasourceSearchList',
            name: '数据源配置管理'
        },
        children: [
            {
                path: ConfigDatasourceUrl.router.add,
                component: () => import('./element/ConfigDatasourceAdd'),
                meta: {
                    code:'ConfigDatasourceAdd',
                    showInDrawer: true,
                    name: '数据源配置添加'
                }
            },
            {
                path: ConfigDatasourceUrl.router.update,
                component: () => import('./element/ConfigDatasourceUpdate'),
                meta: {
                    code:'ConfigDatasourceUpdate',
                    showInDrawer: true,
                    name: '数据源配置修改'
                }
            },
        ]
    },

]
export default ConfigDatasourceRoute