const basePath = '' + '/config/config-template'
const ConfigTemplateUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ConfigTemplateSearchList',
        add: '/ConfigTemplateAdd',
        update: '/ConfigTemplateUpdate',
    }
}
export default ConfigTemplateUrl