const basePath = '' + '/config/config-params'
const ConfigParamsUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ConfigParamsSearchList',
        add: '/ConfigParamsAdd',
        update: '/ConfigParamsUpdate',
    }
}
export default ConfigParamsUrl