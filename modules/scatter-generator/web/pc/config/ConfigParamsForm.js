import ConfigParamsUrl from './ConfigParamsUrl.js'
const ConfigParamsForm = [
    {
        ConfigParamsSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'label',
        },
        element:{
            label: '标签',
            required: true,
            options: {
                placeholder: '自定义'
            }
        }
    },
    {
        ConfigParamsSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
            options: {
                placeholder: '英文'
            }
        }
    },

    {
        ConfigParamsSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'value',
        },
        element:{
            label: '值',
            required: true,
        }
    },
    {
        ConfigParamsSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'valueType',
            value: 'string'
        },
        element:{
            label: '值类型',
            required: true,
            type: 'select',
            options: {
                datas: [
                    {
                        id: 'boolean',
                        name: '布尔',
                    },
                    {
                        id: 'number',
                        name: '数值',
                    },
                    {
                        id: 'string',
                        name: '字符串',
                    }
                ]
            }

        }
    },
    {
        ConfigParamsSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
            type: 'textarea'
        }
    },
]
export default ConfigParamsForm