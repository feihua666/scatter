import ConfigTemplateUrl from './ConfigTemplateUrl.js'
const ConfigTemplateRoute = [
    {
        path: ConfigTemplateUrl.router.searchList,
        component: () => import('./element/ConfigTemplateSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ConfigTemplateSearchList',
            name: '模板配置管理'
        },
        children:[
            {
                path: ConfigTemplateUrl.router.add,
                component: () => import('./element/ConfigTemplateAdd'),
                meta: {
                    code:'ConfigTemplateAdd',
                    showInDrawer: true,
                    name: '模板配置添加'
                }
            },
            {
                path: ConfigTemplateUrl.router.update,
                component: () => import('./element/ConfigTemplateUpdate'),
                meta: {
                    code:'ConfigTemplateUpdate',
                    showInDrawer: true,
                    name: '模板配置修改'
                }
            },
        ]
    },

]
export default ConfigTemplateRoute