import ConfigDatasourceUrl from './ConfigDatasourceUrl.js'
const ConfigDatasourceForm = [
    {
        ConfigDatasourceSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        ConfigDatasourceSearchList: {
            element:{
                required: false
            },
            field: {
                value: ''
            },
        },
        field: {
            name: 'driverClassName',
            value: 'com.mysql.cj.jdbc.Driver'
        },
        element:{
            label: '驱动类名',
            required: true,
        }
    },
    {
        ConfigDatasourceSearchList: {
            element:{
                required: false
            },
            field: {
                value: ''
            },
        },
        field: {
            name: 'url',
            value: 'jdbc:mysql://localhost:3306/scatter_dev?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&serverTimezone=GMT%2B8'
        },
        element:{
            label: '地址',
            required: true,
        }
    },
    {
        ConfigDatasourceSearchList: false,
        field: {
            name: 'username',
            value: 'root'
        },
        element:{
            label: '用户名',
            required: true,
        }
    },
    {
        ConfigDatasourceSearchList: false,
        field: {
            name: 'password',
            value: 'root'
        },
        element:{
            label: '密码',
            required: true,
        }
    },
    {
        ConfigDatasourceSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
            type: 'textarea'
        }
    },
]
export default ConfigDatasourceForm