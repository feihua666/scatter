const ConfigDatasourceTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'driverClassName',
        label: '驱动类名'
    },
    {
        prop: 'url',
        label: '地址'
    },
    {
        prop: 'username',
        label: '用户名'
    },
    {
        prop: 'password',
        label: '密码'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ConfigDatasourceTable