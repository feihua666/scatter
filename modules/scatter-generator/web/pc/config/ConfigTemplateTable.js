const ConfigTemplateTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'templateName',
        label: '模板名称'
    },
    {
        prop: 'templateContent',
        label: '模板内容'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ConfigTemplateTable