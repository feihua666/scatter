import ConfigTemplateUrl from './ConfigTemplateUrl.js'
const ConfigTemplateForm = [
    {
        ConfigTemplateSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        ConfigTemplateSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'templateName',
        },
        element:{
            label: '模板名称',
            required: true,
        }
    },
    {
        field: {
            name: 'templateContent',
        },
        element:{
            label: '模板内容',
        }
    },
    {
        ConfigTemplateSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
            type: 'textarea'
        }
    },
]
export default ConfigTemplateForm