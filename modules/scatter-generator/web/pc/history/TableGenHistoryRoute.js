import TableGenHistoryUrl from './TableGenHistoryUrl.js'
const TableGenHistoryRoute = [
    {
        path: TableGenHistoryUrl.router.searchList,
        component: () => import('./element/TableGenHistorySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'TableGenHistorySearchList',
            name: '生成历史管理'
        }
    },
]
export default TableGenHistoryRoute