import TableGenHistoryForm from './TableGenHistoryForm.js'
import TableGenHistoryTable from './TableGenHistoryTable.js'
import TableGenHistoryUrl from './TableGenHistoryUrl.js'
const TableGenHistoryMixin = {
    computed: {
        TableGenHistoryFormOptions() {
            return this.$stDynamicFormTools.options(TableGenHistoryForm,this.$options.name)
        },
        TableGenHistoryTableOptions() {
            return TableGenHistoryTable
        },
        TableGenHistoryUrl(){
            return TableGenHistoryUrl
        }
    },
}
export default TableGenHistoryMixin