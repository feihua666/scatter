import TableGenHistoryUrl from './TableGenHistoryUrl.js'
const TableGenHistoryForm = [
    {
        field: {
            name: 'tableName',
        },
        element:{
            label: '名称',
        }
    },
    {
        field: {
            name: 'tableComment',
        },
        element:{
            label: '注释',
        }
    },
    {
        field: {
            name: 'tableType',
        },
        element:{
            label: '类型',
            type: 'select',
            options: {
                datas: [
                    {
                        id: 'rel',
                        name: 'rel',
                    },
                    {
                        id: 'normal',
                        name: 'normal',
                    },
                    {
                        id: 'tree',
                        name: 'tree',
                    }
                ]
            }
        }
    },
]
export default TableGenHistoryForm