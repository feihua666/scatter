const basePath = '' + '/history/table-gen-history'
const TableGenHistoryUrl = {
    searchList: basePath + '/getPage',
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/TableGenHistorySearchList',
    }
}
export default TableGenHistoryUrl