const TableGenHistoryTable = [
    {
        prop: 'tableName',
        label: '名称'
    },
    {
        prop: 'tableComment',
        label: '注释'
    },
    {
        prop: 'tableType',
        label: '类型'
    },
    {
        prop: 'createAt',
        label: '创建时间'
    },
]
export default TableGenHistoryTable