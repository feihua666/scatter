import AreaRoute from '../../../../../../components/area/web/pc/AreaRoute.js'
import DictRoute from '../../../../../../components/dict/web/pc/DictRoute.js'
import FuncRoute from '../../../../../../components/func/web/pc/FuncRoute.js'
import RoleRoutesEntry from '../../../../../../components/role/web/pc/RoleRoutesEntry.js'
import UserSimpleRoute from '../../../../../../components/usersimple/web/pc/UserSimpleRoute.js'
import FuncGroupRoute from '../../../../../../components/func/web/pc/FuncGroupRoute.js'

import ServerResourceCodeRoute from '../../../../../../components/server-resource/web/pc/code/ServerResourceCodeRoute.js'
import ServerResourceAccountRoute from '../../../../../../components/server-resource/web/pc/general/ServerResourceAccountRoute.js'
import ServerResourceCodeProjectVcRelRoute from '../../../../../../components/server-resource/web/pc/general/ServerResourceCodeProjectVcRelRoute.js'
import ServerResourceEnvironmentRoute from '../../../../../../components/server-resource/web/pc/general/ServerResourceEnvironmentRoute.js'
import ServerResourceMachineRoute from '../../../../../../components/server-resource/web/pc/machine/ServerResourceMachineRoute.js'
import ServerResourceMachineRoomRoute from '../../../../../../components/server-resource/web/pc/machine/ServerResourceMachineRoomRoute.js'
import ServerResourceProjectRoute from '../../../../../../components/server-resource/web/pc/project/ServerResourceProjectRoute.js'

let indexChildren = [].concat(AreaRoute)
.concat(DictRoute)
.concat(FuncRoute)
.concat(RoleRoutesEntry)
.concat(UserSimpleRoute)
.concat(FuncGroupRoute)

.concat(ServerResourceCodeRoute)
.concat(ServerResourceAccountRoute)
.concat(ServerResourceCodeProjectVcRelRoute)
.concat(ServerResourceEnvironmentRoute)
.concat(ServerResourceMachineRoute)
.concat(ServerResourceMachineRoomRoute)
.concat(ServerResourceProjectRoute)

let routes = [
    {
        path: '/login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/index',
        component: () => import('../views/Index.vue'),
        children: indexChildren
    },
]

export default routes