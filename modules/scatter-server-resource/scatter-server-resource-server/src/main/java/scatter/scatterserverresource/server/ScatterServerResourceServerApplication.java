package scatter.scatterserverresource.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import scatter.area.rest.AreaConfiguration;
import scatter.captcha.rest.CaptchaConfiguration;
import scatter.common.rest.config.CommonCacheConfig;
import scatter.common.rest.config.CommonGlobalMethodSecurityConfig;
import scatter.common.rest.config.CommonRestConfig;
import scatter.common.rest.config.CommonWebSecurityConfig;
import scatter.scatterserverresource.rest.ScatterServerResourceConfiguration;
import scatter.dict.rest.DictConfiguration;
import scatter.func.rest.FuncConfiguration;
import scatter.identifier.rest.IdentifierConfiguration;
import scatter.role.rest.RoleConfiguration;
import scatter.serverresource.rest.ServerResourceConfiguration;
import scatter.usersimple.rest.UsersimpleConfiguration;

/**
 * <p>
 * server 应用启动类
 * </p>
 *
 * @author yangwei
 * @since 2022-01-28 15:53
 */
@SpringBootApplication
@Import({CommonCacheConfig.class, CommonRestConfig.class, CommonGlobalMethodSecurityConfig.class, CommonWebSecurityConfig.class,
		ScatterServerResourceConfiguration.class,
		DictConfiguration.class,
		AreaConfiguration.class,
		FuncConfiguration.class,
		UsersimpleConfiguration.class,
		IdentifierConfiguration.class,
		RoleConfiguration.class,
		CaptchaConfiguration.class,
		ServerResourceConfiguration.class,
})
public class ScatterServerResourceServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(ScatterServerResourceServerApplication.class, args);
	}

}
