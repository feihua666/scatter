package scatter.scatterhello.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.scatterhello.rest.**.mapper")
public class ScatterHelloConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "scatterhello";

}
