package scatter.scatterhello.rest.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comp.pojo.po.Comp;
import scatter.dept.pojo.po.Dept;
import scatter.user.pojo.po.User;
import scatter.user.rest.service.IUserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 公司负责人翻译
 * Created by yangwei
 * Created at 2021/3/23 19:01
 */
@Component
public class TransBackendUserImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserService iUserService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type, Comp.TRANS_COMP_MASTER_USER_NICKNAME_BY_MASTER_USER_ID, Dept.TRANS_DEPT_MASTER_USER_NICKNAME_BY_MASTER_USER_ID);
    }

    @Override
    public boolean supportBatch(String type) {
        return isEqualAny(type, Comp.TRANS_COMP_MASTER_USER_NICKNAME_BY_MASTER_USER_ID, Dept.TRANS_DEPT_MASTER_USER_NICKNAME_BY_MASTER_USER_ID);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqualAny(type,Comp.TRANS_COMP_MASTER_USER_NICKNAME_BY_MASTER_USER_ID, Dept.TRANS_DEPT_MASTER_USER_NICKNAME_BY_MASTER_USER_ID)) {
            List<User> byId = iUserService.listByIds(keys);
            if (isEmpty(byId)) {
                return null;
            }

            return byId.stream().map(item->new TransResult<Object, String>(item.getNickname(),item.getId())).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqualAny(type,Comp.TRANS_COMP_MASTER_USER_NICKNAME_BY_MASTER_USER_ID, Dept.TRANS_DEPT_MASTER_USER_NICKNAME_BY_MASTER_USER_ID)) {
            User byId = iUserService.getById(key);
            if (byId == null) {
                return null;
            }
            return new TransResult(byId.getNickname(),key);
        }
        return null;
    }
}
