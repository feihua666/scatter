import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router/router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../../../../../components/common/web/st.css'
import ToolsPlugin from '../../../../../components/common/web/tools/ToolsPlugin.js'
import ElementPlugin from '../../../../../components/common/web/components/pc/element/ElementPlugin.js'
import CommonPlugin from '../../../../../components/common/web/components/pc/common/CommonPlugin.js'

import UserUrl from '../../../../../components/user/web/pc/UserUrl.js'

Vue.config.productionTip = false
Vue.use(ElementUI)
// 自定义工具插件
Vue.use(ToolsPlugin)
// 自定义基于element组件
Vue.use(ElementPlugin)
// 自定义基于通用组件
Vue.use(CommonPlugin)
Vue.use(VueAxios, axios)
Vue.use(VueRouter)
// 上传组件全局配置上传地址
//Vue.prototype.$fileUploadUrl = 'https://jsonplaceholder.typicode.com/posts/'
// 字典项查询全局配置数据地址，依赖后台字典组件
//Vue.prototype.$dictItemUrl = DictUrl.getItems
// 全局配置
Vue.prototype.$scatterConfig = {
    CompForm: {
        userRemoteSearchUrl: UserUrl.searchList
    },
    DeptForm: {
        userRemoteSearchUrl: UserUrl.searchList
    }
}
new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
