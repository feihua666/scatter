import AreaRoute from '../../../../../../components/area/web/pc/AreaRoute.js'
import DictRoute from '../../../../../../components/dict/web/pc/DictRoute.js'
import FuncRoute from '../../../../../../components/func/web/pc/FuncRoute.js'
import RoleRoutesEntry from '../../../../../../components/role/web/pc/RoleRoutesEntry.js'
import UserSimpleRoute from '../../../../../../components/usersimple/web/pc/UserSimpleRoute.js'
import FuncGroupRoute from '../../../../../../components/func/web/pc/FuncGroupRoute.js'
import FileRoute from '../../../../../../components/file/web/pc/FileRoute.js'

import WxMpRoutesEntry from '../../../../../../components/wx-mp/web/pc/WxMpRoutesEntry.js'


import BannerRoute from '../../../../../../components/banner/web/pc/BannerRoute'
import ErrorLogRoute from '../../../../../../components/error-log/web/pc/ErrorLogRoute'
import ScheduleRoutesEntry from '../../../../../../components/schedule-quartz/web/pc/ScheduleRoutesEntry'

import CompRoute from '../../../../../../components/comp/web/pc/CompRoute.js'
import DeptRoutesEntry from '../../../../../../components/dept/web/pc/DeptRoutesEntry.js'
import JobRoutesEntry from '../../../../../../components/job/web/pc/JobRoutesEntry.js'
import PostRoute from '../../../../../../components/post/web/pc/PostRoute.js'
import UserRoute from '../../../../../../components/user/web/pc/UserRoute.js'
import UserPostRoute from '../../../../../../components/user-post/web/pc/UserPostRoute.js'

import IdentifierRoutesEntry from '../../../../../../components/identifier/web/pc/IdentifierRoutesEntry.js'
import DataConstraintRoutesEntry from '../../../../../../components/data-constraint/web/pc/DataConstraintRoutesEntry.js'


let indexChildren = [].concat(AreaRoute)
    .concat(DictRoute)
    .concat(FuncRoute)
    .concat(RoleRoutesEntry)
    .concat(UserSimpleRoute)
    .concat(FuncGroupRoute)
    .concat(FileRoute)

    .concat(WxMpRoutesEntry)

    .concat(BannerRoute)
    .concat(ErrorLogRoute)
    .concat(ScheduleRoutesEntry)

    .concat(CompRoute)
    .concat(DeptRoutesEntry)
    .concat(JobRoutesEntry)
    .concat(PostRoute)
    .concat(UserRoute)
    .concat(UserPostRoute)
    .concat(IdentifierRoutesEntry)
    .concat(DataConstraintRoutesEntry)
let routes = [
    {
        path: '/login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/index',
        component: () => import('../views/Index.vue'),
        children: indexChildren
    },
]

export default routes