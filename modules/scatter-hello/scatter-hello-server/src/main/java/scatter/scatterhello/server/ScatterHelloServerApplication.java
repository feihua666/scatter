package scatter.scatterhello.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import scatter.area.rest.AreaConfiguration;
import scatter.banner.rest.BannerConfiguration;
import scatter.captcha.rest.CaptchaConfiguration;
import scatter.comment.rest.CommentConfiguration;
import scatter.common.boot.OnRunningBanner;
import scatter.common.rest.config.CommonGlobalMethodSecurityConfig;
import scatter.common.rest.config.CommonRestConfig;
import scatter.common.rest.config.CommonWebSecurityConfig;
import scatter.comp.rest.CompConfiguration;
import scatter.dataconstraint.rest.DataConstraintConfiguration;
import scatter.dept.rest.DeptConfiguration;
import scatter.dict.rest.DictConfiguration;
import scatter.errorlog.rest.ErrorLogConfiguration;
import scatter.file.rest.FileConfiguration;
import scatter.func.rest.FuncConfiguration;
import scatter.identifier.rest.IdentifierConfiguration;
import scatter.job.rest.JobConfiguration;
import scatter.moment.rest.MomentConfiguration;
import scatter.post.rest.PostConfiguration;
import scatter.role.rest.RoleConfiguration;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.tools.rest.ToolsConfiguration;
import scatter.user.rest.UserConfiguration;
import scatter.userpost.rest.UserPostConfiguration;
import scatter.usersimple.rest.UsersimpleConfiguration;
import scatter.scatterhello.rest.ScatterHelloConfiguration ;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * Created by yangwei
 * Created at 2021/3/22 14:24
 */
@SpringBootApplication
@EnableRedisHttpSession
@Import({CommonRestConfig.class, CommonGlobalMethodSecurityConfig.class, CommonWebSecurityConfig.class,
        ScatterHelloConfiguration.class,
        AreaConfiguration.class,
        DictConfiguration.class,
        FuncConfiguration.class,
        UsersimpleConfiguration.class,
        IdentifierConfiguration.class,
        RoleConfiguration.class,
        FileConfiguration.class,
        WxMpConfiguration.class,
        CaptchaConfiguration.class,
        BannerConfiguration.class,
        ErrorLogConfiguration.class,
        ScheduleQuartzConfiguration.class,
        ToolsConfiguration.class,
        MomentConfiguration.class,
        CommentConfiguration.class,

        CompConfiguration.class,
        DeptConfiguration.class,
        JobConfiguration.class,
        PostConfiguration.class,
        UserConfiguration.class,
        UserPostConfiguration.class,
        DataConstraintConfiguration.class,
})
public class ScatterHelloServerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ScatterHelloServerApplication.class, args);
        OnRunningBanner.banner_success();
        OnRunningBanner.banner_address(run.getEnvironment());

    }
}

