package scatter.scatterdesign.pojo.component.frontend.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 前端组件和后端组件关系响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="前端组件和后端组件关系响应对象")
public class ComponentFrontendBackendRelVo extends BaseIdVo {

    @ApiModelProperty(value = "前端组件id，外键")
    private String componentFrontendId;

    @ApiModelProperty(value = "后端组件id，外键")
    private String componentBackendId;

}
