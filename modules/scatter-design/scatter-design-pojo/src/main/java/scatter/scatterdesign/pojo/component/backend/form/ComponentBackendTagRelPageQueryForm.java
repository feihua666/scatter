package scatter.scatterdesign.pojo.component.backend.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 后端组件标签关系分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="后端组件标签关系分页表单对象")
public class ComponentBackendTagRelPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "后端组件id，外键")
    private String componentBackendId;

    @ApiModelProperty(value = "后端组件标签id，外键")
    private String componentBackendTagId;

}
