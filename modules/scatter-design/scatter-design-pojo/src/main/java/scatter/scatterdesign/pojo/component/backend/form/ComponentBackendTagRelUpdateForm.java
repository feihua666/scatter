package scatter.scatterdesign.pojo.component.backend.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 后端组件标签关系更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="后端组件标签关系更新表单对象")
public class ComponentBackendTagRelUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="后端组件id不能为空")
    @ApiModelProperty(value = "后端组件id，外键",required = true)
    private String componentBackendId;

    @NotEmpty(message="后端组件标签id不能为空")
    @ApiModelProperty(value = "后端组件标签id，外键",required = true)
    private String componentBackendTagId;

}
