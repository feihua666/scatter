package scatter.scatterdesign.pojo.component.backend.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 后端组件标签关系表
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_design_component_backend_tag_rel")
@ApiModel(value="ComponentBackendTagRel对象", description="后端组件标签关系表")
public class ComponentBackendTagRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMPONENTBACKENDTAGREL_BY_ID = "trans_componentbackendtagrel_by_id_scatter.scatterdesign.pojo.component.backend.po";

    @ApiModelProperty(value = "后端组件id，外键")
    private String componentBackendId;

    @ApiModelProperty(value = "后端组件标签id，外键")
    private String componentBackendTagId;


}
