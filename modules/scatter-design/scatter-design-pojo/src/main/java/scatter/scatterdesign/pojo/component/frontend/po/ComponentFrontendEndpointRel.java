package scatter.scatterdesign.pojo.component.frontend.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 前端组件闪端点关系表
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_design_component_frontend_endpoint_rel")
@ApiModel(value="ComponentFrontendEndpointRel对象", description="前端组件闪端点关系表")
public class ComponentFrontendEndpointRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMPONENTFRONTENDENDPOINTREL_BY_ID = "trans_componentfrontendendpointrel_by_id_scatter.scatterdesign.pojo.component.frontend.po";

    @ApiModelProperty(value = "前端组件id，外键")
    private String componentFrontendId;

    @ApiModelProperty(value = "前端组件端点id，外键")
    private String componentFrontendEndpointId;


}
