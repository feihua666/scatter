package scatter.scatterdesign.pojo.component.backend.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 后端组件表
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_design_component_backend")
@ApiModel(value="ComponentBackend对象", description="后端组件表")
public class ComponentBackend extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMPONENTBACKEND_BY_ID = "trans_componentbackend_by_id_scatter.scatterdesign.pojo.component.backend.po";

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "开发语言，字典")
    private String developLangDictId;

    @ApiModelProperty(value = "是否必须")
    private Boolean isMust;

    @ApiModelProperty(value = "介绍")
    private String description;

    @ApiModelProperty(value = "相较上一次发布，是否新增")
    private Boolean isNew;

    @ApiModelProperty(value = "相较上一次发布，是否有更新")
    private Boolean isUpdate;

    @ApiModelProperty(value = "更新描述")
    private String updateDescription;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime publishAt;

    @ApiModelProperty(value = "发布用户id")
    private String publishUserId;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;


}
