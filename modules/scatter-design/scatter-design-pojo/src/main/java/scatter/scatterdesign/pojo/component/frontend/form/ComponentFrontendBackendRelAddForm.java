package scatter.scatterdesign.pojo.component.frontend.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 前端组件和后端组件关系添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="前端组件和后端组件关系添加表单对象")
public class ComponentFrontendBackendRelAddForm extends BaseAddForm {

    @NotEmpty(message="前端组件id不能为空")
    @ApiModelProperty(value = "前端组件id，外键",required = true)
    private String componentFrontendId;

    @NotEmpty(message="后端组件id不能为空")
    @ApiModelProperty(value = "后端组件id，外键",required = true)
    private String componentBackendId;

}
