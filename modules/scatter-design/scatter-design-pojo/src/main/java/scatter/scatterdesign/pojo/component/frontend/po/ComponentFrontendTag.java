package scatter.scatterdesign.pojo.component.frontend.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 前端组件标签表
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("scatter_design_component_frontend_tag")
@ApiModel(value="ComponentFrontendTag对象", description="前端组件标签表")
public class ComponentFrontendTag extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMPONENTFRONTENDTAG_BY_ID = "trans_componentfrontendtag_by_id_scatter.scatterdesign.pojo.component.frontend.po";

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;


}
