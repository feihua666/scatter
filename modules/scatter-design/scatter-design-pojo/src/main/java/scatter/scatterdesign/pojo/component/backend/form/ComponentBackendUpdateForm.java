package scatter.scatterdesign.pojo.component.backend.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 后端组件更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="后端组件更新表单对象")
public class ComponentBackendUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @NotEmpty(message="开发语言不能为空")
    @ApiModelProperty(value = "开发语言，字典",required = true)
    private String developLangDictId;

    @NotNull(message="是否必须不能为空")
    @ApiModelProperty(value = "是否必须",required = true)
    private Boolean isMust;

    @NotEmpty(message="介绍不能为空")
    @ApiModelProperty(value = "介绍",required = true)
    private String description;

    @NotNull(message="相较上一次发布不能为空")
    @ApiModelProperty(value = "相较上一次发布，是否新增",required = true)
    private Boolean isNew;

    @NotNull(message="相较上一次发布不能为空")
    @ApiModelProperty(value = "相较上一次发布，是否有更新",required = true)
    private Boolean isUpdate;

    @ApiModelProperty(value = "更新描述")
    private String updateDescription;

    @NotNull(message="发布时间不能为空")
    @ApiModelProperty(value = "发布时间",required = true)
    private LocalDateTime publishAt;

    @NotEmpty(message="发布用户id不能为空")
    @ApiModelProperty(value = "发布用户id",required = true)
    private String publishUserId;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
