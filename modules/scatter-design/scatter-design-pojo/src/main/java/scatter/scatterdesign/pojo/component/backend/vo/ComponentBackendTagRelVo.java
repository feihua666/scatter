package scatter.scatterdesign.pojo.component.backend.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 后端组件标签关系响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="后端组件标签关系响应对象")
public class ComponentBackendTagRelVo extends BaseIdVo {

    @ApiModelProperty(value = "后端组件id，外键")
    private String componentBackendId;

    @ApiModelProperty(value = "后端组件标签id，外键")
    private String componentBackendTagId;

}
