package scatter.scatterdesign.pojo.component.frontend.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
/**
 * <p>
 * 前端组件分配前端组件标签表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@PropValid
@Setter
@Getter
@ApiModel(value="前端组件分配前端组件标签表单对象")
public class ComponentFrontendAssignComponentFrontendTagForm extends BaseForm {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "前端组件id不能为空")
    @ApiModelProperty(value = "前端组件id")
    private String componentFrontendId;

    @NotEmpty(message = "前端组件标签id不能为空")
    @ApiModelProperty(value = "前端组件标签id")
    private String componentFrontendTagId;
    @ApiModelProperty(value = "选择的前端组件标签id")
    private List<String> checkedComponentFrontendTagIds;

    @PropValid.DependCondition(message = "未选择的前端组件标签id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的前端组件标签id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedComponentFrontendTagIds;
    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
