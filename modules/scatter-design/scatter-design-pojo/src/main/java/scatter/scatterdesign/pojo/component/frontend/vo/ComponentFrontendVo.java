package scatter.scatterdesign.pojo.component.frontend.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 前端组件响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Setter
@Getter
@ApiModel(value="前端组件响应对象")
public class ComponentFrontendVo extends BaseIdVo {

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "开发语言，字典")
    private String developLangDictId;

    @ApiModelProperty(value = "是否必须")
    private Boolean isMust;

    @ApiModelProperty(value = "介绍")
    private String description;

    @ApiModelProperty(value = "相较上一次发布，是否新增")
    private Boolean isNew;

    @ApiModelProperty(value = "相较上一次发布，是否有更新")
    private Boolean isUpdate;

    @ApiModelProperty(value = "更新描述")
    private String updateDescription;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime publishAt;

    @ApiModelProperty(value = "发布用户id")
    private String publishUserId;

    @ApiModelProperty(value = "描述,注意事项等")
    private String remark;

}
