package scatter.scatterdesign.rest.component.test.component.backend.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagPageQueryForm;
import scatter.scatterdesign.rest.component.test.component.backend.ComponentBackendTagSuperTest;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 后端组件标签 服务测试类
* </p>
*
* @author yw
* @since 2021-02-22
*/
@SpringBootTest
public class ComponentBackendTagServiceTest extends ComponentBackendTagSuperTest{

    @Autowired
    private IComponentBackendTagService componentBackendTagService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ComponentBackendTag> pos = componentBackendTagService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}