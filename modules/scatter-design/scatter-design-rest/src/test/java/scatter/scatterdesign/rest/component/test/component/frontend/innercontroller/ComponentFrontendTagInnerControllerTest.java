package scatter.scatterdesign.rest.component.test.component.frontend.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scatterdesign.rest.component.test.component.frontend.ComponentFrontendTagSuperTest;
/**
* <p>
* 前端组件标签 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-02-22
*/
@SpringBootTest
public class ComponentFrontendTagInnerControllerTest extends ComponentFrontendTagSuperTest{
    @Test
    void contextLoads() {
    }
}