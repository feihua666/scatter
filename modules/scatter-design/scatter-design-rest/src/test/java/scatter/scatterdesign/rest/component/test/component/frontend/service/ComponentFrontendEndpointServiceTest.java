package scatter.scatterdesign.rest.component.test.component.frontend.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpoint;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointPageQueryForm;
import scatter.scatterdesign.rest.component.test.component.frontend.ComponentFrontendEndpointSuperTest;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendEndpointService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 前端组件端点 服务测试类
* </p>
*
* @author yw
* @since 2021-02-22
*/
@SpringBootTest
public class ComponentFrontendEndpointServiceTest extends ComponentFrontendEndpointSuperTest{

    @Autowired
    private IComponentFrontendEndpointService componentFrontendEndpointService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ComponentFrontendEndpoint> pos = componentFrontendEndpointService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}