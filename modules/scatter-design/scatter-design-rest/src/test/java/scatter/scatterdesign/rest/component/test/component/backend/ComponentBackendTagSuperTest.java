package scatter.scatterdesign.rest.component.test.component.backend;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagPageQueryForm;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 后端组件标签 测试类基类
* </p>
*
* @author yw
* @since 2021-02-22
*/
public class ComponentBackendTagSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IComponentBackendTagService componentBackendTagService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return componentBackendTagService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return componentBackendTagService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ComponentBackendTag mockPo() {
        return JMockData.mock(ComponentBackendTag.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ComponentBackendTagAddForm mockAddForm() {
        return JMockData.mock(ComponentBackendTagAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ComponentBackendTagUpdateForm mockUpdateForm() {
        return JMockData.mock(ComponentBackendTagUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ComponentBackendTagPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ComponentBackendTagPageQueryForm.class, mockConfig);
    }
}