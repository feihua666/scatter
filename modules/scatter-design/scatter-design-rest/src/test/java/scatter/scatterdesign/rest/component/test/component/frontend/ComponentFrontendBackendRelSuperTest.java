package scatter.scatterdesign.rest.component.test.component.frontend;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendBackendRel;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelPageQueryForm;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendBackendRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 前端组件和后端组件关系 测试类基类
* </p>
*
* @author yw
* @since 2021-02-22
*/
public class ComponentFrontendBackendRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IComponentFrontendBackendRelService componentFrontendBackendRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return componentFrontendBackendRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return componentFrontendBackendRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ComponentFrontendBackendRel mockPo() {
        return JMockData.mock(ComponentFrontendBackendRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ComponentFrontendBackendRelAddForm mockAddForm() {
        return JMockData.mock(ComponentFrontendBackendRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ComponentFrontendBackendRelUpdateForm mockUpdateForm() {
        return JMockData.mock(ComponentFrontendBackendRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ComponentFrontendBackendRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ComponentFrontendBackendRelPageQueryForm.class, mockConfig);
    }
}