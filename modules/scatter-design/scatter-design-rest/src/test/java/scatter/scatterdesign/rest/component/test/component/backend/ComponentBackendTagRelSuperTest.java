package scatter.scatterdesign.rest.component.test.component.backend;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTagRel;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelPageQueryForm;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 后端组件标签关系 测试类基类
* </p>
*
* @author yw
* @since 2021-02-22
*/
public class ComponentBackendTagRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IComponentBackendTagRelService componentBackendTagRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return componentBackendTagRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return componentBackendTagRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ComponentBackendTagRel mockPo() {
        return JMockData.mock(ComponentBackendTagRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ComponentBackendTagRelAddForm mockAddForm() {
        return JMockData.mock(ComponentBackendTagRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ComponentBackendTagRelUpdateForm mockUpdateForm() {
        return JMockData.mock(ComponentBackendTagRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ComponentBackendTagRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ComponentBackendTagRelPageQueryForm.class, mockConfig);
    }
}