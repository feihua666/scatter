package scatter.scatterdesign.rest.component.test.component.frontend;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpointRel;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelPageQueryForm;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendEndpointRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 前端组件闪端点关系 测试类基类
* </p>
*
* @author yw
* @since 2021-02-22
*/
public class ComponentFrontendEndpointRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IComponentFrontendEndpointRelService componentFrontendEndpointRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return componentFrontendEndpointRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return componentFrontendEndpointRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ComponentFrontendEndpointRel mockPo() {
        return JMockData.mock(ComponentFrontendEndpointRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ComponentFrontendEndpointRelAddForm mockAddForm() {
        return JMockData.mock(ComponentFrontendEndpointRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ComponentFrontendEndpointRelUpdateForm mockUpdateForm() {
        return JMockData.mock(ComponentFrontendEndpointRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ComponentFrontendEndpointRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ComponentFrontendEndpointRelPageQueryForm.class, mockConfig);
    }
}