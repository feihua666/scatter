package scatter.scatterdesign.rest.component.test.component.frontend.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendBackendRel;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelPageQueryForm;
import scatter.scatterdesign.rest.component.test.component.frontend.ComponentFrontendBackendRelSuperTest;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendBackendRelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 前端组件和后端组件关系 服务测试类
* </p>
*
* @author yw
* @since 2021-02-22
*/
@SpringBootTest
public class ComponentFrontendBackendRelServiceTest extends ComponentFrontendBackendRelSuperTest{

    @Autowired
    private IComponentFrontendBackendRelService componentFrontendBackendRelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ComponentFrontendBackendRel> pos = componentFrontendBackendRelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}