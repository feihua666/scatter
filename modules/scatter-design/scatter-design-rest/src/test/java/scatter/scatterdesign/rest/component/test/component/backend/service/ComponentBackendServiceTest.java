package scatter.scatterdesign.rest.component.test.component.backend.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendPageQueryForm;
import scatter.scatterdesign.rest.component.test.component.backend.ComponentBackendSuperTest;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 后端组件 服务测试类
* </p>
*
* @author yw
* @since 2021-02-22
*/
@SpringBootTest
public class ComponentBackendServiceTest extends ComponentBackendSuperTest{

    @Autowired
    private IComponentBackendService componentBackendService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ComponentBackend> pos = componentBackendService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}