package scatter.scatterdesign.rest.component.test.component.frontend.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.scatterdesign.rest.component.test.component.frontend.ComponentFrontendEndpointRelSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendEndpointRelVo;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
/**
* <p>
* 前端组件闪端点关系 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-02-22
*/
@SpringBootTest
@AutoConfigureMockMvc
public class ComponentFrontendEndpointRelControllerTest extends ComponentFrontendEndpointRelSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend-endpoint-rel";

        // 请求表单
        ComponentFrontendEndpointRelAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        ComponentFrontendEndpointRelVo vo = JSONUtil.toBean(response.getContentAsString(), ComponentFrontendEndpointRelVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}