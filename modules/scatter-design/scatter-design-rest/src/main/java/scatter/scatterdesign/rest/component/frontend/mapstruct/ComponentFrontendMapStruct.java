package scatter.scatterdesign.rest.component.frontend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontend;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendPageQueryForm;

/**
 * <p>
 * 前端组件 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentFrontendMapStruct extends IBaseVoMapStruct<ComponentFrontend, ComponentFrontendVo>,
                                  IBaseAddFormMapStruct<ComponentFrontend,ComponentFrontendAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentFrontend,ComponentFrontendUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentFrontend,ComponentFrontendPageQueryForm>{
    ComponentFrontendMapStruct INSTANCE = Mappers.getMapper( ComponentFrontendMapStruct.class );

}
