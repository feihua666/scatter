package scatter.scatterdesign.rest.component.backend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.scatterdesign.pojo.component.backend.vo.ComponentBackendVo;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 后端组件表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "后端组件相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.backend/component-backend")
public class ComponentBackendController extends BaseAddUpdateQueryFormController<ComponentBackend, ComponentBackendVo, ComponentBackendAddForm, ComponentBackendUpdateForm, ComponentBackendPageQueryForm> {


     @Override
	 @ApiOperation("添加后端组件")
     @PreAuthorize("hasAuthority('ComponentBackend:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentBackendVo add(@RequestBody @Valid ComponentBackendAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询后端组件")
     @PreAuthorize("hasAuthority('ComponentBackend:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentBackendVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除后端组件")
     @PreAuthorize("hasAuthority('ComponentBackend:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新后端组件")
     @PreAuthorize("hasAuthority('ComponentBackend:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentBackendVo update(@RequestBody @Valid ComponentBackendUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询后端组件")
    @PreAuthorize("hasAuthority('ComponentBackend:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentBackendVo> getList(ComponentBackendPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询后端组件")
    @PreAuthorize("hasAuthority('ComponentBackend:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentBackendVo> getPage(ComponentBackendPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
