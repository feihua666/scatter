package scatter.scatterdesign.rest.component.frontend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTagRel;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendTagRelVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 前端组件和前端组件标签关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "前端组件和前端组件标签相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend-tag-rel")
public class ComponentFrontendTagRelController extends BaseAddUpdateQueryFormController<ComponentFrontendTagRel, ComponentFrontendTagRelVo, ComponentFrontendTagRelAddForm, ComponentFrontendTagRelUpdateForm, ComponentFrontendTagRelPageQueryForm> {


     @Override
	 @ApiOperation("添加前端组件和前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTagRel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendTagRelVo add(@RequestBody @Valid ComponentFrontendTagRelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询前端组件和前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTagRel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentFrontendTagRelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除前端组件和前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTagRel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新前端组件和前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTagRel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendTagRelVo update(@RequestBody @Valid ComponentFrontendTagRelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询前端组件和前端组件标签")
    @PreAuthorize("hasAuthority('ComponentFrontendTagRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentFrontendTagRelVo> getList(ComponentFrontendTagRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询前端组件和前端组件标签")
    @PreAuthorize("hasAuthority('ComponentFrontendTagRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentFrontendTagRelVo> getPage(ComponentFrontendTagRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

}
