package scatter.scatterdesign.rest.component.frontend.mapper;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpoint;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 前端组件端点表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface ComponentFrontendEndpointMapper extends IBaseMapper<ComponentFrontendEndpoint> {

}
