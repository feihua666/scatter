package scatter.scatterdesign.rest.component.backend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagPageQueryForm;
import java.util.List;
/**
 * <p>
 * 后端组件标签表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentBackendTagService extends IBaseService<ComponentBackendTag> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ComponentBackendTag getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ComponentBackendTag>lambdaQuery().eq(ComponentBackendTag::getName, name));
    }

}
