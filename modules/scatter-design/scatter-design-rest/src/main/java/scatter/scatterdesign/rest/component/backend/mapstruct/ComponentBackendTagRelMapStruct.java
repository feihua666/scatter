package scatter.scatterdesign.rest.component.backend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTagRel;
import scatter.scatterdesign.pojo.component.backend.vo.ComponentBackendTagRelVo;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelPageQueryForm;

/**
 * <p>
 * 后端组件标签关系 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentBackendTagRelMapStruct extends IBaseVoMapStruct<ComponentBackendTagRel, ComponentBackendTagRelVo>,
                                  IBaseAddFormMapStruct<ComponentBackendTagRel,ComponentBackendTagRelAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentBackendTagRel,ComponentBackendTagRelUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentBackendTagRel,ComponentBackendTagRelPageQueryForm>{
    ComponentBackendTagRelMapStruct INSTANCE = Mappers.getMapper( ComponentBackendTagRelMapStruct.class );

}
