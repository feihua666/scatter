package scatter.scatterdesign.rest.component.frontend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTagRel;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 前端组件和前端组件标签关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentFrontendTagRelService extends IBaseService<ComponentFrontendTagRel> {

    /**
     * 根据前端组件id查询
     * @param componentFrontendId
     * @return
     */
    default List<ComponentFrontendTagRel> getByComponentFrontendId(String componentFrontendId) {
        Assert.hasText(componentFrontendId,"componentFrontendId不能为空");
        return list(Wrappers.<ComponentFrontendTagRel>lambdaQuery().eq(ComponentFrontendTagRel::getComponentFrontendId, componentFrontendId));
    }
    /**
     * 根据前端组件标签id查询
     * @param componentFrontendTagId
     * @return
     */
    default List<ComponentFrontendTagRel> getByComponentFrontendTagId(String componentFrontendTagId) {
        Assert.hasText(componentFrontendTagId,"componentFrontendTagId不能为空");
        return list(Wrappers.<ComponentFrontendTagRel>lambdaQuery().eq(ComponentFrontendTagRel::getComponentFrontendTagId, componentFrontendTagId));
    }

}
