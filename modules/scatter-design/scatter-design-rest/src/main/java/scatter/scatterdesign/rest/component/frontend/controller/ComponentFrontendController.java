package scatter.scatterdesign.rest.component.frontend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontend;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 前端组件表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "前端组件相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend")
public class ComponentFrontendController extends BaseAddUpdateQueryFormController<ComponentFrontend, ComponentFrontendVo, ComponentFrontendAddForm, ComponentFrontendUpdateForm, ComponentFrontendPageQueryForm> {


     @Override
	 @ApiOperation("添加前端组件")
     @PreAuthorize("hasAuthority('ComponentFrontend:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendVo add(@RequestBody @Valid ComponentFrontendAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询前端组件")
     @PreAuthorize("hasAuthority('ComponentFrontend:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentFrontendVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除前端组件")
     @PreAuthorize("hasAuthority('ComponentFrontend:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新前端组件")
     @PreAuthorize("hasAuthority('ComponentFrontend:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendVo update(@RequestBody @Valid ComponentFrontendUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询前端组件")
    @PreAuthorize("hasAuthority('ComponentFrontend:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentFrontendVo> getList(ComponentFrontendPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询前端组件")
    @PreAuthorize("hasAuthority('ComponentFrontend:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentFrontendVo> getPage(ComponentFrontendPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
