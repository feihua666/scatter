package scatter.scatterdesign.rest.component.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 后端组件翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Component
public class ComponentBackendTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IComponentBackendService componentBackendService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ComponentBackend.TRANS_COMPONENTBACKEND_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ComponentBackend.TRANS_COMPONENTBACKEND_BY_ID)) {
            ComponentBackend byId = componentBackendService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ComponentBackend.TRANS_COMPONENTBACKEND_BY_ID)) {
            return componentBackendService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
