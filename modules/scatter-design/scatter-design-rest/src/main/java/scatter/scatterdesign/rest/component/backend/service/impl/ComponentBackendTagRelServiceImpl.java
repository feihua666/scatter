package scatter.scatterdesign.rest.component.backend.service.impl;

import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTagRel;
import scatter.scatterdesign.rest.component.backend.mapper.ComponentBackendTagRelMapper;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 后端组件标签关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentBackendTagRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentBackendTagRelMapper, ComponentBackendTagRel, ComponentBackendTagRelAddForm, ComponentBackendTagRelUpdateForm, ComponentBackendTagRelPageQueryForm> implements IComponentBackendTagRelService {
    @Override
    public void preAdd(ComponentBackendTagRelAddForm addForm,ComponentBackendTagRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ComponentBackendTagRelUpdateForm updateForm,ComponentBackendTagRel po) {
        super.preUpdate(updateForm,po);

    }
}
