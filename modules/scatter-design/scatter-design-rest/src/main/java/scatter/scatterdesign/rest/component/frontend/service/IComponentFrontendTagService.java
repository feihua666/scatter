package scatter.scatterdesign.rest.component.frontend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTag;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagPageQueryForm;
import java.util.List;
/**
 * <p>
 * 前端组件标签表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentFrontendTagService extends IBaseService<ComponentFrontendTag> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ComponentFrontendTag getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ComponentFrontendTag>lambdaQuery().eq(ComponentFrontendTag::getName, name));
    }

}
