package scatter.scatterdesign.rest.component.frontend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpointRel;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendEndpointRelVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 前端组件闪端点关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "前端组件闪端点关系相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend-endpoint-rel")
public class ComponentFrontendEndpointRelController extends BaseAddUpdateQueryFormController<ComponentFrontendEndpointRel, ComponentFrontendEndpointRelVo, ComponentFrontendEndpointRelAddForm, ComponentFrontendEndpointRelUpdateForm, ComponentFrontendEndpointRelPageQueryForm> {


     @Override
	 @ApiOperation("添加前端组件闪端点关系")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpointRel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendEndpointRelVo add(@RequestBody @Valid ComponentFrontendEndpointRelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询前端组件闪端点关系")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpointRel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentFrontendEndpointRelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除前端组件闪端点关系")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpointRel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新前端组件闪端点关系")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpointRel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendEndpointRelVo update(@RequestBody @Valid ComponentFrontendEndpointRelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询前端组件闪端点关系")
    @PreAuthorize("hasAuthority('ComponentFrontendEndpointRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentFrontendEndpointRelVo> getList(ComponentFrontendEndpointRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询前端组件闪端点关系")
    @PreAuthorize("hasAuthority('ComponentFrontendEndpointRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentFrontendEndpointRelVo> getPage(ComponentFrontendEndpointRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
