package scatter.scatterdesign.rest.component.backend.service.impl;

import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.scatterdesign.rest.component.backend.mapper.ComponentBackendMapper;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 后端组件表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentBackendServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentBackendMapper, ComponentBackend, ComponentBackendAddForm, ComponentBackendUpdateForm, ComponentBackendPageQueryForm> implements IComponentBackendService {
    @Override
    public void preAdd(ComponentBackendAddForm addForm,ComponentBackend po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ComponentBackend::getName,false);
        }

    }

    @Override
    public void preUpdate(ComponentBackendUpdateForm updateForm,ComponentBackend po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ComponentBackend byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ComponentBackend::getName,false);
            }
        }

    }
}
