package scatter.scatterdesign.rest.component.frontend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpoint;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointPageQueryForm;
import java.util.List;
/**
 * <p>
 * 前端组件端点表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentFrontendEndpointService extends IBaseService<ComponentFrontendEndpoint> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ComponentFrontendEndpoint getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ComponentFrontendEndpoint>lambdaQuery().eq(ComponentFrontendEndpoint::getName, name));
    }

}
