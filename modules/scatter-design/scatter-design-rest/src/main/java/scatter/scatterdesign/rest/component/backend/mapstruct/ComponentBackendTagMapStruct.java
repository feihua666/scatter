package scatter.scatterdesign.rest.component.backend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.scatterdesign.pojo.component.backend.vo.ComponentBackendTagVo;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagPageQueryForm;

/**
 * <p>
 * 后端组件标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentBackendTagMapStruct extends IBaseVoMapStruct<ComponentBackendTag, ComponentBackendTagVo>,
                                  IBaseAddFormMapStruct<ComponentBackendTag,ComponentBackendTagAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentBackendTag,ComponentBackendTagUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentBackendTag,ComponentBackendTagPageQueryForm>{
    ComponentBackendTagMapStruct INSTANCE = Mappers.getMapper( ComponentBackendTagMapStruct.class );

}
