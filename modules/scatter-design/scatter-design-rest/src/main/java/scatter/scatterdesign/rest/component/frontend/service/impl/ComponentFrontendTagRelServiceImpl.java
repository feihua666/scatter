package scatter.scatterdesign.rest.component.frontend.service.impl;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTagRel;
import scatter.scatterdesign.rest.component.frontend.mapper.ComponentFrontendTagRelMapper;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendTagRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 前端组件和前端组件标签关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentFrontendTagRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentFrontendTagRelMapper, ComponentFrontendTagRel, ComponentFrontendTagRelAddForm, ComponentFrontendTagRelUpdateForm, ComponentFrontendTagRelPageQueryForm> implements IComponentFrontendTagRelService {
    @Override
    public void preAdd(ComponentFrontendTagRelAddForm addForm,ComponentFrontendTagRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ComponentFrontendTagRelUpdateForm updateForm,ComponentFrontendTagRel po) {
        super.preUpdate(updateForm,po);

    }
}
