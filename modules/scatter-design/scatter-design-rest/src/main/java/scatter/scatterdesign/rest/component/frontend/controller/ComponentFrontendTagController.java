package scatter.scatterdesign.rest.component.frontend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTag;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendTagVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 前端组件标签表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "前端组件标签相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend-tag")
public class ComponentFrontendTagController extends BaseAddUpdateQueryFormController<ComponentFrontendTag, ComponentFrontendTagVo, ComponentFrontendTagAddForm, ComponentFrontendTagUpdateForm, ComponentFrontendTagPageQueryForm> {


     @Override
	 @ApiOperation("添加前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTag:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendTagVo add(@RequestBody @Valid ComponentFrontendTagAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTag:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentFrontendTagVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTag:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新前端组件标签")
     @PreAuthorize("hasAuthority('ComponentFrontendTag:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendTagVo update(@RequestBody @Valid ComponentFrontendTagUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询前端组件标签")
    @PreAuthorize("hasAuthority('ComponentFrontendTag:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentFrontendTagVo> getList(ComponentFrontendTagPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询前端组件标签")
    @PreAuthorize("hasAuthority('ComponentFrontendTag:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentFrontendTagVo> getPage(ComponentFrontendTagPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
