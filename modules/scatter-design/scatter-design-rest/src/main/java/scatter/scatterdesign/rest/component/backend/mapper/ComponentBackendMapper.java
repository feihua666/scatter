package scatter.scatterdesign.rest.component.backend.mapper;

import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 后端组件表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface ComponentBackendMapper extends IBaseMapper<ComponentBackend> {

}
