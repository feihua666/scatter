package scatter.scatterdesign.rest.component.frontend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendBackendRel;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendBackendRelVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelPageQueryForm;

/**
 * <p>
 * 前端组件和后端组件关系 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentFrontendBackendRelMapStruct extends IBaseVoMapStruct<ComponentFrontendBackendRel, ComponentFrontendBackendRelVo>,
                                  IBaseAddFormMapStruct<ComponentFrontendBackendRel,ComponentFrontendBackendRelAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentFrontendBackendRel,ComponentFrontendBackendRelUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentFrontendBackendRel,ComponentFrontendBackendRelPageQueryForm>{
    ComponentFrontendBackendRelMapStruct INSTANCE = Mappers.getMapper( ComponentFrontendBackendRelMapStruct.class );

}
