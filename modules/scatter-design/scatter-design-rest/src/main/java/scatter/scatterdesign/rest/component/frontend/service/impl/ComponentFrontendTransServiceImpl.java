package scatter.scatterdesign.rest.component.frontend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontend;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端组件翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Component
public class ComponentFrontendTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IComponentFrontendService componentFrontendService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ComponentFrontend.TRANS_COMPONENTFRONTEND_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ComponentFrontend.TRANS_COMPONENTFRONTEND_BY_ID)) {
            ComponentFrontend byId = componentFrontendService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ComponentFrontend.TRANS_COMPONENTFRONTEND_BY_ID)) {
            return componentFrontendService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
