package scatter.scatterdesign.rest.component.backend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.scatterdesign.pojo.component.backend.vo.ComponentBackendTagVo;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 后端组件标签表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "后端组件标签相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.backend/component-backend-tag")
public class ComponentBackendTagController extends BaseAddUpdateQueryFormController<ComponentBackendTag, ComponentBackendTagVo, ComponentBackendTagAddForm, ComponentBackendTagUpdateForm, ComponentBackendTagPageQueryForm> {


     @Override
	 @ApiOperation("添加后端组件标签")
     @PreAuthorize("hasAuthority('ComponentBackendTag:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentBackendTagVo add(@RequestBody @Valid ComponentBackendTagAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询后端组件标签")
     @PreAuthorize("hasAuthority('ComponentBackendTag:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentBackendTagVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除后端组件标签")
     @PreAuthorize("hasAuthority('ComponentBackendTag:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新后端组件标签")
     @PreAuthorize("hasAuthority('ComponentBackendTag:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentBackendTagVo update(@RequestBody @Valid ComponentBackendTagUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询后端组件标签")
    @PreAuthorize("hasAuthority('ComponentBackendTag:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentBackendTagVo> getList(ComponentBackendTagPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询后端组件标签")
    @PreAuthorize("hasAuthority('ComponentBackendTag:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentBackendTagVo> getPage(ComponentBackendTagPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
