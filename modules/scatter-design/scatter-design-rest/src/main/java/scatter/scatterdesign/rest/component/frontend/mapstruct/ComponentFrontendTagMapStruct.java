package scatter.scatterdesign.rest.component.frontend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTag;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendTagVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagPageQueryForm;

/**
 * <p>
 * 前端组件标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentFrontendTagMapStruct extends IBaseVoMapStruct<ComponentFrontendTag, ComponentFrontendTagVo>,
                                  IBaseAddFormMapStruct<ComponentFrontendTag,ComponentFrontendTagAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentFrontendTag,ComponentFrontendTagUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentFrontendTag,ComponentFrontendTagPageQueryForm>{
    ComponentFrontendTagMapStruct INSTANCE = Mappers.getMapper( ComponentFrontendTagMapStruct.class );

}
