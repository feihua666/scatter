package scatter.scatterdesign.rest.component.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 后端组件标签翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Component
public class ComponentBackendTagTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IComponentBackendTagService componentBackendTagService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ComponentBackendTag.TRANS_COMPONENTBACKENDTAG_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ComponentBackendTag.TRANS_COMPONENTBACKENDTAG_BY_ID)) {
            ComponentBackendTag byId = componentBackendTagService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ComponentBackendTag.TRANS_COMPONENTBACKENDTAG_BY_ID)) {
            return componentBackendTagService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
