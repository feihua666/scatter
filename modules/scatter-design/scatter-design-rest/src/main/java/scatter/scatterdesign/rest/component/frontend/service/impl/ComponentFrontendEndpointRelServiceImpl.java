package scatter.scatterdesign.rest.component.frontend.service.impl;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpointRel;
import scatter.scatterdesign.rest.component.frontend.mapper.ComponentFrontendEndpointRelMapper;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendEndpointRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 前端组件闪端点关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentFrontendEndpointRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentFrontendEndpointRelMapper, ComponentFrontendEndpointRel, ComponentFrontendEndpointRelAddForm, ComponentFrontendEndpointRelUpdateForm, ComponentFrontendEndpointRelPageQueryForm> implements IComponentFrontendEndpointRelService {
    @Override
    public void preAdd(ComponentFrontendEndpointRelAddForm addForm,ComponentFrontendEndpointRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ComponentFrontendEndpointRelUpdateForm updateForm,ComponentFrontendEndpointRel po) {
        super.preUpdate(updateForm,po);

    }
}
