package scatter.scatterdesign.rest.component.frontend.service.impl;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontend;
import scatter.scatterdesign.rest.component.frontend.mapper.ComponentFrontendMapper;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 前端组件表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentFrontendServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentFrontendMapper, ComponentFrontend, ComponentFrontendAddForm, ComponentFrontendUpdateForm, ComponentFrontendPageQueryForm> implements IComponentFrontendService {
    @Override
    public void preAdd(ComponentFrontendAddForm addForm,ComponentFrontend po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ComponentFrontend::getName,false);
        }

    }

    @Override
    public void preUpdate(ComponentFrontendUpdateForm updateForm,ComponentFrontend po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ComponentFrontend byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ComponentFrontend::getName,false);
            }
        }

    }
}
