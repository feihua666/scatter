package scatter.scatterdesign.rest.component.backend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTagRel;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 后端组件标签关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentBackendTagRelService extends IBaseService<ComponentBackendTagRel> {

    /**
     * 根据后端组件id查询
     * @param componentBackendId
     * @return
     */
    default List<ComponentBackendTagRel> getByComponentBackendId(String componentBackendId) {
        Assert.hasText(componentBackendId,"componentBackendId不能为空");
        return list(Wrappers.<ComponentBackendTagRel>lambdaQuery().eq(ComponentBackendTagRel::getComponentBackendId, componentBackendId));
    }
    /**
     * 根据后端组件标签id查询
     * @param componentBackendTagId
     * @return
     */
    default List<ComponentBackendTagRel> getByComponentBackendTagId(String componentBackendTagId) {
        Assert.hasText(componentBackendTagId,"componentBackendTagId不能为空");
        return list(Wrappers.<ComponentBackendTagRel>lambdaQuery().eq(ComponentBackendTagRel::getComponentBackendTagId, componentBackendTagId));
    }

}
