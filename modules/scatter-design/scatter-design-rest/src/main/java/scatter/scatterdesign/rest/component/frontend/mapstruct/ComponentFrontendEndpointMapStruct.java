package scatter.scatterdesign.rest.component.frontend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpoint;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendEndpointVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointPageQueryForm;

/**
 * <p>
 * 前端组件端点 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentFrontendEndpointMapStruct extends IBaseVoMapStruct<ComponentFrontendEndpoint, ComponentFrontendEndpointVo>,
                                  IBaseAddFormMapStruct<ComponentFrontendEndpoint,ComponentFrontendEndpointAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentFrontendEndpoint,ComponentFrontendEndpointUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentFrontendEndpoint,ComponentFrontendEndpointPageQueryForm>{
    ComponentFrontendEndpointMapStruct INSTANCE = Mappers.getMapper( ComponentFrontendEndpointMapStruct.class );

}
