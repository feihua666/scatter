package scatter.scatterdesign.rest.component.backend.service.impl;

import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTag;
import scatter.scatterdesign.rest.component.backend.mapper.ComponentBackendTagMapper;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 后端组件标签表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentBackendTagServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentBackendTagMapper, ComponentBackendTag, ComponentBackendTagAddForm, ComponentBackendTagUpdateForm, ComponentBackendTagPageQueryForm> implements IComponentBackendTagService {
    @Override
    public void preAdd(ComponentBackendTagAddForm addForm,ComponentBackendTag po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ComponentBackendTag::getName,false);
        }

    }

    @Override
    public void preUpdate(ComponentBackendTagUpdateForm updateForm,ComponentBackendTag po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ComponentBackendTag byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ComponentBackendTag::getName,false);
            }
        }

    }
}
