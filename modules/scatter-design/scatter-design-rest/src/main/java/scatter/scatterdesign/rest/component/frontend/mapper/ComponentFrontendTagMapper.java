package scatter.scatterdesign.rest.component.frontend.mapper;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTag;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 前端组件标签表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface ComponentFrontendTagMapper extends IBaseMapper<ComponentFrontendTag> {

}
