package scatter.scatterdesign.rest.component.frontend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTagRel;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendTagRelVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagRelPageQueryForm;

/**
 * <p>
 * 前端组件和前端组件标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentFrontendTagRelMapStruct extends IBaseVoMapStruct<ComponentFrontendTagRel, ComponentFrontendTagRelVo>,
                                  IBaseAddFormMapStruct<ComponentFrontendTagRel,ComponentFrontendTagRelAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentFrontendTagRel,ComponentFrontendTagRelUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentFrontendTagRel,ComponentFrontendTagRelPageQueryForm>{
    ComponentFrontendTagRelMapStruct INSTANCE = Mappers.getMapper( ComponentFrontendTagRelMapStruct.class );

}
