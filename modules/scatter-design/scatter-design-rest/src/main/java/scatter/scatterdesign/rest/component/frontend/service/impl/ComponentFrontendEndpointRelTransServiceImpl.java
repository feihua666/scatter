package scatter.scatterdesign.rest.component.frontend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpointRel;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendEndpointRelService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端组件闪端点关系翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Component
public class ComponentFrontendEndpointRelTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IComponentFrontendEndpointRelService componentFrontendEndpointRelService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ComponentFrontendEndpointRel.TRANS_COMPONENTFRONTENDENDPOINTREL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ComponentFrontendEndpointRel.TRANS_COMPONENTFRONTENDENDPOINTREL_BY_ID)) {
            ComponentFrontendEndpointRel byId = componentFrontendEndpointRelService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ComponentFrontendEndpointRel.TRANS_COMPONENTFRONTENDENDPOINTREL_BY_ID)) {
            return componentFrontendEndpointRelService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
