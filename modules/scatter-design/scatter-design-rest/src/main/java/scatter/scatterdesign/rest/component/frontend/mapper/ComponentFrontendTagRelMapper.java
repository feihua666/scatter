package scatter.scatterdesign.rest.component.frontend.mapper;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTagRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 前端组件和前端组件标签关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface ComponentFrontendTagRelMapper extends IBaseMapper<ComponentFrontendTagRel> {

}
