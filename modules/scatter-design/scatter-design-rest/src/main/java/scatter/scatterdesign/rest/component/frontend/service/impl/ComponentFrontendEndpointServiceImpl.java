package scatter.scatterdesign.rest.component.frontend.service.impl;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpoint;
import scatter.scatterdesign.rest.component.frontend.mapper.ComponentFrontendEndpointMapper;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendEndpointService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 前端组件端点表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentFrontendEndpointServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentFrontendEndpointMapper, ComponentFrontendEndpoint, ComponentFrontendEndpointAddForm, ComponentFrontendEndpointUpdateForm, ComponentFrontendEndpointPageQueryForm> implements IComponentFrontendEndpointService {
    @Override
    public void preAdd(ComponentFrontendEndpointAddForm addForm,ComponentFrontendEndpoint po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ComponentFrontendEndpoint::getName,false);
        }

    }

    @Override
    public void preUpdate(ComponentFrontendEndpointUpdateForm updateForm,ComponentFrontendEndpoint po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ComponentFrontendEndpoint byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ComponentFrontendEndpoint::getName,false);
            }
        }

    }
}
