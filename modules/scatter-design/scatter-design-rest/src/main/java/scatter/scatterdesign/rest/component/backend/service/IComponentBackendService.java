package scatter.scatterdesign.rest.component.backend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendPageQueryForm;
import java.util.List;
/**
 * <p>
 * 后端组件表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentBackendService extends IBaseService<ComponentBackend> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ComponentBackend getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ComponentBackend>lambdaQuery().eq(ComponentBackend::getName, name));
    }

}
