package scatter.scatterdesign.rest.component.frontend.mapper;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontend;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 前端组件表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface ComponentFrontendMapper extends IBaseMapper<ComponentFrontend> {

}
