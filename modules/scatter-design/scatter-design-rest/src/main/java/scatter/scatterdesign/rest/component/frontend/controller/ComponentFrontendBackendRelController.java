package scatter.scatterdesign.rest.component.frontend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendBackendRel;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendBackendRelVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 前端组件和后端组件关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "前端组件和后端组件关系相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend-backend-rel")
public class ComponentFrontendBackendRelController extends BaseAddUpdateQueryFormController<ComponentFrontendBackendRel, ComponentFrontendBackendRelVo, ComponentFrontendBackendRelAddForm, ComponentFrontendBackendRelUpdateForm, ComponentFrontendBackendRelPageQueryForm> {


     @Override
	 @ApiOperation("添加前端组件和后端组件关系")
     @PreAuthorize("hasAuthority('ComponentFrontendBackendRel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendBackendRelVo add(@RequestBody @Valid ComponentFrontendBackendRelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询前端组件和后端组件关系")
     @PreAuthorize("hasAuthority('ComponentFrontendBackendRel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentFrontendBackendRelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除前端组件和后端组件关系")
     @PreAuthorize("hasAuthority('ComponentFrontendBackendRel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新前端组件和后端组件关系")
     @PreAuthorize("hasAuthority('ComponentFrontendBackendRel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendBackendRelVo update(@RequestBody @Valid ComponentFrontendBackendRelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询前端组件和后端组件关系")
    @PreAuthorize("hasAuthority('ComponentFrontendBackendRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentFrontendBackendRelVo> getList(ComponentFrontendBackendRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询前端组件和后端组件关系")
    @PreAuthorize("hasAuthority('ComponentFrontendBackendRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentFrontendBackendRelVo> getPage(ComponentFrontendBackendRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
