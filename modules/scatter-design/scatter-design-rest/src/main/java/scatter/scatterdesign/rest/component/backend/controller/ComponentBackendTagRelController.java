package scatter.scatterdesign.rest.component.backend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTagRel;
import scatter.scatterdesign.pojo.component.backend.vo.ComponentBackendTagRelVo;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendTagRelPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 后端组件标签关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "后端组件标签关系相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.backend/component-backend-tag-rel")
public class ComponentBackendTagRelController extends BaseAddUpdateQueryFormController<ComponentBackendTagRel, ComponentBackendTagRelVo, ComponentBackendTagRelAddForm, ComponentBackendTagRelUpdateForm, ComponentBackendTagRelPageQueryForm> {


     @Override
	 @ApiOperation("添加后端组件标签关系")
     @PreAuthorize("hasAuthority('ComponentBackendTagRel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentBackendTagRelVo add(@RequestBody @Valid ComponentBackendTagRelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询后端组件标签关系")
     @PreAuthorize("hasAuthority('ComponentBackendTagRel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentBackendTagRelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除后端组件标签关系")
     @PreAuthorize("hasAuthority('ComponentBackendTagRel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新后端组件标签关系")
     @PreAuthorize("hasAuthority('ComponentBackendTagRel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentBackendTagRelVo update(@RequestBody @Valid ComponentBackendTagRelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询后端组件标签关系")
    @PreAuthorize("hasAuthority('ComponentBackendTagRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentBackendTagRelVo> getList(ComponentBackendTagRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询后端组件标签关系")
    @PreAuthorize("hasAuthority('ComponentBackendTagRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentBackendTagRelVo> getPage(ComponentBackendTagRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
