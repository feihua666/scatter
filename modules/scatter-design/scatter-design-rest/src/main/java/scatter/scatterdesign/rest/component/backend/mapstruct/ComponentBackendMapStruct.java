package scatter.scatterdesign.rest.component.backend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackend;
import scatter.scatterdesign.pojo.component.backend.vo.ComponentBackendVo;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendAddForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendUpdateForm;
import scatter.scatterdesign.pojo.component.backend.form.ComponentBackendPageQueryForm;

/**
 * <p>
 * 后端组件 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentBackendMapStruct extends IBaseVoMapStruct<ComponentBackend, ComponentBackendVo>,
                                  IBaseAddFormMapStruct<ComponentBackend,ComponentBackendAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentBackend,ComponentBackendUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentBackend,ComponentBackendPageQueryForm>{
    ComponentBackendMapStruct INSTANCE = Mappers.getMapper( ComponentBackendMapStruct.class );

}
