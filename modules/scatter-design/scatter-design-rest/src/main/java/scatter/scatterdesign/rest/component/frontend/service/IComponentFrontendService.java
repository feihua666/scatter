package scatter.scatterdesign.rest.component.frontend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontend;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendPageQueryForm;
import java.util.List;
/**
 * <p>
 * 前端组件表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentFrontendService extends IBaseService<ComponentFrontend> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default ComponentFrontend getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<ComponentFrontend>lambdaQuery().eq(ComponentFrontend::getName, name));
    }

}
