package scatter.scatterdesign.rest.component.frontend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpointRel;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 前端组件闪端点关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentFrontendEndpointRelService extends IBaseService<ComponentFrontendEndpointRel> {

    /**
     * 根据前端组件id查询
     * @param componentFrontendId
     * @return
     */
    default List<ComponentFrontendEndpointRel> getByComponentFrontendId(String componentFrontendId) {
        Assert.hasText(componentFrontendId,"componentFrontendId不能为空");
        return list(Wrappers.<ComponentFrontendEndpointRel>lambdaQuery().eq(ComponentFrontendEndpointRel::getComponentFrontendId, componentFrontendId));
    }
    /**
     * 根据前端组件端点id查询
     * @param componentFrontendEndpointId
     * @return
     */
    default List<ComponentFrontendEndpointRel> getByComponentFrontendEndpointId(String componentFrontendEndpointId) {
        Assert.hasText(componentFrontendEndpointId,"componentFrontendEndpointId不能为空");
        return list(Wrappers.<ComponentFrontendEndpointRel>lambdaQuery().eq(ComponentFrontendEndpointRel::getComponentFrontendEndpointId, componentFrontendEndpointId));
    }

}
