package scatter.scatterdesign.rest.component.frontend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpoint;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendEndpointVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 前端组件端点表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Api(tags = "前端组件端点相关接口")
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/component.frontend/component-frontend-endpoint")
public class ComponentFrontendEndpointController extends BaseAddUpdateQueryFormController<ComponentFrontendEndpoint, ComponentFrontendEndpointVo, ComponentFrontendEndpointAddForm, ComponentFrontendEndpointUpdateForm, ComponentFrontendEndpointPageQueryForm> {


     @Override
	 @ApiOperation("添加前端组件端点")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpoint:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendEndpointVo add(@RequestBody @Valid ComponentFrontendEndpointAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询前端组件端点")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpoint:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ComponentFrontendEndpointVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除前端组件端点")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpoint:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新前端组件端点")
     @PreAuthorize("hasAuthority('ComponentFrontendEndpoint:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ComponentFrontendEndpointVo update(@RequestBody @Valid ComponentFrontendEndpointUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询前端组件端点")
    @PreAuthorize("hasAuthority('ComponentFrontendEndpoint:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ComponentFrontendEndpointVo> getList(ComponentFrontendEndpointPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询前端组件端点")
    @PreAuthorize("hasAuthority('ComponentFrontendEndpoint:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComponentFrontendEndpointVo> getPage(ComponentFrontendEndpointPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
