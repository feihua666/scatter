package scatter.scatterdesign.rest.component.frontend.service.impl;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTag;
import scatter.scatterdesign.rest.component.frontend.mapper.ComponentFrontendTagMapper;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendTagService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendTagPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 前端组件标签表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentFrontendTagServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentFrontendTagMapper, ComponentFrontendTag, ComponentFrontendTagAddForm, ComponentFrontendTagUpdateForm, ComponentFrontendTagPageQueryForm> implements IComponentFrontendTagService {
    @Override
    public void preAdd(ComponentFrontendTagAddForm addForm,ComponentFrontendTag po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),ComponentFrontendTag::getName,false);
        }

    }

    @Override
    public void preUpdate(ComponentFrontendTagUpdateForm updateForm,ComponentFrontendTag po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            ComponentFrontendTag byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),ComponentFrontendTag::getName,false);
            }
        }

    }
}
