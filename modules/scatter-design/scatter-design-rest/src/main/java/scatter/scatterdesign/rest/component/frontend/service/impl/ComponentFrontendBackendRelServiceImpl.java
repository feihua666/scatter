package scatter.scatterdesign.rest.component.frontend.service.impl;

import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendBackendRel;
import scatter.scatterdesign.rest.component.frontend.mapper.ComponentFrontendBackendRelMapper;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendBackendRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 前端组件和后端组件关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Service
@Transactional
public class ComponentFrontendBackendRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComponentFrontendBackendRelMapper, ComponentFrontendBackendRel, ComponentFrontendBackendRelAddForm, ComponentFrontendBackendRelUpdateForm, ComponentFrontendBackendRelPageQueryForm> implements IComponentFrontendBackendRelService {
    @Override
    public void preAdd(ComponentFrontendBackendRelAddForm addForm,ComponentFrontendBackendRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ComponentFrontendBackendRelUpdateForm updateForm,ComponentFrontendBackendRel po) {
        super.preUpdate(updateForm,po);

    }
}
