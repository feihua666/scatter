package scatter.scatterdesign.rest.component.frontend.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendBackendRel;
import scatter.common.rest.service.IBaseService;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendBackendRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 前端组件和后端组件关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
public interface IComponentFrontendBackendRelService extends IBaseService<ComponentFrontendBackendRel> {

    /**
     * 根据前端组件id查询
     * @param componentFrontendId
     * @return
     */
    default List<ComponentFrontendBackendRel> getByComponentFrontendId(String componentFrontendId) {
        Assert.hasText(componentFrontendId,"componentFrontendId不能为空");
        return list(Wrappers.<ComponentFrontendBackendRel>lambdaQuery().eq(ComponentFrontendBackendRel::getComponentFrontendId, componentFrontendId));
    }
    /**
     * 根据后端组件id查询
     * @param componentBackendId
     * @return
     */
    default List<ComponentFrontendBackendRel> getByComponentBackendId(String componentBackendId) {
        Assert.hasText(componentBackendId,"componentBackendId不能为空");
        return list(Wrappers.<ComponentFrontendBackendRel>lambdaQuery().eq(ComponentFrontendBackendRel::getComponentBackendId, componentBackendId));
    }

}
