package scatter.scatterdesign.rest.component.backend.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import scatter.scatterdesign.pojo.component.backend.po.ComponentBackendTagRel;
import scatter.scatterdesign.rest.component.backend.service.IComponentBackendTagRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 后端组件标签关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/inner/component.backend/component-backend-tag-rel")
public class ComponentBackendTagRelInnerController extends BaseInnerController<ComponentBackendTagRel> {
 @Autowired
 private IComponentBackendTagRelService componentBackendTagRelService;

 public IComponentBackendTagRelService getService(){
     return componentBackendTagRelService;
 }
}
