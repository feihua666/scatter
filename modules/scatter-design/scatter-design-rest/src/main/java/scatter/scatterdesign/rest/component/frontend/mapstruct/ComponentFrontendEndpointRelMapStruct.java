package scatter.scatterdesign.rest.component.frontend.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendEndpointRel;
import scatter.scatterdesign.pojo.component.frontend.vo.ComponentFrontendEndpointRelVo;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelAddForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelUpdateForm;
import scatter.scatterdesign.pojo.component.frontend.form.ComponentFrontendEndpointRelPageQueryForm;

/**
 * <p>
 * 前端组件闪端点关系 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComponentFrontendEndpointRelMapStruct extends IBaseVoMapStruct<ComponentFrontendEndpointRel, ComponentFrontendEndpointRelVo>,
                                  IBaseAddFormMapStruct<ComponentFrontendEndpointRel,ComponentFrontendEndpointRelAddForm>,
                                  IBaseUpdateFormMapStruct<ComponentFrontendEndpointRel,ComponentFrontendEndpointRelUpdateForm>,
                                  IBaseQueryFormMapStruct<ComponentFrontendEndpointRel,ComponentFrontendEndpointRelPageQueryForm>{
    ComponentFrontendEndpointRelMapStruct INSTANCE = Mappers.getMapper( ComponentFrontendEndpointRelMapStruct.class );

}
