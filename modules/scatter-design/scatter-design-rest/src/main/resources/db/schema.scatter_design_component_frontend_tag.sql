DROP TABLE IF EXISTS scatter_design_component_frontend_tag;
CREATE TABLE `scatter_design_component_frontend_tag` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件标签表';
