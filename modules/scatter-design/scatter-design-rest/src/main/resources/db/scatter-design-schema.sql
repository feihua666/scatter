DROP TABLE IF EXISTS scatter_design_component_frontend_tag_rel;
CREATE TABLE `scatter_design_component_frontend_tag_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_frontend_id` varchar(20) NOT NULL COMMENT '前端组件id，外键',
  `component_frontend_tag_id` varchar(20) NOT NULL COMMENT '前端组件标签id，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_frontend_id`) USING BTREE,
  KEY `component_java_tag_id` (`component_frontend_tag_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件标签关系表';
DROP TABLE IF EXISTS scatter_design_component_frontend_tag;
CREATE TABLE `scatter_design_component_frontend_tag` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件标签表';
DROP TABLE IF EXISTS scatter_design_component_frontend_endpoint_rel;
CREATE TABLE `scatter_design_component_frontend_endpoint_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_frontend_id` varchar(20) NOT NULL COMMENT '前端组件id，外键',
  `component_frontend_endpoint_id` varchar(20) NOT NULL COMMENT '前端组件端点id，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_frontend_id`) USING BTREE,
  KEY `component_java_tag_id` (`component_frontend_endpoint_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件闪端点关系表';
DROP TABLE IF EXISTS scatter_design_component_frontend_endpoint;
CREATE TABLE `scatter_design_component_frontend_endpoint` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件端点表';
DROP TABLE IF EXISTS scatter_design_component_frontend_backend_rel;
CREATE TABLE `scatter_design_component_frontend_backend_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_frontend_id` varchar(20) NOT NULL COMMENT '前端组件id，外键',
  `component_backend_id` varchar(20) NOT NULL COMMENT '后端组件id，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_frontend_id`) USING BTREE,
  KEY `component_java_tag_id` (`component_backend_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件和后端组件关系表';
DROP TABLE IF EXISTS scatter_design_component_frontend;
CREATE TABLE `scatter_design_component_frontend` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `develop_lang_dict_id` varchar(20) NOT NULL COMMENT '开发语言，字典',
  `is_must` tinyint(1) NOT NULL COMMENT '是否必须',
  `description` varchar(1000) NOT NULL COMMENT '介绍',
  `is_new` tinyint(1) NOT NULL COMMENT '相较上一次发布，是否新增',
  `is_update` tinyint(1) NOT NULL COMMENT '相较上一次发布，是否有更新',
  `update_description` varchar(1000) DEFAULT NULL COMMENT '更新描述',
  `publish_at` datetime DEFAULT NULL COMMENT '发布时间',
  `publish_user_id` varchar(20) NOT NULL COMMENT '发布用户id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件表';
DROP TABLE IF EXISTS scatter_design_component_frontend_tag_rel;
CREATE TABLE `scatter_design_component_frontend_tag_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_frontend_id` varchar(20) NOT NULL COMMENT '前端组件id，外键',
  `component_frontend_tag_id` varchar(20) NOT NULL COMMENT '前端组件标签id，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_frontend_id`) USING BTREE,
  KEY `component_java_tag_id` (`component_frontend_tag_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件和前端组件标签关系表';
DROP TABLE IF EXISTS scatter_design_component_backend_backend_rel;
CREATE TABLE `scatter_design_component_backend_backend_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_backend_id` varchar(20) NOT NULL COMMENT '后端组件id，外键',
  `depand_component_backend_id` varchar(20) NOT NULL COMMENT '依赖的后端组件id，后端组件依赖，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_backend_id`) USING BTREE,
  KEY `component_java_tag_id` (`depand_component_backend_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='后端组件和后端组件依赖关系表';
