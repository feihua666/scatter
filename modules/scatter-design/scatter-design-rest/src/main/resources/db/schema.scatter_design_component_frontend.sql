DROP TABLE IF EXISTS scatter_design_component_frontend;
CREATE TABLE `scatter_design_component_frontend` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `develop_lang_dict_id` varchar(20) NOT NULL COMMENT '开发语言，字典',
  `is_must` tinyint(1) NOT NULL COMMENT '是否必须',
  `description` varchar(1000) NOT NULL COMMENT '介绍',
  `is_new` tinyint(1) NOT NULL COMMENT '相较上一次发布，是否新增',
  `is_update` tinyint(1) NOT NULL COMMENT '相较上一次发布，是否有更新',
  `update_description` varchar(1000) DEFAULT NULL COMMENT '更新描述',
  `publish_at` datetime DEFAULT NULL COMMENT '发布时间',
  `publish_user_id` varchar(20) NOT NULL COMMENT '发布用户id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述,注意事项等',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='前端组件表';
