DROP TABLE IF EXISTS scatter_design_component_backend_tag_rel;
CREATE TABLE `scatter_design_component_backend_tag_rel` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `component_backend_id` varchar(20) NOT NULL COMMENT '后端组件id，外键',
  `component_backend_tag_id` varchar(20) NOT NULL COMMENT '后端组件标签id，外键',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `component_java_id` (`component_backend_id`) USING BTREE,
  KEY `component_java_tag_id` (`component_backend_tag_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='后端组件标签关系表';
