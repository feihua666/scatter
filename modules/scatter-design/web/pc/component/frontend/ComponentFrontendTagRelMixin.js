import ComponentFrontendTagRelForm from './ComponentFrontendTagRelForm.js'
import ComponentFrontendTagRelTable from './ComponentFrontendTagRelTable.js'
import ComponentFrontendTagRelUrl from './ComponentFrontendTagRelUrl.js'
const ComponentFrontendTagRelMixin = {
    computed: {
        ComponentFrontendTagRelFormOptions() {
            return this.$stDynamicFormTools.options(ComponentFrontendTagRelForm,this.$options.name)
        },
        ComponentFrontendTagRelTableOptions() {
            return ComponentFrontendTagRelTable
        },
        ComponentFrontendTagRelUrl(){
            return ComponentFrontendTagRelUrl
        }
    },
}
export default ComponentFrontendTagRelMixin