import ComponentFrontendTagUrl from './ComponentFrontendTagUrl.js'
const ComponentFrontendTagRoute = [
    {
        path: ComponentFrontendTagUrl.router.searchList,
        component: () => import('./element/ComponentFrontendTagSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentFrontendTagSearchList',
            name: '前端组件标签管理'
        }
    },
    {
        path: ComponentFrontendTagUrl.router.add,
        component: () => import('./element/ComponentFrontendTagAdd'),
        meta: {
            code:'ComponentFrontendTagAdd',
            name: '前端组件标签添加'
        }
    },
    {
        path: ComponentFrontendTagUrl.router.update,
        component: () => import('./element/ComponentFrontendTagUpdate'),
        meta: {
            code:'ComponentFrontendTagUpdate',
            name: '前端组件标签修改'
        }
    },
]
export default ComponentFrontendTagRoute