import ComponentFrontendTagForm from './ComponentFrontendTagForm.js'
import ComponentFrontendTagTable from './ComponentFrontendTagTable.js'
import ComponentFrontendTagUrl from './ComponentFrontendTagUrl.js'
const ComponentFrontendTagMixin = {
    computed: {
        ComponentFrontendTagFormOptions() {
            return this.$stDynamicFormTools.options(ComponentFrontendTagForm,this.$options.name)
        },
        ComponentFrontendTagTableOptions() {
            return ComponentFrontendTagTable
        },
        ComponentFrontendTagUrl(){
            return ComponentFrontendTagUrl
        }
    },
}
export default ComponentFrontendTagMixin