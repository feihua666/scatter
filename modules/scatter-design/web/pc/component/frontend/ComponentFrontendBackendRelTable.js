const ComponentFrontendBackendRelTable = [
    {
        prop: 'componentFrontendId',
        label: '前端组件id'
    },
    {
        prop: 'componentBackendId',
        label: '后端组件id'
    },
]
export default ComponentFrontendBackendRelTable