import ComponentFrontendUrl from './ComponentFrontendUrl.js'
const ComponentFrontendForm = [
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'developLangDictId',
        },
        element:{
            label: '开发语言',
            required: true,
        }
    },
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isMust',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否必须',
            required: true,
        }
    },
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'description',
        },
        element:{
            label: '介绍',
            required: true,
        }
    },
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isNew',
            value: false,
        },
        element:{
            type: 'switch',
            label: '相较上一次发布',
            required: true,
        }
    },
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isUpdate',
            value: false,
        },
        element:{
            type: 'switch',
            label: '相较上一次发布',
            required: true,
        }
    },
    {
        field: {
            name: 'updateDescription',
        },
        element:{
            label: '更新描述',
        }
    },
    {
        field: {
            name: 'publishAt',
        },
        element:{
            label: '发布时间',
        }
    },
    {
        ComponentFrontendSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'publishUserId',
        },
        element:{
            label: '发布用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
]
export default ComponentFrontendForm