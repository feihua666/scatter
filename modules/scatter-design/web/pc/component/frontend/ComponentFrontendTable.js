const ComponentFrontendTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'developLangDictId',
        label: '开发语言'
    },
    {
        prop: 'isMust',
        label: '是否必须'
    },
    {
        prop: 'description',
        label: '介绍'
    },
    {
        prop: 'isNew',
        label: '相较上一次发布'
    },
    {
        prop: 'isUpdate',
        label: '相较上一次发布'
    },
    {
        prop: 'updateDescription',
        label: '更新描述'
    },
    {
        prop: 'publishAt',
        label: '发布时间'
    },
    {
        prop: 'publishUserId',
        label: '发布用户id'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ComponentFrontendTable