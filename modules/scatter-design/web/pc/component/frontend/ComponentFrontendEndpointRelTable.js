const ComponentFrontendEndpointRelTable = [
    {
        prop: 'componentFrontendId',
        label: '前端组件id'
    },
    {
        prop: 'componentFrontendEndpointId',
        label: '前端组件端点id'
    },
]
export default ComponentFrontendEndpointRelTable