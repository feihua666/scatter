const basePath = '' + '/component.frontend/component-frontend'
const ComponentFrontendUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentFrontendSearchList',
        add: '/ComponentFrontendAdd',
        update: '/ComponentFrontendUpdate',
    }
}
export default ComponentFrontendUrl