const ComponentFrontendTagRelTable = [
    {
        prop: 'componentFrontendId',
        label: '前端组件id'
    },
    {
        prop: 'componentFrontendTagId',
        label: '前端组件标签id'
    },
]
export default ComponentFrontendTagRelTable