import ComponentFrontendTagRelUrl from './ComponentFrontendTagRelUrl.js'
const ComponentFrontendTagRelRoute = [
    {
        path: ComponentFrontendTagRelUrl.router.searchList,
        component: () => import('./element/ComponentFrontendTagRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentFrontendTagRelSearchList',
            name: '前端组件和前端组件标签管理'
        }
    },
    {
        path: ComponentFrontendTagRelUrl.router.add,
        component: () => import('./element/ComponentFrontendTagRelAdd'),
        meta: {
            code:'ComponentFrontendTagRelAdd',
            name: '前端组件和前端组件标签添加'
        }
    },
    {
        path: ComponentFrontendTagRelUrl.router.update,
        component: () => import('./element/ComponentFrontendTagRelUpdate'),
        meta: {
            code:'ComponentFrontendTagRelUpdate',
            name: '前端组件和前端组件标签修改'
        }
    },
]
export default ComponentFrontendTagRelRoute