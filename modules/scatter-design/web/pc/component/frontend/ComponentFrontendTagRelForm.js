import ComponentFrontendTagRelUrl from './ComponentFrontendTagRelUrl.js'
const ComponentFrontendTagRelForm = [
    {
        ComponentFrontendTagRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentFrontendId',
        },
        element:{
            label: '前端组件id',
            required: true,
        }
    },
    {
        ComponentFrontendTagRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentFrontendTagId',
        },
        element:{
            label: '前端组件标签id',
            required: true,
        }
    },
]
export default ComponentFrontendTagRelForm