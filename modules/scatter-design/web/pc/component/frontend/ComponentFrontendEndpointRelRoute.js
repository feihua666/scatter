import ComponentFrontendEndpointRelUrl from './ComponentFrontendEndpointRelUrl.js'
const ComponentFrontendEndpointRelRoute = [
    {
        path: ComponentFrontendEndpointRelUrl.router.searchList,
        component: () => import('./element/ComponentFrontendEndpointRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentFrontendEndpointRelSearchList',
            name: '前端组件闪端点关系管理'
        }
    },
    {
        path: ComponentFrontendEndpointRelUrl.router.add,
        component: () => import('./element/ComponentFrontendEndpointRelAdd'),
        meta: {
            code:'ComponentFrontendEndpointRelAdd',
            name: '前端组件闪端点关系添加'
        }
    },
    {
        path: ComponentFrontendEndpointRelUrl.router.update,
        component: () => import('./element/ComponentFrontendEndpointRelUpdate'),
        meta: {
            code:'ComponentFrontendEndpointRelUpdate',
            name: '前端组件闪端点关系修改'
        }
    },
]
export default ComponentFrontendEndpointRelRoute