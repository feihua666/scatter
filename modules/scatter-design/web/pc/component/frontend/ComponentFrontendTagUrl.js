const basePath = '' + '/component.frontend/component-frontend-tag'
const ComponentFrontendTagUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentFrontendTagSearchList',
        add: '/ComponentFrontendTagAdd',
        update: '/ComponentFrontendTagUpdate',
    }
}
export default ComponentFrontendTagUrl