const basePath = '' + '/component.frontend/component-frontend-endpoint'
const ComponentFrontendEndpointUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentFrontendEndpointSearchList',
        add: '/ComponentFrontendEndpointAdd',
        update: '/ComponentFrontendEndpointUpdate',
    }
}
export default ComponentFrontendEndpointUrl