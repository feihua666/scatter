import ComponentFrontendEndpointRelUrl from './ComponentFrontendEndpointRelUrl.js'
const ComponentFrontendEndpointRelForm = [
    {
        ComponentFrontendEndpointRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentFrontendId',
        },
        element:{
            label: '前端组件id',
            required: true,
        }
    },
    {
        ComponentFrontendEndpointRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentFrontendEndpointId',
        },
        element:{
            label: '前端组件端点id',
            required: true,
        }
    },
]
export default ComponentFrontendEndpointRelForm