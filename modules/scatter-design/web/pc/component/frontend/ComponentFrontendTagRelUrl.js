const basePath = '' + '/component.frontend/component-frontend-tag-rel'
const ComponentFrontendTagRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentFrontendTagRelSearchList',
        add: '/ComponentFrontendTagRelAdd',
        update: '/ComponentFrontendTagRelUpdate',
    }
}
export default ComponentFrontendTagRelUrl