import ComponentFrontendBackendRelUrl from './ComponentFrontendBackendRelUrl.js'
const ComponentFrontendBackendRelForm = [
    {
        ComponentFrontendBackendRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentFrontendId',
        },
        element:{
            label: '前端组件id',
            required: true,
        }
    },
    {
        ComponentFrontendBackendRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentBackendId',
        },
        element:{
            label: '后端组件id',
            required: true,
        }
    },
]
export default ComponentFrontendBackendRelForm