import ComponentFrontendEndpointForm from './ComponentFrontendEndpointForm.js'
import ComponentFrontendEndpointTable from './ComponentFrontendEndpointTable.js'
import ComponentFrontendEndpointUrl from './ComponentFrontendEndpointUrl.js'
const ComponentFrontendEndpointMixin = {
    computed: {
        ComponentFrontendEndpointFormOptions() {
            return this.$stDynamicFormTools.options(ComponentFrontendEndpointForm,this.$options.name)
        },
        ComponentFrontendEndpointTableOptions() {
            return ComponentFrontendEndpointTable
        },
        ComponentFrontendEndpointUrl(){
            return ComponentFrontendEndpointUrl
        }
    },
}
export default ComponentFrontendEndpointMixin