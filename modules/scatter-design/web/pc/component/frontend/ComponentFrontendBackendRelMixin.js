import ComponentFrontendBackendRelForm from './ComponentFrontendBackendRelForm.js'
import ComponentFrontendBackendRelTable from './ComponentFrontendBackendRelTable.js'
import ComponentFrontendBackendRelUrl from './ComponentFrontendBackendRelUrl.js'
const ComponentFrontendBackendRelMixin = {
    computed: {
        ComponentFrontendBackendRelFormOptions() {
            return this.$stDynamicFormTools.options(ComponentFrontendBackendRelForm,this.$options.name)
        },
        ComponentFrontendBackendRelTableOptions() {
            return ComponentFrontendBackendRelTable
        },
        ComponentFrontendBackendRelUrl(){
            return ComponentFrontendBackendRelUrl
        }
    },
}
export default ComponentFrontendBackendRelMixin