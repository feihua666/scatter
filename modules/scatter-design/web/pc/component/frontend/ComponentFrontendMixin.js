import ComponentFrontendForm from './ComponentFrontendForm.js'
import ComponentFrontendTable from './ComponentFrontendTable.js'
import ComponentFrontendUrl from './ComponentFrontendUrl.js'
const ComponentFrontendMixin = {
    computed: {
        ComponentFrontendFormOptions() {
            return this.$stDynamicFormTools.options(ComponentFrontendForm,this.$options.name)
        },
        ComponentFrontendTableOptions() {
            return ComponentFrontendTable
        },
        ComponentFrontendUrl(){
            return ComponentFrontendUrl
        }
    },
}
export default ComponentFrontendMixin