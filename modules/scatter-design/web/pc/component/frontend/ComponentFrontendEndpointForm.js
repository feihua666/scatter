import ComponentFrontendEndpointUrl from './ComponentFrontendEndpointUrl.js'
const ComponentFrontendEndpointForm = [
    {
        ComponentFrontendEndpointSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
]
export default ComponentFrontendEndpointForm