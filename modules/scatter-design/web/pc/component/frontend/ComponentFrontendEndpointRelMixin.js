import ComponentFrontendEndpointRelForm from './ComponentFrontendEndpointRelForm.js'
import ComponentFrontendEndpointRelTable from './ComponentFrontendEndpointRelTable.js'
import ComponentFrontendEndpointRelUrl from './ComponentFrontendEndpointRelUrl.js'
const ComponentFrontendEndpointRelMixin = {
    computed: {
        ComponentFrontendEndpointRelFormOptions() {
            return this.$stDynamicFormTools.options(ComponentFrontendEndpointRelForm,this.$options.name)
        },
        ComponentFrontendEndpointRelTableOptions() {
            return ComponentFrontendEndpointRelTable
        },
        ComponentFrontendEndpointRelUrl(){
            return ComponentFrontendEndpointRelUrl
        }
    },
}
export default ComponentFrontendEndpointRelMixin