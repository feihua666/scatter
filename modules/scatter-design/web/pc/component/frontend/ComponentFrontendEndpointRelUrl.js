const basePath = '' + '/component.frontend/component-frontend-endpoint-rel'
const ComponentFrontendEndpointRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentFrontendEndpointRelSearchList',
        add: '/ComponentFrontendEndpointRelAdd',
        update: '/ComponentFrontendEndpointRelUpdate',
    }
}
export default ComponentFrontendEndpointRelUrl