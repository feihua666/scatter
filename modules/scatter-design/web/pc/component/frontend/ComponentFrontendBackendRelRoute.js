import ComponentFrontendBackendRelUrl from './ComponentFrontendBackendRelUrl.js'
const ComponentFrontendBackendRelRoute = [
    {
        path: ComponentFrontendBackendRelUrl.router.searchList,
        component: () => import('./element/ComponentFrontendBackendRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentFrontendBackendRelSearchList',
            name: '前端组件和后端组件关系管理'
        }
    },
    {
        path: ComponentFrontendBackendRelUrl.router.add,
        component: () => import('./element/ComponentFrontendBackendRelAdd'),
        meta: {
            code:'ComponentFrontendBackendRelAdd',
            name: '前端组件和后端组件关系添加'
        }
    },
    {
        path: ComponentFrontendBackendRelUrl.router.update,
        component: () => import('./element/ComponentFrontendBackendRelUpdate'),
        meta: {
            code:'ComponentFrontendBackendRelUpdate',
            name: '前端组件和后端组件关系修改'
        }
    },
]
export default ComponentFrontendBackendRelRoute