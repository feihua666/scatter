const basePath = '' + '/component.frontend/component-frontend-backend-rel'
const ComponentFrontendBackendRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentFrontendBackendRelSearchList',
        add: '/ComponentFrontendBackendRelAdd',
        update: '/ComponentFrontendBackendRelUpdate',
    }
}
export default ComponentFrontendBackendRelUrl