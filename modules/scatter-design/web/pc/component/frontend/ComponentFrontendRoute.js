import ComponentFrontendUrl from './ComponentFrontendUrl.js'
const ComponentFrontendRoute = [
    {
        path: ComponentFrontendUrl.router.searchList,
        component: () => import('./element/ComponentFrontendSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentFrontendSearchList',
            name: '前端组件管理'
        }
    },
    {
        path: ComponentFrontendUrl.router.add,
        component: () => import('./element/ComponentFrontendAdd'),
        meta: {
            code:'ComponentFrontendAdd',
            name: '前端组件添加'
        }
    },
    {
        path: ComponentFrontendUrl.router.update,
        component: () => import('./element/ComponentFrontendUpdate'),
        meta: {
            code:'ComponentFrontendUpdate',
            name: '前端组件修改'
        }
    },
]
export default ComponentFrontendRoute