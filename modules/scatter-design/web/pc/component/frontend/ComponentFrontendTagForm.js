import ComponentFrontendTagUrl from './ComponentFrontendTagUrl.js'
const ComponentFrontendTagForm = [
    {
        ComponentFrontendTagSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
]
export default ComponentFrontendTagForm