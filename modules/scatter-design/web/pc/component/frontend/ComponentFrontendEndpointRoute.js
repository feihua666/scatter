import ComponentFrontendEndpointUrl from './ComponentFrontendEndpointUrl.js'
const ComponentFrontendEndpointRoute = [
    {
        path: ComponentFrontendEndpointUrl.router.searchList,
        component: () => import('./element/ComponentFrontendEndpointSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentFrontendEndpointSearchList',
            name: '前端组件端点管理'
        }
    },
    {
        path: ComponentFrontendEndpointUrl.router.add,
        component: () => import('./element/ComponentFrontendEndpointAdd'),
        meta: {
            code:'ComponentFrontendEndpointAdd',
            name: '前端组件端点添加'
        }
    },
    {
        path: ComponentFrontendEndpointUrl.router.update,
        component: () => import('./element/ComponentFrontendEndpointUpdate'),
        meta: {
            code:'ComponentFrontendEndpointUpdate',
            name: '前端组件端点修改'
        }
    },
]
export default ComponentFrontendEndpointRoute