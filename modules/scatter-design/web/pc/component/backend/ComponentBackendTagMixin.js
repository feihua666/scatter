import ComponentBackendTagForm from './ComponentBackendTagForm.js'
import ComponentBackendTagTable from './ComponentBackendTagTable.js'
import ComponentBackendTagUrl from './ComponentBackendTagUrl.js'
const ComponentBackendTagMixin = {
    computed: {
        ComponentBackendTagFormOptions() {
            return this.$stDynamicFormTools.options(ComponentBackendTagForm,this.$options.name)
        },
        ComponentBackendTagTableOptions() {
            return ComponentBackendTagTable
        },
        ComponentBackendTagUrl(){
            return ComponentBackendTagUrl
        }
    },
}
export default ComponentBackendTagMixin