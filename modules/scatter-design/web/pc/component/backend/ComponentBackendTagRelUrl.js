const basePath = '' + '/component.backend/component-backend-tag-rel'
const ComponentBackendTagRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentBackendTagRelSearchList',
        add: '/ComponentBackendTagRelAdd',
        update: '/ComponentBackendTagRelUpdate',
    }
}
export default ComponentBackendTagRelUrl