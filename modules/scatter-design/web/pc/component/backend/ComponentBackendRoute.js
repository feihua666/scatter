import ComponentBackendUrl from './ComponentBackendUrl.js'
const ComponentBackendRoute = [
    {
        path: ComponentBackendUrl.router.searchList,
        component: () => import('./element/ComponentBackendSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentBackendSearchList',
            name: '后端组件管理'
        }
    },
    {
        path: ComponentBackendUrl.router.add,
        component: () => import('./element/ComponentBackendAdd'),
        meta: {
            code:'ComponentBackendAdd',
            name: '后端组件添加'
        }
    },
    {
        path: ComponentBackendUrl.router.update,
        component: () => import('./element/ComponentBackendUpdate'),
        meta: {
            code:'ComponentBackendUpdate',
            name: '后端组件修改'
        }
    },
]
export default ComponentBackendRoute