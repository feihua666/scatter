import ComponentBackendForm from './ComponentBackendForm.js'
import ComponentBackendTable from './ComponentBackendTable.js'
import ComponentBackendUrl from './ComponentBackendUrl.js'
const ComponentBackendMixin = {
    computed: {
        ComponentBackendFormOptions() {
            return this.$stDynamicFormTools.options(ComponentBackendForm,this.$options.name)
        },
        ComponentBackendTableOptions() {
            return ComponentBackendTable
        },
        ComponentBackendUrl(){
            return ComponentBackendUrl
        }
    },
}
export default ComponentBackendMixin