const ComponentBackendTagRelTable = [
    {
        prop: 'componentBackendId',
        label: '后端组件id'
    },
    {
        prop: 'componentBackendTagId',
        label: '后端组件标签id'
    },
]
export default ComponentBackendTagRelTable