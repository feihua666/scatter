import ComponentBackendTagUrl from './ComponentBackendTagUrl.js'
const ComponentBackendTagRoute = [
    {
        path: ComponentBackendTagUrl.router.searchList,
        component: () => import('./element/ComponentBackendTagSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentBackendTagSearchList',
            name: '后端组件标签管理'
        }
    },
    {
        path: ComponentBackendTagUrl.router.add,
        component: () => import('./element/ComponentBackendTagAdd'),
        meta: {
            code:'ComponentBackendTagAdd',
            name: '后端组件标签添加'
        }
    },
    {
        path: ComponentBackendTagUrl.router.update,
        component: () => import('./element/ComponentBackendTagUpdate'),
        meta: {
            code:'ComponentBackendTagUpdate',
            name: '后端组件标签修改'
        }
    },
]
export default ComponentBackendTagRoute