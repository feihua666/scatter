const basePath = '' + '/component.backend/component-backend-tag'
const ComponentBackendTagUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentBackendTagSearchList',
        add: '/ComponentBackendTagAdd',
        update: '/ComponentBackendTagUpdate',
    }
}
export default ComponentBackendTagUrl