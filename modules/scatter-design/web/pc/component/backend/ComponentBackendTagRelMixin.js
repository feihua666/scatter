import ComponentBackendTagRelForm from './ComponentBackendTagRelForm.js'
import ComponentBackendTagRelTable from './ComponentBackendTagRelTable.js'
import ComponentBackendTagRelUrl from './ComponentBackendTagRelUrl.js'
const ComponentBackendTagRelMixin = {
    computed: {
        ComponentBackendTagRelFormOptions() {
            return this.$stDynamicFormTools.options(ComponentBackendTagRelForm,this.$options.name)
        },
        ComponentBackendTagRelTableOptions() {
            return ComponentBackendTagRelTable
        },
        ComponentBackendTagRelUrl(){
            return ComponentBackendTagRelUrl
        }
    },
}
export default ComponentBackendTagRelMixin