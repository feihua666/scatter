import ComponentBackendTagRelUrl from './ComponentBackendTagRelUrl.js'
const ComponentBackendTagRelForm = [
    {
        ComponentBackendTagRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentBackendId',
        },
        element:{
            label: '后端组件id',
            required: true,
        }
    },
    {
        ComponentBackendTagRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'componentBackendTagId',
        },
        element:{
            label: '后端组件标签id',
            required: true,
        }
    },
]
export default ComponentBackendTagRelForm