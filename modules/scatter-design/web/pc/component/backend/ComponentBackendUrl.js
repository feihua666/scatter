const basePath = '' + '/component.backend/component-backend'
const ComponentBackendUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComponentBackendSearchList',
        add: '/ComponentBackendAdd',
        update: '/ComponentBackendUpdate',
    }
}
export default ComponentBackendUrl