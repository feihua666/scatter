import ComponentBackendTagRelUrl from './ComponentBackendTagRelUrl.js'
const ComponentBackendTagRelRoute = [
    {
        path: ComponentBackendTagRelUrl.router.searchList,
        component: () => import('./element/ComponentBackendTagRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComponentBackendTagRelSearchList',
            name: '后端组件标签关系管理'
        }
    },
    {
        path: ComponentBackendTagRelUrl.router.add,
        component: () => import('./element/ComponentBackendTagRelAdd'),
        meta: {
            code:'ComponentBackendTagRelAdd',
            name: '后端组件标签关系添加'
        }
    },
    {
        path: ComponentBackendTagRelUrl.router.update,
        component: () => import('./element/ComponentBackendTagRelUpdate'),
        meta: {
            code:'ComponentBackendTagRelUpdate',
            name: '后端组件标签关系修改'
        }
    },
]
export default ComponentBackendTagRelRoute