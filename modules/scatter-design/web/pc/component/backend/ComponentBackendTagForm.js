import ComponentBackendTagUrl from './ComponentBackendTagUrl.js'
const ComponentBackendTagForm = [
    {
        ComponentBackendTagSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
]
export default ComponentBackendTagForm