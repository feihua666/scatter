import AreaRoute from '../../../../../../components/area/web/pc/AreaRoute.js'
import DictRoute from '../../../../../../components/dict/web/pc/DictRoute.js'
import FuncRoute from '../../../../../../components/func/web/pc/FuncRoute.js'
import RoleRoutesEntry from '../../../../../../components/role/web/pc/RoleRoutesEntry.js'
import UserSimpleRoute from '../../../../../../components/usersimple/web/pc/UserSimpleRoute.js'
import FuncGroupRoute from '../../../../../../components/func/web/pc/FuncGroupRoute.js'
import FileRoute from '../../../../../../components/file/web/pc/FileRoute.js'

import WxMpRoutesEntry from '../../../../../../components/wx-mp/web/pc/WxMpRoutesEntry.js'


import BannerRoute from '../../../../../../components/banner/web/pc/BannerRoute'
import ErrorLogRoute from '../../../../../../components/error-log/web/pc/ErrorLogRoute'
import ScheduleRoutesEntry from '../../../../../../components/schedule-quartz/web/pc/ScheduleRoutesEntry'


let indexChildren = [].concat(AreaRoute)
.concat(DictRoute)
.concat(FuncRoute)
.concat(RoleRoutesEntry)
.concat(UserSimpleRoute)
.concat(FuncGroupRoute)
.concat(FileRoute)

.concat(WxMpRoutesEntry)

.concat(BannerRoute)
.concat(ErrorLogRoute)

.concat(ScheduleRoutesEntry)
let routes = [
    {
        path: '/login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/index',
        component: () => import('../views/Index.vue'),
        children: indexChildren
    },
]

export default routes