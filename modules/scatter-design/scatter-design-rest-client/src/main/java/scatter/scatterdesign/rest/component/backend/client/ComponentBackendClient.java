package scatter.scatterdesign.rest.component.backend.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 后端组件表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Component
@FeignClient(value = "ComponentBackend-client")
public interface ComponentBackendClient {

}
