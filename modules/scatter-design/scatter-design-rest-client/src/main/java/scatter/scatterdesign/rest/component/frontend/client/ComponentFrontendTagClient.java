package scatter.scatterdesign.rest.component.frontend.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 前端组件标签表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@Component
@FeignClient(value = "ComponentFrontendTag-client")
public interface ComponentFrontendTagClient {

}
