package scatter.scatterdesign.rest.component.frontend.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.scatterdesign.rest.ScatterDesignConfiguration;
import scatter.scatterdesign.pojo.component.frontend.po.ComponentFrontendTagRel;
import scatter.scatterdesign.rest.component.frontend.service.IComponentFrontendTagRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 前端组件和前端组件标签关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-22
 */
@RestController
@RequestMapping(ScatterDesignConfiguration.CONTROLLER_BASE_PATH + "/inner/component.frontend/component-frontend-tag-rel")
public class ComponentFrontendTagRelInnerController extends BaseInnerController<ComponentFrontendTagRel> {
 @Autowired
 private IComponentFrontendTagRelService componentFrontendTagRelService;

 public IComponentFrontendTagRelService getService(){
     return componentFrontendTagRelService;
 }
}
