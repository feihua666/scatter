const UserEnjoyForm = [
    {
        field: {
            name: 'userId'
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'enjoyedUserId'
        },
        element:{
            label: '有意思用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'message'
        },
        element:{
            label: '添加有意思时',
        }
    },
]
export default UserEnjoyForm