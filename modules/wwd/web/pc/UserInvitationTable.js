const UserInvitationTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'code',
        label: '邀请码'
    },
    {
        prop: 'isUsed',
        label: '是否已使用'
    },
    {
        prop: 'invitedUserId',
        label: '被邀请的用户'
    },
]
export default UserInvitationTable