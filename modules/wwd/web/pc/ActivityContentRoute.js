import ActivityContentUrl from './ActivityContentUrl.js'

const ActivityContentRoute = [
    {
        path: ActivityContentUrl.router.searchList,
        component: () => import('./element/ActivityContentSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ActivityContentSearchList',
            name: '活动内容管理'
        }
    },
    {
        path: ActivityContentUrl.router.add,
        component: () => import('./element/ActivityContentAdd'),
        meta: {
            code:'ActivityContentAdd',
            name: '活动内容添加'
        }
    },
    {
        path: ActivityContentUrl.router.update,
        component: () => import('./element/ActivityContentUpdate'),
        meta: {
            code:'ActivityContentUpdate',
            name: '活动内容修改'
        }
    },
]
export default ActivityContentRoute