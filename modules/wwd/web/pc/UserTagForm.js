import UserSimpleUrl from "../../../../components/usersimple/web/pc/UserSimpleUrl.js";

const UserTagForm = [
    {
        field: {
            name: 'userId'
        },
        element:{
            label: '用户',
            required: false,
            type: 'select',
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'nickname', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按昵称搜索',
                    remote: true,
                },
                remoteUrl: UserSimpleUrl.searchList,
                remoteQueryProp: 'nickname'
            }
        }
    },
    {
        field: {
            name: 'typeDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'movie_type_parent,food_type_parent,sport_type_parent,trip_type_parent,hobby_type_parent,nature_type_parent',
                groupType: 'group'
            },
            label: '标签类型',
            required: false,
        }
    },
    {
        field: {
            name: 'content'
        },
        element:{
            type: 'selectDict',
            options: ({form,extForm}) => {
                return {
                    groupCode: extForm.typeDictId ? extForm.typeDictId.code: '',
                }
            },
            label: '标签内容',
            required: false,
        }
    },
    {
        field: {
            name: 'selfContent'
        },
        element:{
            label: '自定义标签内容',
        }
    },
]
export default UserTagForm