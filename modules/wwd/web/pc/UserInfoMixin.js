import UserInfoForm from './UserInfoForm.js'
import UserInfoTable from './UserInfoTable.js'
import UserInfoUrl from './UserInfoUrl.js'

const UserInfoMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserInfoForm,this.$options.name)
        },
        computedTableOptions() {
            return UserInfoTable
        },
        computedUrl(){
            return UserInfoUrl
        }
    },
}
export default UserInfoMixin