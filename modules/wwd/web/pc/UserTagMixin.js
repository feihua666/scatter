import UserTagForm from './UserTagForm.js'
import UserTagTable from './UserTagTable.js'
import UserTagUrl from './UserTagUrl.js'

const UserTagMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserTagForm,this.$options.name)
        },
        computedTableOptions() {
            return UserTagTable
        },
        computedUrl(){
            return UserTagUrl
        }
    },
}
export default UserTagMixin