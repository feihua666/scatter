import ActivityUrl from "./ActivityUrl";
import UserSimpleUrl from "../../../../components/usersimple/web/pc/UserSimpleUrl.js";

const ActivityParticipateForm = [
    {
        ActivityParticipateSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'activityId'
        },
        element:{
            type: 'select',
            label: '活动',
            required: true,
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'title', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按活动标题搜索',
                    remote: true,
                },
                remoteUrl: ActivityUrl.searchList,
                remoteQueryProp: 'title'
            }
        }
    },
    {
        ActivityParticipateSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'userId'
        },
        element:{
            label: '用户',
            required: true,
            type: 'select',
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'nickname', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按昵称搜索',
                    remote: true,
                },
                remoteUrl: UserSimpleUrl.searchList,
                remoteQueryProp: 'nickname'
            }
        }
    },
    {
        ActivityParticipateSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'isLeader'
        },
        element:{
            label: '是否是领队',
            required: true,
            type: 'select',
            options: {
                datas: 'yesNoBool'
            }
        }
    },
    {
        ActivityParticipateSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'isQuit'
        },
        element:{
            label: '报名状态',
            required: true,
            type: 'select',
            options: {
                datas: [
                    {
                        id: true,
                        name: '已报名'
                    },
                    {
                        id: false,
                        name: '已取消'
                    },
                ]
            }
        }
    },

    {
        field: {
            name: 'name'
        },
        element:{
            label: '报名人员姓名',
        }
    },
    {
        field: {
            name: 'mobile'
        },
        element:{
            label: '报名人员手机号',
        }
    },
    {
        field: {
            name: 'idTypeDictId'
        },
        element:{
            label: '证件类型',
            type: 'selectDict',
            options: {
                groupCode: 'id_type_group'
            }
        }
    },
    {
        field: {
            name: 'idCardNo'
        },
        element:{
            label: '证件号码',
        }
    },
    {
        ActivityParticipateSearchList: false,
        field: {
            name: 'remarks'
        },
        element:{
            label: '备注',
        }
    },
]
export default ActivityParticipateForm