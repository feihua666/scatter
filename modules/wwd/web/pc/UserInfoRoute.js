import UserInfoUrl from './UserInfoUrl.js'

const UserInfoRoute = [
    {
        path: UserInfoUrl.router.searchList,
        component: () => import('./element/UserInfoSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserInfoSearchList',
            name: '用户信息管理'
        }
    },
    {
        path: UserInfoUrl.router.add,
        component: () => import('./element/UserInfoAdd'),
        meta: {
            code:'UserInfoAdd',
            name: '用户信息添加'
        }
    },
    {
        path: UserInfoUrl.router.update,
        component: () => import('./element/UserInfoUpdate'),
        meta: {
            code:'UserInfoUpdate',
            name: '用户信息修改'
        }
    },
]
export default UserInfoRoute