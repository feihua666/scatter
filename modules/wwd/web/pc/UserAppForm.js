import UserAppUrl from './UserAppUrl.js'
const UserAppForm = [
    {
        UserAppSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'nickname',
        },
        element:{
            label: '昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'genderDictId',
        },
        element:{
            label: '性别',
        }
    },
    {
        field: {
            name: 'avatar',
        },
        element:{
            label: '头像',
        }
    },
    {
        UserAppSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isLock',
            value: false,
        },
        element:{
            type: 'switch',
            label: '锁定状态',
            required: true,
        }
    },
    {
        field: {
            name: 'lockReason',
        },
        element:{
            label: '锁定原因',
        }
    },
]
export default UserAppForm