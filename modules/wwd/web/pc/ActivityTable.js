const ActivityTable = [
    {
        prop: 'title',
        label: '活动标题',
        showOverflowTooltip: true
    },
    {
        prop: 'titleUrl',
        label: '标题图',
        stype: 'image'
    },
    {
        prop: 'introduced',
        label: '活动简介',
        showOverflowTooltip: true
    },
    {
        prop: 'sponsor',
        label: '主办方'
    },
    {
        prop: 'sponsorLink',
        label: '主办方链接'
    },
    {
        prop: 'author',
        label: '作者'
    },
    {
        prop: 'startTime',
        label: '开始时间'
    },
    {
        prop: 'endTime',
        label: '结束时间'
    },
    {
        prop: 'address',
        label: '活动地点',
        showOverflowTooltip: true
    },
    {
        prop: 'contact',
        label: '联系方式'
    },
    {
        prop: 'placeTypeDictName',
        label: '场地类型'
    },
    {
        prop: 'isPublish',
        label: '是否发布'
    },
    {
        prop: 'activityStatement',
        label: '活动声明',
        showOverflowTooltip: true
    },
    {
        prop: 'seq',
        label: '序号'
    },
    {
        prop: 'createAt',
        label: '创建时间'
    },
    {
        prop: 'updateAt',
        label: '更新时间'
    },
]
export default ActivityTable