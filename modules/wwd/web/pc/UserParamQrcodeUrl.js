const basePath = '' + '/user-param-qrcode'
const UserParamQrcodeUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserParamQrcodeSearchList',
        add: '/UserParamQrcodeAdd',
        update: '/UserParamQrcodeUpdate',
    }
}
export default UserParamQrcodeUrl