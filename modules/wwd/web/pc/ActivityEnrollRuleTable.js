const ActivityEnrollRuleTable = [
    {
        type: 'expand'
    },
    {
        prop: 'activityTitle',
        label: '活动名称'
    },
    {
        prop: 'startAt',
        label: '报名开始时间',
        showInDetail: true
    },
    {
        prop: 'endAt',
        label: '报名结束时间',
        showInDetail: true
    },
    {
        prop: 'contact',
        label: '报名联系方式',
        showInDetail: true
    },
    {
        prop: 'isRequireIdCard',
        label: '需要证件',
        showInDetail: true
    },
    {
        prop: 'headcount',
        label: '活动限制总人数',
        showInDetail: true
    },
    {
        prop: 'headcountMale',
        label: '活动限制男人数'
    },
    {
        prop: 'headcountFemale',
        label: '活动限制女人数'
    },
    {
        prop: 'headcountDesc',
        label: '人数说明',
        showInDetail: true
    },
    {
        prop: 'isHeadcountGenderSelf',
        label: '人数自定义',
        showInDetail: true
    },
    {
        prop: 'malePrice',
        label: '男价格(分)'
    },
    {
        prop: 'femalePrice',
        label: '女价格(分)'
    },
    {
        prop: 'isNeedPayOnline',
        label: '线上支付'
    },
    {
        prop: 'countMale',
        label: '已报名人数男'
    },
    {
        prop: 'countFemale',
        label: '已报名人数女'
    }
]
export default ActivityEnrollRuleTable