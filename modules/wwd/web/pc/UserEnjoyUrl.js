const basePath = '' + '/user-enjoy'
const UserEnjoyUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    queryEnjoy: basePath + '/current/enjoy/{enjoyedUserId}',
    queryEnjoyed: basePath + '/{userId}/enjoy/current',
    queryUserEnjoyStatus: basePath + '/userEnjoyStatus/{enjoyedUserId}',
    router: {
        searchList: '/UserEnjoySearchList',
        add: '/UserEnjoyAdd',
        update: '/UserEnjoyUpdate',
    }
}
export default UserEnjoyUrl