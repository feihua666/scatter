const ActivityParticipateTable = [
    {
        type: 'expand'
    },
    {
        prop: 'activityTitle',
        label: '活动标题'
    },
    {
        prop: 'userNickname',
        label: '用户昵称'
    },
    {
        prop: 'userName',
        label: '用户真实姓名',
        formatter(row){
            return row.userName ? row.userName : row.name
        }
    },
    {
        prop: 'userAvatar',
        label: '用户头像',
        stype: 'image'
    },
    {
        prop: 'userGenderDictName',
        label: '用户性别',
    },
    {
        prop: 'isLeader',
        label: '是否领队'
    },
    {
        prop: 'isQuit',
        label: '报名状态',
        formatter(row){
            return row.isQuit ? '已报名' : '已取消'
        }
    },
    {
        prop: 'orderNo',
        label: '订单号'
    },
    {
        prop: 'mobile',
        label: '报名人员手机号',
        showInDetail: true,
    },
    {
        prop: 'idCardNo',
        label: '证件号码',
        showInDetail: true,
    },
    {
        prop: 'idTypeDictName',
        label: '证件类型',
        showInDetail: true,
    },
    {
        prop: 'remarks',
        label: '备注'
    },
]
export default ActivityParticipateTable