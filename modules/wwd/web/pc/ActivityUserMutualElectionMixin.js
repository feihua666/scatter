import ActivityUserMutualElectionForm from './ActivityUserMutualElectionForm.js'
import ActivityUserMutualElectionTable from './ActivityUserMutualElectionTable.js'
import ActivityUserMutualElectionUrl from './ActivityUserMutualElectionUrl.js'

const ActivityUserMutualElectionMixin = {
    computed: {
        computedFormOptions() {
            // 回显远程搜索，从活动跳转时回显
            if (this.$route.query.activityId && this.$route.query.activityTitle) {
                ActivityUserMutualElectionForm[0].element.options.datas = [{id:this.$route.query.activityId,title:this.$route.query.activityTitle}]
                ActivityUserMutualElectionForm[0].field.value = this.$route.query.activityId
            }
            return this.$stDynamicFormTools.options(ActivityUserMutualElectionForm,this.$options.name)
        },
        computedTableOptions() {
            return ActivityUserMutualElectionTable
        },
        computedUrl(){
            return ActivityUserMutualElectionUrl
        }
    },
}
export default ActivityUserMutualElectionMixin