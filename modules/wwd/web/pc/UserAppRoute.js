import UserAppUrl from './UserAppUrl.js'
const UserAppRoute = [
    {
        path: UserAppUrl.router.searchList,
        component: () => import('./element/UserAppSearchList'),
        meta: {
            code:'UserAppSearchList',
            name: '简单用户管理'
        }
    },
    {
        path: UserAppUrl.router.add,
        component: () => import('./element/UserAppAdd'),
        meta: {
            code:'UserAppAdd',
            name: '简单用户添加'
        }
    },
    {
        path: UserAppUrl.router.update,
        component: () => import('./element/UserAppUpdate'),
        meta: {
            code:'UserAppUpdate',
            name: '简单用户修改'
        }
    },
]
export default UserAppRoute