import UserVisitForm from './UserVisitForm.js'
import UserVisitTable from './UserVisitTable.js'
import UserVisitUrl from './UserVisitUrl.js'

const UserVisitMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserVisitForm,this.$options.name)
        },
        computedTableOptions() {
            return UserVisitTable
        },
        computedUrl(){
            return UserVisitUrl
        }
    },
}
export default UserVisitMixin