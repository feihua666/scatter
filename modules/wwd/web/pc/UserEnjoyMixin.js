import UserEnjoyForm from './UserEnjoyForm.js'
import UserEnjoyTable from './UserEnjoyTable.js'
import UserEnjoyUrl from './UserEnjoyUrl.js'

const UserEnjoyMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserEnjoyForm,this.$options.name)
        },
        computedTableOptions() {
            return UserEnjoyTable
        },
        computedUrl(){
            return UserEnjoyUrl
        }
    },
}
export default UserEnjoyMixin