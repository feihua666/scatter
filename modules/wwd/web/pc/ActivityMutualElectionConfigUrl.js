const basePath = '' + '/activity-mutual-election-config'
const ActivityMutualElectionConfigUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ActivityMutualElectionConfigSearchList',
        add: '/ActivityMutualElectionConfigAdd',
        update: '/ActivityMutualElectionConfigUpdate',
    }
}
export default ActivityMutualElectionConfigUrl