import UserParamQrcodeForm from './UserParamQrcodeForm.js'
import UserParamQrcodeTable from './UserParamQrcodeTable.js'
import UserParamQrcodeUrl from './UserParamQrcodeUrl.js'

const UserParamQrcodeMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserParamQrcodeForm,this.$options.name)
        },
        computedTableOptions() {
            return UserParamQrcodeTable
        },
        computedUrl(){
            return UserParamQrcodeUrl
        }
    },
}
export default UserParamQrcodeMixin