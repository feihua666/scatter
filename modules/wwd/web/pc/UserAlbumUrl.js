const basePath = '' + '/user-album'
const UserAlbumUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserAlbumSearchList',
        add: '/UserAlbumAdd',
        update: '/UserAlbumUpdate',
    }
}
export default UserAlbumUrl