const UserVisitForm = [
    {
        field: {
            name: 'userId'
        },
        element:{
            label: '访问的用户ID',
            required: true,
        }
    },
    {
        field: {
            name: 'visitUserId'
        },
        element:{
            label: '被访问的用户ID',
            required: true,
        }
    },
    {
        field: {
            name: 'visitTypeDictId'
        },
        element:{
            label: '访问类型',
            required: true,
        }
    },
    {
        field: {
            name: 'isRead'
        },
        element:{
            label: '是否已读',
            required: true,
        }
    },
    {
        field: {
            name: 'readAt'
        },
        element:{
            label: '读取',
        }
    },
]
export default UserVisitForm