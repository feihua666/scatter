// import AreaUrl from './AreaUrl.js'
const ActivityForm = [
    {
        ActivitySearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'title'
        },
        element:{
            label: '活动标题',
            required: true,
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'titleUrl'
        },
        element:{
            type: 'uploadSingleImage',
            label: '标题图',
        }
    },
    {
        field: {
            name: 'author'
        },
        element:{
            label: '作者',
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'cityAreaId'
        },
        element:{
            type: 'cascader',
            options: {
                datas: '/area/getList?levelMin=1&levelMax=4',
                cacheKey: 'areaListCityProvince',
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
                originProp: {
                    filterMethod(node, keyword) {
                        if (!keyword) return true;
                        return node.data.name.indexOf(keyword) !== -1
                            || node.data.spellFirst.indexOf(keyword) !== -1
                            || node.data.spellSimple.indexOf(keyword) !== -1
                            || node.data.spell.indexOf(keyword) !== -1
                            ;
                    }
                }
            },
            label: '所在城市',
            isBlock: true
        }
    },
    {
        field: {
            name: 'address'
        },
        element:{
            label: '活动地点',
        }
    },
    {
        ActivitySearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'placeTypeDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'activity_place_type'
            },
            label: '场地类型',
            required: true,
        }
    },
    {
        field: {
            name: 'sponsor'
        },
        element:{
            label: '主办方',
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'sponsorLink'
        },
        element:{
            label: '主办方链接',
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'contact'
        },
        element:{
            label: '联系方式',
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'startTime'
        },
        element:{
            type: 'datetime',
            label: '开始时间',
            options: {
                pickerOptions: {
                    disabledDate: (time) => {
                        return time.getTime() < Date.now();
                    }
                }
            },
            required: true
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'endTime'
        },
        element:{
            type: 'datetime',
            label: '结束时间',
            options: ({form})=> {
                return {
                    pickerOptions: {
                        disabledDate: (time) => {
                            return time.getTime() < (form.startTime ? new Date(form.startTime).getTime(): Date.now());
                        }
                    }
                }
            },
            required: true
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'isPublish',
            value: false
        },
        element:{
            type: 'switch',
            label: '是否发布',
            required: true,
        }
    },

    {
        ActivitySearchList: false,
        field: {
            name: 'seq',
            value: 10
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        },
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'introduced'
        },
        element:{
            type: 'textarea',
            label: '活动简介',
            isBlock: true
        }
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'content'
        },
        element:{
            type: 'tinymceEditor',
            label: '活动内容',
            required: true,
            isBlock: true
        },
    },
    {
        ActivitySearchList: false,
        field: {
            name: 'activityStatement'
        },
        element:{
            type: 'textarea',
            label: '活动声明',
            isBlock: true
        }
    },
]
export default ActivityForm