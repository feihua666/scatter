const UserAlbumForm = [
    {
        field: {
            name: 'userId'
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'picUrl'
        },
        element:{
            label: '图片url',
            required: true,
        }
    },
    {
        field: {
            name: 'picThumbUrl'
        },
        element:{
            label: '缩略图url',
            required: true,
        }
    },
    {
        field: {
            name: 'description'
        },
        element:{
            label: '描述',
        }
    },
]
export default UserAlbumForm