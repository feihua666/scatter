import UserAlbumForm from './UserAlbumForm.js'
import UserAlbumTable from './UserAlbumTable.js'
import UserAlbumUrl from './UserAlbumUrl.js'

const UserAlbumMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserAlbumForm,this.$options.name)
        },
        computedTableOptions() {
            return UserAlbumTable
        },
        computedUrl(){
            return UserAlbumUrl
        }
    },
}
export default UserAlbumMixin