import UserAlbumUrl from './UserAlbumUrl.js'

const UserAlbumRoute = [
    {
        path: UserAlbumUrl.router.searchList,
        component: () => import('./element/UserAlbumSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserAlbumSearchList',
            name: '用户相册管理'
        }
    },
    {
        path: UserAlbumUrl.router.add,
        component: () => import('./element/UserAlbumAdd'),
        meta: {
            code:'UserAlbumAdd',
            name: '用户相册添加'
        }
    },
    {
        path: UserAlbumUrl.router.update,
        component: () => import('./element/UserAlbumUpdate'),
        meta: {
            code:'UserAlbumUpdate',
            name: '用户相册修改'
        }
    },
]
export default UserAlbumRoute