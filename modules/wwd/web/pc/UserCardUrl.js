const basePath = '' + '/user-card'
const UserCardUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserCardSearchList',
        add: '/UserCardAdd',
        update: '/UserCardUpdate',
    }
}
export default UserCardUrl