import UserParamQrcodeUrl from './UserParamQrcodeUrl.js'

const UserParamQrcodeRoute = [
    {
        path: UserParamQrcodeUrl.router.searchList,
        component: () => import('./element/UserParamQrcodeSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserParamQrcodeSearchList',
            name: '用户标签管理'
        }
    },
    {
        path: UserParamQrcodeUrl.router.add,
        component: () => import('./element/UserParamQrcodeAdd'),
        meta: {
            code:'UserParamQrcodeAdd',
            name: '用户标签添加'
        }
    },
    {
        path: UserParamQrcodeUrl.router.update,
        component: () => import('./element/UserParamQrcodeUpdate'),
        meta: {
            code:'UserParamQrcodeUpdate',
            name: '用户标签修改'
        }
    },
]
export default UserParamQrcodeRoute