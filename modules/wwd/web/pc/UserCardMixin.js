import UserCardForm from './UserCardForm.js'
import UserCardTable from './UserCardTable.js'
import UserCardUrl from './UserCardUrl.js'

const UserCardMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserCardForm,this.$options.name)
        },
        computedTableOptions() {
            return UserCardTable
        },
        computedUrl(){
            return UserCardUrl
        }
    },
}
export default UserCardMixin