import UserEnjoyUrl from './UserEnjoyUrl.js'

const UserEnjoyRoute = [
    {
        path: UserEnjoyUrl.router.searchList,
        component: () => import('./element/UserEnjoySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserEnjoySearchList',
            name: '用户有意思管理'
        }
    },
    {
        path: UserEnjoyUrl.router.add,
        component: () => import('./element/UserEnjoyAdd'),
        meta: {
            code:'UserEnjoyAdd',
            name: '用户有意思添加'
        }
    },
    {
        path: UserEnjoyUrl.router.update,
        component: () => import('./element/UserEnjoyUpdate'),
        meta: {
            code:'UserEnjoyUpdate',
            name: '用户有意思修改'
        }
    },
]
export default UserEnjoyRoute