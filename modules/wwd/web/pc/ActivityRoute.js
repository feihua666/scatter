import ActivityUrl from './ActivityUrl.js'

const ActivityRoute = [
    {
        path: ActivityUrl.router.searchList,
        component: () => import('./element/ActivitySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ActivitySearchList',
            name: '活动管理'
        }
    },
    {
        path: ActivityUrl.router.add,
        component: () => import('./element/ActivityAdd'),
        meta: {
            code:'ActivityAdd',
            name: '活动添加'
        }
    },
    {
        path: ActivityUrl.router.update,
        component: () => import('./element/ActivityUpdate'),
        meta: {
            code:'ActivityUpdate',
            name: '活动修改'
        }
    },
]
export default ActivityRoute