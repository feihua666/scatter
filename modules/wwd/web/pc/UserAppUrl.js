const basePath = '' + '/user-app'
const UserAppUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserAppSearchList',
        add: '/UserAppAdd',
        update: '/UserAppUpdate',
    }
}
export default UserAppUrl