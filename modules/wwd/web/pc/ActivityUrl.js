const basePath = '' + '/activity'
const ActivityUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    activityWithContent: basePath + '/activityWithContent/{id}',
    getPage1: basePath + '/getPage1',
    activityDetail: basePath + '/activityDetail/{id}',
    activityParticipateConfirmInfo: basePath + '/activityParticipate/confirmInfo/{id}',
    router: {
        searchList: '/ActivitySearchList',
        add: '/ActivityAdd',
        update: '/ActivityUpdate',
    }
}
export default ActivityUrl