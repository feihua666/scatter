import ActivityUrl from './ActivityUrl.js'
import UserInfoUrl from './UserInfoUrl.js'
const ActivityMutualElectionConfigForm = [
    {
        ActivityMutualElectionConfigSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'activityId'
        },
        element:{
            type: 'select',
            label: '活动',
            required: true,
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'title', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按活动标题搜索',
                    remote: true,
                },
                remoteUrl: ActivityUrl.searchList,
                remoteQueryProp: 'title'
            }
        }
    },
    {
        ActivityMutualElectionConfigSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'mutualElectionStatusDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'mutual_election_status'
            },
            label: '互选状态',
            required: true,
        }
    },
    {
        ActivityMutualElectionConfigSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'manageUserIds'
        },
        element:{
            type: 'select',
            label: '用户',
            required: true,
            options: {
                datas: UserInfoUrl.list,
                optionProp:{
                    value: 'userId', // 选中的值属性
                    label: 'name', // 显示的值属性
                }
            }
        }
    },
]
export default ActivityMutualElectionConfigForm