import ActivityParticipateUrl from './ActivityParticipateUrl.js'

const ActivityParticipateRoute = [
    {
        path: ActivityParticipateUrl.router.searchList,
        component: () => import('./element/ActivityParticipateSearchList'),
        meta: {
            root: true,
            code:'ActivityParticipateSearchList',
            name: '活动参与管理',
            keepAlive: true
        }
    },
    {
        path: ActivityParticipateUrl.router.update,
        component: () => import('./element/ActivityParticipateUpdate'),
        meta: {
            code:'ActivityParticipateUpdate',
            name: '活动参与修改'
        }
    },
]
export default ActivityParticipateRoute