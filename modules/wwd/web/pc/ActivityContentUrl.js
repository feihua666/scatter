const basePath = '' + '/activity-content'
const ActivityContentUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ActivityContentSearchList',
        add: '/ActivityContentAdd',
        update: '/ActivityContentUpdate',
    }
}
export default ActivityContentUrl