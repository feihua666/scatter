const basePath = '' + '/user-visit'
const UserVisitUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserVisitSearchList',
        add: '/UserVisitAdd',
        update: '/UserVisitUpdate',
    }
}
export default UserVisitUrl