import UserCardUrl from './UserCardUrl.js'

const UserCardRoute = [
    {
        path: UserCardUrl.router.searchList,
        component: () => import('./element/UserCardSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserCardSearchList',
            name: '用户卡片管理'
        }
    },
    {
        path: UserCardUrl.router.add,
        component: () => import('./element/UserCardAdd'),
        meta: {
            code:'UserCardAdd',
            name: '用户卡片添加'
        }
    },
    {
        path: UserCardUrl.router.update,
        component: () => import('./element/UserCardUpdate'),
        meta: {
            code:'UserCardUpdate',
            name: '用户卡片修改'
        }
    },
]
export default UserCardRoute