const basePath = '' + '/activity-user-mutual-election'
const ActivityUserMutualElectionUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ActivityUserMutualElectionSearchList',
    }
}
export default ActivityUserMutualElectionUrl