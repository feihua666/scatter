const UserVisitTable = [
    {
        prop: 'userId',
        label: '访问的用户ID'
    },
    {
        prop: 'visitUserId',
        label: '被访问的用户ID'
    },
    {
        prop: 'visitTypeDictId',
        label: '访问类型'
    },
    {
        prop: 'isRead',
        label: '是否已读'
    },
    {
        prop: 'readAt',
        label: '读取'
    },
]
export default UserVisitTable