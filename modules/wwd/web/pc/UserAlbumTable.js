const UserAlbumTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'picUrl',
        label: '图片url'
    },
    {
        prop: 'picThumbUrl',
        label: '缩略图url'
    },
    {
        prop: 'description',
        label: '描述'
    },
]
export default UserAlbumTable