const UserAppTable = [
    {
        prop: 'nickname',
        label: '昵称'
    },
    {
        prop: 'genderDictId',
        label: '性别'
    },
    {
        prop: 'avatar',
        label: '头像'
    },
    {
        prop: 'isLock',
        label: '锁定状态'
    },
    {
        prop: 'lockReason',
        label: '锁定原因'
    },
]
export default UserAppTable