const ActivityContentForm = [
    {
        field: {
            name: 'activityId'
        },
        element:{
            label: '活动id',
            required: true,
        }
    },
    {
        field: {
            name: 'content'
        },
        element:{
            type: 'tinymceEditor',
            label: '活动内容',
        }
    },
]
export default ActivityContentForm