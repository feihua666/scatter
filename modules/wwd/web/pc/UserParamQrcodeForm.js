const UserParamQrcodeForm = [
    {
        field: {
            name: 'userId'
        },
        element:{
            label: '微信用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'ticket'
        },
        element:{
            label: '二维码tiket',
            required: true,
        }
    },
    {
        field: {
            name: 'expireSeconds'
        },
        element:{
            label: '过期时长',
        }
    },
    {
        field: {
            name: 'content'
        },
        element:{
            label: '二维码图片内容',
            required: true,
        }
    },
    {
        field: {
            name: 'url'
        },
        element:{
            label: '二维码图片地址',
        }
    },
    {
        field: {
            name: 'isPermanent'
        },
        element:{
            label: '是否永久',
            required: true,
        }
    },
]
export default UserParamQrcodeForm