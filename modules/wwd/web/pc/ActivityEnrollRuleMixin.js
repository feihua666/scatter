import ActivityEnrollRuleForm from './ActivityEnrollRuleForm.js'
import ActivityEnrollRuleTable from './ActivityEnrollRuleTable.js'
import ActivityEnrollRuleUrl from './ActivityEnrollRuleUrl.js'

const ActivityEnrollRuleMixin = {
    computed: {
        computedFormOptions() {
            // 回显远程搜索
            if (this.$route.query.activityId && this.$route.query.activityTitle) {
                ActivityEnrollRuleForm[0].element.options.datas = [{id:this.$route.query.activityId,title:this.$route.query.activityTitle}]
                ActivityEnrollRuleForm[0].field.value = this.$route.query.activityId
            }
            return this.$stDynamicFormTools.options(ActivityEnrollRuleForm,this.$options.name)
        },
        computedTableOptions() {
            return ActivityEnrollRuleTable
        },
        computedUrl(){
            return ActivityEnrollRuleUrl
        }
    },
    methods: {
        dataFormat(data){
            if(data.isHeadcountGenderSelf){
                data.headcount = data.headcountMale + data.headcountFemale
            }
            return data
        }
    }
}
export default ActivityEnrollRuleMixin