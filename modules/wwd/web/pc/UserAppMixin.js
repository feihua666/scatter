import UserAppForm from './UserAppForm.js'
import UserAppTable from './UserAppTable.js'
import UserAppUrl from './UserAppUrl.js'
const UserAppMixin = {
    computed: {
        UserAppFormOptions() {
            return this.$stDynamicFormTools.options(UserAppForm,this.$options.name)
        },
        UserAppTableOptions() {
            return UserAppTable
        },
        UserAppUrl(){
            return UserAppUrl
        }
    },
}
export default UserAppMixin