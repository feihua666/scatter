const ActivityUserMutualElectionTable = [
    {
        prop: 'activityTitle',
        label: '活动id'
    },
    {
        prop: 'userNickname',
        label: '用户昵称'
    },
    {
        prop: 'selectedUserNickname',
        label: '被选择的用户昵称'
    },
    {
        prop: 'level',
        label: '互选级别：A-B-C...'
    },
]
export default ActivityUserMutualElectionTable