const basePath = '' + '/activity-enroll-rule'
const ActivityEnrollRuleUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ActivityEnrollRuleSearchList',
        add: '/ActivityEnrollRuleAdd',
        update: '/ActivityEnrollRuleUpdate',
    }
}
export default ActivityEnrollRuleUrl