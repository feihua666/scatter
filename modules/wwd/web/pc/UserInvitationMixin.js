import UserInvitationForm from './UserInvitationForm.js'
import UserInvitationTable from './UserInvitationTable.js'
import UserInvitationUrl from './UserInvitationUrl.js'

const UserInvitationMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserInvitationForm,this.$options.name)
        },
        computedTableOptions() {
            return UserInvitationTable
        },
        computedUrl(){
            return UserInvitationUrl
        }
    },
}
export default UserInvitationMixin