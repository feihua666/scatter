const UserParamQrcodeTable = [
    {
        prop: 'userId',
        label: '微信用户id'
    },
    {
        prop: 'ticket',
        label: '二维码tiket'
    },
    {
        prop: 'expireSeconds',
        label: '过期时长'
    },
    {
        prop: 'content',
        label: '二维码图片内容'
    },
    {
        prop: 'url',
        label: '二维码图片地址'
    },
    {
        prop: 'isPermanent',
        label: '是否永久'
    },
]
export default UserParamQrcodeTable