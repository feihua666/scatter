const basePath = '' + '/activity-participate'
const ActivityParticipateUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    // 查询报名人员
    participatePage: basePath + '/participatePage',
    router: {
        searchList: '/ActivityParticipateSearchList',
        update: '/ActivityParticipateUpdate',
    }
}
export default ActivityParticipateUrl