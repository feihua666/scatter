import ActivityContentForm from './ActivityContentForm.js'
import ActivityContentTable from './ActivityContentTable.js'
import ActivityContentUrl from './ActivityContentUrl.js'

const ActivityContentMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(ActivityContentForm,this.$options.name)
        },
        computedTableOptions() {
            return ActivityContentTable
        },
        computedUrl(){
            return ActivityContentUrl
        }
    },
}
export default ActivityContentMixin