import ActivityMutualElectionConfigUrl from './ActivityMutualElectionConfigUrl.js'

const ActivityMutualElectionConfigRoute = [
    {
        path: ActivityMutualElectionConfigUrl.router.searchList,
        component: () => import('./element/ActivityMutualElectionConfigSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ActivityMutualElectionConfigSearchList',
            name: '活动互选配置管理'
        }
    },
    {
        path: ActivityMutualElectionConfigUrl.router.add,
        component: () => import('./element/ActivityMutualElectionConfigAdd'),
        meta: {
            code:'ActivityMutualElectionConfigAdd',
            name: '活动互选配置添加'
        }
    },
    {
        path: ActivityMutualElectionConfigUrl.router.update,
        component: () => import('./element/ActivityMutualElectionConfigUpdate'),
        meta: {
            code:'ActivityMutualElectionConfigUpdate',
            name: '活动互选配置修改'
        }
    },
]
export default ActivityMutualElectionConfigRoute