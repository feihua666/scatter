const UserInfoTable = [

    {
        type: 'expand'
    },
    {
        prop: 'name',
        label: '真实姓名'
    },
    {
        prop: 'nickname',
        label: '昵称',
        showInDetail: true
    },
    {
        prop: 'genderDictName',
        label: '性别'
    },
    {
        prop: 'wechatNumber',
        label: '微信号'
    },
    {
        prop: 'height',
        label: '身高',
        showInDetail: true
    },
    {
        prop: 'weight',
        label: '体重',
        showInDetail: true
    },
    {
        prop: 'description',
        label: '个人简介',
        showInDetail: true
    },
    {
        prop: 'company',
        label: '公司'
    },
    {
        prop: 'profession',
        label: '职业'
    },
    {
        prop: 'position',
        label: '职位',
        showInDetail: true
    },
    {
        prop: 'college',
        label: '大学'
    },
    {
        prop: 'educationDictName',
        label: '学历'
    },
    {
        prop: 'academicDictName',
        label: '学位',
        showInDetail: true
    },
    {
        prop: 'major',
        label: '专业',
        showInDetail: true
    },
    {
        prop: 'cardNo',
        label: '汪汪队卡片号'
    },
    {
        prop: 'looksDictName',
        label: '相貌',
        showInDetail: true
    },
    {
        prop: 'mobile',
        label: '手机号',
        showInDetail: true
    },
    {
        prop: 'idCardNo',
        label: '证件号码',
        showInDetail: true
    },
    {
        prop: 'idTypeDictName',
        label: '证件类型',
        showInDetail: true
    },
    {
        prop: 'bloodTypeDictName',
        label: '血型',
        showInDetail: true
    },
    {
        prop: 'shapeDictName',
        label: '体型',
        showInDetail: true
    },
    {
        prop: 'standard',
        label: '择偶标准',
        showInDetail: true
    },
    {
        prop: 'smokingDictName',
        label: '抽烟',
        showInDetail: true
    },
    {
        prop: 'drinkingDictName',
        label: '喝酒',
        showInDetail: true
    },
    {
        prop: 'constellationDictName',
        label: '星座',
        showInDetail: true
    },
    {
        prop: 'monthSalary',
        label: '月薪',
        showInDetail: true
    },
    {
        prop: 'yearSalary',
        label: '年薪',
        showInDetail: true
    },
    {
        prop: 'maritalStatusDictName',
        label: '婚姻状况'
    },
    {
        prop: 'isHasCar',
        label: '是否有车'
    },
    {
        prop: 'carCity',
        label: '车牌归属城市'
    },
    {
        prop: 'isHasHourse',
        label: '是否有房'
    },
    {
        prop: 'hourseCity',
        label: '房所在城市'
    },
    {
        prop: 'birthDay',
        label: '生日',
        showInDetail: true
    },
    {
        prop: 'isVerified',
        label: '是否认证'
    },
    {
        prop: 'isShowInList',
        label: '是否展示到列表'
    },
    {
        prop: 'nowAreaName',
        label: '当前所在地'
    },
    {
        prop: 'homeAreaName',
        label: '家乡所在地'
    },
]
export default UserInfoTable