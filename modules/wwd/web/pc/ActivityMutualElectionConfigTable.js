const ActivityMutualElectionConfigTable = [
    {
        prop: 'activityTitle',
        label: '活动'
    },
    {
        prop: 'startAt',
        label: '互选开始时间'
    },
    {
        prop: 'endAt',
        label: '互选结束时间'
    },
    {
        prop: 'mutualElectionStatusDictName',
        label: '互选状态'
    },
    {
        prop: 'manageUserIds',
        label: '管理的用户'
    },
]
export default ActivityMutualElectionConfigTable