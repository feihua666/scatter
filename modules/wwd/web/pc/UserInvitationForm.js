const UserInvitationForm = [
    {
        field: {
            name: 'userId'
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'code'
        },
        element:{
            label: '邀请码',
            required: true,
        }
    },
    {
        field: {
            name: 'isUsed'
        },
        element:{
            label: '是否已使用',
            required: true,
        }
    },
    {
        field: {
            name: 'invitedUserId'
        },
        element:{
            label: '被邀请的用户',
        }
    },
]
export default UserInvitationForm