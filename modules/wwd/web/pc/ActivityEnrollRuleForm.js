import ActivityUrl from './ActivityUrl.js'
const ActivityEnrollRuleForm = [
    {
        ActivityEnrollRuleSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'activityId'
        },
        element:{
            type: 'select',
            label: '活动',
            required: true,
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'title', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按活动标题搜索',
                    remote: true,
                },
                remoteUrl: ActivityUrl.searchList,
                remoteQueryProp: 'title'
            }
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'startAt'
        },
        element:{
            type: 'datetime',
            label: '报名开始时间',
            options: {
                pickerOptions: {
                    disabledDate: (time) => {
                        return time.getTime() < Date.now();
                    }
                }
            },
            required: true
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'endAt'
        },
        element:{
            type: 'datetime',
            label: '报名结束时间',
            options: ({form})=> {
                return {
                    pickerOptions: {
                        disabledDate: (time) => {
                            return time.getTime() < (form.startAt ? new Date(form.startAt).getTime(): Date.now());
                        }
                    }
                }
            },
            required: true
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'refundDeadline'
        },
        element:{
            type: 'datetime',
            label: '退款截止时间',
            required: true
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'contact'
        },
        element:{
            label: '联系方式',
        }
    },
    {
        ActivityEnrollRuleSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'isRequireIdCard'
        },
        element:{
            type: 'select',
            options: {
                datas: 'yesNoBool'
            },
            label: '需要证件',
            required: true,
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'isHeadcountGenderSelf',
            value: false
        },
        element:{
            type: 'switch',
            label: '活动人数',
            options: {
                inactiveText: '固定',
                activeText: '自定义',
            }
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'headcount'
        },
        element:{
            type: 'inputNumber',
            label: '活动人数',
            required: ({form})=>{
                return !form.isHeadcountGenderSelf
            },
            show: ({form})=>{
                return !form.isHeadcountGenderSelf
            },
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'headcountMale'
        },
        element:{
            type: 'inputNumber',
            label: '活动人数/男',
            required: ({form})=>{
                return !!form.isHeadcountGenderSelf
            },
            show: ({form})=>{
                return !!form.isHeadcountGenderSelf
            },
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'headcountFemale'
        },
        element:{
            type: 'inputNumber',
            label: '活动人数/女',
            required: ({form})=>{
                return !!form.isHeadcountGenderSelf
            },
            show: ({form})=>{
                return !!form.isHeadcountGenderSelf
            },
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'headcountDesc'
        },
        element:{
            type: 'textarea',
            label: '人数说明',
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'malePrice'
        },
        element:{
            type: 'inputNumber',
            label: '价格/男',
            tips: ' 注：价格单位为分 '
        }
    },
    {
        ActivityEnrollRuleSearchList: false,
        field: {
            name: 'femalePrice'
        },
        element:{
            type: 'inputNumber',
            label: '价格/女',
            tips: ' 注：价格单位为分 '
        }
    },
    {
        field: {
            name: 'isNeedPayOnline'
        },
        element:{
            type: 'select',
            options: {
                datas: 'yesNoBool'
            },
            label: '线上支付',
        }
    },
]
export default ActivityEnrollRuleForm