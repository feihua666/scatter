import ActivityParticipateForm from './ActivityParticipateForm.js'
import ActivityParticipateTable from './ActivityParticipateTable.js'
import ActivityParticipateUrl from './ActivityParticipateUrl.js'

const ActivityParticipateMixin = {
    computed: {
        computedFormOptions() {
            // 回显远程搜索
            if (this.updateDatas) {
                // 编辑时活动回显
                ActivityParticipateForm[0].element.options.datas = [{id:this.updateDatas.activityId,title:this.updateDatas.activityTitle}]
                // 编辑时用户回显
                ActivityParticipateForm[1].element.options.datas = [{id:this.updateDatas.userId,nickname:this.updateDatas.userNickname}]
            }
            // 回显远程搜索，从活动跳转时回显
            if (this.$route.query.activityId && this.$route.query.activityTitle) {
                ActivityParticipateForm[0].element.options.datas = [{id:this.$route.query.activityId,title:this.$route.query.activityTitle}]
                ActivityParticipateForm[0].field.value = this.$route.query.activityId
            }
            return this.$stDynamicFormTools.options(ActivityParticipateForm,this.$options.name)
        },
        computedTableOptions() {
            return ActivityParticipateTable
        },
        computedUrl(){
            return ActivityParticipateUrl
        }
    },
    data(){
        return {
            updateDatas: null
        }
    },
    methods: {
        // update表单初始化回调
        updateDatasSuccess(resData) {
            this.updateDatas = resData
        }
    }
}
export default ActivityParticipateMixin