import UserInvitationUrl from './UserInvitationUrl.js'

const UserInvitationRoute = [
    {
        path: UserInvitationUrl.router.searchList,
        component: () => import('./element/UserInvitationSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserInvitationSearchList',
            name: '用户邀请管理'
        }
    },
    {
        path: UserInvitationUrl.router.add,
        component: () => import('./element/UserInvitationAdd'),
        meta: {
            code:'UserInvitationAdd',
            name: '用户邀请添加'
        }
    },
    {
        path: UserInvitationUrl.router.update,
        component: () => import('./element/UserInvitationUpdate'),
        meta: {
            code:'UserInvitationUpdate',
            name: '用户邀请修改'
        }
    },
]
export default UserInvitationRoute