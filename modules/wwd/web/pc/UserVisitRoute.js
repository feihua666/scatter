import UserVisitUrl from './UserVisitUrl.js'

const UserVisitRoute = [
    {
        path: UserVisitUrl.router.searchList,
        component: () => import('./element/UserVisitSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserVisitSearchList',
            name: '汪汪队用户访问管理'
        }
    },
    {
        path: UserVisitUrl.router.add,
        component: () => import('./element/UserVisitAdd'),
        meta: {
            code:'UserVisitAdd',
            name: '汪汪队用户访问添加'
        }
    },
    {
        path: UserVisitUrl.router.update,
        component: () => import('./element/UserVisitUpdate'),
        meta: {
            code:'UserVisitUpdate',
            name: '汪汪队用户访问修改'
        }
    },
]
export default UserVisitRoute