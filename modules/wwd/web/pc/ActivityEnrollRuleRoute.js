import ActivityEnrollRuleUrl from './ActivityEnrollRuleUrl.js'

const ActivityEnrollRuleRoute = [
    {
        path: ActivityEnrollRuleUrl.router.searchList,
        component: () => import('./element/ActivityEnrollRuleSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ActivityEnrollRuleSearchList',
            name: '活动报名规则管理'
        }
    },
    {
        path: ActivityEnrollRuleUrl.router.add,
        component: () => import('./element/ActivityEnrollRuleAdd'),
        meta: {
            code:'ActivityEnrollRuleAdd',
            name: '活动报名规则添加'
        }
    },
    {
        path: ActivityEnrollRuleUrl.router.update,
        component: () => import('./element/ActivityEnrollRuleUpdate'),
        meta: {
            code:'ActivityEnrollRuleUpdate',
            name: '活动报名规则修改'
        }
    },
]
export default ActivityEnrollRuleRoute