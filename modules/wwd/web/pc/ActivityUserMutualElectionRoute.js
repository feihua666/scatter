import ActivityUserMutualElectionUrl from './ActivityUserMutualElectionUrl.js'

const ActivityUserMutualElectionRoute = [
    {
        path: ActivityUserMutualElectionUrl.router.searchList,
        component: () => import('./element/ActivityUserMutualElectionSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ActivityUserMutualElectionSearchList',
            name: '汪汪队用户互选管理'
        }
    },
]
export default ActivityUserMutualElectionRoute