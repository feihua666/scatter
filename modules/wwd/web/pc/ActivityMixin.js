import ActivityForm from './ActivityForm.js'
import ActivityTable from './ActivityTable.js'
import ActivityUrl from './ActivityUrl.js'

const ActivityMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(ActivityForm,this.$options.name)
        },
        computedTableOptions() {
            return ActivityTable
        },
        computedUrl(){
            return ActivityUrl
        }
    },
    methods:{
        dataFormat(data,ext){
            if(ext.cityAreaId){
                data.provinceAreaId = ext.cityAreaId.parentId
            }
            return data
        }
    }
}
export default ActivityMixin