const basePath = '' + '/user-info'
const UserInfoUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    wxNumber: basePath + '/wxNumber',
    enjoyedWxNumber: basePath + '/enjoyed/wxNumber/{userId}',
    detail: basePath + '/{userId}/detail',
    recommend: basePath + '/recommend',
    getPageBySameCity: basePath + '/getPageBySameCity',
    updateSelfInfo: basePath + '/updateSelfInfo',
    userInfoSelf: basePath + '/userInfoSelf',
    router: {
        searchList: '/UserInfoSearchList',
        add: '/UserInfoAdd',
        update: '/UserInfoUpdate',
    }
}
export default UserInfoUrl