const UserTagTable = [
    {
        prop: 'userNickname',
        label: '用户'
    },
    {
        prop: 'typeDictName',
        label: '标签类型'
    },
    {
        prop: 'contentNames',
        label: '标签内容'
    },
    {
        prop: 'selfContent',
        label: '自定义标签内容'
    },
]
export default UserTagTable