const basePath = '' + '/user-invitation'
const UserInvitationUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserInvitationSearchList',
        add: '/UserInvitationAdd',
        update: '/UserInvitationUpdate',
    }
}
export default UserInvitationUrl