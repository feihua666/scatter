import ActivityUrl from "./ActivityUrl.js";
import UserInfoUrl from "./UserInfoUrl.js";
import UserSimpleUrl from "../../../../components/usersimple/web/pc/UserSimpleUrl.js";

const ActivityUserMutualElectionForm = [
    {
        ActivityUserMutualElectionSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'activityId'
        },
        element:{
            type: 'select',
            label: '活动',
            required: true,
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'title', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按活动标题搜索',
                    remote: true,
                },
                remoteUrl: ActivityUrl.searchList,
                remoteQueryProp: 'title'
            }
        }
    },
    {
        ActivityUserMutualElectionSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'userId'
        },
        element:{
            type: 'select',
            label: '用户',
            required: true,
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'nickname', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按昵称搜索',
                    remote: true,
                },
                remoteUrl: UserSimpleUrl.searchList,
                remoteQueryProp: 'nickname'
            }
        }
    },
    {
        ActivityUserMutualElectionSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'selectedUserId'
        },
        element:{
            type: 'select',
            label: '被选择用户',
            required: true,
            options: {
                optionProp:{
                    value: 'id', // 选中的值属性
                    label: 'nickname', // 显示的值属性
                },
                originProp: {
                    placeholder: '请按昵称搜索',
                    remote: true,
                },
                remoteUrl: UserSimpleUrl.searchList,
                remoteQueryProp: 'nickname'
            }
        }
    },
    {
        field: {
            name: 'level'
        },
        element:{
            label: '互选级别',
        }
    },
]
export default ActivityUserMutualElectionForm