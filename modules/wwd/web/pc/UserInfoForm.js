const UserInfoForm = [
    {
        UserInfoSearchList: false,
        field: {
            name: 'userId'
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        UserInfoSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'genderDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'gender'
            },
            label: '性别',
            required: true,
        }
    },
    {
        UserInfoSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '真实姓名',
            required: true,
        }
    },
    {
        UserInfoSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'nickname'
        },
        element:{
            label: '昵称',
            required: true,
        }
    },
    {
        UserInfoSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'wechatNumber'
        },
        element:{
            label: '微信号',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'height'
        },
        element:{
            label: '身高',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'weight'
        },
        element:{
            label: '体重',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'description'
        },
        element:{
            label: '个人简介',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'company'
        },
        element:{
            label: '公司',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'profession'
        },
        element:{
            label: '职业',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'position'
        },
        element:{
            label: '职位',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'college'
        },
        element:{
            label: '大学',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'educationDictId'
        },
        element:{
            label: '学历',
        }
    },
    {
        field: {
            name: 'academicDictId'
        },
        element:{
            label: '学位',
        }
    },
    {
        field: {
            name: 'major'
        },
        element:{
            label: '专业',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'cardNo'
        },
        element:{
            label: '汪汪队卡片号',
        }
    },
    {
        field: {
            name: 'looksDictId'
        },
        element:{
            label: '相貌',
        }
    },
    {
        field: {
            name: 'mobile'
        },
        element:{
            label: '手机号',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'idCardNo'
        },
        element:{
            label: '证件号码',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'idTypeDictId'
        },
        element:{
            label: '证件类型',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'bloodTypeDictId'
        },
        element:{
            label: '血型',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'shapeDictId'
        },
        element:{
            label: '体型',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'standard'
        },
        element:{
            label: '择偶标准',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'smokingDictId'
        },
        element:{
            label: '抽烟',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'drinkingDictId'
        },
        element:{
            label: '喝酒',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'constellationDictId'
        },
        element:{
            label: '星座',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'monthSalary'
        },
        element:{
            label: '月薪',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'yearSalary'
        },
        element:{
            label: '年薪',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'maritalStatusDictId'
        },
        element:{
            label: '婚姻状况',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'isHasCar'
        },
        element:{
            label: '是否有车',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'carCity'
        },
        element:{
            label: '车牌归属城市',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'isHasHourse'
        },
        element:{
            label: '是否有房',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'hourseCity'
        },
        element:{
            label: '房所在城市',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'birthDay'
        },
        element:{
            label: '生日',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'isVerified'
        },
        element:{
            label: '是否认证',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'isShowInList'
        },
        element:{
            label: '是否展示到列表',
            required: true,
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'nowAreaId'
        },
        element:{
            label: '当前所在地',
        }
    },
    {
        UserInfoSearchList: false,
        field: {
            name: 'homeAreaId'
        },
        element:{
            label: '家乡所在地',
        }
    },
]
export default UserInfoForm