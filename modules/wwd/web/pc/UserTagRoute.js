import UserTagUrl from './UserTagUrl.js'

const UserTagRoute = [
    {
        path: UserTagUrl.router.searchList,
        component: () => import('./element/UserTagSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserTagSearchList',
            name: '用户标签管理'
        }
    },
]
export default UserTagRoute