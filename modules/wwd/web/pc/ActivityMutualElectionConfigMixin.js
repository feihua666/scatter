import ActivityMutualElectionConfigForm from './ActivityMutualElectionConfigForm.js'
import ActivityMutualElectionConfigTable from './ActivityMutualElectionConfigTable.js'
import ActivityMutualElectionConfigUrl from './ActivityMutualElectionConfigUrl.js'
import ActivityEnrollRuleForm from "./ActivityEnrollRuleForm";

const ActivityMutualElectionConfigMixin = {
    computed: {
        computedFormOptions() {
            // 回显远程搜索
            if (this.$route.query.activityId && this.$route.query.activityTitle) {
                ActivityMutualElectionConfigForm[0].element.options.datas = [{id:this.$route.query.activityId,title:this.$route.query.activityTitle}]
                ActivityMutualElectionConfigForm[0].field.value = this.$route.query.activityId
            }
            return this.$stDynamicFormTools.options(ActivityMutualElectionConfigForm,this.$options.name)
        },
        computedTableOptions() {
            return ActivityMutualElectionConfigTable
        },
        computedUrl(){
            return ActivityMutualElectionConfigUrl
        }
    },
}
export default ActivityMutualElectionConfigMixin