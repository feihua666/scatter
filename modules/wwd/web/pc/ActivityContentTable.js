const ActivityContentTable = [
    {
        prop: 'activityId',
        label: '活动id'
    },
    {
        prop: 'content',
        label: '活动内容'
    },
]
export default ActivityContentTable