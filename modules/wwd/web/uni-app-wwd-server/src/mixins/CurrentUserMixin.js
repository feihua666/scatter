// 当前登录用户相关mixins
import IdentifierUserUrl from '../../../../../../components/identifier/web/pc/IdentifierUserUrl.js'
import {mapState} from 'vuex'

export default {
    computed: {
        ...mapState(['hasLogin'])
    },
    methods: {
        // 刷新用户登录状态
        refreshLoginStatus(callback){
            this.$http.get(IdentifierUserUrl.hasLogin)
                .then(res => {

                    this.refreshUserInfo(callback)

                })
                .catch(error=>{
                    // 401 代表未登录
                    if (error.statusCode == 401) {
                        // 设置未登录状态
                        this.$store.commit('SET_HAS_LOGIN',false)
                    }
                })
        },
        // 刷新用户信息
        refreshUserInfo(callback){
            this.$http.get(IdentifierUserUrl.loginUserinfo)
                .then(res => {
                    // 设置当前登录用户信息
                    this.$store.commit('SET_USER_INFO',res.data)
                    // 能返回数据，已登录
                    this.$store.commit('SET_HAS_LOGIN',true)

                    // 回调成功
                    if(callback){
                        callback()
                    }
                })
                .catch(error=>{
                    // 将用户信息设置为空
                    this.$store.commit('SET_USER_INFO',{})
                })
        }
    }
}