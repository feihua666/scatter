// 有意思mixin
import UserEnjoyUrl from '../../../pc/UserEnjoyUrl.js'
export default {
    methods:{
        // 有意思按钮
        //  userEnjoyStatuss 当前有意思状态
        // loginCallback 如果用户未登录，需要点击登录，点击登录时调用，因为在不同的页面逻辑不同，这里仅作为回调
        // buttonDesc 点击按钮的位置描述，用于记录，用于在什么地方点击同意登录或不同意登录
        // enjoySuccessCallback 有意思成功后回调
        enjoy (userEnjoyStatuss,loginCallback= ()=>{},buttonDesc,enjoySuccessCallback= ()=>{}) {
            let self = this

            // 如果用户已经登录直接可以点击
            if(this.hasLogin){
                console.log(userEnjoyStatuss)
                // 如果不能点有意思提示原因
                if(!userEnjoyStatuss.isCanEnjoy){
                    uni.showToast({
                        title: userEnjoyStatuss.errorMsgForIsCanEnjoyFalse,
                        icon:'none'
                    })
                    return
                }

                uni.showModal({
                    title: '有意思',
                    cancelText:'算了，再想想',
                    confirmText:'确定，告诉TA',
                    content: '每周只有7次使用该功能机会哦,若双方均“有意思”即可互看微信号',
                    showCancel: true,
                    success: (res) => {
                        if (res.confirm) {
                            self.doEnjoy(userEnjoyStatuss.userId,userEnjoyStatuss.enjoyedUserId,'',enjoySuccessCallback)
                        }
                    }
                })
            }else {
                // 如果用户尚未登录，提示用户登录
                uni.showModal({
                    cancelText:'暂不登录',
                    confirmText:'确定登录',
                    content: '您需要登录才可以哦',
                    showCancel: true,
                    success: (res) => {
                        if (res.confirm) {
                            // 点击确定，调用登录
                            // buttonDesc 这里埋点 todo
                            loginCallback()

                        }else {
                            // 埋点，用户点击了取消，这里可以是重点关注的用户为什么没有登录，发消息提醒？是否可以考虑
                            // buttonDesc 这里埋点 todo
                        }
                    }
                })
            }
        },
        // enjoy 请求
        doEnjoy(userId,enjoyedUserId,message,enjoySuccessCallback){
            let self = this
            // 发起对他有意思
            let postData = {
                userId: userId,
                enjoyedUserId: enjoyedUserId,
                message: message
            }
            self.$http.post(UserEnjoyUrl.add,postData).then(function (res) {
                uni.showToast({
                    title:'你的心动信号已发出 记得关注公众号信息等待对方回应哦',
                    icon:'none'
                })
                enjoySuccessCallback()
            }).catch( error =>{
                uni.showToast({
                    title:error.data.errorMsg,
                    icon:'none',
                    duration:2000
                })
            })
        }
    }
}