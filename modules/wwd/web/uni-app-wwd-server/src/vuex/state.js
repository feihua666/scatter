export default {
    // 是否登录加一个标识判断
    hasLogin: false,
    // 用户信息
    userInfo: {
        id: '',
        userId: '',
        name: '靠谱单身',
        nickname: '靠谱单身',
        gender: '男',
        genderDictId: '3',
        mobile: '16666666666',
        avatar: '../../static/image/login/logo.png',
        introduction: '靠谱单身'
    },
    showInList: false,
    isVerified: false
}
