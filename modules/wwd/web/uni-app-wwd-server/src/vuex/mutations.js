export default {
    // 设置登录状态
    SET_HAS_LOGIN(state, hasLogin) {
        state.hasLogin = hasLogin;
    },
    // 设置登录用户信息
    SET_USER_INFO: (state, userInfo) => {
        state.userInfo = userInfo
    },
    // 设置登录用户昵称
    SET_NICKNAME: (state, nickname) => {
        state.userInfo.nickname = nickname
    },
    // 设置
    SET_IS_VERIFIED: (state, isVerified) => {
        state.isVerified = isVerified
    },
    SET_SHOW_IN_LIST: (state, showList) => {
        state.isVerified = showList
    }
}
