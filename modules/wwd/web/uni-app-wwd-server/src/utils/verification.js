const startWith = function (str, appoint) {
    str = str.toLowerCase()  //不区分大小写：全部转为小写后进行判断
    let char = str.substr(0, appoint.length)//从0开始，往后截取特定字符长度

    if (char == appoint) { //两者相同，则代表验证通过
        return true
    }
    return false
}
const endWith = function (str, appoint) {
    str = str.toLowerCase()  //不区分大小写：全部转为小写后进行判断

    let start = str.length - appoint.length  //相差长度=字符串长度-特定字符长度
    let char = str.substr(start, appoint.length)//将相差长度作为开始下标，特定字符长度为截取长度

    if (char == appoint) { //两者相同，则代表验证通过
        return true
    }
    return false
}

const MOBILE_REG = '^(1[23456789]\\d{9})$'
const ID_CARD_REG = '/(^\\d{15}$)|(^\\d{17}(\\d|X|x)$)/'

const REG = function (type) {
    switch (type) {
        case 'mobile':
            return MOBILE_REG
        case 'id_card':
            return ID_CARD_REG
        default:
            return ''
    }
}

export default {
    REG: REG,
    startWith: startWith,
    endWith: endWith
}