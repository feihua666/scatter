import Vue from 'vue'
import App from './App'
import LuchRequest from '../../../../../components/common/web/components/app/uni-app/luch-request/LuchRequestPlugin.js'
import ToolsPlugin from '../../../../../components/common/web/tools/ToolsPlugin.js'
import utils from './utils/utils.js'
import store from './vuex/store'

import CurrentUserMixin from './mixins/CurrentUserMixin.js'
// 设置当前登录用户方法为全局mixin
Vue.mixin(CurrentUserMixin)
Vue.use(LuchRequest,{
    // baseURL: 'http://localhost:8080'
    baseURL: 'http://yw.ngrok.ahbdz.com'
    // baseURL: 'http://wwd.ngrok2.xiaomiqiu.cn'
})
// 自定义工具插件
Vue.use(ToolsPlugin)
Vue.config.productionTip = false
// 工具类
Vue.prototype.$utils = utils
// 配置信息
Vue.prototype.$config = {
    // 公众号配置编码，需要后台提供
    mpAppCode: 'ywPersonalTest',
    // 分享带标题所加 logo
    shareTitleLogo: '[靠谱单身]',
    // 用于图片拼接参数转换 结合 $stUrlTools.appendParam
    imageParam: {
        'x-oss-process': 'image/resize,h_528/auto-orient,1'
    }
}
App.mpType = 'app';

const app = new Vue({
    store,
    ...App
});
app.$mount();
