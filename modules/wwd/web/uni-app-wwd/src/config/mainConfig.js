import Vue from 'vue'
// http 请求库
import LuchRequest from '../../../../../../components/common/web/components/app/uni-app/luch-request/LuchRequestPlugin.js'
// scatter 通用工具
import ToolsPlugin from '../../../../../../components/common/web/tools/ToolsPlugin.js'
// scatter 对 uni-app 的通用工具
import ToolsUniPlugin from '../../../../../../components/common/web/components/app/uni-app/tools/ToolsPlugin.js'
import commonMixin from '../../../../../../components/common/web/components/app/uni-app/mixins/commonMixin.js'
// 全局存储
import store from '../vuex/store.js'
// 全局当前登录用户
import CurrentUserMixin from '../mixins/CurrentUserMixin.js'
import globalMixin from '../mixins/globalMixin.js'
import errorLoginMixin from '../../../../../../components/error-log/web/components/mixin/errorLoginMixin.js'

// 登录用户相关
import IdentifierUserUrl from '../../../../../../components/identifier/web/pc/IdentifierUserUrl.js'
// vuex 增强 ******
import vuexEnhancePlugin from '../../../../../../components/common/web/vuex/vuexEnhancePlugin.js'
// scater 自定义css 全局样式
import '../../../../../../components/common/web/st.css'
import '../../../../../../components/common/web/components/app/uni-app/st-u.css'
// 安装
Vue.use(vuexEnhancePlugin,{
    store,
    localSaveStateKeys: []
})
// vuex 增强 结束******

// 设置当前登录用户方法为全局mixin
Vue.mixin(CurrentUserMixin)
Vue.mixin(commonMixin)
Vue.mixin(errorLoginMixin)
Vue.mixin(globalMixin)

Vue.use(LuchRequest,{
    baseURL: 'http://yw.ngrok.ahbdz.com',
    // baseURL: 'http://localhost:8082'
})
// 自定义工具插件
Vue.use(ToolsPlugin)
Vue.use(ToolsUniPlugin)

// 配置信息
Vue.prototype.$config = {
    hasLoginUrl: IdentifierUserUrl.hasLogin,
    userInfoUrl: IdentifierUserUrl.loginUserinfo,
    // 公众号配置编码，需要后台提供
    mpAppCode: 'ywPersonalTest',
    // 分享带标题所加 logo
    shareTitleLogo: '[靠谱单身]',
    // 用于图片拼接参数转换 结合 $stUrlTools.appendParam
    imageParam: {
        'x-oss-process': 'image/resize,h_528/auto-orient,1'
    },
    // 动态分类
    dynamicGroupFlag: 'wwd_dynamic',
    // 活动相关分类
    activityGroupFlag: 'wwd_activity',
    // 认证编码
    identi:{
        // 实名认证
        realName: 'real_name'
    }
}

// 将 store 导出
export {store}