import Vue from 'vue'
import App from './App'
// 自定义配置
import {store} from './config/mainConfig.js'
// uView-ui
import uView from "uview-ui";
Vue.use(uView);
// 下面这行创建项目的时候自带，暂时不知道作用
Vue.config.productionTip = false
// 下面这行创建项目的时候自带，不能去掉
App.mpType = 'app'
// vue
const app = new Vue({
  store,
  ...App
})
app.$mount()
