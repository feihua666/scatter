export default {
    methods: {
        // 跳转到用户详情
        navigateToUserDetail(userId){
            uni.navigateTo({
                url: '/pages/index/userDetail?userId=' + userId
            })
        },
        replyClick(commentSubject){
            this.checkLogin().then(()=>{
                let q = {
                    targetUserId: commentSubject.ownerUserId,
                    subjectId: commentSubject.id
                }
                uni.navigateTo({
                    url: '/pages/activity/activityCommentHierarchicalForCommentPublish' + this.$u.queryParams(q)
                })
            })
        },
        toAllStar(commentSubject){

            uni.navigateTo({
                url: '/pages/activity/activityCommentStar?commentSubjectId=' + commentSubject.id
            })
        },
        // 点击全部回复事件，跳转到回复详情页，这和活动的单条活动的单个评论回复跳转页面一致
        toAllReply(commentSubject){
            let query = {activityId: commentSubject.subjectId,commentId: commentSubject.id}
            // 为了方便传参，这里使用 uview封装的路由跳转
            this.$u.route({
                url: '/pages/activity/activityCommentReplyDetail',
                params: query
            })
        },
        // 点击头像跳转到用户详情
        avatarClick(commentSubject){
            this.navigateToUserDetail(commentSubject.ownerUserId)
        },
        // 点赞头像点击事件
        starAvatarClick({starItem}){
            this.navigateToUserDetail(starItem.ownerUserId)
        },
        // 点击层级盖楼评论头像
        commentHierarchicalAvatarClick({hierarchicalItem}){
            this.navigateToUserDetail(hierarchicalItem.ownerUserId)
        },
        commentReplyClick({commentSubject,hierarchicalItem}){
            let query = {commentHierarchicalId: hierarchicalItem.id}
            // 为了方便传参，这里使用 uview封装的路由跳转
            this.$u.route({
                url: '/pages/activity/activityCommentHierarchicalReplyDetail',
                params: query
            })
        }
    }
}