import CommentStarUrl from '../../../../../../../components/comment/web/pc/star/CommentStarUrl.js'
export default {
    methods: {
        // 点赞或取消
        starOrCancel(activityDetail){
            if (activityDetail.isHasStared) {
                // 最后面的参数 true 表示立即执行
                this.$u.debounce(()=>{this.doCancelStar(activityDetail)}, 800,true)
            }else {
                this.$u.debounce(()=>{this.doStar(activityDetail)}, 800,true)
            }
        },
        // 点赞发起请求
        doStar(activityDetail){
            this.checkLogin().then(()=>{
                let data = {
                    subjectId: activityDetail.id,
                    groupFlag: this.groupFlag
                }
                this.$http.post(CommentStarUrl.addStar,data).then((res)=>{
                    activityDetail.isHasStared = true
                    activityDetail.starCount += 1
                    if (activityDetail.latestCommentStars) {
                        activityDetail.latestCommentStars.records.unshift(res.data)
                    }
                    uni.$emit('activity_starIncrement',{activityId: this.activityId,num: 1})

                })
            })
        },
        // 取消点赞发起请求
        doCancelStar(activityDetail){
            this.checkLogin().then(()=>{
                let data = {
                    subjectId: activityDetail.id,
                    groupFlag: this.groupFlag
                }
                this.$http.post(CommentStarUrl.cancelStar,data).then(()=>{
                    activityDetail.isHasStared = false
                    activityDetail.starCount -= 1

                    if (activityDetail.latestCommentStars) {
                        for (let i = 0; i < activityDetail.latestCommentStars.records.length; i++) {
                            if(activityDetail.latestCommentStars.records[i].ownerUserId == this.vuex_userInfo.id){
                                activityDetail.latestCommentStars.records.splice(i,1)
                            }
                        }
                    }
                    uni.$emit('activity_starIncrement',{activityId: this.activityId,num: -1})

                })
            })
        },
    }
}