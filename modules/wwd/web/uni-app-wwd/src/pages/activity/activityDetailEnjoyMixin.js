import CommentEnjoyUrl from '../../../../../../../components/comment/web/pc/enjoy/CommentEnjoyUrl.js'
export default {
    methods: {
        // 点赞或取消
        enjoyOrCancel(activityDetail){
            if (activityDetail.isHasEnjoyed) {
                // 最后面的参数 true 表示立即执行
                this.$u.debounce(()=>{this.doCancelEnjoy(activityDetail)}, 800,true)
            }else {
                this.$u.debounce(()=>{this.doEnjoy(activityDetail)}, 800,true)
            }
        },
        // 点赞发起请求
        doEnjoy(activityDetail){
            this.checkLogin().then(()=>{
                let data = {
                    subjectId: activityDetail.id,
                    groupFlag: this.groupFlag
                }
                this.$http.post(CommentEnjoyUrl.addEnjoy,data).then((res)=>{
                    activityDetail.isHasEnjoyed = true
                    activityDetail.enjoyCount += 1
                    if (activityDetail.latestCommentEnjoys) {
                        activityDetail.latestCommentEnjoys.records.unshift(res.data)
                    }
                    uni.$emit('activity_enjoyIncrement',{activityId: this.activityId,num: 1})

                })
            })
        },
        // 取消点赞发起请求
        doCancelEnjoy(activityDetail){
            this.checkLogin().then(()=>{
                let data = {
                    subjectId: activityDetail.id,
                    groupFlag: this.groupFlag
                }
                this.$http.post(CommentEnjoyUrl.cancelEnjoy,data).then(()=>{
                    activityDetail.isHasEnjoyed = false
                    activityDetail.enjoyCount -= 1
                    if (activityDetail.latestCommentEnjoys) {
                        for (let i = 0; i < activityDetail.latestCommentEnjoys.records.length; i++) {
                            if(activityDetail.latestCommentEnjoys.records[i].ownerUserId == this.vuex_userInfo.id){
                                activityDetail.latestCommentEnjoys.records.splice(i,1)
                            }
                        }
                    }
                    uni.$emit('activity_enjoyIncrement',{activityId: this.activityId,num: -1})

                })
            })
        },
    }
}