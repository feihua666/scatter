// vuex 增强
import {mutationsStore,mutationName} from '../../../../../../components/common/web/vuex/vuexEnhancePlugin.js'
/**
 * 所有方法名大写，并使用 SET 作为前缀，以下划线分隔
 */
export default {
    // 设置登录状态
    SET_HAS_LOGIN(state, hasLogin) {
        state.vuex_hasLogin = hasLogin;
    },
    // 设置登录用户信息
    SET_USER_INFO: (state, userInfo) => {
        state.vuex_userInfo = userInfo
    },
    // 设置登录用户昵称
    SET_NICKNAME: (state, nickname) => {
        state.vuex_userInfo.nickname = nickname
    },
    // 名字要和 mixin配置一致
    [mutationName]: mutationsStore
}
