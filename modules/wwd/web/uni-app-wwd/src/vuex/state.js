/**
 * 推荐属性添加 vues 前缀，这样在使用的时候知道变量是从哪来的，也为避免冲突
 */
export default {
    // 是否登录
    vuex_hasLogin: false,
    // 用户信息
    vuex_userInfo: {
    },
}
