// 当前登录用户相关mixins
export default {
    methods: {

        /**
         * 提供一个检查登录用户的 promise 方法，方便统一用户未登录时处理引导登录情况
         * @returns {Promise<unknown>}
         */
        checkLogin(defaultActtion = true){
            return new Promise((resolve,reject)=> {
                if (this.vuex_hasLogin) {
                    resolve(true)
                }else {
                    reject(false)
                    if(defaultActtion){
                        uni.navigateTo({
                            url: '/pages/login/index',
                            animationType: 'pop-in',
                            animationDuration: 200
                        })
                    }
                }
            })
        },
        // 刷新用户登录状态
        async refreshLoginStatus(callback){
            await this.$http.get(this.$config.hasLoginUrl)
                .then(res => {
                    this.refreshUserInfo(callback)
                })
                .catch(error=>{
                    // 401 代表未登录
                    if (error.statusCode == 401) {
                        // 设置未登录状态
                        this.$store.commit('SET_HAS_LOGIN',false)
                    }
                })
        },
        // 刷新用户信息
        async refreshUserInfo(callback){
            await  this.$http.get(this.$config.userInfoUrl)
                .then(res => {
                    // 设置当前登录用户信息
                    this.$store.commit('SET_USER_INFO',res.data)
                    // 能返回数据，已登录
                    this.$store.commit('SET_HAS_LOGIN',true)

                    // 回调成功
                    if(callback){
                        callback()
                    }
                })
                .catch(error=>{
                    // 将用户信息设置为空
                    this.$store.commit('SET_USER_INFO',{})
                })
        }
    }
}