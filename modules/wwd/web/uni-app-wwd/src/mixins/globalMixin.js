export default {
    data(){
        return {
            // 性别转换
            gender: {
                f: 'woman',
                m: 'man'
            },
        }
    },
    methods:{
        // 全局，图片加参数，用来缩小图片访问流量，提高访问速度
        imgUrl(url){
            let r = url
            if(r){
                r = this.$stUrlTools.appendParam(r,this.$config.imageParam)
            }
            return r
        },
        // 布尔转中文解释
        booleanText(flag){
            if (flag === true) {
                return '有'
            }
            return '没有'
        }
    }
}