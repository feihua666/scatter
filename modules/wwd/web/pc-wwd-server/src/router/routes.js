import AreaRoute from '../../../../../../components/area/web/pc/AreaRoute.js'
import DictRoute from '../../../../../../components/dict/web/pc/DictRoute.js'
import FuncRoute from '../../../../../../components/func/web/pc/FuncRoute.js'
import RoleRoutesEntry from '../../../../../../components/role/web/pc/RoleRoutesEntry.js'
import UserSimpleRoute from '../../../../../../components/usersimple/web/pc/UserSimpleRoute.js'
import FuncGroupRoute from '../../../../../../components/func/web/pc/FuncGroupRoute.js'
import FileRoute from '../../../../../../components/file/web/pc/FileRoute.js'
import FileNeedDeleteRoute from '../../../../../../components/file/web/pc/FileNeedDeleteRoute.js'

import WxMpRoutesEntry from '../../../../../../components/wx-mp/web/pc/WxMpRoutesEntry.js'

import ActivityEnrollRuleRoute from '../../../pc/ActivityEnrollRuleRoute.js'
import ActivityRoute from '../../../pc/ActivityRoute.js'
import ActivityParticipateRoute from '../../../pc/ActivityParticipateRoute.js'
import ActivityUserMutualElectionRoute from '../../../pc/ActivityUserMutualElectionRoute.js'
import ActivityMutualElectionConfigRoute from '../../../pc/ActivityMutualElectionConfigRoute.js'
import UserInfoRoute from '../../../pc/UserInfoRoute.js'
import UserTagRoute from '../../../pc/UserTagRoute.js'
import BannerRoute from '../../../../../../components/banner/web/pc/BannerRoute'
import ErrorLogRoute from '../../../../../../components/error-log/web/pc/ErrorLogRoute'
import ScheduleRoutesEntry from '../../../../../../components/schedule-quartz/web/pc/ScheduleRoutesEntry'

import OrderRoute from '../../../../../../components/order/web/pc/OrderRoute'
import OrderGoodsRoute from '../../../../../../components/order/web/pc/OrderGoodsRoute.js'
import OrderRefundRoute from '../../../../../../components/order/web/pc/OrderRefundRoute.js'
import OrderRefundGoodsRoute from '../../../../../../components/order/web/pc/OrderRefundGoodsRoute.js'

import PayRoute from '../../../../../../components/pay/web/pc/PayRoute.js'
import OrderWxRoute from '../../../../../../components/pay/web/pc/wx/OrderWxRoute.js'
import IdentifierRoutesEntry from '../../../../../../components/identifier/web/pc/IdentifierRoutesEntry.js'

import PortraitItemRoutesEntry from '../../../../../../components/portrait/web/pc/item/PortraitItemRoutesEntry.js'
import PortraitUserRoutesEntry from '../../../../../../components/portrait/web/pc/user/PortraitUserRoutesEntry.js'
// 富文本
import RichTextRoute from '../../../../../../components/richtext/web/pc/richtext/RichTextRoute.js'
// 敏感词
import SensitiveWordRoute from '../../../../../../components/sensitive/web/pc/word/SensitiveWordRoute.js'
//意见反馈
import FeedbackRoute from '../../../../../../components/feedback/web/pc/FeedbackRoute.js'
// 举报投诉
import ComplaintRoute from '../../../../../../components/complaint/web/pc/ComplaintRoute.js'
// 消息
import MessageRoutesEntry from '../../../../../../components/message/web/pc/MessageRoutesEntry.js'
// 认证
import IdentificationRoute from '../../../../../../components/identification/web/pc/IdentificationRoute.js'

let indexChildren = [].concat(AreaRoute)
.concat(DictRoute)
.concat(FuncRoute)
.concat(RoleRoutesEntry)
.concat(UserSimpleRoute)
.concat(FuncGroupRoute)
.concat(FileRoute)
.concat(FileNeedDeleteRoute)

.concat(WxMpRoutesEntry)


.concat(ActivityEnrollRuleRoute)
.concat(ActivityRoute)
.concat(ActivityParticipateRoute)
.concat(ActivityUserMutualElectionRoute)
.concat(ActivityMutualElectionConfigRoute)
.concat(UserInfoRoute)
.concat(UserTagRoute)
.concat(BannerRoute)
.concat(ErrorLogRoute)

.concat(ScheduleRoutesEntry)
.concat(OrderRoute)
.concat(OrderRefundRoute)
.concat(OrderGoodsRoute)
.concat(OrderRefundGoodsRoute)

.concat(PayRoute)
.concat(OrderWxRoute)
.concat(IdentifierRoutesEntry)

.concat(PortraitItemRoutesEntry)
.concat(PortraitUserRoutesEntry)

.concat(RichTextRoute)
.concat(SensitiveWordRoute)
.concat(FeedbackRoute)
.concat(ComplaintRoute)

.concat(MessageRoutesEntry)
.concat(IdentificationRoute)

let routes = [
    {
        path: '/login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/index',
        component: () => import('../views/Index.vue'),
        children: indexChildren
    },
]

export default routes