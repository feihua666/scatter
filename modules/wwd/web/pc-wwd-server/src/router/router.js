import VueRouter from 'vue-router'
import routes from './routes'

const router = new VueRouter({
    routes: routes
})
// 路由守卫
router.beforeEach((to, from, next) => {
    if (!to.meta) {
        to.meta = {}
    }
    if (from.meta) {
        if( from.meta.nextKeepAlive === false){
            to.meta.keepAlive = false
        }else if( from.meta.nextKeepAlive === true){
            to.meta.keepAlive = true
        }
    }
    next();
})
export default router