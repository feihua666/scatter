let packageName = require('./package.json').name
module.exports = {
    lintOnSave: false,
    devServer: {
        disableHostCheck: true,
        port: 8081,
        proxy: 'http://localhost:8080'
    }

}