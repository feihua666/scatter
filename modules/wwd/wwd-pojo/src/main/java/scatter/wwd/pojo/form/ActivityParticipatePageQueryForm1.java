package scatter.wwd.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;

import javax.validation.constraints.NotEmpty;


/**
 * <p>
 * 活动参与分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动参与分页表单对象")
public class ActivityParticipatePageQueryForm1 extends BasePageQueryForm {


    @NotEmpty(message="活动ID不能为空")
    @ApiModelProperty(value = "活动ID",required = true)
    private String activityId;
}
