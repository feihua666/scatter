package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户标签更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户标签更新表单对象")
public class UserTagUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="标签类型不能为空")
    @ApiModelProperty(value = "标签类型，字典，兴趣，性格，食物，旅游等",required = true)
    private String typeDictId;

    @NotEmpty(message="对应标签的内容字典编码不能为空")
    @ApiModelProperty(value = "对应标签的内容字典编码，多个以逗号分隔",required = true)
    private String content;

    @ApiModelProperty(value = "对应标签的自定义内容，文本，直接展示")
    private String selfContent;

}
