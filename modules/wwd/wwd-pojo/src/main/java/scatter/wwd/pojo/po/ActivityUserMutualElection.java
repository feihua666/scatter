package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 汪汪队用户互选
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_activity_user_mutual_election")
@ApiModel(value="ActivityUserMutualElection对象", description="汪汪队用户互选")
public class ActivityUserMutualElection extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ACTIVITYUSERMUTUALELECTION_BY_ID = "trans_activityusermutualelection_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "微信模块的用户表用户ID，")
    private String userId;

    @ApiModelProperty(value = "微信模块的用户表，选择的用户ID")
    private String selectedUserId;

    @ApiModelProperty(value = "互选级别：A-B-C...")
    private String level;


}
