package scatter.wwd.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseUpdateIdForm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 活动更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Setter
@Getter
@ApiModel(value="活动更新表单对象")
public class ActivityUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="活动标题不能为空")
    @ApiModelProperty(value = "活动标题,模糊查询",required = true)
    private String title;

    @ApiModelProperty(value = "标题图")
    private String titleUrl;

    @ApiModelProperty(value = "所在省ID")
    private String provinceAreaId;

    @ApiModelProperty(value = "所在城市ID")
    private String cityAreaId;

    @ApiModelProperty(value = "活动简介,模糊查询")
    private String introduced;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "主办方链接")
    private String sponsorLink;

    @ApiModelProperty(value = "作者,模糊查询")
    private String author;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "活动地点,模糊查询")
    private String address;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @NotEmpty(message="场地类型(室内、户外)不能为空")
    @ApiModelProperty(value = "场地类型(室内、户外)，字典",required = true)
    private String placeTypeDictId;

    @ApiModelProperty(value = "是否发布")
    private Boolean isPublish;

    @ApiModelProperty(value = "活动声明,模糊查询")
    private String activityStatement;

    @NotNull(message="序号不能为空")
    @ApiModelProperty(value = "序号，从小到大排序",required = true)
    private Integer seq;

    @NotEmpty(message="活动内容不能为空")
    @ApiModelProperty(value = "活动内容",required = true)
    private String content;
}
