package scatter.wwd.pojo;

import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;

/**
 * 汪汪队常量
 * Created by yangwei
 * Created at 2018/7/24 19:13
 */
public class WwdConstants {
    /**
     * 动态分组标识
     */
    public static final String GROUP_FLAG_DYNAMIC = "wwd_dynamic";
    /**
     * 活动点赞分组标识
     */
    public static final String GROUP_FLAG_ACTIVITY_STAR = "wwd_activity";
    /**
     * 活动想去分组标识
     */
    public static final String GROUP_FLAG_ACTIVITY_ENJOY = "wwd_activity";
    /**
     * 活动评论分组标识
     */
    public static final String GROUP_FLAG_ACTIVITY_COMMENT = "wwd_activity";
    /**
     * 活动评分分组标识
     */
    public static final String GROUP_FLAG_ACTIVITY_RATE = "wwd_activity";

    /**
     * 每一个人最多评10分
     */
    public static final int activity_rate_score = 10;

    // 汪汪队图片类型
    public enum WwdUserPicType {
        main,      // 主图
        normal     //非主图
    }
    /**
     * 互选状态
     */
    public enum MutualElectionStatus {
        no_start,
        ongoing,
        ended
    }
    /**
     * 报名人数规则
     */
    public enum HeadCountRule {
        unlimited,      // 不限制男女
        custom     //限制男女人数
    }
    public enum VisitTypeDictGroup implements IDictGroup{
        wwd_visit_type;

        @Override
        public String groupCode() {
            return this.name();
        }
    }
    public enum VisitTypeDictItem implements IDictItem{
        list, //列表
        timeline, //朋友圈
        groupmessage,//微信群
        singlemessage//好友分享
        ;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return VisitTypeDictGroup.wwd_visit_type.groupCode();
        }
    }
}

