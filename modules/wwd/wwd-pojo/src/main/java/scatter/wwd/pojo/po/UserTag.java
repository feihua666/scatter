package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户标签表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_tag")
@ApiModel(value="UserTag对象", description="用户标签表")
public class UserTag extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERTAG_BY_ID = "trans_usertag_by_id_scatter.wwd.pojo.po";
    public static final String TRANS_USERTAGS_VO_BY_USER_ID = "trans_usertags_vo_by_user_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "标签类型，字典，兴趣，性格，食物，旅游等")
    private String typeDictId;

    @ApiModelProperty(value = "对应标签的内容字典编码，多个以逗号分隔")
    private String content;

    @ApiModelProperty(value = "对应标签的自定义内容，文本，直接展示")
    private String selfContent;


}
