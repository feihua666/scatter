package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.wwd.pojo.po.Activity;


/**
 * <p>
 * 活动参与响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动参与响应对象")
public class ActivityParticipateVo extends BaseIdVo {

    @ApiModelProperty(value = "活动ID")
    private String activityId;

    @TransBy(type = Activity.TRANS_ACTIVITY_BY_ID,byFieldName = "activityId",mapValueField = "title")
    @ApiModelProperty(value = "活动标题")
    private String activityTitle;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "nickname")
    @ApiModelProperty(value = "用户昵称")
    private String userNickname;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "avatar")
    @ApiModelProperty(value = "用户头像")
    private String userAvatar;

    @ApiModelProperty(value = "用户性别字典id")
    private String userGenderDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "userGenderDictId",mapValueField = "value")
    @ApiModelProperty(value = "用户性别值")
    private String userGenderDictValue;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "userGenderDictId",mapValueField = "name")
    @ApiModelProperty(value = "用户性别名称")
    private String userGenderDictName;

    @ApiModelProperty(value = "是否是领队")
    private Boolean isLeader;

    @ApiModelProperty(value = "是否退出")
    private Boolean isQuit;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "报名人员姓名")
    private String name;

    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "idTypeDictId",mapValueField = "name")
    @ApiModelProperty(value = "证件类型，字典名称")
    private String idTypeDictName;

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;
}
