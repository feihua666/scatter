package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户卡片分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户卡片分页表单对象")
public class UserCardPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "图片url")
    private String picUrl;

    @ApiModelProperty(value = "缩略图url")
    private String picThumbUrl;

    @ApiModelProperty(value = "描述")
    private String description;

}
