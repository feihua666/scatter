package scatter.wwd.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseUpdateIdForm;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * <p>
 * 用户信息更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户信息更新表单对象")
public class UserInfoUpdateForm1 extends BaseUpdateIdForm {

    @ApiModelProperty(value = "用户id",hidden = true)
    private String userId;

    @ApiModelProperty(value = "性别字典")
    private String genderDictId;

    @ApiModelProperty(value = "真实姓名")
    private String name;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "微信号")
    private String wechatNumber;

    @ApiModelProperty(value = "身高")
    private String height;

    @ApiModelProperty(value = "体重")
    private String weight;

    @ApiModelProperty(value = "个人简介")
    private String description;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "职业")
    private String profession;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "大学")
    private String college;

    @ApiModelProperty(value = "学历，字典")
    private String educationDictId;

    @ApiModelProperty(value = "学位，字典")
    private String academicDictId;

    @ApiModelProperty(value = "专业")
    private String major;

    @ApiModelProperty(value = "汪汪队卡片号")
    private String cardNo;

    @ApiModelProperty(value = "相貌，字典：一般，帅，漂亮等")
    private String looksDictId;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "证件号码")
    private String idCardNo;

    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

    @ApiModelProperty(value = "血型，字典")
    private String bloodTypeDictId;

    @ApiModelProperty(value = "体型，字典：苗条，偏胖，偏瘦等")
    private String shapeDictId;

    @NotEmpty(message="择偶标准不能为空")
    @ApiModelProperty(value = "择偶标准",required = true)
    private String standard;

    @NotEmpty(message="抽烟不能为空")
    @ApiModelProperty(value = "抽烟，字典",required = true)
    private String smokingDictId;

    @NotEmpty(message="喝酒不能为空")
    @ApiModelProperty(value = "喝酒，字典",required = true)
    private String drinkingDictId;


    @ApiModelProperty(value = "月薪")
    private String monthSalary;

    @ApiModelProperty(value = "年薪")
    private String yearSalary;

    @ApiModelProperty(value = "婚姻状况，字典")
    private String maritalStatusDictId;

    @ApiModelProperty(value = "是否有车")
    private Boolean isHasCar;

    @ApiModelProperty(value = "车牌归属城市，如果有车，车牌归属哪，如：京牌")
    private String carCity;

    @ApiModelProperty(value = "是否有房")
    private Boolean isHasHourse;

    @ApiModelProperty(value = "房所在城市，如果有房，在哪，如：北京")
    private String hourseCity;

    @ApiModelProperty(value = "生日")
    private LocalDate birthDay;

    @ApiModelProperty(value = "是否展示到列表")
    private Boolean isShowInList;

    @ApiModelProperty(value = "当前所在地")
    private String nowAreaId;

    @ApiModelProperty(value = "家乡所在地")
    private String homeAreaId;

}
