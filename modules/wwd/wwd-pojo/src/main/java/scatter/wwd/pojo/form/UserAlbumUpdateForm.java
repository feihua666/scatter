package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户相册更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户相册更新表单对象")
public class UserAlbumUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="图片url不能为空")
    @ApiModelProperty(value = "图片url",required = true)
    private String picUrl;

    @NotEmpty(message="缩略图url不能为空")
    @ApiModelProperty(value = "缩略图url",required = true)
    private String picThumbUrl;

    @ApiModelProperty(value = "描述")
    private String description;

}
