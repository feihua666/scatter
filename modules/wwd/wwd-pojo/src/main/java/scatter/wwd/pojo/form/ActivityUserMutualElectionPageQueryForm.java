package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 汪汪队用户互选分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="汪汪队用户互选分页表单对象")
public class ActivityUserMutualElectionPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "微信模块的用户表用户ID，")
    private String userId;

    @ApiModelProperty(value = "微信模块的用户表，选择的用户ID")
    private String selectedUserId;

    @ApiModelProperty(value = "互选级别：A-B-C...")
    private String level;

}
