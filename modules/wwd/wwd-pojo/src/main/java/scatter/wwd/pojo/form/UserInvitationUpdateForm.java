package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户邀请更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户邀请更新表单对象")
public class UserInvitationUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="邀请码不能为空")
    @ApiModelProperty(value = "邀请码",required = true)
    private String code;

    @NotNull(message="是否已使用不能为空")
    @ApiModelProperty(value = "是否已使用",required = true)
    private Boolean isUsed;

    @ApiModelProperty(value = "被邀请的用户")
    private String invitedUserId;

}
