package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.wwd.pojo.po.ActivityContent;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-10 21:26
 */
@Setter
@Getter
public class ActivityWithContentVo extends ActivityVo{

	@TransBy(type = ActivityContent.TRANS_ACTIVITYCONTENT_BY_ACTIVITY_ID,byFieldName = "id",mapValueField = "content")
	@ApiModelProperty("活动内容")
	private String content;
}
