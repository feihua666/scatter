package scatter.wwd.pojo.form.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Description
 * @Date 6/17/21 5:09 PM
 * @Created by ciaj.
 */
@Setter
@Getter
@ApiModel(value="卡片生成表单对象")
public class GenerateCardForm {

    @NotEmpty(message="appCode不能为空")
    @ApiModelProperty(value = "appCode",required = true)
    private String appCode;

    @NotEmpty(message="主图不能为空")
    @ApiModelProperty(value = "主图",required = true)
    private String mainPicSelectedId;

    @NotEmpty(message="附图不能为空")
    @ApiModelProperty(value = "附图",required = true)
    private List<String> normalPicSelectedIds;
}
