package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDate;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户信息更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户信息更新表单对象")
public class UserInfoUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="性别字典不能为空")
    @ApiModelProperty(value = "性别字典",required = true)
    private String genderDictId;

    @NotEmpty(message="真实姓名不能为空")
    @ApiModelProperty(value = "真实姓名",required = true)
    private String name;

    @NotEmpty(message="昵称不能为空")
    @ApiModelProperty(value = "昵称",required = true)
    private String nickname;

    @ApiModelProperty(value = "微信号")
    private String wechatNumber;

    @NotEmpty(message="身高不能为空")
    @ApiModelProperty(value = "身高",required = true)
    private String height;

    @NotEmpty(message="体重不能为空")
    @ApiModelProperty(value = "体重",required = true)
    private String weight;

    @NotEmpty(message="个人简介不能为空")
    @ApiModelProperty(value = "个人简介",required = true)
    private String description;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "职业")
    private String profession;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "大学")
    private String college;

    @ApiModelProperty(value = "学历，字典")
    private String educationDictId;

    @ApiModelProperty(value = "学位，字典")
    private String academicDictId;

    @ApiModelProperty(value = "专业")
    private String major;

    @ApiModelProperty(value = "汪汪队卡片号")
    private String cardNo;

    @ApiModelProperty(value = "相貌，字典：一般，帅，漂亮等")
    private String looksDictId;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "证件号码")
    private String idCardNo;

    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

    @ApiModelProperty(value = "血型，字典")
    private String bloodTypeDictId;

    @ApiModelProperty(value = "体型，字典：苗条，偏胖，偏瘦等")
    private String shapeDictId;

    @NotEmpty(message="择偶标准不能为空")
    @ApiModelProperty(value = "择偶标准",required = true)
    private String standard;

    @NotEmpty(message="抽烟不能为空")
    @ApiModelProperty(value = "抽烟，字典",required = true)
    private String smokingDictId;

    @NotEmpty(message="喝酒不能为空")
    @ApiModelProperty(value = "喝酒，字典",required = true)
    private String drinkingDictId;

    @NotEmpty(message="星座不能为空")
    @ApiModelProperty(value = "星座，字典",required = true)
    private String constellationDictId;

    @ApiModelProperty(value = "月薪")
    private String monthSalary;

    @ApiModelProperty(value = "年薪")
    private String yearSalary;

    @NotEmpty(message="婚姻状况不能为空")
    @ApiModelProperty(value = "婚姻状况，字典",required = true)
    private String maritalStatusDictId;

    @NotNull(message="是否有车不能为空")
    @ApiModelProperty(value = "是否有车",required = true)
    private Boolean isHasCar;

    @ApiModelProperty(value = "车牌归属城市，如果有车，车牌归属哪，如：京牌")
    private String carCity;

    @NotNull(message="是否有房不能为空")
    @ApiModelProperty(value = "是否有房",required = true)
    private Boolean isHasHourse;

    @ApiModelProperty(value = "房所在城市，如果有房，在哪，如：北京")
    private String hourseCity;

    @ApiModelProperty(value = "生日")
    private LocalDate birthDay;

    //@NotNull(message="是否认证不能为空")
    @ApiModelProperty(value = "是否认证",required = true)
    private Boolean isVerified;

    @NotNull(message="是否展示到列表不能为空")
    @ApiModelProperty(value = "是否展示到列表",required = true)
    private Boolean isShowInList;

    @ApiModelProperty(value = "当前所在地")
    private String nowAreaId;

    @ApiModelProperty(value = "家乡所在地")
    private String homeAreaId;

}
