package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户卡片表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_card")
@ApiModel(value="UserCard对象", description="用户卡片表")
public class UserCard extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERCARD_BY_ID = "trans_usercard_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "图片url")
    private String picUrl;

    @ApiModelProperty(value = "缩略图url")
    private String picThumbUrl;

    @ApiModelProperty(value = "描述")
    private String description;


}
