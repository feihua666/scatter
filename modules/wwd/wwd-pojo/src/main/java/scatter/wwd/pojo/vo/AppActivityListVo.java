package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;

/**
 * <p>
 * app活动列表vo
 * </p>
 *
 * @author yangwei
 * @since 2021-09-06 10:01
 */
@Setter
@Getter
@ApiModel(value="活动列表响应对象")
public class AppActivityListVo extends BaseIdVo {

	@ApiModelProperty(value = "活动标题,模糊查询")
	private String title;

	@ApiModelProperty(value = "标题图")
	private String titleUrl;

	@ApiModelProperty(value = "活动简介,模糊查询")
	private String introduced;

	@ApiModelProperty(value = "已报名人数")
	private Integer participateTotal;

	@ApiModelProperty(value = "限制报名人数")
	private Integer participateLimitTotal;

	@ApiModelProperty(value = "活动规则")
	private ActivityEnrollRuleVo ruleVo;

	@ApiModelProperty(value = "是否报名")
	private Boolean isParticipate;

	@ApiModelProperty(value = "订单号")
	private String orderNo;

}
