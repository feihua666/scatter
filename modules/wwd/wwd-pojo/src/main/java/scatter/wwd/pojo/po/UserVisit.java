package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * <p>
 * 汪汪队用户访问
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_visit")
@ApiModel(value="UserVisit对象", description="汪汪队用户访问")
public class UserVisit extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERVISIT_BY_ID = "trans_uservisit_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "访问的用户ID")
    private String userId;

    @ApiModelProperty(value = "被访问的用户ID")
    private String visitUserId;

    @ApiModelProperty(value = "访问类型,例个人详情是从列表进，或分享（群，朋友圈等）进来，list=列表,timeline=朋友圈,groupmessage=微信群,singlemessage=好友分享")
    private String visitTypeDictId;

    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

    @ApiModelProperty(value = "读取，查看时间")
    private LocalDateTime readAt;


}
