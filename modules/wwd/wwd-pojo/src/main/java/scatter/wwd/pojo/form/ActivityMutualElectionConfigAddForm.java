package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 活动互选配置添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动互选配置添加表单对象")
public class ActivityMutualElectionConfigAddForm extends BaseAddForm {

    @NotEmpty(message="活动id不能为空")
    @ApiModelProperty(value = "活动id",required = true)
    private String activityId;

    @ApiModelProperty(value = "互选开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "互选结束时间")
    private LocalDateTime endAt;

    @NotEmpty(message="互选状态不能为空")
    @ApiModelProperty(value = "互选状态，未开始，进行中，已结束",required = true)
    private String mutualElectionStatusDictId;

    @ApiModelProperty(value = "管理的用户id，多个以逗号分隔，人员可以进行h5活动管理，目前是为控制互选而生")
    private String manageUserIds;

}
