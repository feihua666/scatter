package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动表
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_activity")
@ApiModel(value="Activity对象", description="活动表")
public class Activity extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ACTIVITY_BY_ID = "trans_activity_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "活动标题,模糊查询")
    private String title;

    @ApiModelProperty(value = "标题图")
    private String titleUrl;

    @ApiModelProperty(value = "所在省ID")
    private String provinceAreaId;

    @ApiModelProperty(value = "所在城市ID")
    private String cityAreaId;

    @ApiModelProperty(value = "活动简介,模糊查询")
    private String introduced;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "主办方链接")
    private String sponsorLink;

    @ApiModelProperty(value = "作者,模糊查询")
    private String author;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "活动地点,模糊查询")
    private String address;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "场地类型(室内、户外)，字典")
    private String placeTypeDictId;

    @ApiModelProperty(value = "是否发布")
    private Boolean isPublish;

    @ApiModelProperty(value = "活动声明,模糊查询")
    private String activityStatement;

    @ApiModelProperty(value = "序号，从小到大排序")
    private Integer seq;


    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "点赞数")
    private Integer starCount;

    @ApiModelProperty(value = "想去数")
    private Integer enjoyCount;

    @ApiModelProperty(value = "评分人数")
    private Integer rateCount;

    @ApiModelProperty(value = "评分总数")
    private Integer rateSum;

    @ApiModelProperty(value = "评分得分，计算方法：总评分数/(评分人数*10)")
    private Double rateScore;

}
