package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动内容表
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_activity_content")
@ApiModel(value="ActivityContent对象", description="活动内容表")
public class ActivityContent extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ACTIVITYCONTENT_BY_ID = "trans_activitycontent_by_id_scatter.wwd.pojo.po";
    public static final String TRANS_ACTIVITYCONTENT_BY_ACTIVITY_ID = "trans_activitycontent_by_activity_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "活动内容")
    private String content;


}
