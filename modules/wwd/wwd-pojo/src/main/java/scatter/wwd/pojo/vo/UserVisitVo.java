package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.wwd.pojo.po.UserInfo;

import java.time.LocalDateTime;


/**
 * <p>
 * 汪汪队用户访问响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="汪汪队用户访问响应对象")
public class UserVisitVo extends BaseIdVo {

    @ApiModelProperty(value = "访问的用户ID")
    private String userId;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "nickname")
    @ApiModelProperty(value = "访问的用户昵称")
    private String nickname;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "avatar")
    @ApiModelProperty(value = "访问的用户头像")
    private String avatar;

    @TransBy(type = UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID,byFieldName = "userId",mapValueField = "genderDictName")
    @ApiModelProperty(value = " 访问的用户性别")
    private String gender;

    @ApiModelProperty(value = "时间")
    private LocalDateTime createAt;

    @ApiModelProperty(value = "被访问的用户ID")
    private String visitUserId;

    @TransBy(type = UserInfo.TRANS_USERINFO_BY_USER_ID,byFieldName = "visitUserId",mapValueField = "nickname")
    @ApiModelProperty(value = "被访问的用户昵称")
    private String visitNickname;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "visitUserId",mapValueField = "avatar")
    @ApiModelProperty(value = "被访问的用户头像")
    private String visitAvatar;

    @TransBy(type = UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID,byFieldName = "visitUserId",mapValueField = "genderDictName")
    @ApiModelProperty(value = "被访问的用户性别")
    private String visitGender;

    @ApiModelProperty(value = "访问类型,例个人详情是从列表进，或分享（群，朋友圈等）进来，list=列表,timeline=朋友圈,groupmessage=微信群,singlemessage=好友分享")
    private String visitTypeDictId;

    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

    @ApiModelProperty(value = "读取，查看时间")
    private LocalDateTime readAt;

}
