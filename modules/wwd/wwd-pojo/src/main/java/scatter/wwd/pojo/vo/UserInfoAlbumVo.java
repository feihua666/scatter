package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author: wzn
 * @desc:
 * @date: 2021/9/12
 */
@Setter
@Getter
@ApiModel(value="APP用户相册编辑响应对象")
public class UserInfoAlbumVo {
    @ApiModelProperty(value = "用户主图")
    private List<UserAlbumVo> main;

    @ApiModelProperty(value = "用户相册")
    private List<UserAlbumVo> normal;

}
