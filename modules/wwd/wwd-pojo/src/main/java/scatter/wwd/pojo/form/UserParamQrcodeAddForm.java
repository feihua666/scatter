package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户标签添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户标签添加表单对象")
public class UserParamQrcodeAddForm extends BaseAddForm {

    @NotEmpty(message="微信用户id不能为空")
    @ApiModelProperty(value = "微信用户id",required = true)
    private String userId;

    @NotEmpty(message="二维码tiket不能为空")
    @ApiModelProperty(value = "二维码tiket",required = true)
    private String ticket;

    @ApiModelProperty(value = "过期时长，秒")
    private Integer expireSeconds;

    @NotEmpty(message="二维码图片内容不能为空")
    @ApiModelProperty(value = "二维码图片内容",required = true)
    private String content;

    @ApiModelProperty(value = "二维码图片地址")
    private String url;

    @NotNull(message="是否永久不能为空")
    @ApiModelProperty(value = "是否永久",required = true)
    private Boolean isPermanent;

}
