package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.wwd.pojo.po.UserInfo;

import java.time.LocalDateTime;


/**
 * <p>
 * 用户邀请响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户邀请响应对象")
public class UserInvitationVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "邀请码")
    private String code;

    @ApiModelProperty(value = "是否已使用")
    private Boolean isUsed;

    @ApiModelProperty(value = "被邀请的用户")
    private String invitedUserId;

    @TransBy(type = UserInfo.TRANS_USERINFO_BY_USER_ID,byFieldName = "invitedUserId",mapValueField = "nickname")
    @ApiModelProperty(value = "被邀请的用户昵称")
    private String nickname;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "invitedUserId",mapValueField = "avatar")
    @ApiModelProperty(value = "被邀请的用户头像")
    private String avatar;

    @TransBy(type = UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID,byFieldName = "invitedUserId",mapValueField = "genderDictName")
    @ApiModelProperty(value = "被邀请的用户性别")
    private String gender;

    @ApiModelProperty(value = "时间")
    private LocalDateTime createAt;

}
