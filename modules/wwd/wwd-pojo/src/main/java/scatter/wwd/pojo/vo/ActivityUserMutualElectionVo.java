package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.wwd.pojo.po.Activity;


/**
 * <p>
 * 汪汪队用户互选响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="汪汪队用户互选响应对象")
public class ActivityUserMutualElectionVo extends BaseIdVo {

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @TransBy(type = Activity.TRANS_ACTIVITY_BY_ID,byFieldName = "activityId",mapValueField = "title")
    @ApiModelProperty(value = "活动标题")
    private String activityTitle;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "nickname")
    @ApiModelProperty(value = "用户昵称")
    private String userNickname;

    @ApiModelProperty(value = "选择的用户ID")
    private String selectedUserId;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "selectedUserId",mapValueField = "nickname")
    @ApiModelProperty(value = "选择的用户昵称")
    private String selectedUserNickname;

    @ApiModelProperty(value = "互选级别：A-B-C...")
    private String level;

}
