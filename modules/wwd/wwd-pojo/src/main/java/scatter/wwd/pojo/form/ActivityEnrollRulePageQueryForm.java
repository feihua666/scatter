package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * <p>
 * 活动报名规则分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动报名规则分页表单对象")
public class ActivityEnrollRulePageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "报名开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "报名结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "1=需要证件，身份证 0=不需要，这在报名的时候会判断添加")
    private Boolean isRequireIdCard;

    @ApiModelProperty(value = "是否需要线上支付")
    private Boolean isNeedPayOnline;

}
