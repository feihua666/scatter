package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 活动参与分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动参与分页表单对象")
public class ActivityParticipatePageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "活动ID")
    private String activityId;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "是否是领队")
    private Boolean isLeader;

    @ApiModelProperty(value = "是否退出")
    private Boolean isQuit;

    @ApiModelProperty(value = "备注，退款。。。")
    private String remarks;

    @ApiModelProperty(value = "报名人员姓名")
    private String name;

    @ApiModelProperty(value = "报名人员手机号")
    private String mobile;

    @ApiModelProperty(value = "证件号码")
    private String idCardNo;

    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

}
