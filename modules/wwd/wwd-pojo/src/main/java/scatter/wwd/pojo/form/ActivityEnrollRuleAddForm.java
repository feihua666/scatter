package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 活动报名规则添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动报名规则添加表单对象")
public class ActivityEnrollRuleAddForm extends BaseAddForm {

    @NotEmpty(message="活动id不能为空")
    @ApiModelProperty(value = "活动id",required = true)
    private String activityId;

    @ApiModelProperty(value = "报名开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "报名结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "报名联系方式")
    private String contact;

    @NotNull(message="1=需要证件不能为空")
    @ApiModelProperty(value = "1=需要证件，身份证 0=不需要，这在报名的时候会判断添加",required = true)
    private Boolean isRequireIdCard;

    @ApiModelProperty(value = "活动人数")
    private Integer headcount;

    @ApiModelProperty(value = "活动人数，男")
    private Integer headcountMale;

    @ApiModelProperty(value = "活动人数，女")
    private Integer headcountFemale;

    @ApiModelProperty(value = "人数说明")
    private String headcountDesc;

    @ApiModelProperty(value = "男女活动人数是否按性别自定义")
    private Boolean isHeadcountGenderSelf;

    @ApiModelProperty(value = "男价格，单位分")
    private Integer malePrice;

    @ApiModelProperty(value = "女价格，单位分")
    private Integer femalePrice;

    @ApiModelProperty(value = "是否需要线上支付")
    private Boolean isNeedPayOnline;


    @ApiModelProperty(value = "退款截止时间")
    private LocalDateTime refundDeadline;
}
