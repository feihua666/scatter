package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 活动互选配置表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_activity_mutual_election_config")
@ApiModel(value="ActivityMutualElectionConfig对象", description="活动互选配置表")
public class ActivityMutualElectionConfig extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ACTIVITYMUTUALELECTIONCONFIG_BY_ID = "trans_activitymutualelectionconfig_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "互选开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "互选结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "互选状态，未开始，进行中，已结束")
    private String mutualElectionStatusDictId;

    @ApiModelProperty(value = "管理的用户id，多个以逗号分隔，人员可以进行h5活动管理，目前是为控制互选而生")
    private String manageUserIds;


}
