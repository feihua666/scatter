package scatter.wwd.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户卡片响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户卡片响应对象")
public class UserCardVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "图片url")
    private String picUrl;

    @ApiModelProperty(value = "缩略图url")
    private String picThumbUrl;

    @ApiModelProperty(value = "描述")
    private String description;

}
