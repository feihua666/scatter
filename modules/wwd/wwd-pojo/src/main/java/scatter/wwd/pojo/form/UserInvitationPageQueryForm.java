package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户邀请分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户邀请分页表单对象")
public class UserInvitationPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "邀请码")
    private String code;

    @ApiModelProperty(value = "是否已使用")
    private Boolean isUsed;

    @ApiModelProperty(value = "被邀请的用户")
    private String invitedUserId;

}
