package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.TransField;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.pojo.po.UserTag;

import java.util.List;


/**
 * <p>
 * 用户信息响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户信息详情响应对象")
public class UserInfoDetailVo extends UserInfoVo {



    @TransBy(type = UserAlbum.TRANS_USERALBUMS_VO_BY_USER_ID ,byFieldName = "userId",isJoin = false)
    @ApiModelProperty(value = "用户相册")
    private List<UserAlbumVo> userAlbums;


    @TransField
    @TransBy(type = UserTag.TRANS_USERTAGS_VO_BY_USER_ID ,byFieldName = "userId",isJoin = false)
    @ApiModelProperty(value = "用户标签")
    List<UserTagVo> userTags;


    /**
     * 用户有意思状态，如果没有登录则无数据
     */
    @ApiModelProperty(value = "用户有意思状态")
    private UserEnjoyStatusVo userEnjoyStatus;
}
