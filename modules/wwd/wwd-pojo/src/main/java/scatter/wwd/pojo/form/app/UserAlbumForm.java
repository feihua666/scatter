package scatter.wwd.pojo.form.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * @Description
 * @Date 6/12/21 4:01 PM
 * @Created by ciaj.
 */
@Setter
@Getter
@ApiModel(value="用户相册表单对象")
public class UserAlbumForm {

    @NotEmpty(message="图片地址错误")
    @ApiModelProperty(value = "图片地址",required = true)
    private String url;
    @NotEmpty(message="图片类型错误")
    @ApiModelProperty(value = "图片类型",required = true)
    private String type;
    @ApiModelProperty(value = "排序")
    private Integer seq = 10;

}
