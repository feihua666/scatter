package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.Ne;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.LocalTime;


/**
 * <p>
 * 汪汪队用户访问分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="汪汪队用户访问分页表单对象")
public class UserVisitPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "访问的用户ID")
    private String userId;


    @ApiModelProperty(value = "被访问的用户ID")
    private String visitUserId;


    @ApiModelProperty(value = "访问类型,例个人详情是从列表进，或分享（群，朋友圈等）进来，list=列表,timeline=朋友圈,groupmessage=微信群,singlemessage=好友分享")
    private String visitTypeDictId;

    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

    @ApiModelProperty(value = "读取，查看时间")
    private LocalDateTime readAt;

}
