package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 活动报名规则表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_activity_enroll_rule")
@ApiModel(value="ActivityEnrollRule对象", description="活动报名规则表")
public class ActivityEnrollRule extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ACTIVITYENROLLRULE_BY_ID = "trans_activityenrollrule_by_id_scatter.wwd.pojo.po";
    public static final String TRANS_ACTIVITYENROLLRULE_BY_ACTIVITY_ID = "trans_activityenrollrule_by_activity_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "报名开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "报名结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "报名联系方式")
    private String contact;

    @ApiModelProperty(value = "1=需要证件，身份证 0=不需要，这在报名的时候会判断添加")
    private Boolean isRequireIdCard;

    @ApiModelProperty(value = "活动人数")
    private Integer headcount;

    @ApiModelProperty(value = "活动人数，男")
    private Integer headcountMale;

    @ApiModelProperty(value = "活动人数，女")
    private Integer headcountFemale;

    @ApiModelProperty(value = "人数说明")
    private String headcountDesc;

    @ApiModelProperty(value = "男女活动人数是否按性别自定义")
    private Boolean isHeadcountGenderSelf;

    @ApiModelProperty(value = "男价格，单位分")
    private Integer malePrice;

    @ApiModelProperty(value = "女价格，单位分")
    private Integer femalePrice;

    @ApiModelProperty(value = "是否需要线上支付")
    private Boolean isNeedPayOnline;

    @ApiModelProperty(value = "已报名活动-已支付人数，男")
    private Integer countMale;

    @ApiModelProperty(value = "已报名活动-已支付人数，女")
    private Integer countFemale;

    @ApiModelProperty(value = "退款截止时间")
    private LocalDateTime refundDeadline;
}
