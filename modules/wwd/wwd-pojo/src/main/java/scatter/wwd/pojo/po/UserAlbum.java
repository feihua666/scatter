package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户相册表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_album")
@ApiModel(value="UserAlbum对象", description="用户相册表")
public class UserAlbum extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERALBUM_BY_ID = "trans_useralbum_by_id_scatter.wwd.pojo.po";
    public static final String TRANS_USERALBUMS_VO_BY_USER_ID = "trans_useralbums_vo_by_user_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "图片url")
    private String picUrl;

    @ApiModelProperty(value = "缩略图url")
    private String picThumbUrl;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "序号，从小到大排序")
    private Integer seq;

}
