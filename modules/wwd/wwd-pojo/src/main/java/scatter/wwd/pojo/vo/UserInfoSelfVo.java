package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 个人用户信息响应对象
 * </p>
 *
 * @author yangwei
 * @since 2021-11-08 14:02
 */
@Setter
@Getter
@ApiModel(value="个人用户信息响应对象")
public class UserInfoSelfVo extends UserInfoVo{
}
