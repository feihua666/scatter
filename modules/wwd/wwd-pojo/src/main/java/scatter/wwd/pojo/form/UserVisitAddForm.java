package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

/**
 * <p>
 * 汪汪队用户访问添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="汪汪队用户访问添加表单对象")
public class UserVisitAddForm extends BaseAddForm {

    @ApiModelProperty(value = "访问的用户ID,请不要传值，后台取登录用户的id",hidden = true)
    private String userId;

    @NotEmpty(message="被访问的用户ID不能为空")
    @ApiModelProperty(value = "被访问的用户ID",required = true)
    private String visitUserId;

    @NotEmpty(message="访问类型不能为空")
    @ApiModelProperty(value = "访问类型,例个人详情是从列表进，或分享（群，朋友圈等）进来，list=列表,timeline=朋友圈,groupmessage=微信群,singlemessage=好友分享",required = true)
    private String visitTypeDictValue;
}
