package scatter.wwd.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户标签响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户标签响应对象")
public class UserParamQrcodeVo extends BaseIdVo {

    @ApiModelProperty(value = "微信用户id")
    private String userId;

    @ApiModelProperty(value = "二维码tiket")
    private String ticket;

    @ApiModelProperty(value = "过期时长，秒")
    private Integer expireSeconds;

    @ApiModelProperty(value = "二维码图片内容")
    private String content;

    @ApiModelProperty(value = "二维码图片地址")
    private String url;

    @ApiModelProperty(value = "是否永久")
    private Boolean isPermanent;

}
