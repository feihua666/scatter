package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 活动参与
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_activity_participate")
@ApiModel(value="ActivityParticipate对象", description="活动参与")
public class ActivityParticipate extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ACTIVITYPARTICIPATE_BY_ID = "trans_activityparticipate_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "活动ID")
    private String activityId;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "是否是领队")
    private Boolean isLeader;

    @ApiModelProperty(value = "是否退出")
    private Boolean isQuit;

    @ApiModelProperty(value = "备注，退款。。。")
    private String remarks;

    @ApiModelProperty(value = "报名人员姓名")
    private String name;

    @ApiModelProperty(value = "报名人员手机号")
    private String mobile;

    @ApiModelProperty(value = "证件号码")
    private String idCardNo;

    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;
}
