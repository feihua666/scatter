package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户标签表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_param_qrcode")
@ApiModel(value="UserParamQrcode对象", description="用户标签表")
public class UserParamQrcode extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERPARAMQRCODE_BY_ID = "trans_userparamqrcode_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "微信用户id")
    private String userId;

    @ApiModelProperty(value = "二维码tiket")
    private String ticket;

    @ApiModelProperty(value = "过期时长，秒")
    private Integer expireSeconds;

    @ApiModelProperty(value = "二维码图片内容")
    private String content;

    @ApiModelProperty(value = "二维码图片地址")
    private String url;

    @ApiModelProperty(value = "是否永久")
    private Boolean isPermanent;


}
