package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 活动分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Setter
@Getter
@ApiModel(value="活动分页表单对象")
public class ActivityPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "活动标题,模糊查询")
    private String title;

    @ApiModelProperty(value = "标题图")
    private String titleUrl;

    @ApiModelProperty(value = "所在省ID")
    private String provinceAreaId;

    @ApiModelProperty(value = "所在城市ID")
    private String cityAreaId;


    @Like
    @ApiModelProperty(value = "活动简介,模糊查询")
    private String introduced;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "主办方链接")
    private String sponsorLink;

    @Like
    @ApiModelProperty(value = "作者,模糊查询")
    private String author;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @Like
    @ApiModelProperty(value = "活动地点,模糊查询")
    private String address;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "场地类型(室内、户外)，字典")
    private String placeTypeDictId;

    @ApiModelProperty(value = "是否发布")
    private Boolean isPublish;

    @Like
    @ApiModelProperty(value = "活动声明,模糊查询")
    private String activityStatement;

    @OrderBy(asc = false)
    @ApiModelProperty(value = "序号，从小到大排序")
    private Integer seq;

    @ApiModelProperty(value = "返回结果包括规则信息")
    private Boolean includeRule = false;

}
