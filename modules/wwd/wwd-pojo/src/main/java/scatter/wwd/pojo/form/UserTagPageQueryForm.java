package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户标签分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户标签分页表单对象")
public class UserTagPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "标签类型，字典，兴趣，性格，食物，旅游等")
    private String typeDictId;

    @Like
    @ApiModelProperty(value = "对应标签的内容字典id")
    private String content;

    @ApiModelProperty(value = "对应标签的自定义内容，文本，直接展示")
    private String selfContent;

}
