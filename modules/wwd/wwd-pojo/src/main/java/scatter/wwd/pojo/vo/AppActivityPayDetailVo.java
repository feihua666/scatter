package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;

import java.time.LocalDateTime;

/**
 * <p>
 * 活动支付确认详情
 * </p>
 *
 * @author yangwei
 * @since 2021-09-07 21:32
 */
@Setter
@Getter
@ApiModel(value = "活动支付确认详情")
public class AppActivityPayDetailVo extends BaseIdVo {

	@ApiModelProperty(value = "活动标题")
	private String title;

	@ApiModelProperty(value = "活动头图")
	private String titleUrl;

	@ApiModelProperty(value = "活动费用，单位分")
	private Integer totalFee;

	@ApiModelProperty(value = "活动地址")
	private String address;

	@ApiModelProperty(value = "活动开始时间")
	private LocalDateTime startTime;

	@ApiModelProperty(value = "活动结束时间")
	private LocalDateTime endTime;

	@ApiModelProperty(value = "活动线上或线下支付")
	private Boolean isNeedPayOnline;
}
