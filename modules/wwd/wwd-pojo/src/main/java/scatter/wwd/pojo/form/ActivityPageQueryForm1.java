package scatter.wwd.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;

import java.time.LocalDateTime;


/**
 * <p>
 * 活动分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Setter
@Getter
@ApiModel(value="活动分页表单对象")
public class ActivityPageQueryForm1 extends BasePageQueryForm {

    /**
     * groupCode-value格式
     * 可以为空
     */
    @ApiModelProperty(value = "场地类型(室内、户外)，字典")
    private String placeTypeDictGroupCodeAndValue;

    @ApiModelProperty(value = "是否同城")
    private Boolean isSameCity;

}
