package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户邀请表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_invitation")
@ApiModel(value="UserInvitation对象", description="用户邀请表")
public class UserInvitation extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERINVITATION_BY_ID = "trans_userinvitation_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "邀请码")
    private String code;

    @ApiModelProperty(value = "是否已使用")
    private Boolean isUsed;

    @ApiModelProperty(value = "被邀请的用户")
    private String invitedUserId;


}
