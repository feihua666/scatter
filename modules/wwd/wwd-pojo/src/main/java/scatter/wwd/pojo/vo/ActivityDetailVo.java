package scatter.wwd.pojo.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.comment.pojo.enjoy.vo.CommentEnjoyVo;
import scatter.comment.pojo.star.vo.CommentStarVo;
import scatter.common.rest.trans.TransField;

import java.util.List;

/**
 * <p>
 * app活动详情响应数据对象
 * </p>
 *
 * @author yangwei
 * @since 2021-09-06 21:57
 */
@ApiModel("app活动详情响应数据对象")
@Setter
@Getter
public class ActivityDetailVo extends ActivityVo {

	@ApiModelProperty("活动内容")
	private String activityContent;


	@ApiModelProperty(value = "是否参与",notes = "当前登录用户是否报名")
	private Boolean isParticipate;

	@TransField
	@ApiModelProperty("最新的几个参与人员")
	private IPage<ActivityParticipateVo> latestParticipates;

	@ApiModelProperty("最新的几个点赞")
	private IPage<CommentStarVo> latestCommentStars;

	@ApiModelProperty("最新的几个想去")
	private IPage<CommentEnjoyVo> latestCommentEnjoys;


	@TransField
	@ApiModelProperty("报名规则")
	private ActivityEnrollRuleVo enrollRule;

	@ApiModelProperty(value = "是否点赞",notes = "当前登录用户是否点赞")
	private Boolean isHasStared;

	@ApiModelProperty(value = "是否想去",notes = "当前登录用户是否想去")
	private Boolean isHasEnjoyed;

	@ApiModelProperty(value = "是否评分",notes = "当前登录用户是否打分，只有已添加人员，才能参与打分")
	private Boolean isHasRated;
}
