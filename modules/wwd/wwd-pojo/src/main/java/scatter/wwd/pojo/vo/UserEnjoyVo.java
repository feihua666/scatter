package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.wwd.pojo.po.UserInfo;

import java.time.LocalDateTime;


/**
 * <p>
 * 用户有意思响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户有意思响应对象")
public class UserEnjoyVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @TransBy(type = UserInfo.TRANS_USERINFO_BY_USER_ID,byFieldName = "userId",mapValueField = "nickname")
    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @TransBy(type = UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID,byFieldName = "userId",mapValueField = "genderDictName")
    @ApiModelProperty(value = "用户性别")
    private String gender;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "avatar")
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "有意思用户id")
    private String enjoyedUserId;

    @TransBy(type = UserInfo.TRANS_USERINFO_BY_USER_ID,byFieldName = "enjoyedUserId",mapValueField = "nickname")
    @ApiModelProperty(value = "有意思用户昵称")
    private String enjoyedNickname;

    @TransBy(type = UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID,byFieldName = "enjoyedUserId",mapValueField = "genderDictName")
    @ApiModelProperty(value = "有意思用户性别")
    private String enjoyedGender;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "enjoyedUserId",mapValueField = "avatar")
    @ApiModelProperty(value = "有意思用户头像")
    private String enjoyedAvatar;

    @ApiModelProperty(value = "添加有意思时，可以写一句话")
    private String message;

    @ApiModelProperty(value = "时间")
    private LocalDateTime createAt;

}
