package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.area.pojo.po.Area;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.usersimple.pojo.po.UserSimple;

import java.time.LocalDate;


/**
 * <p>
 * 用户信息响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户信息响应对象")
public class UserInfoVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "avatar")
    @ApiModelProperty(value = "用户头像")
    private String userAvatar;

    @ApiModelProperty(value = "图片url")
    private String picUrl;

    @ApiModelProperty(value = "性别字典id")
    private String genderDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "genderDictId",mapValueField = "value")
    @ApiModelProperty(value = "性别字典值")
    private String genderDictValue;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "genderDictId",mapValueField = "name")
    @ApiModelProperty(value = "性别字典名称")
    private String genderDictName;

    @ApiModelProperty(value = "真实姓名")
    private String name;

    @ApiModelProperty(value = "昵称")
    private String nickname;


    @ApiModelProperty(value = "身高")
    private String height;

    @ApiModelProperty(value = "体重")
    private String weight;

    @ApiModelProperty(value = "个人简介")
    private String description;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "职业")
    private String profession;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "大学")
    private String college;

    @ApiModelProperty(value = "学历，字典")
    private String educationDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "educationDictId",mapValueField = "name")
    @ApiModelProperty(value = "学历，字典名称")
    private String educationDictName;

    @ApiModelProperty(value = "学位，字典")
    private String academicDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "academicDictId",mapValueField = "name")
    @ApiModelProperty(value = "学位，字典名称")
    private String academicDictName;

    @ApiModelProperty(value = "专业")
    private String major;

    @ApiModelProperty(value = "汪汪队卡片号")
    private String cardNo;

    @ApiModelProperty(value = "相貌，字典：一般，帅，漂亮等")
    private String looksDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "looksDictId",mapValueField = "name")
    @ApiModelProperty(value = "相貌，字典名称")
    private String looksDictName;


    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "idTypeDictId",mapValueField = "name")
    @ApiModelProperty(value = "证件类型名称")
    private String idTypeDictName;

    @ApiModelProperty(value = "血型，字典")
    private String bloodTypeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "bloodTypeDictId",mapValueField = "name")
    @ApiModelProperty(value = "血型，字典名称")
    private String bloodTypeDictName;

    @ApiModelProperty(value = "体型，字典：苗条，偏胖，偏瘦等")
    private String shapeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "shapeDictId",mapValueField = "name")
    @ApiModelProperty(value = "体型，字典名称")
    private String shapeDictName;

    @ApiModelProperty(value = "择偶标准")
    private String standard;

    @ApiModelProperty(value = "抽烟，字典")
    private String smokingDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "smokingDictId",mapValueField = "name")
    @ApiModelProperty(value = "抽烟，字典名称")
    private String smokingDictName;

    @ApiModelProperty(value = "喝酒，字典")
    private String drinkingDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "drinkingDictId",mapValueField = "name")
    @ApiModelProperty(value = "喝酒，字典名称")
    private String drinkingDictName;

    @ApiModelProperty(value = "星座，字典")
    private String constellationDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "constellationDictId",mapValueField = "name")
    @ApiModelProperty(value = "星座，字典名称")
    private String constellationDictName;

    @ApiModelProperty(value = "月薪")
    private String monthSalary;

    @ApiModelProperty(value = "年薪")
    private String yearSalary;

    @ApiModelProperty(value = "婚姻状况，字典")
    private String maritalStatusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "maritalStatusDictId",mapValueField = "name")
    @ApiModelProperty(value = "婚姻状况，字典名称")
    private String maritalStatusDictName;

    @ApiModelProperty(value = "是否有车")
    private Boolean isHasCar;

    @ApiModelProperty(value = "车牌归属城市，如果有车，车牌归属哪，如：京牌")
    private String carCity;

    @ApiModelProperty(value = "是否有房")
    private Boolean isHasHourse;

    @ApiModelProperty(value = "房所在城市，如果有房，在哪，如：北京")
    private String hourseCity;

    @ApiModelProperty(value = "生日")
    private LocalDate birthDay;

    @ApiModelProperty(value = "是否认证")
    private Boolean isVerified;

    @ApiModelProperty(value = "是否展示到列表")
    private Boolean isShowInList;

    @ApiModelProperty(value = "当前所在地")
    private String nowAreaId;

    @TransBy(type = Area.TRANS_AREA_AND_PARENTS_BY_ID,byFieldName = "nowAreaId",mapValueField = "name")
    @ApiModelProperty(value = "当前所在地名称")
    private String nowAreaName;

    @ApiModelProperty(value = "家乡所在地")
    private String homeAreaId;

    @TransBy(type = Area.TRANS_AREA_AND_PARENTS_BY_ID,byFieldName = "homeAreaId",mapValueField = "name")
    @ApiModelProperty(value = "家乡所在地名称")
    private String homeAreaName;

}
