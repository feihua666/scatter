package scatter.wwd.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 活动参与添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动参与添加表单对象")
public class ActivityParticipateAddForm extends BaseAddForm {

    @NotEmpty(message="活动ID不能为空")
    @ApiModelProperty(value = "活动ID",required = true)
    private String activityId;

    @NotEmpty(message="用户ID不能为空")
    @ApiModelProperty(value = "用户ID",required = true)
    private String userId;

    @NotNull(message="是否是领队不能为空")
    @ApiModelProperty(value = "是否是领队",required = true)
    private Boolean isLeader;

    @ApiModelProperty(value = "是否退出")
    private Boolean isQuit;

    @ApiModelProperty(value = "备注，退款。。。")
    private String remarks;

    @ApiModelProperty(value = "报名人员姓名")
    private String name;

    @ApiModelProperty(value = "报名人员手机号")
    private String mobile;

    @ApiModelProperty(value = "证件号码")
    private String idCardNo;

    @ApiModelProperty(value = "证件类型")
    private String idTypeDictId;

}
