package scatter.wwd.pojo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.LoginUser;
import scatter.identifier.pojo.po.Identifier;
import scatter.usersimple.pojo.po.UserSimple;

/**
 * 汪汪队登录用户信息
 * Created by yangwei
 * Created at 2021/1/21 16:52
 */
@Setter
@Getter
@ApiModel("汪汪队登录用户信息")
public class WwdAppLoginUser extends LoginUser {

    @ApiModelProperty(value = "用户登录信息")
    private UserSimple user;

    @ApiModelProperty(value = "用户登录标识信息")
    private Identifier userIdentifier;

    @Override
    public String getId() {
        return user.getId();
    }

    @Override
    public String getNickname() {
        return user.getNickname();
    }

    @Override
    public String getAvatar() {
        return user.getAvatar();
    }

    @Override
    public Boolean getIsExpired() {
        return userIdentifier.getIsExpired() || super.getIsExpired();
    }

    @Override
    public Boolean getIsLocked() {
        return user.getIsLock() || userIdentifier.getIsLock() || super.getIsLocked();
    }
}
