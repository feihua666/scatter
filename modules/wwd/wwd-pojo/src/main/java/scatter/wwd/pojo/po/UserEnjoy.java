package scatter.wwd.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户有意思表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wwd_user_enjoy")
@ApiModel(value="UserEnjoy对象", description="用户有意思表")
public class UserEnjoy extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERENJOY_BY_ID = "trans_userenjoy_by_id_scatter.wwd.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "有意思用户id")
    private String enjoyedUserId;

    @ApiModelProperty(value = "添加有意思时，可以写一句话")
    private String message;


}
