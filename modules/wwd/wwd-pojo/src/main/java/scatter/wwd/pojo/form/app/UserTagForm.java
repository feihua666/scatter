package scatter.wwd.pojo.form.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.wwd.pojo.form.UserTagAddForm;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Description
 * @Date 6/12/21 4:01 PM
 * @Created by ciaj.
 */
@Setter
@Getter
@ApiModel(value="用户标签添加表单对象")
public class UserTagForm {
    @NotEmpty(message="用户ID不能为空")
    @ApiModelProperty(value = "用户ID",required = true)
    private String userId;

    @NotEmpty(message="标签类型不能为空")
    List<UserTagAddForm> tags;

}
