package scatter.wwd.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.wwd.pojo.po.Activity;

import java.time.LocalDateTime;


/**
 * <p>
 * 活动互选配置响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动互选配置响应对象")
public class ActivityMutualElectionConfigVo extends BaseIdVo {

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @TransBy(type = Activity.TRANS_ACTIVITY_BY_ID,byFieldName = "activityId",mapValueField = "title")
    @ApiModelProperty(value = "活动标题")
    private String activityTitle;

    @ApiModelProperty(value = "互选开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "互选结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "互选状态，未开始，进行中，已结束")
    private String mutualElectionStatusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "mutualElectionStatusDictId",mapValueField = "name")
    @ApiModelProperty(value = "互选状态，未开始，进行中，已结束")
    private String mutualElectionStatusDictName;

    @ApiModelProperty(value = "管理的用户id，多个以逗号分隔，人员可以进行h5活动管理，目前是为控制互选而生")
    private String manageUserIds;

}
