package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.usersimple.pojo.po.UserSimple;


/**
 * <p>
 * 用户标签响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户标签响应对象")
public class UserTagVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @TransBy(type = UserSimple.TRANS_USERSIMPLE_BY_ID,byFieldName = "userId",mapValueField = "nickname")
    @ApiModelProperty(value = "用户")
    private String userNickname;

    @ApiModelProperty(value = "标签类型，字典，兴趣，性格，食物，旅游等")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "code")
    @ApiModelProperty(value = "标签类型，字典值")
    private String typeDictCode;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "value")
    @ApiModelProperty(value = "标签类型，字典值")
    private String typeDictValue;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "标签类型，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "对应标签的内容字典编码，多个以逗号分隔")
    private String content;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "content",mapValueField = "name",isGroup = true)
    @ApiModelProperty(value = "对应标签的内容字典编码，多个以逗号分隔")
    private String contentNames;

    @ApiModelProperty(value = "对应标签的自定义内容，文本，直接展示")
    private String selfContent;

}
