package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.area.pojo.po.Area;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.TransField;
import scatter.dict.pojo.po.Dict;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.po.ActivityParticipate;

import java.time.LocalDateTime;


/**
 * <p>
 * 活动响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Setter
@Getter
@ApiModel(value = "活动响应对象")
public class ActivityVo extends BaseIdVo {

    @ApiModelProperty(value = "活动标题,模糊查询")
    private String title;

    @ApiModelProperty(value = "标题图")
    private String titleUrl;

    @ApiModelProperty(value = "所在省ID")
    private String provinceAreaId;

    @TransBy(type = Area.TRANS_AREA_BY_ID, byFieldName = "provinceAreaId", mapValueField = "name")
    @ApiModelProperty(value = "所在省名称")
    private String provinceAreaName;

    @ApiModelProperty(value = "所在城市ID")
    private String cityAreaId;

    @TransBy(type = Area.TRANS_AREA_BY_ID, byFieldName = "cityAreaId", mapValueField = "name")
    @ApiModelProperty(value = "所在城市名称")
    private String cityAreaName;

    @ApiModelProperty(value = "活动简介,模糊查询")
    private String introduced;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "主办方链接")
    private String sponsorLink;

    @ApiModelProperty(value = "作者,模糊查询")
    private String author;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "活动地点,模糊查询")
    private String address;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "场地类型(室内、户外)，字典")
    private String placeTypeDictId;

    @ApiModelProperty(value = "场地类型名称")
    @TransBy(type = Dict.TRANS_DICT_BY_ID, byFieldName = "placeTypeDictId", mapValueField = "name")
    private String placeTypeDictName;

    @ApiModelProperty(value = "是否发布")
    private Boolean isPublish;

    @ApiModelProperty(value = "活动声明,模糊查询")
    private String activityStatement;

    @ApiModelProperty(value = "序号，从小到大排序")
    private Integer seq;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createAt;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateAt;

    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "点赞数")
    private Integer starCount;

    @ApiModelProperty(value = "想去数")
    private Integer enjoyCount;

    @ApiModelProperty(value = "评分人数")
    private Integer rateCount;

    @ApiModelProperty(value = "评分总数")
    private Integer rateSum;

    @ApiModelProperty(value = "评分得分，计算方法：总评分数/(评分人数*10)")
    private Double rateScore;
}
