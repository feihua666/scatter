package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 此vo目前用于 wwd h5 活动列表
 * </p>
 *
 * @author yangwei
 * @since 2021-11-01 22:00
 */
@Setter
@Getter
@ApiModel(value = "活动响应对象")
public class ActivityVo1 extends ActivityVo{
}
