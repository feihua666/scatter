package scatter.wwd.pojo.param;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import java.util.List;

/**
 * <p>
 * 汪汪队卡片生成参数
 * </p>
 *
 * @author yangwei
 * @since 2021-10-21 10:29
 */
@Setter
@Getter
public class GenerateCardParam extends BaseParam {

	/**
	 * 主图可选
	 */
	private String mainPicUrl;

	/**
	 * 其它图片，可选
	 */
	private List<String> otherPicUrls;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 标识公众号
	 */
	private String appCode;
}
