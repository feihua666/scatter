package scatter.wwd.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;

import java.time.LocalDate;


/**
 * <p>
 * 用户信息分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户信息分页表单对象")
public class UserInfoPageSameCityQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id")
    private String userId;
}
