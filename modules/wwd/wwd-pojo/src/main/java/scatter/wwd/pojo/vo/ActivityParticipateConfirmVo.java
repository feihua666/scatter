package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.area.pojo.po.Area;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.TransField;
import scatter.dict.pojo.po.Dict;

import java.time.LocalDateTime;


/**
 * <p>
 * 活动报名信息确认响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Setter
@Getter
@ApiModel(value = "活动报名信息确认响应对象")
public class ActivityParticipateConfirmVo extends BaseIdVo {

    @ApiModelProperty(value = "活动标题,模糊查询")
    private String title;

    @ApiModelProperty(value = "标题图")
    private String titleUrl;

    @ApiModelProperty(value = "所在省ID")
    private String provinceAreaId;

    @TransBy(type = Area.TRANS_AREA_BY_ID, byFieldName = "provinceAreaId", mapValueField = "name")
    @ApiModelProperty(value = "所在省名称")
    private String provinceAreaName;

    @ApiModelProperty(value = "所在城市ID")
    private String cityAreaId;

    @TransBy(type = Area.TRANS_AREA_BY_ID, byFieldName = "cityAreaId", mapValueField = "name")
    @ApiModelProperty(value = "所在城市名称")
    private String cityAreaName;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "活动地点,模糊查询")
    private String address;

    @ApiModelProperty(value = "场地类型(室内、户外)，字典")
    private String placeTypeDictId;

    @ApiModelProperty(value = "场地类型名称")
    @TransBy(type = Dict.TRANS_DICT_BY_ID, byFieldName = "placeTypeDictId", mapValueField = "name")
    private String placeTypeDictName;

    @TransField
    @ApiModelProperty(value = "报名规则")
    private ActivityEnrollRuleVo enrollRule;
}
