package scatter.wwd.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户相册响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户相册响应对象")
public class UserAlbumVo extends BaseIdVo {

    @ApiModelProperty(value = "main/normal")
    private String type;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "图片url")
    private String picUrl;

    @ApiModelProperty(value = "缩略图url")
    private String picThumbUrl;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "序号，从小到大排序")
    private Integer seq;

   public enum AlbumType{
       /**
        * main，用户信息主图，normal，用户相册图片
        */
       main,normal;
    }
}
