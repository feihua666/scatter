package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;


/**
 * <p>
 * 用户有意思状态响应对象
 * </p>
 *
 * @author yw
 * @since 2021-06-14 21:39:32
 */
@Setter
@Getter
@ApiModel(value="用户有意思状态响应对象")
public class UserEnjoyStatusVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "有意思用户id")
    private String enjoyedUserId;

    @ApiModelProperty(value = "是否有意思",notes = "userId是否对enjoyedUserId有意思")
    private Boolean isEnjoy;

    @ApiModelProperty(value = "是否有意思",notes = "enjoyedUserId是否对userId有意思")
    private Boolean isEnjoyed;

    @ApiModelProperty(value = "是否可以有意思",notes = "userId是否可以对enjoyedUserId有意思")
    private Boolean isCanEnjoy;

    @ApiModelProperty(value = "isCanEnjoy为false时，错误原因",notes = "")
    private String errorMsgForIsCanEnjoyFalse;
}
