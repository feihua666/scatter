package scatter.wwd.pojo.form.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * @Description
 * @Date 6/12/21 4:01 PM
 * @Created by ciaj.
 */
@Setter
@Getter
@ApiModel(value="活动报名添加表单对象")
public class ActivitySingupForm {
    @NotEmpty(message="用户ID不能为空")
    @ApiModelProperty(value = "用户ID",required = true)
    private String userId;
    @NotEmpty(message="活动ID不能为空")
    @ApiModelProperty(value = "活动ID",required = true)
    private String activityId;
    @NotEmpty(message="姓名不能为空")
    @ApiModelProperty(value = "姓名",required = true)
    private String name;
    @NotEmpty(message="性别不能为空")
    @ApiModelProperty(value = "性别",required = true)
    private String genderDictId;
    @NotEmpty(message="手机号不能为空")
    @ApiModelProperty(value = "手机号",required = true)
    private String mobile;
    @ApiModelProperty(value = "身份证号")
    private String idCardNo;
    @ApiModelProperty(value = "保存到信息")
    private Boolean saveToInfo;

    @ApiModelProperty(value = "订单id")
    private String orderId;
    @ApiModelProperty(value = "订单号")
    private String orderNo;
}
