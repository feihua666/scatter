package scatter.wwd.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.wwd.pojo.po.Activity;

import java.time.LocalDateTime;


/**
 * <p>
 * 活动报名规则响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="活动报名规则响应对象")
public class ActivityEnrollRuleVo extends BaseIdVo {

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @TransBy(type = Activity.TRANS_ACTIVITY_BY_ID,byFieldName = "activityId",mapValueField = "title")
    @ApiModelProperty(value = "活动标题")
    private String activityTitle;

    @ApiModelProperty(value = "报名开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "报名结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "报名联系方式")
    private String contact;

    @ApiModelProperty(value = "1=需要证件，身份证 0=不需要，这在报名的时候会判断添加")
    private Boolean isRequireIdCard;

    @ApiModelProperty(value = "活动人数")
    private Integer headcount;

    @ApiModelProperty(value = "活动人数，男")
    private Integer headcountMale;

    @ApiModelProperty(value = "活动人数，女")
    private Integer headcountFemale;

    @ApiModelProperty(value = "人数说明")
    private String headcountDesc;

    @ApiModelProperty(value = "男女活动人数是否按性别自定义")
    private Boolean isHeadcountGenderSelf;

    @ApiModelProperty(value = "男价格，单位分")
    private Integer malePrice;

    @ApiModelProperty(value = "女价格，单位分")
    private Integer femalePrice;

    @ApiModelProperty(value = "是否需要线上支付")
    private Boolean isNeedPayOnline;

    @ApiModelProperty(value = "已报名活动-已支付人数，男")
    private Integer countMale;

    @ApiModelProperty(value = "已报名活动-已支付人数，女")
    private Integer countFemale;

    @ApiModelProperty(value = "退款截止时间")
    private LocalDateTime refundDeadline;
}
