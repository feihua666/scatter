package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.form.app.GenerateCardForm;
import scatter.wwd.pojo.param.GenerateCardParam;
import scatter.wwd.pojo.po.UserCard;

/**
 * <p>
 * 用户卡片表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserCardService extends IBaseService<UserCard> {


    /**
     * 卡片生成
     * @param param
     */
    void generateCard(GenerateCardParam param);

    /**
     * 根据用户获取
     * @param userId
     * @return
     */
    default UserCard getByUserId(String userId){
        return this.getOne(new QueryWrapper<UserCard>().lambda().eq(UserCard::getUserId,userId));
    }
}
