package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.pojo.vo.UserEnjoyStatusVo;

import java.util.List;

/**
 * <p>
 * 用户有意思表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserEnjoyService extends IBaseService<UserEnjoy> {


	/**
	 * 根据用户id查询
	 * @param userId
	 * @return
	 */
	default List<UserEnjoy> getByUserId(String userId){
		Assert.hasText(userId,"userId不能为空");
		return list(Wrappers.<UserEnjoy>lambdaQuery().eq(UserEnjoy::getUserId, userId));
	}
	/**
	 * 根据enjoyedUserId查询
	 * @param enjoyedUserId
	 * @return
	 */
	default List<UserEnjoy> getByEnjoyedUserId(String enjoyedUserId){
		Assert.hasText(enjoyedUserId,"enjoyedUserId不能为空");
		return list(Wrappers.<UserEnjoy>lambdaQuery().eq(UserEnjoy::getEnjoyedUserId, enjoyedUserId));
	}

	/**
	 * 根据用户id和有意思用户id查询
	 * @param userId
	 * @param enjoyedUserId
	 * @return
	 */
	default UserEnjoy getByUserIdAndEnjoyedUserId(String userId,String enjoyedUserId){
		Assert.hasText(userId,"userId不能为空");
		Assert.hasText(enjoyedUserId,"enjoyedUserId不能为空");
		return getOne(Wrappers.<UserEnjoy>lambdaQuery().eq(UserEnjoy::getUserId, userId).eq(UserEnjoy::getEnjoyedUserId, enjoyedUserId));
	}

	/**
	 * 获取用户 userId 对用户 enjoyedUserId 有意思状态
	 * @param userId
	 * @param enjoyedUserId
	 * @return
	 */
	UserEnjoyStatusVo getUserEnjoyStatus(String userId,String enjoyedUserId);

}
