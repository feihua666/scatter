package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.*;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.vo.ActivityEnrollRuleVo;
import scatter.wwd.pojo.form.ActivityEnrollRuleAddForm;
import scatter.wwd.pojo.form.ActivityEnrollRuleUpdateForm;
import scatter.wwd.pojo.form.ActivityEnrollRulePageQueryForm;

/**
 * <p>
 * 活动报名规则 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ActivityEnrollRuleMapStruct extends
        IBaseVoMapStruct<ActivityEnrollRule, ActivityEnrollRuleVo> ,
        IBaseAddFormMapStruct<ActivityEnrollRule,ActivityEnrollRuleAddForm>,
        IBaseUpdateFormMapStruct<ActivityEnrollRule,ActivityEnrollRuleUpdateForm>,
        IBaseQueryFormMapStruct<ActivityEnrollRule,ActivityEnrollRulePageQueryForm>{
    ActivityEnrollRuleMapStruct INSTANCE = Mappers.getMapper( ActivityEnrollRuleMapStruct.class );

}
