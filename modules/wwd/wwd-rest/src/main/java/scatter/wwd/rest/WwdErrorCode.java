package scatter.wwd.rest;

import lombok.Getter;
import scatter.common.ECF;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-01 22:30
 */
@Getter
public enum WwdErrorCode  implements ECF {
	ERROR_NO_NOW_AREA(244033,"无当前所在地信息");

	private Integer code;
	private String msg;
	WwdErrorCode(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
