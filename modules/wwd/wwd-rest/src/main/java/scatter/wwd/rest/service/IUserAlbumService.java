package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.UserAlbum;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户相册表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserAlbumService extends IBaseService<UserAlbum> {


    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<UserAlbum> getByUserId(String userId) {
        Assert.hasText(userId,"用户id不能为空");
        return list(Wrappers.<UserAlbum>lambdaQuery().eq(UserAlbum::getUserId, userId));
    }
    /**
     * 根据用户ids查询
     * @param userIds
     * @return
     */
    default List<UserAlbum> getByUserIds(Set<String> userIds) {
        Assert.notEmpty(userIds,"用户id不能为空");
        return list(Wrappers.<UserAlbum>lambdaQuery().in(UserAlbum::getUserId, userIds));
    }
}
