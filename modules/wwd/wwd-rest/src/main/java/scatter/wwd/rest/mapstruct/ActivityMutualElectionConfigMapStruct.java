package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigAddForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigPageQueryForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigUpdateForm;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.wwd.pojo.vo.ActivityMutualElectionConfigVo;

/**
 * <p>
 * 活动互选配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ActivityMutualElectionConfigMapStruct extends IBaseVoMapStruct<ActivityMutualElectionConfig, ActivityMutualElectionConfigVo>,
        IBaseAddFormMapStruct<ActivityMutualElectionConfig, ActivityMutualElectionConfigAddForm>,
        IBaseUpdateFormMapStruct<ActivityMutualElectionConfig, ActivityMutualElectionConfigUpdateForm>,
        IBaseQueryFormMapStruct<ActivityMutualElectionConfig, ActivityMutualElectionConfigPageQueryForm> {
    ActivityMutualElectionConfigMapStruct INSTANCE = Mappers.getMapper( ActivityMutualElectionConfigMapStruct.class );

}
