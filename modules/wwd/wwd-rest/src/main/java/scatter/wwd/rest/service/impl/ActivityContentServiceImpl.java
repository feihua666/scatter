package scatter.wwd.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wwd.pojo.form.ActivityContentAddForm;
import scatter.wwd.pojo.form.ActivityContentPageQueryForm;
import scatter.wwd.pojo.form.ActivityContentUpdateForm;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.rest.mapper.ActivityContentMapper;
import scatter.wwd.rest.service.IActivityContentService;
/**
 * <p>
 * 活动内容表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Service
@Transactional
public class ActivityContentServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ActivityContentMapper, ActivityContent, ActivityContentAddForm, ActivityContentUpdateForm, ActivityContentPageQueryForm> implements IActivityContentService {
    @Override
    public void preAdd(ActivityContentAddForm addForm,ActivityContent po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getActivityId())) {
            // 活动id已存在不能添加
            assertByColumn(addForm.getActivityId(),ActivityContent::getActivityId,false);
        }

    }

    @Override
    public void preUpdate(ActivityContentUpdateForm updateForm,ActivityContent po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getActivityId())) {
            ActivityContent byId = getById(updateForm.getId());
            // 如果活动id有改动
            if (!isEqual(updateForm.getActivityId(), byId.getActivityId())) {
                // 活动id已存在不能修改
                assertByColumn(updateForm.getActivityId(),ActivityContent::getActivityId,false);
            }
        }

    }
}
