package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.UserTag;
import scatter.wwd.rest.mapstruct.UserTagMapStruct;
import scatter.wwd.rest.service.IUserTagService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户标签翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserTagTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserTagService userTagService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,UserTag.TRANS_USERTAG_BY_ID,UserTag.TRANS_USERTAGS_VO_BY_USER_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserTag.TRANS_USERTAG_BY_ID)) {
            UserTag byId = userTagService.getById(key);
            return new TransResult(byId,key);
        }else if (isEqual(type,UserTag.TRANS_USERTAGS_VO_BY_USER_ID)) {
            List<UserTag> byUserId = userTagService.getByUserId(key);
            return new TransResult(UserTagMapStruct.INSTANCE.posToVos(byUserId),key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserTag.TRANS_USERTAG_BY_ID)) {
            return userTagService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if (isEqual(type,UserTag.TRANS_USERTAGS_VO_BY_USER_ID)) {
            List<UserTag> byUserId = userTagService.getByUserIds(keys);
            Map<String, List<UserTag>> map = new HashMap<>();
            for (String key : keys) {
                map.put(key, byUserId.stream().filter(item -> isEqual(key, item.getUserId())).collect(Collectors.toList()));
            }
            List<TransResult<Object, String>> r = new ArrayList<>(keys.size());
            for (String key : map.keySet()) {
                r.add(new TransResult<Object, String>(UserTagMapStruct.INSTANCE.posToVos(map.get(key)),key));
            }
            return r;
        }
        return null;
    }
}
