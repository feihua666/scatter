package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import scatter.common.dict.PublicDictEnums;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.wwd.pojo.vo.ActivityDetailVo;
import scatter.wwd.pojo.vo.AppActivityListVo;
import scatter.wwd.pojo.vo.AppActivityPayDetailVo;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Date 6/12/21 11:56 AM
 * @Created by ciaj.
 */
public interface IMiniAppService {


    /**
     * 获取活动支付信息确认详情
     * @param id
     * @return
     */
    AppActivityPayDetailVo getActivityPayDetailById(String id, PublicDictEnums.DictGender dictGender);

    /**
     * 获取用户相册
     * @param userId
     * @return
     */
    List<Map<String, Object>> getUserAlbum(String userId);
}
