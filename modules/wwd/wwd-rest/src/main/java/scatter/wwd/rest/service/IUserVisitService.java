package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.form.UserVisitPageQueryForm;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.pojo.vo.UserVisitVo;

/**
 * <p>
 * 汪汪队用户访问 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserVisitService extends IBaseService<UserVisit> {


}
