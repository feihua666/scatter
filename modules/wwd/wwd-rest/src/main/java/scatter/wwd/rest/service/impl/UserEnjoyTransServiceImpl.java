package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.rest.service.IUserEnjoyService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户有意思翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserEnjoyTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserEnjoyService userEnjoyService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,UserEnjoy.TRANS_USERENJOY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserEnjoy.TRANS_USERENJOY_BY_ID)) {
            UserEnjoy byId = userEnjoyService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserEnjoy.TRANS_USERENJOY_BY_ID)) {
            return userEnjoyService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
