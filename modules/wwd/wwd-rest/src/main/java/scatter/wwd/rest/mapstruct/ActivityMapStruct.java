package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.ActivityAddForm;
import scatter.wwd.pojo.form.ActivityPageQueryForm;
import scatter.wwd.pojo.form.ActivityUpdateForm;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.pojo.vo.*;

/**
 * <p>
 * 活动 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ActivityMapStruct extends IBaseVoMapStruct<Activity, ActivityVo>,
        IBaseAddFormMapStruct<Activity, ActivityAddForm>,
        IBaseUpdateFormMapStruct<Activity, ActivityUpdateForm>,
        IBaseQueryFormMapStruct<Activity, ActivityPageQueryForm> {
    ActivityMapStruct INSTANCE = Mappers.getMapper( ActivityMapStruct.class );

    ActivityVo1 poToActivityVo1(Activity activity);
    ActivityDetailVo poToActivityDetailVo(Activity activity);
    ActivityParticipateConfirmVo poToActivityParticipateConfirmVo(Activity activity);

    ActivityWithContentVo voToWithContentVo(ActivityVo activityVo);
}
