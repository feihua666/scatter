package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.pojo.vo.UserAlbumVo;
import scatter.wwd.pojo.form.UserAlbumAddForm;
import scatter.wwd.pojo.form.UserAlbumUpdateForm;
import scatter.wwd.pojo.form.UserAlbumPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户相册表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-album")
@Api(tags = "用户相册")
public class UserAlbumController extends BaseAddUpdateQueryFormController<UserAlbum, UserAlbumVo, UserAlbumAddForm, UserAlbumUpdateForm, UserAlbumPageQueryForm> {


     @Override
	 @ApiOperation("添加用户相册")
     @PreAuthorize("hasAuthority('UserAlbum:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserAlbumVo add(@RequestBody @Valid UserAlbumAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户相册")
     @PreAuthorize("hasAuthority('UserAlbum:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserAlbumVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户相册")
     @PreAuthorize("hasAuthority('UserAlbum:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户相册")
     @PreAuthorize("hasAuthority('UserAlbum:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserAlbumVo update(@RequestBody @Valid UserAlbumUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户相册")
    //@PreAuthorize("hasAuthority('UserAlbum:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserAlbumVo> getList(UserAlbumPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户相册")
    @PreAuthorize("hasAuthority('UserAlbum:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserAlbumVo> getPage(UserAlbumPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
