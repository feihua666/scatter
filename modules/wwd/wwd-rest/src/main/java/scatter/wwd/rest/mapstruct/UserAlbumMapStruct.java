package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserAlbumAddForm;
import scatter.wwd.pojo.form.UserAlbumPageQueryForm;
import scatter.wwd.pojo.form.UserAlbumUpdateForm;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.pojo.vo.UserAlbumVo;

/**
 * <p>
 * 用户相册 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserAlbumMapStruct extends IBaseVoMapStruct<UserAlbum, UserAlbumVo>,
        IBaseAddFormMapStruct<UserAlbum, UserAlbumAddForm>,
        IBaseUpdateFormMapStruct<UserAlbum, UserAlbumUpdateForm>,
        IBaseQueryFormMapStruct<UserAlbum, UserAlbumPageQueryForm> {
    UserAlbumMapStruct INSTANCE = Mappers.getMapper( UserAlbumMapStruct.class );

}
