package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.ActivityEnrollRule;

import java.util.List;

/**
 * <p>
 * 活动报名规则表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IActivityEnrollRuleService extends IBaseService<ActivityEnrollRule> {

    /**
     * 根据活动id查询
     * @param activityId
     * @return
     */
    default ActivityEnrollRule getByActivityId(String activityId) {
        Assert.hasText(activityId,"activityId不能为空");
        return getOne(Wrappers.<ActivityEnrollRule>lambdaQuery().eq(ActivityEnrollRule::getActivityId, activityId));
    }
    /**
     * 根据活动id查询
     * @param activityIds
     * @return
     */
    default List<ActivityEnrollRule> getByActivityIds(List<String> activityIds) {
        Assert.notEmpty(activityIds,"activityIds 不能为空");
        return list(Wrappers.<ActivityEnrollRule>lambdaQuery().in(ActivityEnrollRule::getActivityId, activityIds));
    }
    /**
     * 更新报名人数
     * @param activityId
     * @param userId
     * @param num
     */
    public void updateNum(String activityId,String userId,int num);
}
