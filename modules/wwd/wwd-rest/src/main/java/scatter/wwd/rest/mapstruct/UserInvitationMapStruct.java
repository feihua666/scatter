package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserInvitationAddForm;
import scatter.wwd.pojo.form.UserInvitationPageQueryForm;
import scatter.wwd.pojo.form.UserInvitationUpdateForm;
import scatter.wwd.pojo.po.UserInvitation;
import scatter.wwd.pojo.vo.UserInvitationVo;

/**
 * <p>
 * 用户邀请 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserInvitationMapStruct extends IBaseVoMapStruct<UserInvitation, UserInvitationVo>,
        IBaseAddFormMapStruct<UserInvitation, UserInvitationAddForm>,
        IBaseUpdateFormMapStruct<UserInvitation, UserInvitationUpdateForm>,
        IBaseQueryFormMapStruct<UserInvitation, UserInvitationPageQueryForm> {
    UserInvitationMapStruct INSTANCE = Mappers.getMapper( UserInvitationMapStruct.class );

}
