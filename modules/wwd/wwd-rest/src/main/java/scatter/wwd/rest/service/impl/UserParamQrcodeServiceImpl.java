package scatter.wwd.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wwd.pojo.form.UserParamQrcodeAddForm;
import scatter.wwd.pojo.form.UserParamQrcodePageQueryForm;
import scatter.wwd.pojo.form.UserParamQrcodeUpdateForm;
import scatter.wwd.pojo.po.UserParamQrcode;
import scatter.wwd.rest.mapper.UserParamQrcodeMapper;
import scatter.wwd.rest.service.IUserParamQrcodeService;
/**
 * <p>
 * 用户标签表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserParamQrcodeServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserParamQrcodeMapper, UserParamQrcode, UserParamQrcodeAddForm, UserParamQrcodeUpdateForm, UserParamQrcodePageQueryForm> implements IUserParamQrcodeService {
    @Override
    public void preAdd(UserParamQrcodeAddForm addForm,UserParamQrcode po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserParamQrcodeUpdateForm updateForm,UserParamQrcode po) {
        super.preUpdate(updateForm,po);

    }
}
