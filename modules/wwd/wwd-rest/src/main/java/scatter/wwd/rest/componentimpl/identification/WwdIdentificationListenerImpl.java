package scatter.wwd.rest.componentimpl.identification;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.notify.NotifyParam;
import scatter.common.rest.notify.NotifyTool;
import scatter.identification.pojo.param.IdentificationApplyParam;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.componentext.IdentificationApplyListener;
import scatter.identification.rest.componentext.IdentificationAuditListener;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.service.IUserInfoService;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 15:21
 */
@Component
public class WwdIdentificationListenerImpl implements IdentificationApplyListener, IdentificationAuditListener {

	@Autowired
	private IUserInfoService iUserInfoService;

	@Override
	public void identificationApply(IdentificationApplyParam param, Identification identification) {
		UserInfo byUserId = iUserInfoService.getByUserId(identification.getUserId());
		NotifyParam notifyParam = NotifyParam.business().setContentType("identification.apply")
				.setTitle(identification.getName() + " 申请")
				.setContent(StrUtil.format("用户 {} 发起认证申请，请及时审核", byUserId.getNickname()));
		NotifyTool.notify(notifyParam);
	}

	@Override
	public void identificationAudit(Identification identification) {
		UserInfo byUserId = iUserInfoService.getByUserId(identification.getUserId());
		String flag = identification.getIsAudited() ? "成功" : "失败";
		NotifyParam notifyParam = NotifyParam.business().setContentType("identification.identified.success")
				.setTitle(identification.getName() + " " + flag)
				.setContent(StrUtil.format("用户 {} {} {}，请知悉", byUserId.getNickname(),identification.getName(),flag));
		NotifyTool.notify(notifyParam);
	}
}
