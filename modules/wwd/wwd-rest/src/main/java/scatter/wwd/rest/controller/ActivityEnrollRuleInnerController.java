package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 活动报名规则表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/inner/activity-enroll-rule")
public class ActivityEnrollRuleInnerController extends BaseInnerController<ActivityEnrollRule> {
 @Autowired
 private IActivityEnrollRuleService activityEnrollRuleService;

 public IActivityEnrollRuleService getService(){
     return activityEnrollRuleService;
 }
}
