package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserParamQrcodeAddForm;
import scatter.wwd.pojo.form.UserParamQrcodePageQueryForm;
import scatter.wwd.pojo.form.UserParamQrcodeUpdateForm;
import scatter.wwd.pojo.po.UserParamQrcode;
import scatter.wwd.pojo.vo.UserParamQrcodeVo;

/**
 * <p>
 * 用户标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserParamQrcodeMapStruct extends IBaseVoMapStruct<UserParamQrcode, UserParamQrcodeVo>,
        IBaseAddFormMapStruct<UserParamQrcode, UserParamQrcodeAddForm>,
        IBaseUpdateFormMapStruct<UserParamQrcode, UserParamQrcodeUpdateForm>,
        IBaseQueryFormMapStruct<UserParamQrcode, UserParamQrcodePageQueryForm> {
    UserParamQrcodeMapStruct INSTANCE = Mappers.getMapper( UserParamQrcodeMapStruct.class );

}
