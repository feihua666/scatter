package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.rest.service.IUserInfoService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户信息表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/inner/user-info")
public class UserInfoInnerController extends BaseInnerController<UserInfo> {
 @Autowired
 private IUserInfoService userInfoService;

 public IUserInfoService getService(){
     return userInfoService;
 }
}
