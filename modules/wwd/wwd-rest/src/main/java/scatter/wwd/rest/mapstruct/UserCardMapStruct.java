package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserCardAddForm;
import scatter.wwd.pojo.form.UserCardPageQueryForm;
import scatter.wwd.pojo.form.UserCardUpdateForm;
import scatter.wwd.pojo.po.UserCard;
import scatter.wwd.pojo.vo.UserCardVo;

/**
 * <p>
 * 用户卡片 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserCardMapStruct extends IBaseVoMapStruct<UserCard, UserCardVo>,
        IBaseAddFormMapStruct<UserCard, UserCardAddForm>,
        IBaseUpdateFormMapStruct<UserCard, UserCardUpdateForm>,
        IBaseQueryFormMapStruct<UserCard, UserCardPageQueryForm> {
    UserCardMapStruct INSTANCE = Mappers.getMapper( UserCardMapStruct.class );

}
