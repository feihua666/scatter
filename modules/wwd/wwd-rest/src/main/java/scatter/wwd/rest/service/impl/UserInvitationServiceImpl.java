package scatter.wwd.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wwd.pojo.form.UserInvitationAddForm;
import scatter.wwd.pojo.form.UserInvitationPageQueryForm;
import scatter.wwd.pojo.form.UserInvitationUpdateForm;
import scatter.wwd.pojo.po.UserInvitation;
import scatter.wwd.rest.mapper.UserInvitationMapper;
import scatter.wwd.rest.service.IUserInvitationService;
/**
 * <p>
 * 用户邀请表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserInvitationServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserInvitationMapper, UserInvitation, UserInvitationAddForm, UserInvitationUpdateForm, UserInvitationPageQueryForm> implements IUserInvitationService {
    @Override
    public void preAdd(UserInvitationAddForm addForm,UserInvitation po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserInvitationUpdateForm updateForm,UserInvitation po) {
        super.preUpdate(updateForm,po);

    }
}
