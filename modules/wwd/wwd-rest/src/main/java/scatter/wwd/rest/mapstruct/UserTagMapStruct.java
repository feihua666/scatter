package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserTagAddForm;
import scatter.wwd.pojo.form.UserTagPageQueryForm;
import scatter.wwd.pojo.form.UserTagUpdateForm;
import scatter.wwd.pojo.po.UserTag;
import scatter.wwd.pojo.vo.UserTagVo;

/**
 * <p>
 * 用户标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserTagMapStruct extends IBaseVoMapStruct<UserTag, UserTagVo>,
        IBaseAddFormMapStruct<UserTag, UserTagAddForm>,
        IBaseUpdateFormMapStruct<UserTag, UserTagUpdateForm>,
        IBaseQueryFormMapStruct<UserTag, UserTagPageQueryForm> {
    UserTagMapStruct INSTANCE = Mappers.getMapper( UserTagMapStruct.class );

}
