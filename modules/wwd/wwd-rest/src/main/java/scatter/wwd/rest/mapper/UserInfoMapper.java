package scatter.wwd.rest.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import scatter.wwd.pojo.po.UserInfo;
import scatter.common.rest.service.IBaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserInfoMapper extends IBaseMapper<UserInfo> {

    /**
     * 主要用于随机推荐，查找认证用户且存在相册的
     * @param wrapper
     * @param limit
     * @return
     */
    @Select("SELECT * FROM wwd_user_info ${ew.customSqlSegment} and user_id in (select user_id from wwd_user_album) order by rand() limit #{limit} ")
    List<UserInfo> recommend(@Param(Constants.WRAPPER) Wrapper wrapper,@Param("limit") int limit);

    List<Map<String, Object>> getUserAlbum(@Param("userId") String userId);
}
