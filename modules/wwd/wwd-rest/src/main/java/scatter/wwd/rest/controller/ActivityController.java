package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.validation.DictService;
import scatter.wwd.pojo.form.ActivityPageQueryForm1;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.vo.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.pojo.form.ActivityAddForm;
import scatter.wwd.pojo.form.ActivityUpdateForm;
import scatter.wwd.pojo.form.ActivityPageQueryForm;
import scatter.wwd.rest.mapstruct.ActivityEnrollRuleMapStruct;
import scatter.wwd.rest.mapstruct.ActivityMapStruct;
import scatter.wwd.rest.service.IActivityContentService;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import scatter.wwd.rest.service.IActivityService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * <p>
 * 活动表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Api(tags = "活动相关接口")
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/activity")
public class ActivityController extends BaseAddUpdateQueryFormController<Activity, ActivityVo, ActivityAddForm, ActivityUpdateForm, ActivityPageQueryForm> {


    @Autowired
    private IActivityContentService iActivityContentService;

    @Autowired
    private IActivityEnrollRuleService iActivityEnrollRuleService;

    @Autowired
    private IActivityService iActivityService;

    @Autowired
    private DictService dictService;

    @ApiOperation("添加活动")
    @PreAuthorize("hasAuthority('Activity:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ActivityVo add(@RequestBody @Valid ActivityAddForm addForm) {
        return super.add(addForm);
    }

    @ApiOperation("根据ID查询活动")
    @PreAuthorize("hasAuthority('Activity:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ActivityVo queryById(@PathVariable String id) {
        ActivityVo vo = super.queryById(id);
       return vo;
    }

    @ApiOperation("根据ID删除活动")
    @PreAuthorize("hasAuthority('Activity:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新活动")
    @PreAuthorize("hasAuthority('Activity:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ActivityVo update(@RequestBody @Valid ActivityUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询活动")
    @PreAuthorize("hasAuthority('Activity:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ActivityVo> getList(ActivityPageQueryForm listPageForm) {
        return super.getList(listPageForm);
    }

    @ApiOperation("分页查询活动")
    @PreAuthorize("hasAuthority('Activity:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ActivityVo> getPage(ActivityPageQueryForm listPageForm) {
        IPage<ActivityVo> result =  super.getPage(listPageForm);
        return result;
    }

    @ApiOperation("根据ID查询活动带活动内容字段")
    @PreAuthorize("hasAuthority('Activity:queryById')")
    @GetMapping("/activityWithContent/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActivityWithContentVo activityWithContent(@PathVariable String id) {
        ActivityVo vo = super.queryById(id);
        return ActivityMapStruct.INSTANCE.voToWithContentVo(vo);
    }


    @ApiOperation("活动详情")
    @GetMapping("/activityDetail/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActivityDetailVo activityDetail(@PathVariable String id, @ApiIgnore LoginUser loginUser) {
        return iActivityService.getActivityDetailById(id,Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
    }

    @ApiOperation("分页查询活动")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ActivityVo1> getPage1(ActivityPageQueryForm1 listPageForm, @ApiIgnore LoginUser loginUser) {
        return iActivityService.getPage1(listPageForm,Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
    }

    @ApiOperation("分页查询我的活动")
    @PreAuthorize("hasAuthority('appclient')")
    @GetMapping("/my-activity/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ActivityVo1> getMyActivityPage(BasePageQueryForm basePageQueryForm, @ApiIgnore LoginUser loginUser) {
        return iActivityService.getPageForUser(basePageQueryForm,Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
    }

    @ApiOperation("活动报名确认信息")
    @GetMapping("/activityParticipate/confirmInfo/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActivityParticipateConfirmVo activityParticipateConfirmInfo(@PathVariable String id, @ApiIgnore LoginUser loginUser) {
        Activity activity = iActivityService.getById(id);
        ActivityEnrollRule enrollRule = iActivityEnrollRuleService.getByActivityId(id);

        ActivityParticipateConfirmVo activityParticipateConfirmVo = ActivityMapStruct.INSTANCE.poToActivityParticipateConfirmVo(activity);
        ActivityEnrollRuleVo activityEnrollRuleVo = ActivityEnrollRuleMapStruct.INSTANCE.poToVo(enrollRule);
        activityParticipateConfirmVo.setEnrollRule(activityEnrollRuleVo);
        return activityParticipateConfirmVo;
    }
}
