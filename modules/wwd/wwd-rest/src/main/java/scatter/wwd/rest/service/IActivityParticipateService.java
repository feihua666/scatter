package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.form.ActivityParticipatePageQueryForm1;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.pojo.vo.ActivityParticipateVo;
import scatter.wwd.rest.mapstruct.ActivityParticipateMapStruct;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 活动参与 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IActivityParticipateService extends IBaseService<ActivityParticipate> {


    /**
     * 是否已参与
     * @param activityId
     * @param userId
     * @return
     */
    default boolean hasParticipated(String activityId,String userId){
        Assert.hasText(activityId,"activityId 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        LambdaQueryWrapper<ActivityParticipate> queryWrapper = Wrappers.<ActivityParticipate>lambdaQuery().eq(ActivityParticipate::getActivityId, activityId).eq(ActivityParticipate::getUserId, userId).eq(ActivityParticipate::getIsQuit, false);
        return count(queryWrapper) > 0;
    }

    /**
     * 根据活动id获取参数信息
     * @param activityId
     * @return
     */
    default List<ActivityParticipate> getByActivityIdAndUserId(String activityId, String userId){
        Assert.hasText(activityId,"activityId 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        return list(Wrappers.<ActivityParticipate>lambdaQuery().eq(ActivityParticipate::getActivityId, activityId).eq(ActivityParticipate::getUserId,userId));
    }
    /**
     * 根据活动id获取参数信息
     * @param activityId
     * @return
     */
    default List<ActivityParticipate> getByActivityIdAndIsQuit(String activityId,boolean isQuit){
        Assert.hasText(activityId,"activityId 不能为空");
        return list(Wrappers.<ActivityParticipate>lambdaQuery().eq(ActivityParticipate::getActivityId, activityId).eq(ActivityParticipate::getIsQuit,isQuit));
    }
    /**
     * 根据活动id获取参数信息
     * @param activityId
     * @return
     */
    default List<ActivityParticipate> getByActivityIdAndUserIdAndIsQuit(String activityId, String userId,boolean isQuit){
        Assert.hasText(activityId,"activityId 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        return list(Wrappers.<ActivityParticipate>lambdaQuery().eq(ActivityParticipate::getActivityId, activityId).eq(ActivityParticipate::getUserId,userId).eq(ActivityParticipate::getIsQuit,isQuit));
    }
    /**
     * 根据活动id获取参数信息
     * @param activityIds
     * @return
     */
    default List<ActivityParticipate> getByActivityIdsAndUserIdAndIsQuit(List<String> activityIds, String userId,boolean isQuit){
        Assert.notEmpty(activityIds,"activityIds 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        return list(Wrappers.<ActivityParticipate>lambdaQuery().in(ActivityParticipate::getActivityId, activityIds).eq(ActivityParticipate::getUserId,userId).eq(ActivityParticipate::getIsQuit,isQuit));
    }
    /**
     * 根据活动id获取参数信息
     * @param activityId
     * @return
     */
    default ActivityParticipate getByActivityIdAndUserIdAndOrderNo(String activityId, String userId,String orderNo){
        Assert.hasText(activityId,"activityId 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        Assert.hasText(orderNo,"orderNo 不能为空");
        return getOne(Wrappers.<ActivityParticipate>lambdaQuery().eq(ActivityParticipate::getActivityId, activityId).eq(ActivityParticipate::getUserId,userId).eq(ActivityParticipate::getOrderNo,orderNo));
    }
    /**
     * 根据活动id获取参数信息
     * @param activityId
     * @return
     */
    default ActivityParticipate getByActivityIdAndUserIdAndOrderId(String activityId, String userId,String orderId){
        Assert.hasText(activityId,"activityId 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        Assert.hasText(orderId,"orderId 不能为空");
        return getOne(Wrappers.<ActivityParticipate>lambdaQuery().eq(ActivityParticipate::getActivityId, activityId).eq(ActivityParticipate::getUserId,userId).eq(ActivityParticipate::getOrderId,orderId));
    }

    /**
     * 列出单个活动最新的参与人员
     * @param activityId
     * @param count
     * @return
     */
    default Page<ActivityParticipate> listLatestParticipates(String activityId,Integer count){
        return listLatestParticipates(newArrayList(activityId),count).get(activityId);
    }
    /**
     * 列出每一个活动最新的参与数据
     * @param activityIds
     * @param count
     * @return
     */
    public Map<String, Page<ActivityParticipate>> listLatestParticipates(List<String> activityIds, Integer count);

    /**
     * 分页查询活动下所有报名信息
     * @param listPageForm
     * @return
     */
    default public IPage<ActivityParticipateVo> participatePage(ActivityParticipatePageQueryForm1 listPageForm) {
        Page page = convertPage(listPageForm);
        Page page1 = page(page, Wrappers.<ActivityParticipate>lambdaQuery()
                .eq(ActivityParticipate::getActivityId, listPageForm.getActivityId())
                .eq(ActivityParticipate::getIsQuit,false)
                .orderByDesc(ActivityParticipate::getCreateAt));
        return ActivityParticipateMapStruct.INSTANCE.pagePoToVo(page1);
    }
}
