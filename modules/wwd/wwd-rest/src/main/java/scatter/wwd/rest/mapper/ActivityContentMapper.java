package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.ActivityContent;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 活动内容表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
public interface ActivityContentMapper extends IBaseMapper<ActivityContent> {

}
