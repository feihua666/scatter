package scatter.wwd.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wwd.pojo.form.ActivityUserMutualElectionAddForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionPageQueryForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionUpdateForm;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.rest.mapper.ActivityUserMutualElectionMapper;
import scatter.wwd.rest.service.IActivityUserMutualElectionService;
/**
 * <p>
 * 汪汪队用户互选 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class ActivityUserMutualElectionServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ActivityUserMutualElectionMapper, ActivityUserMutualElection, ActivityUserMutualElectionAddForm, ActivityUserMutualElectionUpdateForm, ActivityUserMutualElectionPageQueryForm> implements IActivityUserMutualElectionService {
    @Override
    public void preAdd(ActivityUserMutualElectionAddForm addForm,ActivityUserMutualElection po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ActivityUserMutualElectionUpdateForm updateForm,ActivityUserMutualElection po) {
        super.preUpdate(updateForm,po);

    }
}
