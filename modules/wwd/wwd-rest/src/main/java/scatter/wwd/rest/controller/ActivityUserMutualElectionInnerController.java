package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.rest.service.IActivityUserMutualElectionService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 汪汪队用户互选 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/inner/activity-user-mutual-election")
public class ActivityUserMutualElectionInnerController extends BaseInnerController<ActivityUserMutualElection> {
 @Autowired
 private IActivityUserMutualElectionService activityUserMutualElectionService;

 public IActivityUserMutualElectionService getService(){
     return activityUserMutualElectionService;
 }
}
