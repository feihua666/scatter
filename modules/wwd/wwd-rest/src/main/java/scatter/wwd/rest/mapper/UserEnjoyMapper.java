package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.UserEnjoy;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户有意思表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserEnjoyMapper extends IBaseMapper<UserEnjoy> {

}
