package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 活动参与 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface ActivityParticipateMapper extends IBaseMapper<ActivityParticipate> {

}
