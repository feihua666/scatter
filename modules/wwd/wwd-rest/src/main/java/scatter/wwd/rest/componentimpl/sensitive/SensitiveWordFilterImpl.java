package scatter.wwd.rest.componentimpl.sensitive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.subject.param.CommentSubjectAddParam;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.rest.componentext.CommentHierarchicalOnAddListener;
import scatter.comment.rest.componentext.CommentSubjectOnAddListener;
import scatter.moment.pojo.param.MomentPublishParam;
import scatter.moment.pojo.po.Moment;
import scatter.moment.rest.componentext.MomentPublishListener;
import scatter.sensitive.pojo.word.dto.SensitiveWordDto;
import scatter.sensitive.pojo.word.param.SensitiveWordParam;
import scatter.sensitive.rest.common.ISensitiveWordFramework;

/**
 * <p>
 * 敏感词过滤，主要针对动态的发布，评论的发布等
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 16:31
 */
@Component
public class SensitiveWordFilterImpl implements MomentPublishListener, CommentSubjectOnAddListener, CommentHierarchicalOnAddListener {

	@Autowired
	private ISensitiveWordFramework iSensitiveWordFramework;


	@Override
	public void preCommentHierarchicalForSubjectAdd(CommentHierarchicalForSubjectAddParam addParam, CommentHierarchical commentHierarchical) {
		commentHierarchical.setContent(filter(commentHierarchical.getContent()));
	}

	@Override
	public void postCommentHierarchicalForSubjectAdd(CommentHierarchicalForSubjectAddParam addParam, CommentHierarchical commentHierarchical) {

	}

	@Override
	public void preCommentHierarchicalForHierarchicalAdd(CommentHierarchicalForHierarchicalAddParam addParam, CommentHierarchical commentHierarchical) {
		commentHierarchical.setContent(filter(commentHierarchical.getContent()));

	}

	@Override
	public void postCommentHierarchicalForHierarchicalAdd(CommentHierarchicalForHierarchicalAddParam addParam, CommentHierarchical commentHierarchical) {

	}

	@Override
	public void preCommentSubjectAdd(CommentSubjectAddParam addParam, CommentSubject subject) {
		subject.setContent(filter(subject.getContent()));

	}

	@Override
	public void postCommentSubjectAdd(CommentSubjectAddParam addParam, CommentSubject subject) {

	}

	@Override
	public void preMomentPublish(MomentPublishParam param, Moment moment) {
		moment.setContent(filter(moment.getContent()));

	}

	@Override
	public void postMomentPublish(MomentPublishParam param, Moment moment) {

	}

	/**
	 * 敏感词过虑替换
	 * @param content
	 * @return
	 */
	private String filter(String content){
		SensitiveWordParam sensitiveWordParam = new SensitiveWordParam();
		sensitiveWordParam.setContent(content);
		SensitiveWordDto sensitiveWordDto = iSensitiveWordFramework.replaceSensitiveWord(sensitiveWordParam);
		return sensitiveWordDto.getContent();
	}
}
