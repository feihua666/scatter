package scatter.wwd.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigAddForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigPageQueryForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigUpdateForm;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.wwd.rest.mapper.ActivityMutualElectionConfigMapper;
import scatter.wwd.rest.service.IActivityMutualElectionConfigService;
/**
 * <p>
 * 活动互选配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class ActivityMutualElectionConfigServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ActivityMutualElectionConfigMapper, ActivityMutualElectionConfig, ActivityMutualElectionConfigAddForm, ActivityMutualElectionConfigUpdateForm, ActivityMutualElectionConfigPageQueryForm> implements IActivityMutualElectionConfigService {
    @Override
    public void preAdd(ActivityMutualElectionConfigAddForm addForm,ActivityMutualElectionConfig po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getActivityId())) {
            // 活动id已存在不能添加
            assertByColumn(addForm.getActivityId(),ActivityMutualElectionConfig::getActivityId,false);
        }

    }

    @Override
    public void preUpdate(ActivityMutualElectionConfigUpdateForm updateForm,ActivityMutualElectionConfig po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getActivityId())) {
            ActivityMutualElectionConfig byId = getById(updateForm.getId());
            // 如果活动id有改动
            if (!isEqual(updateForm.getActivityId(), byId.getActivityId())) {
                // 活动id已存在不能修改
                assertByColumn(updateForm.getActivityId(),ActivityMutualElectionConfig::getActivityId,false);
            }
        }

    }
}
