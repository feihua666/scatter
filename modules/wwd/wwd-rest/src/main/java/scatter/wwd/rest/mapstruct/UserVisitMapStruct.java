package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserVisitAddForm;
import scatter.wwd.pojo.form.UserVisitPageQueryForm;
import scatter.wwd.pojo.form.UserVisitUpdateForm;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.pojo.vo.UserVisitVo;

/**
 * <p>
 * 汪汪队用户访问 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserVisitMapStruct extends IBaseVoMapStruct<UserVisit, UserVisitVo>,
        IBaseAddFormMapStruct<UserVisit, UserVisitAddForm>,
        IBaseUpdateFormMapStruct<UserVisit, UserVisitUpdateForm>,
        IBaseQueryFormMapStruct<UserVisit, UserVisitPageQueryForm> {
    UserVisitMapStruct INSTANCE = Mappers.getMapper( UserVisitMapStruct.class );

}
