package scatter.wwd.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
/**
 * <p>
 * 汪汪队用户互选 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IActivityUserMutualElectionService extends IBaseService<ActivityUserMutualElection> {


}
