package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.UserInvitation;
import scatter.wwd.pojo.vo.UserInvitationVo;
import scatter.wwd.pojo.form.UserInvitationAddForm;
import scatter.wwd.pojo.form.UserInvitationUpdateForm;
import scatter.wwd.pojo.form.UserInvitationPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户邀请表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-invitation")
@Api(tags = "用户邀请")
public class UserInvitationController extends BaseAddUpdateQueryFormController<UserInvitation, UserInvitationVo, UserInvitationAddForm, UserInvitationUpdateForm, UserInvitationPageQueryForm> {


     @Override
	 @ApiOperation("添加用户邀请")
     @PreAuthorize("hasAuthority('UserInvitation:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserInvitationVo add(@RequestBody @Valid UserInvitationAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户邀请")
     @PreAuthorize("hasAuthority('UserInvitation:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserInvitationVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户邀请")
     @PreAuthorize("hasAuthority('UserInvitation:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户邀请")
     @PreAuthorize("hasAuthority('UserInvitation:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserInvitationVo update(@RequestBody @Valid UserInvitationUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户邀请")
    //@PreAuthorize("hasAuthority('UserInvitation:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserInvitationVo> getList(UserInvitationPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户邀请")
    //@PreAuthorize("hasAuthority('UserInvitation:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserInvitationVo> getPage(UserInvitationPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
