package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 活动报名规则表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface ActivityEnrollRuleMapper extends IBaseMapper<ActivityEnrollRule> {

}
