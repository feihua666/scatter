package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
/**
 * <p>
 * 活动互选配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IActivityMutualElectionConfigService extends IBaseService<ActivityMutualElectionConfig> {

    /**
     * 根据编码查询
     * @param activityId
     * @return
     */
    default ActivityMutualElectionConfig getByActivityId(String activityId) {
        Assert.hasText(activityId,"activityId不能为空");
        return getOne(Wrappers.<ActivityMutualElectionConfig>lambdaQuery().eq(ActivityMutualElectionConfig::getActivityId, activityId));
    }

}
