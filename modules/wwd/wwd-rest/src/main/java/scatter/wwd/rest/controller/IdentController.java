package scatter.wwd.rest.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import scatter.identification.pojo.form.*;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.IdentificationConstants;
import scatter.identification.rest.service.IIdentificationService;
import scatter.wwd.rest.WwdConfiguration;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * @Author wb.yang
 * @Date 2021/11/28
 */
@Slf4j
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/identification")
@Api(tags = "用户认证信息信息")
public class IdentController {


    @Resource
    private IIdentificationService iIdentificationService;

    @ApiOperation("添加实名认证")
    @PostMapping("/saveRealName")
    @ResponseStatus(HttpStatus.CREATED)
    public Identification saveRealName(@RequestBody @Valid RealName realName) {
        Identification identification = new Identification();
        identification.setIsAudited(false);
        identification.setName(IdentificationConstants.Identify.real_name.getName());
        identification.setCode(IdentificationConstants.Identify.real_name.name());
        JSONObject parse = JSONUtil.parseObj(realName);
        identification.setContent(parse.toString());
        identification.setUserId("1");
        identification.setApplyAt(LocalDateTime.now());
        identification.setIsAudited(false);
        identification.setIsIdentified(false);
        iIdentificationService.save(identification);
        return iIdentificationService.getById(identification.getId());
    }

    @ApiOperation("添加职业认证")
    @PostMapping("/saveProfession")
    @ResponseStatus(HttpStatus.CREATED)
    public Identification saveProfession(@RequestBody @Valid Profession profession) {
        Identification identification = new Identification();
        identification.setIsAudited(false);
        identification.setName(IdentificationConstants.Identify.profession.getName());
        identification.setCode(IdentificationConstants.Identify.profession.name());
        JSONObject parse = JSONUtil.parseObj(profession);
        identification.setContent(parse.toString());
        identification.setUserId("1");
        identification.setApplyAt(LocalDateTime.now());
        identification.setIsAudited(false);
        identification.setIsIdentified(false);
        iIdentificationService.save(identification);
        return iIdentificationService.getById(identification.getId());
    }

    @ApiOperation("添加学历认证")
    @PostMapping("/saveEducation")
    @ResponseStatus(HttpStatus.CREATED)
    public Identification saveEducation(@RequestBody @Valid Education education) {
        Identification identification = new Identification();
        identification.setIsAudited(false);
        identification.setName(IdentificationConstants.Identify.education.getName());
        identification.setCode(IdentificationConstants.Identify.education.name());
        JSONObject parse = JSONUtil.parseObj(education);
        identification.setContent(parse.toString());
        identification.setUserId("1");
        identification.setApplyAt(LocalDateTime.now());
        identification.setIsAudited(false);
        identification.setIsIdentified(false);
        iIdentificationService.save(identification);
        return iIdentificationService.getById(identification.getId());
    }
}
