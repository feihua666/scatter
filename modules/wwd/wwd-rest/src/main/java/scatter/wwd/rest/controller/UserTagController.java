package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.UserTag;
import scatter.wwd.pojo.vo.UserTagVo;
import scatter.wwd.pojo.form.UserTagAddForm;
import scatter.wwd.pojo.form.UserTagUpdateForm;
import scatter.wwd.pojo.form.UserTagPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户标签表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-tag")
@Api(tags = "用户标签")
public class UserTagController extends BaseAddUpdateQueryFormController<UserTag, UserTagVo, UserTagAddForm, UserTagUpdateForm, UserTagPageQueryForm> {


     @Override
	 @ApiOperation("添加用户标签")
     @PreAuthorize("hasAuthority('UserTag:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserTagVo add(@RequestBody @Valid UserTagAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户标签")
     @PreAuthorize("hasAuthority('UserTag:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserTagVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户标签")
     @PreAuthorize("hasAuthority('UserTag:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户标签")
     @PreAuthorize("hasAuthority('UserTag:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserTagVo update(@RequestBody @Valid UserTagUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户标签")
    //@PreAuthorize("hasAuthority('UserTag:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserTagVo> getList(UserTagPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户标签")
    @PreAuthorize("hasAuthority('UserTag:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserTagVo> getPage(UserTagPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
