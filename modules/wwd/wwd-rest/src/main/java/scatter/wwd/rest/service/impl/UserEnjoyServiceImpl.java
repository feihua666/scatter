package scatter.wwd.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.security.LoginUserTool;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.common.rest.tools.BooleanTool;
import scatter.common.rest.validation.DictService;
import scatter.wwd.pojo.form.UserEnjoyAddForm;
import scatter.wwd.pojo.form.UserEnjoyPageQueryForm;
import scatter.wwd.pojo.form.UserEnjoyUpdateForm;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.pojo.vo.UserEnjoyStatusVo;
import scatter.wwd.pojo.vo.UserEnjoyVo;
import scatter.wwd.rest.mapper.UserEnjoyMapper;
import scatter.wwd.rest.service.IUserEnjoyService;
import scatter.wwd.rest.service.IUserInfoService;

import java.util.Optional;

/**
 * <p>
 * 用户有意思表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Slf4j
@Service
@Transactional
public class UserEnjoyServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserEnjoyMapper, UserEnjoy, UserEnjoyAddForm, UserEnjoyUpdateForm, UserEnjoyPageQueryForm> implements IUserEnjoyService {

    @Autowired
    private IUserInfoService iUserInfoService;

    @Autowired
    DictService dictService;

    @Override
    public void preAdd(UserEnjoyAddForm addForm, UserEnjoy po) {
        super.preAdd(addForm, po);

        // 校验是否可以添加有意思
        UserEnjoyStatusVo userEnjoyStatus = getUserEnjoyStatus(addForm.getUserId(), addForm.getEnjoyedUserId());
        if (!userEnjoyStatus.getIsCanEnjoy()) {
            throw new BusinessException(userEnjoyStatus.getErrorMsgForIsCanEnjoyFalse());
        }

        // 先判断是否已经有意思，如果已经有意思，不能重复添加
        int count = count(Wrappers.<UserEnjoy>lambdaQuery().eq(UserEnjoy::getUserId, addForm.getUserId()).eq(UserEnjoy::getEnjoyedUserId, addForm.getEnjoyedUserId()));
        if (count > 0) {
            throw new BusinessException("不能重复添加哦");
        }
    }

    @Override
    public void postAdd(UserEnjoyAddForm userEnjoyAddForm, UserEnjoy po) {
        // todo 发消息告诉对方
    }

    @Override
    public void preUpdate(UserEnjoyUpdateForm updateForm, UserEnjoy po) {
        super.preUpdate(updateForm, po);

    }

    @Override
    public UserEnjoyStatusVo getUserEnjoyStatus(String userId, String enjoyedUserId) {
        UserEnjoy enjoy = getByUserIdAndEnjoyedUserId(userId, enjoyedUserId);
        UserEnjoy enjoyed = getByUserIdAndEnjoyedUserId(enjoyedUserId, userId);
        UserEnjoyStatusVo userEnjoyStatusVo = new UserEnjoyStatusVo();
        userEnjoyStatusVo.setUserId(userId);
        userEnjoyStatusVo.setEnjoyedUserId(enjoyedUserId);
        userEnjoyStatusVo.setIsEnjoy(enjoy != null);
        userEnjoyStatusVo.setIsEnjoyed(enjoyed != null);

        boolean canEnjoy = true;
        String errorMsgForIsCanEnjoyFalse = null;
        // 如果还没有有意思
        if (enjoy == null) {
            UserInfo byUserId = iUserInfoService.getByUserId(userId);
            UserInfo byUserId1 = iUserInfoService.getByUserId(enjoyedUserId);
            Assert.notNull(byUserId, "userId=" + userId + " 对应用户信息不存在");
            Assert.notNull(byUserId1, "enjoyedUserId=" + enjoyedUserId + " 对应用户信息不存在");
            // 资料不全
            if (isStrAnyEmpty(byUserId.getGenderDictId(), byUserId.getMaritalStatusDictId())) {
                log.info("资料不全，userId={}", userId);
                errorMsgForIsCanEnjoyFalse = "您的资料还未完善，请先完善资料吧";
                canEnjoy = false;
            }
            // 对方资料不全
            if (canEnjoy && isStrAnyEmpty(byUserId.getGenderDictId(), byUserId.getMaritalStatusDictId())) {
                log.info("对方资料不全，enjoyedUserId={}", enjoyedUserId);
                errorMsgForIsCanEnjoyFalse = "对方资料还未完善";

                canEnjoy = false;
            }
            if (canEnjoy) {
                // 性别相同或任何一个已婚都不能有意思
                BooleanTool btOr = BooleanTool.or(BooleanTool.bc(isEqual(byUserId.getGenderDictId(), byUserId1.getGenderDictId()), "性别相同"),
                        BooleanTool.bc(dictService.isEqualValueByDictItemId(byUserId.getMaritalStatusDictId(), PublicDictEnums.DictMarriedStatus.married_status_married.name()), "您已婚"),
                        BooleanTool.bc(dictService.isEqualValueByDictItemId(byUserId1.getMaritalStatusDictId(), PublicDictEnums.DictMarriedStatus.married_status_married.name()), "对方已婚"));

                if (btOr.result()) {
                    log.info("性别相同或期中一个已婚，userId={},enjoyedUserId={},最终结果={}", userId, enjoyedUserId, btOr.resultMsg());
                    errorMsgForIsCanEnjoyFalse = btOr.resultMsg();
                    canEnjoy = false;
                }
            }

        } else {
            // 已经有意思不能重复再有意思
            canEnjoy = false;
            errorMsgForIsCanEnjoyFalse = "已经有意思";
        }


        userEnjoyStatusVo.setIsCanEnjoy(canEnjoy);
        userEnjoyStatusVo.setErrorMsgForIsCanEnjoyFalse(errorMsgForIsCanEnjoyFalse);

        return userEnjoyStatusVo;
    }

}
