package scatter.wwd.rest.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import scatter.wwd.pojo.form.ActivityPageQueryForm;
import scatter.wwd.pojo.po.Activity;
import scatter.common.rest.service.IBaseMapper;
import scatter.wwd.pojo.vo.AppActivityListVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 活动表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
public interface ActivityMapper extends IBaseMapper<Activity> {

    IPage<Activity> userParticipateActivityPage(Page page,@Param("userId") String userId);
}
