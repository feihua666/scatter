package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.form.ActivityParticipatePageQueryForm1;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.pojo.vo.ActivityParticipateVo;
import scatter.wwd.pojo.form.ActivityParticipateAddForm;
import scatter.wwd.pojo.form.ActivityParticipateUpdateForm;
import scatter.wwd.pojo.form.ActivityParticipatePageQueryForm;
import scatter.wwd.rest.service.IActivityParticipateService;
import scatter.wwd.rest.service.IUserInfoService;

import javax.validation.Valid;
/**
 * <p>
 * 活动参与 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/activity-participate")
@Api(tags = "活动参与")
public class ActivityParticipateController extends BaseAddUpdateQueryFormController<ActivityParticipate, ActivityParticipateVo, ActivityParticipateAddForm, ActivityParticipateUpdateForm, ActivityParticipatePageQueryForm> {


    @Autowired
    private IActivityParticipateService iActivityParticipateService;
    @Autowired
    private IUserInfoService iUserInfoService;


     @ApiOperation("添加活动参与")
     @PreAuthorize("hasAuthority('ActivityParticipate:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityParticipateVo add(@RequestBody @Valid ActivityParticipateAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询活动参与")
     @PreAuthorize("hasAuthority('ActivityParticipate:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public ActivityParticipateVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除活动参与")
     @PreAuthorize("hasAuthority('ActivityParticipate:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新活动参与")
     @PreAuthorize("hasAuthority('ActivityParticipate:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityParticipateVo update(@RequestBody @Valid ActivityParticipateUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询活动参与")
    @PreAuthorize("hasAuthority('ActivityParticipate:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ActivityParticipateVo> getList(ActivityParticipatePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询活动参与")
    @PreAuthorize("hasAuthority('ActivityParticipate:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ActivityParticipateVo> getPage(ActivityParticipatePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("分页查询活动参与人员")
    @GetMapping("/participatePage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ActivityParticipateVo> participatePage(@Valid ActivityParticipatePageQueryForm1 listPageForm) {
        IPage<ActivityParticipateVo> activityParticipateVoIPage = iActivityParticipateService.participatePage(listPageForm);
        if (activityParticipateVoIPage != null && activityParticipateVoIPage.getRecords() != null) {
            List<String> userIds = activityParticipateVoIPage.getRecords().stream().map(ActivityParticipateVo::getUserId).collect(Collectors.toList());
            List<UserInfo> byUserIds = iUserInfoService.getByUserIds(userIds);
            if (!isEmpty(byUserIds)) {
                Map<String, String> userIdGenderMap = byUserIds.stream().collect(Collectors.toMap(userInfo -> userInfo.getId(), userInfo -> userInfo.getGenderDictId()));

                activityParticipateVoIPage.getRecords().stream().forEach(activityParticipateVo -> {
                    activityParticipateVo.setUserGenderDictId(userIdGenderMap.get(activityParticipateVo.getUserId()));
                });
            }
        }
        return activityParticipateVoIPage;
    }
}
