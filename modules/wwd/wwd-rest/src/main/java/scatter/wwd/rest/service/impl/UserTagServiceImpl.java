package scatter.wwd.rest.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;
import scatter.wwd.pojo.form.UserTagAddForm;
import scatter.wwd.pojo.form.UserTagPageQueryForm;
import scatter.wwd.pojo.form.UserTagUpdateForm;
import scatter.wwd.pojo.form.app.UserTagForm;
import scatter.wwd.pojo.po.UserTag;
import scatter.wwd.rest.mapper.UserTagMapper;
import scatter.wwd.rest.mapstruct.UserTagMapStruct;
import scatter.wwd.rest.service.IUserTagService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户标签表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserTagServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserTagMapper, UserTag, UserTagAddForm, UserTagUpdateForm, UserTagPageQueryForm> implements IUserTagService {

    @Autowired
    private IDictService iDictService;


    @Override
    public void preAdd(UserTagAddForm addForm,UserTag po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserTagUpdateForm updateForm,UserTag po) {
        super.preUpdate(updateForm,po);

    }

    @Override
	@Transactional(rollbackFor = Exception.class)
    public void userTagEdit(UserTagForm form) {

        remove(new QueryWrapper<UserTag>().lambda().eq(UserTag::getUserId,form.getUserId()));

        form.getTags().forEach(item->{
            Dict dict = iDictService.getByCode(item.getTypeDictId());
            UserTag userTag = UserTagMapStruct.INSTANCE.addFormToPo(item);
            userTag.setTypeDictId(dict.getId());
            save(userTag);
        });
    }
}
