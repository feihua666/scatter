package scatter.wwd.rest.componentimpl.order;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.notify.NotifyParam;
import scatter.common.rest.notify.NotifyTool;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.param.RefundOrderParam;
import scatter.order.pojo.po.OrderGoods;
import scatter.pay.rest.service.impl.DefaultThirdPayRefundOrderServiceImpl;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import scatter.wwd.rest.service.IActivityParticipateService;
import scatter.wwd.rest.service.IActivityService;
import scatter.wwd.rest.service.IUserInfoService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-07 23:23
 */
@Service
@Slf4j
public class ActivityRefundOrderServiceImpl extends DefaultThirdPayRefundOrderServiceImpl {

	@Autowired
	private IActivityEnrollRuleService iActivityEnrollRuleService;
	@Autowired
	private IActivityService iActivityService;

	@Autowired
	private IUserInfoService iUserInfoService;

	@Autowired
	private IActivityParticipateService iActivityParticipateService;

	@Autowired
	private DictService dictService;

	@Override
	protected boolean preRefund(RefundOrderParam param) {
		boolean superValid = super.preRefund(param);
		if (!superValid) {
			return superValid;
		}
		// 校验如果退款规则限制则不能退款
		List<OrderGoods> byOrderId = iOrderGoodsService.getByOrderNo(param.getOrderNo());
		String activityId = byOrderId.get(0).getGoodsId();
		ActivityEnrollRule byActivityId = iActivityEnrollRuleService.getByActivityId(activityId);
		if (byActivityId.getRefundDeadline() != null) {
			if (LocalDateTime.now().isAfter(byActivityId.getRefundDeadline())) {
				throw new BusinessException("退款失败，已过取消时间");
			}
		}
		return true;
	}

	@Override
	protected void postRefund(RefundOrderParam param) {
		super.postRefund(param);
		// 获取订单商品，此处为活动信息
		List<OrderGoods> byOrderId = iOrderGoodsService.getByOrderNo(param.getOrderNo());
		OrderGoods activity = byOrderId.get(0);


		// 退款报名人员减 1
		iActivityEnrollRuleService.updateNum(activity.getGoodsId(),activity.getUserId(),-1);

		// 状态修改为退出
		// 更新参与状态
		String activityId = byOrderId.get(0).getGoodsId();
		String userId = byOrderId.get(0).getUserId();
		String orderNo = byOrderId.get(0).getOrderNo();

		ActivityParticipate participateServiceOne = iActivityParticipateService.getByActivityIdAndUserIdAndOrderNo(activityId,userId,orderNo);
		if (participateServiceOne != null) {
			participateServiceOne.setIsQuit(true);
			iActivityParticipateService.updateById(participateServiceOne);
		}


		// 退款完成，业务通知

		UserInfo byUserId = iUserInfoService.getByUserId(activity.getUserId());

		NotifyParam notifyParam = NotifyParam.business()
				.setTitle("活动退款通知")
				.setContent(StrUtil.format("用户 {} 退出了活动 {}，已完成退款",byUserId.getNickname(),activity.getGoodsName()));
		NotifyTool.notify(notifyParam);
	}

	@Override
	public boolean support(String channel, String CategoryDictValue) {
		return true;
	}
}
