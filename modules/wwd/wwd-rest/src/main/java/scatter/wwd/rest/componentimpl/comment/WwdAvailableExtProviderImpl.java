package scatter.wwd.rest.componentimpl.comment;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.area.pojo.po.Area;
import scatter.area.rest.service.IAreaService;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalAvailableVo;
import scatter.comment.pojo.subject.vo.CommentSubjectAvailableVo;
import scatter.comment.rest.componentext.CommentHierarchicalAvailableExtProvider;
import scatter.comment.rest.componentext.CommentSubjectAvailableExtProvider;
import scatter.common.rest.validation.DictService;
import scatter.moment.pojo.vo.AvailableMomentVo;
import scatter.moment.rest.componentext.MomentAvailableExtProvider;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.service.IUserInfoService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 16:25
 */
@Component
public class WwdAvailableExtProviderImpl implements MomentAvailableExtProvider , CommentSubjectAvailableExtProvider, CommentHierarchicalAvailableExtProvider {

	@Autowired
	private IUserInfoService iUserInfoService;
	@Autowired
	private DictService dictService;

	@Autowired
	private IAreaService iAreaService;

	@Override
	public void provideMoment(List<AvailableMomentVo> momentVoList) {
		List<String> userIds = momentVoList.stream().map(AvailableMomentVo::getOwnerUserId).collect(Collectors.toList());

		List<UserInfo> byUserIds = iUserInfoService.getByUserIds(new ArrayList<>(userIds));

		for (AvailableMomentVo momentVo : momentVoList) {
			for (UserInfo byUserId : byUserIds) {
				if(StrUtil.equals(momentVo.getOwnerUserId(),byUserId.getUserId())){

					momentVo.getExt().putAll(getExt(byUserId));
					break;
				}
			}
		}

	}


	@Override
	public void provideCommentSubject(List<CommentSubjectAvailableVo> momentVoList) {
		List<String> userIds = momentVoList.stream().map(CommentSubjectAvailableVo::getOwnerUserId).collect(Collectors.toList());

		List<UserInfo> byUserIds = iUserInfoService.getByUserIds(new ArrayList<>(userIds));

		for (CommentSubjectAvailableVo momentVo : momentVoList) {
			for (UserInfo byUserId : byUserIds) {
				if(StrUtil.equals(momentVo.getOwnerUserId(),byUserId.getUserId())){
					momentVo.getExt().putAll(getExt(byUserId));
					break;
				}
			}
		}
	}

	/**
	 * 添加额外信息
	 * @param byUserId
	 * @return
	 */
	private Map<String,Object> getExt(UserInfo byUserId){
		Map<String,Object> map = new HashMap<>();
		map.put("genderDictValue", dictService.getValueById(byUserId.getGenderDictId()));
		map.put("birthDay", byUserId.getBirthDay());
		//map.put("nowAreaName",null);
		// 当前在哪
		if (StrUtil.isNotEmpty(byUserId.getNowAreaId())) {
			List<Area> allParentsAndSelf = iAreaService.getAllParentsAndSelf(byUserId.getNowAreaId());
			String nowAreaName = allParentsAndSelf.stream().filter(area -> area.getLevel() > 1).map(Area::getName).collect(Collectors.joining(" "));
			map.put("nowAreaName",nowAreaName);
		}
		return map;
	}

	@Override
	public void provideCommentHierarchical(List<CommentHierarchicalAvailableVo> vos) {
		List<String> userIds = vos.stream().map(CommentHierarchicalAvailableVo::getOwnerUserId).collect(Collectors.toList());

		List<UserInfo> byUserIds = iUserInfoService.getByUserIds(new ArrayList<>(userIds));

		for (CommentHierarchicalAvailableVo vo : vos) {
			for (UserInfo byUserId : byUserIds) {
				if(StrUtil.equals(vo.getOwnerUserId(),byUserId.getUserId())){
					vo.getExt().putAll(getExt(byUserId));
					break;
				}
			}
		}
	}
}
