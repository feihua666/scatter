package scatter.wwd.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wwd.pojo.form.UserAlbumAddForm;
import scatter.wwd.pojo.form.UserAlbumPageQueryForm;
import scatter.wwd.pojo.form.UserAlbumUpdateForm;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.rest.mapper.UserAlbumMapper;
import scatter.wwd.rest.service.IUserAlbumService;
/**
 * <p>
 * 用户相册表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserAlbumServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserAlbumMapper, UserAlbum, UserAlbumAddForm, UserAlbumUpdateForm, UserAlbumPageQueryForm> implements IUserAlbumService {
    @Override
    public void preAdd(UserAlbumAddForm addForm,UserAlbum po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserAlbumUpdateForm updateForm,UserAlbum po) {
        super.preUpdate(updateForm,po);

    }
}
