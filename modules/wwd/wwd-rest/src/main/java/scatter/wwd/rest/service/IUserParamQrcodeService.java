package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.UserParamQrcode;
/**
 * <p>
 * 用户标签表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserParamQrcodeService extends IBaseService<UserParamQrcode> {


	/**
	 * 根据用户id获取，目前一个用户只有一个
	 * @param userId
	 * @return
	 */
	default UserParamQrcode getByUserId(String userId){
		return getOne(Wrappers.<UserParamQrcode>lambdaQuery().eq(UserParamQrcode::getUserId,userId));
	}
}
