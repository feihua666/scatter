package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.vo.ActivityEnrollRuleVo;
import scatter.wwd.pojo.form.ActivityEnrollRuleAddForm;
import scatter.wwd.pojo.form.ActivityEnrollRuleUpdateForm;
import scatter.wwd.pojo.form.ActivityEnrollRulePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 活动报名规则表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/activity-enroll-rule")
@Api(tags = "活动报名规则")
public class ActivityEnrollRuleController extends BaseAddUpdateQueryFormController<ActivityEnrollRule, ActivityEnrollRuleVo, ActivityEnrollRuleAddForm, ActivityEnrollRuleUpdateForm, ActivityEnrollRulePageQueryForm> {


     @ApiOperation("添加活动报名规则")
     @PreAuthorize("hasAuthority('ActivityEnrollRule:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityEnrollRuleVo add(@RequestBody @Valid ActivityEnrollRuleAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询活动报名规则")
     @PreAuthorize("hasAuthority('ActivityEnrollRule:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public ActivityEnrollRuleVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除活动报名规则")
     @PreAuthorize("hasAuthority('ActivityEnrollRule:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新活动报名规则")
     @PreAuthorize("hasAuthority('ActivityEnrollRule:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityEnrollRuleVo update(@RequestBody @Valid ActivityEnrollRuleUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询活动报名规则")
    @PreAuthorize("hasAuthority('ActivityEnrollRule:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ActivityEnrollRuleVo> getList(ActivityEnrollRulePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询活动报名规则")
    @PreAuthorize("hasAuthority('ActivityEnrollRule:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ActivityEnrollRuleVo> getPage(ActivityEnrollRulePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
