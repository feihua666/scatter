package scatter.wwd.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.dict.rest.service.IDictService;
import scatter.wwd.pojo.WwdConstants;
import scatter.wwd.pojo.form.UserVisitAddForm;
import scatter.wwd.pojo.form.UserVisitPageQueryForm;
import scatter.wwd.pojo.form.UserVisitUpdateForm;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.pojo.vo.UserVisitVo;
import scatter.wwd.rest.mapper.UserVisitMapper;
import scatter.wwd.rest.mapstruct.UserVisitMapStruct;
import scatter.wwd.rest.service.IUserVisitService;

import java.util.List;

/**
 * <p>
 * 汪汪队用户访问 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserVisitServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserVisitMapper, UserVisit, UserVisitAddForm, UserVisitUpdateForm, UserVisitPageQueryForm> implements IUserVisitService {

    @Autowired
    private IDictService iDictService;

    @Override
    public void preAdd(UserVisitAddForm addForm,UserVisit po) {
        super.preAdd(addForm,po);

        po.setIsRead(false);
        // 设置字典id
        String idByGroupCodeAndValue = iDictService.getIdByGroupCodeAndValue(WwdConstants.VisitTypeDictGroup.wwd_visit_type.groupCode(), addForm.getVisitTypeDictValue());
        po.setVisitTypeDictId(idByGroupCodeAndValue);
    }

    @Override
    public void preUpdate(UserVisitUpdateForm updateForm,UserVisit po) {
        super.preUpdate(updateForm,po);

    }

}
