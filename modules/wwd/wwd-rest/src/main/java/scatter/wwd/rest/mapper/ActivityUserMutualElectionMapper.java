package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 汪汪队用户互选 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface ActivityUserMutualElectionMapper extends IBaseMapper<ActivityUserMutualElection> {

}
