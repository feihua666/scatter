package scatter.wwd.rest.componentimpl.notify;

import org.springframework.stereotype.Component;
import scatter.common.rest.notify.NotifyParam;
import scatter.wxcp.rest.notify.DefaultWxCpNotifyListenerImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-25 20:16
 */
@Component
public class WxCpNotify extends DefaultWxCpNotifyListenerImpl {

	@Override
	protected boolean support(NotifyParam param) {
		return true;
	}
}
