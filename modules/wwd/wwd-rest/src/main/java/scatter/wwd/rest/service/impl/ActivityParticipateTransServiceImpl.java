package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.rest.service.IActivityParticipateService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 活动参与翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class ActivityParticipateTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IActivityParticipateService activityParticipateService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ActivityParticipate.TRANS_ACTIVITYPARTICIPATE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ActivityParticipate.TRANS_ACTIVITYPARTICIPATE_BY_ID)) {
            ActivityParticipate byId = activityParticipateService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ActivityParticipate.TRANS_ACTIVITYPARTICIPATE_BY_ID)) {
            return activityParticipateService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
