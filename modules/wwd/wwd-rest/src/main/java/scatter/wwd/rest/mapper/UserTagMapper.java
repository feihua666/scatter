package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.UserTag;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户标签表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserTagMapper extends IBaseMapper<UserTag> {

}
