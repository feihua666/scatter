package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.wwd.rest.service.IActivityMutualElectionConfigService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 活动互选配置翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class ActivityMutualElectionConfigTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IActivityMutualElectionConfigService activityMutualElectionConfigService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ActivityMutualElectionConfig.TRANS_ACTIVITYMUTUALELECTIONCONFIG_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ActivityMutualElectionConfig.TRANS_ACTIVITYMUTUALELECTIONCONFIG_BY_ID)) {
            ActivityMutualElectionConfig byId = activityMutualElectionConfigService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ActivityMutualElectionConfig.TRANS_ACTIVITYMUTUALELECTIONCONFIG_BY_ID)) {
            return activityMutualElectionConfigService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
