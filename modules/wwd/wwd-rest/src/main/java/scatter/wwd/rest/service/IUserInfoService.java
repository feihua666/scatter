package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.LoginUser;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.form.UserInfoPageQueryForm;
import scatter.wwd.pojo.form.UserInfoPageSameCityQueryForm;
import scatter.wwd.pojo.form.UserInfoUpdateForm1;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.pojo.vo.UserInfoVo;

import java.util.List;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserInfoService extends IBaseService<UserInfo> {

    /**
     * 用户推荐用户
     * @param loginUser 登录的用户，可以为null
     * @param queryForm 如果用户未登录，可以选择一个性别进行异性推荐
     * @return
     */
    List<UserInfo> recommend(LoginUser loginUser, UserInfoPageQueryForm queryForm);

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default UserInfo getByUserId(String userId){
        Assert.hasText(userId, "userId不能为空");
        return getOne(Wrappers.<UserInfo>lambdaQuery().eq(UserInfo::getUserId,userId));
    }

    /**
     * 根据用户id查询
     * @param userIds
     * @return
     */
    default List<UserInfo> getByUserIds(List<String> userIds){
        if (isEmpty(userIds)) {
            return null;
        }
        return list(Wrappers.<UserInfo>lambdaQuery().in(UserInfo::getUserId,userIds));
    }

    /**
     * 获取同城用户信息
     * @param queryForm
     * @return
     */
    IPage<UserInfo> getPageBySameCity(UserInfoPageSameCityQueryForm queryForm);

    /**
     * 获取异性性别字典id
     * @param genderDictId
     * @return
     */
    public String getAnotherGenderDictId(String genderDictId);

}
