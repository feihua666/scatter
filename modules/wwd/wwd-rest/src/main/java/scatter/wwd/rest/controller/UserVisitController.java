package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.security.LoginUserTool;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.pojo.vo.UserVisitVo;
import scatter.wwd.pojo.form.UserVisitAddForm;
import scatter.wwd.pojo.form.UserVisitUpdateForm;
import scatter.wwd.pojo.form.UserVisitPageQueryForm;
import scatter.wwd.rest.service.IUserVisitService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 汪汪队用户访问 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Slf4j
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-visit")
@Api(tags = "汪汪队用户访问")
public class UserVisitController extends BaseAddUpdateQueryFormController<UserVisit, UserVisitVo, UserVisitAddForm, UserVisitUpdateForm, UserVisitPageQueryForm> {

    @Autowired
    private IUserVisitService iUserVisitService;

     @Override
     @ApiOperation("添加汪汪队用户访问")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserVisitVo add(@RequestBody @Valid UserVisitAddForm addForm) {
         LoginUser loginUser = LoginUserTool.getLoginUser();
         if (loginUser == null) {
             // 当前登录用户为空，不添加用户访问记录
             log.warn("当前登录用户为空，不添加用户访问记录 visitUserId=",addForm.getVisitUserId());
         }else {
             addForm.setUserId(loginUser.getId());
         }
         return super.add(addForm);
     }

     @Override
     @ApiOperation("根据ID查询汪汪队用户访问")
     @PreAuthorize("hasAuthority('UserVisit:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserVisitVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
     @ApiOperation("根据ID删除汪汪队用户访问")
     @PreAuthorize("hasAuthority('UserVisit:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
     @ApiOperation("根据ID更新汪汪队用户访问")
     @PreAuthorize("hasAuthority('UserVisit:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserVisitVo update(@RequestBody @Valid UserVisitUpdateForm updateForm) {
         return super.update(updateForm);
     }


     @ApiOperation("根据当前用户ID更新汪汪队用户访问")
     @PreAuthorize("hasAuthority('UserVisit:update')")
     @PutMapping("/read")
     @ResponseStatus(HttpStatus.CREATED)
     public void updateRead() {
         LoginUser loginUser = LoginUserTool.getLoginUser();
         if (loginUser != null) {
             iBaseService.update(new UserVisit().setIsRead(true).setReadAt(LocalDateTime.now()),new QueryWrapper<UserVisit>().lambda().eq(UserVisit::getVisitUserId,loginUser.getId()));
         }
     }

    @Override
    @ApiOperation("不分页查询汪汪队用户访问")
    @PreAuthorize("hasAuthority('UserVisit:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserVisitVo> getList(UserVisitPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
    @ApiOperation("分页查询汪汪队用户访问")
    @PreAuthorize("hasAuthority('UserVisit:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserVisitVo> getPage(UserVisitPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("分页查询谁看过我")
    @PreAuthorize("hasAuthority('appclient')")
    @GetMapping("/visit-me/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserVisitVo> getVisitMe(@ApiIgnore LoginUser loginUser) {
        UserVisitPageQueryForm userVisitPageQueryForm = new UserVisitPageQueryForm();
        userVisitPageQueryForm.setVisitUserId(loginUser.getId());
        return super.getPage(userVisitPageQueryForm);
    }
    @ApiOperation("分页查询我看过谁")
    @PreAuthorize("hasAuthority('appclient')")
    @GetMapping("/me-visit/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserVisitVo> getMeVisit(@ApiIgnore LoginUser loginUser) {
        UserVisitPageQueryForm userVisitPageQueryForm = new UserVisitPageQueryForm();
        userVisitPageQueryForm.setUserId(loginUser.getId());
        return super.getPage(userVisitPageQueryForm);
    }
}
