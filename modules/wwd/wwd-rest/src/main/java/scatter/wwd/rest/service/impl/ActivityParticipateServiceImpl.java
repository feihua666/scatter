package scatter.wwd.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import scatter.common.dict.PublicDictEnums;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;
import scatter.wwd.pojo.WwdConstants;
import scatter.wwd.pojo.form.ActivityParticipateAddForm;
import scatter.wwd.pojo.form.ActivityParticipatePageQueryForm;
import scatter.wwd.pojo.form.ActivityParticipateUpdateForm;
import scatter.wwd.pojo.form.app.ActivitySingupForm;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.mapper.ActivityParticipateMapper;
import scatter.wwd.rest.mapstruct.ActivityParticipateMapStruct;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import scatter.wwd.rest.service.IActivityParticipateService;
import scatter.wwd.rest.service.IActivityService;
import scatter.wwd.rest.service.IUserInfoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

/**
 * <p>
 * 活动参与 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class ActivityParticipateServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ActivityParticipateMapper, ActivityParticipate, ActivityParticipateAddForm, ActivityParticipateUpdateForm, ActivityParticipatePageQueryForm> implements IActivityParticipateService {
    @Autowired
    private IUserInfoService iUserInfoService;

    @Autowired
    private IDictService iDictService;

    @Autowired
    private IActivityEnrollRuleService iActivityEnrollRuleService;

    @Autowired
    private IActivityService iActivityService;
    @Qualifier("commonDbTaskExecutor")
    @Autowired
    private ExecutorService executorService;

    @Override
    public void preAdd(ActivityParticipateAddForm addForm, ActivityParticipate po) {
        super.preAdd(addForm, po);

    }

    @Override
    public void preUpdate(ActivityParticipateUpdateForm updateForm, ActivityParticipate po) {
        super.preUpdate(updateForm, po);

    }

    @Override
    public Map<String, Page<ActivityParticipate>> listLatestParticipates(List<String> activityIds, Integer count) {
        Assert.notEmpty(activityIds,"activityIds 不能为空");
        Assert.notNull(count,"count 不能为空");


        Map<String,Page<ActivityParticipate>> result = new HashMap<>();

        CompletableFuture[] completableFutures = activityIds.stream().map(activityId -> CompletableFuture.supplyAsync(() -> {
            // 我嚓，这个 page 不能提出来，否则得到的数据会是一样的
            Page page = convertPage(new BasePageQueryForm().setCurrent(1L).setSize(Integer.toUnsignedLong(count)));
            page.setSearchCount(false);
            Page page1 = page(page, Wrappers.<ActivityParticipate>lambdaQuery()
                    .eq(ActivityParticipate::getActivityId, activityId)
                    .eq(ActivityParticipate::getIsQuit, false)
                    .orderByDesc(ActivityParticipate::getCreateAt));
            return page1;
        }, executorService).whenComplete((r,e)->{result.put(activityId, (Page<ActivityParticipate>)r);})).toArray(CompletableFuture[]::new);
        CompletableFuture.allOf(completableFutures).join();
        return result;
    }
}
