package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.ActivityContentAddForm;
import scatter.wwd.pojo.form.ActivityContentPageQueryForm;
import scatter.wwd.pojo.form.ActivityContentUpdateForm;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.pojo.vo.ActivityContentVo;

/**
 * <p>
 * 活动内容 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ActivityContentMapStruct extends IBaseVoMapStruct<ActivityContent, ActivityContentVo>,
        IBaseAddFormMapStruct<ActivityContent, ActivityContentAddForm>,
        IBaseUpdateFormMapStruct<ActivityContent, ActivityContentUpdateForm>,
        IBaseQueryFormMapStruct<ActivityContent, ActivityContentPageQueryForm> {
    ActivityContentMapStruct INSTANCE = Mappers.getMapper( ActivityContentMapStruct.class );

}
