package scatter.wwd.rest.componentimpl.comment;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import scatter.comment.pojo.enjoy.param.CommentEnjoyAddEnjoyParam;
import scatter.comment.pojo.enjoy.param.CommentEnjoyCancelEnjoyParam;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.pojo.rate.param.CommentRateAddRateParam;
import scatter.comment.pojo.rate.param.CommentRateCancelRateParam;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.star.param.CommentStarAddStarParam;
import scatter.comment.pojo.star.param.CommentStarCancelStarParam;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.rest.componentext.CommentEnjoyOnAddOrCancelListener;
import scatter.comment.rest.componentext.CommentRateOnAddOrCancelListener;
import scatter.comment.rest.componentext.CommentStarOnAddOrCancelListener;
import scatter.wwd.pojo.WwdConstants;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.rest.service.IActivityService;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-04 11:52
 */
@Component
public class WwdActivityDealListener implements CommentStarOnAddOrCancelListener, CommentEnjoyOnAddOrCancelListener, CommentRateOnAddOrCancelListener {


	@Lazy
	@Autowired
	private IActivityService iActivityService;

	@Override
	public void onCommentEnjoyAdd(CommentEnjoyAddEnjoyParam addParam, CommentEnjoy commentEnjoy) {
		if (StrUtil.equals(commentEnjoy.getGroupFlag(), WwdConstants.GROUP_FLAG_ACTIVITY_ENJOY)) {
			iActivityService.plusForColumnById(commentEnjoy.getSubjectId(), Activity::getEnjoyCount,1);
		}

	}

	@Override
	public void onCommentEnjoyCancel(CommentEnjoyCancelEnjoyParam addParam) {
		if (StrUtil.equals(addParam.getGroupFlag(), WwdConstants.GROUP_FLAG_ACTIVITY_ENJOY)) {
			iActivityService.plusForColumnById(addParam.getSubjectId(), Activity::getEnjoyCount,-1);
		}
	}

	@Override
	public void onCommentRateAdd(CommentRateAddRateParam addParam, CommentRate commentRate) {
		if (StrUtil.equals(commentRate.getGroupFlag(), WwdConstants.GROUP_FLAG_ACTIVITY_RATE)) {
			iActivityService.plusForColumnById(commentRate.getSubjectId(), Activity::getRateCount,1);
			iActivityService.plusForColumnById(commentRate.getSubjectId(), Activity::getRateSum,commentRate.getRate());
			// 计算实际得分
			iActivityService.calculateRateScore(commentRate.getSubjectId());
		}
	}

	@Override
	public void onCommentRateCancel(CommentRateCancelRateParam addParam, CommentRate commentRate) {
		if (StrUtil.equals(addParam.getGroupFlag(), WwdConstants.GROUP_FLAG_ACTIVITY_RATE)) {
			iActivityService.plusForColumnById(addParam.getSubjectId(), Activity::getRateCount,-1);
			iActivityService.plusForColumnById(commentRate.getSubjectId(), Activity::getRateSum, - commentRate.getRate());
			// 计算实际得分
			iActivityService.calculateRateScore(commentRate.getSubjectId());
		}
	}

	@Override
	public void onCommentStarAdd(CommentStarAddStarParam addParam, CommentStar commentStar) {
		if (StrUtil.equals(commentStar.getGroupFlag(), WwdConstants.GROUP_FLAG_ACTIVITY_RATE)) {
			iActivityService.plusForColumnById(commentStar.getSubjectId(), Activity::getStarCount,1);
		}
	}

	@Override
	public void onCommentStarCancel(CommentStarCancelStarParam addParam) {
		if (StrUtil.equals(addParam.getGroupFlag(), WwdConstants.GROUP_FLAG_ACTIVITY_STAR)) {
			iActivityService.plusForColumnById(addParam.getSubjectId(), Activity::getStarCount,-1);
		}
	}
}
