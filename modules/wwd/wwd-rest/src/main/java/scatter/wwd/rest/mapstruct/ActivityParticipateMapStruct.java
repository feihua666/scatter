package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.ActivityParticipateAddForm;
import scatter.wwd.pojo.form.ActivityParticipatePageQueryForm;
import scatter.wwd.pojo.form.ActivityParticipateUpdateForm;
import scatter.wwd.pojo.form.app.ActivitySingupForm;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.pojo.vo.ActivityParticipateVo;

/**
 * <p>
 * 活动参与 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ActivityParticipateMapStruct extends IBaseVoMapStruct<ActivityParticipate, ActivityParticipateVo>,
        IBaseAddFormMapStruct<ActivityParticipate, ActivityParticipateAddForm>,
        IBaseUpdateFormMapStruct<ActivityParticipate, ActivityParticipateUpdateForm>,
        IBaseQueryFormMapStruct<ActivityParticipate, ActivityParticipatePageQueryForm> {
    ActivityParticipateMapStruct INSTANCE = Mappers.getMapper( ActivityParticipateMapStruct.class );

    ActivityParticipate activitySingupFormToPo(ActivitySingupForm form);
}
