package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.rest.service.IActivityMutualElectionConfigService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 活动互选配置表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/inner/activity-mutual-election-config")
public class ActivityMutualElectionConfigInnerController extends BaseInnerController<ActivityMutualElectionConfig> {
 @Autowired
 private IActivityMutualElectionConfigService activityMutualElectionConfigService;

 public IActivityMutualElectionConfigService getService(){
     return activityMutualElectionConfigService;
 }
}
