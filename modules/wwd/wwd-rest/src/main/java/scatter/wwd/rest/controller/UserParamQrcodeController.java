package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.UserParamQrcode;
import scatter.wwd.pojo.vo.UserParamQrcodeVo;
import scatter.wwd.pojo.form.UserParamQrcodeAddForm;
import scatter.wwd.pojo.form.UserParamQrcodeUpdateForm;
import scatter.wwd.pojo.form.UserParamQrcodePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户标签表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-param-qrcode")
@Api(tags = "用户标签")
public class UserParamQrcodeController extends BaseAddUpdateQueryFormController<UserParamQrcode, UserParamQrcodeVo, UserParamQrcodeAddForm, UserParamQrcodeUpdateForm, UserParamQrcodePageQueryForm> {


     @Override
	 @ApiOperation("添加用户标签")
     @PreAuthorize("hasAuthority('UserParamQrcode:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserParamQrcodeVo add(@RequestBody @Valid UserParamQrcodeAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户标签")
     @PreAuthorize("hasAuthority('UserParamQrcode:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserParamQrcodeVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户标签")
     @PreAuthorize("hasAuthority('UserParamQrcode:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户标签")
     @PreAuthorize("hasAuthority('UserParamQrcode:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserParamQrcodeVo update(@RequestBody @Valid UserParamQrcodeUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户标签")
    @PreAuthorize("hasAuthority('UserParamQrcode:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserParamQrcodeVo> getList(UserParamQrcodePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户标签")
    @PreAuthorize("hasAuthority('UserParamQrcode:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserParamQrcodeVo> getPage(UserParamQrcodePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
