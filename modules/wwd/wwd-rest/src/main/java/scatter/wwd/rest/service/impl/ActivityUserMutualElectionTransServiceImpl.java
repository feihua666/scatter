package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.rest.service.IActivityUserMutualElectionService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 汪汪队用户互选翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class ActivityUserMutualElectionTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IActivityUserMutualElectionService activityUserMutualElectionService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ActivityUserMutualElection.TRANS_ACTIVITYUSERMUTUALELECTION_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ActivityUserMutualElection.TRANS_ACTIVITYUSERMUTUALELECTION_BY_ID)) {
            ActivityUserMutualElection byId = activityUserMutualElectionService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ActivityUserMutualElection.TRANS_ACTIVITYUSERMUTUALELECTION_BY_ID)) {
            return activityUserMutualElectionService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
