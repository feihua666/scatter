package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserInfoAddForm;
import scatter.wwd.pojo.form.UserInfoPageQueryForm;
import scatter.wwd.pojo.form.UserInfoUpdateForm;
import scatter.wwd.pojo.form.UserInfoUpdateForm1;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.pojo.vo.UserInfoDetailVo;
import scatter.wwd.pojo.vo.UserInfoSelfVo;
import scatter.wwd.pojo.vo.UserInfoVo;

/**
 * <p>
 * 用户信息 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserInfoMapStruct extends IBaseVoMapStruct<UserInfo, UserInfoVo>,
        IBaseAddFormMapStruct<UserInfo, UserInfoAddForm>,
        IBaseUpdateFormMapStruct<UserInfo, UserInfoUpdateForm>,
        IBaseQueryFormMapStruct<UserInfo, UserInfoPageQueryForm> {
    UserInfoMapStruct INSTANCE = Mappers.getMapper( UserInfoMapStruct.class );

    UserInfoDetailVo mapDetail(UserInfo vo);
    UserInfoSelfVo poToUserInfoSelfVo(UserInfo vo);

    UserInfo userInfoUpdateForm1ToPo(UserInfoUpdateForm1 form1);
}
