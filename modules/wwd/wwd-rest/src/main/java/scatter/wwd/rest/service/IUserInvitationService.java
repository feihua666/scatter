package scatter.wwd.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.UserInvitation;
/**
 * <p>
 * 用户邀请表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserInvitationService extends IBaseService<UserInvitation> {


}
