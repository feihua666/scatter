package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.rest.service.IActivityContentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 活动内容翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Component
public class ActivityContentTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IActivityContentService activityContentService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ActivityContent.TRANS_ACTIVITYCONTENT_BY_ID,ActivityContent.TRANS_ACTIVITYCONTENT_BY_ACTIVITY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ActivityContent.TRANS_ACTIVITYCONTENT_BY_ID)) {
            ActivityContent byId = activityContentService.getById(key);
            return new TransResult(byId,key);
        }else if(isEqual(type,ActivityContent.TRANS_ACTIVITYCONTENT_BY_ACTIVITY_ID)){

            ActivityContent byId = activityContentService.getByActivityId(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ActivityContent.TRANS_ACTIVITYCONTENT_BY_ID)) {
            return activityContentService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if (isEqual(type,ActivityContent.TRANS_ACTIVITYCONTENT_BY_ACTIVITY_ID)) {
            return activityContentService.getByActivityIds(new ArrayList<>(keys)).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
