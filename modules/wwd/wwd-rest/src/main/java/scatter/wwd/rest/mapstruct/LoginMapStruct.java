package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.wwd.pojo.login.WwdAppLoginUser;
import scatter.wxmp.pojo.login.WxMpLoginUser;

/**
 * Created by yangwei
 * Created at 2021/1/21 17:48
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LoginMapStruct {
    LoginMapStruct INSTANCE = Mappers.getMapper( LoginMapStruct.class );


    @Mapping(target = "authorities",ignore = true)
    WwdAppLoginUser map(WxMpLoginUser wxMpLoginUser);
}
