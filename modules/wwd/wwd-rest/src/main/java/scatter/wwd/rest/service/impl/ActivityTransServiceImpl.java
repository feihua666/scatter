package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.rest.service.IActivityService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 活动翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Component
public class ActivityTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IActivityService activityService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Activity.TRANS_ACTIVITY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Activity.TRANS_ACTIVITY_BY_ID)) {
            Activity byId = activityService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Activity.TRANS_ACTIVITY_BY_ID)) {
            return activityService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
