package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.po.ActivityContent;

import java.util.List;

/**
 * <p>
 * 活动内容表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
public interface IActivityContentService extends IBaseService<ActivityContent> {

    /**
     * 根据编码查询
     * @param activityId
     * @return
     */
    default ActivityContent getByActivityId(String activityId) {
        Assert.hasText(activityId,"activityId不能为空");
        return getOne(Wrappers.<ActivityContent>lambdaQuery().eq(ActivityContent::getActivityId, activityId));
    }
    /**
     * 根据编码查询
     * @param activityIds
     * @return
     */
    default List<ActivityContent> getByActivityIds(List<String> activityIds) {
        Assert.notEmpty(activityIds,"activityIds 不能为空");
        return list(Wrappers.<ActivityContent>lambdaQuery().in(ActivityContent::getActivityId, activityIds));
    }
}
