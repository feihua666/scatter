package scatter.wwd.rest.componentimpl.recommend;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.tools.InterfaceTool;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;
import scatter.recommend.rest.recommend.IRecommender;
import scatter.recommend.rest.recommend.dto.RecommendResultDto;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.mapper.UserInfoMapper;
import scatter.wwd.rest.service.IUserInfoService;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * <p>
 * 汪汪队随机推荐实现
 * </p>
 *
 * @author yangwei
 * @since 2021-08-29 13:28
 */
@Component
public class WwdRandomRecommender implements IRecommender, InterfaceTool {

	@Autowired
	private IDictService iDictService;

	@Autowired
	private UserInfoMapper userInfoMapper;

	@Autowired
	private IUserInfoService iUserInfoService;


	@Override
	public List<RecommendResultDto> recommend(String userId, int recommendNum, String category) {

		String genderDictId = null;
		if (userId != null) {
			UserInfo byUserId = iUserInfoService.getByUserId(userId);
			genderDictId = byUserId.getGenderDictId();

		}


		String queryGenderDictId = iUserInfoService.getAnotherGenderDictId(genderDictId);

		LambdaQueryWrapper<UserInfo> eq = Wrappers.<UserInfo>query().lambda()
				.eq(!isStrEmpty(queryGenderDictId), UserInfo::getGenderDictId, queryGenderDictId)
				.eq(UserInfo::getIsVerified, true);

		// 如果用户已登录每次可以推荐10个用户
		List<UserInfo> recommend = userInfoMapper.recommend(eq, recommendNum);
		return recommend.stream().map(u -> new RecommendResultDto().setItemId(u.getUserId()).setRecommender(getClass().getSimpleName())).collect(Collectors.toList());
	}



}
