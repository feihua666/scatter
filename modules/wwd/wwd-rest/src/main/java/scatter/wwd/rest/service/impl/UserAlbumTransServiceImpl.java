package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.rest.mapstruct.UserAlbumMapStruct;
import scatter.wwd.rest.service.IUserAlbumService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户相册翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserAlbumTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserAlbumService userAlbumService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,UserAlbum.TRANS_USERALBUM_BY_ID,UserAlbum.TRANS_USERALBUMS_VO_BY_USER_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserAlbum.TRANS_USERALBUM_BY_ID)) {
            UserAlbum byId = userAlbumService.getById(key);
            return new TransResult(byId,key);
        }else if (isEqual(type,UserAlbum.TRANS_USERALBUMS_VO_BY_USER_ID)) {
            List<UserAlbum> byUserId = userAlbumService.getByUserId(key);
            return new TransResult(UserAlbumMapStruct.INSTANCE.posToVos(byUserId),key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserAlbum.TRANS_USERALBUM_BY_ID)) {
            return userAlbumService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if (isEqual(type,UserAlbum.TRANS_USERALBUMS_VO_BY_USER_ID)) {
            List<UserAlbum> byUserId = userAlbumService.getByUserIds(keys);
            Map<String, List<UserAlbum>> map = new HashMap<>();
            for (String key : keys) {
                map.put(key, byUserId.stream().filter(item -> isEqual(key, item.getUserId())).collect(Collectors.toList()));
            }
            List<TransResult<Object, String>> r = new ArrayList<>(keys.size());
            for (String key : map.keySet()) {
                r.add(new TransResult<Object, String>(UserAlbumMapStruct.INSTANCE.posToVos(map.get(key)),key));
            }
            return r;
        }
        return null;
    }
}
