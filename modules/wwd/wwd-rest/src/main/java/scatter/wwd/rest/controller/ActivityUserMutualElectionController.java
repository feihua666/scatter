package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.pojo.vo.ActivityUserMutualElectionVo;
import scatter.wwd.pojo.form.ActivityUserMutualElectionAddForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionUpdateForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 汪汪队用户互选 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/activity-user-mutual-election")
@Api(tags = "汪汪队用户互选")
public class ActivityUserMutualElectionController extends BaseAddUpdateQueryFormController<ActivityUserMutualElection, ActivityUserMutualElectionVo, ActivityUserMutualElectionAddForm, ActivityUserMutualElectionUpdateForm, ActivityUserMutualElectionPageQueryForm> {


     @ApiOperation("添加汪汪队用户互选")
     @PreAuthorize("hasAuthority('ActivityUserMutualElection:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityUserMutualElectionVo add(@RequestBody @Valid ActivityUserMutualElectionAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询汪汪队用户互选")
     @PreAuthorize("hasAuthority('ActivityUserMutualElection:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public ActivityUserMutualElectionVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除汪汪队用户互选")
     @PreAuthorize("hasAuthority('ActivityUserMutualElection:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新汪汪队用户互选")
     @PreAuthorize("hasAuthority('ActivityUserMutualElection:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityUserMutualElectionVo update(@RequestBody @Valid ActivityUserMutualElectionUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询汪汪队用户互选")
    @PreAuthorize("hasAuthority('ActivityUserMutualElection:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ActivityUserMutualElectionVo> getList(ActivityUserMutualElectionPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询汪汪队用户互选")
    @PreAuthorize("hasAuthority('ActivityUserMutualElection:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ActivityUserMutualElectionVo> getPage(ActivityUserMutualElectionPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
