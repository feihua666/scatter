package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.form.app.UserTagForm;
import scatter.wwd.pojo.po.UserTag;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户标签表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserTagService extends IBaseService<UserTag> {
    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<UserTag> getByUserId(String userId) {
        Assert.hasText(userId,"用户id不能为空");
        return list(Wrappers.<UserTag>lambdaQuery().eq(UserTag::getUserId, userId));
    }
    /**
     * 根据用户ids查询
     * @param userIds
     * @return
     */
    default List<UserTag> getByUserIds(Set<String> userIds) {
        Assert.notEmpty(userIds,"用户id不能为空");
        return list(Wrappers.<UserTag>lambdaQuery().in(UserTag::getUserId, userIds));
    }

    /**
     * 用户标签编辑
     * @param form
     */
    void userTagEdit(UserTagForm form);
}
