package scatter.wwd.rest.componentimpl.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.order.pojo.vo.OrderVo;
import scatter.pay.pojo.vo.PayVo;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.rest.service.IUserSimpleService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 汪汪队用户订单用户昵称翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-01-21
 */
@Component
public class UserNicknameTransForOrderServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserSimpleService userService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type, OrderVo.TRANS_ORDER_USER_NICKNAME_BY_USER_ID, PayVo.TRANS_USER_NICKNAME_BY_USER_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqualAny(type, OrderVo.TRANS_ORDER_USER_NICKNAME_BY_USER_ID, PayVo.TRANS_USER_NICKNAME_BY_USER_ID)) {
            UserSimple byId = userService.getById(key);
            if (byId == null) {
                return null;
            }
            return new TransResult(byId.getNickname(),key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqualAny(type, OrderVo.TRANS_ORDER_USER_NICKNAME_BY_USER_ID, PayVo.TRANS_USER_NICKNAME_BY_USER_ID)) {
            return userService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item.getNickname(),item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
