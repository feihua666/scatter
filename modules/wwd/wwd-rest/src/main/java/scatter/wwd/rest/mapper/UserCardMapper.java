package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.UserCard;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户卡片表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserCardMapper extends IBaseMapper<UserCard> {

}
