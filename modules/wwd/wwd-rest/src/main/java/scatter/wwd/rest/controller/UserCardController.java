package scatter.wwd.rest.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.UserCard;
import scatter.wwd.pojo.vo.UserCardVo;
import scatter.wwd.pojo.form.UserCardAddForm;
import scatter.wwd.pojo.form.UserCardUpdateForm;
import scatter.wwd.pojo.form.UserCardPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户卡片表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-card")
@Api(tags = "用户卡片")
public class UserCardController extends BaseAddUpdateQueryFormController<UserCard, UserCardVo, UserCardAddForm, UserCardUpdateForm, UserCardPageQueryForm> {


     @Override
	 @ApiOperation("添加用户卡片")
     @PreAuthorize("hasAuthority('UserCard:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserCardVo add(@RequestBody @Valid UserCardAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户卡片")
     @PreAuthorize("hasAuthority('UserCard:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserCardVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }


     @ApiOperation("根据用户ID查询用户卡片")
     //@PreAuthorize("hasAuthority('UserCard:queryByUserId')")
     @GetMapping("/userId/{userId}")
     @ResponseStatus(HttpStatus.OK)
     public UserCardVo queryByUserId(@PathVariable String userId) {
         UserCardPageQueryForm form = new UserCardPageQueryForm();
         form.setUserId(userId);
         List<UserCardVo> list = super.getList(form);
         return CollUtil.isNotEmpty(list)? list.get(0): null;
     }

     @Override
	 @ApiOperation("根据ID删除用户卡片")
     @PreAuthorize("hasAuthority('UserCard:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户卡片")
     @PreAuthorize("hasAuthority('UserCard:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserCardVo update(@RequestBody @Valid UserCardUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户卡片")
    @PreAuthorize("hasAuthority('UserCard:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserCardVo> getList(UserCardPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户卡片")
    @PreAuthorize("hasAuthority('UserCard:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserCardVo> getPage(UserCardPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
