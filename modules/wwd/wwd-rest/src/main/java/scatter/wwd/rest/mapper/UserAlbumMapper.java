package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.UserAlbum;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户相册表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserAlbumMapper extends IBaseMapper<UserAlbum> {

}
