package scatter.wwd.rest.componentimpl.order;

import org.springframework.stereotype.Service;
import scatter.pay.rest.service.impl.ThirdPayQueryOrderFrameworkServiceImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-08 15:30
 */
@Service
public class ActivityQueryOrderFrameworkServiceImpl extends ThirdPayQueryOrderFrameworkServiceImpl {
	@Override
	public boolean support(String channel, String CategoryDictValue) {
		return true;
	}
}
