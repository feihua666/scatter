package scatter.wwd.rest.componentimpl.usersimple;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.LoginUser;
import scatter.common.rest.cache.CacheHelper;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm1;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.rest.ext.UserSimpleUpdateSelfListener;
import scatter.usersimple.rest.service.IUserSimpleService;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-08 18:24
 */
@Component
public class UserSimpleListenerImpl implements UserSimpleUpdateSelfListener {


	@Autowired
	private CacheHelper cacheHelper;
	@Autowired
	private IUserSimpleService iUserSimpleService;


	@Override
	public void onUserSimpleUpdateSelf(UserSimpleUpdateForm1 updateForm, LoginUser loginUser) {
		cacheHelper.removeCacheByParam("ITransBatchServiceCache", UserSimple.TRANS_USERSIMPLE_BY_ID, Lists.newArrayList(loginUser.getId()));
	}
}
