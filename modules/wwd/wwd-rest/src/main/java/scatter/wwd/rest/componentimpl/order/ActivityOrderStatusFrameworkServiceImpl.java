package scatter.wwd.rest.componentimpl.order;

import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.notify.WxPayRefundNotifyResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.param.UpdateOrderStatusParam;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.rest.service.IOrderGoodsService;
import scatter.order.rest.service.IOrderService;
import scatter.pay.rest.service.impl.DefaultThirdPayOrderStatusFrameworkServiceImpl;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import scatter.wwd.rest.service.IActivityParticipateService;
import scatter.wwd.rest.service.IUserInfoService;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-08 19:31
 */
@Slf4j
@Service
public class ActivityOrderStatusFrameworkServiceImpl extends DefaultThirdPayOrderStatusFrameworkServiceImpl {

	@Autowired
	private IActivityParticipateService iActivityParticipateService;

	@Autowired
	private IOrderGoodsService iOrderGoodsService;

	@Autowired
	private IOrderService iOrderService;
	@Autowired
	private DictService dictService;

	@Autowired
	private IActivityEnrollRuleService iActivityEnrollRuleService;

	@Autowired
	private IUserInfoService iUserInfoService;

	@Override
	public boolean support(String channel, String categoryDictValue) {
		return true;
	}

	@Override
	protected void postUpdate(UpdateOrderStatusParam param) {
		super.postUpdate(param);

		OrderConstants.OrderStatusItem orderStatusItem = OrderConstants.OrderStatusItem.valueOf(param.getStatus());
		Boolean isQuit = true;
		// 支付成功添加支付流水
		if (orderStatusItem == OrderConstants.OrderStatusItem.paid) {
			isQuit = false;
		}
		// 退款成功添加退款流水
		else if (orderStatusItem == OrderConstants.OrderStatusItem.refund) {
			isQuit = true;
		}
		if (isQuit != null) {
			List<OrderGoods> byOrderNo = iOrderGoodsService.getByOrderNo(param.getOrderNo());
			if (!isEmpty(byOrderNo)) {
				OrderGoods next = byOrderNo.iterator().next();
				ActivityParticipate byActivityIdAndUserIdAndOrderId = iActivityParticipateService.getByActivityIdAndUserIdAndOrderId(next.getGoodsId(), next.getUserId(), next.getOrderId());
				if (byActivityIdAndUserIdAndOrderId != null) {
					byActivityIdAndUserIdAndOrderId.setIsQuit(false);
					iActivityParticipateService.updateById(byActivityIdAndUserIdAndOrderId);
				}
			}else {
				log.info("未获取到商品信息不处理");
			}
		}else {
			log.info("isQuit 为空不处理");
		}

	}
}
