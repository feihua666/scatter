package scatter.wwd.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.rest.service.IActivityEnrollRuleService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 活动报名规则翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class ActivityEnrollRuleTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IActivityEnrollRuleService activityEnrollRuleService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ActivityEnrollRule.TRANS_ACTIVITYENROLLRULE_BY_ID,ActivityEnrollRule.TRANS_ACTIVITYENROLLRULE_BY_ACTIVITY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ActivityEnrollRule.TRANS_ACTIVITYENROLLRULE_BY_ID)) {
            ActivityEnrollRule byId = activityEnrollRuleService.getById(key);
            return new TransResult(byId,key);
        }
        if (isEqual(type,ActivityEnrollRule.TRANS_ACTIVITYENROLLRULE_BY_ACTIVITY_ID)) {
            ActivityEnrollRule byId = activityEnrollRuleService.getByActivityId(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ActivityEnrollRule.TRANS_ACTIVITYENROLLRULE_BY_ID)) {
            return activityEnrollRuleService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        if (isEqual(type,ActivityEnrollRule.TRANS_ACTIVITYENROLLRULE_BY_ACTIVITY_ID)) {
            return activityEnrollRuleService.list( new QueryWrapper<ActivityEnrollRule>().lambda().in(ActivityEnrollRule::getActivityId,keys)).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
