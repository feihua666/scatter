package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.ActivityUserMutualElectionAddForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionPageQueryForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionUpdateForm;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.pojo.vo.ActivityUserMutualElectionVo;

/**
 * <p>
 * 汪汪队用户互选 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ActivityUserMutualElectionMapStruct extends IBaseVoMapStruct<ActivityUserMutualElection, ActivityUserMutualElectionVo>,
        IBaseAddFormMapStruct<ActivityUserMutualElection, ActivityUserMutualElectionAddForm>,
        IBaseUpdateFormMapStruct<ActivityUserMutualElection, ActivityUserMutualElectionUpdateForm>,
        IBaseQueryFormMapStruct<ActivityUserMutualElection, ActivityUserMutualElectionPageQueryForm> {
    ActivityUserMutualElectionMapStruct INSTANCE = Mappers.getMapper( ActivityUserMutualElectionMapStruct.class );

}
