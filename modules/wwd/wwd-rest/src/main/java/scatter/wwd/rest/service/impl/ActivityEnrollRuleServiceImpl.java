package scatter.wwd.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.common.rest.validation.DictService;
import scatter.wwd.pojo.form.ActivityEnrollRuleAddForm;
import scatter.wwd.pojo.form.ActivityEnrollRulePageQueryForm;
import scatter.wwd.pojo.form.ActivityEnrollRuleUpdateForm;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.rest.mapper.ActivityEnrollRuleMapper;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import scatter.wwd.rest.service.IUserInfoService;

/**
 * <p>
 * 活动报名规则表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class ActivityEnrollRuleServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ActivityEnrollRuleMapper, ActivityEnrollRule, ActivityEnrollRuleAddForm, ActivityEnrollRuleUpdateForm, ActivityEnrollRulePageQueryForm> implements IActivityEnrollRuleService {


    @Autowired
    private DictService dictService;
    @Autowired
    private IUserInfoService iUserInfoService;

    @Override
    public void preAdd(ActivityEnrollRuleAddForm addForm,ActivityEnrollRule po) {
        super.preAdd(addForm,po);
        po.setCountFemale(0);
        po.setCountMale(0);
        if (!isStrEmpty(addForm.getActivityId())) {
            // 活动id已存在不能添加
            assertByColumn(addForm.getActivityId(),ActivityEnrollRule::getActivityId,false);
        }

    }

    @Override
    public void preUpdate(ActivityEnrollRuleUpdateForm updateForm,ActivityEnrollRule po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getActivityId())) {
            ActivityEnrollRule byId = getById(updateForm.getId());
            // 如果活动id有改动
            if (!isEqual(updateForm.getActivityId(), byId.getActivityId())) {
                // 活动id已存在不能修改
                assertByColumn(updateForm.getActivityId(),ActivityEnrollRule::getActivityId,false);
            }
        }

    }



    /**
     * 更新报名人数
     * @param activityId
     * @param userId
     * @param num
     */
    public void updateNum(String activityId,String userId,int num){
            ActivityEnrollRule byActivityId = getByActivityId(activityId);
            UserInfo byUserId = iUserInfoService.getByUserId(userId);
            String valueById = dictService.getValueById(byUserId.getGenderDictId());
            if (PublicDictEnums.DictGender.f.name().equals(valueById)) {
                byActivityId.setCountFemale(byActivityId.getCountFemale() + num);
            }else if (PublicDictEnums.DictGender.m.name().equals(valueById)) {
                byActivityId.setCountMale(byActivityId.getCountMale() + num);
            }
            updateById(byActivityId);
    }
}
