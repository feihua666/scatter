package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 活动互选配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface ActivityMutualElectionConfigMapper extends IBaseMapper<ActivityMutualElectionConfig> {

}
