package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.wwd.pojo.vo.ActivityMutualElectionConfigVo;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigAddForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigUpdateForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 活动互选配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/activity-mutual-election-config")
@Api(tags = "活动互选配置")
public class ActivityMutualElectionConfigController extends BaseAddUpdateQueryFormController<ActivityMutualElectionConfig, ActivityMutualElectionConfigVo, ActivityMutualElectionConfigAddForm, ActivityMutualElectionConfigUpdateForm, ActivityMutualElectionConfigPageQueryForm> {


     @ApiOperation("添加活动互选配置")
     @PreAuthorize("hasAuthority('ActivityMutualElectionConfig:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityMutualElectionConfigVo add(@RequestBody @Valid ActivityMutualElectionConfigAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询活动互选配置")
     @PreAuthorize("hasAuthority('ActivityMutualElectionConfig:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public ActivityMutualElectionConfigVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除活动互选配置")
     @PreAuthorize("hasAuthority('ActivityMutualElectionConfig:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新活动互选配置")
     @PreAuthorize("hasAuthority('ActivityMutualElectionConfig:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityMutualElectionConfigVo update(@RequestBody @Valid ActivityMutualElectionConfigUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询活动互选配置")
    @PreAuthorize("hasAuthority('ActivityMutualElectionConfig:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ActivityMutualElectionConfigVo> getList(ActivityMutualElectionConfigPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询活动互选配置")
    @PreAuthorize("hasAuthority('ActivityMutualElectionConfig:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ActivityMutualElectionConfigVo> getPage(ActivityMutualElectionConfigPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
