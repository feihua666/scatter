package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.form.UserEnjoyAddForm;
import scatter.wwd.pojo.form.UserEnjoyPageQueryForm;
import scatter.wwd.pojo.form.UserEnjoyUpdateForm;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.pojo.vo.UserEnjoyStatusVo;
import scatter.wwd.pojo.vo.UserEnjoyVo;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.rest.service.IUserEnjoyService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
/**
 * <p>
 * 用户有意思表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/user-enjoy")
@Api(tags = "用户有意思")
public class UserEnjoyController extends BaseAddUpdateQueryFormController<UserEnjoy, UserEnjoyVo, UserEnjoyAddForm, UserEnjoyUpdateForm, UserEnjoyPageQueryForm> {


    @Autowired
    private IUserEnjoyService iUserEnjoyService;

     @Override
	 @ApiOperation("添加用户有意思")
     @PreAuthorize("hasAnyAuthority('UserEnjoy:add','appclient')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserEnjoyVo add(@RequestBody @Valid UserEnjoyAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户有意思")
     @PreAuthorize("hasAuthority('UserEnjoy:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserEnjoyVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户有意思")
     @PreAuthorize("hasAuthority('UserEnjoy:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户有意思")
     @PreAuthorize("hasAuthority('UserEnjoy:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserEnjoyVo update(@RequestBody @Valid UserEnjoyUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户有意思")
    @PreAuthorize("hasAuthority('UserEnjoy:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserEnjoyVo> getList(UserEnjoyPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

	@ApiOperation("分页查询用户有意思")
    @PreAuthorize("hasAnyAuthority('appclient')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserEnjoyVo> getPage(@ApiIgnore LoginUser loginUser) {
        UserEnjoyPageQueryForm userEnjoyPageQueryForm = new UserEnjoyPageQueryForm();
        userEnjoyPageQueryForm.setUserId(loginUser.getId());
        return super.getPage(userEnjoyPageQueryForm);
    }

    @ApiOperation("分页查询对我有意思")
    @PreAuthorize("hasAnyAuthority('appclient')")
    @GetMapping("/enjoy-me/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserEnjoyVo> getEnjoyMe(@ApiIgnore LoginUser loginUser) {
        UserEnjoyPageQueryForm userEnjoyPageQueryForm = new UserEnjoyPageQueryForm();
        userEnjoyPageQueryForm.setEnjoyedUserId(loginUser.getId());
        return super.getPage(userEnjoyPageQueryForm);
    }
    @ApiOperation("分页查询我对谁有意思")
    @PreAuthorize("hasAnyAuthority('appclient')")
    @GetMapping("/me-enjoy/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserEnjoyVo> getMeEnjoy(UserEnjoyPageQueryForm listPageForm) {
        return super.getPage(listPageForm);
    }

    @ApiOperation("根据用户查询用户有意思,我对谁有意思")
    @PreAuthorize("hasAnyAuthority('UserEnjoy:queryEnjoy','appclient')")
    @GetMapping("/current/enjoy/{enjoyedUserId}")
    @ResponseStatus(HttpStatus.OK)
    public UserEnjoyVo queryEnjoy(@PathVariable String enjoyedUserId, @ApiIgnore LoginUser loginUser) {
        return getVo(iUserEnjoyService.getByUserIdAndEnjoyedUserId(loginUser.getId(),enjoyedUserId));
    }
    @ApiOperation("根据用户查询用户有意思,谁对我有意思")
    @PreAuthorize("hasAnyAuthority('UserEnjoy:queryEnjoyed','appclient')")
    @GetMapping("/{userId}/enjoy/current")
    @ResponseStatus(HttpStatus.OK)
    public UserEnjoyVo queryEnjoyed(@PathVariable String userId, @ApiIgnore LoginUser loginUser) {
        return getVo(iUserEnjoyService.getByUserIdAndEnjoyedUserId(userId,loginUser.getId()));
    }

    @ApiOperation("获取有意思状态")
    @PreAuthorize("hasAnyAuthority('UserEnjoy:userEnjoyStatus','appclient')")
    @GetMapping("/userEnjoyStatus/{enjoyedUserId}")
    @ResponseStatus(HttpStatus.OK)
    public UserEnjoyStatusVo getUserEnjoyStatus(@ApiParam("用户id") String userId,
                                                @ApiParam("有意思的用户id") @PathVariable String enjoyedUserId,
                                                @ApiIgnore LoginUser loginUser) {
        return iUserEnjoyService.getUserEnjoyStatus(Optional.ofNullable(userId).orElse(loginUser.getId()),enjoyedUserId);
    }
}
