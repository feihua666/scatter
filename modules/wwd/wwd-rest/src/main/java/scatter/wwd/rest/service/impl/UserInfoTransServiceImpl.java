package scatter.wwd.rest.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.dict.pojo.form.DictItemsQueryForm;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.pojo.vo.UserInfoVo;
import scatter.wwd.rest.mapstruct.UserInfoMapStruct;
import scatter.wwd.rest.service.IUserInfoService;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户信息翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserInfoTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserInfoService userInfoService;

    @Autowired
    private IDictService iDictService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type
                ,UserInfo.TRANS_USERINFO_BY_ID
                ,UserInfo.TRANS_USERINFO_BY_USER_ID
                ,UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID
        );
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserInfo.TRANS_USERINFO_BY_ID)) {
            UserInfo byId = userInfoService.getById(key);
            return new TransResult(byId,key);
        }
        if (isEqual(type,UserInfo.TRANS_USERINFO_BY_USER_ID)) {
            UserInfo byId = userInfoService.getByUserId(key);
            return new TransResult(byId,key);
        }
        if (isEqual(type,UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID)) {
            UserInfo byId = userInfoService.getByUserId(key);
            UserInfoVo vo = UserInfoMapStruct.INSTANCE.poToVo(byId);
            if(vo!= null && vo.getGenderDictId()!= null){
                String gender = iDictService.getNameById(byId.getGenderDictId());
                vo.setGenderDictName(gender);
            }
            return new TransResult(vo,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserInfo.TRANS_USERINFO_BY_ID)) {
            return userInfoService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        if (isEqual(type,UserInfo.TRANS_USERINFO_BY_USER_ID)) {
            return userInfoService.list(Wrappers.<UserInfo>lambdaQuery().in(UserInfo::getUserId,keys)).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        if (isEqual(type,UserInfo.TRANS_USERINFO_GENDER_BY_USER_ID)) {
            List<UserInfo> list = userInfoService.list(Wrappers.<UserInfo>lambdaQuery().in(UserInfo::getUserId, keys));
            List<UserInfoVo> userInfoVos = UserInfoMapStruct.INSTANCE.posToVos(list);

            if(CollUtil.isNotEmpty(userInfoVos)){
                DictItemsQueryForm q = new DictItemsQueryForm();
                q.setGroupCode(PublicDictEnums.DictGenderGroupCode.gender.groupCode());
                List<Dict> genders = iDictService.getItemsByGroupCode(q);
                Map<String, Dict> dictMap = genders.stream().collect(Collectors.toMap(Dict::getId, v -> v));
                userInfoVos.stream().filter(f-> StrUtil.isNotBlank(f.getGenderDictId())).forEach(item->{
                    item.setGenderDictName(dictMap.get(item.getGenderDictId()).getName());
                });
            }
            return userInfoVos.stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
