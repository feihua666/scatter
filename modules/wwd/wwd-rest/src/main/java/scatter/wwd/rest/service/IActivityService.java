package scatter.wwd.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;
import scatter.wwd.pojo.WwdConstants;
import scatter.wwd.pojo.form.ActivityPageQueryForm1;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.pojo.vo.ActivityDetailVo;
import scatter.wwd.pojo.vo.ActivityVo1;

/**
 * <p>
 * 活动表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
public interface IActivityService extends IBaseService<Activity> {

	/**
	 * 活动详情，目前用于 wwd h5 详情
	 * @param activityId
	 * @return 只返回已发布的
	 */
	public ActivityDetailVo getActivityDetailById(String activityId,String loginUserId);

	/**
	 * 分页列表，目前适用的 app 活动列表
	 * @param pageQueryForm1
	 * @param currentUserId 当前登录用户的id
	 * @return
	 */
	IPage<ActivityVo1> getPage1(ActivityPageQueryForm1 pageQueryForm1,String currentUserId);

	/**
	 * 查询已参与的活动
	 * @param basePageQueryForm
	 * @param userId
	 * @return
	 */
	IPage<ActivityVo1> getPageForUser(BasePageQueryForm basePageQueryForm, String userId);

	/**
	 * 计算得分
	 * @param activityId
	 * @return
	 */
	default boolean calculateRateScore(String activityId){
		Activity byId = getById(activityId);
		// 评分人数 * 每个人可评分总数
		int totalScore = byId.getRateCount() * WwdConstants.activity_rate_score;
		if (totalScore == 0) {
			return false;
		}
		double score = byId.getRateSum() / totalScore;

		return update(Wrappers.<Activity>lambdaUpdate().set(Activity::getRateScore, score).eq(Activity::getId, activityId));

	}
}
