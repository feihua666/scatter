package scatter.wwd.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wwd.rest.WwdConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.pojo.vo.ActivityContentVo;
import scatter.wwd.pojo.form.ActivityContentAddForm;
import scatter.wwd.pojo.form.ActivityContentUpdateForm;
import scatter.wwd.pojo.form.ActivityContentPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 活动内容表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Api(tags = "活动内容相关接口")
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/activity-content")
public class ActivityContentController extends BaseAddUpdateQueryFormController<ActivityContent, ActivityContentVo, ActivityContentAddForm, ActivityContentUpdateForm, ActivityContentPageQueryForm> {


     @ApiOperation("添加活动内容")
     @PreAuthorize("hasAuthority('ActivityContent:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityContentVo add(@RequestBody @Valid ActivityContentAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询活动内容")
     @PreAuthorize("hasAuthority('ActivityContent:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public ActivityContentVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除活动内容")
     @PreAuthorize("hasAuthority('ActivityContent:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新活动内容")
     @PreAuthorize("hasAuthority('ActivityContent:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public ActivityContentVo update(@RequestBody @Valid ActivityContentUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询活动内容")
    @PreAuthorize("hasAuthority('ActivityContent:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ActivityContentVo> getList(ActivityContentPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询活动内容")
    @PreAuthorize("hasAuthority('ActivityContent:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ActivityContentVo> getPage(ActivityContentPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
