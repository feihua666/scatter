package scatter.wwd.rest.componentimpl.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import scatter.comment.pojo.subject.param.CommentSubjectAddParam;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.rest.componentext.CommentSubjectOnAddListener;
import scatter.comment.rest.subject.mapper.CommentSubjectMapper;
import scatter.wwd.pojo.WwdConstants;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.rest.service.IActivityService;

/**
 * <p>
 * 主体评论添加监听
 * </p>
 *
 * @author yangwei
 * @since 2021-10-27 22:13
 */
@Component
public class ActivityCommentSubjectOnAddListenerImpl implements CommentSubjectOnAddListener {

	@Lazy
	@Autowired
	private IActivityService iActivityService;

	@Autowired
	private CommentSubjectMapper commentSubjectMapper;

	@Override
	public void preCommentSubjectAdd(CommentSubjectAddParam addParam, CommentSubject subject) {

	}

	/**
	 * 添加主体评论后，动态评论数加 1
	 * @param addParam
	 * @param subject
	 */
	@Override
	public void postCommentSubjectAdd(CommentSubjectAddParam addParam, CommentSubject subject) {
		if (!WwdConstants.GROUP_FLAG_ACTIVITY_COMMENT.equals(subject.getGroupFlag())) {
			return;
		}
		Activity byId = iActivityService.getById(addParam.getSubjectId());
		if (byId != null) {
			iActivityService.plusForColumnById(byId.getId(),Activity::getCommentCount,1);
			// 更新楼层，这里可能会有并发问题导致楼层数字会重复，要解决只能加锁，不值当的，因为楼层重复也基本不会影响
			CommentSubject commentSubject = new CommentSubject().setSubjectId(subject.getId()).setFloor(byId.getCommentCount() + 1);
			commentSubjectMapper.updateById(commentSubject);
		}
	}
}
