package scatter.wwd.rest.mapper;

import scatter.wwd.pojo.po.UserInvitation;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户邀请表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserInvitationMapper extends IBaseMapper<UserInvitation> {

}
