package scatter.wwd.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wwd.pojo.form.UserEnjoyAddForm;
import scatter.wwd.pojo.form.UserEnjoyPageQueryForm;
import scatter.wwd.pojo.form.UserEnjoyUpdateForm;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.pojo.vo.UserEnjoyVo;

/**
 * <p>
 * 用户有意思 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserEnjoyMapStruct extends IBaseVoMapStruct<UserEnjoy, UserEnjoyVo>,
        IBaseAddFormMapStruct<UserEnjoy, UserEnjoyAddForm>,
        IBaseUpdateFormMapStruct<UserEnjoy, UserEnjoyUpdateForm>,
        IBaseQueryFormMapStruct<UserEnjoy, UserEnjoyPageQueryForm> {
    UserEnjoyMapStruct INSTANCE = Mappers.getMapper( UserEnjoyMapStruct.class );

}
