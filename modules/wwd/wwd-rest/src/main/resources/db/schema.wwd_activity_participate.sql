DROP TABLE IF EXISTS wwd_activity_participate;
CREATE TABLE `wwd_activity_participate` (
  `id` varchar(32) NOT NULL,
  `activity_id` varchar(32) NOT NULL COMMENT '活动ID',
  `user_id` varchar(32) NOT NULL COMMENT '用户ID',
  `is_leader` tinyint(1) NOT NULL COMMENT '是否是领队',
  `is_quit` tinyint(1) not NULL COMMENT '是否退出',
  `order_id` varchar(20) NOT NULL COMMENT '订单id',
  `order_no` varchar(50) NOT NULL COMMENT '订单号',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注，退款。。。',
  `name` varchar(255) DEFAULT NULL COMMENT '报名人员姓名',
  `mobile` varchar(50) DEFAULT NULL COMMENT '报名人员手机号',
  `id_card_no` varchar(255) DEFAULT NULL COMMENT '证件号码',
  `id_type_dict_id` varchar(20) DEFAULT NULL COMMENT '证件类型',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `is_leader` (`is_leader`) USING BTREE,
  KEY `activity_id` (`activity_id`),
  KEY `wx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='活动参与';
