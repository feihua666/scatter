DROP TABLE IF EXISTS wwd_user_album;
CREATE TABLE `wwd_user_album` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `pic_url` varchar(255) NOT NULL COMMENT '图片url',
  `pic_thumb_url` varchar(255) DEFAULT NULL COMMENT '缩略图url',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `seq` int(11) NOT NULL DEFAULT '11' COMMENT '序号，从小到大排序',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wwd_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户相册表';
