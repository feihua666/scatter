DROP TABLE IF EXISTS wwd_user_enjoy;
CREATE TABLE `wwd_user_enjoy` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `enjoyed_user_id` varchar(32) NOT NULL COMMENT '有意思用户id',
  `message` varchar(1500) DEFAULT NULL COMMENT '添加有意思时，可以写一句话',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wwd_user_id` (`user_id`) USING BTREE,
  KEY `enjoyed_wwd_user_id` (`enjoyed_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户有意思表';
