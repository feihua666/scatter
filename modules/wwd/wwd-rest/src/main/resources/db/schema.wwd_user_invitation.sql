DROP TABLE IF EXISTS wwd_user_invitation;
CREATE TABLE `wwd_user_invitation` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `code` varchar(255) NOT NULL COMMENT '邀请码',
  `is_used` tinyint(1) NOT NULL COMMENT '是否已使用',
  `invited_user_id` varchar(32) DEFAULT NULL COMMENT '被邀请的用户',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wwd_user_id` (`user_id`) USING BTREE,
  KEY `is_used` (`is_used`) USING BTREE,
  KEY `wwd_user_id__is_used` (`user_id`,`is_used`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户邀请表';
