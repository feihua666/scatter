-- 需要迁移的表
-- base_user
-- base_user_auth
-- wwd_activity
-- wwd_activity_order
-- wwd_activity_user_mutual_election
-- wwd_banner
-- wwd_participate
-- wwd_user
-- wwd_user_area
-- wwd_user_card
-- wwd_user_enjoy
-- wwd_user_invitation
-- wwd_user_param_qrcode
-- wwd_user_pic
-- wwd_user_tag
-- wwd_user_visit
-- 汪汪队数据迁移脚本




-- 汪汪队用户id
drop table if exists `wwd_db`.`wwd_user_id`;
CREATE TABLE `wwd_db`.`wwd_user_id`  (
                                         `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `old_id` varchar(50) NULL DEFAULT NULL,
                                         PRIMARY KEY (`new_id`) USING BTREE,
                                         KEY `old_id` (`old_id`) USING BTREE

) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_user_id` (old_id) select id from `wwd_db`.`wwd_user`;
-- 全局用户id
drop table if exists `wwd_db`.`base_user_id`;
CREATE TABLE `wwd_db`.`base_user_id`  (
                                         `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `old_id` varchar(50) NULL DEFAULT NULL,
                                         PRIMARY KEY (`new_id`) USING BTREE,
                                         KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`base_user_id` (old_id) select user_id from `wwd_db`.`wwd_user`;


-- ------------------------------------------活动相关
-- 活动主键
drop table if exists `wwd_db`.`wwd_activity_id`;
CREATE TABLE `wwd_db`.`wwd_activity_id`  (
  `new_id` int(11) NOT NULL AUTO_INCREMENT,
  `old_id` varchar(50) NULL DEFAULT NULL,
  PRIMARY KEY (`new_id`) USING BTREE,
  KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_activity_id` (old_id) select id from `wwd_db`.`wwd_activity`;

-- 活动主体
truncate table scatter_wwd.wwd_activity;

INSERT INTO `scatter_wwd`.`wwd_activity`
     (`id`                                                              , `title`, `title_url`, `province_area_id`, `city_area_id`, `introduced`, `sponsor`, `sponsor_link`, `author`, `start_time`, `end_time`, `address`, `contact`, `place_type_dict_id`                                           , `is_publish`  , `activity_statement`, `seq`                ,                            `version` , `create_at`               , `create_by` , `update_at`  , `update_by`)
select (select new_id from `wwd_db`.`wwd_activity_id` where old_id = id), `title`, `title_url`,  null             ,  null         ,  null       ,  null    ,  null         , `author`, `start_time`, `end_time`, `addr`   , `contact`, if(`type` = '室内','1298222036749410305','1298222089878659074'), 0,               `activity_statement`, ifnull(`sequence`,10),                             1        ,  ifnull(`CREATE_AT`,now()), null        , null         , null
from `wwd_db`.`wwd_activity` where DEL_FLAG = 'N';


-- 活动内容
truncate table scatter_wwd.wwd_activity_content;
INSERT INTO `scatter_wwd`.`wwd_activity_content`
      (`id`                                                             , `activity_id`                                                    , `content`,   `version`, `create_at`               , `create_by`         , `update_at`  , `update_by`)
select (select new_id from `wwd_db`.`wwd_activity_id` where old_id = id),(select new_id from `wwd_db`.`wwd_activity_id` where old_id = id) , `content`,   1        ,  ifnull(`CREATE_AT`,now()), null                , null          , null
from `wwd_db`.`wwd_activity` where DEL_FLAG = 'N';

-- 活动互选id
drop table if exists `wwd_db`.`wwd_activity_user_mutual_election_id`;
CREATE TABLE `wwd_db`.`wwd_activity_user_mutual_election_id`  (
  `new_id` int(11) NOT NULL AUTO_INCREMENT,
  `old_id` varchar(50) NULL DEFAULT NULL,
  PRIMARY KEY (`new_id`) USING BTREE,
  KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_activity_user_mutual_election_id` (old_id) select id from `wwd_db`.`wwd_activity_user_mutual_election`;


-- 活动互选
truncate table scatter_wwd.wwd_activity_user_mutual_election;
INSERT INTO `scatter_wwd`.`wwd_activity_user_mutual_election`
      (`id`                                                                                  , `activity_id`                                                              , `user_id`                                                       ,`selected_user_id`                                                        ,`level`,             `version`, `create_at`               , `create_by`   , `update_at`  , `update_by`)
select (select new_id from `wwd_db`.`wwd_activity_user_mutual_election_id` where old_id = ID), (select new_id from `wwd_db`.`wwd_activity_id` where old_id = ACTIVITY_ID) , (select new_id from wwd_db.wwd_user_id where old_id=WWD_USER_ID),(select new_id from wwd_db.wwd_user_id where old_id=SELECTED_WWD__USER_ID),`LEVEL`,              1       , ifnull(`CREATE_AT`,now()) , null          , null         , null
       from `wwd_db`.`wwd_activity_user_mutual_election` where DEL_FLAG = 'N' and exists (select id from `wwd_db`.`wwd_activity` wa where DEL_FLAG = 'N' and wa.id = ACTIVITY_ID);

-- 活动互选配置
truncate table scatter_wwd.wwd_activity_mutual_election_config;
INSERT INTO `scatter_wwd`.`wwd_activity_mutual_election_config`
      (`id`                                                             , `activity_id`                                                    , `start_at` ,`end_at`,`mutual_election_status_dict_id`                                                                                                                                                                                                      ,`manage_user_ids`,              `version`, `create_at`               , `create_by`  , `update_at`  , `update_by`)
select (select new_id from `wwd_db`.`wwd_activity_id` where old_id = id),(select new_id from `wwd_db`.`wwd_activity_id` where old_id = id) , null       ,null    ,case  when mutual_election_status='no_start' then '1299216138580267009' when mutual_election_status='ongoing' then '1299216205877874690' when mutual_election_status='ended' then '1299216263130124289' else '1299216138580267009' end, null           ,                1       , ifnull(`CREATE_AT`,now()) , null         , null         , null
       from `wwd_db`.`wwd_activity` where DEL_FLAG = 'N';



-- 活动参与人id
drop table if exists `wwd_db`.`wwd_activity_participate_id`;
CREATE TABLE `wwd_db`.`wwd_activity_participate_id`  (
  `new_id` int(11) NOT NULL AUTO_INCREMENT,
  `old_id` varchar(50) NULL DEFAULT NULL,
  PRIMARY KEY (`new_id`) USING BTREE,
  KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_activity_participate_id` (old_id) select id from `wwd_db`.`wwd_participate`;


-- 活动参与人
truncate table scatter_wwd.wwd_activity_participate;
INSERT INTO `scatter_wwd`.`wwd_activity_participate`
      (`id`                                                                      , `activity_id`                                                                 , `user_id`                                                        , `is_leader`     , `is_quit`               , `remarks`, `name`, `mobile`, `id_card_no`, `id_type_dict_id`                            ,    order_id,      order_no,        `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select (select new_id from `wwd_db`.`wwd_activity_participate_id` where old_id = id), (select new_id from `wwd_db`.`wwd_activity_id` where old_id = wwd_activity_id) , (select new_id from wwd_db.wwd_user_id where old_id=wwd_user_id),if(type='Y',1,0),if(status='normal',0,1)  ,  remarks , `name`, `mobile`, `id_card_no`, if(id_card_no is null,null,'1299233444224045058'),      '',       '',               1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_participate` where DEL_FLAG = 'N';

-- 活动报名规则
truncate table scatter_wwd.wwd_activity_enroll_rule;
INSERT INTO `scatter_wwd`.`wwd_activity_enroll_rule`
      (`id`                                                             , `activity_id`                                                    , `start_at`, `end_at`, `contact`, `is_require_id_card`        , `headcount`, `headcount_male`, `headcount_female`, `headcount_desc`, `is_headcount_gender_self`     , `male_price`      , `female_price`      , `is_need_pay_online`          ,count_male, count_female,         `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select (select new_id from `wwd_db`.`wwd_activity_id` where old_id = id),(select new_id from `wwd_db`.`wwd_activity_id` where old_id = id) , null      ,null     ,  contact ,  if(require_id_card='Y',1,0) ,`headcount`, `headcount_male`, `headcount_female`, `headcount_desc`, if(headcount_rule='custom',1,0), `male_price` * 100, `female_price` * 100, if(pay_type='online_pay',1,0) ,           0,0                    ,1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_activity` where DEL_FLAG = 'N';

-- ------------------------------------------用户相关




-- 用户登录标识
truncate table scatter_wwd.component_identifier;
INSERT INTO scatter_wwd.`component_identifier`(`id`, `user_id`, `identifier`, `identity_type_dict_id`, `is_lock`, `lock_reason`, `union_id`, `is_expired`, `is_disabled`, `disabled_reason`, `version`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('1', '1', 'superadmin', '1301070741924077569', 0, NULL, NULL, 0, 0, NULL, 1, '2020-12-07 17:17:45', NULL, NULL, NULL);

INSERT INTO `scatter_wwd`.`component_identifier`
       (`id`                                                                 , `user_id`                                                             , `identifier`, `identity_type_dict_id`, `is_lock`, `lock_reason`, `union_id`, `is_expired`, `is_disabled`, `disabled_reason`, `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select  (select new_id from `wwd_db`.`base_user_id` where old_id = t.user_id),(select new_id from `wwd_db`.`base_user_id` where old_id = t.user_id)  ,  identifier , '1352183332630310913'  , 0        , null         ,null       , 0           , 0            ,  null            ,  1       , ifnull(t.`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`base_user_auth`  t,`wwd_db`.`wwd_user` tt where tt.user_id=t.user_id and t.DEL_FLAG = 'N' and IDENTITY_TYPE='WX_PLATFORM';

-- 用户主体信息
truncate table scatter_wwd.component_user_simple;
INSERT INTO scatter_wwd.`component_user_simple`(`id`, `nickname`, `gender_dict_id`, `avatar`, `is_lock`, `lock_reason`,`source_from_dict_id`, `version`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('1', '简单用户超级管理员', NULL, NULL, 0, NULL,'1442731678232498178', 1, '2020-12-08 18:14:57', NULL, NULL, NULL);

INSERT INTO `scatter_wwd`.`component_user_simple`
       (`id`       , `nickname`, `gender_dict_id`                                                                  , `avatar`, `is_lock`                  , `lock_reason`, `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select  ttt.new_id , t.nickname ,  case when t.gender='male' then '3' when t.gender='female' then '4' else null end , t.photo , if(t.locked='Y',1,0)        , null         ,  1       , ifnull(t.`CREATE_AT`,now()), null, null, null
                from `wwd_db`.`base_user`  t,`wwd_db`.`wwd_user` tt ,`wwd_db`.`base_user_id` ttt where ttt.old_id = t.id and tt.user_id=t.id and t.DEL_FLAG = 'N';

-- 更新用户来源和分组标识
update `scatter_wwd`.`component_user_simple`
set source_from_dict_id = '1441946957063462914',group_flag = 'wx_mp'
where id !='1';
update `scatter_wwd`.`component_identifier`
   set group_flag = 'wx_mp'
where id !='1';

-- 删除已经被删除的用户图片表
delete up from wwd_db.wwd_user_pic up where DEL_FLAG != 'N';
-- 用户信息表
truncate table scatter_wwd.wwd_user_info;
INSERT INTO `scatter_wwd`.`wwd_user_info`
      (`id`         , `user_id`  ,`pic_url`                                                                                                       , gender_dict_id                                                              , name  ,  nickname ,wechat_number  , height  , weight  ,  description ,company,   profession,position, college  , education_dict_id                                                                     , academic_dict_id                                                                      ,major   ,  card_no ,looks_dict_id                                                                                                                             , mobile  , id_card_no  ,id_type_dict_id                                   ,blood_type_dict_id                                                                     ,shape_dict_id                                                                      ,standard   ,smoking_dict_id                                                                      ,drinking_dict_id                                                                      ,constellation_dict_id                                                                      , month_salary  , year_salary  ,marital_status_dict_id                                                                      ,is_has_car              , car_city  , is_has_hourse            ,hourse_city   ,birth_day    ,is_verified             ,is_show_in_list              ,now_area_id,home_area_id ,           version,create_at,create_by,update_at,update_by)
select ttt.new_id   ,ttt.new_id  ,(select ifnull(up.pic_origin_url,'') from wwd_db.wwd_user_pic up where wwd_user_id=t.id and type='main' limit 1), case when gender='male' then '3' when gender='female' then '4' else null end, `name`, `nickname`,`wechat_number`, `height`, `weight`, `description`,null   , `profession`,null    , `college`, (select d.id from scatter_wwd.component_dict d where is_group=0 and value=`education`),  (select d.id from scatter_wwd.component_dict d where is_group=0 and value=`academic`), `major`, `card_no`, (case looks when 'normal' then '1298955512675160065' when 'low' then '1298955593981743105' when 'perfect' then '1298955660704731138' end), `mobile`, `id_card_no`, if(id_card_no is null,null,'1299233444224045058'), (select d.id from scatter_wwd.component_dict d where is_group=0 and VALUE=`blood_type`), (select d.id from scatter_wwd.component_dict d where is_group=0 and value=`shape`), `standard`, (select d.id from scatter_wwd.component_dict d where is_group=0 and value=`smoking`), (select d.id from scatter_wwd.component_dict d where is_group=0 and value=`drinking`), (select d.id from scatter_wwd.component_dict d where is_group=0 and value=`constellation`), `month_salary`, `year_salary`, (select d.id from scatter_wwd.component_dict d where is_group=0 and value= concat('married_status_',`marital_status`)), if(`has_car`='yes',1,0), `car_city`, if(`has_hourse`='no',0,1), `hourse_city`, `birth_day`, if(`isverified`='Y',1,0), if(`show_in_list` = 'Y',1,0), null      ,null        ,          1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_user` t,`wwd_db`.`base_user_id` ttt where ttt.old_id = t.user_id and DEL_FLAG = 'N';

-- 更新user_id为wwd_user表的id


 -- 更新区域
UPDATE scatter_wwd.wwd_user_info ui
    join `wwd_db`.`wwd_user_id` wui on wui.new_id = ui.user_id
JOIN wwd_db.wwd_user_area  ua on ua.wwd_user_id = wui.old_id
join scatter_wwd.component_area ca on ca.name=ua.now_district_name and ca.level=4
join scatter_wwd.component_area cap on cap.name=case when ua.now_city_name='北京市' then '市辖区' when ua.now_city_name='上海市' then '市辖区' when ua.now_city_name='重庆市' then '市辖区' when ua.now_city_name='天津市' then '市辖区' else ua.now_city_name end and cap.level=3 and cap.id=ca.parent_id
set ui.now_area_id= ca.id;

UPDATE scatter_wwd.wwd_user_info ui
    join `wwd_db`.`wwd_user_id` wui on wui.new_id = ui.user_id
JOIN wwd_db.wwd_user_area  ua on ua.wwd_user_id = wui.old_id
join scatter_wwd.component_area ca on ca.name=ua.home_district_name and ca.level=4
join scatter_wwd.component_area cap on cap.name=case when ua.home_city_name='北京市' then '市辖区' when ua.home_city_name='上海市' then '市辖区' when ua.home_city_name='重庆市' then '市辖区' when ua.home_city_name='天津市' then '市辖区' else ua.home_city_name end and cap.level=3 and cap.id=ca.parent_id
set ui.home_area_id= ca.id;




-- 相册id
drop table if exists `wwd_db`.`wwd_user_album_id`;
CREATE TABLE `wwd_db`.`wwd_user_album_id`  (
                                         `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `old_id` varchar(50) NULL DEFAULT NULL,
                                         PRIMARY KEY (`new_id`) USING BTREE,
                                         KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_user_album_id` (old_id) select id from `wwd_db`.`wwd_user_pic`;


-- 相册
truncate table scatter_wwd.wwd_user_album;
INSERT INTO `scatter_wwd`.`wwd_user_album`
      (`id`                                                               , `user_id`                                                        , `pic_url`      , `pic_thumb_url`, `description`, `seq`     ,          `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select (select new_id from `wwd_db`.`wwd_user_album_id` where old_id = id), (select new_id from wwd_db.wwd_user_id where old_id=wwd_user_id) , pic_origin_url , pic_thumb_url  ,  describtion ,  sequence ,           1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_user_pic` where type != 'main';

-- 标签id
drop table if exists `wwd_db`.`wwd_user_tag_id`;
CREATE TABLE `wwd_db`.`wwd_user_tag_id`  (
                                               `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                               `old_id` varchar(50) NULL DEFAULT NULL,
                                               PRIMARY KEY (`new_id`) USING BTREE,
                                               KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_user_tag_id` (old_id) select id from `wwd_db`.`wwd_user_tag`;


-- 标签
truncate table scatter_wwd.wwd_user_tag;
INSERT INTO `scatter_wwd`.`wwd_user_tag`
       (`id`                                                               , `user_id`                                                         , `type_dict_id`                                                                                     , `content`, `self_content`, `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select  (select new_id from `wwd_db`.`wwd_user_tag_id` where old_id = id), (select new_id from wwd_db.wwd_user_id where old_id=wwd_user_id)  , (select d.id from scatter_wwd.component_dict d where is_group=1 and code=concat(`type`,'_parent')) , content  ,  self_content ,  1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_user_tag` where DEL_FLAG = 'N';
-- 标签内容字典处理，全部统一为字典id

UPDATE `scatter_wwd`.`wwd_user_tag` tt
join
(SELECT t.id, d.parent_id, GROUP_CONCAT(d.id) as content from  scatter_wwd.component_dict d
JOIN `scatter_wwd`.`wwd_user_tag` t on d.parent_id = t.type_dict_id
WHERE FIND_IN_SET(d.value, replace(t.content,'food','food_type'))
or  FIND_IN_SET(d.value, replace(t.content,'movie','movie_type'))
or  FIND_IN_SET(d.value, replace(t.content,'nature','nature_type'))
or  FIND_IN_SET(d.value, replace(t.content,'hobby','hobby_type'))
or  FIND_IN_SET(d.value, replace(t.content,'trip','trip_type'))
or  FIND_IN_SET(d.value, replace(t.content,'sport','sport_type'))
or  FIND_IN_SET(d.name, t.content)
GROUP BY d.parent_id,t.id) t on t.id=tt.id

set tt.content=t.content;

-- 卡片 -- 仅测试展示用，新开发数据已不正确因为卡片二维码内容地址已变，不再迁移，测试好像也不需要
-- truncate table scatter_wwd.wwd_user_card;
-- INSERT INTO `scatter_wwd`.`wwd_user_card`
--       (`id`         , `user_id`              , `pic_url`       , `pic_thumb_url`, `description`, `version`, `create_at`, `create_by`, `update_at`, `update_by`)
-- select left(`ID`,20), left(`wwd_user_id`,20) , pic_origin_url  , `pic_thumb_url` ,  describtion,  1       , ifnull(`CREATE_AT`,now()), null, null, null
--        from `wwd_db`.`wwd_user_card` where DEL_FLAG = 'N';


-- 有意思 id
drop table if exists `wwd_db`.`wwd_user_enjoy_id`;
CREATE TABLE `wwd_db`.`wwd_user_enjoy_id`  (
                                             `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                             `old_id` varchar(50) NULL DEFAULT NULL,
                                             PRIMARY KEY (`new_id`) USING BTREE,
                                             KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_user_enjoy_id` (old_id) select id from `wwd_db`.`wwd_user_enjoy`;


-- 有意思
truncate table scatter_wwd.wwd_user_enjoy;
INSERT INTO `scatter_wwd`.`wwd_user_enjoy`
      (`id`                                                               , `user_id`                                                       , `enjoyed_user_id`                                                                       , `message`,  `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select (select new_id from `wwd_db`.`wwd_user_enjoy_id` where old_id = id), (select new_id from wwd_db.wwd_user_id where old_id=wwd_user_id), ifnull((select new_id from wwd_db.wwd_user_id where old_id=enjoyed_wwd_user_id),'xxx')  , `message` ,  1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_user_enjoy` where DEL_FLAG = 'N';


-- 二维码数据信息 id
-- drop table if exists `wwd_db`.`wwd_user_param_qrcode_id`;
-- CREATE TABLE `wwd_db`.`wwd_user_param_qrcode_id`  (
--                                                `new_id` int(11) NOT NULL AUTO_INCREMENT,
--                                                `old_id` varchar(50) NULL DEFAULT NULL,
--                                                PRIMARY KEY (`new_id`) USING BTREE,
--                                                KEY `old_id` (`old_id`) USING BTREE
-- ) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;
--
-- insert into `wwd_db`.`wwd_user_param_qrcode_id` (old_id) select id from `wwd_db`.`wwd_user_param_qrcode`;

-- 二维码数据信息 二维码数据不再迁移，数据内容包含用户信息，已不正确
-- truncate table scatter_wwd.wwd_user_param_qrcode;
-- INSERT INTO `scatter_wwd`.`wwd_user_param_qrcode`
--       (`id`                                                                      , `user_id`                                                                 ,`ticket` , `expire_seconds`, `content`, `url`, `is_permanent`         ,  `version`, `create_at`, `create_by`, `update_at`, `update_by`)
-- select (select new_id from `wwd_db`.`wwd_user_param_qrcode_id` where old_id = id), ifnull(( SELECT new_id FROM wwd_db.wwd_user_id WHERE old_id = wwd_user_id ),(select concat('xxx',new_id) from `wwd_db`.`wwd_user_param_qrcode_id` where old_id = id)) , `ticket`, `expire_seconds` ,`content`, `url`, if(`is_limit`= 'Y',0,1),   1       , ifnull(`CREATE_AT`,now()), null, null, null
--        from `wwd_db`.`wwd_user_param_qrcode` where DEL_FLAG = 'N';


-- 邀请码 id
drop table if exists `wwd_db`.`wwd_user_invitation_id`;
CREATE TABLE `wwd_db`.`wwd_user_invitation_id`  (
                                                      `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                                      `old_id` varchar(50) NULL DEFAULT NULL,
                                                      PRIMARY KEY (`new_id`) USING BTREE,
                                                      KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_user_invitation_id` (old_id) select id from `wwd_db`.`wwd_user_invitation`;


-- 邀请码
truncate table scatter_wwd.wwd_user_invitation;
INSERT INTO `scatter_wwd`.`wwd_user_invitation`
      (`id`                                                                    , `user_id`                                                                     ,`code`, `is_used`            , `invited_user_id`                                                       ,  `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select (select new_id from `wwd_db`.`wwd_user_invitation_id` where old_id = id), ifnull((select new_id from wwd_db.wwd_user_id where old_id=wwd_user_id),'xxx'),`code`, if(`is_used`='Y',1,0),(select new_id from wwd_db.wwd_user_id where old_id=invited_wwd_user_id) ,   1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_user_invitation` where DEL_FLAG = 'N';


-- 用户访问 id
drop table if exists `wwd_db`.`wwd_user_visit_id`;
CREATE TABLE `wwd_db`.`wwd_user_visit_id`  (
                                                    `new_id` int(11) NOT NULL AUTO_INCREMENT,
                                                    `old_id` varchar(50) NULL DEFAULT NULL,
                                                    PRIMARY KEY (`new_id`) USING BTREE,
                                                    KEY `old_id` (`old_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

insert into `wwd_db`.`wwd_user_visit_id` (old_id) select id from `wwd_db`.`wwd_user_visit`;


-- 用户访问
truncate table scatter_wwd.wwd_user_visit;
INSERT INTO `scatter_wwd`.`wwd_user_visit`
      (`id`      , `user_id`                                                                      , `visit_user_id`                                                                    , `visit_type_dict_id`                                                                                                                                                                  , `is_read`              , `read_at`                           ,  `version`, `create_at`, `create_by`, `update_at`, `update_by`)
select tt.new_id , ifnull((select new_id from wwd_db.wwd_user_id where old_id=wwd_user_id),'xxx') ,ifnull((select new_id from wwd_db.wwd_user_id where old_id=visit_wwd_user_id),'xxx'),case visit_type when '微信群' then '1438330633888260097' when '微信朋友' then '1438330704805552130' when '朋友圈' then '1438330546936143874' else '1438330411225243650' end               , if(`read_flag`='Y',1,0),if(`read_flag`='Y',MODIFIED_AT,null) ,   1       , ifnull(`CREATE_AT`,now()), null, null, null
       from `wwd_db`.`wwd_user_visit` t ,`wwd_db`.`wwd_user_visit_id` tt where t.id = tt.old_id;


-- id转换 统一所有用户id都使用wwd_user表的id
-- 活动参与
update scatter_wwd.wwd_activity_participate t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
set t.user_id=bui.new_id;

-- 相册
update scatter_wwd.wwd_user_album t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.user_id=bui.new_id;

-- 有意思
update scatter_wwd.wwd_user_enjoy t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.user_id=bui.new_id;
update scatter_wwd.wwd_user_enjoy t
    join wwd_db.wwd_user_id wui on wui.new_id = t.enjoyed_user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
set t.enjoyed_user_id=bui.new_id;

-- 邀请码
update scatter_wwd.wwd_user_invitation t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.user_id=bui.new_id;
update scatter_wwd.wwd_user_invitation t
    join wwd_db.wwd_user_id wui on wui.new_id = t.invited_user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.invited_user_id=bui.new_id;

-- 二维码参数,不再迁移
-- update scatter_wwd.wwd_user_param_qrcode t
--     join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
--     join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
--     join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
--     set t.user_id=bui.new_id;

-- 标签 需要将唯一索引去了，然后再设置
alter table scatter_wwd.wwd_user_tag drop index wwd_user_id ;
update scatter_wwd.wwd_user_tag t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.user_id=bui.new_id;
alter table scatter_wwd.wwd_user_tag add index wwd_user_id(user_id,type_dict_id);

-- 互选
update scatter_wwd.wwd_activity_user_mutual_election t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.user_id=bui.new_id;
update scatter_wwd.wwd_activity_user_mutual_election t
    join wwd_db.wwd_user_id wui on wui.new_id = t.selected_user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.selected_user_id=bui.new_id;

-- 访问关系
update scatter_wwd.wwd_user_visit t
    join wwd_db.wwd_user_id wui on wui.new_id = t.user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.user_id=bui.new_id;
update scatter_wwd.wwd_user_visit t
    join wwd_db.wwd_user_id wui on wui.new_id = t.visit_user_id
    join `wwd_db`.`wwd_user` wu on wu.id = wui.old_id
    join `wwd_db`.`base_user_id` bui on bui.old_id = wu.user_id
    set t.visit_user_id=bui.new_id;

-- banner 迁移,不需要迁移了，直接添加吧
-- truncate table scatter_wwd.component_banner;
-- INSERT INTO `scatter_wwd`.`component_banner`
--            (`id`    , `name`, `img_url`  , `img_description`, `action_link`, `is_disabled`, `disabled_reason`, `start_at`, `end_at`, `group_flag`  , `seq`, `remark`, `version`, `create_at`, `create_by`, `update_at`, `update_by`)
-- select left(`ID`,20), title , `title_url`, `descp`          ,`redirect_url`, 0            , null             ,   null    , null    ,   'wwd_dynamic',   10,  null   , 1         , ifnull(`CREATE_AT`,now()), null, null, null
--        from `wwd_db`.`wwd_banner` where DEL_FLAG = 'N';

-- 活动参与人数修正
UPDATE wwd_activity_enroll_rule t
SET t.count_male =(
	SELECT
		count( tt.id )
FROM
	wwd_activity_participate tt
JOIN wwd_user_info u on tt.user_id=u.user_id
where u.gender_dict_id='3' and tt.is_quit=0 and  t.activity_id=tt.activity_id),
t.count_female =(
	SELECT
		count( tt.id )
FROM
	wwd_activity_participate tt
JOIN wwd_user_info u on tt.user_id=u.user_id
where u.gender_dict_id='4' and tt.is_quit=0 and  t.activity_id=tt.activity_id);
/****************************************/

-- 正则字替换
DROP  FUNCTION IF EXISTS `regex_replace`;
CREATE  FUNCTION `regex_replace`(pattern VARCHAR(1000),replacement VARCHAR(1000),original VARCHAR(1000)) RETURNS varchar(1000) CHARSET utf8mb4
    DETERMINISTIC
BEGIN

DECLARE temp VARCHAR(1000);

DECLARE ch VARCHAR(1);

DECLARE i INT;

SET i = 1;

SET temp = '';
-- 正则替换
IF original REGEXP pattern THEN
		loop_label: LOOP
				IF i>CHAR_LENGTH(original) THEN
						LEAVE loop_label;
				END IF;

				SET ch = SUBSTRING(original,i,1);

				IF NOT ch REGEXP pattern THEN
					SET temp = CONCAT(temp,ch);
				ELSE
					SET temp = CONCAT(temp,replacement);
				END IF;

				SET i=i+1;

		END LOOP;
ELSE
SET temp = original;
END IF;
--
RETURN temp;
--
END;

-- 更新身高体重
UPDATE wwd_user_info t set t.weight=left(regex_replace('[^0-9]','',t.weight),3),t.height=left(regex_replace('[^0-9]','',t.height),3);

DROP  FUNCTION IF EXISTS `regex_replace`;

-- 恭喜数据迁移完成