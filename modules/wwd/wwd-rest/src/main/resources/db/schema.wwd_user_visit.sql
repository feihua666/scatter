DROP TABLE IF EXISTS wwd_user_visit;
CREATE TABLE `wwd_user_visit` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) NOT NULL COMMENT '访问的用户ID',
  `visit_user_id` varchar(32) NOT NULL COMMENT '被访问的用户ID',
  `visit_type_dict_id` varchar(20) NOT NULL DEFAULT '' COMMENT '访问类型,例个人详情是从列表进，或分享（群，朋友圈等）进来，list=列表,timeline=朋友圈,groupmessage=微信群,singlemessage=好友分享',
  `is_read` tinyint(1) NOT NULL COMMENT '是否已读',
  `read_at` datetime DEFAULT NULL COMMENT '读取，查看时间',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='汪汪队用户访问';
