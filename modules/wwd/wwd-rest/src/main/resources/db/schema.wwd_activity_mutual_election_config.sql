DROP TABLE IF EXISTS wwd_activity_mutual_election_config;
CREATE TABLE `wwd_activity_mutual_election_config` (
  `id` varchar(32) NOT NULL,
  `activity_id` varchar(32) NOT NULL COMMENT '活动id',
  `start_at` datetime DEFAULT NULL COMMENT '互选开始时间',
  `end_at` datetime DEFAULT NULL COMMENT '互选结束时间',
  `mutual_election_status_dict_id` varchar(20) NOT NULL COMMENT '互选状态，未开始，进行中，已结束',
  `manage_user_ids` varchar(2000) DEFAULT NULL COMMENT '管理的用户id，多个以逗号分隔，人员可以进行h5活动管理，目前是为控制互选而生',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `activity_id` (`activity_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='活动互选配置表';
