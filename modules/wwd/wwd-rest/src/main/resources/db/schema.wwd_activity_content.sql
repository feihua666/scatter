DROP TABLE IF EXISTS wwd_activity_content;
CREATE TABLE `wwd_activity_content` (
  `id` varchar(32) NOT NULL,
  `activity_id` varchar(32) NOT NULL COMMENT '活动id',
  `content` mediumtext COMMENT '活动内容',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `activity_id` (`activity_id`) USING BTREE,
  KEY `version` (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动内容表';
