DROP TABLE IF EXISTS wwd_activity_user_mutual_election;
CREATE TABLE `wwd_activity_user_mutual_election` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `activity_id` varchar(32) NOT NULL COMMENT '活动id',
  `user_id` varchar(32) NOT NULL COMMENT '微信模块的用户表用户ID，',
  `selected_user_id` varchar(32) NOT NULL COMMENT '微信模块的用户表，选择的用户ID',
  `level` char(1) DEFAULT 'A' COMMENT '互选级别：A-B-C...',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wx_user_id` (`user_id`) USING BTREE,
  KEY `selected_wx_user_id` (`selected_user_id`) USING BTREE,
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='汪汪队用户互选';
