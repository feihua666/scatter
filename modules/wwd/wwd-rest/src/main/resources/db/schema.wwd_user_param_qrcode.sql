DROP TABLE IF EXISTS wwd_user_param_qrcode;
CREATE TABLE `wwd_user_param_qrcode` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `user_id` varchar(32) NOT NULL COMMENT '微信用户id',
  `ticket` varchar(1000) NOT NULL COMMENT '二维码tiket',
  `expire_seconds` int(11) DEFAULT NULL COMMENT '过期时长，秒',
  `content` varchar(500) NOT NULL COMMENT '二维码图片内容',
  `url` varchar(500) DEFAULT NULL COMMENT '二维码图片地址',
  `is_permanent` tinyint(1) NOT NULL COMMENT '是否永久',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `wwd_user_id_is_limit_unique` (`user_id`,`is_permanent`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户标签表';
