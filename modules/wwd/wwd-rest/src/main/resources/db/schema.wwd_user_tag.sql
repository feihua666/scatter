DROP TABLE IF EXISTS wwd_user_tag;
CREATE TABLE `wwd_user_tag` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `type_dict_id` varchar(20) NOT NULL COMMENT '标签类型，字典，兴趣，性格，食物，旅游等',
  `content` varchar(1500) NOT NULL COMMENT '对应标签的内容字典编码，多个以逗号分隔',
  `self_content` varchar(500) DEFAULT NULL COMMENT '对应标签的自定义内容，文本，直接展示',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `wwd_user_id` (`user_id`,`type_dict_id`) USING BTREE,
  KEY `wwd_user_id_2` (`user_id`) USING BTREE,
  KEY `type` (`type_dict_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户标签表';
