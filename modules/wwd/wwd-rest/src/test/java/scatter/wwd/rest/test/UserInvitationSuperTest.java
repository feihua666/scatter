package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserInvitation;
import scatter.wwd.pojo.form.UserInvitationAddForm;
import scatter.wwd.pojo.form.UserInvitationUpdateForm;
import scatter.wwd.pojo.form.UserInvitationPageQueryForm;
import scatter.wwd.rest.service.IUserInvitationService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户邀请 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserInvitationSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserInvitationService userInvitationService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userInvitationService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userInvitationService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserInvitation mockPo() {
        return JMockData.mock(UserInvitation.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserInvitationAddForm mockAddForm() {
        return JMockData.mock(UserInvitationAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserInvitationUpdateForm mockUpdateForm() {
        return JMockData.mock(UserInvitationUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserInvitationPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserInvitationPageQueryForm.class, mockConfig);
    }
}