package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.ActivityMutualElectionConfig;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigAddForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigUpdateForm;
import scatter.wwd.pojo.form.ActivityMutualElectionConfigPageQueryForm;
import scatter.wwd.rest.test.ActivityMutualElectionConfigSuperTest;
import scatter.wwd.rest.service.IActivityMutualElectionConfigService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 活动互选配置 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class ActivityMutualElectionConfigServiceTest extends ActivityMutualElectionConfigSuperTest{

    @Autowired
    private IActivityMutualElectionConfigService activityMutualElectionConfigService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ActivityMutualElectionConfig> pos = activityMutualElectionConfigService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}