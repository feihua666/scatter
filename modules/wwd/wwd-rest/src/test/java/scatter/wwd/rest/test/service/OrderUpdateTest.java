package scatter.wwd.rest.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.param.UpdateOrderStatusParam;
import scatter.order.pojo.po.Order;
import scatter.order.rest.service.IOrderService;
import scatter.order.rest.service.IOrderStatusFrameworkService;
import scatter.order.rest.service.impl.OrderFrameworkHelperService;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-29 14:04
 */
@SpringBootTest
public class OrderUpdateTest {

	@Autowired
	private IOrderService iOrderService;
	@Autowired
	private OrderFrameworkHelperService orderFrameworkHelperService;

	@Autowired
	private DictService dictService;

	@Test
	public void orderStatusUpdateTest() {

		String outTradeNo = "1431856705898352642";
		Order byOrderNo = iOrderService.getByOrderNo(outTradeNo);

		String categoryDictValue = dictService.getValueById(byOrderNo.getCategoryDictId());


		IOrderStatusFrameworkService iOrderStatusFrameworkService = orderFrameworkHelperService.getIOrderStatusFrameworkService(byOrderNo.getChannelCode(), categoryDictValue);


		UpdateOrderStatusParam updateOrderStatusParam = new UpdateOrderStatusParam();
		updateOrderStatusParam.setOrderNo(outTradeNo);
		updateOrderStatusParam.setStatus(OrderConstants.OrderStatusItem.paid.groupCode());
		updateOrderStatusParam.setChannel(byOrderNo.getChannelCode());
		updateOrderStatusParam.setCategoryDictValue(categoryDictValue);


		boolean b = iOrderStatusFrameworkService.updateOrderStatus(updateOrderStatusParam);

		System.out.println(b);
	}
}
