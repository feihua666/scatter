package scatter.wwd.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.area.rest.AreaConfiguration;
import scatter.banner.rest.BannerConfiguration;
import scatter.captcha.rest.CaptchaConfiguration;
import scatter.comment.rest.CommentConfiguration;
import scatter.common.rest.config.CommonCacheConfig;
import scatter.common.rest.config.CommonGlobalMethodSecurityConfig;
import scatter.common.rest.config.CommonRestConfig;
import scatter.common.rest.config.CommonWebSecurityConfig;
import scatter.dict.rest.DictConfiguration;
import scatter.errorlog.rest.ErrorLogConfiguration;
import scatter.file.rest.FileConfiguration;
import scatter.func.rest.FuncConfiguration;
import scatter.identifier.rest.IdentifierConfiguration;
import scatter.moment.rest.MomentConfiguration;
import scatter.order.rest.OrderConfiguration;
import scatter.pay.rest.PayConfiguration;
import scatter.recommend.rest.RecommendConfiguration;
import scatter.role.rest.RoleConfiguration;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.tools.rest.ToolsConfiguration;
import scatter.usersimple.rest.UsersimpleConfiguration;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wxcp.rest.WxCpConfiguration;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonCacheConfig.class,CommonRestConfig.class, CommonGlobalMethodSecurityConfig.class, CommonWebSecurityConfig.class,
        WwdConfiguration.class,
        DictConfiguration.class,
        AreaConfiguration.class,
        FuncConfiguration.class,
        UsersimpleConfiguration.class,
        IdentifierConfiguration.class,
        RoleConfiguration.class,
        FileConfiguration.class,
        WxMpConfiguration.class,
        CaptchaConfiguration.class,
        BannerConfiguration.class,
        ErrorLogConfiguration.class,
        ScheduleQuartzConfiguration.class,
        ToolsConfiguration.class,
        MomentConfiguration.class,
        CommentConfiguration.class,
        OrderConfiguration.class,
        PayConfiguration.class,
        WxCpConfiguration.class,
        RecommendConfiguration.class,
})
@MapperScan("scatter.wwd.rest.mapper")
public class WwdTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(WwdTestApplication.class, args);
    }
}
