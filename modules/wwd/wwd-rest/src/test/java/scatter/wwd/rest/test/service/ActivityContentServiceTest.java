package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.pojo.form.ActivityContentAddForm;
import scatter.wwd.pojo.form.ActivityContentUpdateForm;
import scatter.wwd.pojo.form.ActivityContentPageQueryForm;
import scatter.wwd.rest.test.ActivityContentSuperTest;
import scatter.wwd.rest.service.IActivityContentService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 活动内容 服务测试类
* </p>
*
* @author yw
* @since 2020-12-09
*/
@SpringBootTest
public class ActivityContentServiceTest extends ActivityContentSuperTest{

    @Autowired
    private IActivityContentService activityContentService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ActivityContent> pos = activityContentService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}