package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.pojo.form.UserEnjoyAddForm;
import scatter.wwd.pojo.form.UserEnjoyUpdateForm;
import scatter.wwd.pojo.form.UserEnjoyPageQueryForm;
import scatter.wwd.rest.service.IUserEnjoyService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户有意思 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserEnjoySuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserEnjoyService userEnjoyService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userEnjoyService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userEnjoyService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserEnjoy mockPo() {
        return JMockData.mock(UserEnjoy.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserEnjoyAddForm mockAddForm() {
        return JMockData.mock(UserEnjoyAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserEnjoyUpdateForm mockUpdateForm() {
        return JMockData.mock(UserEnjoyUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserEnjoyPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserEnjoyPageQueryForm.class, mockConfig);
    }
}