package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.pojo.form.UserEnjoyAddForm;
import scatter.wwd.pojo.form.UserEnjoyUpdateForm;
import scatter.wwd.pojo.form.UserEnjoyPageQueryForm;
import scatter.wwd.rest.test.UserEnjoySuperTest;
import scatter.wwd.rest.service.IUserEnjoyService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户有意思 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserEnjoyServiceTest extends UserEnjoySuperTest{

    @Autowired
    private IUserEnjoyService userEnjoyService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserEnjoy> pos = userEnjoyService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}