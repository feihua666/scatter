package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.pojo.form.UserVisitAddForm;
import scatter.wwd.pojo.form.UserVisitUpdateForm;
import scatter.wwd.pojo.form.UserVisitPageQueryForm;
import scatter.wwd.rest.test.UserVisitSuperTest;
import scatter.wwd.rest.service.IUserVisitService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 汪汪队用户访问 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserVisitServiceTest extends UserVisitSuperTest{

    @Autowired
    private IUserVisitService userVisitService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserVisit> pos = userVisitService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}