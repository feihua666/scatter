package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.pojo.form.ActivityParticipateAddForm;
import scatter.wwd.pojo.form.ActivityParticipateUpdateForm;
import scatter.wwd.pojo.form.ActivityParticipatePageQueryForm;
import scatter.wwd.rest.service.IActivityParticipateService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 活动参与 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class ActivityParticipateSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IActivityParticipateService activityParticipateService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return activityParticipateService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return activityParticipateService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ActivityParticipate mockPo() {
        return JMockData.mock(ActivityParticipate.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ActivityParticipateAddForm mockAddForm() {
        return JMockData.mock(ActivityParticipateAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ActivityParticipateUpdateForm mockUpdateForm() {
        return JMockData.mock(ActivityParticipateUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ActivityParticipatePageQueryForm mockPageQueryForm() {
        return JMockData.mock(ActivityParticipatePageQueryForm.class, mockConfig);
    }
}