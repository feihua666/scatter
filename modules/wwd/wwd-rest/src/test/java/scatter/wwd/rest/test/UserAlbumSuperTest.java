package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.pojo.form.UserAlbumAddForm;
import scatter.wwd.pojo.form.UserAlbumUpdateForm;
import scatter.wwd.pojo.form.UserAlbumPageQueryForm;
import scatter.wwd.rest.service.IUserAlbumService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户相册 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserAlbumSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserAlbumService userAlbumService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userAlbumService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userAlbumService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserAlbum mockPo() {
        return JMockData.mock(UserAlbum.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserAlbumAddForm mockAddForm() {
        return JMockData.mock(UserAlbumAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserAlbumUpdateForm mockUpdateForm() {
        return JMockData.mock(UserAlbumUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserAlbumPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserAlbumPageQueryForm.class, mockConfig);
    }
}