package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserCard;
import scatter.wwd.pojo.form.UserCardAddForm;
import scatter.wwd.pojo.form.UserCardUpdateForm;
import scatter.wwd.pojo.form.UserCardPageQueryForm;
import scatter.wwd.rest.service.IUserCardService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户卡片 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserCardSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserCardService userCardService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userCardService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userCardService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserCard mockPo() {
        return JMockData.mock(UserCard.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserCardAddForm mockAddForm() {
        return JMockData.mock(UserCardAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserCardUpdateForm mockUpdateForm() {
        return JMockData.mock(UserCardUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserCardPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserCardPageQueryForm.class, mockConfig);
    }
}