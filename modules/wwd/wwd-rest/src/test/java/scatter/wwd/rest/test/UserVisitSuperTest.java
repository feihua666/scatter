package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.pojo.form.UserVisitAddForm;
import scatter.wwd.pojo.form.UserVisitUpdateForm;
import scatter.wwd.pojo.form.UserVisitPageQueryForm;
import scatter.wwd.rest.service.IUserVisitService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 汪汪队用户访问 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserVisitSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserVisitService userVisitService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userVisitService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userVisitService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserVisit mockPo() {
        return JMockData.mock(UserVisit.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserVisitAddForm mockAddForm() {
        return JMockData.mock(UserVisitAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserVisitUpdateForm mockUpdateForm() {
        return JMockData.mock(UserVisitUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserVisitPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserVisitPageQueryForm.class, mockConfig);
    }
}