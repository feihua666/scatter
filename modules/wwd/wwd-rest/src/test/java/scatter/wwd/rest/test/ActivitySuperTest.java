package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.pojo.form.ActivityAddForm;
import scatter.wwd.pojo.form.ActivityUpdateForm;
import scatter.wwd.pojo.form.ActivityPageQueryForm;
import scatter.wwd.rest.service.IActivityService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 活动 测试类基类
* </p>
*
* @author yw
* @since 2020-12-09
*/
public class ActivitySuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IActivityService activityService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return activityService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return activityService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Activity mockPo() {
        return JMockData.mock(Activity.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ActivityAddForm mockAddForm() {
        return JMockData.mock(ActivityAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ActivityUpdateForm mockUpdateForm() {
        return JMockData.mock(ActivityUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ActivityPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ActivityPageQueryForm.class, mockConfig);
    }
}