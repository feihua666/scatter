package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.pojo.form.UserInfoAddForm;
import scatter.wwd.pojo.form.UserInfoUpdateForm;
import scatter.wwd.pojo.form.UserInfoPageQueryForm;
import scatter.wwd.rest.test.UserInfoSuperTest;
import scatter.wwd.rest.service.IUserInfoService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户信息 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserInfoServiceTest extends UserInfoSuperTest{

    @Autowired
    private IUserInfoService userInfoService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserInfo> pos = userInfoService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}