package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.pojo.form.UserAlbumAddForm;
import scatter.wwd.pojo.form.UserAlbumUpdateForm;
import scatter.wwd.pojo.form.UserAlbumPageQueryForm;
import scatter.wwd.rest.test.UserAlbumSuperTest;
import scatter.wwd.rest.service.IUserAlbumService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户相册 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserAlbumServiceTest extends UserAlbumSuperTest{

    @Autowired
    private IUserAlbumService userAlbumService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserAlbum> pos = userAlbumService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}