package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserInvitation;
import scatter.wwd.pojo.form.UserInvitationAddForm;
import scatter.wwd.pojo.form.UserInvitationUpdateForm;
import scatter.wwd.pojo.form.UserInvitationPageQueryForm;
import scatter.wwd.rest.test.UserInvitationSuperTest;
import scatter.wwd.rest.service.IUserInvitationService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户邀请 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserInvitationServiceTest extends UserInvitationSuperTest{

    @Autowired
    private IUserInvitationService userInvitationService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserInvitation> pos = userInvitationService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}