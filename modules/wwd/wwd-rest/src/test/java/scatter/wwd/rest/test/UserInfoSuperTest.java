package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserInfo;
import scatter.wwd.pojo.form.UserInfoAddForm;
import scatter.wwd.pojo.form.UserInfoUpdateForm;
import scatter.wwd.pojo.form.UserInfoPageQueryForm;
import scatter.wwd.rest.service.IUserInfoService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户信息 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserInfoSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserInfoService userInfoService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userInfoService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userInfoService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserInfo mockPo() {
        return JMockData.mock(UserInfo.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserInfoAddForm mockAddForm() {
        return JMockData.mock(UserInfoAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserInfoUpdateForm mockUpdateForm() {
        return JMockData.mock(UserInfoUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserInfoPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserInfoPageQueryForm.class, mockConfig);
    }
}