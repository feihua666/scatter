package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserParamQrcode;
import scatter.wwd.pojo.form.UserParamQrcodeAddForm;
import scatter.wwd.pojo.form.UserParamQrcodeUpdateForm;
import scatter.wwd.pojo.form.UserParamQrcodePageQueryForm;
import scatter.wwd.rest.service.IUserParamQrcodeService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户标签 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserParamQrcodeSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserParamQrcodeService userParamQrcodeService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userParamQrcodeService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userParamQrcodeService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserParamQrcode mockPo() {
        return JMockData.mock(UserParamQrcode.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserParamQrcodeAddForm mockAddForm() {
        return JMockData.mock(UserParamQrcodeAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserParamQrcodeUpdateForm mockUpdateForm() {
        return JMockData.mock(UserParamQrcodeUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserParamQrcodePageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserParamQrcodePageQueryForm.class, mockConfig);
    }
}