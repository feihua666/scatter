package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.ActivityEnrollRule;
import scatter.wwd.pojo.form.ActivityEnrollRuleAddForm;
import scatter.wwd.pojo.form.ActivityEnrollRuleUpdateForm;
import scatter.wwd.pojo.form.ActivityEnrollRulePageQueryForm;
import scatter.wwd.rest.test.ActivityEnrollRuleSuperTest;
import scatter.wwd.rest.service.IActivityEnrollRuleService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 活动报名规则 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class ActivityEnrollRuleServiceTest extends ActivityEnrollRuleSuperTest{

    @Autowired
    private IActivityEnrollRuleService activityEnrollRuleService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ActivityEnrollRule> pos = activityEnrollRuleService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}