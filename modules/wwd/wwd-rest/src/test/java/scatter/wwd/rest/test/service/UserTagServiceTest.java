package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserTag;
import scatter.wwd.pojo.form.UserTagAddForm;
import scatter.wwd.pojo.form.UserTagUpdateForm;
import scatter.wwd.pojo.form.UserTagPageQueryForm;
import scatter.wwd.rest.test.UserTagSuperTest;
import scatter.wwd.rest.service.IUserTagService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户标签 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserTagServiceTest extends UserTagSuperTest{

    @Autowired
    private IUserTagService userTagService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserTag> pos = userTagService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}