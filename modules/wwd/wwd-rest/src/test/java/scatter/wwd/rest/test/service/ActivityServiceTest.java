package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.pojo.form.ActivityAddForm;
import scatter.wwd.pojo.form.ActivityUpdateForm;
import scatter.wwd.pojo.form.ActivityPageQueryForm;
import scatter.wwd.rest.test.ActivitySuperTest;
import scatter.wwd.rest.service.IActivityService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 活动 服务测试类
* </p>
*
* @author yw
* @since 2020-12-09
*/
@SpringBootTest
public class ActivityServiceTest extends ActivitySuperTest{

    @Autowired
    private IActivityService activityService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Activity> pos = activityService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}