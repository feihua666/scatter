package scatter.wwd.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.pojo.form.ActivityUserMutualElectionAddForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionUpdateForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionPageQueryForm;
import scatter.wwd.rest.service.IActivityUserMutualElectionService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 汪汪队用户互选 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class ActivityUserMutualElectionSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IActivityUserMutualElectionService activityUserMutualElectionService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return activityUserMutualElectionService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return activityUserMutualElectionService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ActivityUserMutualElection mockPo() {
        return JMockData.mock(ActivityUserMutualElection.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ActivityUserMutualElectionAddForm mockAddForm() {
        return JMockData.mock(ActivityUserMutualElectionAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ActivityUserMutualElectionUpdateForm mockUpdateForm() {
        return JMockData.mock(ActivityUserMutualElectionUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ActivityUserMutualElectionPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ActivityUserMutualElectionPageQueryForm.class, mockConfig);
    }
}