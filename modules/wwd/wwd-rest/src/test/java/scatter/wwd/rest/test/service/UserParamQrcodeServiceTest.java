package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.UserParamQrcode;
import scatter.wwd.pojo.form.UserParamQrcodeAddForm;
import scatter.wwd.pojo.form.UserParamQrcodeUpdateForm;
import scatter.wwd.pojo.form.UserParamQrcodePageQueryForm;
import scatter.wwd.rest.test.UserParamQrcodeSuperTest;
import scatter.wwd.rest.service.IUserParamQrcodeService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户标签 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserParamQrcodeServiceTest extends UserParamQrcodeSuperTest{

    @Autowired
    private IUserParamQrcodeService userParamQrcodeService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserParamQrcode> pos = userParamQrcodeService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}