package scatter.wwd.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wwd.pojo.po.ActivityUserMutualElection;
import scatter.wwd.pojo.form.ActivityUserMutualElectionAddForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionUpdateForm;
import scatter.wwd.pojo.form.ActivityUserMutualElectionPageQueryForm;
import scatter.wwd.rest.test.ActivityUserMutualElectionSuperTest;
import scatter.wwd.rest.service.IActivityUserMutualElectionService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 汪汪队用户互选 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class ActivityUserMutualElectionServiceTest extends ActivityUserMutualElectionSuperTest{

    @Autowired
    private IActivityUserMutualElectionService activityUserMutualElectionService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ActivityUserMutualElection> pos = activityUserMutualElectionService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}