truncate table scatter_wwd.component_moment;
INSERT INTO `scatter_wwd`.`component_moment`
       (`id`, `content`     , `time_at`, `owner_user_id`, `owner_user_nickname`, `owner_user_avatar`, `comment_count`, `star_count`, `is_disabled`, `disabled_reason`, `group_flag`, `link_pic_url`, `link_content`, `link_url`, `version`, `create_at`, `create_by`, `update_at`, `update_by`)

select `id`, ifnull(description,'测试测试'), create_at,  user_id        , `name`               , null               , 0               , 0          , 0             , null            , null        , null          , null          , null      , 1        , create_at  , null        ,null        ,null
from `scatter_wwd`.`wwd_user_info`;
-- 更新头像
update `scatter_wwd`.`component_moment` m
join `scatter_wwd`.`wwd_user` u on m.owner_user_id = u.id
set m.owner_user_avatar = u.avatar;

-- 插入图片
truncate table scatter_wwd.component_moment_pic;
INSERT INTO `scatter_wwd`.`component_moment_pic`
       (`id`, `moment_id`, `pic_url`   , `seq`  , `version`, `create_at`, `create_by`, `update_at`, `update_by`)
SELECT a.`id`, m.`id`      , a.pic_url,   a.seq ,  a.`version`, a.`create_at`, a.`create_by`, a.`update_at`, a.`update_by` FROM  `scatter_wwd`.`component_moment` m
join scatter_wwd.wwd_user_album a  on a.user_id = m.owner_user_id;
