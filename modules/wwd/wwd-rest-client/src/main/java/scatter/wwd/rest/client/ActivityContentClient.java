package scatter.wwd.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 活动内容表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@Component
@FeignClient(value = "ActivityContent-client")
public interface ActivityContentClient {

}
