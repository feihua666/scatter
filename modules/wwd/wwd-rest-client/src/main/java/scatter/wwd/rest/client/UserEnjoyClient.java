package scatter.wwd.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户有意思表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
@FeignClient(value = "UserEnjoy-client")
public interface UserEnjoyClient {

}
