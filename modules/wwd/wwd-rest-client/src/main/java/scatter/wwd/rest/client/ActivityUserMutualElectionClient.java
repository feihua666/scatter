package scatter.wwd.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 汪汪队用户互选 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
@FeignClient(value = "ActivityUserMutualElection-client")
public interface ActivityUserMutualElectionClient {

}
