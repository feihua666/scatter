package scatter.wwd.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户相册表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
@FeignClient(value = "UserAlbum-client")
public interface UserAlbumClient {

}
