package scatter.wwd.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import scatter.area.rest.AreaConfiguration;
import scatter.banner.rest.BannerConfiguration;
import scatter.captcha.rest.CaptchaConfiguration;
import scatter.comment.rest.CommentConfiguration;
import scatter.common.rest.config.CommonCacheConfig;
import scatter.common.rest.config.CommonGlobalMethodSecurityConfig;
import scatter.common.rest.config.CommonRestConfig;
import scatter.common.rest.config.CommonWebSecurityConfig;
import scatter.complaint.rest.ComplaintConfiguration;
import scatter.dict.rest.DictConfiguration;
import scatter.errorlog.rest.ErrorLogConfiguration;
import scatter.feedback.rest.FeedbackConfiguration;
import scatter.file.rest.FileConfiguration;
import scatter.func.rest.FuncConfiguration;
import scatter.identification.rest.IdentificationConfiguration;
import scatter.identifier.rest.IdentifierConfiguration;
import scatter.message.rest.MessageConfiguration;
import scatter.moment.rest.MomentConfiguration;
import scatter.order.rest.OrderConfiguration;
import scatter.pay.rest.PayConfiguration;
import scatter.portrait.rest.PortraitConfiguration;
import scatter.recommend.rest.RecommendConfiguration;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.role.rest.RoleConfiguration;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.sensitive.rest.SensitiveConfiguration;
import scatter.tools.rest.ToolsConfiguration;
import scatter.usersimple.rest.UsersimpleConfiguration;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wxcp.rest.WxCpConfiguration;
import scatter.wxminiapp.rest.WxMiniappConfiguration;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * Created by yangwei
 * Created at 2020/12/8 18:23
 */
@SpringBootApplication
@EnableRedisHttpSession
@Import({CommonCacheConfig.class,CommonRestConfig.class, CommonGlobalMethodSecurityConfig.class, CommonWebSecurityConfig.class,
        WwdConfiguration.class,
        DictConfiguration.class,
        AreaConfiguration.class,
        FuncConfiguration.class,
        UsersimpleConfiguration.class,
        IdentifierConfiguration.class,
        RoleConfiguration.class,
        FileConfiguration.class,
        WxMpConfiguration.class,
        CaptchaConfiguration.class,
        BannerConfiguration.class,
        ErrorLogConfiguration.class,
        ScheduleQuartzConfiguration.class,
        ToolsConfiguration.class,
        MomentConfiguration.class,
        CommentConfiguration.class,
        OrderConfiguration.class,
        PayConfiguration.class,
        WxCpConfiguration.class,
        RecommendConfiguration.class,
        PortraitConfiguration.class,
        IdentificationConfiguration.class,
        RichtextConfiguration.class,
        SensitiveConfiguration.class,
        FeedbackConfiguration.class,
        ComplaintConfiguration.class,
        WxMiniappConfiguration.class,
        MessageConfiguration.class,
})
public class WwdServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(WwdServerApplication.class, args);
    }

}
