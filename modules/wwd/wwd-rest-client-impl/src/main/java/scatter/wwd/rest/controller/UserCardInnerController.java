package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserCard;
import scatter.wwd.rest.service.IUserCardService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户卡片表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-card")
public class UserCardInnerController extends BaseInnerController<UserCard> {
 @Autowired
 private IUserCardService userCardService;

 public IUserCardService getService(){
     return userCardService;
 }
}
