package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.ActivityParticipate;
import scatter.wwd.rest.service.IActivityParticipateService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 活动参与 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/activity-participate")
public class ActivityParticipateInnerController extends BaseInnerController<ActivityParticipate> {
 @Autowired
 private IActivityParticipateService activityParticipateService;

 public IActivityParticipateService getService(){
     return activityParticipateService;
 }
}
