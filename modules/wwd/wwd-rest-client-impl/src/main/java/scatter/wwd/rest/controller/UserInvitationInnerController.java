package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserInvitation;
import scatter.wwd.rest.service.IUserInvitationService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户邀请表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-invitation")
public class UserInvitationInnerController extends BaseInnerController<UserInvitation> {
 @Autowired
 private IUserInvitationService userInvitationService;

 public IUserInvitationService getService(){
     return userInvitationService;
 }
}
