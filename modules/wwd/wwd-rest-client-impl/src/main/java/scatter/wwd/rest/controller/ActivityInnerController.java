package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.pojo.po.Activity;
import scatter.wwd.rest.service.IActivityService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 活动表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/inner/activity")
public class ActivityInnerController extends BaseInnerController<Activity> {
 @Autowired
 private IActivityService activityService;

 public IActivityService getService(){
     return activityService;
 }
}
