package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserEnjoy;
import scatter.wwd.rest.service.IUserEnjoyService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户有意思表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-enjoy")
public class UserEnjoyInnerController extends BaseInnerController<UserEnjoy> {
 @Autowired
 private IUserEnjoyService userEnjoyService;

 public IUserEnjoyService getService(){
     return userEnjoyService;
 }
}
