package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserVisit;
import scatter.wwd.rest.service.IUserVisitService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 汪汪队用户访问 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-visit")
public class UserVisitInnerController extends BaseInnerController<UserVisit> {
 @Autowired
 private IUserVisitService userVisitService;

 public IUserVisitService getService(){
     return userVisitService;
 }
}
