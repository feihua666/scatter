package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserAlbum;
import scatter.wwd.rest.service.IUserAlbumService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户相册表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-album")
public class UserAlbumInnerController extends BaseInnerController<UserAlbum> {
 @Autowired
 private IUserAlbumService userAlbumService;

 public IUserAlbumService getService(){
     return userAlbumService;
 }
}
