package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.rest.WwdConfiguration;
import scatter.wwd.pojo.po.ActivityContent;
import scatter.wwd.rest.service.IActivityContentService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 活动内容表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-09
 */
@RestController
@RequestMapping(WwdConfiguration.CONTROLLER_BASE_PATH + "/inner/activity-content")
public class ActivityContentInnerController extends BaseInnerController<ActivityContent> {
 @Autowired
 private IActivityContentService activityContentService;

 public IActivityContentService getService(){
     return activityContentService;
 }
}
