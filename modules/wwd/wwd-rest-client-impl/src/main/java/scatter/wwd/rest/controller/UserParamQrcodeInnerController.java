package scatter.wwd.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wwd.pojo.po.UserParamQrcode;
import scatter.wwd.rest.service.IUserParamQrcodeService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户标签表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-param-qrcode")
public class UserParamQrcodeInnerController extends BaseInnerController<UserParamQrcode> {
 @Autowired
 private IUserParamQrcodeService userParamQrcodeService;

 public IUserParamQrcodeService getService(){
     return userParamQrcodeService;
 }
}
