import PostUrl from './PostUrl.js'

const PostRoute = [
    {
        path: PostUrl.router.searchList,
        component: () => import('./element/PostSearchList'),
        meta: {
            code:'PostSearchList',
            name: '岗位管理'
        }
    },
    {
        path: PostUrl.router.add,
        component: () => import('./element/PostAdd'),
        meta: {
            code:'PostAdd',
            name: '岗位添加'
        }
    },
    {
        path: PostUrl.router.update,
        component: () => import('./element/PostUpdate'),
        meta: {
            code:'PostUpdate',
            name: '岗位修改'
        }
    },
]
export default PostRoute