import PostForm from './PostForm.js'
import PostTable from './PostTable.js'
import PostUrl from './PostUrl.js'

const PostMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(PostForm,this.$options.name)
        },
        computedTableOptions() {
            return PostTable
        },
        computedUrl(){
            return PostUrl
        }
    },
}
export default PostMixin