import JobUrl from '../../../job/web/pc/JobUrl.js'
import DeptUrl from '../../../dept/web/pc/DeptUrl.js'
const PostForm = [
    {
        PostSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '岗位编码',
            required: true,
        }
    },
    {
        PostSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '岗位名称',
            required: true,
        }
    },
    {
        PostSearchList: false,
        field: {
            name: 'isDisabled',
            valueDefault: false,
        },
        element:{
            label: '是否禁用',
            required: true,
            type: 'switch'
        }
    },
    {
        PostSearchList: false,
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
            required: ({form})=>{
                if (form.isDisabled === true) {
                    return true
                }
                return false
            }
        }
    },
    {
        PostSearchList: false,
        field: {
            name: 'isPublic'
        },
        element:{
            label: '是否公共',
            required: true,
            type: 'switch'
        }
    },
    {
        PostSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'typeDictId'
        },
        element:{
            label: '类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'post_type'
            }
        }
    },
    {
        PostSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'deptId'
        },
        element:{
            label: '部门',
            type: 'cascader',
            options: {
                datas: DeptUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            required: ({form})=>{
                if (form.isPublic === true) {
                    form.deptId = ''
                    return false
                }
                return true
            }
        }
    },
    {
        PostSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'jobId'
        },
        element:{
            label: '职务',
            required: true,
            type: 'select',
            options: {
                datas: JobUrl.list
            }
        }
    },
    {
        PostSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default PostForm