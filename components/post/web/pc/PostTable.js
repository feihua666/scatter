const PostTable = [
    {
        prop: 'code',
        label: '岗位编码'
    },
    {
        prop: 'name',
        label: '岗位名称'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'isPublic',
        label: '是否公共'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'deptName',
        label: '部门'
    },
    {
        prop: 'jobName',
        label: '职务'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default PostTable