const basePath = '' + '/post'
const PostUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PostSearchList',
        add: '/PostAdd',
        update: '/PostUpdate',
    }
}
export default PostUrl