package scatter.post.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.post.pojo.po.Post;
import scatter.post.rest.service.IPostService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 岗位表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/post")
public class PostInnerController extends BaseInnerController<Post> {
 @Autowired
 private IPostService postService;

 public IPostService getService(){
     return postService;
 }
}
