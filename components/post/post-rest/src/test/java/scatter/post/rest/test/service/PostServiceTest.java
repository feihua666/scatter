package scatter.post.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.post.pojo.po.Post;
import scatter.post.pojo.form.PostAddForm;
import scatter.post.pojo.form.PostUpdateForm;
import scatter.post.pojo.form.PostPageQueryForm;
import scatter.post.rest.test.PostSuperTest;
import scatter.post.rest.service.IPostService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 岗位 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class PostServiceTest extends PostSuperTest{

    @Autowired
    private IPostService postService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Post> pos = postService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}