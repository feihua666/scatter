package scatter.post.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.post.pojo.po.Post;
import scatter.post.pojo.form.PostAddForm;
import scatter.post.pojo.form.PostUpdateForm;
import scatter.post.pojo.form.PostPageQueryForm;
import scatter.post.rest.service.IPostService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 岗位 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class PostSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPostService postService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return postService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return postService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Post mockPo() {
        return JMockData.mock(Post.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PostAddForm mockAddForm() {
        return JMockData.mock(PostAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PostUpdateForm mockUpdateForm() {
        return JMockData.mock(PostUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PostPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PostPageQueryForm.class, mockConfig);
    }
}