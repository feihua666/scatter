package scatter.post.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.post.pojo.form.PostAddForm;
import scatter.post.pojo.form.PostPageQueryForm;
import scatter.post.pojo.form.PostUpdateForm;
import scatter.post.pojo.po.Post;
import scatter.post.rest.mapper.PostMapper;
import scatter.post.rest.service.IPostService;
/**
 * <p>
 * 岗位表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class PostServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PostMapper, Post, PostAddForm, PostUpdateForm, PostPageQueryForm> implements IPostService {
    @Override
    public void preAdd(PostAddForm addForm,Post po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PostUpdateForm updateForm,Post po) {
        super.preUpdate(updateForm,po);

    }

    @Override
    public QueryWrapper<Post> prepareQueryFormQueryWrapper(QueryWrapper<Post> queryWrapper, PostPageQueryForm queryForm) {
        if (queryForm.getIsPublic() == null && queryForm.getIsIncludePublic() != null && queryForm.getIsIncludePublic()) {
            queryWrapper = convertEntityConditionToWrapper(queryWrapper);
            queryWrapper.or().lambda().eq(Post::getIsPublic, true);
        }
        return queryWrapper;
    }
}
