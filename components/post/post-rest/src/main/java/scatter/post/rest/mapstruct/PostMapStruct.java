package scatter.post.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.post.pojo.form.PostAddForm;
import scatter.post.pojo.form.PostPageQueryForm;
import scatter.post.pojo.form.PostUpdateForm;
import scatter.post.pojo.po.Post;
import scatter.post.pojo.vo.PostVo;

/**
 * <p>
 * 岗位 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PostMapStruct extends IBaseVoMapStruct<Post, PostVo>,
        IBaseAddFormMapStruct<Post,PostAddForm>,
        IBaseUpdateFormMapStruct<Post,PostUpdateForm>,
        IBaseQueryFormMapStruct<Post,PostPageQueryForm> {
    PostMapStruct INSTANCE = Mappers.getMapper( PostMapStruct.class );

}
