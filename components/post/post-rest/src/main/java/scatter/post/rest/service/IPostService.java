package scatter.post.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.post.pojo.po.Post;
/**
 * <p>
 * 岗位表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IPostService extends IBaseService<Post> {


}
