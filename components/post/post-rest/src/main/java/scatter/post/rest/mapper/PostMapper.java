package scatter.post.rest.mapper;

import scatter.post.pojo.po.Post;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 岗位表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface PostMapper extends IBaseMapper<Post> {

}
