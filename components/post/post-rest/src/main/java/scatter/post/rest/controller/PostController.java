package scatter.post.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.post.rest.PostConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.post.pojo.po.Post;
import scatter.post.pojo.vo.PostVo;
import scatter.post.pojo.form.PostAddForm;
import scatter.post.pojo.form.PostUpdateForm;
import scatter.post.pojo.form.PostPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 岗位表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(PostConfiguration.CONTROLLER_BASE_PATH + "/post")
@Api(tags = "职务岗位")
public class PostController extends BaseAddUpdateQueryFormController<Post, PostVo, PostAddForm, PostUpdateForm, PostPageQueryForm> {


     @Override
	 @ApiOperation("添加岗位")
     @PreAuthorize("hasAuthority('Post:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PostVo add(@RequestBody @Valid PostAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询岗位")
     @PreAuthorize("hasAuthority('Post:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public PostVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除岗位")
     @PreAuthorize("hasAuthority('Post:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新岗位")
     @PreAuthorize("hasAuthority('Post:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PostVo update(@RequestBody @Valid PostUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询岗位")
    @PreAuthorize("hasAuthority('Post:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<PostVo> getList(PostPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询岗位")
    @PreAuthorize("hasAuthority('Post:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<PostVo> getPage(PostPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
