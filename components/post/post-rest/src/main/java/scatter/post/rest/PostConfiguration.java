package scatter.post.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.post.rest.**.mapper")
public class PostConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "post";

}
