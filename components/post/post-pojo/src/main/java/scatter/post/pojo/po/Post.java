package scatter.post.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 岗位表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_post")
@ApiModel(value="Post对象", description="岗位表")
public class Post extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_POST_BY_ID = "trans_post_by_id_scatter.post.pojo.po";

    @ApiModelProperty(value = "岗位编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "岗位名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "是否公共")
    private Boolean isPublic;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "部门id，该岗位归属哪个部门")
    private String deptId;

    @ApiModelProperty(value = "职务id")
    private String jobId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
