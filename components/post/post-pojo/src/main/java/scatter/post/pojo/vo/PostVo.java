package scatter.post.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dept.pojo.po.Dept;
import scatter.dict.pojo.po.Dict;
import scatter.job.pojo.po.Job;


/**
 * <p>
 * 岗位响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="岗位响应对象")
public class PostVo extends BaseIdVo {

    @ApiModelProperty(value = "岗位编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "岗位名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "是否公共")
    private Boolean isPublic;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "类型，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "部门id，该岗位归属哪个部门")
    private String deptId;

    @TransBy(type = Dept.TRANS_DEPT_BY_ID,byFieldName = "deptId",mapValueField = "name")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "职务id")
    private String jobId;

    @TransBy(type = Job.TRANS_JOB_BY_ID,byFieldName = "jobId",mapValueField = "name")
    @ApiModelProperty(value = "职务名称")
    private String jobName;

    @ApiModelProperty(value = "描述")
    private String remark;

}
