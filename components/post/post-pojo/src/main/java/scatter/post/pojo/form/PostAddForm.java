package scatter.post.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 岗位添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@PropValid
@Setter
@Getter
@ApiModel(value="岗位添加表单对象")
public class PostAddForm extends BaseAddForm {

    @NotEmpty(message="岗位编码不能为空")
    @ApiModelProperty(value = "岗位编码，模糊查询",required = true)
    private String code;

    @NotEmpty(message="岗位名称不能为空")
    @ApiModelProperty(value = "岗位名称，模糊查询",required = true)
    private String name;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @NotNull(message="是否公共不能为空")
    @ApiModelProperty(value = "是否公共",required = true)
    private Boolean isPublic;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "部门id，该岗位归属哪个部门")
    private String deptId;

    @NotEmpty(message="职务id不能为空")
    @ApiModelProperty(value = "职务id",required = true)
    private String jobId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
