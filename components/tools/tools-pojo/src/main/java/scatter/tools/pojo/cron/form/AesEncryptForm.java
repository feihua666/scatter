package scatter.tools.pojo.cron.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

/**
 * Created by yangwei
 * Created at 2021/2/5 17:48
 */
@Setter
@Getter
@ApiModel(value="aes加密表单对象")
public class AesEncryptForm extends BaseForm {


    @NotEmpty(message = "加密数据不能为空")
    @ApiModelProperty("加密数据")
    private String data;

    @NotEmpty(message = "加密密钥不能为空")
    @ApiModelProperty("加密密钥")
    private String key;
}
