package scatter.tools.rest.test;

import cn.hutool.core.date.DateUtil;
import cn.hutool.cron.CronException;
import cn.hutool.cron.CronUtil;
import org.springframework.scheduling.support.CronExpression;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by yangwei
 * Created at 2021/2/5 17:29
 */
public class CronTest {
    public static void main(String[] args) {
        LocalDateTime next = LocalDateTime.now();
        for (int i = 0; i < 10; i++) {
            next = CronExpression.parse("0 10,44 14 ? 3 WED").next(next);

            System.out.println(DateUtil.format(next,"yyyy-MM-dd HH:mm:ss"));
        }

    }
}
