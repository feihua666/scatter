package scatter.tools.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.tools.rest.ToolsConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, ToolsConfiguration.class})
@MapperScan("scatter.tools.rest.mapper")
public class ToolsTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(ToolsTestApplication.class, args);
    }
}
