package scatter.tools.rest.aes.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.AES;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.tools.pojo.cron.form.AesEncryptForm;
import scatter.tools.pojo.cron.form.CronQueryForm;
import scatter.tools.rest.ToolsConfiguration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/8 15:35
 */
@Api(tags = "aes相关接口")
@RestController
@RequestMapping(ToolsConfiguration.CONTROLLER_BASE_PATH + "/aes")
public class AESController extends SuperController {

    @ApiOperation("生成 16 位随机 AES 密钥")
    @GetMapping("/randomKey")
    @ResponseStatus(HttpStatus.OK)
    public String randomKey() {
        // 生成 16 位随机 AES 密钥
        String randomKey = AES.generateRandomKey();
        return randomKey;
    }
    @ApiOperation("随机密钥加密")
    @PostMapping("/encrypt")
    @ResponseStatus(HttpStatus.OK)
    public String randomKey( @RequestBody @Validated AesEncryptForm form) {
        // 随机密钥加密
        String result = AES.encrypt(form.getData(), form.getKey());
        return result;
    }
}
