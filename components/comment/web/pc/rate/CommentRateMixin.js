import CommentRateForm from './CommentRateForm.js'
import CommentRateTable from './CommentRateTable.js'
import CommentRateUrl from './CommentRateUrl.js'
const CommentRateMixin = {
    computed: {
        CommentRateFormOptions() {
            return this.$stDynamicFormTools.options(CommentRateForm,this.$options.name)
        },
        CommentRateTableOptions() {
            return CommentRateTable
        },
        CommentRateUrl(){
            return CommentRateUrl
        }
    },
}
export default CommentRateMixin