import CommentRateUrl from './CommentRateUrl.js'
const CommentRateForm = [
    {
        CommentRateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'subjectId',
        },
        element:{
            label: '主体评论表id',
            required: true,
        }
    },
    {
        CommentRateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'timeAt',
        },
        element:{
            label: '时间',
            required: true,
        }
    },
    {
        CommentRateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        CommentRateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '用户昵称',
            required: true,
        }
    },
    {

        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '用户头像',
        }
    },
    {
        CommentRateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'rate',
        },
        element:{
            label: '评分',
            required: true,
        }
    },
    {
        CommentRateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'floor',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '所在楼层',
            required: true,
        }
    },
    {

        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },

]
export default CommentRateForm