const basePath = '' + '/rate/comment-rate'
const CommentRateUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getPage1: basePath + '/getPage1',
    addRate: basePath + '/addRate',
    cancelRate: basePath + '/cancelRate',
    router: {
        searchList: '/CommentRateSearchList',
        add: '/CommentRateAdd',
        update: '/CommentRateUpdate',
    }
}
export default CommentRateUrl