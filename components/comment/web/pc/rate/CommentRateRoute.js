import CommentRateUrl from './CommentRateUrl.js'

const CommentRateRoute = [
    {
        path: CommentRateUrl.router.searchList,
        component: () => import('./element/CommentRateSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentRateSearchList',
            name: '评分管理'
        }
    },
    {
        path: CommentRateUrl.router.add,
        component: () => import('./element/CommentRateAdd'),
        meta: {
            code:'CommentRateAdd',
            name: '评分添加'
        }
    },
    {
        path: CommentRateUrl.router.update,
        component: () => import('./element/CommentRateUpdate'),
        meta: {
            code:'CommentRateUpdate',
            name: '评分修改'
        }
    },
]
export default CommentRateRoute