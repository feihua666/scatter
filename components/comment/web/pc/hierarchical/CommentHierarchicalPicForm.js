import CommentHierarchicalPicUrl from './CommentHierarchicalPicUrl.js'
const CommentHierarchicalPicForm = [
    {
        field: {
            name: 'subjectId',
        },
        element:{
            label: '评论主体id',
        }
    },
    {
        CommentHierarchicalPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'commentHierarchicalId',
        },
        element:{
            label: '评论主体评论层级表id',
            required: true,
        }
    },
    {
        CommentHierarchicalPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'picUrl',
        },
        element:{
            label: '图片地址',
            required: true,
        }
    },
    {
        CommentHierarchicalPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'seq',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        }
    },
]
export default CommentHierarchicalPicForm