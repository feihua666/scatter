const basePath = '' + '/hierarchical/comment-hierarchical-star'
const CommentHierarchicalStarUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    addStar: basePath + '/addStar',
    cancelStar: basePath + '/cancelStar',
    getPage1: basePath + '/getPage1',
    router: {
        searchList: '/CommentHierarchicalStarSearchList',
        add: '/CommentHierarchicalStarAdd',
        update: '/CommentHierarchicalStarUpdate',
    }
}
export default CommentHierarchicalStarUrl