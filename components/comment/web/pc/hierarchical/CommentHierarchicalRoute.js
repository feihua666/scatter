import CommentHierarchicalUrl from './CommentHierarchicalUrl.js'
const CommentHierarchicalRoute = [
    {
        path: CommentHierarchicalUrl.router.searchList,
        component: () => import('./element/CommentHierarchicalSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentHierarchicalSearchList',
            name: '评论层级评论管理'
        }
    },
    {
        path: CommentHierarchicalUrl.router.add,
        component: () => import('./element/CommentHierarchicalAdd'),
        meta: {
            code:'CommentHierarchicalAdd',
            name: '评论层级评论添加'
        }
    },
    {
        path: CommentHierarchicalUrl.router.update,
        component: () => import('./element/CommentHierarchicalUpdate'),
        meta: {
            code:'CommentHierarchicalUpdate',
            name: '评论层级评论修改'
        }
    },
]
export default CommentHierarchicalRoute