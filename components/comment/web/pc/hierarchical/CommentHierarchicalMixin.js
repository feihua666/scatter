import CommentHierarchicalForm from './CommentHierarchicalForm.js'
import CommentHierarchicalTable from './CommentHierarchicalTable.js'
import CommentHierarchicalUrl from './CommentHierarchicalUrl.js'
const CommentHierarchicalMixin = {
    computed: {
        CommentHierarchicalFormOptions() {
            return this.$stDynamicFormTools.options(CommentHierarchicalForm,this.$options.name)
        },
        CommentHierarchicalTableOptions() {
            return CommentHierarchicalTable
        },
        CommentHierarchicalUrl(){
            return CommentHierarchicalUrl
        }
    },
}
export default CommentHierarchicalMixin