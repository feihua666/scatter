import CommentHierarchicalStarUrl from './CommentHierarchicalStarUrl.js'
const CommentHierarchicalStarForm = [
    {
        CommentHierarchicalStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'subjectId',
        },
        element:{
            label: '评论主体id',
            required: true,
        }
    },
    {
        field: {
            name: 'commentHierarchicalId',
        },
        element:{
            label: '评论表id',
        }
    },
    {
        CommentHierarchicalStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'timeAt',
        },
        element:{
            label: '点赞时间',
            required: true,
        }
    },
    {
        CommentHierarchicalStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '点赞人用户id',
            required: true,
        }
    },
    {
        CommentHierarchicalStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '点赞人用户昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '点赞人用户头像',
        }
    },
    {
        CommentHierarchicalStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'floor',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '所在楼层',
            required: true,
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
]
export default CommentHierarchicalStarForm