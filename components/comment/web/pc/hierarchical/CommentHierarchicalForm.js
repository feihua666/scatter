import CommentHierarchicalUrl from './CommentHierarchicalUrl.js'
const CommentHierarchicalForm = [
    {
        field: {
            name: 'subjectId',
        },
        element:{
            label: '评论主体id',
        }
    },
    {
        field: {
            name: 'commentSubjectId',
        },
        element:{
            label: '主体评论表id',
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'content',
        },
        element:{
            label: '评论内容',
            required: true,
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'timeAt',
        },
        element:{
            label: '评论时间',
            required: true,
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '评论人用户id',
            required: true,
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '点赞人用户昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '点赞人用户头像',
        }
    },
    {
        field: {
            name: 'targetUserId',
        },
        element:{
            label: '被评论人用户id',
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'floor',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '所在楼层',
            required: true,
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'commentCount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '子一级评论数',
            required: true,
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'starCount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '点赞数',
            required: true,
        }
    },
    {
        CommentHierarchicalSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: CommentHierarchicalUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },
]
export default CommentHierarchicalForm