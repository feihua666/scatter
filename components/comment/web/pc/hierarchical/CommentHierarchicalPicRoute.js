import CommentHierarchicalPicUrl from './CommentHierarchicalPicUrl.js'
const CommentHierarchicalPicRoute = [
    {
        path: CommentHierarchicalPicUrl.router.searchList,
        component: () => import('./element/CommentHierarchicalPicSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentHierarchicalPicSearchList',
            name: '评论层级评论图片管理'
        }
    },
    {
        path: CommentHierarchicalPicUrl.router.add,
        component: () => import('./element/CommentHierarchicalPicAdd'),
        meta: {
            code:'CommentHierarchicalPicAdd',
            name: '评论层级评论图片添加'
        }
    },
    {
        path: CommentHierarchicalPicUrl.router.update,
        component: () => import('./element/CommentHierarchicalPicUpdate'),
        meta: {
            code:'CommentHierarchicalPicUpdate',
            name: '评论层级评论图片修改'
        }
    },
]
export default CommentHierarchicalPicRoute