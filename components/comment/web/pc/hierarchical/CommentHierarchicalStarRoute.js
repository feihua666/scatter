import CommentHierarchicalStarUrl from './CommentHierarchicalStarUrl.js'
const CommentHierarchicalStarRoute = [
    {
        path: CommentHierarchicalStarUrl.router.searchList,
        component: () => import('./element/CommentHierarchicalStarSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentHierarchicalStarSearchList',
            name: '评论点赞管理'
        }
    },
    {
        path: CommentHierarchicalStarUrl.router.add,
        component: () => import('./element/CommentHierarchicalStarAdd'),
        meta: {
            code:'CommentHierarchicalStarAdd',
            name: '评论点赞添加'
        }
    },
    {
        path: CommentHierarchicalStarUrl.router.update,
        component: () => import('./element/CommentHierarchicalStarUpdate'),
        meta: {
            code:'CommentHierarchicalStarUpdate',
            name: '评论点赞修改'
        }
    },
]
export default CommentHierarchicalStarRoute