import CommentHierarchicalPicForm from './CommentHierarchicalPicForm.js'
import CommentHierarchicalPicTable from './CommentHierarchicalPicTable.js'
import CommentHierarchicalPicUrl from './CommentHierarchicalPicUrl.js'
const CommentHierarchicalPicMixin = {
    computed: {
        CommentHierarchicalPicFormOptions() {
            return this.$stDynamicFormTools.options(CommentHierarchicalPicForm,this.$options.name)
        },
        CommentHierarchicalPicTableOptions() {
            return CommentHierarchicalPicTable
        },
        CommentHierarchicalPicUrl(){
            return CommentHierarchicalPicUrl
        }
    },
}
export default CommentHierarchicalPicMixin