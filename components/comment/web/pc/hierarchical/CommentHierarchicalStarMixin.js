import CommentHierarchicalStarForm from './CommentHierarchicalStarForm.js'
import CommentHierarchicalStarTable from './CommentHierarchicalStarTable.js'
import CommentHierarchicalStarUrl from './CommentHierarchicalStarUrl.js'
const CommentHierarchicalStarMixin = {
    computed: {
        CommentHierarchicalStarFormOptions() {
            return this.$stDynamicFormTools.options(CommentHierarchicalStarForm,this.$options.name)
        },
        CommentHierarchicalStarTableOptions() {
            return CommentHierarchicalStarTable
        },
        CommentHierarchicalStarUrl(){
            return CommentHierarchicalStarUrl
        }
    },
}
export default CommentHierarchicalStarMixin