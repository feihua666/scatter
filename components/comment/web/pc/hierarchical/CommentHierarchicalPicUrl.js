const basePath = '' + '/hierarchical/comment-hierarchical-pic'
const CommentHierarchicalPicUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/CommentHierarchicalPicSearchList',
        add: '/CommentHierarchicalPicAdd',
        update: '/CommentHierarchicalPicUpdate',
    }
}
export default CommentHierarchicalPicUrl