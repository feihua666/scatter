const CommentHierarchicalPicTable = [
    {
        prop: 'subjectId',
        label: '评论主体id'
    },
    {
        prop: 'commentHierarchicalId',
        label: '评论主体评论层级表id'
    },
    {
        prop: 'picUrl',
        label: '图片地址'
    },
    {
        prop: 'seq',
        label: '排序'
    },
]
export default CommentHierarchicalPicTable