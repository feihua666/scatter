const basePath = '' + '/comment-hierarchical'
const CommentHierarchicalUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    // 区别于 queryById ，比 queryById数据更全
    detail: basePath + '/detail/{id}',
    getAvailableForSubjectPage: basePath + '/getAvailableForSubjectPage',
    getAvailableForHierarchicalPage: basePath + '/getAvailableForHierarchicalPage',
    addCommentHierarchicalForSubject: basePath + '/addCommentHierarchicalForSubject',
    addCommentHierarchicalForHierarchical: basePath + '/addCommentHierarchicalForHierarchical',
    router: {
        searchList: '/CommentHierarchicalSearchList',
        add: '/CommentHierarchicalAdd',
        update: '/CommentHierarchicalUpdate',
    }
}
export default CommentHierarchicalUrl