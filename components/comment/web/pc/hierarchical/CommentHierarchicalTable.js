const CommentHierarchicalTable = [
    {
        prop: 'subjectId',
        label: '评论主体id'
    },
    {
        prop: 'commentSubjectId',
        label: '主体评论表id'
    },
    {
        prop: 'content',
        label: '评论内容'
    },
    {
        prop: 'timeAt',
        label: '评论时间'
    },
    {
        prop: 'ownerUserId',
        label: '评论人用户id'
    },
    {
        prop: 'ownerUserNickname',
        label: '点赞人用户昵称'
    },
    {
        prop: 'ownerUserAvatar',
        label: '点赞人用户头像'
    },
    {
        prop: 'targetUserId',
        label: '被评论人用户id'
    },
    {
        prop: 'floor',
        label: '所在楼层'
    },
    {
        prop: 'commentCount',
        label: '子一级评论数'
    },
    {
        prop: 'starCount',
        label: '点赞数'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default CommentHierarchicalTable