import CommentEnjoyForm from './CommentEnjoyForm.js'
import CommentEnjoyTable from './CommentEnjoyTable.js'
import CommentEnjoyUrl from './CommentEnjoyUrl.js'
const CommentEnjoyMixin = {
    computed: {
        CommentEnjoyFormOptions() {
            return this.$stDynamicFormTools.options(CommentEnjoyForm,this.$options.name)
        },
        CommentEnjoyTableOptions() {
            return CommentEnjoyTable
        },
        CommentEnjoyUrl(){
            return CommentEnjoyUrl
        }
    },
}
export default CommentEnjoyMixin