const CommentEnjoyTable = [
    {
        prop: 'subjectId',
        label: '主体评论表id'
    },
    {
        prop: 'timeAt',
        label: '时间'
    },
    {
        prop: 'ownerUserId',
        label: '用户id'
    },
    {
        prop: 'ownerUserNickname',
        label: '用户昵称'
    },
    {
        prop: 'ownerUserAvatar',
        label: '用户头像'
    },
    {
        prop: 'floor',
        label: '所在楼层'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
]
export default CommentEnjoyTable