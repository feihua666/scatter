const basePath = '' + '/enjoy/comment-enjoy'
const CommentEnjoyUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getPage1: basePath + '/getPage1',
    addEnjoy: basePath + '/addEnjoy',
    cancelEnjoy: basePath + '/cancelEnjoy',
    router: {
        searchList: '/CommentEnjoySearchList',
        add: '/CommentEnjoyAdd',
        update: '/CommentEnjoyUpdate',
    }
}
export default CommentEnjoyUrl