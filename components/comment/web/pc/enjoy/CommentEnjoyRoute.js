import CommentEnjoyUrl from './CommentEnjoyUrl.js'

const CommentEnjoyRoute = [
    {
        path: CommentEnjoyUrl.router.searchList,
        component: () => import('./element/CommentEnjoySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentEnjoySearchList',
            name: '喜欢|想去|想看管理'
        }
    },
    {
        path: CommentEnjoyUrl.router.add,
        component: () => import('./element/CommentEnjoyAdd'),
        meta: {
            code:'CommentEnjoyAdd',
            name: '喜欢|想去|想看添加'
        }
    },
    {
        path: CommentEnjoyUrl.router.update,
        component: () => import('./element/CommentEnjoyUpdate'),
        meta: {
            code:'CommentEnjoyUpdate',
            name: '喜欢|想去|想看修改'
        }
    },
]
export default CommentEnjoyRoute