import CommentStarForm from './CommentStarForm.js'
import CommentStarTable from './CommentStarTable.js'
import CommentStarUrl from './CommentStarUrl.js'
const CommentStarMixin = {
    computed: {
        CommentStarFormOptions() {
            return this.$stDynamicFormTools.options(CommentStarForm,this.$options.name)
        },
        CommentStarTableOptions() {
            return CommentStarTable
        },
        CommentStarUrl(){
            return CommentStarUrl
        }
    },
}
export default CommentStarMixin