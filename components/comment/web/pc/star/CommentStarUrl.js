const basePath = '' + '/star/comment-star'
const CommentStarUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getPage1: basePath + '/getPage1',
    addStar: basePath + '/addStar',
    cancelStar: basePath + '/cancelStar',
    router: {
        searchList: '/CommentStarSearchList',
        add: '/CommentStarAdd',
        update: '/CommentStarUpdate',
    }
}
export default CommentStarUrl