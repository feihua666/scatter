import CommentStarUrl from './CommentStarUrl.js'

const CommentStarRoute = [
    {
        path: CommentStarUrl.router.searchList,
        component: () => import('./element/CommentStarSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentStarSearchList',
            name: '主体点赞管理'
        }
    },
    {
        path: CommentStarUrl.router.add,
        component: () => import('./element/CommentStarAdd'),
        meta: {
            code:'CommentStarAdd',
            name: '主体点赞添加'
        }
    },
    {
        path: CommentStarUrl.router.update,
        component: () => import('./element/CommentStarUpdate'),
        meta: {
            code:'CommentStarUpdate',
            name: '主体点赞修改'
        }
    },
]
export default CommentStarRoute