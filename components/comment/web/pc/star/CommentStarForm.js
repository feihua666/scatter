import CommentStarUrl from './CommentStarUrl.js'
const CommentStarForm = [
    {
        CommentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'subjectId',
        },
        element:{
            label: '主体id',
            required: true,
        }
    },
    {
        CommentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'timeAt',
        },
        element:{
            label: '点赞时间',
            required: true,
        }
    },
    {
        CommentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '点赞人用户id',
            required: true,
        }
    },
    {
        CommentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '点赞人用户昵称',
            required: true,
        }
    },
    {

        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '点赞人用户头像',
        }
    },
    {
        CommentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'floor',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '所在楼层',
            required: true,
        }
    },
    {

        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },

]
export default CommentStarForm