const basePath = '' + '/comment-subject-pic'
const CommentSubjectPicUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/CommentSubjectPicSearchList',
        add: '/CommentSubjectPicAdd',
        update: '/CommentSubjectPicUpdate',
    }
}
export default CommentSubjectPicUrl