import CommentSubjectPicUrl from './CommentSubjectPicUrl.js'
const CommentSubjectPicForm = [
    {
        CommentSubjectPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'subjectId',
        },
        element:{
            label: '评论主体id',
            required: true,
        }
    },
    {
        CommentSubjectPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'commentSubjectId',
        },
        element:{
            label: '评论主体评论表id',
            required: true,
        }
    },
    {
        CommentSubjectPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'picUrl',
        },
        element:{
            label: '图片地址',
            required: true,
        }
    },
    {
        CommentSubjectPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'seq',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        }
    },
]
export default CommentSubjectPicForm