import CommentSubjectPicForm from './CommentSubjectPicForm.js'
import CommentSubjectPicTable from './CommentSubjectPicTable.js'
import CommentSubjectPicUrl from './CommentSubjectPicUrl.js'
const CommentSubjectPicMixin = {
    computed: {
        CommentSubjectPicFormOptions() {
            return this.$stDynamicFormTools.options(CommentSubjectPicForm,this.$options.name)
        },
        CommentSubjectPicTableOptions() {
            return CommentSubjectPicTable
        },
        CommentSubjectPicUrl(){
            return CommentSubjectPicUrl
        }
    },
}
export default CommentSubjectPicMixin