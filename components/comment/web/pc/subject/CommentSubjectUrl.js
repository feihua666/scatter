const basePath = '' + '/comment-subject'
const CommentSubjectUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    // 区别于 queryById ，比 queryById 数据更详细
    detail: basePath + '/detail/{id}',
    addCommentSubject: basePath + '/addCommentSubject',
    getAvailablePage: basePath + '/getAvailablePage',
    router: {
        searchList: '/CommentSubjectSearchList',
        add: '/CommentSubjectAdd',
        update: '/CommentSubjectUpdate',
    }
}
export default CommentSubjectUrl