import CommentSubjectStarForm from './CommentSubjectStarForm.js'
import CommentSubjectStarTable from './CommentSubjectStarTable.js'
import CommentSubjectStarUrl from './CommentSubjectStarUrl.js'
const CommentSubjectStarMixin = {
    computed: {
        CommentSubjectStarFormOptions() {
            return this.$stDynamicFormTools.options(CommentSubjectStarForm,this.$options.name)
        },
        CommentSubjectStarTableOptions() {
            return CommentSubjectStarTable
        },
        CommentSubjectStarUrl(){
            return CommentSubjectStarUrl
        }
    },
}
export default CommentSubjectStarMixin