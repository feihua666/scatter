const basePath = '' + '/comment-subject-star'
const CommentSubjectStarUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getPage1: basePath + '/getPage1',
    addStar: basePath + '/addStar',
    cancelStar: basePath + '/cancelStar',
    router: {
        searchList: '/CommentSubjectStarSearchList',
        add: '/CommentSubjectStarAdd',
        update: '/CommentSubjectStarUpdate',
    }
}
export default CommentSubjectStarUrl