import CommentSubjectUrl from './CommentSubjectUrl.js'
const CommentSubjectForm = [
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'subjectId',
        },
        element:{
            label: '评论主体id',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'content',
        },
        element:{
            label: '评论内容',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'timeAt',
        },
        element:{
            label: '评论时间',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '评论人用户id',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '点赞人用户昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '点赞人用户头像',
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'floor',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '所在楼层',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'commentCount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '子一级评论数',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'starCount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '点赞数',
            required: true,
        }
    },
    {
        CommentSubjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
]
export default CommentSubjectForm