import CommentSubjectPicUrl from './CommentSubjectPicUrl.js'
const CommentSubjectPicRoute = [
    {
        path: CommentSubjectPicUrl.router.searchList,
        component: () => import('./element/CommentSubjectPicSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentSubjectPicSearchList',
            name: '主体评论图片管理'
        }
    },
    {
        path: CommentSubjectPicUrl.router.add,
        component: () => import('./element/CommentSubjectPicAdd'),
        meta: {
            code:'CommentSubjectPicAdd',
            name: '主体评论图片添加'
        }
    },
    {
        path: CommentSubjectPicUrl.router.update,
        component: () => import('./element/CommentSubjectPicUpdate'),
        meta: {
            code:'CommentSubjectPicUpdate',
            name: '主体评论图片修改'
        }
    },
]
export default CommentSubjectPicRoute