import CommentSubjectUrl from './CommentSubjectUrl.js'
const CommentSubjectRoute = [
    {
        path: CommentSubjectUrl.router.searchList,
        component: () => import('./element/CommentSubjectSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentSubjectSearchList',
            name: '主体评论管理'
        }
    },
    {
        path: CommentSubjectUrl.router.add,
        component: () => import('./element/CommentSubjectAdd'),
        meta: {
            code:'CommentSubjectAdd',
            name: '主体评论添加'
        }
    },
    {
        path: CommentSubjectUrl.router.update,
        component: () => import('./element/CommentSubjectUpdate'),
        meta: {
            code:'CommentSubjectUpdate',
            name: '主体评论修改'
        }
    },
]
export default CommentSubjectRoute