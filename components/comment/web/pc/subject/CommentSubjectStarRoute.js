import CommentSubjectStarUrl from './CommentSubjectStarUrl.js'
const CommentSubjectStarRoute = [
    {
        path: CommentSubjectStarUrl.router.searchList,
        component: () => import('./element/CommentSubjectStarSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CommentSubjectStarSearchList',
            name: '主体评论点赞管理'
        }
    },
    {
        path: CommentSubjectStarUrl.router.add,
        component: () => import('./element/CommentSubjectStarAdd'),
        meta: {
            code:'CommentSubjectStarAdd',
            name: '主体评论点赞添加'
        }
    },
    {
        path: CommentSubjectStarUrl.router.update,
        component: () => import('./element/CommentSubjectStarUpdate'),
        meta: {
            code:'CommentSubjectStarUpdate',
            name: '主体评论点赞修改'
        }
    },
]
export default CommentSubjectStarRoute