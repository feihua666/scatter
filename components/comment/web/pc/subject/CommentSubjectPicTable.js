const CommentSubjectPicTable = [
    {
        prop: 'subjectId',
        label: '评论主体id'
    },
    {
        prop: 'commentSubjectId',
        label: '评论主体评论表id'
    },
    {
        prop: 'picUrl',
        label: '图片地址'
    },
    {
        prop: 'seq',
        label: '排序'
    },
]
export default CommentSubjectPicTable