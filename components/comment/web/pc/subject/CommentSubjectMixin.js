import CommentSubjectForm from './CommentSubjectForm.js'
import CommentSubjectTable from './CommentSubjectTable.js'
import CommentSubjectUrl from './CommentSubjectUrl.js'
const CommentSubjectMixin = {
    computed: {
        CommentSubjectFormOptions() {
            return this.$stDynamicFormTools.options(CommentSubjectForm,this.$options.name)
        },
        CommentSubjectTableOptions() {
            return CommentSubjectTable
        },
        CommentSubjectUrl(){
            return CommentSubjectUrl
        }
    },
}
export default CommentSubjectMixin