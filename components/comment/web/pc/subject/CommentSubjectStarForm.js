import CommentSubjectStarUrl from './CommentSubjectStarUrl.js'
const CommentSubjectStarForm = [
    {
        CommentSubjectStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'subjectId',
        },
        element:{
            label: '评论主体id',
            required: true,
        }
    },
    {
        field: {
            name: 'commentSubjectId',
        },
        element:{
            label: '主体评论表id',
        }
    },
    {
        CommentSubjectStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'timeAt',
        },
        element:{
            label: '点赞时间',
            required: true,
        }
    },
    {
        CommentSubjectStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '点赞人用户id',
            required: true,
        }
    },
    {
        CommentSubjectStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '点赞人用户昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '点赞人用户头像',
        }
    },
    {
        CommentSubjectStarSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'floor',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '所在楼层',
            required: true,
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
]
export default CommentSubjectStarForm