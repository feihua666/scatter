package scatter.comment.rest.rate.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 评分表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Component
@FeignClient(value = "CommentRate-client")
public interface CommentRateClient {

}
