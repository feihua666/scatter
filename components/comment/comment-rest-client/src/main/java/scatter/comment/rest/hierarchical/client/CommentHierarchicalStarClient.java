package scatter.comment.rest.hierarchical.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 评论点赞表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
@FeignClient(value = "CommentHierarchicalStar-client")
public interface CommentHierarchicalStarClient {

}
