package scatter.comment.rest.subject.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 主体评论表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
@FeignClient(value = "CommentSubject-client")
public interface CommentSubjectClient {

}
