package scatter.comment.rest.enjoy.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 喜欢|想去|想看表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Component
@FeignClient(value = "CommentEnjoy-client")
public interface CommentEnjoyClient {

}
