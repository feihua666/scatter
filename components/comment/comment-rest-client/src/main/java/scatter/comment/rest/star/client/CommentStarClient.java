package scatter.comment.rest.star.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 主体点赞表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
@Component
@FeignClient(value = "CommentStar-client")
public interface CommentStarClient {

}
