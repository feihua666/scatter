package scatter.comment.rest.star.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.rest.star.service.ICommentStarService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 主体点赞表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/inner/star/comment-star")
public class CommentStarInnerController extends BaseInnerController<CommentStar> {
 @Autowired
 private ICommentStarService commentStarService;

 public ICommentStarService getService(){
     return commentStarService;
 }
}
