package scatter.comment.rest.rate.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.rest.rate.service.ICommentRateService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 评分表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/inner/rate/comment-rate")
public class CommentRateInnerController extends BaseInnerController<CommentRate> {
 @Autowired
 private ICommentRateService commentRateService;

 public ICommentRateService getService(){
     return commentRateService;
 }
}
