package scatter.comment.rest.hierarchical.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 评论层级评论表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/inner/comment-hierarchical")
public class CommentHierarchicalInnerController extends BaseInnerController<CommentHierarchical> {
 @Autowired
 private ICommentHierarchicalService commentHierarchicalService;

 public ICommentHierarchicalService getService(){
     return commentHierarchicalService;
 }
}
