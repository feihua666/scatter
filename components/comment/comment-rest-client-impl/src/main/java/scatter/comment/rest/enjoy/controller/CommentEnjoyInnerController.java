package scatter.comment.rest.enjoy.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.rest.enjoy.service.ICommentEnjoyService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 喜欢|想去|想看表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/inner/enjoy/comment-enjoy")
public class CommentEnjoyInnerController extends BaseInnerController<CommentEnjoy> {
 @Autowired
 private ICommentEnjoyService commentEnjoyService;

 public ICommentEnjoyService getService(){
     return commentEnjoyService;
 }
}
