package scatter.comment.rest.subject.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.comment.rest.subject.service.ICommentSubjectStarService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 主体评论点赞表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/inner/comment-subject-star")
public class CommentSubjectStarInnerController extends BaseInnerController<CommentSubjectStar> {
 @Autowired
 private ICommentSubjectStarService commentSubjectStarService;

 public ICommentSubjectStarService getService(){
     return commentSubjectStarService;
 }
}
