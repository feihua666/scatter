DROP TABLE IF EXISTS component_comment_star;
CREATE TABLE `component_comment_star` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `subject_id` varchar(20) NOT NULL COMMENT '主体id',
  `time_at` datetime NOT NULL COMMENT '点赞时间',
  `owner_user_id` varchar(20) NOT NULL COMMENT '点赞人用户id',
  `owner_user_nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '点赞人用户昵称',
  `owner_user_avatar` varchar(300) DEFAULT NULL COMMENT '点赞人用户头像',
  `floor` int NOT NULL COMMENT '所在楼层，排序，升序',
  `group_flag` varchar(255) DEFAULT NULL COMMENT '分组标识',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `comment_subject_id_2` (`subject_id`,`owner_user_id`,`group_flag`),
  KEY `comment_subject_id` (`subject_id`),
  KEY `owner_user_id` (`owner_user_id`),
  KEY `group_flag` (`group_flag`),
  KEY `create_at` (`create_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='主体点赞表';
