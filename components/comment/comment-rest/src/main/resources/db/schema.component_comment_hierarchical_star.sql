DROP TABLE IF EXISTS component_comment_hierarchical_star;
CREATE TABLE `component_comment_hierarchical_star` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `comment_hierarchical_id` varchar(20) NOT NULL COMMENT '评论表id',
  `time_at` datetime NOT NULL COMMENT '点赞时间',
  `owner_user_id` varchar(20) NOT NULL COMMENT '点赞人用户id',
  `owner_user_nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '点赞人用户昵称',
  `owner_user_avatar` varchar(300) DEFAULT NULL COMMENT '点赞人用户头像',
  `floor` int(11) NOT NULL COMMENT '所在楼层，排序，升序',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `comment_hierarchical_id` (`comment_hierarchical_id`),
  KEY `owner_user_id` (`owner_user_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='评论点赞表';
