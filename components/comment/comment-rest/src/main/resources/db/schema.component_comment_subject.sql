DROP TABLE IF EXISTS component_comment_subject;
CREATE TABLE `component_comment_subject` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `subject_id` varchar(20) NOT NULL COMMENT '评论主体id',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '评论内容',
  `time_at` datetime NOT NULL COMMENT '评论时间',
  `owner_user_id` varchar(20) NOT NULL COMMENT '评论人用户id',
  `owner_user_nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '点赞人用户昵称',
  `owner_user_avatar` varchar(300) DEFAULT NULL COMMENT '点赞人用户头像',
  `floor` int(11) NOT NULL COMMENT '所在楼层，排序，升序',
  `comment_count` int(11) NOT NULL COMMENT '子一级评论数',
  `star_count` int(11) NOT NULL COMMENT '点赞数',
  `is_disabled` tinyint(1) NOT NULL COMMENT '是否禁用',
  `disabled_reason` varchar(255) DEFAULT NULL COMMENT '禁用原因',
  `group_flag` varchar(255) DEFAULT NULL COMMENT '分组标识',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `subject_id` (`subject_id`) USING BTREE,
  KEY `owner_user_id` (`owner_user_id`),
  KEY `is_disabled` (`is_disabled`),
  KEY `version` (`version`),
  KEY `create_at` (`create_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='主体评论表';
