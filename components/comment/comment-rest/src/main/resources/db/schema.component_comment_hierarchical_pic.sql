DROP TABLE IF EXISTS component_comment_hierarchical_pic;
CREATE TABLE `component_comment_hierarchical_pic` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `comment_hierarchical_id` varchar(20) NOT NULL COMMENT '评论主体评论层级表id',
  `pic_url` varchar(300) NOT NULL COMMENT '图片地址',
  `seq` int(2) NOT NULL COMMENT '排序，默认升序',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `comment_hierarchical_id` (`comment_hierarchical_id`) USING BTREE,
  KEY `seq` (`seq`),
  KEY `create_at` (`create_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='评论层级评论图片表';
