package scatter.comment.rest.hierarchical.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalPic;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalPicService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 评论层级评论图片翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
public class CommentHierarchicalPicTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentHierarchicalPicService commentHierarchicalPicService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentHierarchicalPic.TRANS_COMMENTHIERARCHICALPIC_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentHierarchicalPic.TRANS_COMMENTHIERARCHICALPIC_BY_ID)) {
            CommentHierarchicalPic byId = commentHierarchicalPicService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentHierarchicalPic.TRANS_COMMENTHIERARCHICALPIC_BY_ID)) {
            return commentHierarchicalPicService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
