package scatter.comment.rest.componentext;

import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;

/**
 * <p>
 * 扩展添加层级评论监听
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 13:15
 */
public interface CommentHierarchicalOnAddListener {

	/**
	 * 添加层级评论前调用
	 * 本监听只针对于主体
	 * @param addParam 属性 subjectId 一定有值
	 * @param commentHierarchical
	 */
	void preCommentHierarchicalForSubjectAdd(CommentHierarchicalForSubjectAddParam addParam, CommentHierarchical commentHierarchical);

	/**
	 * 添加层级评论后调用
	 * 本监听只针对于主体
	 * @param addParam 属性 subjectId 一定有值
	 * @param commentHierarchical
	 */
	void postCommentHierarchicalForSubjectAdd(CommentHierarchicalForSubjectAddParam addParam, CommentHierarchical commentHierarchical);

	/**
	 * 添加层级评论前调用 区别于 {@link CommentHierarchicalOnAddListener#preCommentHierarchicalForSubjectAdd(scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam, scatter.comment.pojo.hierarchical.po.CommentHierarchical)}
	 * 本监听是盖楼的监听
	 * @param addParam 属性 subjectId 一定没值
	 * @param commentHierarchical
	 */
	void preCommentHierarchicalForHierarchicalAdd(CommentHierarchicalForHierarchicalAddParam addParam, CommentHierarchical commentHierarchical);


	/**
	 * 添加层级评论后调用 区别于 {@link CommentHierarchicalOnAddListener#postCommentHierarchicalForSubjectAdd(scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam, scatter.comment.pojo.hierarchical.po.CommentHierarchical)}
	 * 本监听是盖楼的监听
	 * @param addParam 属性 subjectId 一定没值
	 * @param commentHierarchical
	 */
	void postCommentHierarchicalForHierarchicalAdd(CommentHierarchicalForHierarchicalAddParam addParam, CommentHierarchical commentHierarchical);

}
