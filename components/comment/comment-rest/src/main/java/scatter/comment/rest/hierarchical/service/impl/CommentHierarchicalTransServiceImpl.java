package scatter.comment.rest.hierarchical.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 评论层级评论翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
public class CommentHierarchicalTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentHierarchicalService commentHierarchicalService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentHierarchical.TRANS_COMMENTHIERARCHICAL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentHierarchical.TRANS_COMMENTHIERARCHICAL_BY_ID)) {
            CommentHierarchical byId = commentHierarchicalService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentHierarchical.TRANS_COMMENTHIERARCHICAL_BY_ID)) {
            return commentHierarchicalService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
