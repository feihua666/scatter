package scatter.comment.rest.componentimpl;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.rest.componentext.CommentHierarchicalOnAddListener;
import scatter.comment.rest.hierarchical.mapper.CommentHierarchicalMapper;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;
import scatter.comment.rest.subject.service.ICommentSubjectService;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 13:20
 */
@Component
public class CommentSubjectCommentHierarchicalOnAddListener implements CommentHierarchicalOnAddListener {

	@Lazy
	@Autowired
	private ICommentSubjectService iCommentSubjectService;

	@Autowired
	private CommentHierarchicalMapper commentHierarchicalMapper;
	/**
	 * 这里加Lazy注解，循环依赖了
	 */
	@Lazy
	@Autowired
	private ICommentHierarchicalService iCommentHierarchicalService;

	@Override
	public void preCommentHierarchicalForSubjectAdd(CommentHierarchicalForSubjectAddParam addParam, CommentHierarchical commentHierarchical) {

	}

	@Override
	public void postCommentHierarchicalForSubjectAdd(CommentHierarchicalForSubjectAddParam addParam, CommentHierarchical commentHierarchical) {
		if (StrUtil.isNotEmpty(commentHierarchical.getSubjectId())) {
			CommentSubject commentSubject = iCommentSubjectService.getById(commentHierarchical.getSubjectId());
			if (commentSubject != null) {
				// 评论数加1
				iCommentSubjectService.plusForColumnById(commentSubject.getId(),CommentSubject::getCommentCount,1);
				// 更新楼层,如果参数中没有指定，尝试修正
				if (addParam.getFloor() == null) {
					// 默认默认添加的是楼层为1，如果当前评论楼层 比1大，再尝试更新
					if (commentSubject.getCommentCount() > 1) {
						CommentHierarchical commentHierarchical1 = new CommentHierarchical();
						commentHierarchical1.setId(commentHierarchical.getId());
						commentHierarchical1.setFloor(commentSubject.getCommentCount() + 1);
						commentHierarchicalMapper.updateById(commentHierarchical1);
					}
				}

			}
		}

	}

	@Override
	public void preCommentHierarchicalForHierarchicalAdd(CommentHierarchicalForHierarchicalAddParam addParam, CommentHierarchical commentHierarchical) {

	}

	@Override
	public void postCommentHierarchicalForHierarchicalAdd(CommentHierarchicalForHierarchicalAddParam addParam, CommentHierarchical commentHierarchical) {
		if (StrUtil.isNotEmpty(commentHierarchical.getParentId())) {
			CommentHierarchical byId = iCommentHierarchicalService.getById(commentHierarchical.getParentId());
			if (byId != null) {
				// 评论数加1
				iCommentHierarchicalService.plusForColumnById(byId.getId(),CommentHierarchical::getCommentCount,1);
				// 更新楼层,如果参数中没有指定，尝试修正
				if (addParam.getFloor() == null) {
					// 默认默认添加的是楼层为1，如果当前评论楼层 比1大，再尝试更新
					if (byId.getCommentCount() > 1) {
						CommentHierarchical commentHierarchical1 = new CommentHierarchical();
						commentHierarchical1.setId(commentHierarchical.getId());
						commentHierarchical1.setFloor(byId.getCommentCount() + 1);
						commentHierarchicalMapper.updateById(commentHierarchical1);
					}
				}
			}
		}
	}
}
