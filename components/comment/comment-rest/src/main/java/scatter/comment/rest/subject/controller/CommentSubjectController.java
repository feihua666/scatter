package scatter.comment.rest.subject.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.subject.form.*;
import scatter.comment.pojo.subject.param.CommentSubjectAddParam;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.comment.pojo.subject.vo.CommentSubjectAvailableVo;
import scatter.comment.rest.CommentConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RestController;
import scatter.comment.rest.componentext.CommentSubjectAvailableExtProvider;
import scatter.comment.rest.hierarchical.mapstruct.CommentHierarchicalMapStruct;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;
import scatter.comment.rest.subject.mapstruct.CommentSubjectMapStruct;
import scatter.comment.rest.subject.mapstruct.CommentSubjectStarMapStruct;
import scatter.comment.rest.subject.service.ICommentSubjectService;
import scatter.comment.rest.subject.service.ICommentSubjectStarService;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.pojo.subject.vo.CommentSubjectVo;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 主体评论表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "主体评论相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/comment-subject")
public class CommentSubjectController extends BaseAddUpdateQueryFormController<CommentSubject, CommentSubjectVo, CommentSubjectAddForm, CommentSubjectUpdateForm, CommentSubjectPageQueryForm> {

    @Autowired
    private ICommentSubjectService iCommentSubjectService;

    
     @Override
	 @ApiOperation("添加主体评论")
     @PreAuthorize("hasAuthority('CommentSubject:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentSubjectVo add(@RequestBody @Valid CommentSubjectAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询主体评论")
     @PreAuthorize("hasAuthority('CommentSubject:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public CommentSubjectVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除主体评论")
     @PreAuthorize("hasAuthority('CommentSubject:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新主体评论")
     @PreAuthorize("hasAuthority('CommentSubject:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentSubjectVo update(@RequestBody @Valid CommentSubjectUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询主体评论")
    @PreAuthorize("hasAuthority('CommentSubject:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentSubjectVo> getList(CommentSubjectPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询主体评论")
    @PreAuthorize("hasAuthority('CommentSubject:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentSubjectVo> getPage(CommentSubjectPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("根据ID查询主体评论")
    @GetMapping("/detail/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CommentSubjectAvailableVo detailById(@PathVariable String id,String groupFlag,@ApiIgnore LoginUser loginUser) throws InterruptedException {
        CommentSubjectAvailableVo byId = iCommentSubjectService.detailById(id,groupFlag,Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
        return byId;
    }



    @ApiOperation("分页查询可展示主体评论")
    @GetMapping("/getAvailablePage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentSubjectAvailableVo> getAvailablePage(@Valid CommentSubjectAvailablePageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) throws InterruptedException {
        IPage<CommentSubjectAvailableVo> commentSubjectIPage = iCommentSubjectService.listAvailablePage(listPageForm, Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
        return commentSubjectIPage;
    }

    @ApiOperation("添加主体评论")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addCommentSubject")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentSubjectVo addCommentSubject(@RequestBody @Valid CommentSubjectAddForm1 addForm, @ApiIgnore LoginUser loginUser) {
        CommentSubjectAddParam commentSubjectAddParam = CommentSubjectMapStruct.INSTANCE.addForm1ToAddParam(addForm);
        commentSubjectAddParam.setOwnerUserId(loginUser.getId());
        commentSubjectAddParam.setOwnerUserAvatar(loginUser.getAvatar());
        commentSubjectAddParam.setOwnerUserNickname(loginUser.getNickname());

        CommentSubject commentSubject = iCommentSubjectService.addCommentSubject(commentSubjectAddParam);
        return getVo(commentSubject);

    }

}
