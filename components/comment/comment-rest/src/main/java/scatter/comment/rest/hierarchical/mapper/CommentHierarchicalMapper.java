package scatter.comment.rest.hierarchical.mapper;

import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 评论层级评论表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface CommentHierarchicalMapper extends IBaseMapper<CommentHierarchical> {

}
