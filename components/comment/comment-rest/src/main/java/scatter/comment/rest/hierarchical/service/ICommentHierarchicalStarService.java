package scatter.comment.rest.hierarchical.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarPageQueryForm1;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalCancelStarParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalStarAddStarParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 评论点赞表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface ICommentHierarchicalStarService extends IBaseService<CommentHierarchicalStar> {



	/**
	 * 添加一个点赞
	 * @param addStartParam
	 * @return
	 */
	public CommentHierarchicalStar addStar(CommentHierarchicalStarAddStarParam addStartParam);

	/**
	 * 取消点赞
	 * @param cancelStarParam
	 * @return
	 */
	public boolean cancelStar(CommentHierarchicalCancelStarParam cancelStarParam);


	/**
	 * 是否已点赞
	 * @param commentHierarchicalId
	 * @param userId
	 * @return
	 */
	default public boolean hasStared(String commentHierarchicalId,String userId){
		Assert.hasText(commentHierarchicalId,"commentHierarchicalId 不能为空");

		return hasStared(newArrayList(commentHierarchicalId),userId).get(commentHierarchicalId);
	}
	/**
	 * 是否已点赞
	 * @param commentHierarchicalIds
	 * @param userId
	 * @return
	 */
	default public Map<String,Boolean> hasStared(List<String> commentHierarchicalIds, String userId){
		Assert.notEmpty(commentHierarchicalIds,"commentHierarchicalIds 不能为空");
		if (isStrEmpty(userId)) {
			return commentHierarchicalIds.stream().collect(Collectors.toMap(Function.identity(), Boolean::new));
		}
		List<CommentHierarchicalStar> list = list(Wrappers.<CommentHierarchicalStar>lambdaQuery().in(CommentHierarchicalStar::getCommentHierarchicalId, commentHierarchicalIds).eq(CommentHierarchicalStar::getOwnerUserId, userId));
		Map<String,Boolean> result = new HashMap<>();
		for (String commentHierarchicalId : commentHierarchicalIds) {
			result.put(commentHierarchicalId,list.stream().filter(item->isEqual(commentHierarchicalId,item.getCommentHierarchicalId())).count() > 0);
		}
		return result;
	}

	/**
	 * 列出每一个主体评论最新的点赞数据
	 * @param commentSubjectIds
	 * @param count
	 * @return
	 */
	public Map<String, Page<CommentHierarchicalStar>> listLatestStarForPerCommentHierarchical(List<String> commentSubjectIds, Integer count);

	/**
	 * 获取 CommentHierarchicalId 对应的点赞数据
	 * @param pageQueryForm
	 * @return
	 */
	default public IPage<CommentHierarchicalStar> listPage1(CommentHierarchicalStarPageQueryForm1 pageQueryForm) {
		return page(convertPage(pageQueryForm), Wrappers.<CommentHierarchicalStar>lambdaQuery()
				.eq(CommentHierarchicalStar::getCommentHierarchicalId,pageQueryForm.getCommentHierarchicalId())
				.orderByDesc(CommentHierarchicalStar::getTimeAt).orderByDesc(CommentHierarchicalStar::getId));
	}
}
