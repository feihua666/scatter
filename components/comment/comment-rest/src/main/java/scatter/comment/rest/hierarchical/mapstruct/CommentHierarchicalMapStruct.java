package scatter.comment.rest.hierarchical.mapstruct;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.hierarchical.form.*;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalAvailableVo;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalVo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 评论层级评论 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentHierarchicalMapStruct extends IBaseVoMapStruct<CommentHierarchical, CommentHierarchicalVo>,
                                  IBaseAddFormMapStruct<CommentHierarchical,CommentHierarchicalAddForm>,
                                  IBaseUpdateFormMapStruct<CommentHierarchical,CommentHierarchicalUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentHierarchical,CommentHierarchicalPageQueryForm>{
    CommentHierarchicalMapStruct INSTANCE = Mappers.getMapper( CommentHierarchicalMapStruct.class );

    CommentHierarchicalForSubjectAddParam addFormForSubjectToAddParam(CommentHierarchicalForSubjectAddForm addForm);

    CommentHierarchicalForHierarchicalAddParam addFormForHierarchicalToAddParam(CommentHierarchicalForHierarchicalAddForm addForm);

    CommentHierarchical addParamForSubjectToPo(CommentHierarchicalForSubjectAddParam addParam);

    CommentHierarchical addParamForHierarchicalToPo(CommentHierarchicalForHierarchicalAddParam addParam);

    /**
     * 单表po转vo
     * @param po
     * @return
     */
    CommentHierarchicalAvailableVo poToCommentHierarchicalAvailableVo(CommentHierarchical po);

    default List<CommentHierarchicalAvailableVo> posToCommentHierarchicalAvailableVos(List<CommentHierarchical> pos) {
        return pos.stream().map(po->(poToCommentHierarchicalAvailableVo(po))).collect(Collectors.toList());
    }

    default IPage<CommentHierarchicalAvailableVo> pagePoCommentHierarchicalAvailableVo(IPage<CommentHierarchical> page){
        IPage pageR = page;
        List<CommentHierarchical> records = pageR.getRecords();
        if (pageR != null && !CollectionUtil.isEmpty(records)) {
            pageR.setRecords(posToCommentHierarchicalAvailableVos(records));
            return pageR;
        }
        // 原样返回page
        return pageR;
    }
    
}
