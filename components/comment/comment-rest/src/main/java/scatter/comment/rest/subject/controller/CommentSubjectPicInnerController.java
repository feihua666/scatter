package scatter.comment.rest.subject.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.comment.rest.subject.service.ICommentSubjectPicService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 主体评论图片表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/inner/comment-subject-pic")
public class CommentSubjectPicInnerController extends BaseInnerController<CommentSubjectPic> {
 @Autowired
 private ICommentSubjectPicService commentSubjectPicService;

 public ICommentSubjectPicService getService(){
     return commentSubjectPicService;
 }
}
