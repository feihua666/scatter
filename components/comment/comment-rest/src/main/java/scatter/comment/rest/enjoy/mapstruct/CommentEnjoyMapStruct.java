package scatter.comment.rest.enjoy.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.enjoy.param.CommentEnjoyAddEnjoyParam;
import scatter.comment.pojo.subject.param.CommentSubjectStarAddStarParam;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.pojo.enjoy.vo.CommentEnjoyVo;
import scatter.comment.pojo.enjoy.form.CommentEnjoyAddForm;
import scatter.comment.pojo.enjoy.form.CommentEnjoyUpdateForm;
import scatter.comment.pojo.enjoy.form.CommentEnjoyPageQueryForm;

/**
 * <p>
 * 喜欢|想去|想看 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentEnjoyMapStruct extends IBaseVoMapStruct<CommentEnjoy, CommentEnjoyVo>,
                                  IBaseAddFormMapStruct<CommentEnjoy,CommentEnjoyAddForm>,
                                  IBaseUpdateFormMapStruct<CommentEnjoy,CommentEnjoyUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentEnjoy,CommentEnjoyPageQueryForm>{
    CommentEnjoyMapStruct INSTANCE = Mappers.getMapper( CommentEnjoyMapStruct.class );

    CommentEnjoy addEnjoytParamToPo(CommentEnjoyAddEnjoyParam addStarParam);

}
