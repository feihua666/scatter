package scatter.comment.rest.rate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.rest.rate.service.ICommentRateService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 评分翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Component
public class CommentRateTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentRateService commentRateService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentRate.TRANS_COMMENTRATE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentRate.TRANS_COMMENTRATE_BY_ID)) {
            CommentRate byId = commentRateService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentRate.TRANS_COMMENTRATE_BY_ID)) {
            return commentRateService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
