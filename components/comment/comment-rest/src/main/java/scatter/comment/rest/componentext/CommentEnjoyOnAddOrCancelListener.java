package scatter.comment.rest.componentext;

import scatter.comment.pojo.enjoy.param.CommentEnjoyAddEnjoyParam;
import scatter.comment.pojo.enjoy.param.CommentEnjoyCancelEnjoyParam;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;

/**
 * <p>
 * 扩展添加喜欢监听
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 13:15
 */
public interface CommentEnjoyOnAddOrCancelListener {
	/**
	 * 添加喜欢后调用
	 * 本监听只针对于主体
	 * @param addParam 属性 subjectId 一定有值
	 * @param commentEnjoy
	 */
	void onCommentEnjoyAdd(CommentEnjoyAddEnjoyParam addParam, CommentEnjoy commentEnjoy);

	/**
	 * 取消喜欢后调用
	 * @param addParam
	 */
	void onCommentEnjoyCancel(CommentEnjoyCancelEnjoyParam addParam);

}
