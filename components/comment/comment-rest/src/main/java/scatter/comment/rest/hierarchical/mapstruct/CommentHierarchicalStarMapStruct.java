package scatter.comment.rest.hierarchical.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalStarAddStarParam;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalStarVo;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarPageQueryForm;

/**
 * <p>
 * 评论点赞 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentHierarchicalStarMapStruct extends IBaseVoMapStruct<CommentHierarchicalStar, CommentHierarchicalStarVo>,
                                  IBaseAddFormMapStruct<CommentHierarchicalStar,CommentHierarchicalStarAddForm>,
                                  IBaseUpdateFormMapStruct<CommentHierarchicalStar,CommentHierarchicalStarUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentHierarchicalStar,CommentHierarchicalStarPageQueryForm>{
    CommentHierarchicalStarMapStruct INSTANCE = Mappers.getMapper( CommentHierarchicalStarMapStruct.class );

    CommentHierarchicalStar addStartParamToPo(CommentHierarchicalStarAddStarParam addStarParam);
}
