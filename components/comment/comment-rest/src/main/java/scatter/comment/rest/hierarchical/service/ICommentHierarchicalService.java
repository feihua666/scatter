package scatter.comment.rest.hierarchical.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalAvailableForHierarchicalPageQueryForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalAvailableForSubjectPageQueryForm;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalAvailableVo;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalVo;
import scatter.common.LoginUser;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论层级评论表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface ICommentHierarchicalService extends IBaseService<CommentHierarchical> {


	/**
	 * 列出每一个 subject 最新的层级评论数据
	 * 适用于直接盖楼的场景
	 * @param subjectIds
	 * @param count
	 * @param groupFlag 分组标识 选填
	 * @return
	 */
	public Map<String, Page<CommentHierarchical>> listLatestCommentHierarchicalForPerSubject(List<String> subjectIds, Integer count, String groupFlag);

	/**
	 * 详情
	 * @param id
	 * @param groupFlag
	 * @param loginUserId
	 * @return
	 * @throws InterruptedException
	 */
	public CommentHierarchicalAvailableVo detailById( String id, String groupFlag, String loginUserId) throws InterruptedException;
	/**
	 * 列出每一个评论层级的下一级评论层级
	 * @param commentHierarchicalIds
	 * @param count
	 * @param groupFlag 分组标识 选填
	 * @return
	 */
	public Map<String, Page<CommentHierarchical>> listLatestCommentHierarchicalForPerCommentHierarchical(List<String> commentHierarchicalIds, Integer count, String groupFlag);


	/**
	 * 分页查询可用层级评论 for subject
	 * 只适用于subject评论
	 * @param pageQueryForm
	 * @return
	 */
	public IPage<CommentHierarchicalAvailableVo> listAvailableForSubjectPage(CommentHierarchicalAvailableForSubjectPageQueryForm pageQueryForm,String loginUserId) throws InterruptedException;
	/**
	 * 分页查询可用层级评论 for CommentHierarchical
	 * 只适用于 CommentHierarchical 评论
	 * @param pageQueryForm
	 * @return
	 */
	public IPage<CommentHierarchicalAvailableVo> listAvailableForHierarchicalPage(CommentHierarchicalAvailableForHierarchicalPageQueryForm pageQueryForm,String loginUserId) throws InterruptedException;
	/**
	 * 添加主体层级评论
	 * @param addParam
	 * @return
	 */
	public CommentHierarchical addCommentHierarchicalForSubject(CommentHierarchicalForSubjectAddParam addParam);

	/**
	 * 添加主体层级评论 盖楼
	 * @param addParam
	 * @return
	 */
	public CommentHierarchical addCommentHierarchicalForHierarchical(CommentHierarchicalForHierarchicalAddParam addParam);

}
