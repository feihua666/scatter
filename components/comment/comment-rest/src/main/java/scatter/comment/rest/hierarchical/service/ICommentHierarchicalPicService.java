package scatter.comment.rest.hierarchical.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalPic;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.common.rest.service.IBaseService;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicPageQueryForm;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * 评论层级评论图片表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface ICommentHierarchicalPicService extends IBaseService<CommentHierarchicalPic> {

	/**
	 * 添加图片
	 * @param picUrls
	 * @param commentHierarchicalId
	 * @return
	 */
	default boolean addPics(List<String> picUrls,String commentHierarchicalId){
		if (isEmpty(picUrls)) {
			return false;
		}
		List<CommentHierarchicalPic> toBeInsertedList = new ArrayList<>(picUrls.size());
		for (int i = 0; i < picUrls.size(); i++) {
			toBeInsertedList.add(new CommentHierarchicalPic().setCommentHierarchicalId(commentHierarchicalId).setPicUrl(picUrls.get(i)).setSeq(i *10));
		}
		return saveBatch(toBeInsertedList);
	}
}
