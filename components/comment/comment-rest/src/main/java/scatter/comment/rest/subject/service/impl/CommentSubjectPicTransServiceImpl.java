package scatter.comment.rest.subject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.comment.rest.subject.service.ICommentSubjectPicService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 主体评论图片翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
public class CommentSubjectPicTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentSubjectPicService commentSubjectPicService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentSubjectPic.TRANS_COMMENTSUBJECTPIC_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentSubjectPic.TRANS_COMMENTSUBJECTPIC_BY_ID)) {
            CommentSubjectPic byId = commentSubjectPicService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentSubjectPic.TRANS_COMMENTSUBJECTPIC_BY_ID)) {
            return commentSubjectPicService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
