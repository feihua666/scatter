package scatter.comment.rest.hierarchical.mapper;

import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 评论点赞表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface CommentHierarchicalStarMapper extends IBaseMapper<CommentHierarchicalStar> {

}
