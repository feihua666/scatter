package scatter.comment.rest.star.mapper;

import scatter.comment.pojo.star.po.CommentStar;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 主体点赞表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
public interface CommentStarMapper extends IBaseMapper<CommentStar> {

}
