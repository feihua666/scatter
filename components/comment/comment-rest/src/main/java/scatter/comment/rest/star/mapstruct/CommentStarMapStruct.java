package scatter.comment.rest.star.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.star.param.CommentStarAddStarParam;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.pojo.star.vo.CommentStarVo;
import scatter.comment.pojo.star.form.CommentStarAddForm;
import scatter.comment.pojo.star.form.CommentStarUpdateForm;
import scatter.comment.pojo.star.form.CommentStarPageQueryForm;

/**
 * <p>
 * 主体点赞 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentStarMapStruct extends IBaseVoMapStruct<CommentStar, CommentStarVo>,
                                  IBaseAddFormMapStruct<CommentStar,CommentStarAddForm>,
                                  IBaseUpdateFormMapStruct<CommentStar,CommentStarUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentStar,CommentStarPageQueryForm>{
    CommentStarMapStruct INSTANCE = Mappers.getMapper( CommentStarMapStruct.class );
    CommentStar addStartParamToPo(CommentStarAddStarParam addStarParam);

}
