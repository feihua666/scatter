package scatter.comment.rest.componentext;

import scatter.comment.pojo.rate.param.CommentRateAddRateParam;
import scatter.comment.pojo.rate.param.CommentRateCancelRateParam;
import scatter.comment.pojo.rate.po.CommentRate;

/**
 * <p>
 * 扩展添加评分监听
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 13:15
 */
public interface CommentRateOnAddOrCancelListener {
	/**
	 * 添加评分后调用
	 * 本监听只针对于主体
	 * @param addParam 属性 subjectId 一定有值
	 * @param commentRate
	 */
	void onCommentRateAdd(CommentRateAddRateParam addParam, CommentRate commentRate);

	/**
	 * 取消评分后调用
	 * @param addParam
	 * @param commentRate 删除的数据
	 */
	void onCommentRateCancel(CommentRateCancelRateParam addParam, CommentRate commentRate);

}
