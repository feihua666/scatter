package scatter.comment.rest.enjoy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.enjoy.form.*;
import scatter.comment.pojo.enjoy.param.CommentEnjoyAddEnjoyParam;
import scatter.comment.pojo.enjoy.param.CommentEnjoyCancelEnjoyParam;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.pojo.enjoy.vo.CommentEnjoyVo;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.rest.enjoy.service.ICommentEnjoyService;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 喜欢|想去|想看表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Api(tags = "喜欢|想去|想看相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/enjoy/comment-enjoy")
public class CommentEnjoyController extends BaseAddUpdateQueryFormController<CommentEnjoy, CommentEnjoyVo, CommentEnjoyAddForm, CommentEnjoyUpdateForm, CommentEnjoyPageQueryForm> {
    @Autowired
    private ICommentEnjoyService iCommentEnjoyService;

    @ApiOperation("添加喜欢|想去|想看")
    @PreAuthorize("hasAuthority('CommentEnjoy:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CommentEnjoyVo add(@RequestBody @Valid CommentEnjoyAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询喜欢|想去|想看")
    @PreAuthorize("hasAuthority('CommentEnjoy:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CommentEnjoyVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除喜欢|想去|想看")
    @PreAuthorize("hasAuthority('CommentEnjoy:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新喜欢|想去|想看")
    @PreAuthorize("hasAuthority('CommentEnjoy:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CommentEnjoyVo update(@RequestBody @Valid CommentEnjoyUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询喜欢|想去|想看")
    @PreAuthorize("hasAuthority('CommentEnjoy:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<CommentEnjoyVo> getList(CommentEnjoyPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询喜欢|想去|想看")
    @PreAuthorize("hasAuthority('CommentEnjoy:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<CommentEnjoyVo> getPage(CommentEnjoyPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }



    @ApiOperation("分页查询主体评论喜欢")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentEnjoyVo> getPage1(@Valid CommentEnjoyPageQueryForm1 listPageForm) {
        IPage<CommentEnjoy> commentSubjectEnjoyIPage = iCommentEnjoyService.listPage1(listPageForm);
        return super.pagePoToVo(commentSubjectEnjoyIPage);
    }

    @ApiOperation("添加主体评论一个喜欢")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addEnjoy")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentEnjoyVo addEnjoy(@RequestBody @Valid CommentEnjoyAddForm1 addForm1, @ApiIgnore LoginUser loginUser) {
        CommentEnjoyAddEnjoyParam addEnjoytParam = new CommentEnjoyAddEnjoyParam();
        addEnjoytParam.setSubjectId(addForm1.getSubjectId());
        addEnjoytParam.setOwnerUserId(loginUser.getId());
        addEnjoytParam.setOwnerUserAvatar(loginUser.getAvatar());
        addEnjoytParam.setOwnerUserNickname(loginUser.getNickname());
        addEnjoytParam.setGroupFlag(addForm1.getGroupFlag());
        return getVo(iCommentEnjoyService.addEnjoy(addEnjoytParam));
    }

    @ApiOperation("取消主体评论一个喜欢")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/cancelEnjoy")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean cancelEnjoy(@RequestBody @Valid CommentEnjoyCancelForm cancelForm, @ApiIgnore LoginUser loginUser) {
        CommentEnjoyCancelEnjoyParam cancelEnjoyParam = new CommentEnjoyCancelEnjoyParam();
        cancelEnjoyParam.setSubjectId(cancelForm.getSubjectId());
        cancelEnjoyParam.setOwnerUserId(loginUser.getId());
        cancelEnjoyParam.setGroupFlag(cancelForm.getGroupFlag());
        return iCommentEnjoyService.cancelEnjoy(cancelEnjoyParam);
    }
}
