package scatter.comment.rest.subject.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.comment.pojo.subject.form.CommentSubjectStarPageQueryForm1;
import scatter.comment.pojo.subject.param.CommentSubjectStarCancelStarParam;
import scatter.comment.pojo.subject.param.CommentSubjectStarAddStarParam;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 主体评论点赞表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface ICommentSubjectStarService extends IBaseService<CommentSubjectStar> {

	/**
	 * 添加一个点赞
	 * @param addStartParam
	 * @return
	 */
	public CommentSubjectStar addStar(CommentSubjectStarAddStarParam addStartParam);

	/**
	 * 取消点赞
	 * @param cancelStarParam
	 * @return
	 */
	public boolean cancelStar(CommentSubjectStarCancelStarParam cancelStarParam);


	/**
	 * 是否已点赞
	 * @param commentSubjectId
	 * @param userId
	 * @return
	 */
	default public boolean hasStared(String commentSubjectId,String userId){
		Assert.hasText(commentSubjectId,"commentSubjectId 不能为空");

		return hasStared(newArrayList(commentSubjectId),userId).get(commentSubjectId);
	}
	/**
	 * 是否已点赞
	 * @param commentSubjectIds
	 * @param userId
	 * @return
	 */
	default public Map<String,Boolean> hasStared(List<String> commentSubjectIds, String userId){
		Assert.notEmpty(commentSubjectIds,"commentSubjectIds 不能为空");
		if (isStrEmpty(userId)) {
			return commentSubjectIds.stream().collect(Collectors.toMap(Function.identity(), Boolean::new));
		}
		List<CommentSubjectStar> list = list(Wrappers.<CommentSubjectStar>lambdaQuery().in(CommentSubjectStar::getCommentSubjectId, commentSubjectIds).eq(CommentSubjectStar::getOwnerUserId, userId));
		Map<String,Boolean> result = new HashMap<>();
		for (String commentSubjectId : commentSubjectIds) {
			result.put(commentSubjectId,list.stream().filter(item->isEqual(commentSubjectId,item.getCommentSubjectId())).count() > 0);
		}
		return result;
	}

	/**
	 * 列出每一个主体评论最新的点赞数据
	 * @param commentSubjectIds
	 * @param count
	 * @return
	 */
	public Map<String, Page<CommentSubjectStar>> listLatestStarForPerCommentSubject(List<String> commentSubjectIds, Integer count);

	/**
	 * 获取commentSubjectId对应的点赞数据
	 * @param pageQueryForm
	 * @return
	 */
	default public IPage<CommentSubjectStar> listPage1(CommentSubjectStarPageQueryForm1 pageQueryForm) {
		return page(convertPage(pageQueryForm), Wrappers.<CommentSubjectStar>lambdaQuery()
				.eq(CommentSubjectStar::getCommentSubjectId,pageQueryForm.getCommentSubjectId())
				.orderByDesc(CommentSubjectStar::getTimeAt).orderByDesc(CommentSubjectStar::getId));
	}
}
