package scatter.comment.rest.componentext;

import scatter.comment.pojo.subject.vo.CommentSubjectAvailableVo;
import scatter.comment.pojo.subject.vo.CommentSubjectVo;

import java.util.List;

/**
 * <p>
 * 获取可用动态添加额外数据处理器
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 16:07
 */
public interface CommentSubjectAvailableExtProvider {
	/**
	 * 提供额外补充
	 * @param momentVoList
	 */
	public void provideCommentSubject(List<CommentSubjectAvailableVo> momentVoList);
}
