package scatter.comment.rest.subject.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.common.rest.service.IBaseService;
import scatter.comment.pojo.subject.form.CommentSubjectPicAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicPageQueryForm;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * 主体评论图片表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface ICommentSubjectPicService extends IBaseService<CommentSubjectPic> {

	/**
	 * 添加图片
	 * @param picUrls
	 * @param CommentSubjectId
	 * @return
	 */
	default boolean addPics(List<String> picUrls,String CommentSubjectId){
		if (isEmpty(picUrls)) {
			return false;
		}
		List<CommentSubjectPic> toBeInsertedList = new ArrayList<>(picUrls.size());
		for (int i = 0; i < picUrls.size(); i++) {
			toBeInsertedList.add(new CommentSubjectPic().setCommentSubjectId(CommentSubjectId).setPicUrl(picUrls.get(i)).setSeq(i *10));
		}
		return saveBatch(toBeInsertedList);
	}
}
