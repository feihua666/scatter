package scatter.comment.rest.rate.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.rate.param.CommentRateAddRateParam;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.rate.vo.CommentRateVo;
import scatter.comment.pojo.rate.form.CommentRateAddForm;
import scatter.comment.pojo.rate.form.CommentRateUpdateForm;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm;

/**
 * <p>
 * 评分 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentRateMapStruct extends IBaseVoMapStruct<CommentRate, CommentRateVo>,
                                  IBaseAddFormMapStruct<CommentRate,CommentRateAddForm>,
                                  IBaseUpdateFormMapStruct<CommentRate,CommentRateUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentRate,CommentRatePageQueryForm>{
    CommentRateMapStruct INSTANCE = Mappers.getMapper( CommentRateMapStruct.class );

    CommentRate addRatetParamToPo(CommentRateAddRateParam addRateParam);
}
