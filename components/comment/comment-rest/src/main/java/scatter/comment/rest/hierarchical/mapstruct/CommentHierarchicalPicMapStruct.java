package scatter.comment.rest.hierarchical.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalPic;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalPicVo;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicPageQueryForm;

/**
 * <p>
 * 评论层级评论图片 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentHierarchicalPicMapStruct extends IBaseVoMapStruct<CommentHierarchicalPic, CommentHierarchicalPicVo>,
                                  IBaseAddFormMapStruct<CommentHierarchicalPic,CommentHierarchicalPicAddForm>,
                                  IBaseUpdateFormMapStruct<CommentHierarchicalPic,CommentHierarchicalPicUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentHierarchicalPic,CommentHierarchicalPicPageQueryForm>{
    CommentHierarchicalPicMapStruct INSTANCE = Mappers.getMapper( CommentHierarchicalPicMapStruct.class );

}
