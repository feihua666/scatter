package scatter.comment.rest.star.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.comment.pojo.star.form.CommentStarPageQueryForm1;
import scatter.comment.pojo.star.param.CommentStarAddStarParam;
import scatter.comment.pojo.star.param.CommentStarCancelStarParam;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 主体点赞表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
public interface ICommentStarService extends IBaseService<CommentStar> {



	/**
	 * 添加一个点赞
	 * @param addStarParam
	 * @return
	 */
	public CommentStar addStar(CommentStarAddStarParam addStarParam);

	/**
	 * 取消点赞
	 * @param cancelStarParam
	 * @return
	 */
	public boolean cancelStar(CommentStarCancelStarParam cancelStarParam);


	/**
	 * 是否已点赞
	 * @param subjectId
	 * @param userId
	 * @return
	 */
	default public boolean hasStared(String subjectId,String userId,String groupFlag){
		Assert.hasText(subjectId,"subjectId 不能为空");

		return hasStared(newArrayList(subjectId),userId,groupFlag).get(subjectId);
	}
	/**
	 * 是否已点赞
	 * @param subjectIds
	 * @param userId
	 * @return
	 */
	default public Map<String,Boolean> hasStared(List<String> subjectIds, String userId, String groupFlag){
		Assert.notEmpty(subjectIds,"subjectIds 不能为空");
		if (isStrEmpty(userId)) {
			return subjectIds.stream().collect(Collectors.toMap(Function.identity(), Boolean::new));
		}

		List<CommentStar> list = list(Wrappers.<CommentStar>lambdaQuery().in(CommentStar::getSubjectId, subjectIds)
				.eq(CommentStar::getOwnerUserId, userId)
				.eq(!isStrEmpty(groupFlag),CommentStar::getGroupFlag,groupFlag)

		);
		Map<String,Boolean> result = new HashMap<>();
		for (String subjectId : subjectIds) {
			result.put(subjectId,list.stream().filter(item->isEqual(subjectId,item.getSubjectId())).count() > 0);
		}
		return result;
	}

	/**
	 * 列出一个主体评论最新的点赞数据
	 * @param subjectId
	 * @param count
	 * @param groupFlag
	 * @return
	 */
	default public Page<CommentStar> listLatestStarForSubject(String subjectId, Integer count, String groupFlag){
		return listLatestStarForPerSubject(newArrayList(subjectId),count,groupFlag).get(subjectId);
	}
	/**
	 * 列出每一个主体评论最新的点赞数据
	 * @param subjectIds
	 * @param count
	 * @return
	 */
	public Map<String, Page<CommentStar>> listLatestStarForPerSubject(List<String> subjectIds, Integer count, String groupFlag);

	/**
	 * 获取subjectId对应的点赞数据
	 * @param pageQueryForm
	 * @return
	 */
	default public IPage<CommentStar> listPage1(CommentStarPageQueryForm1 pageQueryForm) {
		return page(convertPage(pageQueryForm), Wrappers.<CommentStar>lambdaQuery()
				.eq(CommentStar::getSubjectId,pageQueryForm.getSubjectId())
				.eq(!isStrEmpty(pageQueryForm.getGroupFlag()),CommentStar::getGroupFlag,pageQueryForm.getGroupFlag())
				.orderByDesc(CommentStar::getTimeAt).orderByDesc(CommentStar::getId));
	}
}
