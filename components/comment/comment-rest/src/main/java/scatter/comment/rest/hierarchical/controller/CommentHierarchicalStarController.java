package scatter.comment.rest.hierarchical.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.hierarchical.form.*;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalCancelStarParam;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalStarAddStarParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalStarVo;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalStarService;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 评论点赞表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "评论点赞相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/hierarchical/comment-hierarchical-star")
public class CommentHierarchicalStarController extends BaseAddUpdateQueryFormController<CommentHierarchicalStar, CommentHierarchicalStarVo, CommentHierarchicalStarAddForm, CommentHierarchicalStarUpdateForm, CommentHierarchicalStarPageQueryForm> {


    @Autowired
    private ICommentHierarchicalStarService iCommentHierarchicalStarService;


     @Override
	 @ApiOperation("添加评论点赞")
     @PreAuthorize("hasAuthority('CommentHierarchicalStar:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentHierarchicalStarVo add(@RequestBody @Valid CommentHierarchicalStarAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询评论点赞")
     @PreAuthorize("hasAuthority('CommentHierarchicalStar:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public CommentHierarchicalStarVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除评论点赞")
     @PreAuthorize("hasAuthority('CommentHierarchicalStar:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新评论点赞")
     @PreAuthorize("hasAuthority('CommentHierarchicalStar:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentHierarchicalStarVo update(@RequestBody @Valid CommentHierarchicalStarUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询评论点赞")
    @PreAuthorize("hasAuthority('CommentHierarchicalStar:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentHierarchicalStarVo> getList(CommentHierarchicalStarPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询评论点赞")
    @PreAuthorize("hasAuthority('CommentHierarchicalStar:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentHierarchicalStarVo> getPage(CommentHierarchicalStarPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("分页查询可展示评论点赞")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentHierarchicalStarVo> getPage1(CommentHierarchicalStarPageQueryForm1 listPageForm) {
        IPage<CommentHierarchicalStar> commentHierarchicalStarIPage = iCommentHierarchicalStarService.listPage1(listPageForm);
        return super.pagePoToVo(commentHierarchicalStarIPage);
    }


    @ApiOperation("添加层级评论一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addStar")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentHierarchicalStarVo addStar(@RequestBody @Valid CommentHierarchicalStarAddForm1 addForm1, @ApiIgnore LoginUser loginUser) {
        CommentHierarchicalStarAddStarParam addStartParam = new CommentHierarchicalStarAddStarParam();
        addStartParam.setCommentHierarchicalId(addForm1.getCommentHierarchicalId());
        addStartParam.setOwnerUserId(loginUser.getId());
        addStartParam.setOwnerUserAvatar(loginUser.getAvatar());
        addStartParam.setOwnerUserNickname(loginUser.getNickname());
        return getVo(iCommentHierarchicalStarService.addStar(addStartParam));
    }
    @ApiOperation("取消层级评论一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/cancelStar")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean addStar(@RequestBody @Valid CommentHierarchicalStarCancelForm cancelForm, @ApiIgnore LoginUser loginUser) {
        CommentHierarchicalCancelStarParam cancelStarParam = new CommentHierarchicalCancelStarParam();
        cancelStarParam.setCommentHierarchicalId(cancelForm.getCommentHierarchicalId());
        cancelStarParam.setOwnerUserId(loginUser.getId());
        return iCommentHierarchicalStarService.cancelStar(cancelStarParam);
    }
}
