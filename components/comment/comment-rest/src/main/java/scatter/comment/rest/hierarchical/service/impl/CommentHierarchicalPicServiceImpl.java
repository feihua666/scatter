package scatter.comment.rest.hierarchical.service.impl;

import scatter.comment.pojo.hierarchical.po.CommentHierarchicalPic;
import scatter.comment.rest.hierarchical.mapper.CommentHierarchicalPicMapper;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalPicService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPicPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 评论层级评论图片表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Service
@Transactional
public class CommentHierarchicalPicServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<CommentHierarchicalPicMapper, CommentHierarchicalPic, CommentHierarchicalPicAddForm, CommentHierarchicalPicUpdateForm, CommentHierarchicalPicPageQueryForm> implements ICommentHierarchicalPicService {
    @Override
    public void preAdd(CommentHierarchicalPicAddForm addForm,CommentHierarchicalPic po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(CommentHierarchicalPicUpdateForm updateForm,CommentHierarchicalPic po) {
        super.preUpdate(updateForm,po);

    }
}
