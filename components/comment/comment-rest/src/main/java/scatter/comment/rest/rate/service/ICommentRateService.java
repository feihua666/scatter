package scatter.comment.rest.rate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm1;
import scatter.comment.pojo.rate.param.CommentRateAddRateParam;
import scatter.comment.pojo.rate.param.CommentRateCancelRateParam;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;
import scatter.comment.pojo.rate.form.CommentRateAddForm;
import scatter.comment.pojo.rate.form.CommentRateUpdateForm;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 评分表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
public interface ICommentRateService extends IBaseService<CommentRate> {


	/**
	 * 添加一个评分
	 * @param addRateParam
	 * @return
	 */
	public CommentRate addRate(CommentRateAddRateParam addRateParam);

	/**
	 * 取消评分
	 * @param cancelRateParam
	 * @return
	 */
	public boolean cancelRate(CommentRateCancelRateParam cancelRateParam);


	/**
	 * 是否已评分
	 * @param subjectId
	 * @param userId
	 * @return
	 */
	default public boolean hasRated(String subjectId,String userId,String groupFlag){
		Assert.hasText(subjectId,"subjectId 不能为空");

		return hasRated(newArrayList(subjectId),userId,groupFlag).get(subjectId);
	}
	/**
	 * 是否已评分
	 * @param subjectIds
	 * @param userId
	 * @return
	 */
	default public Map<String,Boolean> hasRated(List<String> subjectIds, String userId,String groupFlag){
		Assert.notEmpty(subjectIds,"subjectIds 不能为空");
		if (isStrEmpty(userId)) {
			return subjectIds.stream().collect(Collectors.toMap(Function.identity(), Boolean::new));
		}
		List<CommentRate> list = list(Wrappers.<CommentRate>lambdaQuery().in(CommentRate::getSubjectId, subjectIds)
				.eq(CommentRate::getOwnerUserId, userId)
				.eq(!isStrEmpty(groupFlag),CommentRate::getGroupFlag,groupFlag)

		);
		Map<String,Boolean> result = new HashMap<>();
		for (String subjectId : subjectIds) {
			result.put(subjectId,list.stream().filter(item->isEqual(subjectId,item.getSubjectId())).count() > 0);
		}
		return result;
	}

	/**
	 * 列出一个主体评论最新的评分数据
	 * @param subjectId
	 * @param count
	 * @param groupFlag
	 * @return
	 */
	default public Page<CommentRate> listLatestRateForSubject(String subjectId, Integer count,String groupFlag){
		return listLatestRateForPerSubject(newArrayList(subjectId),count,groupFlag).get(subjectId);
	}
	/**
	 * 列出每一个主体评论最新的评分数据
	 * @param subjectIds
	 * @param count
	 * @return
	 */
	public Map<String, Page<CommentRate>> listLatestRateForPerSubject(List<String> subjectIds, Integer count,String groupFlag);

	/**
	 * 获取subjectId对应的评分数据
	 * @param pageQueryForm
	 * @return
	 */
	default public IPage<CommentRate> listPage1(CommentRatePageQueryForm1 pageQueryForm) {
		return page(convertPage(pageQueryForm), Wrappers.<CommentRate>lambdaQuery()
				.eq(CommentRate::getSubjectId,pageQueryForm.getSubjectId())
				.eq(!isStrEmpty(pageQueryForm.getGroupFlag()),CommentRate::getGroupFlag,pageQueryForm.getGroupFlag())
				.orderByDesc(CommentRate::getTimeAt).orderByDesc(CommentRate::getId));
	}
}
