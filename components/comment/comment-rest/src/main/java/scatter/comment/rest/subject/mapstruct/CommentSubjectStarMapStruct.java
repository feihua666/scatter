package scatter.comment.rest.subject.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.subject.param.CommentSubjectStarAddStarParam;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.comment.pojo.subject.vo.CommentSubjectStarVo;
import scatter.comment.pojo.subject.form.CommentSubjectStarAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectStarUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectStarPageQueryForm;

/**
 * <p>
 * 主体评论点赞 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentSubjectStarMapStruct extends IBaseVoMapStruct<CommentSubjectStar, CommentSubjectStarVo>,
                                  IBaseAddFormMapStruct<CommentSubjectStar,CommentSubjectStarAddForm>,
                                  IBaseUpdateFormMapStruct<CommentSubjectStar,CommentSubjectStarUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentSubjectStar,CommentSubjectStarPageQueryForm>{
    CommentSubjectStarMapStruct INSTANCE = Mappers.getMapper( CommentSubjectStarMapStruct.class );

    CommentSubjectStar addStartParamToPo(CommentSubjectStarAddStarParam addStarParam);

}
