package scatter.comment.rest.subject.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.subject.form.*;
import scatter.comment.pojo.subject.param.CommentSubjectStarCancelStarParam;
import scatter.comment.pojo.subject.param.CommentSubjectStarAddStarParam;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.comment.pojo.subject.vo.CommentSubjectStarVo;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.rest.subject.service.ICommentSubjectStarService;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 主体评论点赞表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "主体评论点赞相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/comment-subject-star")
public class CommentSubjectStarController extends BaseAddUpdateQueryFormController<CommentSubjectStar, CommentSubjectStarVo, CommentSubjectStarAddForm, CommentSubjectStarUpdateForm, CommentSubjectStarPageQueryForm> {


    @Autowired
    private ICommentSubjectStarService iCommentSubjectStarService;

     @Override
	 @ApiOperation("添加主体评论点赞")
     @PreAuthorize("hasAuthority('CommentSubjectStar:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentSubjectStarVo add(@RequestBody @Valid CommentSubjectStarAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询主体评论点赞")
     @PreAuthorize("hasAuthority('CommentSubjectStar:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public CommentSubjectStarVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除主体评论点赞")
     @PreAuthorize("hasAuthority('CommentSubjectStar:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新主体评论点赞")
     @PreAuthorize("hasAuthority('CommentSubjectStar:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentSubjectStarVo update(@RequestBody @Valid CommentSubjectStarUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询主体评论点赞")
    @PreAuthorize("hasAuthority('CommentSubjectStar:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentSubjectStarVo> getList(CommentSubjectStarPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询主体评论点赞")
    @PreAuthorize("hasAuthority('CommentSubjectStar:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentSubjectStarVo> getPage(CommentSubjectStarPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("分页查询主体评论点赞")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentSubjectStarVo> getPage1(@Valid CommentSubjectStarPageQueryForm1 listPageForm) {
        IPage<CommentSubjectStar> commentSubjectStarIPage = iCommentSubjectStarService.listPage1(listPageForm);
        return super.pagePoToVo(commentSubjectStarIPage);
    }

    @ApiOperation("添加主体评论一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addStar")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentSubjectStarVo addStar(@RequestBody @Valid CommentSubjectStarAddForm1 addForm1, @ApiIgnore LoginUser loginUser) {
        CommentSubjectStarAddStarParam addStartParam = new CommentSubjectStarAddStarParam();
        addStartParam.setCommentSubjectId(addForm1.getCommentSubjectId());
        addStartParam.setOwnerUserId(loginUser.getId());
        addStartParam.setOwnerUserAvatar(loginUser.getAvatar());
        addStartParam.setOwnerUserNickname(loginUser.getNickname());
        return getVo(iCommentSubjectStarService.addStar(addStartParam));
    }
    @ApiOperation("取消主体评论一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/cancelStar")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean cancelStar(@RequestBody @Valid CommentSubjectStarCancelForm cancelForm, @ApiIgnore LoginUser loginUser) {
        CommentSubjectStarCancelStarParam cancelStarParam = new CommentSubjectStarCancelStarParam();
        cancelStarParam.setCommentSubjectId(cancelForm.getCommentSubjectId());
        cancelStarParam.setOwnerUserId(loginUser.getId());
        return iCommentSubjectStarService.cancelStar(cancelStarParam);
    }
}
