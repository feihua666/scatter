package scatter.comment.rest.hierarchical.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalStarService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 评论点赞翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
public class CommentHierarchicalStarTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentHierarchicalStarService commentHierarchicalStarService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentHierarchicalStar.TRANS_COMMENTHIERARCHICALSTAR_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentHierarchicalStar.TRANS_COMMENTHIERARCHICALSTAR_BY_ID)) {
            CommentHierarchicalStar byId = commentHierarchicalStarService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentHierarchicalStar.TRANS_COMMENTHIERARCHICALSTAR_BY_ID)) {
            return commentHierarchicalStarService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
