package scatter.comment.rest.enjoy.mapper;

import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 喜欢|想去|想看表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
public interface CommentEnjoyMapper extends IBaseMapper<CommentEnjoy> {

}
