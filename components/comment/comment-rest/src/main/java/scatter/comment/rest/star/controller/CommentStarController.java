package scatter.comment.rest.star.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.star.form.CommentStarAddForm1;
import scatter.comment.pojo.star.form.CommentStarCancelForm;
import scatter.comment.pojo.star.form.CommentStarPageQueryForm1;
import scatter.comment.pojo.star.param.CommentStarAddStarParam;
import scatter.comment.pojo.star.param.CommentStarCancelStarParam;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.pojo.star.vo.CommentStarVo;
import scatter.comment.rest.CommentConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.pojo.star.vo.CommentStarVo;
import scatter.comment.pojo.star.form.CommentStarAddForm;
import scatter.comment.pojo.star.form.CommentStarUpdateForm;
import scatter.comment.pojo.star.form.CommentStarPageQueryForm;
import scatter.comment.rest.star.service.ICommentStarService;
import org.springframework.beans.factory.annotation.Autowired;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 主体点赞表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
@Api(tags = "主体点赞相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/star/comment-star")
public class CommentStarController extends BaseAddUpdateQueryFormController<CommentStar, CommentStarVo, CommentStarAddForm, CommentStarUpdateForm, CommentStarPageQueryForm> {
    @Autowired
    private ICommentStarService iCommentStarService;

    @ApiOperation("添加主体点赞")
    @PreAuthorize("hasAuthority('CommentStar:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CommentStarVo add(@RequestBody @Valid CommentStarAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询主体点赞")
    @PreAuthorize("hasAuthority('CommentStar:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CommentStarVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除主体点赞")
    @PreAuthorize("hasAuthority('CommentStar:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新主体点赞")
    @PreAuthorize("hasAuthority('CommentStar:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CommentStarVo update(@RequestBody @Valid CommentStarUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询主体点赞")
    @PreAuthorize("hasAuthority('CommentStar:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<CommentStarVo> getList(CommentStarPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询主体点赞")
    @PreAuthorize("hasAuthority('CommentStar:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<CommentStarVo> getPage(CommentStarPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }



    @ApiOperation("分页查询主体点赞")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentStarVo> getPage1(@Valid CommentStarPageQueryForm1 listPageForm) {
        IPage<CommentStar> commentSubjectStarIPage = iCommentStarService.listPage1(listPageForm);
        return super.pagePoToVo(commentSubjectStarIPage);
    }

    @ApiOperation("添加主体一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addStar")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentStarVo addStar(@RequestBody @Valid CommentStarAddForm1 addForm1, @ApiIgnore LoginUser loginUser) {
        CommentStarAddStarParam addStartParam = new CommentStarAddStarParam();
        addStartParam.setSubjectId(addForm1.getSubjectId());
        addStartParam.setOwnerUserId(loginUser.getId());
        addStartParam.setOwnerUserAvatar(loginUser.getAvatar());
        addStartParam.setOwnerUserNickname(loginUser.getNickname());
        addStartParam.setGroupFlag(addForm1.getGroupFlag());
        return getVo(iCommentStarService.addStar(addStartParam));
    }

    @ApiOperation("取消主体一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/cancelStar")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean cancelStar(@RequestBody @Valid CommentStarCancelForm cancelForm, @ApiIgnore LoginUser loginUser) {
        CommentStarCancelStarParam cancelStarParam = new CommentStarCancelStarParam();
        cancelStarParam.setSubjectId(cancelForm.getSubjectId());
        cancelStarParam.setOwnerUserId(loginUser.getId());
        cancelStarParam.setGroupFlag(cancelForm.getGroupFlag());
        return iCommentStarService.cancelStar(cancelStarParam);
    }
}
