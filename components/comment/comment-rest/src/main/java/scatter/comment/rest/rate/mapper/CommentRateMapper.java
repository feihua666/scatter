package scatter.comment.rest.rate.mapper;

import scatter.comment.pojo.rate.po.CommentRate;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 评分表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
public interface CommentRateMapper extends IBaseMapper<CommentRate> {

}
