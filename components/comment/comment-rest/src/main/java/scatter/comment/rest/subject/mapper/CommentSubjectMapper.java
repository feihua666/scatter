package scatter.comment.rest.subject.mapper;

import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 主体评论表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface CommentSubjectMapper extends IBaseMapper<CommentSubject> {

}
