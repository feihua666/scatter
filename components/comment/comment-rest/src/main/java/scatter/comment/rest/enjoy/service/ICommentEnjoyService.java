package scatter.comment.rest.enjoy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.comment.pojo.enjoy.form.CommentEnjoyPageQueryForm1;
import scatter.comment.pojo.enjoy.param.CommentEnjoyAddEnjoyParam;
import scatter.comment.pojo.enjoy.param.CommentEnjoyCancelEnjoyParam;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 喜欢|想去|想看表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
public interface ICommentEnjoyService extends IBaseService<CommentEnjoy> {


	/**
	 * 添加一个喜欢
	 * @param addEnjoyParam
	 * @return
	 */
	public CommentEnjoy addEnjoy(CommentEnjoyAddEnjoyParam addEnjoyParam);

	/**
	 * 取消喜欢
	 * @param cancelEnjoyParam
	 * @return
	 */
	public boolean cancelEnjoy(CommentEnjoyCancelEnjoyParam cancelEnjoyParam);


	/**
	 * 是否已喜欢
	 * @param subjectId
	 * @param userId
	 * @return
	 */
	default public boolean hasEnjoyed(String subjectId,String userId,String groupFlag){
		Assert.hasText(subjectId,"subjectId 不能为空");

		return hasEnjoyed(newArrayList(subjectId),userId,groupFlag).get(subjectId);
	}
	/**
	 * 是否已喜欢
	 * @param subjectIds
	 * @param userId
	 * @return
	 */
	default public Map<String,Boolean> hasEnjoyed(List<String> subjectIds, String userId,String groupFlag){
		Assert.notEmpty(subjectIds,"subjectIds 不能为空");
		if (isStrEmpty(userId)) {
			return subjectIds.stream().collect(Collectors.toMap(Function.identity(), Boolean::new));
		}
		List<CommentEnjoy> list = list(Wrappers.<CommentEnjoy>lambdaQuery()
				.in(CommentEnjoy::getSubjectId, subjectIds)
				.eq(CommentEnjoy::getOwnerUserId, userId)
				.eq(!isStrEmpty(groupFlag), CommentEnjoy::getGroupFlag,groupFlag)
		);
		Map<String,Boolean> result = new HashMap<>();
		for (String subjectId : subjectIds) {
			result.put(subjectId,list.stream().filter(item->isEqual(subjectId,item.getSubjectId())).count() > 0);
		}
		return result;
	}

	/**
	 * 列出一个主体评论最新的喜欢数据
	 * @param subjectId
	 * @param count
	 * @param groupFlag
	 * @return
	 */
	default public Page<CommentEnjoy> listLatestEnjoyForSubject(String subjectId, Integer count,String groupFlag){
		return listLatestEnjoyForPerSubject(newArrayList(subjectId),count,groupFlag).get(subjectId);
	}
	/**
	 * 列出每一个主体评论最新的喜欢数据
	 * @param subjectIds
	 * @param count
	 * @return
	 */
	public Map<String, Page<CommentEnjoy>> listLatestEnjoyForPerSubject(List<String> subjectIds, Integer count,String groupFlag);

	/**
	 * 获取subjectId对应的喜欢数据
	 * @param pageQueryForm
	 * @return
	 */
	default public IPage<CommentEnjoy> listPage1(CommentEnjoyPageQueryForm1 pageQueryForm) {
		return page(convertPage(pageQueryForm), Wrappers.<CommentEnjoy>lambdaQuery()
				.eq(CommentEnjoy::getSubjectId,pageQueryForm.getSubjectId())
				.eq(!isStrEmpty(pageQueryForm.getGroupFlag()),CommentEnjoy::getGroupFlag,pageQueryForm.getGroupFlag())
				.orderByDesc(CommentEnjoy::getTimeAt).orderByDesc(CommentEnjoy::getId));
	}
}
