package scatter.comment.rest.hierarchical.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.hierarchical.form.*;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForHierarchicalAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalAvailableVo;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalVo;
import scatter.comment.pojo.hierarchical.param.CommentHierarchicalForSubjectAddParam;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.comment.rest.CommentConfiguration;
import scatter.comment.rest.componentext.CommentHierarchicalAvailableExtProvider;
import scatter.comment.rest.hierarchical.mapstruct.CommentHierarchicalMapStruct;
import scatter.comment.rest.hierarchical.mapstruct.CommentHierarchicalStarMapStruct;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalStarService;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
/**
 * <p>
 * 评论层级评论表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "评论层级评论相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/comment-hierarchical")
public class CommentHierarchicalController extends BaseAddUpdateQueryFormController<CommentHierarchical, CommentHierarchicalVo, CommentHierarchicalAddForm, CommentHierarchicalUpdateForm, CommentHierarchicalPageQueryForm> {

    @Autowired
    private ICommentHierarchicalService iCommentHierarchicalService;

    
     @Override
	 @ApiOperation("添加评论层级评论")
     @PreAuthorize("hasAuthority('CommentHierarchical:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentHierarchicalVo add(@RequestBody @Valid CommentHierarchicalAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询评论层级评论")
     @PreAuthorize("hasAuthority('CommentHierarchical:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public CommentHierarchicalVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除评论层级评论")
     @PreAuthorize("hasAuthority('CommentHierarchical:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新评论层级评论")
     @PreAuthorize("hasAuthority('CommentHierarchical:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentHierarchicalVo update(@RequestBody @Valid CommentHierarchicalUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询评论层级评论")
    @PreAuthorize("hasAuthority('CommentHierarchical:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentHierarchicalVo> getList(CommentHierarchicalPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询评论层级评论")
    @PreAuthorize("hasAuthority('CommentHierarchical:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentHierarchicalVo> getPage(CommentHierarchicalPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    /**
     * 区别于 queryById ，比 queryById数据更全
     * @param id
     * @param groupFlag
     * @param loginUser
     * @return
     * @throws InterruptedException
     */
    @ApiOperation("根据ID查询评论层级评论详细")
    @GetMapping("/detail/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CommentHierarchicalAvailableVo detailById(@PathVariable String id,String groupFlag,@ApiIgnore LoginUser loginUser) throws InterruptedException {
        CommentHierarchicalAvailableVo byId = iCommentHierarchicalService.detailById(id,groupFlag, Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));

        return byId;
    }


    @ApiOperation(value = "分页查询可展示层级评论",notes = "只适用于subject")
    @GetMapping("/getAvailableForSubjectPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentHierarchicalAvailableVo> getAvailableForSubjectPage(@Valid CommentHierarchicalAvailableForSubjectPageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) throws InterruptedException {
        IPage<CommentHierarchicalAvailableVo> commentHierarchicalIPage = iCommentHierarchicalService.listAvailableForSubjectPage(listPageForm, Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));

        return commentHierarchicalIPage;
    }

    @ApiOperation(value = "分页查询可展示层级评论",notes = "只适用于hierarchical")
    @GetMapping("/getAvailableForHierarchicalPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentHierarchicalAvailableVo> getAvailableForHierarchicalPage(@Valid CommentHierarchicalAvailableForHierarchicalPageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) throws InterruptedException {
        IPage<CommentHierarchicalAvailableVo> commentHierarchicalIPage = iCommentHierarchicalService.listAvailableForHierarchicalPage(listPageForm, Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));

        return commentHierarchicalIPage;
    }

    @ApiOperation("添加层级评论")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addCommentHierarchicalForSubject")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentHierarchicalVo addCommentHierarchicalForSubject(@RequestBody @Valid CommentHierarchicalForSubjectAddForm addForm, @ApiIgnore LoginUser loginUser) {
        CommentHierarchicalForSubjectAddParam commentHierarchicalAddParam = CommentHierarchicalMapStruct.INSTANCE.addFormForSubjectToAddParam(addForm);
        commentHierarchicalAddParam.setOwnerUserId(loginUser.getId());
        commentHierarchicalAddParam.setOwnerUserAvatar(loginUser.getAvatar());
        commentHierarchicalAddParam.setOwnerUserNickname(loginUser.getNickname());

        CommentHierarchical commentHierarchical = iCommentHierarchicalService.addCommentHierarchicalForSubject(commentHierarchicalAddParam);
        return getVo(commentHierarchical);

    }
    @ApiOperation("添加盖楼层级评论")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addCommentHierarchicalForHierarchical")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentHierarchicalVo addCommentHierarchicalForHierarchical(@RequestBody @Valid CommentHierarchicalForHierarchicalAddForm addForm, @ApiIgnore LoginUser loginUser) {
        CommentHierarchicalForHierarchicalAddParam commentHierarchicalAddParam = CommentHierarchicalMapStruct.INSTANCE.addFormForHierarchicalToAddParam(addForm);
        commentHierarchicalAddParam.setOwnerUserId(loginUser.getId());
        commentHierarchicalAddParam.setOwnerUserAvatar(loginUser.getAvatar());
        commentHierarchicalAddParam.setOwnerUserNickname(loginUser.getNickname());

        CommentHierarchical commentHierarchical = iCommentHierarchicalService.addCommentHierarchicalForHierarchical(commentHierarchicalAddParam);
        return getVo(commentHierarchical);

    }
}
