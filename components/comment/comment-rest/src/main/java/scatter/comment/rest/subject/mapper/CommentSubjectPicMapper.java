package scatter.comment.rest.subject.mapper;

import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 主体评论图片表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface CommentSubjectPicMapper extends IBaseMapper<CommentSubjectPic> {

}
