package scatter.comment.rest.rate.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.pojo.rate.form.CommentRateAddForm1;
import scatter.comment.pojo.rate.form.CommentRateCancelForm;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm1;
import scatter.comment.pojo.rate.param.CommentRateAddRateParam;
import scatter.comment.pojo.rate.param.CommentRateCancelRateParam;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.rate.vo.CommentRateVo;
import scatter.comment.rest.CommentConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.rate.vo.CommentRateVo;
import scatter.comment.pojo.rate.form.CommentRateAddForm;
import scatter.comment.pojo.rate.form.CommentRateUpdateForm;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm;
import scatter.comment.rest.rate.service.ICommentRateService;
import org.springframework.beans.factory.annotation.Autowired;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 评分表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Api(tags = "评分相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/rate/comment-rate")
public class CommentRateController extends BaseAddUpdateQueryFormController<CommentRate, CommentRateVo, CommentRateAddForm, CommentRateUpdateForm, CommentRatePageQueryForm> {
    @Autowired
    private ICommentRateService iCommentRateService;

    @ApiOperation("添加评分")
    @PreAuthorize("hasAuthority('CommentRate:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CommentRateVo add(@RequestBody @Valid CommentRateAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询评分")
    @PreAuthorize("hasAuthority('CommentRate:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CommentRateVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除评分")
    @PreAuthorize("hasAuthority('CommentRate:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新评分")
    @PreAuthorize("hasAuthority('CommentRate:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CommentRateVo update(@RequestBody @Valid CommentRateUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询评分")
    @PreAuthorize("hasAuthority('CommentRate:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<CommentRateVo> getList(CommentRatePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询评分")
    @PreAuthorize("hasAuthority('CommentRate:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<CommentRateVo> getPage(CommentRatePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("分页查询主体评分")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentRateVo> getPage1(@Valid CommentRatePageQueryForm1 listPageForm) {
        IPage<CommentRate> commentSubjectRateIPage = iCommentRateService.listPage1(listPageForm);
        return super.pagePoToVo(commentSubjectRateIPage);
    }

    @ApiOperation("添加主体一个评分")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addRate")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentRateVo addRate(@RequestBody @Valid CommentRateAddForm1 addForm1, @ApiIgnore LoginUser loginUser) {
        CommentRateAddRateParam addRatetParam = new CommentRateAddRateParam();
        addRatetParam.setSubjectId(addForm1.getSubjectId());
        addRatetParam.setOwnerUserId(loginUser.getId());
        addRatetParam.setOwnerUserAvatar(loginUser.getAvatar());
        addRatetParam.setOwnerUserNickname(loginUser.getNickname());
        addRatetParam.setGroupFlag(addForm1.getGroupFlag());
        return getVo(iCommentRateService.addRate(addRatetParam));
    }

    @ApiOperation("取消主体一个评分")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/cancelRate")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean cancelRate(@RequestBody @Valid CommentRateCancelForm cancelForm, @ApiIgnore LoginUser loginUser) {
        CommentRateCancelRateParam cancelRateParam = new CommentRateCancelRateParam();
        cancelRateParam.setSubjectId(cancelForm.getSubjectId());
        cancelRateParam.setOwnerUserId(loginUser.getId());
        cancelRateParam.setGroupFlag(cancelForm.getGroupFlag());
        return iCommentRateService.cancelRate(cancelRateParam);
    }
}
