package scatter.comment.rest.subject.service.impl;

import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.comment.rest.subject.mapper.CommentSubjectPicMapper;
import scatter.comment.rest.subject.service.ICommentSubjectPicService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.comment.pojo.subject.form.CommentSubjectPicAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 主体评论图片表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Service
@Transactional
public class CommentSubjectPicServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<CommentSubjectPicMapper, CommentSubjectPic, CommentSubjectPicAddForm, CommentSubjectPicUpdateForm, CommentSubjectPicPageQueryForm> implements ICommentSubjectPicService {
    @Override
    public void preAdd(CommentSubjectPicAddForm addForm,CommentSubjectPic po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(CommentSubjectPicUpdateForm updateForm,CommentSubjectPic po) {
        super.preUpdate(updateForm,po);

    }
}
