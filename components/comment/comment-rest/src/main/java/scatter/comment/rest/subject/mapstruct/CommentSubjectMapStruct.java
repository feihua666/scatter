package scatter.comment.rest.subject.mapstruct;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.comment.pojo.subject.form.CommentSubjectAddForm1;
import scatter.comment.pojo.subject.param.CommentSubjectAddParam;
import scatter.comment.pojo.subject.vo.CommentSubjectAvailableVo;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.pojo.subject.vo.CommentSubjectVo;
import scatter.comment.pojo.subject.form.CommentSubjectAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPageQueryForm;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 主体评论 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentSubjectMapStruct extends IBaseVoMapStruct<CommentSubject, CommentSubjectVo>,
                                  IBaseAddFormMapStruct<CommentSubject,CommentSubjectAddForm>,
                                  IBaseUpdateFormMapStruct<CommentSubject,CommentSubjectUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentSubject,CommentSubjectPageQueryForm>{
    CommentSubjectMapStruct INSTANCE = Mappers.getMapper( CommentSubjectMapStruct.class );

    CommentSubjectAddParam addForm1ToAddParam(CommentSubjectAddForm1 addform1);
    CommentSubject addParamToPo(CommentSubjectAddParam addParam);

    /**
     * 单表po转vo
     * @param po
     * @return
     */
    CommentSubjectAvailableVo poToCommentSubjectAvailableVo(CommentSubject po);

    default List<CommentSubjectAvailableVo> posToCommentSubjectAvailableVos(List<CommentSubject> pos) {
        return pos.stream().map(po->(poToCommentSubjectAvailableVo(po))).collect(Collectors.toList());
    }

    default IPage<CommentSubjectAvailableVo> pagePoToCommentSubjectAvailableVo(IPage<CommentSubject> page){
        IPage pageR = page;
        List<CommentSubject> records = pageR.getRecords();
        if (pageR != null && !CollectionUtil.isEmpty(records)) {
            pageR.setRecords(posToCommentSubjectAvailableVos(records));
            return pageR;
        }
        // 原样返回page
        return pageR;
    }
}
