package scatter.comment.rest.star.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.rest.star.service.ICommentStarService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 主体点赞翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
@Component
public class CommentStarTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentStarService commentStarService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentStar.TRANS_COMMENTSTAR_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentStar.TRANS_COMMENTSTAR_BY_ID)) {
            CommentStar byId = commentStarService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentStar.TRANS_COMMENTSTAR_BY_ID)) {
            return commentStarService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
