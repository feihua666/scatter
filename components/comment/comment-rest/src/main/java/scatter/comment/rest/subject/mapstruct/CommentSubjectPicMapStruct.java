package scatter.comment.rest.subject.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.comment.pojo.subject.vo.CommentSubjectPicVo;
import scatter.comment.pojo.subject.form.CommentSubjectPicAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicPageQueryForm;

/**
 * <p>
 * 主体评论图片 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentSubjectPicMapStruct extends IBaseVoMapStruct<CommentSubjectPic, CommentSubjectPicVo>,
                                  IBaseAddFormMapStruct<CommentSubjectPic,CommentSubjectPicAddForm>,
                                  IBaseUpdateFormMapStruct<CommentSubjectPic,CommentSubjectPicUpdateForm>,
                                  IBaseQueryFormMapStruct<CommentSubjectPic,CommentSubjectPicPageQueryForm>{
    CommentSubjectPicMapStruct INSTANCE = Mappers.getMapper( CommentSubjectPicMapStruct.class );

}
