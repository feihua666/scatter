package scatter.comment.rest.enjoy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.rest.enjoy.service.ICommentEnjoyService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 喜欢|想去|想看翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Component
public class CommentEnjoyTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ICommentEnjoyService commentEnjoyService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,CommentEnjoy.TRANS_COMMENTENJOY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,CommentEnjoy.TRANS_COMMENTENJOY_BY_ID)) {
            CommentEnjoy byId = commentEnjoyService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,CommentEnjoy.TRANS_COMMENTENJOY_BY_ID)) {
            return commentEnjoyService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
