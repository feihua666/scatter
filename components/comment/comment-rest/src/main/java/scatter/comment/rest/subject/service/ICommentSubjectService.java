package scatter.comment.rest.subject.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import scatter.comment.pojo.subject.form.CommentSubjectAvailablePageQueryForm;
import scatter.comment.pojo.subject.param.CommentSubjectAddParam;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.pojo.subject.vo.CommentSubjectAvailableVo;
import scatter.comment.rest.subject.mapstruct.CommentSubjectMapStruct;
import scatter.common.LoginUser;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.service.IBaseService;
import scatter.comment.pojo.subject.form.CommentSubjectAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPageQueryForm;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 主体评论表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface ICommentSubjectService extends IBaseService<CommentSubject> {



	/**
	 * 列出每一个 subject 最新的主体评论数据
	 * @param subjectIds
	 * @param count
	 * @param groupFlag 分组标识 选填
	 * @return
	 */
	public Map<String, Page<CommentSubject>> listLatestCommentSubjectForPerSubject(List<String> subjectIds, Integer count, String groupFlag);

	/**
	 * 添加主体评论
	 * @param addParam
	 * @return
	 */
	public CommentSubject addCommentSubject(CommentSubjectAddParam addParam);

	/**
	 * 详情
	 * @param id
	 * @param groupFlag
	 * @param loginUserId
	 * @return
	 * @throws InterruptedException
	 */
	public CommentSubjectAvailableVo detailById(String id, String groupFlag,String loginUserId) throws InterruptedException;

	/**
	 * 分页查询可用主体评论
	 * @param pageQueryForm
	 * @return
	 */
	public IPage<CommentSubjectAvailableVo> listAvailablePage(CommentSubjectAvailablePageQueryForm pageQueryForm,String loginUserId) throws InterruptedException;

}
