package scatter.comment.rest.componentext;

import scatter.comment.pojo.subject.param.CommentSubjectAddParam;
import scatter.comment.pojo.subject.po.CommentSubject;

/**
 * <p>
 * 添加评论监听
 * </p>
 *
 * @author yangwei
 * @since 2021-10-27 22:07
 */
public interface CommentSubjectOnAddListener {

	/**
	 * 添加主体评论前调用
	 * @param addParam
	 * @param subject
	 */
	public void preCommentSubjectAdd(CommentSubjectAddParam addParam, CommentSubject subject);
	/**
	 * 添加主体评论后调用
	 * @param addParam
	 * @param subject
	 */
	public void postCommentSubjectAdd(CommentSubjectAddParam addParam, CommentSubject subject);
}
