package scatter.comment.rest.componentext;

import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalAvailableVo;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalVo;
import scatter.comment.pojo.subject.vo.CommentSubjectAvailableVo;
import scatter.comment.pojo.subject.vo.CommentSubjectVo;

import java.util.List;

/**
 * <p>
 * 获取可用层级评论添加额外数据处理器
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 16:07
 */
public interface CommentHierarchicalAvailableExtProvider {
	/**
	 * 提供额外补充
	 * @param vos
	 */
	public void provideCommentHierarchical(List<CommentHierarchicalAvailableVo> vos);
}
