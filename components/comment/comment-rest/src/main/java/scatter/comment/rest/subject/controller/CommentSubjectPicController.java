package scatter.comment.rest.subject.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comment.rest.CommentConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.comment.pojo.subject.vo.CommentSubjectPicVo;
import scatter.comment.pojo.subject.form.CommentSubjectPicAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 主体评论图片表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "主体评论图片相关接口")
@RestController
@RequestMapping(CommentConfiguration.CONTROLLER_BASE_PATH + "/comment-subject-pic")
public class CommentSubjectPicController extends BaseAddUpdateQueryFormController<CommentSubjectPic, CommentSubjectPicVo, CommentSubjectPicAddForm, CommentSubjectPicUpdateForm, CommentSubjectPicPageQueryForm> {


     @Override
	 @ApiOperation("添加主体评论图片")
     @PreAuthorize("hasAuthority('CommentSubjectPic:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentSubjectPicVo add(@RequestBody @Valid CommentSubjectPicAddForm addForm) {
         return super.add(addForm);
     }

     @Override
     @ApiOperation("根据ID查询主体评论图片")
     @PreAuthorize("hasAuthority('CommentSubjectPic:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public CommentSubjectPicVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
     @ApiOperation("根据ID删除主体评论图片")
     @PreAuthorize("hasAuthority('CommentSubjectPic:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
     @ApiOperation("根据ID更新主体评论图片")
     @PreAuthorize("hasAuthority('CommentSubjectPic:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CommentSubjectPicVo update(@RequestBody @Valid CommentSubjectPicUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
    @ApiOperation("不分页查询主体评论图片")
    @PreAuthorize("hasAuthority('CommentSubjectPic:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentSubjectPicVo> getList(CommentSubjectPicPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
    @ApiOperation("分页查询主体评论图片")
    @PreAuthorize("hasAuthority('CommentSubjectPic:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CommentSubjectPicVo> getPage(CommentSubjectPicPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
