package scatter.comment.rest.componentext;

import scatter.comment.pojo.star.param.CommentStarAddStarParam;
import scatter.comment.pojo.star.param.CommentStarCancelStarParam;
import scatter.comment.pojo.star.po.CommentStar;

/**
 * <p>
 * 扩展添加点赞监听
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 13:15
 */
public interface CommentStarOnAddOrCancelListener {
	/**
	 * 添加点赞后调用
	 * 本监听只针对于主体
	 * @param addParam 属性 subjectId 一定有值
	 * @param commentStar
	 */
	void onCommentStarAdd(CommentStarAddStarParam addParam, CommentStar commentStar);

	/**
	 * 取消点赞后调用
	 * @param addParam
	 */
	void onCommentStarCancel(CommentStarCancelStarParam addParam);

}
