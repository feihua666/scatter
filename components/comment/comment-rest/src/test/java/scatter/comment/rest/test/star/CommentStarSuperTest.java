package scatter.comment.rest.test.star;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.pojo.star.form.CommentStarAddForm;
import scatter.comment.pojo.star.form.CommentStarUpdateForm;
import scatter.comment.pojo.star.form.CommentStarPageQueryForm;
import scatter.comment.rest.star.service.ICommentStarService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 主体点赞 测试类基类
* </p>
*
* @author yw
* @since 2021-11-04
*/
public class CommentStarSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ICommentStarService commentStarService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return commentStarService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return commentStarService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public CommentStar mockPo() {
        return JMockData.mock(CommentStar.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public CommentStarAddForm mockAddForm() {
        return JMockData.mock(CommentStarAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public CommentStarUpdateForm mockUpdateForm() {
        return JMockData.mock(CommentStarUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public CommentStarPageQueryForm mockPageQueryForm() {
        return JMockData.mock(CommentStarPageQueryForm.class, mockConfig);
    }
}