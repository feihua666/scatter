package scatter.comment.rest.test.rate.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.rate.form.CommentRateAddForm;
import scatter.comment.pojo.rate.form.CommentRateUpdateForm;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm;
import scatter.comment.rest.test.rate.CommentRateSuperTest;
import scatter.comment.rest.rate.service.ICommentRateService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 评分 服务测试类
* </p>
*
* @author yw
* @since 2021-11-03
*/
@SpringBootTest
public class CommentRateServiceTest extends CommentRateSuperTest{

    @Autowired
    private ICommentRateService commentRateService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentRate> pos = commentRateService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}