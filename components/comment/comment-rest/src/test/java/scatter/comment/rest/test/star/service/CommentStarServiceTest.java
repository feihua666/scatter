package scatter.comment.rest.test.star.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.star.po.CommentStar;
import scatter.comment.pojo.star.form.CommentStarAddForm;
import scatter.comment.pojo.star.form.CommentStarUpdateForm;
import scatter.comment.pojo.star.form.CommentStarPageQueryForm;
import scatter.comment.rest.test.star.CommentStarSuperTest;
import scatter.comment.rest.star.service.ICommentStarService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 主体点赞 服务测试类
* </p>
*
* @author yw
* @since 2021-11-04
*/
@SpringBootTest
public class CommentStarServiceTest extends CommentStarSuperTest{

    @Autowired
    private ICommentStarService commentStarService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentStar> pos = commentStarService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}