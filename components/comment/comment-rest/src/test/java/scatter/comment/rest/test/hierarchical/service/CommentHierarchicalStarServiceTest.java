package scatter.comment.rest.test.hierarchical.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarPageQueryForm;
import scatter.comment.rest.test.hierarchical.CommentHierarchicalStarSuperTest;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalStarService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 评论点赞 服务测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class CommentHierarchicalStarServiceTest extends CommentHierarchicalStarSuperTest{

    @Autowired
    private ICommentHierarchicalStarService commentHierarchicalStarService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentHierarchicalStar> pos = commentHierarchicalStarService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}