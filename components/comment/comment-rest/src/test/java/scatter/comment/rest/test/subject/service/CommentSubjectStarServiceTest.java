package scatter.comment.rest.test.subject.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.subject.po.CommentSubjectStar;
import scatter.comment.pojo.subject.form.CommentSubjectStarAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectStarUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectStarPageQueryForm;
import scatter.comment.rest.test.subject.CommentSubjectStarSuperTest;
import scatter.comment.rest.subject.service.ICommentSubjectStarService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 主体评论点赞 服务测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class CommentSubjectStarServiceTest extends CommentSubjectStarSuperTest{

    @Autowired
    private ICommentSubjectStarService commentSubjectStarService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentSubjectStar> pos = commentSubjectStarService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}