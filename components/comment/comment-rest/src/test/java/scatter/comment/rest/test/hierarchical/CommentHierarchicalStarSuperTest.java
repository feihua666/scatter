package scatter.comment.rest.test.hierarchical;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.pojo.hierarchical.po.CommentHierarchicalStar;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalStarPageQueryForm;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalStarService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 评论点赞 测试类基类
* </p>
*
* @author yw
* @since 2021-02-09
*/
public class CommentHierarchicalStarSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ICommentHierarchicalStarService commentHierarchicalStarService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return commentHierarchicalStarService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return commentHierarchicalStarService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public CommentHierarchicalStar mockPo() {
        return JMockData.mock(CommentHierarchicalStar.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public CommentHierarchicalStarAddForm mockAddForm() {
        return JMockData.mock(CommentHierarchicalStarAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public CommentHierarchicalStarUpdateForm mockUpdateForm() {
        return JMockData.mock(CommentHierarchicalStarUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public CommentHierarchicalStarPageQueryForm mockPageQueryForm() {
        return JMockData.mock(CommentHierarchicalStarPageQueryForm.class, mockConfig);
    }
}