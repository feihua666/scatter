package scatter.comment.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.comment.rest.CommentConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, CommentConfiguration.class})
@MapperScan("scatter.comment.rest.mapper")
public class CommentTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommentTestApplication.class, args);
    }
}
