package scatter.comment.rest.test.subject.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.comment.pojo.subject.form.CommentSubjectPicAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPicPageQueryForm;
import scatter.comment.rest.test.subject.CommentSubjectPicSuperTest;
import scatter.comment.rest.subject.service.ICommentSubjectPicService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 主体评论图片 服务测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class CommentSubjectPicServiceTest extends CommentSubjectPicSuperTest{

    @Autowired
    private ICommentSubjectPicService commentSubjectPicService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentSubjectPic> pos = commentSubjectPicService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}