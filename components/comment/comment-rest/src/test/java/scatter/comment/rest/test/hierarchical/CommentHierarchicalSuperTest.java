package scatter.comment.rest.test.hierarchical;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPageQueryForm;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 评论层级评论 测试类基类
* </p>
*
* @author yw
* @since 2021-02-09
*/
public class CommentHierarchicalSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ICommentHierarchicalService commentHierarchicalService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return commentHierarchicalService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return commentHierarchicalService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public CommentHierarchical mockPo() {
        return JMockData.mock(CommentHierarchical.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public CommentHierarchicalAddForm mockAddForm() {
        return JMockData.mock(CommentHierarchicalAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public CommentHierarchicalUpdateForm mockUpdateForm() {
        return JMockData.mock(CommentHierarchicalUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public CommentHierarchicalPageQueryForm mockPageQueryForm() {
        return JMockData.mock(CommentHierarchicalPageQueryForm.class, mockConfig);
    }
}