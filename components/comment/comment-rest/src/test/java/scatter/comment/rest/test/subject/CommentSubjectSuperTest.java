package scatter.comment.rest.test.subject;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.pojo.subject.po.CommentSubject;
import scatter.comment.pojo.subject.form.CommentSubjectAddForm;
import scatter.comment.pojo.subject.form.CommentSubjectUpdateForm;
import scatter.comment.pojo.subject.form.CommentSubjectPageQueryForm;
import scatter.comment.rest.subject.service.ICommentSubjectService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 主体评论 测试类基类
* </p>
*
* @author yw
* @since 2021-02-09
*/
public class CommentSubjectSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ICommentSubjectService commentSubjectService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return commentSubjectService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return commentSubjectService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public CommentSubject mockPo() {
        return JMockData.mock(CommentSubject.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public CommentSubjectAddForm mockAddForm() {
        return JMockData.mock(CommentSubjectAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public CommentSubjectUpdateForm mockUpdateForm() {
        return JMockData.mock(CommentSubjectUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public CommentSubjectPageQueryForm mockPageQueryForm() {
        return JMockData.mock(CommentSubjectPageQueryForm.class, mockConfig);
    }
}