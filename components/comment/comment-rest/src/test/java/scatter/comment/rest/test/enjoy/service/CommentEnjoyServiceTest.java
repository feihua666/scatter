package scatter.comment.rest.test.enjoy.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.enjoy.po.CommentEnjoy;
import scatter.comment.pojo.enjoy.form.CommentEnjoyAddForm;
import scatter.comment.pojo.enjoy.form.CommentEnjoyUpdateForm;
import scatter.comment.pojo.enjoy.form.CommentEnjoyPageQueryForm;
import scatter.comment.rest.test.enjoy.CommentEnjoySuperTest;
import scatter.comment.rest.enjoy.service.ICommentEnjoyService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 喜欢|想去|想看 服务测试类
* </p>
*
* @author yw
* @since 2021-11-03
*/
@SpringBootTest
public class CommentEnjoyServiceTest extends CommentEnjoySuperTest{

    @Autowired
    private ICommentEnjoyService commentEnjoyService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentEnjoy> pos = commentEnjoyService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}