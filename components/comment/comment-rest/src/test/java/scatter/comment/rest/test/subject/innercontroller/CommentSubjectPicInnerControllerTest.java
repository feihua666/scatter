package scatter.comment.rest.test.subject.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.rest.test.subject.CommentSubjectPicSuperTest;
/**
* <p>
* 主体评论图片 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class CommentSubjectPicInnerControllerTest extends CommentSubjectPicSuperTest{
    @Test
    void contextLoads() {
    }
}