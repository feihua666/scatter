package scatter.comment.rest.test.hierarchical.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comment.pojo.hierarchical.po.CommentHierarchical;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalAddForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalUpdateForm;
import scatter.comment.pojo.hierarchical.form.CommentHierarchicalPageQueryForm;
import scatter.comment.rest.test.hierarchical.CommentHierarchicalSuperTest;
import scatter.comment.rest.hierarchical.service.ICommentHierarchicalService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 评论层级评论 服务测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class CommentHierarchicalServiceTest extends CommentHierarchicalSuperTest{

    @Autowired
    private ICommentHierarchicalService commentHierarchicalService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<CommentHierarchical> pos = commentHierarchicalService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}