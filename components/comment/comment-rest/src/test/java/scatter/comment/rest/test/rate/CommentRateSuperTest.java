package scatter.comment.rest.test.rate;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comment.pojo.rate.po.CommentRate;
import scatter.comment.pojo.rate.form.CommentRateAddForm;
import scatter.comment.pojo.rate.form.CommentRateUpdateForm;
import scatter.comment.pojo.rate.form.CommentRatePageQueryForm;
import scatter.comment.rest.rate.service.ICommentRateService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 评分 测试类基类
* </p>
*
* @author yw
* @since 2021-11-03
*/
public class CommentRateSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ICommentRateService commentRateService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return commentRateService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return commentRateService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public CommentRate mockPo() {
        return JMockData.mock(CommentRate.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public CommentRateAddForm mockAddForm() {
        return JMockData.mock(CommentRateAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public CommentRateUpdateForm mockUpdateForm() {
        return JMockData.mock(CommentRateUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public CommentRatePageQueryForm mockPageQueryForm() {
        return JMockData.mock(CommentRatePageQueryForm.class, mockConfig);
    }
}