package scatter.comment.pojo.rate.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 18:54
 */
@Setter
@Getter
@ApiModel(value="评分分页表单对象")
public class CommentRatePageQueryForm1 extends BasePageQueryForm {

	@NotEmpty(message = "主体id不能为空")
	@ApiModelProperty(value = "主体id",required = true)
	private String subjectId;

	@ApiModelProperty(value = "评分人用户id")
	private String ownerUserId;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;
}
