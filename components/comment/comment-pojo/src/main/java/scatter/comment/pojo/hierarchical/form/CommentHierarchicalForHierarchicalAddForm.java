package scatter.comment.pojo.hierarchical.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 评论层级评论添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="评论层级评论添加表单对象")
public class CommentHierarchicalForHierarchicalAddForm extends BaseAddForm {

    @NotEmpty(message="评论内容不能为空")
    @ApiModelProperty(value = "评论表id",required = true)
    private String commentHierarchicalId;

    @NotEmpty(message="评论内容不能为空")
    @ApiModelProperty(value = "评论内容",required = true)
    private String content;

    @ApiModelProperty(value = "被评论人用户id")
    private String targetUserId;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
