package scatter.comment.pojo.subject.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主体评论表
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_subject")
@ApiModel(value="CommentSubject对象", description="主体评论表")
public class CommentSubject extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTSUBJECT_BY_ID = "trans_commentsubject_by_id_scatter.comment.pojo.subject.po";

    @ApiModelProperty(value = "评论主体id")
    private String subjectId;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "评论人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "点赞人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "点赞数")
    private Integer starCount;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;


}
