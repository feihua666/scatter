package scatter.comment.pojo.star.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 取消点赞表单对象
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 22:23
 */
@Setter
@Getter
@ApiModel(value="主体取消点赞表单对象")
public class CommentStarCancelForm extends BaseForm {

	@NotEmpty(message = "主体表id不能为空")
	@ApiModelProperty(value = "主体表id",required = true)
	private String subjectId;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;

}
