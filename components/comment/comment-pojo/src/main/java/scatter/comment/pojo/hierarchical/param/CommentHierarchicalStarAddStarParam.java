package scatter.comment.pojo.hierarchical.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 添加点赞参数
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 10:46
 */
@Setter
@Getter
public class CommentHierarchicalStarAddStarParam extends BaseParam {

	@ApiModelProperty(value = "评论表id")
	private String commentHierarchicalId;

	@NotEmpty(message="点赞人用户id不能为空")
	@ApiModelProperty(value = "点赞人用户id",required = true)
	private String ownerUserId;

	@NotEmpty(message="点赞人用户昵称不能为空")
	@ApiModelProperty(value = "点赞人用户昵称",required = true)
	private String ownerUserNickname;

	@ApiModelProperty(value = "点赞人用户头像")
	private String ownerUserAvatar;

	@NotNull(message="所在楼层不能为空")
	@ApiModelProperty(value = "所在楼层，排序，升序",required = true)
	private Integer floor;

}
