package scatter.comment.pojo.subject.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.OrderBy;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 18:54
 */
@Setter
@Getter
@ApiModel(value="主体评论点赞分页表单对象")
public class CommentSubjectStarPageQueryForm1 extends BasePageQueryForm {

	@NotEmpty(message = "主体评论表id不能为空")
	@ApiModelProperty(value = "主体评论表id",required = true)
	private String commentSubjectId;

	@ApiModelProperty(value = "点赞人用户id")
	private String ownerUserId;

}
