package scatter.comment.pojo.hierarchical.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 评论层级评论更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="评论层级评论更新表单对象")
public class CommentHierarchicalUpdateForm extends BaseUpdateIdForm {

    @ApiModelProperty(value = "评论主体id")
    private String subjectId;

    @NotEmpty(message="评论内容不能为空")
    @ApiModelProperty(value = "评论内容",required = true)
    private String content;

    @NotNull(message="评论时间不能为空")
    @ApiModelProperty(value = "评论时间",required = true)
    private LocalDateTime timeAt;

    @NotEmpty(message="评论人用户id不能为空")
    @ApiModelProperty(value = "评论人用户id",required = true)
    private String ownerUserId;

    @NotEmpty(message="点赞人用户昵称不能为空")
    @ApiModelProperty(value = "点赞人用户昵称",required = true)
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "被评论人用户id")
    private String targetUserId;

    @NotNull(message="所在楼层不能为空")
    @ApiModelProperty(value = "所在楼层，排序，升序",required = true)
    private Integer floor;

    @NotNull(message="子一级评论数不能为空")
    @ApiModelProperty(value = "子一级评论数",required = true)
    private Integer commentCount;

    @NotNull(message="点赞数不能为空")
    @ApiModelProperty(value = "点赞数",required = true)
    private Integer starCount;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
