package scatter.comment.pojo.subject.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 主体评论图片添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体评论图片添加表单对象")
public class CommentSubjectPicAddForm extends BaseAddForm {

    @NotEmpty(message="评论主体评论表id不能为空")
    @ApiModelProperty(value = "评论主体评论表id",required = true)
    private String commentSubjectId;

    @NotEmpty(message="图片地址不能为空")
    @ApiModelProperty(value = "图片地址",required = true)
    private String picUrl;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序，默认升序",required = true)
    private Integer seq;

}
