package scatter.comment.pojo.subject.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-27 21:17
 */
@PropValid
@Setter
@Getter
public class CommentSubjectAddForm1 {

	@NotEmpty(message="评论主体id不能为空")
	@ApiModelProperty(value = "评论主体id",required = true)
	private String subjectId;

	@NotEmpty(message="评论内容不能为空")
	@ApiModelProperty(value = "评论内容",required = true)
	private String content;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;

	@ApiModelProperty(value = "图片地址")
	private List<String> commentSubjectPicUrls;
}
