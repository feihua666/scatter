package scatter.comment.pojo.star.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主体点赞表
 * </p>
 *
 * @author yw
 * @since 2021-11-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_star")
@ApiModel(value="CommentStar对象", description="主体点赞表")
public class CommentStar extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTSTAR_BY_ID = "trans_commentstar_by_id_scatter.comment.pojo.star.po";

    @ApiModelProperty(value = "主体id")
    private String subjectId;

    @ApiModelProperty(value = "点赞时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "点赞人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "点赞人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;


}
