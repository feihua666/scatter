package scatter.comment.pojo.rate.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 主体评分添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体评分添加表单对象")
public class CommentRateAddForm1 extends BaseAddForm {

    @NotEmpty(message = "主体id不能为空")
    @ApiModelProperty(value = "主体id",required = true)
    private String subjectId;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @NotNull(message = "评分不能为空")
    @Min(message = "评论不能小于0",value = 0)
    @ApiModelProperty(value = "评分",required = true)
    private Double rate;
}
