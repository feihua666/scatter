package scatter.comment.pojo.hierarchical.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.OrderBy;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

/**
 * <p>
 * 可展示评论层级评论分页表单对象
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 11:18
 */
@PropValid
@Setter
@Getter
@ApiModel(value="可展示评论层级评论分页表单对象")
public class CommentHierarchicalAvailableForSubjectPageQueryForm extends BasePageQueryForm {


	@NotEmpty(message = "评论主体id不能为空")
	@ApiModelProperty(value = "评论主体id",required = true)
	private String subjectId;

	@ApiModelProperty(value = "评论人用户id")
	private String ownerUserId;

	@ApiModelProperty(value = "被评论人用户id")
	private String targetUserId;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;

}
