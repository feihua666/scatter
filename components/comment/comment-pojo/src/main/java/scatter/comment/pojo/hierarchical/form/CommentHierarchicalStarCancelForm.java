package scatter.comment.pojo.hierarchical.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 层级评论取消点赞添加表单对象
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 14:25
 */
@Setter
@Getter
@ApiModel(value="层级评论取消点赞添加表单对象")
public class CommentHierarchicalStarCancelForm extends BaseForm {


	@NotEmpty(message = "评论表id不能为空")
	@ApiModelProperty(value = "评论表id",required = true)
	private String commentHierarchicalId;

}
