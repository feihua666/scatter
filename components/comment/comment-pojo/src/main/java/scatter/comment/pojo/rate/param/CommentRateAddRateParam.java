package scatter.comment.pojo.rate.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 添加评分参数对象
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 18:33
 */
@Setter
@Getter
public class CommentRateAddRateParam extends BaseParam {

	@ApiModelProperty(value = "主体id")
	private String subjectId;

	@NotEmpty(message="评分人用户id不能为空")
	@ApiModelProperty(value = "评分人用户id",required = true)
	private String ownerUserId;

	@NotEmpty(message="评分人用户昵称不能为空")
	@ApiModelProperty(value = "评分人用户昵称",required = true)
	private String ownerUserNickname;

	@ApiModelProperty(value = "评分人用户头像")
	private String ownerUserAvatar;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;

	@ApiModelProperty(value = "评分")
	private Double rate;

	@ApiModelProperty(value = "所在楼层，排序，升序")
	private Integer floor;
}
