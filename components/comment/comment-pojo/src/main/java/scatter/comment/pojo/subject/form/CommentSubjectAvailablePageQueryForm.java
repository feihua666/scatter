package scatter.comment.pojo.subject.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.OrderBy;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;


/**
 * <p>
 * 主体评论分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体评论分页表单对象")
public class CommentSubjectAvailablePageQueryForm extends BasePageQueryForm {

    @NotEmpty(message = "评论主体id不能为空")
    @ApiModelProperty(value = "评论主体id",required = true)
    private String subjectId;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
