package scatter.comment.pojo.hierarchical.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 评论层级评论图片表
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_hierarchical_pic")
@ApiModel(value="CommentHierarchicalPic对象", description="评论层级评论图片表")
public class CommentHierarchicalPic extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTHIERARCHICALPIC_BY_ID = "trans_commenthierarchicalpic_by_id_scatter.comment.pojo.hierarchical.po";

    @ApiModelProperty(value = "评论主体评论层级表id")
    private String commentHierarchicalId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "排序，默认升序")
    private Integer seq;


}
