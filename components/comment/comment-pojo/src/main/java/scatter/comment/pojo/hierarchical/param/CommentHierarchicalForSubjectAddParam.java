package scatter.comment.pojo.hierarchical.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 添加层级评论参数
 * </p>
 *
 * @author yangwei
 * @since 2021-10-27 21:20
 */
@Setter
@Getter
public class CommentHierarchicalForSubjectAddParam extends BaseParam {


	@NotEmpty(message="评论主体id不能为空")
	@ApiModelProperty(value = "评论主体id")
	private String subjectId;

	@NotEmpty(message="评论内容不能为空")
	@ApiModelProperty(value = "评论内容",required = true)
	private String content;

	@NotEmpty(message="评论人用户id不能为空")
	@ApiModelProperty(value = "评论人用户id",required = true)
	private String ownerUserId;

	@NotEmpty(message="点赞人用户昵称不能为空")
	@ApiModelProperty(value = "点赞人用户昵称",required = true)
	private String ownerUserNickname;

	@ApiModelProperty(value = "点赞人用户头像")
	private String ownerUserAvatar;

	@ApiModelProperty(value = "被评论人用户id")
	private String targetUserId;

	@ApiModelProperty(value = "所在楼层，排序，升序")
	private Integer floor;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;

	@ApiModelProperty(value = "图片地址")
	private List<String> commentHierarchicalPicUrls;
}
