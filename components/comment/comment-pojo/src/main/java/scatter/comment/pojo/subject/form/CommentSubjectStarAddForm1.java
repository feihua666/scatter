package scatter.comment.pojo.subject.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 主体评论点赞添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体评论点赞添加表单对象")
public class CommentSubjectStarAddForm1 extends BaseAddForm {

    @NotEmpty(message = "主体评论表id不能为空")
    @ApiModelProperty(value = "主体评论表id",required = true)
    private String commentSubjectId;


}
