package scatter.comment.pojo.hierarchical.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 可展示评论层级评论分页表单对象
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 11:18
 */
@PropValid
@Setter
@Getter
@ApiModel(value="可展示评论层级评论分页表单对象")
public class CommentHierarchicalAvailableForHierarchicalPageQueryForm extends BasePageQueryForm {

	@NotEmpty(message = "层级评论id不能为空")
	@ApiModelProperty(value = "层级评论id",required = true)
	private String commentHierarchicalId;

	@ApiModelProperty(value = "评论人用户id")
	private String ownerUserId;

	@ApiModelProperty(value = "被评论人用户id")
	private String targetUserId;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;

}
