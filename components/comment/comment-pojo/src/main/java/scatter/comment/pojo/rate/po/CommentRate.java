package scatter.comment.pojo.rate.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 评分表
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_rate")
@ApiModel(value="CommentRate对象", description="评分表")
public class CommentRate extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTRATE_BY_ID = "trans_commentrate_by_id_scatter.comment.pojo.rate.po";

    @ApiModelProperty(value = "主体评论表id")
    private String subjectId;

    @ApiModelProperty(value = "时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "评分")
    private Double rate;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;


}
