package scatter.comment.pojo.hierarchical.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 取消点赞参数
 * </p>
 *
 * @author yangwei
 * @since 2021-10-29 10:56
 */
@Setter
@Getter
public class CommentHierarchicalCancelStarParam extends BaseParam {

	@ApiModelProperty(value = "评论表id")
	private String commentHierarchicalId;

	@NotEmpty(message="点赞人用户id不能为空")
	@ApiModelProperty(value = "点赞人用户id",required = true)
	private String ownerUserId;

}
