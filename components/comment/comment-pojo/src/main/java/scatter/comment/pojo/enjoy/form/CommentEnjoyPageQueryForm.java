package scatter.comment.pojo.enjoy.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 喜欢|想去|想看分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Setter
@Getter
@ApiModel(value="喜欢|想去|想看分页表单对象")
public class CommentEnjoyPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "主体评论表id")
    private String subjectId;

    @ApiModelProperty(value = "时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "用户头像")
    private String ownerUserAvatar;

    @OrderBy(asc = true)
    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
