package scatter.comment.pojo.hierarchical.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 评论层级评论分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="评论层级评论分页表单对象")
public class CommentHierarchicalPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "评论主体id")
    private String subjectId;

    @ApiModelProperty(value = "评论时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "评论人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "点赞人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "被评论人用户id")
    private String targetUserId;

    @OrderBy(asc = true)
    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "点赞数")
    private Integer starCount;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
