package scatter.comment.pojo.subject.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.comment.pojo.hierarchical.vo.CommentHierarchicalVo;
import scatter.comment.pojo.subject.po.CommentSubjectPic;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.impl.TableNameTransServiceImpl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <p>
 * 主体评论响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体评论响应对象")
public class CommentSubjectVo extends BaseIdVo {

    @ApiModelProperty(value = "评论主体id")
    private String subjectId;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "评论人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "评论人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "评论人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "评论数")
    private Integer starCount;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
