package scatter.comment.pojo.enjoy.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 喜欢|想去|想看表
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_enjoy")
@ApiModel(value="CommentEnjoy对象", description="喜欢|想去|想看表")
public class CommentEnjoy extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTENJOY_BY_ID = "trans_commentenjoy_by_id_scatter.comment.pojo.enjoy.po";

    @ApiModelProperty(value = "主体评论表id")
    private String subjectId;

    @ApiModelProperty(value = "时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;


}
