package scatter.comment.pojo.rate.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 评分响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Setter
@Getter
@ApiModel(value="评分响应对象")
public class CommentRateVo extends BaseIdVo {

    @ApiModelProperty(value = "主体评论表id")
    private String subjectId;

    @ApiModelProperty(value = "时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "评分")
    private Double rate;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
