package scatter.comment.pojo.hierarchical.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 评论点赞添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="评论点赞添加表单对象")
public class CommentHierarchicalStarAddForm1 extends BaseAddForm {

    @NotEmpty(message = "评论表id不能为空")
    @ApiModelProperty(value = "评论表id",required = true)
    private String commentHierarchicalId;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;


}
