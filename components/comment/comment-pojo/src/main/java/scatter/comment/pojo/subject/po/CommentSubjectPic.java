package scatter.comment.pojo.subject.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主体评论图片表
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_subject_pic")
@ApiModel(value="CommentSubjectPic对象", description="主体评论图片表")
public class CommentSubjectPic extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTSUBJECTPIC_BY_ID = "trans_commentsubjectpic_by_id_scatter.comment.pojo.subject.po";

    @ApiModelProperty(value = "评论主体评论表id")
    private String commentSubjectId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "排序，默认升序")
    private Integer seq;


}
