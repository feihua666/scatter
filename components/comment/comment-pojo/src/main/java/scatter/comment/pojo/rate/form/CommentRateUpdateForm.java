package scatter.comment.pojo.rate.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 评分更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-03
 */
@Setter
@Getter
@ApiModel(value="评分更新表单对象")
public class CommentRateUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="主体评论表id不能为空")
    @ApiModelProperty(value = "主体评论表id",required = true)
    private String subjectId;

    @NotNull(message="时间不能为空")
    @ApiModelProperty(value = "时间",required = true)
    private LocalDateTime timeAt;

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String ownerUserId;

    @NotEmpty(message="用户昵称不能为空")
    @ApiModelProperty(value = "用户昵称",required = true)
    private String ownerUserNickname;

    @ApiModelProperty(value = "用户头像")
    private String ownerUserAvatar;

    @NotNull(message="评分不能为空")
    @ApiModelProperty(value = "评分",required = true)
    private Double rate;

    @NotNull(message="所在楼层不能为空")
    @ApiModelProperty(value = "所在楼层，排序，升序",required = true)
    private Integer floor;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
