package scatter.comment.pojo.hierarchical.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 评论点赞表
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comment_hierarchical_star")
@ApiModel(value="CommentHierarchicalStar对象", description="评论点赞表")
public class CommentHierarchicalStar extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMMENTHIERARCHICALSTAR_BY_ID = "trans_commenthierarchicalstar_by_id_scatter.comment.pojo.hierarchical.po";

    @ApiModelProperty(value = "评论表id")
    private String commentHierarchicalId;

    @ApiModelProperty(value = "点赞时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "点赞人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "点赞人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "所在楼层，排序，升序")
    private Integer floor;

}
