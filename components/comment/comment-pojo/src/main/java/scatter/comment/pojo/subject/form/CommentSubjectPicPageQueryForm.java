package scatter.comment.pojo.subject.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 主体评论图片分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体评论图片分页表单对象")
public class CommentSubjectPicPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "评论主体评论表id")
    private String commentSubjectId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @OrderBy(asc = true)
    @ApiModelProperty(value = "排序，默认升序")
    private Integer seq;

}
