package scatter.comment.pojo.hierarchical.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.OrderBy;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;


/**
 * <p>
 * 评论点赞分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="评论点赞分页表单对象")
public class CommentHierarchicalStarPageQueryForm1 extends BasePageQueryForm {

    @NotEmpty(message = "评论表id不能为空")
    @ApiModelProperty(value = "评论表id",required = true)
    private String commentHierarchicalId;

    @ApiModelProperty(value = "点赞人用户id")
    private String ownerUserId;


}
