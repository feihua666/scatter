package scatter.comment.pojo.subject.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 22:23
 */
@Setter
@Getter
@ApiModel(value="主体评论取消点赞表单对象")
public class CommentSubjectStarCancelForm extends BaseForm {

	@NotEmpty(message = "主体评论表id不能为空")
	@ApiModelProperty(value = "主体评论表id",required = true)
	private String commentSubjectId;


}
