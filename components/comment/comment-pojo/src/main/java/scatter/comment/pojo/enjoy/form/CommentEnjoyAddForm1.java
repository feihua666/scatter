package scatter.comment.pojo.enjoy.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 主体喜欢添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="主体喜欢添加表单对象")
public class CommentEnjoyAddForm1 extends BaseAddForm {

    @NotEmpty(message = "主体id不能为空")
    @ApiModelProperty(value = "主体id",required = true)
    private String subjectId;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

}
