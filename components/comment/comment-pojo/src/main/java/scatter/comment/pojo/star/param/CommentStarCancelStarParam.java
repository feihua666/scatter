package scatter.comment.pojo.star.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 取消点赞参数对象
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 18:45
 */
@Getter
@Setter
public class CommentStarCancelStarParam extends BaseParam {

	@NotEmpty(message="主体id")
	@ApiModelProperty(value = "主体id",required = true)
	private String subjectId;

	@NotEmpty(message="点赞人用户id不能为空")
	@ApiModelProperty(value = "点赞人用户id",required = true)
	private String ownerUserId;

	@ApiModelProperty(value = "分组标识")
	private String groupFlag;
}
