package scatter.sensitive.rest.word.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.sensitive.rest.SensitiveConfiguration;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.rest.word.service.ISensitiveWordService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 敏感词表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@RestController
@RequestMapping(SensitiveConfiguration.CONTROLLER_BASE_PATH + "/inner/word/sensitive-word")
public class SensitiveWordInnerController extends BaseInnerController<SensitiveWord> {
 @Autowired
 private ISensitiveWordService sensitiveWordService;

 public ISensitiveWordService getService(){
     return sensitiveWordService;
 }
}
