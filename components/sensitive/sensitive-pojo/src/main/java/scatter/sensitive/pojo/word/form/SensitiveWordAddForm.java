package scatter.sensitive.pojo.word.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 敏感词添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="敏感词添加表单对象")
public class SensitiveWordAddForm extends BaseAddForm {

    @NotEmpty(message="敏感词不能为空")
    @ApiModelProperty(value = "敏感词，模糊查询",required = true)
    private String word;

    @ApiModelProperty(value = "替换词")
    private String replaceWord;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
