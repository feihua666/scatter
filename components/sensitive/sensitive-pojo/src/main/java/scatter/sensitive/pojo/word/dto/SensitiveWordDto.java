package scatter.sensitive.pojo.word.dto;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.dto.BaseDto;

/**
 * <p>
 * 敏感词 dto
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 14:58
 */
@Setter
@Getter
public class SensitiveWordDto extends BaseDto {
	/**
	 * 返回的过滤后的敏感词内容
	 */
	private String content;
}
