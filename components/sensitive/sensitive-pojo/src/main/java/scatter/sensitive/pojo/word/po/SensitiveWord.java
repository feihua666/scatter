package scatter.sensitive.pojo.word.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 敏感词表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_sensitive_word")
@ApiModel(value="SensitiveWord对象", description="敏感词表")
public class SensitiveWord extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SENSITIVEWORD_BY_ID = "trans_sensitiveword_by_id_scatter.sensitive.pojo.word.po";

    @ApiModelProperty(value = "敏感词，模糊查询")
    private String word;

    @ApiModelProperty(value = "替换词")
    private String replaceWord;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
