package scatter.sensitive.pojo.word.param;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

/**
 * <p>
 * 敏感词参数
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 14:56
 */
@Getter
@Setter
public class SensitiveWordParam extends BaseParam {
	/**
	 * 需要检测的内容
	 */
	private String content;
}
