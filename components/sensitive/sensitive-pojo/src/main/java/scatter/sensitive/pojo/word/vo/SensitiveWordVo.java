package scatter.sensitive.pojo.word.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 敏感词响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="敏感词响应对象")
public class SensitiveWordVo extends BaseIdVo {

    @ApiModelProperty(value = "敏感词，模糊查询")
    private String word;

    @ApiModelProperty(value = "替换词")
    private String replaceWord;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "类型，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "描述")
    private String remark;

}
