const basePath = '' + '/word/sensitive-word'
const SensitiveWordUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/SensitiveWordSearchList',
        add: '/SensitiveWordAdd',
        update: '/SensitiveWordUpdate',
    }
}
export default SensitiveWordUrl