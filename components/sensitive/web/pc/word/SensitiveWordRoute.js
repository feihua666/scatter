import SensitiveWordUrl from './SensitiveWordUrl.js'

const SensitiveWordRoute = [
    {
        path: SensitiveWordUrl.router.searchList,
        component: () => import('./element/SensitiveWordSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'SensitiveWordSearchList',
            name: '敏感词管理'
        }
    },
    {
        path: SensitiveWordUrl.router.add,
        component: () => import('./element/SensitiveWordAdd'),
        meta: {
            code:'SensitiveWordAdd',
            name: '敏感词添加'
        }
    },
    {
        path: SensitiveWordUrl.router.update,
        component: () => import('./element/SensitiveWordUpdate'),
        meta: {
            code:'SensitiveWordUpdate',
            name: '敏感词修改'
        }
    },
]
export default SensitiveWordRoute