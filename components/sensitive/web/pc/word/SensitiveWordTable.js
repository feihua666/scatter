const SensitiveWordTable = [
    {
        prop: 'word',
        label: '敏感词'
    },
    {
        prop: 'replaceWord',
        label: '替换词'
    },
    {
        prop: 'typeDictId',
        label: '类型'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default SensitiveWordTable