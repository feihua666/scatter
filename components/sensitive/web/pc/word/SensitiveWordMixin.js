import SensitiveWordForm from './SensitiveWordForm.js'
import SensitiveWordTable from './SensitiveWordTable.js'
import SensitiveWordUrl from './SensitiveWordUrl.js'
const SensitiveWordMixin = {
    computed: {
        SensitiveWordFormOptions() {
            return this.$stDynamicFormTools.options(SensitiveWordForm,this.$options.name)
        },
        SensitiveWordTableOptions() {
            return SensitiveWordTable
        },
        SensitiveWordUrl(){
            return SensitiveWordUrl
        }
    },
}
export default SensitiveWordMixin