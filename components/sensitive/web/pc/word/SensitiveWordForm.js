import SensitiveWordUrl from './SensitiveWordUrl.js'
const SensitiveWordForm = [
    {
        SensitiveWordSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'word',
        },
        element:{
            label: '敏感词',
            required: true,
        }
    },
    {
        SensitiveWordSearchList: false,
        field: {
            name: 'replaceWord',
        },
        element:{
            label: '替换词',
        }
    },
    {
        SensitiveWordSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            type: 'selectDict',
            options: {
                groupCode: 'sensitive_word_type'
            },
            required: true,
        }
    },
    {
        SensitiveWordSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default SensitiveWordForm