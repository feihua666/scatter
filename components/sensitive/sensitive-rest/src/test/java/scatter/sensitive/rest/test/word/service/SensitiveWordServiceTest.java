package scatter.sensitive.rest.test.word.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.pojo.word.form.SensitiveWordAddForm;
import scatter.sensitive.pojo.word.form.SensitiveWordUpdateForm;
import scatter.sensitive.pojo.word.form.SensitiveWordPageQueryForm;
import scatter.sensitive.rest.test.word.SensitiveWordSuperTest;
import scatter.sensitive.rest.word.service.ISensitiveWordService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 敏感词 服务测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
public class SensitiveWordServiceTest extends SensitiveWordSuperTest{

    @Autowired
    private ISensitiveWordService sensitiveWordService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<SensitiveWord> pos = sensitiveWordService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}