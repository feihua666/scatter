package scatter.sensitive.rest.test.word;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.pojo.word.form.SensitiveWordAddForm;
import scatter.sensitive.pojo.word.form.SensitiveWordUpdateForm;
import scatter.sensitive.pojo.word.form.SensitiveWordPageQueryForm;
import scatter.sensitive.rest.word.service.ISensitiveWordService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 敏感词 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class SensitiveWordSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ISensitiveWordService sensitiveWordService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return sensitiveWordService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return sensitiveWordService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public SensitiveWord mockPo() {
        return JMockData.mock(SensitiveWord.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public SensitiveWordAddForm mockAddForm() {
        return JMockData.mock(SensitiveWordAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public SensitiveWordUpdateForm mockUpdateForm() {
        return JMockData.mock(SensitiveWordUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public SensitiveWordPageQueryForm mockPageQueryForm() {
        return JMockData.mock(SensitiveWordPageQueryForm.class, mockConfig);
    }
}