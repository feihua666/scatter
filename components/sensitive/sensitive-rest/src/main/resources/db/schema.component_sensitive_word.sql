DROP TABLE IF EXISTS component_sensitive_word;
CREATE TABLE `component_sensitive_word` (
  `id` varchar(20) NOT NULL COMMENT '主键ID',
  `word` varchar(100) NOT NULL COMMENT '敏感词，模糊查询',
  `replace_word` varchar(100) DEFAULT NULL COMMENT '替换词',
  `type_dict_id` varchar(20) NOT NULL COMMENT '类型，字典id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type_dict_id` (`type_dict_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='敏感词表';
