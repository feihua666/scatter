package scatter.sensitive.rest.common;

/**
 * <p>
 * 敏感信息框架
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 14:53
 */
public interface ISensitiveFramework<P> {
	/**
	 * 是否包含敏感信息
	 * @param p
	 * @return
	 */
	boolean isIncludeSensitiveInfo(P p);
}
