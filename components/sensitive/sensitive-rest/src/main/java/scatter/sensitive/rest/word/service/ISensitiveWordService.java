package scatter.sensitive.rest.word.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.common.rest.service.IBaseService;
import scatter.sensitive.pojo.word.form.SensitiveWordAddForm;
import scatter.sensitive.pojo.word.form.SensitiveWordUpdateForm;
import scatter.sensitive.pojo.word.form.SensitiveWordPageQueryForm;
import java.util.List;
/**
 * <p>
 * 敏感词表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface ISensitiveWordService extends IBaseService<SensitiveWord> {


}
