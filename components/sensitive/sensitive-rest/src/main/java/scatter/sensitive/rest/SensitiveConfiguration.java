package scatter.sensitive.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.sensitive.rest.common.ISensitiveWordFramework;
import scatter.sensitive.rest.common.impl.DefaultSensitiveWordFrameworkImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.sensitive.rest.**.mapper")
public class SensitiveConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "sensitive";

    @Bean
    @ConditionalOnMissingBean
    public ISensitiveWordFramework sensitiveWordFramework(){
        return new DefaultSensitiveWordFrameworkImpl();
    }
}
