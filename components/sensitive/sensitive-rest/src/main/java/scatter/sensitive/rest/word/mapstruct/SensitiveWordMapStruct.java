package scatter.sensitive.rest.word.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.pojo.word.vo.SensitiveWordVo;
import scatter.sensitive.pojo.word.form.SensitiveWordAddForm;
import scatter.sensitive.pojo.word.form.SensitiveWordUpdateForm;
import scatter.sensitive.pojo.word.form.SensitiveWordPageQueryForm;

/**
 * <p>
 * 敏感词 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SensitiveWordMapStruct extends IBaseVoMapStruct<SensitiveWord, SensitiveWordVo>,
                                  IBaseAddFormMapStruct<SensitiveWord,SensitiveWordAddForm>,
                                  IBaseUpdateFormMapStruct<SensitiveWord,SensitiveWordUpdateForm>,
                                  IBaseQueryFormMapStruct<SensitiveWord,SensitiveWordPageQueryForm>{
    SensitiveWordMapStruct INSTANCE = Mappers.getMapper( SensitiveWordMapStruct.class );

}
