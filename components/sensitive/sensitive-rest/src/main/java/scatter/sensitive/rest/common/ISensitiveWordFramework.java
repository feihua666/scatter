package scatter.sensitive.rest.common;

import scatter.sensitive.pojo.word.dto.SensitiveWordDto;
import scatter.sensitive.pojo.word.param.SensitiveWordParam;

/**
 * <p>
 * 敏感词框架
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 14:55
 */
public interface ISensitiveWordFramework extends ISensitiveFramework<SensitiveWordParam>{

	/**
	 * 替换敏感信息
	 * @param param
	 * @return 返回替换后的结果
	 */
	SensitiveWordDto replaceSensitiveWord(SensitiveWordParam param);
}
