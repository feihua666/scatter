package scatter.sensitive.rest.word.service.impl;

import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.rest.word.mapper.SensitiveWordMapper;
import scatter.sensitive.rest.word.service.ISensitiveWordService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.sensitive.pojo.word.form.SensitiveWordAddForm;
import scatter.sensitive.pojo.word.form.SensitiveWordUpdateForm;
import scatter.sensitive.pojo.word.form.SensitiveWordPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 敏感词表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class SensitiveWordServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<SensitiveWordMapper, SensitiveWord, SensitiveWordAddForm, SensitiveWordUpdateForm, SensitiveWordPageQueryForm> implements ISensitiveWordService {
    @Override
    public void preAdd(SensitiveWordAddForm addForm,SensitiveWord po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(SensitiveWordUpdateForm updateForm,SensitiveWord po) {
        super.preUpdate(updateForm,po);

    }
}
