package scatter.sensitive.rest.word.mapper;

import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 敏感词表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface SensitiveWordMapper extends IBaseMapper<SensitiveWord> {

}
