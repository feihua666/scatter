package scatter.sensitive.rest.word.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.rest.word.service.ISensitiveWordService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 敏感词翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
public class SensitiveWordTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ISensitiveWordService sensitiveWordService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,SensitiveWord.TRANS_SENSITIVEWORD_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,SensitiveWord.TRANS_SENSITIVEWORD_BY_ID)) {
            SensitiveWord byId = sensitiveWordService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,SensitiveWord.TRANS_SENSITIVEWORD_BY_ID)) {
            return sensitiveWordService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
