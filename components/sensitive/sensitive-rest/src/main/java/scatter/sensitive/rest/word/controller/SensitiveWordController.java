package scatter.sensitive.rest.word.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.sensitive.rest.SensitiveConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.sensitive.pojo.word.po.SensitiveWord;
import scatter.sensitive.pojo.word.vo.SensitiveWordVo;
import scatter.sensitive.pojo.word.form.SensitiveWordAddForm;
import scatter.sensitive.pojo.word.form.SensitiveWordUpdateForm;
import scatter.sensitive.pojo.word.form.SensitiveWordPageQueryForm;
import scatter.sensitive.rest.word.service.ISensitiveWordService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 敏感词表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "敏感词相关接口")
@RestController
@RequestMapping(SensitiveConfiguration.CONTROLLER_BASE_PATH + "/word/sensitive-word")
public class SensitiveWordController extends BaseAddUpdateQueryFormController<SensitiveWord, SensitiveWordVo, SensitiveWordAddForm, SensitiveWordUpdateForm, SensitiveWordPageQueryForm> {
    @Autowired
    private ISensitiveWordService iSensitiveWordService;

    @ApiOperation("添加敏感词")
    @PreAuthorize("hasAuthority('SensitiveWord:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public SensitiveWordVo add(@RequestBody @Valid SensitiveWordAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询敏感词")
    @PreAuthorize("hasAuthority('SensitiveWord:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public SensitiveWordVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除敏感词")
    @PreAuthorize("hasAuthority('SensitiveWord:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新敏感词")
    @PreAuthorize("hasAuthority('SensitiveWord:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public SensitiveWordVo update(@RequestBody @Valid SensitiveWordUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询敏感词")
    @PreAuthorize("hasAuthority('SensitiveWord:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<SensitiveWordVo> getList(SensitiveWordPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询敏感词")
    @PreAuthorize("hasAuthority('SensitiveWord:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<SensitiveWordVo> getPage(SensitiveWordPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
