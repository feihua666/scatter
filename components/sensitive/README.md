# sensitive 敏感信息处理组件

该组件目前用于处理简单的敏感词过滤

# 组件目标
该组件的定位远不止于只实现敏感词过滤，未来如果有其它敏感信息也会迭代到这里，
如：图片鉴黄、视频等多媒体敏感信息过虑等

参见组件生成器的实现： scatter.generator.component.ComponentGenerator