package scatter.schedulequartz.rest.record.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.rest.record.service.ISchedulerRecordService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 任务计划执行记录表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@RestController
@RequestMapping(ScheduleQuartzConfiguration.CONTROLLER_BASE_PATH + "/inner/record/scheduler-record")
public class SchedulerRecordInnerController extends BaseInnerController<SchedulerRecord> {
 @Autowired
 private ISchedulerRecordService schedulerRecordService;

 public ISchedulerRecordService getService(){
     return schedulerRecordService;
 }
}
