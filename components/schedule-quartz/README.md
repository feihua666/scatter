# schedule-quartz 定时任务组件

基于quartz的可视化的定时任务组件  

+ 支持http接口调用及参数自定义
+ 支持spring bean调用
+ 动态添加/删除任务
+ 任务的暂停与恢复


