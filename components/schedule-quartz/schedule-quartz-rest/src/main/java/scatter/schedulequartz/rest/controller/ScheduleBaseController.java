package scatter.schedulequartz.rest.controller;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.rest.controller.SuperController;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.common.rest.exception.BusinessException;
import scatter.schedulequartz.pojo.form.ScheduleForm;
import scatter.schedulequartz.pojo.vo.JobDetailVo;
import scatter.schedulequartz.rest.mapstruct.ScheduleMapStruct;
import scatter.schedulequartz.rest.service.IQuartzJobService;
import scatter.schedulequartz.rest.tools.ScheduleTool;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/4 12:52
 */
public class ScheduleBaseController extends SuperController {
    @Autowired
    private IQuartzJobService iQuartzJobService;
    @Autowired
    private ScheduleMapStruct scheduleMapStruct;
    /**
     * 获取唯一
     * @param scheduleForm
     * @return
     * @throws SchedulerException
     */
    public Scheduler getUniqueScheduler(ScheduleForm scheduleForm) throws SchedulerException {
        List<Scheduler> schedules = iQuartzJobService.schedules(scheduleMapStruct.map(scheduleForm));
        ScheduleTool.assertSchedulerExistAndUnique(schedules);

        Scheduler scheduler = schedules.stream().findFirst().get();
        return scheduler;
    }
}
