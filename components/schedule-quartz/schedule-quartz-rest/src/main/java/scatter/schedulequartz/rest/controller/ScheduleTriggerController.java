package scatter.schedulequartz.rest.controller;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.schedulequartz.pojo.form.JobQueryForm;
import scatter.schedulequartz.pojo.form.NameAndGroupForm;
import scatter.schedulequartz.pojo.form.ScheduleQueryForm;
import scatter.schedulequartz.pojo.form.TriggerQueryForm;
import scatter.schedulequartz.pojo.vo.JobDetailVo;
import scatter.schedulequartz.pojo.vo.ScheduleVo;
import scatter.schedulequartz.pojo.vo.TriggerVo;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.schedulequartz.rest.service.IQuartzJobService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2021/2/2 18:18
 */
@RestController
@RequestMapping(ScheduleQuartzConfiguration.CONTROLLER_BASE_PATH + "/schedule/trigger")
@Api(tags = "任务计划相关接口")
public class ScheduleTriggerController extends ScheduleBaseController {

    @Autowired
    private IQuartzJobService iQuartzJobService;

    @ApiOperation("暂停触发器")
    @PreAuthorize("hasAuthority('schedule:trigger:pause')")
    @PostMapping("/pause")
    @ResponseStatus(HttpStatus.OK)
    public Boolean pause(@RequestBody @Valid NameAndGroupForm addForm, ScheduleQueryForm scheduleQueryForm) throws SchedulerException, ClassNotFoundException {
        List<Scheduler> schedules = iQuartzJobService.schedules(scheduleQueryForm);
        for (Scheduler schedule : schedules) {
            if (schedule.checkExists(TriggerKey.triggerKey(addForm.getName(), addForm.getGroup()))) {
                iQuartzJobService.pauseTrigger(addForm, schedule);
            }
        }
        return true;
    }
    @ApiOperation("恢复触发器")
    @PreAuthorize("hasAuthority('schedule:trigger:resume')")
    @PostMapping("/resume")
    @ResponseStatus(HttpStatus.OK)
    public Boolean resume(@RequestBody @Valid NameAndGroupForm addForm,ScheduleQueryForm scheduleQueryForm) throws SchedulerException, ClassNotFoundException {
        List<Scheduler> schedules = iQuartzJobService.schedules(scheduleQueryForm);
        for (Scheduler schedule : schedules) {
            if (schedule.checkExists(TriggerKey.triggerKey(addForm.getName(),addForm.getGroup()))) {
                iQuartzJobService.resumeTrigger(addForm, schedule);
            }
        }
        return true;
    }

    @ApiOperation("不分页查询触发器")
    @PreAuthorize("hasAuthority('schedule:getTriggerList')")
    @GetMapping("/getTriggerList")
    @ResponseStatus(HttpStatus.OK)
    public List<TriggerVo> getTriggerList(TriggerQueryForm triggerQueryForm, ScheduleQueryForm scheduleQueryForm) throws SchedulerException {
        List<Scheduler> schedules = iQuartzJobService.schedules(scheduleQueryForm);
        List<TriggerVo> result = new ArrayList<>();
        for (Scheduler schedule : schedules) {
            List<TriggerVo> triggers = iQuartzJobService.getTriggers(triggerQueryForm, schedule);
            if (!isEmpty(triggers)) {
                result.addAll(triggers);
            }
        }
        if (isEmpty(result)) {
            throw new BusinessDataNotFoundException("暂无数据");
        }
        return result;
    }
}
