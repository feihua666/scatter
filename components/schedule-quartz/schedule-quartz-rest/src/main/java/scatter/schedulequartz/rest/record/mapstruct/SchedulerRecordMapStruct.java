package scatter.schedulequartz.rest.record.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.schedulequartz.pojo.record.form.SchedulerRecordClearForm;
import scatter.schedulequartz.pojo.record.form.SchedulerRecordPageQueryForm;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.pojo.record.vo.SchedulerRecordVo;

/**
 * <p>
 * 任务计划执行记录 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SchedulerRecordMapStruct extends IBaseVoMapStruct<SchedulerRecord, SchedulerRecordVo>,
                                  IBaseQueryFormMapStruct<SchedulerRecord,SchedulerRecordPageQueryForm>{
    SchedulerRecordMapStruct INSTANCE = Mappers.getMapper( SchedulerRecordMapStruct.class );

    SchedulerRecord clearFormToPo(SchedulerRecordClearForm form);
}
