package scatter.schedulequartz.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.quartz.Scheduler;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.schedulequartz.rest.listener.GlobalJobListener;
import scatter.schedulequartz.rest.listener.GlobalScheduleListener;
import scatter.schedulequartz.rest.listener.GlobalTriggerListener;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */

@ComponentScan
@MapperScan("scatter.schedulequartz.rest.**.mapper")
public class ScheduleQuartzConfiguration implements InitializingBean {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "schedulequartz";

    @Autowired
    private List<Scheduler> schedulerList;

    @Bean
    public GlobalScheduleListener globalScheduleListener() {
        return new GlobalScheduleListener();
    }
    @Bean
    public GlobalJobListener globalJobListener() {
        return new GlobalJobListener();
    }
    @Bean
    public GlobalTriggerListener globalTriggerListener() {
        return new GlobalTriggerListener();
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        for (Scheduler scheduler : schedulerList) {
            scheduler.getListenerManager().addSchedulerListener(globalScheduleListener());
            scheduler.getListenerManager().addJobListener(globalJobListener());
            scheduler.getListenerManager().addTriggerListener(globalTriggerListener());
        }

    }
}
