package scatter.schedulequartz.rest.record.mapper;

import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 任务计划执行记录表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
public interface SchedulerRecordMapper extends IBaseMapper<SchedulerRecord> {

}
