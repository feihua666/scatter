package scatter.schedulequartz.rest.service;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import scatter.schedulequartz.pojo.form.*;
import scatter.schedulequartz.pojo.vo.JobDetailVo;
import scatter.schedulequartz.pojo.vo.ScheduleVo;
import scatter.schedulequartz.pojo.vo.TriggerVo;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/2/2 12:57
 */
public interface IQuartzJobService {

    /**
     * 列出计划
     * @return
     */
    List<ScheduleVo> schedules() throws SchedulerException;

    /**
     * 查询计划
     * @param scheduleQueryForm
     * @return
     * @throws SchedulerException
     */
    List<Scheduler> schedules( ScheduleQueryForm scheduleQueryForm) throws SchedulerException;

    /**
     * 创建job
     * @param addJobForm
     * @return
     * @throws Exception
     */
    boolean addJob(JobCronAddForm addJobForm, Scheduler scheduler) throws ClassNotFoundException, SchedulerException;
    /**
     * 复制 job
     * @param nameAndGroupForm
     * @return
     * @throws Exception
     */
    boolean copyJob(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws ClassNotFoundException, SchedulerException;

    /**
     * 执行一次任务
     * 重新安排一个触发器，执行一次任务
     * @param nameAndGroupForm
     * @param scheduler
     * @return
     */
    boolean executeOnce(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws SchedulerException;

    /**
     * 暂停job
     * @param nameAndGroupForm
     * @param scheduler
     * @return
     * @throws SchedulerException
     */
    boolean pauseJob(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws SchedulerException;
    /**
     * 暂停触发器
     * @param nameAndGroupForm
     * @param scheduler
     * @return
     * @throws SchedulerException
     */
    boolean pauseTrigger(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws SchedulerException;

    /**
     * 恢复job，job暂停后可以恢复
     * @param nameAndGroupForm
     * @param scheduler
     * @return
     * @throws SchedulerException
     */
    boolean resumeJob(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws SchedulerException;
    /**
     * 恢复触发器，触发器暂停后可以恢复
     * @param nameAndGroupForm
     * @param scheduler
     * @return
     * @throws SchedulerException
     */
    boolean resumeTrigger(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws SchedulerException;


    /**
     * job 更新
     * @param updateJobForm
     * @return
     * @throws Exception
     */
    boolean updateJob(JobCronUpdateForm updateJobForm, Scheduler scheduler) throws SchedulerException, ClassNotFoundException;


    /**
     * job 删除
     * @param nameAndGroupForm
     * @param scheduler
     * @return
     */
    boolean deleteJob(NameAndGroupForm nameAndGroupForm, Scheduler scheduler) throws SchedulerException;


    /**
     * 启动任务计划
     * @throws Exception
     */
    boolean startSchedule(Scheduler scheduler) throws SchedulerException;
    /**
     * 挂起任务计划
     * @throws Exception
     */
    boolean standbySchedule(Scheduler scheduler) throws SchedulerException;

    /**
     * 停止任务计划
     * @param waitForJobsToComplete 是否等待任务执行完成
     * @param scheduler
     * @return
     */
    boolean shutdownSchedule(boolean waitForJobsToComplete,Scheduler scheduler) throws SchedulerException;

    /**
     * 查询job
     * @param jobQueryForm
     * @return
     * @throws Exception
     */
    List<JobDetailVo> getJobs(JobQueryForm jobQueryForm, Scheduler scheduler) throws SchedulerException;

    /**
     * 查询触发器
     * @param triggerQueryForm
     * @return
     * @throws Exception
     */
    List<TriggerVo> getTriggers(TriggerQueryForm triggerQueryForm, Scheduler scheduler) throws SchedulerException;

}
