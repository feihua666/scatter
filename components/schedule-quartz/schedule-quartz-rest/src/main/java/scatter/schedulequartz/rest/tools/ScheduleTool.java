package scatter.schedulequartz.rest.tools;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.schedulequartz.pojo.form.ScheduleForm;
import scatter.schedulequartz.pojo.vo.JobDetailVo;

import java.util.List;

import static cn.hutool.core.collection.CollUtil.isEmpty;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-11 11:50
 */
public class ScheduleTool {


	/**
	 *
	 * @param schedules
	 */
	public static void assertSchedulerExistAndUnique(List<Scheduler> schedules) {
		if (isEmpty(schedules)) {
			// 如果是集群多实例模式可能任务计划不存在，斩时未支持
			throw new BusinessDataNotFoundException("任务计划不存在");
		}
		if (schedules.size() >1) {
			throw new RuntimeException("任务计划存在多个，这可能是系统内部错误");
		}
	}
	/**
	 *
	 * @param jobDetailVos
	 */
	public static void assertJobExistAndUnique(List jobDetailVos) {
		if (isEmpty(jobDetailVos)) {
			// 如果是集群多实例模式可能任务计划不存在，斩时未支持
			throw new BusinessDataNotFoundException("任务不存在");
		}
		if (jobDetailVos.size() >1) {
			throw new RuntimeException("任务存在多个，这可能是系统内部错误");
		}
	}
	/**
	 *
	 * @param triggers
	 */
	public static void assertTriggerExistAndUnique(List triggers) {
		if (isEmpty(triggers)) {
			// 如果是集群多实例模式可能任务计划不存在，斩时未支持
			throw new BusinessDataNotFoundException("触发器不存在");
		}
		if (triggers.size() >1) {
			throw new RuntimeException("触发器存在多个，这可能是系统内部错误");
		}
	}
}
