package scatter.schedulequartz.rest.common;

import lombok.extern.slf4j.Slf4j;
import org.quartz.ObjectAlreadyExistsException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import scatter.common.pojo.vo.ErrorVo;
import scatter.common.rest.advise.GlobalExceptionAdvice;
import scatter.common.rest.tools.InterfaceTool;

import javax.servlet.http.HttpServletRequest;

/**
 * 统一异常处理
 * Created by yangwei
 * Created at 2020/6/15 11:42
 */
@RestControllerAdvice
@Slf4j
@Order(10)
public class SchedulerRestExceptionHandlerAdvice implements InterfaceTool {

    /**
     * 任务计划重复 key 出现的异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(ObjectAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorVo handleRObjectAlreadyExistsException(HttpServletRequest request, ObjectAlreadyExistsException ex) {
        return GlobalExceptionAdvice.createRM(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), null, ex);
    }
}
