package scatter.schedulequartz.rest.record.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseQueryFormController;
import scatter.schedulequartz.pojo.record.form.SchedulerRecordClearForm;
import scatter.schedulequartz.pojo.record.form.SchedulerRecordPageQueryForm;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.pojo.record.vo.SchedulerRecordVo;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.schedulequartz.rest.record.service.ISchedulerRecordService;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 任务计划执行记录表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Api(tags = "任务计划执行记录相关接口")
@RestController
@RequestMapping(ScheduleQuartzConfiguration.CONTROLLER_BASE_PATH + "/record/scheduler-record")
public class SchedulerRecordController extends BaseQueryFormController<SchedulerRecord, SchedulerRecordVo, SchedulerRecordPageQueryForm> {
    @Autowired
    private ISchedulerRecordService iSchedulerRecordService;

    @ApiOperation("根据ID查询任务计划执行记录")
    @PreAuthorize("hasAuthority('SchedulerRecord:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public SchedulerRecordVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除任务计划执行记录")
    @PreAuthorize("hasAuthority('SchedulerRecord:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("不分页查询任务计划执行记录")
    @PreAuthorize("hasAuthority('SchedulerRecord:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<SchedulerRecordVo> getList(SchedulerRecordPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询任务计划执行记录")
    @PreAuthorize("hasAuthority('SchedulerRecord:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<SchedulerRecordVo> getPage(SchedulerRecordPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("清理日志")
    @PreAuthorize("hasAuthority('SchedulerRecord:clear')")
    @PostMapping("/clear")
    @ResponseStatus(HttpStatus.OK)
    public Boolean clear(@RequestBody @Valid SchedulerRecordClearForm form) {
        return iSchedulerRecordService.clear(form);
    }

}
