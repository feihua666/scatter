package scatter.schedulequartz.rest.controller;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.schedulequartz.pojo.form.ScheduleForm;
import scatter.schedulequartz.pojo.form.ScheduleQueryForm;
import scatter.schedulequartz.pojo.form.ScheduleShutdownForm;
import scatter.schedulequartz.pojo.vo.ScheduleVo;
import scatter.schedulequartz.rest.ScheduleQuartzConfiguration;
import scatter.schedulequartz.rest.mapstruct.ScheduleMapStruct;
import scatter.schedulequartz.rest.service.IQuartzJobService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2021/2/2 18:18
 */
@RestController
@RequestMapping(ScheduleQuartzConfiguration.CONTROLLER_BASE_PATH + "/schedule")
@Api(tags = "任务计划相关接口")
public class ScheduleController extends ScheduleBaseController {

    @Autowired
    private IQuartzJobService iQuartzJobService;
    @Autowired
    private ScheduleMapStruct scheduleMapStruct;

    @ApiOperation("挂起任务计划")
    @PreAuthorize("hasAuthority('schedule:standby')")
    @PostMapping("/standby")
    @ResponseStatus(HttpStatus.OK)
    public Boolean standby(@RequestBody @Valid ScheduleForm scheduleForm) throws SchedulerException, ClassNotFoundException {
        Scheduler scheduler = getUniqueScheduler(scheduleForm);
        return iQuartzJobService.standbySchedule(scheduler);
    }

    @ApiOperation("启动任务计划")
    @PreAuthorize("hasAuthority('schedule:start')")
    @PostMapping("/start")
    @ResponseStatus(HttpStatus.OK)
    public Boolean start(@RequestBody @Valid ScheduleForm scheduleForm) throws SchedulerException, ClassNotFoundException {

        Scheduler scheduler = getUniqueScheduler(scheduleForm);
        return iQuartzJobService.startSchedule(scheduler);
    }

    @ApiOperation("停止任务计划")
    @PreAuthorize("hasAuthority('schedule:shutdown')")
    @PostMapping("/shutdown")
    @ResponseStatus(HttpStatus.OK)
    public Boolean shutdown(@RequestBody @Valid ScheduleShutdownForm scheduleShutdownForm) throws SchedulerException, ClassNotFoundException {

        Scheduler scheduler = getUniqueScheduler(scheduleShutdownForm);
        return iQuartzJobService.shutdownSchedule(Optional.ofNullable(scheduleShutdownForm.getIsWaitForJobsToComplete()).orElse(false),scheduler);
    }

    @ApiOperation("不分页查询任务计划")
    @PreAuthorize("hasAuthority('schedule:getScheduleList')")
    @GetMapping("/getScheduleList")
    @ResponseStatus(HttpStatus.OK)
    public List<ScheduleVo> getList(ScheduleQueryForm scheduleQueryForm) throws SchedulerException {
        List<ScheduleVo> schedules = iQuartzJobService.schedules();
        schedules = schedules.stream().filter(item ->
                StrUtil.isEmpty(scheduleQueryForm.getSchedulerName()) || StrUtil.contains(item.getSchedulerName(),scheduleQueryForm.getSchedulerName())
        ).filter(item ->
                StrUtil.isEmpty(scheduleQueryForm.getSchedulerInstanceId()) || StrUtil.contains(item.getSchedulerInstanceId(),scheduleQueryForm.getSchedulerInstanceId())
        ).collect(Collectors.toList());

        if (isEmpty(schedules)) {
            throw new BusinessDataNotFoundException("暂无数据");
        }

        return schedules;
    }

}
