package scatter.schedulequartz.rest.record.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import scatter.common.rest.service.IBaseService;
import scatter.schedulequartz.pojo.form.NameAndGroupForm;
import scatter.schedulequartz.pojo.record.form.SchedulerRecordClearForm;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.rest.record.mapstruct.SchedulerRecordMapStruct;

/**
 * <p>
 * 任务计划执行记录表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
public interface ISchedulerRecordService extends IBaseService<SchedulerRecord> {

	/**
	 * 是否存在执行中的任务
	 * 注意这里没有考虑实例情况
	 * @param nameAndGroupForm
	 * @return
	 */
	public boolean existRunning(NameAndGroupForm nameAndGroupForm);

	/**
	 * 任务开始记录
	 * @param context
	 * @return
	 */
	public String addStartJobRecord(JobExecutionContext context) throws SchedulerException;

	/**
	 * 任务执行完成记录更新
	 * @param recordId
	 * @param result
	 * @param statusDictId
	 * @return
	 */
	public boolean endJobUpdateRecord(String recordId,String result,String statusDictId);

	/**
	 * 任务冲突直接记录
	 * @param context
	 * @param result
	 * @return
	 */
	public boolean jobConflictOverRecord(JobExecutionContext context,String result) throws SchedulerException;

	/**
	 * 清理
	 * @param form
	 * @return
	 */
	default public boolean clear(SchedulerRecordClearForm form){
		SchedulerRecord schedulerRecord = SchedulerRecordMapStruct.INSTANCE.clearFormToPo(form);

		return remove(Wrappers.query(schedulerRecord).lambda().le(SchedulerRecord::getCreateAt,form.getBeforeAt()));
	}
}
