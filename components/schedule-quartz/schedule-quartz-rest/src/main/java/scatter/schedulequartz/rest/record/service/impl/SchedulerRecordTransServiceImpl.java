package scatter.schedulequartz.rest.record.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.rest.record.service.ISchedulerRecordService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 任务计划执行记录翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Component
public class SchedulerRecordTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ISchedulerRecordService schedulerRecordService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,SchedulerRecord.TRANS_SCHEDULERRECORD_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,SchedulerRecord.TRANS_SCHEDULERRECORD_BY_ID)) {
            SchedulerRecord byId = schedulerRecordService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,SchedulerRecord.TRANS_SCHEDULERRECORD_BY_ID)) {
            return schedulerRecordService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
