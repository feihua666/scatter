DROP TABLE IF EXISTS component_scheduler_record;
CREATE TABLE `component_scheduler_record` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `scheduler_name` varchar(255) NOT NULL COMMENT 'schedulerName',
  `schedulerInstanceId` varchar(255) NOT NULL COMMENT 'schedulerInstanceId',
  `job_name` varchar(50) NOT NULL COMMENT '任务名称',
  `job_group` varchar(50) NOT NULL COMMENT '任务组',
  `params` text COMMENT '执行参数，json',
  `result` text COMMENT '执行结果',
  `status_dict_id` varchar(20) NOT NULL COMMENT '执行状态，字典id',
  `start_at` datetime NOT NULL COMMENT '开始时间',
  `end_at` datetime DEFAULT NULL COMMENT '结束时间',
  `trace_id` varchar(100) NULL DEFAULT NULL COMMENT '链路追踪id',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`,`group`,`status_dict_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务计划执行记录表';
