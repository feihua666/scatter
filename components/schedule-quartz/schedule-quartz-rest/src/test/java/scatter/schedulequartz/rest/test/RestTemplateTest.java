package scatter.schedulequartz.rest.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

/**
 * Created by yangwei
 * Created at 2021/2/4 10:35
 */
@SpringBootTest
public class RestTemplateTest {

    @Autowired
    private RestTemplate template;

    @Test
    public void test() {
        String forObject = template.getForObject("http://baidu.com", String.class);
        System.out.println(forObject);
    }
}
