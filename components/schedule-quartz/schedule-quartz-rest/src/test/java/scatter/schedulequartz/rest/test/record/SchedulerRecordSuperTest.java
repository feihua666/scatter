package scatter.schedulequartz.rest.test.record;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.pojo.record.form.SchedulerRecordPageQueryForm;
import scatter.schedulequartz.rest.record.service.ISchedulerRecordService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 任务计划执行记录 测试类基类
* </p>
*
* @author yw
* @since 2021-10-11
*/
public class SchedulerRecordSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ISchedulerRecordService schedulerRecordService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return schedulerRecordService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return schedulerRecordService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public SchedulerRecord mockPo() {
        return JMockData.mock(SchedulerRecord.class, mockConfig);
    }


    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public SchedulerRecordPageQueryForm mockPageQueryForm() {
        return JMockData.mock(SchedulerRecordPageQueryForm.class, mockConfig);
    }
}