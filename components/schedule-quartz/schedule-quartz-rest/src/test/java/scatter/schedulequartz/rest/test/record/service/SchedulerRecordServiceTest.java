package scatter.schedulequartz.rest.test.record.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.schedulequartz.pojo.record.po.SchedulerRecord;
import scatter.schedulequartz.rest.record.service.ISchedulerRecordService;
import scatter.schedulequartz.rest.test.record.SchedulerRecordSuperTest;

import java.util.List;
/**
* <p>
* 任务计划执行记录 服务测试类
* </p>
*
* @author yw
* @since 2021-10-11
*/
@SpringBootTest
public class SchedulerRecordServiceTest extends SchedulerRecordSuperTest{

    @Autowired
    private ISchedulerRecordService schedulerRecordService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<SchedulerRecord> pos = schedulerRecordService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}