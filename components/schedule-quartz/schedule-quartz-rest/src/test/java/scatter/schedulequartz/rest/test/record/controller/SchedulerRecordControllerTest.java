package scatter.schedulequartz.rest.test.record.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import scatter.schedulequartz.rest.test.record.SchedulerRecordSuperTest;
/**
* <p>
* 任务计划执行记录 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-10-11
*/
@SpringBootTest
@AutoConfigureMockMvc
public class SchedulerRecordControllerTest extends SchedulerRecordSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }
}