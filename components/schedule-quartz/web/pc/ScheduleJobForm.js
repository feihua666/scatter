import ScheduleUrl from './ScheduleUrl.js'
const ScheduleJobForm = [
    {
        ScheduleJobSearchList:{
            element: {
                type: 'text',
                options: null,
                required: false,
            }
        },
        field: {
            name: 'schedulerName'
        },
        element:{
            type: 'select',
            options: {
                datas: ScheduleUrl.searchList,
                optionProp:{
                    value: 'schedulerName',
                    label: 'schedulerName'
                }
            },
            required: true,
            label: '任务计划名称'
        }
    },
    {
        ScheduleJobSearchList:{
            element: {
                type: 'text',
                options: null,
                required: false
            }
        },
        field: {
            name: 'schedulerInstanceId'
        },
        element:{
            type: 'select',
            options: {
                datas: ScheduleUrl.searchList,
                optionProp:{
                    value: 'schedulerInstanceId',
                    label: 'schedulerInstanceId'
                }
            },
            required: true,
            label: '任务计划实例id'
        }
    },
    {
        ScheduleJobSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '任务名称',
            required: true,
        }
    },
    {
        ScheduleJobSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'group'
        },
        element:{
            label: '任务组',
            required: true,
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'cronExpression'
        },
        element:{
            type: 'cronInput',
            label: 'cron表达式',
            required: true,
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'jobClassType',
            value: 'custom_class'
        },
        element:{
            label: '任务类型',
            type: 'radioGroup',
            required: true,
            options: {
                datas: [
                    {
                        id: 'custom_class',
                        name: '自定义类名'
                    },
                    {
                        id: 'http_job',
                        name: 'http请求任务'
                    },
                    {
                        id: 'spring_bean',
                        name: '容器bean'
                    },

                ]
            }

        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'jobClassName'
        },
        element:{
            label: '任务类名称',
            required: ({form})=>{return form.jobClassType == 'custom_class'},
            show: ({form})=>{return form.jobClassType == 'custom_class'},
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'httpUrl',
            value: 'http://'
        },
        element:{
            label: '请求地址',
            isBlock: true,
            required: ({form})=>{return form.jobClassType == 'http_job'},
            show: ({form})=>{return form.jobClassType == 'http_job'},
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'httpMethod',
            value: 'get'
        },
        element:{
            label: '请求方法',
            tips: 'get post put delete等',
            required: ({form})=>{return form.jobClassType == 'http_job'},
            show: ({form})=>{return form.jobClassType == 'http_job'},
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'httpHeaders'
        },
        element:{
            label: '请求头信息',
            type: 'textarea',
            isBlock: true,
            show: ({form})=>{return form.jobClassType == 'http_job'},
            tips: '请严格按json填写，对象格式'
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'httpParams'
        },
        element:{
            label: '请求参数',
            type: 'textarea',
            isBlock: true,
            show: ({form})=>{return form.jobClassType == 'http_job'},
            tips: '请严格按json填写，对象格式'
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'beanName'
        },
        element:{
            label: 'bean名称',
            required: ({form})=>{return form.jobClassType == 'spring_bean'},
            show: ({form})=>{return form.jobClassType == 'spring_bean'},
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'beanMethodName'
        },
        element:{
            label: 'bean方法名',
            required: ({form})=>{return form.jobClassType == 'spring_bean'},
            show: ({form})=>{return form.jobClassType == 'spring_bean'},
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'beanMethodParams'
        },
        element:{
            label: 'bean方法参数',
            type: 'textarea',
            isBlock: true,
            show: ({form})=>{return form.jobClassType == 'spring_bean'},
            tips: '请严格按json填写，数组格式'
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'description'
        },
        element:{
            isBlock: true,
            required: true,
            label: '描述信息'
        }
    },
    {
        ScheduleJobSearchList: false,
        field: {
            name: 'dataMap'
        },
        element:{
            isBlock: true,
            type: 'textarea',
            label: '额外数据',
            tips: '请严格按json填写，对象格式'
        }
    },
]
export default ScheduleJobForm