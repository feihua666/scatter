import ScheduleTriggerUrl from './ScheduleTriggerUrl.js'

const ScheduleTriggerRoute = [
    {
        path: ScheduleTriggerUrl.router.searchList,
        component: () => import('./element/ScheduleTriggerSearchList'),
        meta: {
            code:'ScheduleTriggerSearchList',
            name: '触发器管理'
        }
    },
]
export default ScheduleTriggerRoute