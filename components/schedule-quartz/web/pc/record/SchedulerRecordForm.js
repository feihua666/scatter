import SchedulerRecordUrl from './SchedulerRecordUrl.js'
import ScheduleUrl from "../ScheduleUrl";
const SchedulerRecordForm = [
    {
        SchedulerRecordSearchList: {
            element: {
                type: 'text',
                options: null,
                required: false
            }
        },

        field: {
            name: 'schedulerName',
        },
        element:{
            type: 'select',
            options: {
                datas: ScheduleUrl.searchList,
                optionProp:{
                    value: 'schedulerName',
                    label: 'schedulerName'
                }
            },
            label: '任务计划名称',
            required: false,
        }
    },
    {
        SchedulerRecordSearchList: {
            element: {
                type: 'text',
                options: null,
                required: false
            }
        },
        field: {
            name: 'schedulerInstanceId',
        },
        element:{
            type: 'select',
            options: {
                datas: ScheduleUrl.searchList,
                optionProp:{
                    value: 'schedulerInstanceId',
                    label: 'schedulerInstanceId'
                }
            },
            label: '任务计划实例id',
            required: false,
        }
    },
    {
        SchedulerRecordSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'jobName',
        },
        element:{
            label: '任务名称',
            required: false,
        }
    },
    {
        SchedulerRecordSearchList: {
            element: {
                required: false
            }
        },

        field: {
            name: 'jobGroup',
        },
        element:{
            label: '任务组',
            required: false,
        }
    },
    {
        SchedulerRecordSearchList: false,

        field: {
            name: 'beforeAt',
        },
        element:{
            label: '该时间以前的数据将被清除',
            type: 'datetime',
            options: {
                pickerOptions: {
                    shortcuts: [{
                        text: '今天',
                        onClick(picker) {
                            picker.$emit('pick', new Date());
                        }
                    }, {
                        text: '昨天',
                        onClick(picker) {
                            const date = new Date();
                            date.setTime(date.getTime() - 3600 * 1000 * 24);
                            picker.$emit('pick', date);
                        }
                    }, {
                        text: '一周前',
                        onClick(picker) {
                            const date = new Date();
                            date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
                            picker.$emit('pick', date);
                        }
                    }, {
                        text: '一月前',
                        onClick(picker) {
                            const date = new Date();
                            date.setTime(date.getTime() - 3600 * 1000 * 24 * 30);
                            picker.$emit('pick', date);
                        }
                    }
                        , {
                            text: '一年前',
                            onClick(picker) {
                                const date = new Date();
                                date.setTime(date.getTime() - 3600 * 1000 * 24 * 365);
                                picker.$emit('pick', date);
                            }
                        }
                    ]
                }
            },
            required: true,
        }
    },
    {
        SchedulerRecordSearchList: {
            element:{
                required: false
            }
        },
        SchedulerRecordClear: false,
        field: {
            name: 'statusDictId',
        },
        element:{
            label: '执行状态',
            required: false,
            type: 'selectDict',
            options:{
                groupCode: 'scheduler_record_status'
            }
        }
    },


]
export default SchedulerRecordForm