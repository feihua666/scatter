import SchedulerRecordUrl from './SchedulerRecordUrl.js'

const SchedulerRecordRoute = [
    {
        path: SchedulerRecordUrl.router.searchList,
        component: () => import('./element/SchedulerRecordSearchList'),
        meta: {
            root: true,
            keepAlive: false,
            code:'SchedulerRecordSearchList',
            name: '任务计划执行记录管理'
        }
    },
    {
        path: SchedulerRecordUrl.router.clear,
        component: () => import('./element/SchedulerRecordClear.vue'),
        meta: {
            root: true,
            keepAlive: false,
            code:'SchedulerRecordClear',
            name: '任务计划执行记录清理'
        }
    },
]
export default SchedulerRecordRoute