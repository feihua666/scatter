const SchedulerRecordTable = [
    {
        prop: 'schedulerName',
        label: '任务计划名称'
    },
    {
        prop: 'schedulerInstanceId',
        label: '任务计划实例Id',
        showOverflowTooltip: true
    },
    {
        prop: 'jobName',
        label: '任务名称'
    },
    {
        prop: 'jobGroup',
        label: '任务组'
    },
    {
        prop: 'params',
        label: '执行参数',
        showOverflowTooltip: true
    },
    {
        prop: 'result',
        label: '执行结果',
        showOverflowTooltip: true
    },
    {
        prop: 'statusDictName',
        label: '执行状态'
    },
    {
        prop: 'startAt',
        label: '开始时间'
    },
    {
        prop: 'endAt',
        label: '结束时间'
    },
    {
        prop: 'traceId',
        label: 'traceId'
    },
]
export default SchedulerRecordTable