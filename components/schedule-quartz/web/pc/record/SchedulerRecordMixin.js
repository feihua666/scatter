import SchedulerRecordForm from './SchedulerRecordForm.js'
import SchedulerRecordTable from './SchedulerRecordTable.js'
import SchedulerRecordUrl from './SchedulerRecordUrl.js'
const SchedulerRecordMixin = {
    computed: {
        SchedulerRecordFormOptions() {
            return this.$stDynamicFormTools.options(SchedulerRecordForm,this.$options.name)
        },
        SchedulerRecordTableOptions() {
            return SchedulerRecordTable
        },
        SchedulerRecordUrl(){
            return SchedulerRecordUrl
        }
    },
}
export default SchedulerRecordMixin