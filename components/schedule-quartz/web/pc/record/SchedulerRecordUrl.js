const basePath = '' + '/record/scheduler-record'
const SchedulerRecordUrl = {
    searchList: basePath + '/getPage',
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    clear: basePath + '/clear',
    router: {
        searchList: '/SchedulerRecordSearchList',
        clear: '/SchedulerRecordClear',
    }
}
export default SchedulerRecordUrl