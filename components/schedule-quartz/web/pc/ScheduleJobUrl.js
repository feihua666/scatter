const basePath = '' + '/schedule/job'
const ScheduleJobUrl = {
    searchList: basePath + '/getJobList',
    get: basePath,
    add: basePath,
    update: basePath,
    delete: basePath,
    pause: basePath + '/pause',
    resume: basePath + '/resume',
    executeOnce: basePath + '/executeOnce',
    copy: basePath + '/copy',
    router: {
        searchList: '/ScheduleJobSearchList',
        add: '/ScheduleJobAdd',
        update: '/ScheduleJobUpdate',
    }
}
export default ScheduleJobUrl