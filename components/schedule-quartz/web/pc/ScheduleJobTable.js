const ScheduleJobTable = [
    {
        type: 'expand'
    },
    {
        prop: 'schedulerName',
        label: '任务计划名称'
    },
    {
        prop: 'schedulerInstanceId',
        label: '任务计划实例id',
        showInDetail: true
    },
    {
        prop: 'name',
        label: '任务名称'
    },
    {
        prop: 'group',
        label: '任务组'
    },
    {
        prop: 'cronExpression',
        label: 'cronExpression'
    },
    {
        prop: 'isDurable',
        label: '如果没有关联触发器是否持久化',
        showInDetail: true
    },
    {
        prop: 'isPersistJobDataAfterExecution',
        label: '执行完成是否持久化',
        showInDetail: true
    },
    {
        prop: 'isConcurrentExectionDisallowed',
        label: '是否不允许并行',
        showInDetail: true
    },
    {
        prop: 'isRecovery',
        label: '是否可恢复'
    },
    {
        prop: 'jobClassName',
        label: '类名称',
        showOverflowTooltip: true
    },
    {
        prop: 'description',
        label: '描述'
    },
]
export default ScheduleJobTable