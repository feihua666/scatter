const ScheduleTable = [
    {
        type: 'expand'
    },
    {
        prop: 'schedulerName',
        label: '任务计划名称'
    },
    {
        prop: 'schedulerInstanceId',
        label: '任务计划实例id'
    },
    {
        prop: 'isStarted',
        label: '是否开启'
    },
    {
        prop: 'isInStandbyMode',
        label: '是否挂起'
    },
    {
        prop: 'isShutdown',
        label: '是否停止'
    },
    {
        prop: 'scheduleMetaData.startAt',
        label: '启动时间'
    },
    {
        prop: 'scheduleMetaData.version',
        label: '版本'
    },
    {
        prop: 'scheduleMetaData.schedulerClassName',
        label: '任务计划实例类',
        showInDetail: true
    },
    {
        prop: 'scheduleMetaData.numberOfJobsExecuted',
        label: '已执行任务数',
        showInDetail: true
    },
    {
        prop: 'scheduleMetaData.jobStoreClassName',
        label: '任务存储类',
        showInDetail: true
    },
    {
        prop: 'scheduleMetaData.isJobStoreSupportsPersistence',
        label: '任务存储是否支持持久化',
        showInDetail: true
    },
    {
        prop: 'scheduleMetaData.isJobStoreSupportsPersistence',
        label: '任务存储是否集群模式',
        showInDetail: true
    },
    {
        prop: 'scheduleMetaData.threadPoolClassName',
        label: '线程池类',
        showInDetail: true
    },
    {
        prop: 'scheduleMetaData.threadPoolSize',
        label: '当前线程池线程数量',
        showInDetail: true
    },
]
export default ScheduleTable