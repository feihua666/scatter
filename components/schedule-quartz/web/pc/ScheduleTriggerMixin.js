import ScheduleTriggerForm from './ScheduleTriggerForm.js'
import ScheduleTriggerTable from './ScheduleTriggerTable.js'
import ScheduleTriggerUrl from './ScheduleTriggerUrl.js'

const ScheduleTriggerMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(ScheduleTriggerForm,this.$options.name)
        },
        computedTableOptions() {
            return ScheduleTriggerTable
        },
        computedUrl(){
            return ScheduleTriggerUrl
        }
    },
}
export default ScheduleTriggerMixin