import ScheduleJobForm from './ScheduleJobForm.js'
import ScheduleJobTable from './ScheduleJobTable.js'
import ScheduleJobUrl from './ScheduleJobUrl.js'

let formatAttr = ['httpHeaders','httpParams','beanMethodParams','dataMap']
const ScheduleJobMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(ScheduleJobForm,this.$options.name)
        },
        computedTableOptions() {
            return ScheduleJobTable
        },
        computedUrl(){
            return ScheduleJobUrl
        }
    },
    methods: {
        formDataFormat(data) {
            formatAttr.forEach(item => {
                if (data[item]) {
                    data[item] = JSON.parse(data[item])
                }
            })
            return data;
        },
        datasFormatForUpdate(data) {
            formatAttr.forEach(item => {
                if (data[item]) {
                    data[item] = JSON.stringify(data[item])
                }
            })

            data.oldName = data.name
            data.oldGroup = data.group
            return data;
        }
    }
}
export default ScheduleJobMixin