import ScheduleUrl from './ScheduleUrl.js'

const ScheduleRoute = [
    {
        path: ScheduleUrl.router.searchList,
        component: () => import('./element/ScheduleSearchList'),
        meta: {
            root: true,
            code:'ScheduleSearchList',
            name: '任务计划管理'
        }
    },
]
export default ScheduleRoute