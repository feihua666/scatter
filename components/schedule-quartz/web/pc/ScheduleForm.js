const ScheduleForm = [
    {
        field: {
            name: 'schedulerName'
        },
        element:{
            label: '任务计划名称'
        }
    },
    {
        field: {
            name: 'schedulerInstanceId'
        },
        element:{
            label: '任务计划实例id'
        }
    },
]
export default ScheduleForm