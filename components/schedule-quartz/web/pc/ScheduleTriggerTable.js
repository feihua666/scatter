const ScheduleJobTable = [
    {
        type: 'expand'
    },
    {
        prop: 'name',
        label: '任务名称'
    },
    {
        prop: 'group',
        label: '任务组'
    },
    {
        prop: 'cronExpression',
        label: 'cronExpression'
    },
    {
        prop: 'triggerState',
        label: '状态'
    },
    {
        prop: 'triggerClassName',
        label: '类名称'
    },
    {
        prop: 'calendarName',
        label: '日历名称'
    },
    {
        prop: 'description',
        label: '描述信息'
    },
    {
        prop: 'priority',
        label: '优先级'
    },
    {
        prop: 'isMayFireAgain',
        label: '是否可以再次触发'
    },
    {
        prop: 'startAt',
        label: '开始于'
    },
    {
        prop: 'endAt',
        label: '结束于'
    },
    {
        prop: 'nextFireAt',
        label: '下一次触发时间'
    },
    {
        prop: 'previousFireAt',
        label: '上一次触发时间'
    },
    {
        prop: 'finalFireAt',
        label: '最后触发时间'
    },
    {
        prop: 'misfireInstruction',
        label: '失火说明'
    },

]
export default ScheduleJobTable