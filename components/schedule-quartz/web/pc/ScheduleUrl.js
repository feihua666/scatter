const basePath = '' + '/schedule'
const ScheduleUrl = {
    searchList: basePath + '/getScheduleList',
    shutdown: basePath + '/shutdown',
    start: basePath + '/start',
    standby: basePath + '/standby',
    router: {
        searchList: '/ScheduleSearchList',
    }
}
export default ScheduleUrl