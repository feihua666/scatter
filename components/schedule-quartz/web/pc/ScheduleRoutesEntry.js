import ScheduleRoute from './ScheduleRoute.js'
import ScheduleJobRoute from './ScheduleJobRoute.js'
import ScheduleTriggerRoute from './ScheduleTriggerRoute.js'
import SchedulerRecordRoute from './record/SchedulerRecordRoute.js'

const ScheduleRoutesEntry = []
    .concat(ScheduleRoute)
    .concat(ScheduleJobRoute)
    .concat(ScheduleTriggerRoute)
    .concat(SchedulerRecordRoute)
export default ScheduleRoutesEntry