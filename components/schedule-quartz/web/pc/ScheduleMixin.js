import ScheduleForm from './ScheduleForm.js'
import ScheduleTable from './ScheduleTable.js'
import ScheduleUrl from './ScheduleUrl.js'

const ScheduleMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(ScheduleForm,this.$options.name)
        },
        computedTableOptions() {
            return ScheduleTable
        },
        computedUrl(){
            return ScheduleUrl
        }
    },
}
export default ScheduleMixin