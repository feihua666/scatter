const basePath = '' + '/schedule/trigger'
const ScheduleTriggerUrl = {
    searchList: basePath + '/getTriggerList',
    pause: basePath + '/pause',
    resume: basePath + '/resume',
    router: {
        searchList: '/ScheduleTriggerSearchList',
    }
}
export default ScheduleTriggerUrl