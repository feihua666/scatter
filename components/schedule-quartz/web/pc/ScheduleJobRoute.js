import ScheduleJobUrl from './ScheduleJobUrl.js'

const ScheduleJobRoute = [
    {
        path: ScheduleJobUrl.router.searchList,
        component: () => import('./element/ScheduleJobSearchList'),
        meta: {
            code:'ScheduleJobSearchList',
            name: '任务管理'
        }
    },
    {
        path: ScheduleJobUrl.router.add,
        component: () => import('./element/ScheduleJobAdd'),
        meta: {
            code:'ScheduleJobAdd',
            name: '任务添加'
        }
    },
    {
        path: ScheduleJobUrl.router.update,
        component: () => import('./element/ScheduleJobUpdate'),
        meta: {
            code:'ScheduleJobUpdate',
            name: '任务修改'
        }
    },
]
export default ScheduleJobRoute