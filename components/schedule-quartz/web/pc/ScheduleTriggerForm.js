const ScheduleTriggerForm = [
    {
        field: {
            name: 'schedulerName'
        },
        element:{
            label: '任务计划名称'
        }
    },
    {
        field: {
            name: 'schedulerInstanceId'
        },
        element:{
            label: '任务计划实例id'
        }
    },
    {
        field: {
            name: 'name'
        },
        element:{
            label: '触发器名称'
        }
    },
    {
        field: {
            name: 'group'
        },
        element:{
            label: '触发器组'
        }
    },
]
export default ScheduleTriggerForm