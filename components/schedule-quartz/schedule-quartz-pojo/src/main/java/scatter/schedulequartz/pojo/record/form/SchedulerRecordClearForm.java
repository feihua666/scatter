package scatter.schedulequartz.pojo.record.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 任务计划执行记录清理表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Setter
@Getter
@ApiModel(value="任务计划执行记录清理表单对象")
public class SchedulerRecordClearForm extends BaseAddForm {

    @ApiModelProperty(value = "schedulerName")
    private String schedulerName;

    @ApiModelProperty(value = "schedulerInstanceId")
    private String schedulerInstanceId;

    @ApiModelProperty(value = "任务名称")
    private String name;

    @ApiModelProperty(value = "任务组")
    private String group;

    @NotNull(message = "时间不能为空")
    @ApiModelProperty(value = "该时间以前的数据将被清理",required = true)
    private LocalDateTime beforeAt;

}
