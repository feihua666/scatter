package scatter.schedulequartz.pojo.form;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

/**
 * Created by yangwei
 * Created at 2021/2/2 13:25
 */
@Setter
@Getter
@ApiModel(value="触发器查询表单对象")
public class TriggerQueryForm extends NameAndGroupQueryForm {
}
