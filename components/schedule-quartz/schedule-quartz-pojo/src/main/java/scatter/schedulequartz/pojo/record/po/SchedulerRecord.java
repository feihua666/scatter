package scatter.schedulequartz.pojo.record.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 任务计划执行记录表
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_scheduler_record")
@ApiModel(value="SchedulerRecord对象", description="任务计划执行记录表")
public class SchedulerRecord extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SCHEDULERRECORD_BY_ID = "trans_schedulerrecord_by_id_scatter.schedulequartz.pojo.record.po";

    /**
     * 任务计划执行记录字典组
     */
    public enum SchedulerRecordStatusGroupCode implements IDictGroup {
        scheduler_record_status;

        @Override
        public String groupCode() {
            return this.name();
        }
    }


    /**
     * 任务计划执行记录状态字典项
     */
    public enum SchedulerRecordStatusDictItem implements IDictItem {
        success, // 执行成功
        fail, // 执行失败
        running, // 执行中
        ;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return SchedulerRecordStatusGroupCode.scheduler_record_status.groupCode();
        }
    }


    @ApiModelProperty(value = "schedulerName")
    private String schedulerName;

    @ApiModelProperty(value = "schedulerInstanceId")
    @TableField("schedulerInstanceId")
    private String schedulerInstanceId;

    @ApiModelProperty(value = "任务名称")
    private String jobName;

    @ApiModelProperty(value = "任务组")
    private String jobGroup;

    @ApiModelProperty(value = "执行参数，json")
    private String params;

    @ApiModelProperty(value = "执行结果")
    private String result;

    @ApiModelProperty(value = "执行状态，字典id")
    private String statusDictId;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "链路追踪id")
    private String traceId;

}
