package scatter.schedulequartz.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseVo;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2021/2/2 13:45
 */
@Setter
@Getter
@ApiModel(value="NameAndGroup响应数据对象")
public class NameAndGroupVo extends BaseVo {

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "组")
    private String group;
}
