package scatter.schedulequartz.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseVo;

/**
 * Created by yangwei
 * Created at 2021/2/2 13:45
 */
@Setter
@Getter
@ApiModel(value="任务计划监听响应数据对象")
public class ScheduleListenerVo extends BaseVo {

    @ApiModelProperty(value = "监听类名称")
    private String className;
}
