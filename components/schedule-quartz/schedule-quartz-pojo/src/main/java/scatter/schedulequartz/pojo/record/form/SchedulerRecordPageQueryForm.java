package scatter.schedulequartz.pojo.record.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 任务计划执行记录分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Setter
@Getter
@ApiModel(value="任务计划执行记录分页表单对象")
@OrderBy(value = "createAt",asc = false)
public class SchedulerRecordPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "schedulerName")
    private String schedulerName;

    @ApiModelProperty(value = "schedulerInstanceId")
    private String schedulerInstanceId;

    @ApiModelProperty(value = "任务名称")
    private String jobName;

    @ApiModelProperty(value = "任务组")
    private String jobGroup;

    @ApiModelProperty(value = "执行状态，字典id")
    private String statusDictId;
}
