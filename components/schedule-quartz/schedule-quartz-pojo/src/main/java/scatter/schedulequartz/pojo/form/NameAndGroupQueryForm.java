package scatter.schedulequartz.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * Created by yangwei
 * Created at 2021/2/2 17:25
 */
@Setter
@Getter
@ApiModel(value="NameAndGroupQuery表单对象")
public class NameAndGroupQueryForm extends BaseForm {

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "组")
    private String group;
}
