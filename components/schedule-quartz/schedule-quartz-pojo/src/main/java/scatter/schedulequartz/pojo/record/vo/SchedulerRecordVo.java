package scatter.schedulequartz.pojo.record.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 任务计划执行记录响应对象
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Setter
@Getter
@ApiModel(value="任务计划执行记录响应对象")
public class SchedulerRecordVo extends BaseIdVo {

    @ApiModelProperty(value = "schedulerName")
    private String schedulerName;

    @ApiModelProperty(value = "schedulerInstanceId")
    private String schedulerInstanceId;

    @ApiModelProperty(value = "任务名称")
    private String jobName;

    @ApiModelProperty(value = "任务组")
    private String jobGroup;

    @ApiModelProperty(value = "执行参数，json")
    private String params;

    @ApiModelProperty(value = "执行结果")
    private String result;

    @ApiModelProperty(value = "执行状态，字典id")
    private String statusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "statusDictId",mapValueField = "name")
    @ApiModelProperty(value = "执行状态，字典名称")
    private String statusDictName;


    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "链路追踪id")
    private String traceId;
}
