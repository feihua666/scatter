package scatter.schedulequartz.rest.record.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 任务计划执行记录表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-11
 */
@Component
@FeignClient(value = "SchedulerRecord-client")
public interface SchedulerRecordClient {

}
