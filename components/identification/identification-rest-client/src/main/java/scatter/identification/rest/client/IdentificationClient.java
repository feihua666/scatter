package scatter.identification.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 认证表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Component
@FeignClient(value = "Identification-client")
public interface IdentificationClient {

}
