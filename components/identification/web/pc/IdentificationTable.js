const IdentificationTable = [
    {
        prop: 'code',
        label: '认证编码'
    },
    {
        prop: 'name',
        label: '认证名称'
    },
    {
        prop: 'userNickname',
        label: '申请用户'
    },
    {
        prop: 'userAvatar',
        label: '申请用户头像',
        stype: 'image'
    },
    {
        prop: 'applyAt',
        label: '申请时间'
    },
    {
        prop: 'auditAt',
        label: '审核时间'
    },
    {
        prop: 'isAudited',
        label: '是否审核通过'
    },
    {
        prop: 'auditFailReason',
        label: '审核失败原因'
    },
]
export default IdentificationTable