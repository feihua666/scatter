import IdentificationUrl from './IdentificationUrl.js'

const IdentificationRoute = [
    {
        path: IdentificationUrl.router.searchList,
        component: () => import('./element/IdentificationSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'IdentificationSearchList',
            name: '认证管理'
        }
    },
    {
        path: IdentificationUrl.router.realNameAudit,
        component: () => import('./element/IdentificationRealNameAudit'),
        meta: {
            code:'IdentificationRealNameAudit',
            name: '实名认证审核'
        }
    },
]
export default IdentificationRoute