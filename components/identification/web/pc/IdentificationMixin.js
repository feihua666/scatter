import IdentificationForm from './IdentificationForm.js'
import IdentificationTable from './IdentificationTable.js'
import IdentificationUrl from './IdentificationUrl.js'
const IdentificationMixin = {
    computed: {
        IdentificationFormOptions() {
            return this.$stDynamicFormTools.options(IdentificationForm,this.$options.name)
        },
        IdentificationTableOptions() {
            return IdentificationTable
        },
        IdentificationUrl(){
            return IdentificationUrl
        }
    },
}
export default IdentificationMixin