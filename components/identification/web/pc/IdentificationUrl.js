const basePath = '' + '/identification'
const IdentificationUrl = {
    searchList: basePath + '/getPage',
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    audit: basePath + '/audit',
    apply: basePath + '/apply',
    hasIdentified: basePath + '/hasIdentified/{code}',
    getByCode: basePath + '/getByCode/{code}',
    // 请求多个认证结果
    hasIdentifieds: basePath + '/hasIdentifieds/{codes}',
    getByCodes: basePath + '/getByCodes/{codes}',
    router: {
        searchList: '/IdentificationSearchList',
        // 实名认证审核
        realNameAudit: '/IdentificationRealNameAudit',
    }
}
export default IdentificationUrl