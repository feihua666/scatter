import IdentificationUrl from './IdentificationUrl.js'
const IdentificationForm = [
    {
        IdentificationSearchList: {
            element:{
                required: false
            }
        },
        IdentificationRealNameAudit:{
            element: {
                type: 'txt',
                required: false
            }
        },
        field: {
            name: 'code',
        },
        element:{
            label: '认证编码',
            required: true,
        }
    },
    {
        IdentificationSearchList: {
            element:{
                required: false
            }
        },
        IdentificationRealNameAudit:{
            element: {
                type: 'txt',
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '认证名称',
            required: true,
        }
    },

    {
        IdentificationSearchList: {
            element:{
                required: false
            }
        },
        IdentificationRealNameAudit:{
            field: {
                name: 'userNickname',
            },
            element: {
                type: 'txt',
                required: false
            }
        },
        field: {
            name: 'userId',
        },
        element:{
            label: '申请用户',
            required: true,
            type: 'select',
            readonly: true,
            options: ({$route,$vm})=>{
                let datas = []
                if ($route.query.userId) {
                    datas.push({
                        id: $route.query.userId,
                        nickname: $route.query.userNickname || $route.query.userId
                    })
                }
                return {
                    datas: datas,
                    optionProp:{
                        value: 'id', // 选中的值属性
                        label: 'nickname', // 显示的值属性
                    },
                    originProp: {
                        placeholder: '请按用户昵称搜索',
                        remote: true,
                    },
                    // 因为本项目存在两个用户后端组件，这是可以通过全局配置的方式决定使用哪个用户体系
                    remoteUrl: $vm.$stObjectTools.getValue($vm.$scatterConfig,'IdentificationForm.userRemoteSearchUrl') || '/user-simple/getPage',
                    remoteQueryProp: 'nickname'
                }
            }

        }
    },
    /** 实名认证专用开始 ***/
    {
        IdentificationSearchList: false,

        field: {
            name: 'contentMap.realName',
        },
        element:{
            label: '真实姓名',
            type: 'txt',
        }
    },
    {
        IdentificationSearchList: false,

        field: {
            name: 'contentMap.idCardNo',
        },
        element:{
            label: '身份证号',
            type: 'txt',
        }
    },
    {
        IdentificationSearchList: false,

        field: {
            name: 'contentMap.idCardImageUpUrl',
        },
        element:{
            label: '身份证正面照',
            type: 'image',
        }
    },
    {
        IdentificationSearchList: false,

        field: {
            name: 'contentMap.idCardImageDownUrl',
        },
        element:{
            label: '身份证反面照',
            type: 'image',
        }
    },
    /** 实名认证专用结束 ***/
    {
        IdentificationSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'isAudited',
        },
        element:{
            type: 'select',
            options: {
                datas: 'yesNoBool'
            },
            label: '是否审核通过',
            required: true,
        }
    },

    {
        IdentificationSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'auditFailReason',
        },
        element:{
            label: '审核失败原因',
            required: ({form})=>{return !form.isAudited},

        }
    },
]
export default IdentificationForm