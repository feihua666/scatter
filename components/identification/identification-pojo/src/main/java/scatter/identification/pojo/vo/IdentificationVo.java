package scatter.identification.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.Trans;
import scatter.common.rest.trans.TransBy;
import scatter.common.trans.TransConstants;


/**
 * <p>
 * 认证响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Setter
@Getter
@ApiModel(value="认证响应对象")
public class IdentificationVo extends BaseIdVo {

    @ApiModelProperty(value = "认证编码")
    private String code;

    @ApiModelProperty(value = "认证名称")
    private String name;

    @ApiModelProperty(value = "认证内容，根据不同的认证存放不同的json数据")
    private String content;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @TransBy(type = TransConstants.TRANS_USER_BY_ID,byFieldName = "userId",mapValueField = "nickname")
    @ApiModelProperty(value = "用户昵称")
    private String userNickname;

    @TransBy(type = TransConstants.TRANS_USER_BY_ID,byFieldName = "userId",mapValueField = "avatar")
    @ApiModelProperty(value = "用户头像")
    private String userAvatar;

    @ApiModelProperty(value = "是否认证成功")
    private Boolean isIdentified;


    @ApiModelProperty(value = "认证成功时间")
    private LocalDateTime identifiedAt;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime applyAt;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime auditAt;

    @ApiModelProperty(value = "是否审核通过")
    private Boolean isAudited;

    @ApiModelProperty(value = "审核失败原因")
    private String auditFailReason;

    @ApiModelProperty(value = "审核路由地址,json格式，以兼容多端")
    private String auditRoute;

}
