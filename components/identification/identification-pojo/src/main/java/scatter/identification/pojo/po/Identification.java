package scatter.identification.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 认证表
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_identification")
@ApiModel(value="Identification对象", description="认证表")
public class Identification extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_IDENTIFICATION_BY_ID = "trans_identification_by_id_scatter.identification.pojo.po";

    @ApiModelProperty(value = "认证编码")
    private String code;

    @ApiModelProperty(value = "认证名称")
    private String name;

    @ApiModelProperty(value = "认证内容，根据不同的认证存放不同的json数据")
    private String content;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "是否认证成功")
    private Boolean isIdentified;

    @ApiModelProperty(value = "认证成功时间")
    private LocalDateTime identifiedAt;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime applyAt;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime auditAt;

    @ApiModelProperty(value = "是否审核通过")
    private Boolean isAudited;

    @ApiModelProperty(value = "审核失败原因")
    private String auditFailReason;

    @ApiModelProperty(value = "审核路由地址,json格式，以兼容多端")
    private String auditRoute;


}
