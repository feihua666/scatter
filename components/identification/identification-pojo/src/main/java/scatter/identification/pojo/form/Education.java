package scatter.identification.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Author wb.yang
 * @Date 2021/11/28
 */
@Data
@ApiModel(value = "学历认证对象")
public class Education {

    @NotEmpty(message="学历名不能为空")
    @ApiModelProperty(value = "学历姓名",required = true)
    private String name;
    @NotEmpty(message="学校不能为空")
    @ApiModelProperty(value = "学校",required = true)
    private String school;
    @NotEmpty(message="资料不能为空")
    @ApiModelProperty(value = "资料",required = true)
    private String[] materials;
}
