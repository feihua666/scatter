package scatter.identification.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseUpdateForm;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 认证审核表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Setter
@Getter
@ApiModel(value="认证审核表单对象")
public class IdentificationAuditForm extends BaseUpdateForm {

    @ApiModelProperty(value = "id主键",required = true)
    @NotNull(message = "id主键不能为空")
    private String id;

    @NotNull(message="是否审核通过不能为空")
    @ApiModelProperty(value = "是否审核通过",required = true)
    private Boolean isAudited;

    @ApiModelProperty(value = "审核失败原因")
    private String auditFailReason;

}
