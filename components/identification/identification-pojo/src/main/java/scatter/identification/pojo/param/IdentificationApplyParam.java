package scatter.identification.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 认证申请参数
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 11:20
 */
@Setter
@Getter
public class IdentificationApplyParam extends BaseParam {


	@NotEmpty(message="认证编码不能为空")
	@ApiModelProperty(value = "认证编码",required = true)
	private String code;

	@NotEmpty(message="认证名称不能为空")
	@ApiModelProperty(value = "认证名称",required = true)
	private String name;

	@NotEmpty(message="认证内容不能为空")
	@ApiModelProperty(value = "认证内容，根据不同的认证存放不同的json数据",required = true)
	private String content;

	@NotEmpty(message="用户id不能为空")
	@ApiModelProperty(value = "用户id",required = true)
	private String userId;

}
