package scatter.identification.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Author wb.yang
 * @Date 2021/11/28
 */
@Data
@ApiModel(value = "姓名认证对象")
public class RealName {

    @NotEmpty(message="真实姓名不能为空")
    @ApiModelProperty(value = "真实姓名",required = true)
    private String name;
    @NotEmpty(message="身份证号不能为空")
    @ApiModelProperty(value = "身份证号",required = true)
    private String idCard;
    @NotEmpty(message="资料不能为空")
    @ApiModelProperty(value = "资料",required = true)
    private String[] materials;
}
