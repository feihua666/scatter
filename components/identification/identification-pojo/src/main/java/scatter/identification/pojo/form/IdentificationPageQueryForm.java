package scatter.identification.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 认证分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Setter
@Getter
@ApiModel(value="认证分页表单对象")
public class IdentificationPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "认证编码")
    private String code;

    @Like
    @ApiModelProperty(value = "认证名称")
    private String name;

    @ApiModelProperty(value = "认证内容，根据不同的认证存放不同的json数据")
    private String content;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime applyAt;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime auditAt;

    @ApiModelProperty(value = "是否审核通过")
    private Boolean isAudited;

    @ApiModelProperty(value = "审核失败原因")
    private String auditFailReason;

    @ApiModelProperty(value = "审核路由地址,json格式，以兼容多端")
    private String auditRoute;

}
