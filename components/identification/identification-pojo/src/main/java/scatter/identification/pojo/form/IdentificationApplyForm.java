package scatter.identification.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * <p>
 * 认证申请添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Setter
@Getter
@ApiModel(value="认证申请添加表单对象")
public class IdentificationApplyForm extends BaseAddForm {

    @NotEmpty(message="认证编码不能为空")
    @ApiModelProperty(value = "认证编码",required = true)
    private String code;

    @NotEmpty(message="认证名称不能为空")
    @ApiModelProperty(value = "认证名称",required = true)
    private String name;

    /**
     * 以字符串形式接收
     * content 有值 contentMap 无效
     */
    @ApiModelProperty(value = "认证内容，根据不同的认证存放不同的json数据")
    private String content;

    /**
     * 以 json 对象形式接收
     * content 有值该字段无效
     */
    @ApiModelProperty(value = "认证内容，根据不同的认证存放不同的json数据")
    private Map<String,Object> contentMap;
}
