package scatter.identification.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Author wb.yang
 * @Date 2021/11/28
 */
@Data
@ApiModel(value = "职业认证对象")
public class Profession {

    @NotEmpty(message="职业名不能为空")
    @ApiModelProperty(value = "职业名",required = true)
    private String name;
    @NotEmpty(message="公司名不能为空")
    @ApiModelProperty(value = "公司名",required = true)
    private String company;
    @NotEmpty(message="资料不能为空")
    @ApiModelProperty(value = "资料",required = true)
    private String[] materials;
}
