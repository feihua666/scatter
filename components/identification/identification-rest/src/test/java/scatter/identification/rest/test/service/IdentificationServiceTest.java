package scatter.identification.rest.test.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.service.IIdentificationService;
import scatter.identification.rest.test.IdentificationSuperTest;

import java.util.List;
/**
* <p>
* 认证 服务测试类
* </p>
*
* @author yw
* @since 2021-11-05
*/
@SpringBootTest
public class IdentificationServiceTest extends IdentificationSuperTest{

    @Autowired
    private IIdentificationService identificationService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Identification> pos = identificationService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}