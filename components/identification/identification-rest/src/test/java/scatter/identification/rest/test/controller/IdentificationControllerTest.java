package scatter.identification.rest.test.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import scatter.identification.rest.test.IdentificationSuperTest;
/**
* <p>
* 认证 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-11-05
*/
@SpringBootTest
@AutoConfigureMockMvc
public class IdentificationControllerTest extends IdentificationSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

}