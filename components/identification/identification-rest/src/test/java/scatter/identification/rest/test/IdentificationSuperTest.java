package scatter.identification.rest.test;

import cn.hutool.core.util.ReflectUtil;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BaseTreePo;
import scatter.identification.pojo.form.IdentificationPageQueryForm;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.service.IIdentificationService;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
/**
* <p>
* 认证 测试类基类
* </p>
*
* @author yw
* @since 2021-11-05
*/
public class IdentificationSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IIdentificationService identificationService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return identificationService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return identificationService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Identification mockPo() {
        return JMockData.mock(Identification.class, mockConfig);
    }


    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public IdentificationPageQueryForm mockPageQueryForm() {
        return JMockData.mock(IdentificationPageQueryForm.class, mockConfig);
    }
}