package scatter.identification.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.identification.rest.IdentificationConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, IdentificationConfiguration.class})
@MapperScan("scatter.identification.rest.mapper")
public class IdentificationTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(IdentificationTestApplication.class, args);
    }
}
