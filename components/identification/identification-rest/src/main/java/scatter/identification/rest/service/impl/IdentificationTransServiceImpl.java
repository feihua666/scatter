package scatter.identification.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.service.IIdentificationService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 认证翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Component
public class IdentificationTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IIdentificationService identificationService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Identification.TRANS_IDENTIFICATION_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Identification.TRANS_IDENTIFICATION_BY_ID)) {
            Identification byId = identificationService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Identification.TRANS_IDENTIFICATION_BY_ID)) {
            return identificationService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
