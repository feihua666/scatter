package scatter.identification.rest;

import lombok.Getter;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 11:11
 */
public class IdentificationConstants {

	/**
	 * 认证相关枚举
	 */
	public static enum Identify{
		real_name("实名认证"),
		education("学历认证"),
		profession("职业认证");

		@Getter
		private String name;

		Identify(String name) {
			this.name = name;
		}
	}
}
