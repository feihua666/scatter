package scatter.identification.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.identification.rest.IdentificationConfiguration;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.service.IIdentificationService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 认证表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@RestController
@RequestMapping(IdentificationConfiguration.CONTROLLER_BASE_PATH + "/inner/identification")
public class IdentificationInnerController extends BaseInnerController<Identification> {
 @Autowired
 private IIdentificationService identificationService;

 public IIdentificationService getService(){
     return identificationService;
 }
}
