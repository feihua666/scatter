package scatter.identification.rest.componentext;

import scatter.identification.pojo.po.Identification;

/**
 * <p>
 * 审核 监听,可以用来发消息或其它处理
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 11:03
 */
public interface IdentificationAuditListener {
	/**
	 * 审核监听，
	 * @param identification
	 */
	void identificationAudit(Identification identification);
}
