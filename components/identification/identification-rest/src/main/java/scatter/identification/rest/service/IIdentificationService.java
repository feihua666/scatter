package scatter.identification.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.identification.pojo.param.IdentificationApplyParam;
import scatter.identification.pojo.po.Identification;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 认证表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
public interface IIdentificationService extends IBaseService<Identification> {

	/**
	 * 根据认证编码和用户id获取
	 * @param code
	 * @param userId
	 * @return
	 */
	default Identification getByCodeAndUserId(String code,String userId){
		List<Identification> byCodesAndUserId = getByCodesAndUserId(newArrayList(code), userId);
		if (isEmpty(byCodesAndUserId)) {
			return null;
		}
		return byCodesAndUserId.iterator().next();
	}
	/**
	 * 根据认证编码和用户id获取
	 * @param codes
	 * @param userId
	 * @return
	 */
	default List<Identification> getByCodesAndUserId(List<String> codes,String userId){
		Assert.notEmpty(codes,"codes 不能为空");
		Assert.hasText(userId,"userId 不能为空");
		return list(Wrappers.<Identification>lambdaQuery().in(Identification::getCode,codes).eq(Identification::getUserId,userId));
	}
	/**
	 * 是否审核通过，审核通过一定认证成功了
	 * @param code
	 * @param userId
	 * @return
	 */
	default boolean hasAudited(String code,String userId){
		Identification byCodeAndUserId = getByCodeAndUserId(code, userId);
		if (byCodeAndUserId == null) {
			return false;
		}
		return byCodeAndUserId.getIsAudited();
	}
	/**
	 * 是否认证成功，认证成功不一定审核通过，因为有可能重新认证，但以前的认证还有效
	 * @param code
	 * @param userId
	 * @return
	 */
	default boolean hasIdentified(String code,String userId){
		Identification byCodeAndUserId = getByCodeAndUserId(code, userId);
		if (byCodeAndUserId == null) {
			return false;
		}
		return byCodeAndUserId.getIsIdentified();
	}
	/**
	 * 是否认证成功，认证成功不一定审核通过，因为有可能重新认证，但以前的认证还有效
	 * @param codes
	 * @param userId
	 * @return
	 */
	default Map<String,Boolean> hasIdentified(List<String> codes, String userId){
		List<Identification> byCodeAndUserId = getByCodesAndUserId(codes, userId);

		Map<String, Boolean> collect = codes.stream()
				.collect(Collectors.toMap(Function.identity(),
						code -> byCodeAndUserId.stream().filter(identification -> isEqual(code, identification.getCode()))
								.findFirst().map(Identification::getIsIdentified).orElse(false)));
		return collect;
	}
	/**
	 * 认证信息，认证成功不一定审核通过，因为有可能重新认证，但以前的认证还有效
	 * @param codes
	 * @param userId
	 * @return
	 */
	default Map<String,Identification> getByCodes(List<String> codes, String userId){
		List<Identification> byCodeAndUserId = getByCodesAndUserId(codes, userId);

		Map<String, Identification> collect = codes.stream()
				.collect(Collectors.toMap(Function.identity(),
						code -> byCodeAndUserId.stream().filter(identification -> isEqual(code, identification.getCode()))
								.findFirst().orElse(null)));
		return collect;
	}
	/**
	 * 审核
	 * @param id 主键id
	 * @param isAudited true=审核通过，false，审核失败
	 * @return
	 */
	boolean audit(String id,boolean isAudited,String failedReason);

	/**
	 * 申请,可以重复申请或重新申请，不影响以前已认证成功的数据使用
	 * @param param
	 * @return
	 */
	boolean apply(IdentificationApplyParam param);
}
