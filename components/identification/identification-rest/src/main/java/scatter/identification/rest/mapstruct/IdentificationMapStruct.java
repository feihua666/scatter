package scatter.identification.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.identification.pojo.form.IdentificationApplyForm;
import scatter.identification.pojo.form.IdentificationPageQueryForm;
import scatter.identification.pojo.param.IdentificationApplyParam;
import scatter.identification.pojo.po.Identification;
import scatter.identification.pojo.vo.IdentificationVo;

/**
 * <p>
 * 认证 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IdentificationMapStruct extends IBaseVoMapStruct<Identification, IdentificationVo>,
                                  IBaseQueryFormMapStruct<Identification,IdentificationPageQueryForm>{
    IdentificationMapStruct INSTANCE = Mappers.getMapper( IdentificationMapStruct.class );

    IdentificationApplyParam applyFormToParam(IdentificationApplyForm applyForm);
    Identification applyParamToPo(IdentificationApplyParam param);
}
