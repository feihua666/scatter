package scatter.identification.rest.componentext;

import scatter.identification.pojo.param.IdentificationApplyParam;
import scatter.identification.pojo.po.Identification;

/**
 * <p>
 * 申请 监听,可以用来发消息或其它处理
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 11:03
 */
public interface IdentificationApplyListener {
	/**
	 * 申请监听，
	 * @param param
	 * @param identification
	 */
	void identificationApply(IdentificationApplyParam param, Identification identification);
}
