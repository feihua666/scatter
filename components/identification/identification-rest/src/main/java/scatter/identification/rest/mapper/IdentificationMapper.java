package scatter.identification.rest.mapper;

import scatter.identification.pojo.po.Identification;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 认证表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
public interface IdentificationMapper extends IBaseMapper<Identification> {

}
