package scatter.identification.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.service.IBaseQueryFormServiceImpl;
import scatter.identification.pojo.form.IdentificationPageQueryForm;
import scatter.identification.pojo.param.IdentificationApplyParam;
import scatter.identification.pojo.po.Identification;
import scatter.identification.rest.componentext.IdentificationApplyListener;
import scatter.identification.rest.componentext.IdentificationAuditListener;
import scatter.identification.rest.mapper.IdentificationMapper;
import scatter.identification.rest.mapstruct.IdentificationMapStruct;
import scatter.identification.rest.service.IIdentificationService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 认证表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-05
 */
@Service
@Transactional
@ConfigurationProperties(prefix = "scatter.identification")
public class IdentificationServiceImpl extends IBaseQueryFormServiceImpl<IdentificationMapper, Identification, IdentificationPageQueryForm> implements IIdentificationService {

    /**
     * 配置审核路由，这里的key和前端有关，一般为 pc、app两个
     */
    private Map<String,String> auditRoute;

    @Autowired(required = false)
    private List<IdentificationAuditListener> identificationAuditListenerList;

    @Autowired(required = false)
    private List<IdentificationApplyListener> identificationApplyListenerList;


    @Override
    public boolean audit(String id, boolean isAudited, String failedReason) {
        if (!isAudited && isStrEmpty(failedReason)) {
            throw new BusinessException("审核不通过需要填写不通过原因");
        }

        Identification byId = getById(id);

        LocalDateTime now = LocalDateTime.now();
        byId.setAuditAt(now);
        byId.setIsAudited(isAudited);
        if (isAudited) {
            byId.setIsIdentified(true);
            byId.setIdentifiedAt(now);
        }
        byId.setAuditFailReason(failedReason);
        boolean b = updateById(byId);

        if (!isEmpty(identificationAuditListenerList)) {
            for (IdentificationAuditListener identificationAuditListener : identificationAuditListenerList) {
                identificationAuditListener.identificationAudit(byId);
            }
        }

        return b;
    }

    @Override
    public boolean apply(IdentificationApplyParam param) {

        Identification identification = getByCodeAndUserId(param.getCode(), param.getUserId());
        if (identification == null) {
            identification = IdentificationMapStruct.INSTANCE.applyParamToPo(param);
            // 初次申请，设置为 false，如果重新申请不处理，因为默认已经有值
            identification.setIsIdentified(false);
        }
        identification.setIsAudited(false);


        identification.setApplyAt(LocalDateTime.now());
        if (auditRoute != null) {
            identification.setAuditRoute(toJsonStr(auditRoute));
        }
        boolean b = saveOrUpdate(identification);

        if (!isEmpty(identificationApplyListenerList)) {
            for (IdentificationApplyListener identificationApplyListener : identificationApplyListenerList) {
                identificationApplyListener.identificationApply(param,identification);
            }
        }
        return true;
    }
}
