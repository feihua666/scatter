DROP TABLE IF EXISTS component_identification;
CREATE TABLE `component_identification` (
  `id` varchar(20) NOT NULL COMMENT '用户ID',
  `code` varchar(50) NOT NULL COMMENT '认证编码',
  `name` varchar(20) NOT NULL COMMENT '认证名称',
  `content` varchar(2000) NOT NULL COMMENT '认证内容，根据不同的认证存放不同的json数据',
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `is_identified` tinyint(1) NOT NULL COMMENT '是否认证成功',
  `identified_at` datetime NULL DEFAULT NULL COMMENT '认证成功时间',
  `apply_at` datetime NOT NULL COMMENT '申请时间',
  `audit_at` datetime DEFAULT NULL COMMENT '审核时间',
  `is_audited` tinyint(1) NOT NULL COMMENT '是否审核通过',
  `audit_fail_reason` varchar(255) DEFAULT NULL COMMENT '审核失败原因',
  `audit_route` varchar(255) DEFAULT NULL COMMENT '审核路由地址,json格式，以兼容多端',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code_2` (`code`,`user_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='认证表';
