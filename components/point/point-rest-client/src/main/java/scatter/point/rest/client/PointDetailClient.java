package scatter.point.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 积分详情表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Component
@FeignClient(value = "PointDetail-client")
public interface PointDetailClient {

}
