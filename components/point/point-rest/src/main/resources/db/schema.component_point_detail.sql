DROP TABLE IF EXISTS component_point_detail;
CREATE TABLE `component_point_detail` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `user_id` varchar(20) NOT NULL COMMENT '用户id,外键',
  `point_id` varchar(20) NOT NULL COMMENT '积分id,外键',
  `before_points` int(11) NOT NULL COMMENT '记录前积分数',
  `after_points` int(11) NOT NULL COMMENT '记录后积分数',
  `is_plus` tinyint(1) NOT NULL COMMENT '是否加积分，不是加就是减',
  `points` int(11) NOT NULL COMMENT '加或减的积分数',
  `description` varchar(300) NOT NULL COMMENT '记录描述',
  `relation_data_id` varchar(20) DEFAULT NULL COMMENT '关联的数据id,外键',
  `relation_data_type` varchar(255) DEFAULT NULL COMMENT '关联的数据类型',
  `relation_data_content` text COMMENT '关联的数据内容，快照',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `point_id` (`point_id`),
  KEY `user_id_2` (`user_id`,`point_id`),
  KEY `relation_data_id` (`relation_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='积分详情表';
