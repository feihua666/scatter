DROP TABLE IF EXISTS component_point;
CREATE TABLE `component_point` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `user_id` varchar(20) NOT NULL COMMENT '用户id,外键',
  `points` int(11) NOT NULL COMMENT '当前积分数',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='积分表';
