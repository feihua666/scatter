package scatter.point.rest.mapper;

import scatter.point.pojo.po.Point;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 积分表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
public interface PointMapper extends IBaseMapper<Point> {

}
