package scatter.point.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.point.rest.PointConfiguration;
import scatter.point.pojo.po.Point;
import scatter.point.rest.service.IPointService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 积分表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@RestController
@RequestMapping(PointConfiguration.CONTROLLER_BASE_PATH + "/inner/point")
public class PointInnerController extends BaseInnerController<Point> {
 @Autowired
 private IPointService pointService;

 public IPointService getService(){
     return pointService;
 }
}
