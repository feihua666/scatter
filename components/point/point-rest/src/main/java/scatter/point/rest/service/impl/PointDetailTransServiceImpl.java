package scatter.point.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.point.pojo.po.PointDetail;
import scatter.point.rest.service.IPointDetailService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 积分详情翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Component
public class PointDetailTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IPointDetailService pointDetailService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,PointDetail.TRANS_POINTDETAIL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,PointDetail.TRANS_POINTDETAIL_BY_ID)) {
            PointDetail byId = pointDetailService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,PointDetail.TRANS_POINTDETAIL_BY_ID)) {
            return pointDetailService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
