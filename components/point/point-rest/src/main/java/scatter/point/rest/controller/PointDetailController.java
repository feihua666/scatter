package scatter.point.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.point.rest.PointConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.point.pojo.po.PointDetail;
import scatter.point.pojo.vo.PointDetailVo;
import scatter.point.pojo.form.PointDetailAddForm;
import scatter.point.pojo.form.PointDetailUpdateForm;
import scatter.point.pojo.form.PointDetailPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 积分详情表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Api(tags = "积分详情相关接口")
@RestController
@RequestMapping(PointConfiguration.CONTROLLER_BASE_PATH + "/point-detail")
public class PointDetailController extends BaseAddUpdateQueryFormController<PointDetail, PointDetailVo, PointDetailAddForm, PointDetailUpdateForm, PointDetailPageQueryForm> {


     @Override
	 @ApiOperation("添加积分详情")
     @PreAuthorize("hasAuthority('PointDetail:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PointDetailVo add(@RequestBody @Valid PointDetailAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询积分详情")
     @PreAuthorize("hasAuthority('PointDetail:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public PointDetailVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除积分详情")
     @PreAuthorize("hasAuthority('PointDetail:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新积分详情")
     @PreAuthorize("hasAuthority('PointDetail:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PointDetailVo update(@RequestBody @Valid PointDetailUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询积分详情")
    @PreAuthorize("hasAuthority('PointDetail:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<PointDetailVo> getList(PointDetailPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询积分详情")
    @PreAuthorize("hasAuthority('PointDetail:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<PointDetailVo> getPage(PointDetailPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
