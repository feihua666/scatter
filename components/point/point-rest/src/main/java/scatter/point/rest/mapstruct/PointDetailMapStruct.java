package scatter.point.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.point.pojo.po.PointDetail;
import scatter.point.pojo.vo.PointDetailVo;
import scatter.point.pojo.form.PointDetailAddForm;
import scatter.point.pojo.form.PointDetailUpdateForm;
import scatter.point.pojo.form.PointDetailPageQueryForm;

/**
 * <p>
 * 积分详情 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PointDetailMapStruct extends IBaseVoMapStruct<PointDetail, PointDetailVo>,
                                  IBaseAddFormMapStruct<PointDetail,PointDetailAddForm>,
                                  IBaseUpdateFormMapStruct<PointDetail,PointDetailUpdateForm>,
                                  IBaseQueryFormMapStruct<PointDetail,PointDetailPageQueryForm>{
    PointDetailMapStruct INSTANCE = Mappers.getMapper( PointDetailMapStruct.class );

}
