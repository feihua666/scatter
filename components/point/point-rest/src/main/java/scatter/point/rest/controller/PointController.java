package scatter.point.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.point.rest.PointConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.point.pojo.po.Point;
import scatter.point.pojo.vo.PointVo;
import scatter.point.pojo.form.PointAddForm;
import scatter.point.pojo.form.PointUpdateForm;
import scatter.point.pojo.form.PointPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 积分表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Api(tags = "积分相关接口")
@RestController
@RequestMapping(PointConfiguration.CONTROLLER_BASE_PATH + "/point")
public class PointController extends BaseAddUpdateQueryFormController<Point, PointVo, PointAddForm, PointUpdateForm, PointPageQueryForm> {


     @Override
	 @ApiOperation("添加积分")
     @PreAuthorize("hasAuthority('Point:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PointVo add(@RequestBody @Valid PointAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询积分")
     @PreAuthorize("hasAuthority('Point:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public PointVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除积分")
     @PreAuthorize("hasAuthority('Point:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新积分")
     @PreAuthorize("hasAuthority('Point:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PointVo update(@RequestBody @Valid PointUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询积分")
    @PreAuthorize("hasAuthority('Point:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<PointVo> getList(PointPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询积分")
    @PreAuthorize("hasAuthority('Point:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<PointVo> getPage(PointPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
