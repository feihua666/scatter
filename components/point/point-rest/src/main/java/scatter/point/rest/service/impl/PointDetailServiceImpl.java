package scatter.point.rest.service.impl;

import scatter.point.pojo.po.PointDetail;
import scatter.point.rest.mapper.PointDetailMapper;
import scatter.point.rest.service.IPointDetailService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.point.pojo.form.PointDetailAddForm;
import scatter.point.pojo.form.PointDetailUpdateForm;
import scatter.point.pojo.form.PointDetailPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 积分详情表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Service
@Transactional
public class PointDetailServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PointDetailMapper, PointDetail, PointDetailAddForm, PointDetailUpdateForm, PointDetailPageQueryForm> implements IPointDetailService {
    @Override
    public void preAdd(PointDetailAddForm addForm,PointDetail po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PointDetailUpdateForm updateForm,PointDetail po) {
        super.preUpdate(updateForm,po);

    }
}
