package scatter.point.rest.service.impl;

import scatter.point.pojo.po.Point;
import scatter.point.rest.mapper.PointMapper;
import scatter.point.rest.service.IPointService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.point.pojo.form.PointAddForm;
import scatter.point.pojo.form.PointUpdateForm;
import scatter.point.pojo.form.PointPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 积分表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Service
@Transactional
public class PointServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PointMapper, Point, PointAddForm, PointUpdateForm, PointPageQueryForm> implements IPointService {
    @Override
    public void preAdd(PointAddForm addForm,Point po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getUserId())) {
            // 用户id已存在不能添加
            assertByColumn(addForm.getUserId(),Point::getUserId,false);
        }

    }

    @Override
    public void preUpdate(PointUpdateForm updateForm,Point po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getUserId())) {
            Point byId = getById(updateForm.getId());
            // 如果用户id有改动
            if (!isEqual(updateForm.getUserId(), byId.getUserId())) {
                // 用户id已存在不能修改
                assertByColumn(updateForm.getUserId(),Point::getUserId,false);
            }
        }

    }
}
