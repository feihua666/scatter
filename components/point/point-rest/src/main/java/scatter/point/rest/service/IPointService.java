package scatter.point.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.point.pojo.po.Point;
import scatter.common.rest.service.IBaseService;
import scatter.point.pojo.form.PointAddForm;
import scatter.point.pojo.form.PointUpdateForm;
import scatter.point.pojo.form.PointPageQueryForm;
import java.util.List;
/**
 * <p>
 * 积分表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
public interface IPointService extends IBaseService<Point> {


    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default Point getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return getOne(Wrappers.<Point>lambdaQuery().eq(Point::getUserId, userId));
    }

}
