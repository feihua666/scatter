package scatter.point.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.point.rest.PointConfiguration;
import scatter.point.pojo.po.PointDetail;
import scatter.point.rest.service.IPointDetailService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 积分详情表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@RestController
@RequestMapping(PointConfiguration.CONTROLLER_BASE_PATH + "/inner/point-detail")
public class PointDetailInnerController extends BaseInnerController<PointDetail> {
 @Autowired
 private IPointDetailService pointDetailService;

 public IPointDetailService getService(){
     return pointDetailService;
 }
}
