package scatter.point.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.point.pojo.po.PointDetail;
import scatter.common.rest.service.IBaseService;
import scatter.point.pojo.form.PointDetailAddForm;
import scatter.point.pojo.form.PointDetailUpdateForm;
import scatter.point.pojo.form.PointDetailPageQueryForm;
import java.util.List;
/**
 * <p>
 * 积分详情表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
public interface IPointDetailService extends IBaseService<PointDetail> {

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<PointDetail> getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return list(Wrappers.<PointDetail>lambdaQuery().eq(PointDetail::getUserId, userId));
    }
    /**
     * 根据积分id查询
     * @param pointId
     * @return
     */
    default List<PointDetail> getByPointId(String pointId) {
        Assert.hasText(pointId,"pointId不能为空");
        return list(Wrappers.<PointDetail>lambdaQuery().eq(PointDetail::getPointId, pointId));
    }
    /**
     * 根据关联的数据id查询
     * @param relationDataId
     * @return
     */
    default List<PointDetail> getByRelationDataId(String relationDataId) {
        Assert.hasText(relationDataId,"relationDataId不能为空");
        return list(Wrappers.<PointDetail>lambdaQuery().eq(PointDetail::getRelationDataId, relationDataId));
    }

}
