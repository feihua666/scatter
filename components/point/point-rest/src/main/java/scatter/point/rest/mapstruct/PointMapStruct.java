package scatter.point.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.point.pojo.po.Point;
import scatter.point.pojo.vo.PointVo;
import scatter.point.pojo.form.PointAddForm;
import scatter.point.pojo.form.PointUpdateForm;
import scatter.point.pojo.form.PointPageQueryForm;

/**
 * <p>
 * 积分 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PointMapStruct extends IBaseVoMapStruct<Point, PointVo>,
                                  IBaseAddFormMapStruct<Point,PointAddForm>,
                                  IBaseUpdateFormMapStruct<Point,PointUpdateForm>,
                                  IBaseQueryFormMapStruct<Point,PointPageQueryForm>{
    PointMapStruct INSTANCE = Mappers.getMapper( PointMapStruct.class );

}
