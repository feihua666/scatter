package scatter.point.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.point.pojo.po.Point;
import scatter.point.pojo.form.PointAddForm;
import scatter.point.pojo.form.PointUpdateForm;
import scatter.point.pojo.form.PointPageQueryForm;
import scatter.point.rest.test.PointSuperTest;
import scatter.point.rest.service.IPointService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 积分 服务测试类
* </p>
*
* @author yw
* @since 2021-02-20
*/
@SpringBootTest
public class PointServiceTest extends PointSuperTest{

    @Autowired
    private IPointService pointService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Point> pos = pointService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}