package scatter.point.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.point.pojo.po.PointDetail;
import scatter.point.pojo.form.PointDetailAddForm;
import scatter.point.pojo.form.PointDetailUpdateForm;
import scatter.point.pojo.form.PointDetailPageQueryForm;
import scatter.point.rest.test.PointDetailSuperTest;
import scatter.point.rest.service.IPointDetailService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 积分详情 服务测试类
* </p>
*
* @author yw
* @since 2021-02-20
*/
@SpringBootTest
public class PointDetailServiceTest extends PointDetailSuperTest{

    @Autowired
    private IPointDetailService pointDetailService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PointDetail> pos = pointDetailService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}