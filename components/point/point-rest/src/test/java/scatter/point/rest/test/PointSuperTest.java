package scatter.point.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.point.pojo.po.Point;
import scatter.point.pojo.form.PointAddForm;
import scatter.point.pojo.form.PointUpdateForm;
import scatter.point.pojo.form.PointPageQueryForm;
import scatter.point.rest.service.IPointService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 积分 测试类基类
* </p>
*
* @author yw
* @since 2021-02-20
*/
public class PointSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPointService pointService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return pointService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return pointService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Point mockPo() {
        return JMockData.mock(Point.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PointAddForm mockAddForm() {
        return JMockData.mock(PointAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PointUpdateForm mockUpdateForm() {
        return JMockData.mock(PointUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PointPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PointPageQueryForm.class, mockConfig);
    }
}