package scatter.point.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.point.pojo.po.PointDetail;
import scatter.point.pojo.form.PointDetailAddForm;
import scatter.point.pojo.form.PointDetailUpdateForm;
import scatter.point.pojo.form.PointDetailPageQueryForm;
import scatter.point.rest.service.IPointDetailService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 积分详情 测试类基类
* </p>
*
* @author yw
* @since 2021-02-20
*/
public class PointDetailSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPointDetailService pointDetailService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return pointDetailService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return pointDetailService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PointDetail mockPo() {
        return JMockData.mock(PointDetail.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PointDetailAddForm mockAddForm() {
        return JMockData.mock(PointDetailAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PointDetailUpdateForm mockUpdateForm() {
        return JMockData.mock(PointDetailUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PointDetailPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PointDetailPageQueryForm.class, mockConfig);
    }
}