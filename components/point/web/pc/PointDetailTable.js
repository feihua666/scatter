const PointDetailTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'pointId',
        label: '积分id'
    },
    {
        prop: 'beforePoints',
        label: '记录前积分数'
    },
    {
        prop: 'afterPoints',
        label: '记录后积分数'
    },
    {
        prop: 'isPlus',
        label: '是否加积分'
    },
    {
        prop: 'points',
        label: '加或减的积分数'
    },
    {
        prop: 'description',
        label: '记录描述'
    },
    {
        prop: 'relationDataId',
        label: '关联的数据id'
    },
    {
        prop: 'relationDataType',
        label: '关联的数据类型'
    },
    {
        prop: 'relationDataContent',
        label: '关联的数据内容'
    },
]
export default PointDetailTable