import PointUrl from './PointUrl.js'
const PointForm = [
    {
        PointSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        PointSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'points',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '当前积分数',
            required: true,
        }
    },
]
export default PointForm