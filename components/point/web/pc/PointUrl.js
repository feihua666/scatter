const basePath = '' + '/point'
const PointUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PointSearchList',
        add: '/PointAdd',
        update: '/PointUpdate',
    }
}
export default PointUrl