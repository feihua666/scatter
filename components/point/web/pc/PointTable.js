const PointTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'points',
        label: '当前积分数'
    },
]
export default PointTable