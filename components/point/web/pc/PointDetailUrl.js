const basePath = '' + '/point-detail'
const PointDetailUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PointDetailSearchList',
        add: '/PointDetailAdd',
        update: '/PointDetailUpdate',
    }
}
export default PointDetailUrl