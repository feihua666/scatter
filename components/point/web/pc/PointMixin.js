import PointForm from './PointForm.js'
import PointTable from './PointTable.js'
import PointUrl from './PointUrl.js'
const PointMixin = {
    computed: {
        PointFormOptions() {
            return this.$stDynamicFormTools.options(PointForm,this.$options.name)
        },
        PointTableOptions() {
            return PointTable
        },
        PointUrl(){
            return PointUrl
        }
    },
}
export default PointMixin