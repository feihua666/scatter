import PointUrl from './PointUrl.js'
const PointRoute = [
    {
        path: PointUrl.router.searchList,
        component: () => import('./element/PointSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PointSearchList',
            name: '积分管理'
        }
    },
    {
        path: PointUrl.router.add,
        component: () => import('./element/PointAdd'),
        meta: {
            code:'PointAdd',
            name: '积分添加'
        }
    },
    {
        path: PointUrl.router.update,
        component: () => import('./element/PointUpdate'),
        meta: {
            code:'PointUpdate',
            name: '积分修改'
        }
    },
]
export default PointRoute