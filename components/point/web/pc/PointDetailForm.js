import PointDetailUrl from './PointDetailUrl.js'
const PointDetailForm = [
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'pointId',
        },
        element:{
            label: '积分id',
            required: true,
        }
    },
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'beforePoints',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '记录前积分数',
            required: true,
        }
    },
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'afterPoints',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '记录后积分数',
            required: true,
        }
    },
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isPlus',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否加积分',
            required: true,
        }
    },
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'points',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '加或减的积分数',
            required: true,
        }
    },
    {
        PointDetailSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'description',
        },
        element:{
            label: '记录描述',
            required: true,
        }
    },
    {
        field: {
            name: 'relationDataId',
        },
        element:{
            label: '关联的数据id',
        }
    },
    {
        field: {
            name: 'relationDataType',
        },
        element:{
            label: '关联的数据类型',
        }
    },
    {
        field: {
            name: 'relationDataContent',
        },
        element:{
            label: '关联的数据内容',
        }
    },
]
export default PointDetailForm