import PointDetailUrl from './PointDetailUrl.js'
const PointDetailRoute = [
    {
        path: PointDetailUrl.router.searchList,
        component: () => import('./element/PointDetailSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PointDetailSearchList',
            name: '积分详情管理'
        }
    },
    {
        path: PointDetailUrl.router.add,
        component: () => import('./element/PointDetailAdd'),
        meta: {
            code:'PointDetailAdd',
            name: '积分详情添加'
        }
    },
    {
        path: PointDetailUrl.router.update,
        component: () => import('./element/PointDetailUpdate'),
        meta: {
            code:'PointDetailUpdate',
            name: '积分详情修改'
        }
    },
]
export default PointDetailRoute