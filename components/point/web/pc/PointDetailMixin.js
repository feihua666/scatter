import PointDetailForm from './PointDetailForm.js'
import PointDetailTable from './PointDetailTable.js'
import PointDetailUrl from './PointDetailUrl.js'
const PointDetailMixin = {
    computed: {
        PointDetailFormOptions() {
            return this.$stDynamicFormTools.options(PointDetailForm,this.$options.name)
        },
        PointDetailTableOptions() {
            return PointDetailTable
        },
        PointDetailUrl(){
            return PointDetailUrl
        }
    },
}
export default PointDetailMixin