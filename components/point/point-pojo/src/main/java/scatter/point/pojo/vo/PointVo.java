package scatter.point.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 积分响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Setter
@Getter
@ApiModel(value="积分响应对象")
public class PointVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id,外键")
    private String userId;

    @ApiModelProperty(value = "当前积分数")
    private Integer points;

}
