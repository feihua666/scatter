package scatter.point.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 积分添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Setter
@Getter
@ApiModel(value="积分添加表单对象")
public class PointAddForm extends BaseAddForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id,外键",required = true)
    private String userId;

    @NotNull(message="当前积分数不能为空")
    @ApiModelProperty(value = "当前积分数",required = true)
    private Integer points;

}
