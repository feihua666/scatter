package scatter.point.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 积分分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Setter
@Getter
@ApiModel(value="积分分页表单对象")
public class PointPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id,外键")
    private String userId;

    @ApiModelProperty(value = "当前积分数")
    private Integer points;

}
