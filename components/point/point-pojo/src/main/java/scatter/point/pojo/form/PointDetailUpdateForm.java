package scatter.point.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 积分详情更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Setter
@Getter
@ApiModel(value="积分详情更新表单对象")
public class PointDetailUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id,外键",required = true)
    private String userId;

    @NotEmpty(message="积分id不能为空")
    @ApiModelProperty(value = "积分id,外键",required = true)
    private String pointId;

    @NotNull(message="记录前积分数不能为空")
    @ApiModelProperty(value = "记录前积分数",required = true)
    private Integer beforePoints;

    @NotNull(message="记录后积分数不能为空")
    @ApiModelProperty(value = "记录后积分数",required = true)
    private Integer afterPoints;

    @NotNull(message="是否加积分不能为空")
    @ApiModelProperty(value = "是否加积分，不是加就是减",required = true)
    private Boolean isPlus;

    @NotNull(message="加或减的积分数不能为空")
    @ApiModelProperty(value = "加或减的积分数",required = true)
    private Integer points;

    @NotEmpty(message="记录描述不能为空")
    @ApiModelProperty(value = "记录描述",required = true)
    private String description;

    @ApiModelProperty(value = "关联的数据id,外键")
    private String relationDataId;

    @ApiModelProperty(value = "关联的数据类型")
    private String relationDataType;

    @ApiModelProperty(value = "关联的数据内容，快照")
    private String relationDataContent;

}
