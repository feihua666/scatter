package scatter.point.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分详情表
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_point_detail")
@ApiModel(value="PointDetail对象", description="积分详情表")
public class PointDetail extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_POINTDETAIL_BY_ID = "trans_pointdetail_by_id_scatter.point.pojo.po";

    @ApiModelProperty(value = "用户id,外键")
    private String userId;

    @ApiModelProperty(value = "积分id,外键")
    private String pointId;

    @ApiModelProperty(value = "记录前积分数")
    private Integer beforePoints;

    @ApiModelProperty(value = "记录后积分数")
    private Integer afterPoints;

    @ApiModelProperty(value = "是否加积分，不是加就是减")
    private Boolean isPlus;

    @ApiModelProperty(value = "加或减的积分数")
    private Integer points;

    @ApiModelProperty(value = "记录描述")
    private String description;

    @ApiModelProperty(value = "关联的数据id,外键")
    private String relationDataId;

    @ApiModelProperty(value = "关联的数据类型")
    private String relationDataType;

    @ApiModelProperty(value = "关联的数据内容，快照")
    private String relationDataContent;


}
