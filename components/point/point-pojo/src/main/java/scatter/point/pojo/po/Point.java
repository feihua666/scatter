package scatter.point.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分表
 * </p>
 *
 * @author yw
 * @since 2021-02-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_point")
@ApiModel(value="Point对象", description="积分表")
public class Point extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_POINT_BY_ID = "trans_point_by_id_scatter.point.pojo.po";

    @ApiModelProperty(value = "用户id,外键")
    private String userId;

    @ApiModelProperty(value = "当前积分数")
    private Integer points;


}
