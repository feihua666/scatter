package scatter.job.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 职务级别分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="职务级别分页表单对象")
public class JobLevelPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "职务级别编码")
    private String code;

    @ApiModelProperty(value = "职务级别名称")
    private String name;

    @ApiModelProperty(value = "职务id")
    private String jobId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
