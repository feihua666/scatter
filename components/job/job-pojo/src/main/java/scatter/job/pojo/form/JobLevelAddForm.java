package scatter.job.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 职务级别添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="职务级别添加表单对象")
public class JobLevelAddForm extends BaseAddForm {

    @NotEmpty(message="职务级别编码不能为空")
    @ApiModelProperty(value = "职务级别编码",required = true)
    private String code;

    @NotEmpty(message="职务级别名称不能为空")
    @ApiModelProperty(value = "职务级别名称",required = true)
    private String name;

    @NotEmpty(message="职务id不能为空")
    @ApiModelProperty(value = "职务id",required = true)
    private String jobId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
