package scatter.job.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 职务级别表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_job_level")
@ApiModel(value="JobLevel对象", description="职务级别表")
public class JobLevel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_JOBLEVEL_BY_ID = "trans_joblevel_by_id_scatter.job.pojo.po";

    @ApiModelProperty(value = "职务级别编码")
    private String code;

    @ApiModelProperty(value = "职务级别名称")
    private String name;

    @ApiModelProperty(value = "职务id")
    private String jobId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
