package scatter.job.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 职务表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_job")
@ApiModel(value="Job对象", description="职务表")
public class Job extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_JOB_BY_ID = "trans_job_by_id_scatter.job.pojo.po";

    @ApiModelProperty(value = "职务编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "职务名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "是否公共")
    private Boolean isPublic;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
