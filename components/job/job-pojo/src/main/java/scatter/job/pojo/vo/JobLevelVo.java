package scatter.job.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.job.pojo.po.Job;


/**
 * <p>
 * 职务级别响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="职务级别响应对象")
public class JobLevelVo extends BaseIdVo {

    @ApiModelProperty(value = "职务级别编码")
    private String code;

    @ApiModelProperty(value = "职务级别名称")
    private String name;

    @ApiModelProperty(value = "职务id")
    private String jobId;

    @TransBy(type = Job.TRANS_JOB_BY_ID,byFieldName = "jobId",mapValueField = "name")
    @ApiModelProperty(value = "职务名称")
    private String jobName;

    @ApiModelProperty(value = "描述")
    private String remark;

}
