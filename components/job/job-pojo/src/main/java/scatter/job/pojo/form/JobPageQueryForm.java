package scatter.job.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 职务分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="职务分页表单对象")
public class JobPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "职务编码,模糊查询")
    private String code;

    @Like
    @ApiModelProperty(value = "职务名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "是否公共")
    private Boolean isPublic;

    @ApiModelProperty(value = "是否包含公共，如果isPublic有值则忽略该条件")
    private Boolean isIncludePublic;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
