package scatter.job.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.job.pojo.po.Job;
import scatter.job.rest.service.IJobService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 职务表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/job")
public class JobInnerController extends BaseInnerController<Job> {
 @Autowired
 private IJobService jobService;

 public IJobService getService(){
     return jobService;
 }
}
