package scatter.job.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 职务表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
@FeignClient(value = "Job-client")
public interface JobClient {

}
