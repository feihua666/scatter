import DeptUrl from '../../../dept/web/pc/DeptUrl.js'
const JobForm = [
    {
        JobSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '职务编码',
            required: true,
        }
    },
    {
        JobSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '职务名称',
            required: true,
        }
    },
    {
        JobSearchList: false,
        field: {
            name: 'isPublic',
            valueDefault: true
        },
        element:{
            label: '是否公共',
            required: true,
            type: 'switch'
        }
    },
    {
        JobSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'typeDictId'
        },
        element:{
            label: '类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'job_type'
            }
        }
    },
    {
        JobSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'deptId'
        },
        element:{
            label: '部门',
            type: 'cascader',
            options: {
                datas: DeptUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            required: ({form})=>{
                if (form.isPublic === true) {
                    form.deptId = ''
                    return false
                }
                return true
            }
        }
    },
    {

        JobSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default JobForm