const basePath = '' + '/job'
const JobUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/JobSearchList',
        add: '/JobAdd',
        update: '/JobUpdate',
    }
}
export default JobUrl