import JobUrl from './JobUrl.js'
const JobLevelForm = [
    {
        JobLevelSearchList:{
            element:{
                required: false,
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '职务级别编码',
            required: true,
        }
    },
    {
        JobLevelSearchList:{
            element:{
                required: false,
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '职务级别名称',
            required: true,
        }
    },
    {
        JobLevelSearchList:{
            element:{
                required: false,
            }
        },
        field: {
            name: 'jobId'
        },
        element:{
            label: '职务',
            required: true,
            type: 'select',
            options: {
                datas: JobUrl.list
            }
        }
    },
    {
        JobLevelSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default JobLevelForm