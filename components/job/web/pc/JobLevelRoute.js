import JobLevelUrl from './JobLevelUrl.js'

const JobLevelRoute = [
    {
        path: JobLevelUrl.router.searchList,
        component: () => import('./element/JobLevelSearchList'),
        meta: {
            code:'JobLevelSearchList',
            name: '职务级别管理'
        }
    },
    {
        path: JobLevelUrl.router.add,
        component: () => import('./element/JobLevelAdd'),
        meta: {
            code:'JobLevelAdd',
            name: '职务级别添加'
        }
    },
    {
        path: JobLevelUrl.router.update,
        component: () => import('./element/JobLevelUpdate'),
        meta: {
            code:'JobLevelUpdate',
            name: '职务级别修改'
        }
    },
]
export default JobLevelRoute