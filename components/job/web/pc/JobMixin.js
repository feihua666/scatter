import JobForm from './JobForm.js'
import JobTable from './JobTable.js'
import JobUrl from './JobUrl.js'

const JobMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(JobForm,this.$options.name)
        },
        computedTableOptions() {
            return JobTable
        },
        computedUrl(){
            return JobUrl
        }
    },
}
export default JobMixin