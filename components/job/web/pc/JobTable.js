const JobTable = [
    {
        prop: 'code',
        label: '职务编码'
    },
    {
        prop: 'name',
        label: '职务名称'
    },
    {
        prop: 'isPublic',
        label: '是否公共'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'deptName',
        label: '部门名称'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default JobTable