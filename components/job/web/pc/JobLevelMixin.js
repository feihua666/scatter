import JobLevelForm from './JobLevelForm.js'
import JobLevelTable from './JobLevelTable.js'
import JobLevelUrl from './JobLevelUrl.js'

const JobLevelMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(JobLevelForm,this.$options.name)
        },
        computedTableOptions() {
            return JobLevelTable
        },
        computedUrl(){
            return JobLevelUrl
        }
    },
}
export default JobLevelMixin