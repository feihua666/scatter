const basePath = '' + '/job-level'
const JobLevelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/JobLevelSearchList',
        add: '/JobLevelAdd',
        update: '/JobLevelUpdate',
    }
}
export default JobLevelUrl