const JobLevelTable = [
    {
        prop: 'code',
        label: '职务级别编码'
    },
    {
        prop: 'name',
        label: '职务级别名称'
    },
    {
        prop: 'jobName',
        label: '职务'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default JobLevelTable