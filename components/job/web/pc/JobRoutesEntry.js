import JobLevelRoute from './JobLevelRoute.js'
import JobRoute from './JobRoute.js'

const JobRoutesEntry = [
]
.concat(JobLevelRoute)
.concat(JobRoute)

export default JobRoutesEntry