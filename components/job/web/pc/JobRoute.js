import JobUrl from './JobUrl.js'

const JobRoute = [
    {
        path: JobUrl.router.searchList,
        component: () => import('./element/JobSearchList'),
        meta: {
            code:'JobSearchList',
            name: '职务管理'
        }
    },
    {
        path: JobUrl.router.add,
        component: () => import('./element/JobAdd'),
        meta: {
            code:'JobAdd',
            name: '职务添加'
        }
    },
    {
        path: JobUrl.router.update,
        component: () => import('./element/JobUpdate'),
        meta: {
            code:'JobUpdate',
            name: '职务修改'
        }
    },
]
export default JobRoute