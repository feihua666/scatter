package scatter.job.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.job.pojo.po.JobLevel;
import scatter.job.rest.service.IJobLevelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 职务级别表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/job-level")
public class JobLevelInnerController extends BaseInnerController<JobLevel> {
 @Autowired
 private IJobLevelService jobLevelService;

 public IJobLevelService getService(){
     return jobLevelService;
 }
}
