package scatter.job.rest.mapper;

import scatter.job.pojo.po.Job;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 职务表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface JobMapper extends IBaseMapper<Job> {

}
