package scatter.job.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.job.pojo.form.JobAddForm;
import scatter.job.pojo.form.JobPageQueryForm;
import scatter.job.pojo.form.JobUpdateForm;
import scatter.job.pojo.po.Job;
import scatter.job.rest.mapper.JobMapper;
import scatter.job.rest.service.IJobService;
/**
 * <p>
 * 职务表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class JobServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<JobMapper, Job, JobAddForm, JobUpdateForm, JobPageQueryForm> implements IJobService {
    @Override
    public void preAdd(JobAddForm addForm,Job po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 职务编码已存在不能添加
            assertByColumn(addForm.getCode(),Job::getCode,false);
        }

    }

    @Override
    public void preUpdate(JobUpdateForm updateForm,Job po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            Job byId = getById(updateForm.getId());
            // 如果职务编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 职务编码已存在不能修改
                assertByColumn(updateForm.getCode(),Job::getCode,false);
            }
        }

    }
    @Override
    public QueryWrapper<Job> prepareQueryFormQueryWrapper(QueryWrapper<Job> queryWrapper, JobPageQueryForm queryForm) {
        if (queryForm.getIsPublic() == null && queryForm.getIsIncludePublic() != null && queryForm.getIsIncludePublic()) {
            queryWrapper = convertEntityConditionToWrapper(queryWrapper);
            queryWrapper.lambda().or().eq(Job::getIsPublic, true);
        }
        return queryWrapper;
    }
}
