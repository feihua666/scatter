package scatter.job.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.job.rest.JobConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.job.pojo.po.Job;
import scatter.job.pojo.vo.JobVo;
import scatter.job.pojo.form.JobAddForm;
import scatter.job.pojo.form.JobUpdateForm;
import scatter.job.pojo.form.JobPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 职务表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(JobConfiguration.CONTROLLER_BASE_PATH + "/job")
@Api(tags = "职务")
public class JobController extends BaseAddUpdateQueryFormController<Job, JobVo, JobAddForm, JobUpdateForm, JobPageQueryForm> {


     @Override
	 @ApiOperation("添加职务")
     @PreAuthorize("hasAuthority('Job:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public JobVo add(@RequestBody @Valid JobAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询职务")
     @PreAuthorize("hasAuthority('Job:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public JobVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除职务")
     @PreAuthorize("hasAuthority('Job:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新职务")
     @PreAuthorize("hasAuthority('Job:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public JobVo update(@RequestBody @Valid JobUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询职务")
    @PreAuthorize("hasAuthority('Job:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<JobVo> getList(JobPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询职务")
    @PreAuthorize("hasAuthority('Job:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<JobVo> getPage(JobPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
