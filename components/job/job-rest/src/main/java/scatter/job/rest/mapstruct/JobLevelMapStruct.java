package scatter.job.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.job.pojo.form.JobLevelAddForm;
import scatter.job.pojo.form.JobLevelPageQueryForm;
import scatter.job.pojo.form.JobLevelUpdateForm;
import scatter.job.pojo.po.JobLevel;
import scatter.job.pojo.vo.JobLevelVo;

/**
 * <p>
 * 职务级别 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JobLevelMapStruct extends IBaseVoMapStruct<JobLevel, JobLevelVo>,
        IBaseAddFormMapStruct<JobLevel,JobLevelAddForm>,
        IBaseUpdateFormMapStruct<JobLevel,JobLevelUpdateForm>,
        IBaseQueryFormMapStruct<JobLevel,JobLevelPageQueryForm> {
    JobLevelMapStruct INSTANCE = Mappers.getMapper( JobLevelMapStruct.class );

}
