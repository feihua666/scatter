package scatter.job.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.job.rest.JobConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.job.pojo.po.JobLevel;
import scatter.job.pojo.vo.JobLevelVo;
import scatter.job.pojo.form.JobLevelAddForm;
import scatter.job.pojo.form.JobLevelUpdateForm;
import scatter.job.pojo.form.JobLevelPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 职务级别表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(JobConfiguration.CONTROLLER_BASE_PATH + "/job-level")
@Api(tags = "职务级别")
public class JobLevelController extends BaseAddUpdateQueryFormController<JobLevel, JobLevelVo, JobLevelAddForm, JobLevelUpdateForm, JobLevelPageQueryForm> {


     @Override
	 @ApiOperation("添加职务级别")
     @PreAuthorize("hasAuthority('JobLevel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public JobLevelVo add(@RequestBody @Valid JobLevelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询职务级别")
     @PreAuthorize("hasAuthority('JobLevel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public JobLevelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除职务级别")
     @PreAuthorize("hasAuthority('JobLevel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新职务级别")
     @PreAuthorize("hasAuthority('JobLevel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public JobLevelVo update(@RequestBody @Valid JobLevelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询职务级别")
    @PreAuthorize("hasAuthority('JobLevel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<JobLevelVo> getList(JobLevelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询职务级别")
    @PreAuthorize("hasAuthority('JobLevel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<JobLevelVo> getPage(JobLevelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
