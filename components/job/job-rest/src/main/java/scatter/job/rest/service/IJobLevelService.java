package scatter.job.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.job.pojo.po.JobLevel;
/**
 * <p>
 * 职务级别表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IJobLevelService extends IBaseService<JobLevel> {


}
