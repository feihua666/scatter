package scatter.job.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.job.pojo.po.Job;
import scatter.job.rest.service.IJobService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 职务翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
public class JobTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IJobService jobService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Job.TRANS_JOB_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Job.TRANS_JOB_BY_ID)) {
            Job byId = jobService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Job.TRANS_JOB_BY_ID)) {
            return jobService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
