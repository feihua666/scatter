package scatter.job.rest.mapper;

import scatter.job.pojo.po.JobLevel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 职务级别表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface JobLevelMapper extends IBaseMapper<JobLevel> {

}
