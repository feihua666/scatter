package scatter.job.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.job.pojo.form.JobLevelAddForm;
import scatter.job.pojo.form.JobLevelPageQueryForm;
import scatter.job.pojo.form.JobLevelUpdateForm;
import scatter.job.pojo.po.JobLevel;
import scatter.job.rest.mapper.JobLevelMapper;
import scatter.job.rest.service.IJobLevelService;
/**
 * <p>
 * 职务级别表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class JobLevelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<JobLevelMapper, JobLevel, JobLevelAddForm, JobLevelUpdateForm, JobLevelPageQueryForm> implements IJobLevelService {
    @Override
    public void preAdd(JobLevelAddForm addForm,JobLevel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(JobLevelUpdateForm updateForm,JobLevel po) {
        super.preUpdate(updateForm,po);

    }
}
