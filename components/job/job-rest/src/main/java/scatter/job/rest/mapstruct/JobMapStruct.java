package scatter.job.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.job.pojo.form.JobAddForm;
import scatter.job.pojo.form.JobPageQueryForm;
import scatter.job.pojo.form.JobUpdateForm;
import scatter.job.pojo.po.Job;
import scatter.job.pojo.vo.JobVo;

/**
 * <p>
 * 职务 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JobMapStruct extends IBaseVoMapStruct<Job, JobVo>,
        IBaseAddFormMapStruct<Job,JobAddForm>,
        IBaseUpdateFormMapStruct<Job,JobUpdateForm>,
        IBaseQueryFormMapStruct<Job,JobPageQueryForm> {
    JobMapStruct INSTANCE = Mappers.getMapper( JobMapStruct.class );

}
