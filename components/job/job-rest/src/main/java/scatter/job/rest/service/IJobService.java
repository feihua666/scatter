package scatter.job.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.job.pojo.po.Job;
/**
 * <p>
 * 职务表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IJobService extends IBaseService<Job> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Job getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Job>lambdaQuery().eq(Job::getCode, code));
    }

}
