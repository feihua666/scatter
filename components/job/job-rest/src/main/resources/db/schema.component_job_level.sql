DROP TABLE IF EXISTS component_job_level;
CREATE TABLE `component_job_level` (
  `id` varchar(20) NOT NULL COMMENT '字典ID',
  `code` varchar(100) NOT NULL COMMENT '职务级别编码',
  `name` varchar(100) NOT NULL COMMENT '职务级别名称',
  `job_id` varchar(20) NOT NULL COMMENT '职务id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `value` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `job_id` (`job_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='职务级别表';
