DROP TABLE IF EXISTS component_job;
CREATE TABLE `component_job` (
  `id` varchar(20) NOT NULL COMMENT '字典ID',
  `code` varchar(100) NOT NULL COMMENT '职务编码,模糊查询',
  `name` varchar(100) NOT NULL COMMENT '职务名称,模糊查询',
  `is_public` tinyint(1) NOT NULL COMMENT '是否公共',
  `type_dict_id` varchar(20) NOT NULL COMMENT '类型，字典id',
  `dept_id` varchar(20) DEFAULT NULL COMMENT '部门id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `is_public` (`is_public`) USING BTREE,
  KEY `dept_id` (`dept_id`) USING BTREE,
  KEY `type_dict_id` (`type_dict_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='职务表';
