package scatter.job.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.job.pojo.po.JobLevel;
import scatter.job.pojo.form.JobLevelAddForm;
import scatter.job.pojo.form.JobLevelUpdateForm;
import scatter.job.pojo.form.JobLevelPageQueryForm;
import scatter.job.rest.test.JobLevelSuperTest;
import scatter.job.rest.service.IJobLevelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 职务级别 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class JobLevelServiceTest extends JobLevelSuperTest{

    @Autowired
    private IJobLevelService jobLevelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<JobLevel> pos = jobLevelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}