package scatter.job.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.job.pojo.po.Job;
import scatter.job.pojo.form.JobAddForm;
import scatter.job.pojo.form.JobUpdateForm;
import scatter.job.pojo.form.JobPageQueryForm;
import scatter.job.rest.test.JobSuperTest;
import scatter.job.rest.service.IJobService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 职务 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class JobServiceTest extends JobSuperTest{

    @Autowired
    private IJobService jobService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Job> pos = jobService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}