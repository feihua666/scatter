package scatter.job.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.job.pojo.po.JobLevel;
import scatter.job.pojo.form.JobLevelAddForm;
import scatter.job.pojo.form.JobLevelUpdateForm;
import scatter.job.pojo.form.JobLevelPageQueryForm;
import scatter.job.rest.service.IJobLevelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 职务级别 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class JobLevelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IJobLevelService jobLevelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return jobLevelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return jobLevelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public JobLevel mockPo() {
        return JMockData.mock(JobLevel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public JobLevelAddForm mockAddForm() {
        return JMockData.mock(JobLevelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public JobLevelUpdateForm mockUpdateForm() {
        return JMockData.mock(JobLevelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public JobLevelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(JobLevelPageQueryForm.class, mockConfig);
    }
}