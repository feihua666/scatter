package scatter.job.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.job.pojo.po.Job;
import scatter.job.pojo.form.JobAddForm;
import scatter.job.pojo.form.JobUpdateForm;
import scatter.job.pojo.form.JobPageQueryForm;
import scatter.job.rest.service.IJobService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 职务 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class JobSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IJobService jobService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return jobService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return jobService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Job mockPo() {
        return JMockData.mock(Job.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public JobAddForm mockAddForm() {
        return JMockData.mock(JobAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public JobUpdateForm mockUpdateForm() {
        return JMockData.mock(JobUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public JobPageQueryForm mockPageQueryForm() {
        return JMockData.mock(JobPageQueryForm.class, mockConfig);
    }
}