package scatter.comp.pojo.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;

/**
 * Created by yangwei
 * Created at 2021/3/23 16:58
 */
@Setter
@Getter
@ApiModel(value="公司负责人响应对象")
public class CompMasterUserVo extends BaseIdVo {

    private String nickname;
}
