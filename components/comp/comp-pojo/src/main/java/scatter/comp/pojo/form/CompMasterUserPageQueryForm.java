package scatter.comp.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.Like;

import javax.validation.constraints.NotEmpty;


/**
 * <p>
 * 公司负责人分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Setter
@Getter
@ApiModel(value="公司负责人分页表单对象")
public class CompMasterUserPageQueryForm extends BasePageQueryForm {

    @NotEmpty(message = "用户昵称不能为空")
    @ApiModelProperty(value = "用户昵称")
    private String nickname;

}
