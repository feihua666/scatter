package scatter.comp.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.comp.pojo.po.Comp;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 公司响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="公司响应对象")
public class CompVo extends BaseIdVo {

    @ApiModelProperty(value = "公司编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "公司名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "类型,字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="name")
    @ApiModelProperty(value = "类型，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "负责人用户id，该id可用来填充审批人")
    private String masterUserId;

    @TransBy(type = Comp.TRANS_COMP_MASTER_USER_NICKNAME_BY_MASTER_USER_ID,byFieldName = "masterUserId")
    @ApiModelProperty(value = "负责人用户昵称")
    private String masterUserNickname;

    @ApiModelProperty(value = "是否虚拟公司")
    private Boolean isVirtual;

    @ApiModelProperty(value = "描述、备注")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;


    @TransBy(type = Comp.TRANS_COMP_BY_ID,byFieldName = "parentId",mapValueField="name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
