package scatter.comp.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公司表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_comp")
@ApiModel(value="Comp对象", description="公司表")
public class Comp extends BaseTreePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMP_BY_ID = "trans_comp_by_id_scatter.comp.pojo.po";
    // 翻译负责人昵称
    public static final String TRANS_COMP_MASTER_USER_NICKNAME_BY_MASTER_USER_ID = "trans_comp_master_user_nickname_by_master_user_id";

    @ApiModelProperty(value = "公司编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "公司名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "类型,字典id")
    private String typeDictId;

    @ApiModelProperty(value = "负责人用户id，该id可用来填充审批人")
    private String masterUserId;

    @ApiModelProperty(value = "是否虚拟公司")
    private Boolean isVirtual;

    @ApiModelProperty(value = "描述、备注")
    private String remark;


}
