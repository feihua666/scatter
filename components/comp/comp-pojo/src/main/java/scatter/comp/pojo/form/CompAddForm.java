package scatter.comp.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 公司添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="公司添加表单对象")
public class CompAddForm extends BaseAddForm {

    @NotEmpty(message="公司编码不能为空")
    @ApiModelProperty(value = "公司编码,模糊查询",required = true)
    private String code;

    @NotEmpty(message="公司名称不能为空")
    @ApiModelProperty(value = "公司名称,模糊查询",required = true)
    private String name;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型,字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "负责人用户id，该id可用来填充审批人")
    private String masterUserId;

    @NotNull(message="是否虚拟公司不能为空")
    @ApiModelProperty(value = "是否虚拟公司",required = true)
    private Boolean isVirtual;

    @ApiModelProperty(value = "描述、备注")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
