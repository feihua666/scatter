package scatter.comp.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comp.pojo.po.Comp;
import scatter.comp.pojo.form.CompAddForm;
import scatter.comp.pojo.form.CompUpdateForm;
import scatter.comp.pojo.form.CompPageQueryForm;
import scatter.comp.rest.service.ICompService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 公司 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class CompSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ICompService compService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return compService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return compService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Comp mockPo() {
        return JMockData.mock(Comp.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public CompAddForm mockAddForm() {
        return JMockData.mock(CompAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public CompUpdateForm mockUpdateForm() {
        return JMockData.mock(CompUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public CompPageQueryForm mockPageQueryForm() {
        return JMockData.mock(CompPageQueryForm.class, mockConfig);
    }
}