package scatter.comp.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.comp.pojo.po.Comp;
import scatter.comp.pojo.form.CompAddForm;
import scatter.comp.pojo.form.CompUpdateForm;
import scatter.comp.pojo.form.CompPageQueryForm;
import scatter.comp.rest.test.CompSuperTest;
import scatter.comp.rest.service.ICompService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 公司 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class CompServiceTest extends CompSuperTest{

    @Autowired
    private ICompService compService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Comp> pos = compService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}