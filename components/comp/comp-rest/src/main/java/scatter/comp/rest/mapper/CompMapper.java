package scatter.comp.rest.mapper;

import scatter.comp.pojo.po.Comp;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 公司表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface CompMapper extends IBaseMapper<Comp> {

}
