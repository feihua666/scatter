package scatter.comp.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.comp.pojo.po.Comp;
/**
 * <p>
 * 公司表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface ICompService extends IBaseService<Comp> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Comp getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Comp>lambdaQuery().eq(Comp::getCode, code));
    }

}
