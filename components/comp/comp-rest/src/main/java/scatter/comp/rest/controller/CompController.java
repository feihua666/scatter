package scatter.comp.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.comp.rest.CompConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.comp.pojo.po.Comp;
import scatter.comp.pojo.vo.CompVo;
import scatter.comp.pojo.form.CompAddForm;
import scatter.comp.pojo.form.CompUpdateForm;
import scatter.comp.pojo.form.CompPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 公司表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(CompConfiguration.CONTROLLER_BASE_PATH + "/comp")
@Api(tags = "公司")
public class CompController extends BaseAddUpdateQueryFormController<Comp, CompVo, CompAddForm, CompUpdateForm, CompPageQueryForm> {


     @Override
	 @ApiOperation("添加公司")
     @PreAuthorize("hasAuthority('Comp:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CompVo add(@RequestBody @Valid CompAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询公司")
     @PreAuthorize("hasAuthority('Comp:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public CompVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除公司")
     @PreAuthorize("hasAuthority('Comp:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新公司")
     @PreAuthorize("hasAuthority('Comp:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public CompVo update(@RequestBody @Valid CompUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询公司")
    @PreAuthorize("hasAuthority('Comp:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<CompVo> getList(CompPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询公司")
    @PreAuthorize("hasAuthority('Comp:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CompVo> getPage(CompPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
