package scatter.comp.rest.masteruser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.SuperController;
import scatter.comp.pojo.form.CompMasterUserPageQueryForm;
import scatter.comp.pojo.vo.CompMasterUserVo;
import scatter.comp.rest.CompConfiguration;

import javax.validation.Valid;

/**
 * 公司负责人相关
 * Created by yangwei
 * Created at 2021/3/23 16:56
 */
@RestController
@RequestMapping(CompConfiguration.CONTROLLER_BASE_PATH + "/comp")
@Api(tags = "公司")
public class CompMasterUserController extends SuperController {

    @Autowired(required = false)
    private ICompMasterUserService iMasterUserService;


    @ApiOperation("分页查询公司负责人")
    @PreAuthorize("hasAuthority('Comp:masteruser:getPage')")
    @GetMapping("/masteruser/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<CompMasterUserVo> getPage(@Valid CompMasterUserPageQueryForm listPageForm) {
        Assert.notNull(iMasterUserService,"你必须实现接口 scatter.comp.rest.masteruser.IMasterUserService 来查询用户");
        return iMasterUserService.getPage(listPageForm);
    }
}
