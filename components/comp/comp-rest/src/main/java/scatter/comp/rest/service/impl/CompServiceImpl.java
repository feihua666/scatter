package scatter.comp.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.comp.pojo.form.CompAddForm;
import scatter.comp.pojo.form.CompPageQueryForm;
import scatter.comp.pojo.form.CompUpdateForm;
import scatter.comp.pojo.po.Comp;
import scatter.comp.rest.mapper.CompMapper;
import scatter.comp.rest.service.ICompService;
/**
 * <p>
 * 公司表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class CompServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<CompMapper, Comp, CompAddForm, CompUpdateForm, CompPageQueryForm> implements ICompService {
    @Override
    public void preAdd(CompAddForm addForm,Comp po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 公司编码已存在不能添加
            assertByColumn(addForm.getCode(),Comp::getCode,false);
        }

    }

    @Override
    public void preUpdate(CompUpdateForm updateForm,Comp po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            Comp byId = getById(updateForm.getId());
            // 如果公司编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 公司编码已存在不能修改
                assertByColumn(updateForm.getCode(),Comp::getCode,false);
            }
        }

    }
}
