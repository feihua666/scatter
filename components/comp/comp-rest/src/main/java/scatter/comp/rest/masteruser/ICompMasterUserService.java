package scatter.comp.rest.masteruser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import scatter.comp.pojo.form.CompMasterUserPageQueryForm;
import scatter.comp.pojo.vo.CompMasterUserVo;

/**
 * 为了解藕这里定义一个service，实现留给扩展
 * Created by yangwei
 * Created at 2021/3/23 17:01
 */
public interface ICompMasterUserService {

    IPage<CompMasterUserVo> getPage(CompMasterUserPageQueryForm listPageForm);
}
