package scatter.comp.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.comp.pojo.po.Comp;
import scatter.comp.rest.service.ICompService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 公司表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/comp")
public class CompInnerController extends BaseInnerController<Comp> {
 @Autowired
 private ICompService compService;

 public ICompService getService(){
     return compService;
 }
}
