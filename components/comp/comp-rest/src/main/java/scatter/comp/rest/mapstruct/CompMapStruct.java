package scatter.comp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.comp.pojo.form.CompAddForm;
import scatter.comp.pojo.form.CompPageQueryForm;
import scatter.comp.pojo.form.CompUpdateForm;
import scatter.comp.pojo.po.Comp;
import scatter.comp.pojo.vo.CompVo;

/**
 * <p>
 * 公司 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CompMapStruct extends IBaseVoMapStruct<Comp, CompVo>,
        IBaseAddFormMapStruct<Comp,CompAddForm>,
        IBaseUpdateFormMapStruct<Comp,CompUpdateForm>,
        IBaseQueryFormMapStruct<Comp,CompPageQueryForm> {
    CompMapStruct INSTANCE = Mappers.getMapper( CompMapStruct.class );

}
