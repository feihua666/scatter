import CompUrl from './CompUrl.js'

const CompForm = [
    {
        CompSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '公司编码',
            required: true,
        }
    },
    {
        CompSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '公司名称',
            required: true,
        }
    },
    {
        CompSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'typeDictId'
        },
        element:{
            label: '类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'comp_type'
            },

        }
    },
    {

        CompSearchList: false,
        field: {
            name: 'masterUserId'
        },
        element:{
            type: 'select',
            label: '负责人',
            options: ({$route,$vm})=>{
                let datas = []
                // 在修改的时候回显
                if ($route.query.masterUserId) {
                    datas.push({
                        id: $route.query.masterUserId,
                        nickname: $route.query.masterUserNickname
                    })
                }
                return {
                    datas: datas,
                    optionProp:{
                        value: 'id', // 选中的值属性
                        label: 'nickname', // 显示的值属性
                    },
                    originProp: {
                        placeholder: '请按用户昵称搜索',
                        remote: true,
                    },
                    // 因为用户本项目存在多个后端组件用户体系，这是可以通过全局配置的方式决定使用哪个用户体系
                    remoteUrl: $vm.$stObjectTools.getValue($vm.$scatterConfig,'CompForm.userRemoteSearchUrl') || CompUrl.masterUser,
                    remoteQueryProp: 'nickname'
                }
            }
        }
    },
    {

        CompSearchList: false,
        field: {
            name: 'isVirtual'
        },
        element:{
            type: 'switch',
            label: '是否虚拟公司',
            required: true,
        }
    },
    {
        CompSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '备注',
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: CompUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },
]
export default CompForm