import CompForm from './CompForm.js'
import CompTable from './CompTable.js'
import CompUrl from './CompUrl.js'

const CompMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(CompForm,this.$options.name)
        },
        computedTableOptions() {
            return CompTable
        },
        computedUrl(){
            return CompUrl
        }
    },
}
export default CompMixin