import CompUrl from './CompUrl.js'

const CompRoute = [
    {
        path: CompUrl.router.searchList,
        component: () => import('./element/CompSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'CompSearchList',
            name: '公司管理'
        }
    },
    {
        path: CompUrl.router.add,
        component: () => import('./element/CompAdd'),
        meta: {
            code:'CompAdd',
            name: '公司添加'
        }
    },
    {
        path: CompUrl.router.update,
        component: () => import('./element/CompUpdate'),
        meta: {
            code:'CompUpdate',
            name: '公司修改'
        }
    },
]
export default CompRoute