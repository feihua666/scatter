const basePath = '' + '/comp'
const CompUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    masterUser: basePath + '/masteruser/getPage',
    router: {
        searchList: '/CompSearchList',
        add: '/CompAdd',
        update: '/CompUpdate',
    }
}
export default CompUrl