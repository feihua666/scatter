const CompTable = [
    {
        prop: 'code',
        label: '公司编码'
    },
    {
        prop: 'name',
        label: '公司名称'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'masterUserNickname',
        label: '负责人'
    },
    {
        prop: 'isVirtual',
        label: '是否虚拟公司'
    },
    {
        prop: 'remark',
        label: '备注'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default CompTable