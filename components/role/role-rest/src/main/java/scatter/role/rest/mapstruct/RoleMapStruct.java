package scatter.role.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.role.pojo.form.RoleAddForm;
import scatter.role.pojo.form.RolePageQueryForm;
import scatter.role.pojo.form.RoleUpdateForm;
import scatter.role.pojo.po.Role;
import scatter.role.pojo.vo.RoleVo;

/**
 * <p>
 * 角色 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleMapStruct extends IBaseVoMapStruct<Role, RoleVo>,
        IBaseAddFormMapStruct<Role,RoleAddForm>,
        IBaseUpdateFormMapStruct<Role,RoleUpdateForm>,
        IBaseQueryFormMapStruct<Role,RolePageQueryForm> {
    RoleMapStruct INSTANCE = Mappers.getMapper( RoleMapStruct.class );

}
