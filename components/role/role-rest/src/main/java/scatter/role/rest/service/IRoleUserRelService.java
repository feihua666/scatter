package scatter.role.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.role.pojo.po.RoleUserRel;

import java.util.List;
/**
 * <p>
 * 角色用户关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
public interface IRoleUserRelService extends IBaseService<RoleUserRel> {

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<RoleUserRel> getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return list(Wrappers.<RoleUserRel>lambdaQuery().eq(RoleUserRel::getUserId, userId));
    }
    /**
     * 根据角色id查询
     * @param roleId
     * @return
     */
    default List<RoleUserRel> getByRoleId(String roleId) {
        Assert.hasText(roleId,"roleId不能为空");
        return list(Wrappers.<RoleUserRel>lambdaQuery().eq(RoleUserRel::getRoleId, roleId));
    }

}
