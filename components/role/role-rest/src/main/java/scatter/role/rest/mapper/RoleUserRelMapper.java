package scatter.role.rest.mapper;

import scatter.role.pojo.po.RoleUserRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 角色用户关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
public interface RoleUserRelMapper extends IBaseMapper<RoleUserRel> {

}
