package scatter.role.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.role.pojo.form.*;
import scatter.role.pojo.po.RoleFuncRel;
import scatter.role.pojo.vo.RoleFuncRelVo;
import scatter.role.rest.RoleConfiguration;
import scatter.role.rest.service.IRoleFuncRelService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
/**
 * <p>
 * 角色菜单功能关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Api(tags = "角色菜单功能关系相关接口")
@RestController
@RequestMapping(RoleConfiguration.CONTROLLER_BASE_PATH + "/role-func-rel")
public class RoleFuncRelController extends BaseAddUpdateQueryFormController<RoleFuncRel, RoleFuncRelVo, RoleFuncRelAddForm, RoleFuncRelUpdateForm, RoleFuncRelPageQueryForm> {

    @Autowired
    private IRoleFuncRelService iRoleFuncRelService;

     @Override
	 @ApiOperation("添加角色菜单功能关系")
     @PreAuthorize("hasAuthority('RoleFuncRel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public RoleFuncRelVo add(@RequestBody @Valid RoleFuncRelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询角色菜单功能关系")
     @PreAuthorize("hasAuthority('RoleFuncRel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public RoleFuncRelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除角色菜单功能关系")
     @PreAuthorize("hasAuthority('RoleFuncRel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新角色菜单功能关系")
     @PreAuthorize("hasAuthority('RoleFuncRel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public RoleFuncRelVo update(@RequestBody @Valid RoleFuncRelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询角色菜单功能关系")
    @PreAuthorize("hasAuthority('RoleFuncRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<RoleFuncRelVo> getList(RoleFuncRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询角色菜单功能关系")
    @PreAuthorize("hasAuthority('RoleFuncRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<RoleFuncRelVo> getPage(RoleFuncRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("角色分配功能")
    @PreAuthorize("hasAuthority('roleFuncRel:single:roleAssignFunc')")
    @PostMapping("/role/assign/func")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean roleAssignFunc(@RequestBody @Valid RoleAssignFuncForm cf) {
        return iRoleFuncRelService.removeAndAssignRel(cf.getRoleId(),cf.getCheckedFuncIds(),cf.getUncheckedFuncIds(),cf.getIsLazyLoad(),RoleFuncRel::getRoleId,RoleFuncRel::getFuncId,(relDto)->new RoleFuncRel().setRoleId(relDto.getMainId()).setFuncId(relDto.getOtherId()));
    }

    @ApiOperation("根据角色ID查询已分配的功能id")
    @PreAuthorize("hasAuthority('roleFuncRel:single:queryByRoleId')")
    @GetMapping("/role/{roleId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByRoleId(@PathVariable String roleId) {
        List<RoleFuncRel> rels = iRoleFuncRelService.getByRoleId(roleId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }

        return rels.stream().map(item -> item.getFuncId()).collect(Collectors.toList());
    }

    @ApiOperation("清空角色下的所有功能")
    @PreAuthorize("hasAuthority('roleFuncRel:single:deleteByRoleId')")
    @DeleteMapping("/role/{roleId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByRoleId(@PathVariable String roleId) {
        return iRoleFuncRelService.removeAssignRel(roleId,RoleFuncRel::getRoleId);
    }


    @ApiOperation("功能分配角色")
    @PreAuthorize("hasAuthority('roleFuncRel:single:funcAssignRole')")
    @PostMapping("/func/assign/role")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean funcAssignRole(@RequestBody @Valid FuncAssignRoleForm cf) {
        return iRoleFuncRelService.removeAndAssignRel(cf.getFuncId(),cf.getCheckedRoleIds(),cf.getUncheckedRoleIds(),cf.getIsLazyLoad(),RoleFuncRel::getFuncId,RoleFuncRel::getRoleId,(relDto)->new RoleFuncRel().setFuncId(relDto.getMainId()).setRoleId(relDto.getOtherId()));
    }

    @ApiOperation("根据功能ID查询已分配的功能id")
    @PreAuthorize("hasAuthority('roleFuncRel:single:queryByFuncId')")
    @GetMapping("/func/{funcId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByFuncId(@PathVariable String funcId) {
        List<RoleFuncRel> rels = iRoleFuncRelService.getByFuncId(funcId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }
        return rels.stream().map(item -> item.getFuncId()).collect(Collectors.toList());
    }

    @ApiOperation("清空功能下的所有角色")
    @PreAuthorize("hasAuthority('roleFuncRel:single:deleteByFuncId')")
    @DeleteMapping("/func/{funcId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByFuncId(@PathVariable String funcId) {
        return iRoleFuncRelService.removeAssignRel(funcId,RoleFuncRel::getFuncId);
    }
}
