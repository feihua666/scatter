package scatter.role.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.role.pojo.po.RoleFuncRel;

import java.util.List;
/**
 * <p>
 * 角色菜单功能关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
public interface IRoleFuncRelService extends IBaseService<RoleFuncRel> {

    /**
     * 根据角色id查询
     * @param roleId
     * @return
     */
    default List<RoleFuncRel> getByRoleId(String roleId) {
        Assert.hasText(roleId,"roleId不能为空");
        return list(Wrappers.<RoleFuncRel>lambdaQuery().eq(RoleFuncRel::getRoleId, roleId));
    }

    /**
     * 根据角色ids查询
     * @param roleIds
     * @return
     */
    default List<RoleFuncRel> getByRoleIds(List<String> roleIds) {
        Assert.notEmpty(roleIds,"roleIds不能为空");
        return list(Wrappers.<RoleFuncRel>lambdaQuery().in(RoleFuncRel::getRoleId, roleIds));
    }
    /**
     * 根据功能id查询
     * @param funcId
     * @return
     */
    default List<RoleFuncRel> getByFuncId(String funcId) {
        Assert.hasText(funcId,"funcId不能为空");
        return list(Wrappers.<RoleFuncRel>lambdaQuery().eq(RoleFuncRel::getFuncId, funcId));
    }

}
