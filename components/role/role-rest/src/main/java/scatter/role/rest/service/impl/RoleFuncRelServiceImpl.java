package scatter.role.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.role.pojo.form.RoleFuncRelAddForm;
import scatter.role.pojo.form.RoleFuncRelPageQueryForm;
import scatter.role.pojo.form.RoleFuncRelUpdateForm;
import scatter.role.pojo.po.RoleFuncRel;
import scatter.role.rest.mapper.RoleFuncRelMapper;
import scatter.role.rest.service.IRoleFuncRelService;
/**
 * <p>
 * 角色菜单功能关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Service
@Transactional
public class RoleFuncRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<RoleFuncRelMapper, RoleFuncRel, RoleFuncRelAddForm, RoleFuncRelUpdateForm, RoleFuncRelPageQueryForm> implements IRoleFuncRelService {
    @Override
    public void preAdd(RoleFuncRelAddForm addForm,RoleFuncRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(RoleFuncRelUpdateForm updateForm,RoleFuncRel po) {
        super.preUpdate(updateForm,po);

    }
}
