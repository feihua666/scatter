package scatter.role.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.role.rest.RoleConfiguration;
import scatter.role.pojo.po.RoleFuncRel;
import scatter.role.rest.service.IRoleFuncRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 角色菜单功能关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@RestController
@RequestMapping(RoleConfiguration.CONTROLLER_BASE_PATH + "/inner/role-func-rel")
public class RoleFuncRelInnerController extends BaseInnerController<RoleFuncRel> {
 @Autowired
 private IRoleFuncRelService roleFuncRelService;

 public IRoleFuncRelService getService(){
     return roleFuncRelService;
 }
}
