package scatter.role.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.dataconstraint.DataConstraint;
import scatter.role.rest.RoleConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.role.pojo.po.Role;
import scatter.role.pojo.vo.RoleVo;
import scatter.role.pojo.form.RoleAddForm;
import scatter.role.pojo.form.RoleUpdateForm;
import scatter.role.pojo.form.RolePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@DataConstraint(doc = "role",name = "角色")
@Api(tags = "角色相关接口")
@RestController
@RequestMapping(RoleConfiguration.CONTROLLER_BASE_PATH + "/role")
public class RoleController extends BaseAddUpdateQueryFormController<Role, RoleVo, RoleAddForm, RoleUpdateForm, RolePageQueryForm> {


     @Override
	 @ApiOperation("添加角色")
     @PreAuthorize("hasAuthority('Role:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public RoleVo add(@RequestBody @Valid RoleAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询角色")
     @PreAuthorize("hasAuthority('Role:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public RoleVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除角色")
     @PreAuthorize("hasAuthority('Role:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新角色")
     @PreAuthorize("hasAuthority('Role:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public RoleVo update(@RequestBody @Valid RoleUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询角色")
    @PreAuthorize("hasAuthority('Role:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<RoleVo> getList(RolePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询角色")
    @PreAuthorize("hasAuthority('Role:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<RoleVo> getPage(RolePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
