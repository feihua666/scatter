package scatter.role.rest.mapper;

import scatter.role.pojo.po.Role;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
public interface RoleMapper extends IBaseMapper<Role> {

}
