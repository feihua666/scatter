package scatter.role.rest.mapper;

import scatter.role.pojo.po.RoleFuncRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 角色菜单功能关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
public interface RoleFuncRelMapper extends IBaseMapper<RoleFuncRel> {

}
