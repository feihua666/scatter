package scatter.role.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.role.pojo.form.RoleUserRelAddForm;
import scatter.role.pojo.form.RoleUserRelPageQueryForm;
import scatter.role.pojo.form.RoleUserRelUpdateForm;
import scatter.role.pojo.po.RoleUserRel;
import scatter.role.rest.mapper.RoleUserRelMapper;
import scatter.role.rest.service.IRoleUserRelService;
/**
 * <p>
 * 角色用户关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@Service
@Transactional
public class RoleUserRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<RoleUserRelMapper, RoleUserRel, RoleUserRelAddForm, RoleUserRelUpdateForm, RoleUserRelPageQueryForm> implements IRoleUserRelService {
    @Override
    public void preAdd(RoleUserRelAddForm addForm,RoleUserRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(RoleUserRelUpdateForm updateForm,RoleUserRel po) {
        super.preUpdate(updateForm,po);

    }
}
