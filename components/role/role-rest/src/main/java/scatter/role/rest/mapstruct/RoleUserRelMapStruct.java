package scatter.role.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.role.pojo.form.RoleUserRelAddForm;
import scatter.role.pojo.form.RoleUserRelPageQueryForm;
import scatter.role.pojo.form.RoleUserRelUpdateForm;
import scatter.role.pojo.po.RoleUserRel;
import scatter.role.pojo.vo.RoleUserRelVo;

/**
 * <p>
 * 角色用户关系 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleUserRelMapStruct extends IBaseVoMapStruct<RoleUserRel, RoleUserRelVo>,
        IBaseAddFormMapStruct<RoleUserRel,RoleUserRelAddForm>,
        IBaseUpdateFormMapStruct<RoleUserRel,RoleUserRelUpdateForm>,
        IBaseQueryFormMapStruct<RoleUserRel,RoleUserRelPageQueryForm> {
    RoleUserRelMapStruct INSTANCE = Mappers.getMapper( RoleUserRelMapStruct.class );

}
