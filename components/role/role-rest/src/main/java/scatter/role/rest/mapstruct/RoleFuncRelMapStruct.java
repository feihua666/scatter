package scatter.role.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.role.pojo.form.RoleFuncRelAddForm;
import scatter.role.pojo.form.RoleFuncRelPageQueryForm;
import scatter.role.pojo.form.RoleFuncRelUpdateForm;
import scatter.role.pojo.po.RoleFuncRel;
import scatter.role.pojo.vo.RoleFuncRelVo;

/**
 * <p>
 * 角色菜单功能关系 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleFuncRelMapStruct extends IBaseVoMapStruct<RoleFuncRel, RoleFuncRelVo>,
        IBaseAddFormMapStruct<RoleFuncRel,RoleFuncRelAddForm>,
        IBaseUpdateFormMapStruct<RoleFuncRel,RoleFuncRelUpdateForm>,
        IBaseQueryFormMapStruct<RoleFuncRel,RoleFuncRelPageQueryForm> {
    RoleFuncRelMapStruct INSTANCE = Mappers.getMapper( RoleFuncRelMapStruct.class );

}
