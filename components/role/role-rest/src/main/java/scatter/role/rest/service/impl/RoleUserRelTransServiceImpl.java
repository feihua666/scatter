package scatter.role.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.role.pojo.po.RoleUserRel;
import scatter.role.rest.service.IRoleUserRelService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色用户关系翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@Component
public class RoleUserRelTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IRoleUserRelService roleUserRelService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,RoleUserRel.TRANS_ROLEUSERREL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,RoleUserRel.TRANS_ROLEUSERREL_BY_ID)) {
            RoleUserRel byId = roleUserRelService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,RoleUserRel.TRANS_ROLEUSERREL_BY_ID)) {
            return roleUserRelService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
