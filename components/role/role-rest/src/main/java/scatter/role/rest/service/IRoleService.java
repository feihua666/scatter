package scatter.role.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.role.pojo.po.Role;

import java.util.List;
/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
public interface IRoleService extends IBaseService<Role> {


    /**
     * 根据角色编码查询
     * @param code
     * @return
     */
    default Role getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Role>lambdaQuery().eq(Role::getCode, code));
    }
    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    List<Role> getByUserId(String userId,Boolean isDisabled);

    /**
     * 根据功能id查询
     * @param funcId
     * @return
     */
    List<Role> getByFuncId(String funcId,Boolean isDisabled);
}
