DROP TABLE IF EXISTS component_role_func_rel;
CREATE TABLE `component_role_func_rel` (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `func_id` varchar(20) NOT NULL COMMENT '功能id',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `role_id_2` (`role_id`,`func_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `func_id` (`func_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色菜单功能关系表';
