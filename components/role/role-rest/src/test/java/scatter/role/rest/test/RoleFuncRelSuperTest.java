package scatter.role.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.role.pojo.po.RoleFuncRel;
import scatter.role.pojo.form.RoleFuncRelAddForm;
import scatter.role.pojo.form.RoleFuncRelUpdateForm;
import scatter.role.pojo.form.RoleFuncRelPageQueryForm;
import scatter.role.rest.service.IRoleFuncRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 角色菜单功能关系 测试类基类
* </p>
*
* @author yw
* @since 2020-12-14
*/
public class RoleFuncRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRoleFuncRelService roleFuncRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return roleFuncRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return roleFuncRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public RoleFuncRel mockPo() {
        return JMockData.mock(RoleFuncRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public RoleFuncRelAddForm mockAddForm() {
        return JMockData.mock(RoleFuncRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public RoleFuncRelUpdateForm mockUpdateForm() {
        return JMockData.mock(RoleFuncRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public RoleFuncRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(RoleFuncRelPageQueryForm.class, mockConfig);
    }
}