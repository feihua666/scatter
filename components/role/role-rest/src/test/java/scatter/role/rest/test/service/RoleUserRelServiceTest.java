package scatter.role.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.role.pojo.po.RoleUserRel;
import scatter.role.pojo.form.RoleUserRelAddForm;
import scatter.role.pojo.form.RoleUserRelUpdateForm;
import scatter.role.pojo.form.RoleUserRelPageQueryForm;
import scatter.role.rest.test.RoleUserRelSuperTest;
import scatter.role.rest.service.IRoleUserRelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 角色用户关系 服务测试类
* </p>
*
* @author yw
* @since 2020-12-11
*/
@SpringBootTest
public class RoleUserRelServiceTest extends RoleUserRelSuperTest{

    @Autowired
    private IRoleUserRelService roleUserRelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<RoleUserRel> pos = roleUserRelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}