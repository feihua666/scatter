package scatter.role.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.role.pojo.po.RoleUserRel;
import scatter.role.pojo.form.RoleUserRelAddForm;
import scatter.role.pojo.form.RoleUserRelUpdateForm;
import scatter.role.pojo.form.RoleUserRelPageQueryForm;
import scatter.role.rest.service.IRoleUserRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 角色用户关系 测试类基类
* </p>
*
* @author yw
* @since 2020-12-11
*/
public class RoleUserRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRoleUserRelService roleUserRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return roleUserRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return roleUserRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public RoleUserRel mockPo() {
        return JMockData.mock(RoleUserRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public RoleUserRelAddForm mockAddForm() {
        return JMockData.mock(RoleUserRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public RoleUserRelUpdateForm mockUpdateForm() {
        return JMockData.mock(RoleUserRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public RoleUserRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(RoleUserRelPageQueryForm.class, mockConfig);
    }
}