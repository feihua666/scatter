package scatter.role.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.role.pojo.po.Role;
import scatter.role.pojo.form.RoleAddForm;
import scatter.role.pojo.form.RoleUpdateForm;
import scatter.role.pojo.form.RolePageQueryForm;
import scatter.role.rest.service.IRoleService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 角色 测试类基类
* </p>
*
* @author yw
* @since 2020-12-15
*/
public class RoleSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRoleService roleService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return roleService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return roleService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Role mockPo() {
        return JMockData.mock(Role.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public RoleAddForm mockAddForm() {
        return JMockData.mock(RoleAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public RoleUpdateForm mockUpdateForm() {
        return JMockData.mock(RoleUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public RolePageQueryForm mockPageQueryForm() {
        return JMockData.mock(RolePageQueryForm.class, mockConfig);
    }
}