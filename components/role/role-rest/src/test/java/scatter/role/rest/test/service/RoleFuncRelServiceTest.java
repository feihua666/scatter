package scatter.role.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.role.pojo.po.RoleFuncRel;
import scatter.role.pojo.form.RoleFuncRelAddForm;
import scatter.role.pojo.form.RoleFuncRelUpdateForm;
import scatter.role.pojo.form.RoleFuncRelPageQueryForm;
import scatter.role.rest.test.RoleFuncRelSuperTest;
import scatter.role.rest.service.IRoleFuncRelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 角色菜单功能关系 服务测试类
* </p>
*
* @author yw
* @since 2020-12-14
*/
@SpringBootTest
public class RoleFuncRelServiceTest extends RoleFuncRelSuperTest{

    @Autowired
    private IRoleFuncRelService roleFuncRelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<RoleFuncRel> pos = roleFuncRelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}