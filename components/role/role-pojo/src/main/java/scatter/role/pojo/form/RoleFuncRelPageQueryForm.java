package scatter.role.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 角色菜单功能关系分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Setter
@Getter
@ApiModel(value="角色菜单功能关系分页表单对象")
public class RoleFuncRelPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "角色id")
    private String roleId;

    @ApiModelProperty(value = "功能id")
    private String funcId;

}
