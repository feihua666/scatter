package scatter.role.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 角色更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@PropValid
@Setter
@Getter
@ApiModel(value="角色更新表单对象")
public class RoleUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="角色编码不能为空")
    @ApiModelProperty(value = "角色编码,模糊查询",required = true)
    private String code;

    @NotEmpty(message="角色名称不能为空")
    @ApiModelProperty(value = "角色名称,模糊查询",required = true)
    private String name;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
