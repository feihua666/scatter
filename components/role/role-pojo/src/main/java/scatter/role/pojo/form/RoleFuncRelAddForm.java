package scatter.role.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色菜单功能关系添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Setter
@Getter
@ApiModel(value="角色菜单功能关系添加表单对象")
public class RoleFuncRelAddForm extends BaseAddForm {

    @NotEmpty(message="角色id不能为空")
    @ApiModelProperty(value = "角色id",required = true)
    private String roleId;

    @NotEmpty(message="功能id不能为空")
    @ApiModelProperty(value = "功能id",required = true)
    private String funcId;

}
