package scatter.role.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色用户关系添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@Setter
@Getter
@ApiModel(value="角色用户关系添加表单对象")
public class RoleUserRelAddForm extends BaseAddForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="角色id不能为空")
    @ApiModelProperty(value = "角色id",required = true)
    private String roleId;

}
