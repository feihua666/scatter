package scatter.role.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 角色分配功能
 * </p>
 *
 * @author yangwei
 * @since 2019-09-26
 */
@PropValid
@Data
@EqualsAndHashCode(callSuper=false)
@Accessors(chain = true)
@ApiModel(value="角色分配功能表单对象")
public class RoleAssignFuncForm extends BaseForm {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "角色id不能为空")
    @ApiModelProperty(value = "角色id")
    private String roleId;

    @ApiModelProperty(value = "选择的功能id")
    private List<String> checkedFuncIds;

    @PropValid.DependCondition(message = "未选择的功能id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的功能id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedFuncIds;

    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
