package scatter.role.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Setter
@Getter
@ApiModel(value="角色添加表单对象")
public class RoleAddForm extends BaseAddForm {

    @NotEmpty(message="角色编码不能为空")
    @ApiModelProperty(value = "角色编码,模糊查询",required = true)
    private String code;

    @NotEmpty(message="角色名称不能为空")
    @ApiModelProperty(value = "角色名称,模糊查询",required = true)
    private String name;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
