package scatter.role.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色菜单功能关系表
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_role_func_rel")
@ApiModel(value="RoleFuncRel对象", description="角色菜单功能关系表")
public class RoleFuncRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ROLEFUNCREL_BY_ID = "trans_rolefuncrel_by_id_scatter.role.pojo.po";

    @ApiModelProperty(value = "角色id")
    private String roleId;

    @ApiModelProperty(value = "功能id")
    private String funcId;


}
