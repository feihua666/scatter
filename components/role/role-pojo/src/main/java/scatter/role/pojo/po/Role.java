package scatter.role.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_role")
@ApiModel(value="Role对象", description="角色表")
public class Role extends BaseTreePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ROLE_BY_ID = "trans_role_by_id_scatter.role.pojo.po";

    @ApiModelProperty(value = "角色编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "角色名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;


}
