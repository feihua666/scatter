package scatter.role.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.role.pojo.po.Role;


/**
 * <p>
 * 角色响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Setter
@Getter
@ApiModel(value="角色响应对象")
public class RoleVo extends BaseIdVo {

    @ApiModelProperty(value = "角色编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "角色名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @TransBy(type = Role.TRANS_ROLE_BY_ID,byFieldName = "parentId",mapValueField = "name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
