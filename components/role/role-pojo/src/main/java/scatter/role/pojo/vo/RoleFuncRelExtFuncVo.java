package scatter.role.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;

/**
 * Created by yangwei
 * Created at 2021/2/7 14:28
 */
@Setter
@Getter
@ApiModel(value="角色功能配置加载选中的功能响应对象")
public class RoleFuncRelExtFuncVo extends BaseIdVo {


    @ApiModelProperty(value = "功能名称")
    private String name;

    @ApiModelProperty(value = "父级id")
    private String parentId;

}
