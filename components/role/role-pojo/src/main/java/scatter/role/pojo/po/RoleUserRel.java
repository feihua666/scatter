package scatter.role.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色用户关系表
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_role_user_rel")
@ApiModel(value="RoleUserRel对象", description="角色用户关系表")
public class RoleUserRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ROLEUSERREL_BY_ID = "trans_roleuserrel_by_id_scatter.role.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "角色id")
    private String roleId;


}
