import RoleUserRelUrl from './RoleUserRelUrl.js'

const RoleUserRelRoute = [
    {
        path: RoleUserRelUrl.router.UserAssignRoleRel,
        component: () => import('./element/UserAssignRoleRel'),
        meta: {
            code:'UserAssignRoleRel',
            name: '用户角色配置'
        }
    },
]
export default RoleUserRelRoute