import RoleForm from './RoleForm.js'
import RoleTable from './RoleTable.js'
import RoleUrl from './RoleUrl.js'

const RoleMixin = {
    computed: {
        RoleFormOptions() {
            return this.$stDynamicFormTools.options(RoleForm,this.$options.name)
        },
        RoleTableOptions() {
            return RoleTable
        },
        RoleUrl(){
            return RoleUrl
        }
    },
}
export default RoleMixin