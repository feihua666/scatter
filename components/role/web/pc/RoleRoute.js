import RoleUrl from './RoleUrl.js'

const RoleRoute = [
    {
        path: RoleUrl.router.searchList,
        component: () => import('./element/RoleSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'RoleSearchList',
            name: '角色管理'
        }
    },
    {
        path: RoleUrl.router.add,
        component: () => import('./element/RoleAdd'),
        meta: {
            code:'RoleAdd',
            name: '角色添加'
        }
    },
    {
        path: RoleUrl.router.update,
        component: () => import('./element/RoleUpdate'),
        meta: {
            code:'RoleUpdate',
            name: '角色修改'
        }
    },
]
export default RoleRoute