const basePath = '' + '/role-user-rel'
const RoleUserRelUrl = {
    userAssignRole: basePath + '/user/assign/role',
    checkedRoleIds: basePath + '/user/{userId}',
    router: {
        UserAssignRoleRel: '/UserAssignRoleRel',
    }
}
export default RoleUserRelUrl