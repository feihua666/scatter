import RoleFuncRelUrl from './RoleFuncRelUrl.js'

const RoleFuncRelRoute = [
    {
        path: RoleFuncRelUrl.router.RoleAssignFuncRel,
        component: () => import('./element/RoleAssignFuncRel'),
        meta: {
            code:'RoleAssignFuncRel',
            name: '角色功能配置'
        }
    },
]
export default RoleFuncRelRoute