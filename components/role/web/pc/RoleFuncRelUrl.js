const basePath = '' + '/role-func-rel'
const RoleFuncRelUrl = {
    roleAssignFunc: basePath + '/role/assign/func',
    checkedFuncIds: basePath + '/role/{roleId}',
    router: {
        RoleAssignFuncRel: '/RoleAssignFuncRel',
    }
}
export default RoleFuncRelUrl