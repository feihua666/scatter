import RoleUrl from './RoleUrl.js'
import RoleFuncRelUrl from './RoleFuncRelUrl.js'
import FuncUrl from '../../../func/web/pc/FuncUrl'

const RoleFuncRelForm = [
    {
        field: {
            name: 'roleId',
        },
        element:{
            label: '角色',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: RoleUrl.list
            }

        }
    },
    {
        field: {
            name: 'checkedFuncIds',
            value: []
        },
        element:{
            label: '功能',
            type: 'tree',
            options: {
                datas: FuncUrl.list,
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default RoleFuncRelForm