import RoleFuncRelForm from './RoleFuncRelForm.js'
import RoleFuncRelUrl from './RoleFuncRelUrl.js'

const RoleFuncRelMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(RoleFuncRelForm,this.$options.name)
        },
        computedUrl(){
            return RoleFuncRelUrl
        }
    },
}
export default RoleFuncRelMixin