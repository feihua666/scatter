const RoleTable = [
    {
        prop: 'code',
        label: '角色编码'
    },
    {
        prop: 'name',
        label: '角色名称'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'remark',
        label: '描述'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default RoleTable