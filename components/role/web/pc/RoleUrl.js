const basePath = '' + '/role'
const RoleUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/RoleSearchList',
        add: '/RoleAdd',
        update: '/RoleUpdate',
    }
}
export default RoleUrl