import RoleUrl from './RoleUrl.js'
const RoleUserRelForm = [
    {
        field: {
            name: 'userId',
        },
        element:{
            label: '用户',
            type: 'select',
            required: true,
            options: ({$route,$vm})=>{
                let datas = []
                if ($route.query.userId) {
                    datas.push({
                        id: $route.query.userId,
                        nickname: $route.query.userNickname
                    })
                }
                return {
                    datas: datas,
                    optionProp:{
                        value: 'id', // 选中的值属性
                        label: 'nickname', // 显示的值属性
                    },
                    originProp: {
                        placeholder: '请按用户昵称搜索',
                        remote: true,
                    },
                    // 因为用户本项目存在两个后端组件，这是可以通过全局配置的方式决定使用哪个用户体系
                    remoteUrl: $vm.$stObjectTools.getValue($vm.$scatterConfig,'RoleUserRelForm.userRemoteSearchUrl') || '/user-simple/getPage',
                    remoteQueryProp: 'nickname'
                }
            }
        }
    },
    {
        RoleUserRelSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'checkedRoleIds',
            value: []
        },
        element:{
            label: '角色',
            type: 'tree',
            options: {
                datas: RoleUrl.list,
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default RoleUserRelForm