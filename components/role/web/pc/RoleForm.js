import RoleUrl from './RoleUrl.js'

const RoleForm = [
    {
        RoleSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'code',
        },
        element:{
            label: '角色编码',
            required: true,
        }
    },
    {
        RoleSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '角色名称',
            required: true,
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: RoleUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },
    {
        RoleSearchList: false,
        RoleAdd: false,
        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        RoleSearchList: false,
        RoleAdd: false,
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
            disabled: ({form})=>{
                let r = !form.isDisabled
                if(r){
                    form.disabledReason = null
                }
                return r
            },
            required: ({form})=>{
                let r = form.isDisabled
                return r
            }
        }
    },
    {
        RoleSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default RoleForm