import RoleRoute from './RoleRoute.js'
import RoleFuncRelRoute from './RoleFuncRelRoute.js'
import RoleUserRelRoute from './RoleUserRelRoute.js'

const RoleRouteEntry = []
.concat(RoleRoute)
.concat(RoleFuncRelRoute)
.concat(RoleUserRelRoute)

export default RoleRouteEntry