import RoleUserRelForm from './RoleUserRelForm.js'
import RoleUserRelUrl from './RoleUserRelUrl.js'

const RoleUserRelMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(RoleUserRelForm,this.$options.name)
        },
        computedUrl(){
            return RoleUserRelUrl
        }
    },
}
export default RoleUserRelMixin