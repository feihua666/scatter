package scatter.role.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 角色菜单功能关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-14
 */
@Component
@FeignClient(value = "RoleFuncRel-client")
public interface RoleFuncRelClient {

}
