package scatter.role.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 角色用户关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@Component
@FeignClient(value = "RoleUserRel-client")
public interface RoleUserRelClient {

}
