package scatter.role.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 角色表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Component
@FeignClient(value = "Role-client")
public interface RoleClient {

}
