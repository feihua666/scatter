package scatter.role.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.role.rest.RoleConfiguration;
import scatter.role.pojo.po.Role;
import scatter.role.rest.service.IRoleService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 角色表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@RestController
@RequestMapping(RoleConfiguration.CONTROLLER_BASE_PATH + "/inner/role")
public class RoleInnerController extends BaseInnerController<Role> {
 @Autowired
 private IRoleService roleService;

 public IRoleService getService(){
     return roleService;
 }
}
