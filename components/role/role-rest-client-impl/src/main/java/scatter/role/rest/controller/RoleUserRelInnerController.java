package scatter.role.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.role.rest.RoleConfiguration;
import scatter.role.pojo.po.RoleUserRel;
import scatter.role.rest.service.IRoleUserRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 角色用户关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-11
 */
@RestController
@RequestMapping(RoleConfiguration.CONTROLLER_BASE_PATH + "/inner/role-user-rel")
public class RoleUserRelInnerController extends BaseInnerController<RoleUserRel> {
 @Autowired
 private IRoleUserRelService roleUserRelService;

 public IRoleUserRelService getService(){
     return roleUserRelService;
 }
}
