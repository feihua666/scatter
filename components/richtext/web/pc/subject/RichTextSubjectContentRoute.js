import RichTextSubjectContentUrl from './RichTextSubjectContentUrl.js'

const RichTextSubjectContentRoute = [
    {
        path: RichTextSubjectContentUrl.router.searchList,
        component: () => import('./element/RichTextSubjectContentSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'RichTextSubjectContentSearchList',
            name: '主体和内容管理'
        }
    },
    {
        path: RichTextSubjectContentUrl.router.add,
        component: () => import('./element/RichTextSubjectContentAdd'),
        meta: {
            code:'RichTextSubjectContentAdd',
            name: '主体和内容添加'
        }
    },
    {
        path: RichTextSubjectContentUrl.router.update,
        component: () => import('./element/RichTextSubjectContentUpdate'),
        meta: {
            code:'RichTextSubjectContentUpdate',
            name: '主体和内容修改'
        }
    },
]
export default RichTextSubjectContentRoute