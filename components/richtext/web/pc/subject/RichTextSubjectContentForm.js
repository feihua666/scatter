import RichTextSubjectContentUrl from './RichTextSubjectContentUrl.js'
const RichTextSubjectContentForm = [
    {
        RichTextSubjectContentSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'subjectId',
        },
        element:{
            label: '主体id',
            required: true,
        }
    },
    {
        RichTextSubjectContentSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'subjectType',
        },
        element:{
            label: '主体id类型',
            required: true,
        }
    },
    {
        RichTextSubjectContentSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'contentId',
        },
        element:{
            label: '内容id',
            required: true,
        }
    },

]
export default RichTextSubjectContentForm