const basePath = '' + '/subject/rich-text-subject-content'
const RichTextSubjectContentUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/RichTextSubjectContentSearchList',
        add: '/RichTextSubjectContentAdd',
        update: '/RichTextSubjectContentUpdate',
    }
}
export default RichTextSubjectContentUrl