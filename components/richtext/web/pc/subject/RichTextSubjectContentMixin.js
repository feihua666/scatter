import RichTextSubjectContentForm from './RichTextSubjectContentForm.js'
import RichTextSubjectContentTable from './RichTextSubjectContentTable.js'
import RichTextSubjectContentUrl from './RichTextSubjectContentUrl.js'
const RichTextSubjectContentMixin = {
    computed: {
        RichTextSubjectContentFormOptions() {
            return this.$stDynamicFormTools.options(RichTextSubjectContentForm,this.$options.name)
        },
        RichTextSubjectContentTableOptions() {
            return RichTextSubjectContentTable
        },
        RichTextSubjectContentUrl(){
            return RichTextSubjectContentUrl
        }
    },
}
export default RichTextSubjectContentMixin