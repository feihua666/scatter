import RichTextForm from './RichTextForm.js'
import RichTextTable from './RichTextTable.js'
import RichTextUrl from './RichTextUrl.js'
const RichTextMixin = {
    computed: {
        RichTextFormOptions() {
            return this.$stDynamicFormTools.options(RichTextForm,this.$options.name)
        },
        RichTextTableOptions() {
            return RichTextTable
        },
        RichTextUrl(){
            return RichTextUrl
        }
    },
}
export default RichTextMixin