import RichTextContentUrl from './RichTextContentUrl.js'

const RichTextContentRoute = [
    {
        path: RichTextContentUrl.router.searchList,
        component: () => import('./element/RichTextContentSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'RichTextContentSearchList',
            name: '活动内容管理'
        }
    },
    {
        path: RichTextContentUrl.router.add,
        component: () => import('./element/RichTextContentAdd'),
        meta: {
            code:'RichTextContentAdd',
            name: '活动内容添加'
        }
    },
    {
        path: RichTextContentUrl.router.update,
        component: () => import('./element/RichTextContentUpdate'),
        meta: {
            code:'RichTextContentUpdate',
            name: '活动内容修改'
        }
    },
]
export default RichTextContentRoute