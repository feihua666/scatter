import RichTextContentForm from './RichTextContentForm.js'
import RichTextContentTable from './RichTextContentTable.js'
import RichTextContentUrl from './RichTextContentUrl.js'
const RichTextContentMixin = {
    computed: {
        RichTextContentFormOptions() {
            return this.$stDynamicFormTools.options(RichTextContentForm,this.$options.name)
        },
        RichTextContentTableOptions() {
            return RichTextContentTable
        },
        RichTextContentUrl(){
            return RichTextContentUrl
        }
    },
}
export default RichTextContentMixin