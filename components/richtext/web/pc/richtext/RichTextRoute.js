import RichTextUrl from './RichTextUrl.js'

const RichTextRoute = [
    {
        path: RichTextUrl.router.searchList,
        component: () => import('./element/RichTextSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'RichTextSearchList',
            name: '富文本管理'
        }
    },
    {
        path: RichTextUrl.router.add,
        component: () => import('./element/RichTextAdd'),
        meta: {
            code:'RichTextAdd',
            name: '富文本添加'
        }
    },
    {
        path: RichTextUrl.router.update,
        component: () => import('./element/RichTextUpdate'),
        meta: {
            code:'RichTextUpdate',
            name: '富文本修改'
        }
    },
]
export default RichTextRoute