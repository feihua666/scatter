import RichTextUrl from './RichTextUrl.js'
const RichTextForm = [
    {
        RichTextSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        RichTextSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        RichTextSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            type: 'selectDict',
            options: {
                groupCode: 'richtext_type'
            },
            required: true,
        }
    },
    {
        RichTextSearchList: false,
        field: {
            name: 'content'
        },
        element:{
            type: 'tinymceEditor',
            label: '内容',
            required: true,
            isBlock: true
        },
    },
    {
        RichTextSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default RichTextForm