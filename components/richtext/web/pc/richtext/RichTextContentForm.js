import RichTextContentUrl from './RichTextContentUrl.js'
const RichTextContentForm = [
    {
        RichTextContentSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'richTextId',
        },
        element:{
            label: '富文本id',
            required: true,
        }
    },
    {

        field: {
            name: 'content',
        },
        element:{
            label: '活动内容',
        }
    },
    {
        RichTextContentSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'contentVersion',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '内容版本',
            required: true,
        }
    },

]
export default RichTextContentForm