const RichTextContentTable = [
    {
        prop: 'richTextId',
        label: '富文本id'
    },
    {
        prop: 'content',
        label: '活动内容'
    },
    {
        prop: 'contentVersion',
        label: '内容版本'
    },
]
export default RichTextContentTable