const basePath = '' + '/richtext/rich-text-content'
const RichTextContentUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    // 获取最新版本的富文本内容
    getByCode: basePath + '/getByCode/{code}',
    router: {
        searchList: '/RichTextContentSearchList',
        add: '/RichTextContentAdd',
        update: '/RichTextContentUpdate',
    }
}
export default RichTextContentUrl