const RichTextTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default RichTextTable