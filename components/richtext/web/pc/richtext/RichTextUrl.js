const basePath = '' + '/richtext/rich-text'
const RichTextUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    queryByIdWithLatestContent: basePath + '/queryByIdWithLatestContent/{id}',
    queryByCodeWithLatestContent: basePath + '/queryByCodeWithLatestContent/{code}',
    router: {
        searchList: '/RichTextSearchList',
        add: '/RichTextAdd',
        update: '/RichTextUpdate',
    }
}
export default RichTextUrl