package scatter.richtext.rest.richtext.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 活动内容表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
@FeignClient(value = "RichTextContent-client")
public interface RichTextContentClient {

}
