package scatter.richtext.rest.richtext.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 富文本表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
@FeignClient(value = "RichText-client")
public interface RichTextClient {

}
