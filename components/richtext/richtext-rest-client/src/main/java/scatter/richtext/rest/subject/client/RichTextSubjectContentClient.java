package scatter.richtext.rest.subject.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 主体和内容关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
@FeignClient(value = "RichTextSubjectContent-client")
public interface RichTextSubjectContentClient {

}
