package scatter.richtext.rest.richtext.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.rest.richtext.service.IRichTextService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 富文本表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@RestController
@RequestMapping(RichtextConfiguration.CONTROLLER_BASE_PATH + "/inner/richtext/rich-text")
public class RichTextInnerController extends BaseInnerController<RichText> {
 @Autowired
 private IRichTextService richTextService;

 public IRichTextService getService(){
     return richTextService;
 }
}
