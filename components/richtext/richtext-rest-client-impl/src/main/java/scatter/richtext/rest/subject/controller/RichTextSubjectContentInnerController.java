package scatter.richtext.rest.subject.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.rest.subject.service.IRichTextSubjectContentService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 主体和内容关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@RestController
@RequestMapping(RichtextConfiguration.CONTROLLER_BASE_PATH + "/inner/subject/rich-text-subject-content")
public class RichTextSubjectContentInnerController extends BaseInnerController<RichTextSubjectContent> {
 @Autowired
 private IRichTextSubjectContentService richTextSubjectContentService;

 public IRichTextSubjectContentService getService(){
     return richTextSubjectContentService;
 }
}
