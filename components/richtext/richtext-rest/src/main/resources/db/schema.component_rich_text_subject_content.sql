DROP TABLE IF EXISTS component_rich_text_subject_content;
CREATE TABLE `component_rich_text_subject_content` (
  `id` varchar(20) NOT NULL COMMENT '主键ID',
  `subject_id` varchar(20) NOT NULL COMMENT '主体id',
  `subject_type` varchar(255) NOT NULL COMMENT '主体id类型，用来说明主体id指的是什么id',
  `content_id` varchar(20) NOT NULL COMMENT '内容id',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `content_id` (`content_id`) USING BTREE,
  KEY `subject_id` (`subject_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主体和内容关系表';
