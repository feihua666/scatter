DROP TABLE IF EXISTS component_rich_text_content;
CREATE TABLE `component_rich_text_content` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `rich_text_id` varchar(32) NOT NULL COMMENT '富文本id',
  `content` mediumtext COMMENT '活动内容',
  `content_version` int NOT NULL COMMENT '内容版本，如：1.0，2.0',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `rich_text_id` (`rich_text_id`) USING BTREE,
  KEY `version` (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动内容表';
