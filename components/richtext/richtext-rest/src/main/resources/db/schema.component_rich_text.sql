DROP TABLE IF EXISTS component_rich_text;
CREATE TABLE `component_rich_text` (
  `id` varchar(20) NOT NULL COMMENT '主键ID',
  `code` varchar(100) NOT NULL COMMENT '编码，模糊查询',
  `name` varchar(100) NOT NULL COMMENT '名称，模糊查询',
  `type_dict_id` varchar(20) NOT NULL COMMENT '类型，字典id',
  `latest_content_id` varchar(20) DEFAULT NULL COMMENT '最新的内容版本数据id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `type_dict_id` (`type_dict_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='富文本表';
