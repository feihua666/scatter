package scatter.richtext.rest.richtext.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.common.rest.service.IBaseService;
import scatter.richtext.pojo.richtext.form.RichTextAddForm;
import scatter.richtext.pojo.richtext.form.RichTextUpdateForm;
import scatter.richtext.pojo.richtext.form.RichTextPageQueryForm;
import java.util.List;
/**
 * <p>
 * 富文本表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface IRichTextService extends IBaseService<RichText> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default RichText getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<RichText>lambdaQuery().eq(RichText::getCode, code));
    }

}
