package scatter.richtext.rest.richtext.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.rest.richtext.service.IRichTextContentService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 活动内容表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@RestController
@RequestMapping(RichtextConfiguration.CONTROLLER_BASE_PATH + "/inner/richtext/rich-text-content")
public class RichTextContentInnerController extends BaseInnerController<RichTextContent> {
 @Autowired
 private IRichTextContentService richTextContentService;

 public IRichTextContentService getService(){
     return richTextContentService;
 }
}
