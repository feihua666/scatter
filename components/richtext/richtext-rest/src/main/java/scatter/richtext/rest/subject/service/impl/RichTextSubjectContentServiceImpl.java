package scatter.richtext.rest.subject.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseUpdateFormServiceImpl;
import scatter.richtext.pojo.subject.form.RichTextSubjectContentUpdateForm;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.rest.subject.mapper.RichTextSubjectContentMapper;
import scatter.richtext.rest.subject.service.IRichTextSubjectContentService;
/**
 * <p>
 * 主体和内容关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class RichTextSubjectContentServiceImpl extends IBaseUpdateFormServiceImpl<RichTextSubjectContentMapper, RichTextSubjectContent,  RichTextSubjectContentUpdateForm> implements IRichTextSubjectContentService {


    @Override
    public void preUpdate(RichTextSubjectContentUpdateForm updateForm,RichTextSubjectContent po) {

        // 如果不存在先保存一下
        RichTextSubjectContent richTextSubjectContent = get(updateForm.getSubjectId(), updateForm.getSubjectType(), updateForm.getContentId());
        if (richTextSubjectContent == null) {
            save(po);
        }

    }
}
