package scatter.richtext.rest.subject.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseUpdateFormController;
import scatter.richtext.pojo.subject.form.RichTextSubjectContentUpdateForm;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.pojo.subject.vo.RichTextSubjectContentVo;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.richtext.rest.subject.service.IRichTextSubjectContentService;

import javax.validation.Valid;
/**
 * <p>
 * 主体和内容关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "主体和内容相关接口")
@RestController
@RequestMapping(RichtextConfiguration.CONTROLLER_BASE_PATH + "/subject/rich-text-subject-content")
public class RichTextSubjectContentController extends BaseUpdateFormController<RichTextSubjectContent,RichTextSubjectContentVo, RichTextSubjectContentUpdateForm> {
    @Autowired
    private IRichTextSubjectContentService iRichTextSubjectContentService;


    @ApiOperation("添加或更新主体和内容")
    @PreAuthorize("hasAuthority('user')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public RichTextSubjectContentVo update(@RequestBody @Valid RichTextSubjectContentUpdateForm updateForm) {
        return super.update(updateForm);
    }

}
