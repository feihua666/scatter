package scatter.richtext.rest.richtext.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.pojo.richtext.vo.RichTextVo;
import scatter.richtext.pojo.richtext.form.RichTextAddForm;
import scatter.richtext.pojo.richtext.form.RichTextUpdateForm;
import scatter.richtext.pojo.richtext.form.RichTextPageQueryForm;
import scatter.richtext.pojo.richtext.vo.RichTextWithLatestContentVo;

/**
 * <p>
 * 富文本 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RichTextMapStruct extends IBaseVoMapStruct<RichText, RichTextVo>,
                                  IBaseAddFormMapStruct<RichText,RichTextAddForm>,
                                  IBaseUpdateFormMapStruct<RichText,RichTextUpdateForm>,
                                  IBaseQueryFormMapStruct<RichText,RichTextPageQueryForm>{
    RichTextMapStruct INSTANCE = Mappers.getMapper( RichTextMapStruct.class );

    RichTextWithLatestContentVo richTextVoToRichTextWithLatestContentVo(RichTextVo richTextVo);
}
