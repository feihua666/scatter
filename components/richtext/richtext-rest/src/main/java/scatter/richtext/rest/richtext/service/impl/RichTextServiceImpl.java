package scatter.richtext.rest.richtext.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.rest.richtext.mapper.RichTextContentMapper;
import scatter.richtext.rest.richtext.mapper.RichTextMapper;
import scatter.richtext.rest.richtext.service.IRichTextContentService;
import scatter.richtext.rest.richtext.service.IRichTextService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.richtext.pojo.richtext.form.RichTextAddForm;
import scatter.richtext.pojo.richtext.form.RichTextUpdateForm;
import scatter.richtext.pojo.richtext.form.RichTextPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 富文本表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class RichTextServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<RichTextMapper, RichText, RichTextAddForm, RichTextUpdateForm, RichTextPageQueryForm> implements IRichTextService {

    @Autowired
    private IRichTextContentService iRichTextContentService;

    @Override
    public void preAdd(RichTextAddForm addForm,RichText po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),RichText::getCode,false);
        }

    }

    @Override
    public void postAdd(RichTextAddForm richTextAddForm, RichText po) {
        //  添加后 添加内容
        RichTextContent richTextContent = iRichTextContentService.saveAsLatestContentVersion(richTextAddForm.getContent(), po.getId());
        if (richTextContent != null) {
            po.setLatestContentId(richTextContent.getId());
            // 来最新的内容版本数据id
            updateById(po);
        }

    }

    @Override
    public void preUpdate(RichTextUpdateForm updateForm,RichText po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            RichText byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),RichText::getCode,false);
            }
        }

    }

    @Override
    public void postUpdate(RichTextUpdateForm richTextUpdateForm, RichText po) {
        //  添加后 添加内容

        RichTextContent richTextContent = iRichTextContentService.saveAsLatestContentVersion(richTextUpdateForm.getContent(), po.getId());
        if (richTextContent != null) {
            po.setLatestContentId(richTextContent.getId());
            // 来最新的内容版本数据id
            updateById(po);
        }
    }
}
