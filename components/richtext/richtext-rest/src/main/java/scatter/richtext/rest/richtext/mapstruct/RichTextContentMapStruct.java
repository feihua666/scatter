package scatter.richtext.rest.richtext.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.pojo.richtext.vo.RichTextContentVo;

/**
 * <p>
 * 活动内容 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RichTextContentMapStruct extends IBaseVoMapStruct<RichTextContent, RichTextContentVo>{
    RichTextContentMapStruct INSTANCE = Mappers.getMapper( RichTextContentMapStruct.class );

}
