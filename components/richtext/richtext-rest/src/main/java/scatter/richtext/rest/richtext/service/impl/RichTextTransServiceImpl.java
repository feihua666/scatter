package scatter.richtext.rest.richtext.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.rest.richtext.service.IRichTextService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 富文本翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
public class RichTextTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IRichTextService richTextService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,RichText.TRANS_RICHTEXT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,RichText.TRANS_RICHTEXT_BY_ID)) {
            RichText byId = richTextService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,RichText.TRANS_RICHTEXT_BY_ID)) {
            return richTextService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
