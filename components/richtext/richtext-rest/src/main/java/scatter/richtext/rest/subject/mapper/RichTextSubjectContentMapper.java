package scatter.richtext.rest.subject.mapper;

import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 主体和内容关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface RichTextSubjectContentMapper extends IBaseMapper<RichTextSubjectContent> {

}
