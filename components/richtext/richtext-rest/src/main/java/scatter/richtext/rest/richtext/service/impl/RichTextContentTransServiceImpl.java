package scatter.richtext.rest.richtext.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.rest.richtext.service.IRichTextContentService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 活动内容翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
public class RichTextContentTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IRichTextContentService richTextContentService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,RichTextContent.TRANS_RICHTEXTCONTENT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,RichTextContent.TRANS_RICHTEXTCONTENT_BY_ID)) {
            RichTextContent byId = richTextContentService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,RichTextContent.TRANS_RICHTEXTCONTENT_BY_ID)) {
            return richTextContentService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
