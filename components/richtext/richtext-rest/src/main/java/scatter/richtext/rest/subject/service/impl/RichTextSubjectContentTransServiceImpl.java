package scatter.richtext.rest.subject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.rest.subject.service.IRichTextSubjectContentService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 主体和内容翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
public class RichTextSubjectContentTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IRichTextSubjectContentService richTextSubjectContentService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,RichTextSubjectContent.TRANS_RICHTEXTSUBJECTCONTENT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,RichTextSubjectContent.TRANS_RICHTEXTSUBJECTCONTENT_BY_ID)) {
            RichTextSubjectContent byId = richTextSubjectContentService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,RichTextSubjectContent.TRANS_RICHTEXTSUBJECTCONTENT_BY_ID)) {
            return richTextSubjectContentService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
