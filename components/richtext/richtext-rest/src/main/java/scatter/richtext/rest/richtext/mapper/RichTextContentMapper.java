package scatter.richtext.rest.richtext.mapper;

import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 活动内容表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface RichTextContentMapper extends IBaseMapper<RichTextContent> {

}
