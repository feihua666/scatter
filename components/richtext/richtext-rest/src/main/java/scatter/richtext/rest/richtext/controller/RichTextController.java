package scatter.richtext.rest.richtext.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.pojo.richtext.vo.RichTextVo;
import scatter.richtext.pojo.richtext.form.RichTextAddForm;
import scatter.richtext.pojo.richtext.form.RichTextUpdateForm;
import scatter.richtext.pojo.richtext.form.RichTextPageQueryForm;
import scatter.richtext.pojo.richtext.vo.RichTextWithLatestContentVo;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.richtext.rest.richtext.mapstruct.RichTextMapStruct;
import scatter.richtext.rest.richtext.service.IRichTextService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 富文本表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "富文本相关接口")
@RestController
@RequestMapping(RichtextConfiguration.CONTROLLER_BASE_PATH + "/richtext/rich-text")
public class RichTextController extends BaseAddUpdateQueryFormController<RichText, RichTextVo, RichTextAddForm, RichTextUpdateForm, RichTextPageQueryForm> {
    @Autowired
    private IRichTextService iRichTextService;

    @ApiOperation("添加富文本")
    @PreAuthorize("hasAuthority('RichText:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public RichTextVo add(@RequestBody @Valid RichTextAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询富文本")
    @PreAuthorize("hasAuthority('RichText:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public RichTextVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除富文本")
    @PreAuthorize("hasAuthority('RichText:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新富文本")
    @PreAuthorize("hasAuthority('RichText:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public RichTextVo update(@RequestBody @Valid RichTextUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询富文本")
    @PreAuthorize("hasAuthority('RichText:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<RichTextVo> getList(RichTextPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询富文本")
    @PreAuthorize("hasAuthority('RichText:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<RichTextVo> getPage(RichTextPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }



    @ApiOperation("根据ID查询富文本带最新版本内容")
    @PreAuthorize("hasAuthority('RichText:queryById')")
    @GetMapping("/queryByIdWithLatestContent/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RichTextWithLatestContentVo queryByIdWithLatestContent(@PathVariable String id) {
        RichTextVo richTextVo = super.queryById(id);
        return RichTextMapStruct.INSTANCE.richTextVoToRichTextWithLatestContentVo(richTextVo);
    }

    @ApiOperation("根据code查询富文本带最新版本内容")
    @GetMapping("/queryByCodeWithLatestContent/{code}")
    @ResponseStatus(HttpStatus.OK)
    public RichTextWithLatestContentVo queryByCodeWithLatestContent(@PathVariable String code) {
        RichText byCode = iRichTextService.getByCode(code);
        if (byCode != null) {
            throw new BusinessDataNotFoundException(true);
        }
        RichTextVo vo = getVo(byCode);
        return RichTextMapStruct.INSTANCE.richTextVoToRichTextWithLatestContentVo(vo);
    }
}
