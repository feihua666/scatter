package scatter.richtext.rest.richtext.mapper;

import scatter.richtext.pojo.richtext.po.RichText;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 富文本表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface RichTextMapper extends IBaseMapper<RichText> {

}
