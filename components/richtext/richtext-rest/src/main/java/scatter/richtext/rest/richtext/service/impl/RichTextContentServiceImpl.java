package scatter.richtext.rest.richtext.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseServiceImpl;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.rest.richtext.mapper.RichTextContentMapper;
import scatter.richtext.rest.richtext.service.IRichTextContentService;
/**
 * <p>
 * 活动内容表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class RichTextContentServiceImpl extends IBaseServiceImpl<RichTextContentMapper, RichTextContent> implements IRichTextContentService {

}
