package scatter.richtext.rest.richtext.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.pojo.richtext.vo.RichTextContentVo;
import scatter.richtext.rest.RichtextConfiguration;
import scatter.richtext.rest.richtext.mapstruct.RichTextContentMapStruct;
import scatter.richtext.rest.richtext.service.IRichTextContentService;
import scatter.richtext.rest.richtext.service.IRichTextService;
/**
 * <p>
 * 活动内容表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "活动内容相关接口")
@RestController
@RequestMapping(RichtextConfiguration.CONTROLLER_BASE_PATH + "/richtext/rich-text-content")
public class RichTextContentController extends SuperController {
    @Autowired
    private IRichTextContentService iRichTextContentService;
    @Autowired
    private IRichTextService iRichTextService;

    @ApiOperation("根据ID查询活动内容")
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RichTextContentVo getById(@PathVariable String id) {
        RichTextContent byId = iRichTextContentService.getById(id);
        RichTextContentVo richTextContentVo = RichTextContentMapStruct.INSTANCE.poToVo(byId);
        return richTextContentVo;
    }

    @ApiOperation("根据富文本编码获取最新版本内容")
    @GetMapping("/getByCode/{code}")
    @ResponseStatus(HttpStatus.OK)
    public RichTextContentVo getByCode(@PathVariable String code) {
        RichText byCode = iRichTextService.getByCode(code);
        if (byCode == null) {
            throw new BusinessDataNotFoundException("数据不存在",true);
        }
        RichTextContent byId = iRichTextContentService.getLatestContentVersion(byCode.getId());
        RichTextContentVo richTextContentVo = RichTextContentMapStruct.INSTANCE.poToVo(byId);
        return richTextContentVo;
    }
}
