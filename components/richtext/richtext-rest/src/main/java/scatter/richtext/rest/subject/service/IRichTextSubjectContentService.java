package scatter.richtext.rest.subject.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
/**
 * <p>
 * 主体和内容关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface IRichTextSubjectContentService extends IBaseService<RichTextSubjectContent> {


	/**
	 * 获取，唯一确定一条数据
	 * @param subjectId
	 * @param subjectType
	 * @param contentId
	 * @return
	 */
	default RichTextSubjectContent get(String subjectId,String subjectType,String contentId){
		Assert.hasText(subjectId,"subjectId 不能为空");
		Assert.hasText(subjectType,"subjectType 不能为空");
		Assert.hasText(contentId,"contentId 不能为空");

		LambdaQueryWrapper<RichTextSubjectContent> eq = Wrappers.<RichTextSubjectContent>lambdaQuery().eq(RichTextSubjectContent::getSubjectId, subjectId)
				.eq(RichTextSubjectContent::getSubjectType, subjectType)
				.eq(RichTextSubjectContent::getContentId, contentId);
		return getOne(eq);

	}
}
