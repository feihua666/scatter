package scatter.richtext.rest.subject.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.richtext.pojo.subject.form.RichTextSubjectContentUpdateForm;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.pojo.subject.vo.RichTextSubjectContentVo;

/**
 * <p>
 * 主体和内容 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RichTextSubjectContentMapStruct extends IBaseVoMapStruct<RichTextSubjectContent, RichTextSubjectContentVo>,
        IBaseUpdateFormMapStruct<RichTextSubjectContent, RichTextSubjectContentUpdateForm> {
    RichTextSubjectContentMapStruct INSTANCE = Mappers.getMapper( RichTextSubjectContentMapStruct.class );

}
