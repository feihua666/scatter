package scatter.richtext.rest.richtext.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.richtext.pojo.richtext.po.RichTextContent;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 活动内容表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface IRichTextContentService extends IBaseService<RichTextContent> {

    /**
     * 获取最新的版本的那条数据
     * @param richTextId
     * @return
     */
    default RichTextContent getLatestContentVersion(String richTextId){

        LambdaQueryWrapper<RichTextContent> eq = Wrappers.<RichTextContent>lambdaQuery().eq(RichTextContent::getRichTextId, richTextId).orderByDesc(RichTextContent::getContentVersion);
        List<RichTextContent> list = list(eq);
        if(isEmpty(list)){
            return null;
        }
        return list.iterator().next();

    }

    /**
     * 保存内容为最新版本
     * @param content
     * @param richTextId
     * @return
     */
    default RichTextContent saveAsLatestContentVersion(String content,String richTextId){
        Assert.hasText(content,"content 不能为空");
        Assert.hasText(richTextId,"richTextId 不能为空");

        RichTextContent latestContentVersion = getLatestContentVersion(richTextId);
        if (latestContentVersion != null && isEqual(content,latestContentVersion.getContent())) {
            return latestContentVersion;
        }

        int latestNewVersion = Optional.ofNullable(latestContentVersion).map(RichTextContent::getContentVersion).orElse(0) + 1;

        RichTextContent richTextContent = new RichTextContent().setContent(content).setRichTextId(richTextId).setContentVersion(latestNewVersion);

        save(richTextContent);

        return richTextContent;
    }

}
