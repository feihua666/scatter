package scatter.richtext.rest.test.richtext.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.rest.richtext.service.IRichTextContentService;
import scatter.richtext.rest.test.richtext.RichTextContentSuperTest;

import java.util.List;
/**
* <p>
* 活动内容 服务测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
public class RichTextContentServiceTest extends RichTextContentSuperTest{

    @Autowired
    private IRichTextContentService richTextContentService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<RichTextContent> pos = richTextContentService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}