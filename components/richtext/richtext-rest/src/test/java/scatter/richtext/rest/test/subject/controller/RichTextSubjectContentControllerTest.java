package scatter.richtext.rest.test.subject.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import scatter.richtext.rest.test.subject.RichTextSubjectContentSuperTest;
/**
* <p>
* 主体和内容 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
@AutoConfigureMockMvc
public class RichTextSubjectContentControllerTest extends RichTextSubjectContentSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

}