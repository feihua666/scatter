package scatter.richtext.rest.test.subject.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.rest.subject.service.IRichTextSubjectContentService;
import scatter.richtext.rest.test.subject.RichTextSubjectContentSuperTest;

import java.util.List;
/**
* <p>
* 主体和内容 服务测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
public class RichTextSubjectContentServiceTest extends RichTextSubjectContentSuperTest{

    @Autowired
    private IRichTextSubjectContentService richTextSubjectContentService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<RichTextSubjectContent> pos = richTextSubjectContentService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}