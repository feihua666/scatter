package scatter.richtext.rest.test.richtext.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.pojo.richtext.form.RichTextAddForm;
import scatter.richtext.pojo.richtext.form.RichTextUpdateForm;
import scatter.richtext.pojo.richtext.form.RichTextPageQueryForm;
import scatter.richtext.rest.test.richtext.RichTextSuperTest;
import scatter.richtext.rest.richtext.service.IRichTextService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 富文本 服务测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
public class RichTextServiceTest extends RichTextSuperTest{

    @Autowired
    private IRichTextService richTextService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<RichText> pos = richTextService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}