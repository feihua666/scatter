package scatter.richtext.rest.test.richtext;

import cn.hutool.core.util.ReflectUtil;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BaseTreePo;
import scatter.richtext.pojo.richtext.po.RichTextContent;
import scatter.richtext.rest.richtext.service.IRichTextContentService;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
/**
* <p>
* 活动内容 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class RichTextContentSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRichTextContentService richTextContentService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return richTextContentService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return richTextContentService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public RichTextContent mockPo() {
        return JMockData.mock(RichTextContent.class, mockConfig);
    }

}