package scatter.richtext.rest.test.subject;

import cn.hutool.core.util.ReflectUtil;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BaseTreePo;
import scatter.richtext.pojo.subject.form.RichTextSubjectContentUpdateForm;
import scatter.richtext.pojo.subject.po.RichTextSubjectContent;
import scatter.richtext.rest.subject.service.IRichTextSubjectContentService;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
/**
* <p>
* 主体和内容 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class RichTextSubjectContentSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRichTextSubjectContentService richTextSubjectContentService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return richTextSubjectContentService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return richTextSubjectContentService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public RichTextSubjectContent mockPo() {
        return JMockData.mock(RichTextSubjectContent.class, mockConfig);
    }



    /**
    * 模拟数据updateForm
    * @return
    */
    public RichTextSubjectContentUpdateForm mockUpdateForm() {
        return JMockData.mock(RichTextSubjectContentUpdateForm.class, mockConfig);
    }

}