package scatter.richtext.rest.test.richtext;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.richtext.pojo.richtext.po.RichText;
import scatter.richtext.pojo.richtext.form.RichTextAddForm;
import scatter.richtext.pojo.richtext.form.RichTextUpdateForm;
import scatter.richtext.pojo.richtext.form.RichTextPageQueryForm;
import scatter.richtext.rest.richtext.service.IRichTextService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 富文本 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class RichTextSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRichTextService richTextService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return richTextService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return richTextService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public RichText mockPo() {
        return JMockData.mock(RichText.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public RichTextAddForm mockAddForm() {
        return JMockData.mock(RichTextAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public RichTextUpdateForm mockUpdateForm() {
        return JMockData.mock(RichTextUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public RichTextPageQueryForm mockPageQueryForm() {
        return JMockData.mock(RichTextPageQueryForm.class, mockConfig);
    }
}