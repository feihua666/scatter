package scatter.richtext.pojo.richtext.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动内容表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_rich_text_content")
@ApiModel(value="RichTextContent对象", description="活动内容表")
public class RichTextContent extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_RICHTEXTCONTENT_BY_ID = "trans_richtextcontent_by_id_scatter.richtext.pojo.richtext.po";

    @ApiModelProperty(value = "富文本id")
    private String richTextId;

    @ApiModelProperty(value = "活动内容")
    private String content;

    @ApiModelProperty(value = "内容版本，如：1.0，2.0")
    private Integer contentVersion;


}
