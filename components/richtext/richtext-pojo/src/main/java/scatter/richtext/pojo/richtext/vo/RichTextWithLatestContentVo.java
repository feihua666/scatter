package scatter.richtext.pojo.richtext.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.richtext.pojo.richtext.po.RichTextContent;


/**
 * <p>
 * 富文本响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="富文本响应对象")
public class RichTextWithLatestContentVo extends RichTextVo {

    @TransBy(type = RichTextContent.TRANS_RICHTEXTCONTENT_BY_ID,byFieldName = "latestContentId",mapValueField = "content")
    @ApiModelProperty(value = "最新内容")
    private String latestContent;

}
