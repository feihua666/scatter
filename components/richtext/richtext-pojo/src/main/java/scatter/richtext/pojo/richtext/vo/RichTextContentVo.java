package scatter.richtext.pojo.richtext.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 活动内容响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="活动内容响应对象")
public class RichTextContentVo extends BaseIdVo {

    @ApiModelProperty(value = "富文本id")
    private String richTextId;

    @ApiModelProperty(value = "活动内容")
    private String content;

    @ApiModelProperty(value = "内容版本，如：1.0，2.0")
    private Integer contentVersion;

}
