package scatter.richtext.pojo.subject.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 主体和内容更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="主体和内容更新表单对象")
public class RichTextSubjectContentUpdateForm extends BaseForm {

    @NotEmpty(message="主体id不能为空")
    @ApiModelProperty(value = "主体id",required = true)
    private String subjectId;

    @NotEmpty(message="主体id类型不能为空")
    @ApiModelProperty(value = "主体id类型，用来说明主体id指的是什么id",required = true)
    private String subjectType;

    @NotEmpty(message="内容id不能为空")
    @ApiModelProperty(value = "内容id",required = true)
    private String contentId;

}
