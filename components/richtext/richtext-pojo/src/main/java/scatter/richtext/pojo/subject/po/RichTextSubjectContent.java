package scatter.richtext.pojo.subject.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主体和内容关系表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_rich_text_subject_content")
@ApiModel(value="RichTextSubjectContent对象", description="主体和内容关系表")
public class RichTextSubjectContent extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_RICHTEXTSUBJECTCONTENT_BY_ID = "trans_richtextsubjectcontent_by_id_scatter.richtext.pojo.subject.po";

    @ApiModelProperty(value = "主体id")
    private String subjectId;

    @ApiModelProperty(value = "主体id类型，用来说明主体id指的是什么id")
    private String subjectType;

    @ApiModelProperty(value = "内容id")
    private String contentId;


}
