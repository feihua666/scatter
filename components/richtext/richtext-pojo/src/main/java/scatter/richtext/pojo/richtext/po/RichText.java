package scatter.richtext.pojo.richtext.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 富文本表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_rich_text")
@ApiModel(value="RichText对象", description="富文本表")
public class RichText extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_RICHTEXT_BY_ID = "trans_richtext_by_id_scatter.richtext.pojo.richtext.po";

    @ApiModelProperty(value = "编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "最新的内容版本数据id")
    private String latestContentId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
