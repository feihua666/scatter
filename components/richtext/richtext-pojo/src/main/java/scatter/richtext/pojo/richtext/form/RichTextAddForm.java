package scatter.richtext.pojo.richtext.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 富文本添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="富文本添加表单对象")
public class RichTextAddForm extends BaseAddForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码，模糊查询",required = true)
    private String code;

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "最新的内容版本数据id")
    private String latestContentId;

    @NotEmpty(message="内容不能为空")
    @ApiModelProperty(value = "内容",required = true)
    private String content;

    @ApiModelProperty(value = "描述")
    private String remark;

}
