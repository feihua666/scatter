package scatter.richtext.pojo.subject.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 主体和内容响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="主体和内容响应对象")
public class RichTextSubjectContentVo extends BaseIdVo {

    @ApiModelProperty(value = "主体id")
    private String subjectId;

    @ApiModelProperty(value = "主体id类型，用来说明主体id指的是什么id")
    private String subjectType;

    @ApiModelProperty(value = "内容id")
    private String contentId;

}
