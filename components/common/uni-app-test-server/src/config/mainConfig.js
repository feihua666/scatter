import Vue from 'vue'
// http 请求库
import LuchRequest from '../../../../../components/common/web/components/app/uni-app/luch-request/LuchRequestPlugin.js'
// scatter 通用工具
import ToolsPlugin from '../../../../../components/common/web/tools/ToolsPlugin.js'
// scatter 对 uni-app 的通用工具
import ToolsUniPlugin from '../../../web/components/app/uni-app/tools/ToolsPlugin.js'
// 全局存储
import store from '../vuex/store.js'
// 全局当前登录用户
import CurrentUserMixin from '../mixins/CurrentUserMixin.js'
// 登录用户相关
import IdentifierUserUrl from '../../../../../components/identifier/web/pc/IdentifierUserUrl.js'
// vuex 增强 ******
import vuexEnhancePlugin from '../../../web/vuex/vuexEnhancePlugin.js'
// scater 自定义css 全局样式
import '../../../web/st.css'
// 安装
Vue.use(vuexEnhancePlugin,{
    store,
    localSaveStateKeys: []
})
// vuex 增强 结束******

// 设置当前登录用户方法为全局mixin
Vue.mixin(CurrentUserMixin)

Vue.use(LuchRequest,{
    baseURL: 'http://localhost:8080'
    // baseURL: 'http://wwd.ngrok2.xiaomiqiu.cn'
})
// 自定义工具插件
Vue.use(ToolsPlugin)
Vue.use(ToolsUniPlugin)

// 配置信息
Vue.prototype.$config = {
    hasLoginUrl: IdentifierUserUrl.hasLogin,
    userInfoUrl: IdentifierUserUrl.loginUserinfo,
    // 公众号配置编码，需要后台提供
    mpAppCode: 'ywPersonalTest',
    // 分享带标题所加 logo
    shareTitleLogo: '[靠谱单身]',
    // 用于图片拼接参数转换 结合 $stUrlTools.appendParam
    imageParam: {
        'x-oss-process': 'image/resize,h_528/auto-orient,1'
    }
}
// 将 store 导出
export {store}