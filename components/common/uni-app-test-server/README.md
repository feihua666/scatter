# uni-app-test-server

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 在安装 sass-loader 时可能会有依赖冲突问题
参见 https://blog.csdn.net/cvper/article/details/79543262