import VueRouter from 'vue-router'
import routes from './routes'

const router = new VueRouter({
    routes: routes
})
// 路由守卫
router.beforeEach((to, from, next) => {
    next()
})
export default router