import AreaRoute from '../../../../area/web/pc/AreaRoute.js'
import DictRoute from '../../../../dict/web/pc/DictRoute.js'

let routes = [
    {
        path: '/CronTest',
        component: () => import('@/views/CronTest.vue'),
        meta: {
            code:'test',
            name: 'cronTest'
        }
    },
    {
        path: '/thd-model',
        component: () => import('@/views/thd-model.vue'),
        meta: {
            code:'3dModel',
            name: '3dModel'
        }
    },
    {
        path: '/DPlayer',
        component: () => import('@/views/DPlayer.vue'),
        meta: {
            code:'DPlayer',
            name: 'DPlayer'
        }
    },
]
routes = routes.concat(AreaRoute)
.concat(DictRoute)
export default routes