package scatter.common.rest.config;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.lang.annotation.Annotation;
import java.util.function.Predicate;

/**
 * Created by yangwei
 * Created at 2020/8/4 13:13
 */
public interface SwaggerConfig {

    public static final String PARAMETER_TYPE_QUERY = "query";
    public static final String PARAMETER_TYPE_BODY = "body";
    public static final String PARAMETER_TYPE_FORM = "form";
    public static final String PARAMETER_TYPE_HEADER = "header";
    public static final String PARAMETER_TYPE_COOKIE = "cookie";

    default RequestParameterBuilder parameterBuilder(){
        return new RequestParameterBuilder();
    }

    default Docket docket(String grapeName){
        Docket docket = new Docket(DocumentationType.OAS_30);
        if(grapeName != null){
            docket = docket.groupName(grapeName);
        }
        return docket;
    }
    default Docket docket(){
        return docket(null);
    }

    default ApiInfo apiInfo(String name) {
        return new ApiInfoBuilder()
                //自定义信息可按需求填写
                .title("Scatter "+ name +" Swagger 接口文档")
                .build();
    }

    default Predicate<RequestHandler> basePackage(String basePackage){
        return RequestHandlerSelectors.basePackage(basePackage);
    }

    default Predicate<RequestHandler> withClassAnnotation(final Class<? extends Annotation> annotation){
        return RequestHandlerSelectors.withClassAnnotation(annotation);
    }
    default Predicate<RequestHandler> withMethodAnnotation(final Class<? extends Annotation> annotation){
        return RequestHandlerSelectors.withMethodAnnotation(annotation);
    }
    default Predicate<String> path(String path){
        return new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return  s.contains( path);
            }

        };
    }
    default ResolvedType resolvedTypeString(){
        return new TypeResolver().resolve(String.class);
    }


}
