package scatter.common.boot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/4/1 10:45
 */
@Slf4j
@Component
public class CommonCommandLineRunner implements CommandLineRunner {

    @Autowired(required = false)
    List<OnCommandLineListener> listeners;

    @Override
    public void run(String... args) throws Exception {
        log.debug("开始执行：" + this.getClass().getName());
        if (listeners != null) {
            for (OnCommandLineListener listener : listeners) {
                listener.run(args);
            }
        }
        log.debug("执行完成：" + this.getClass().getName());
    }
}
