package scatter.common.rest.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 自定义业务异常，数据不存在
 * Created by yangwei
 * Created at 2020/10/27 12:38
 */
public class BusinessDataNotFoundException extends BusinessException{

    public BusinessDataNotFoundException(boolean http) {
        super("数据不存在",http);
    }
    public BusinessDataNotFoundException(String message) {
        super(message);
    }

    public BusinessDataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessDataNotFoundException(String message, boolean http) {
        super(message, http);
    }

    public BusinessDataNotFoundException(String message, boolean http, Integer code) {
        super(message, http, code);
    }

    public BusinessDataNotFoundException(String message, boolean http, Object payload) {
        super(message, http, payload);
    }

    public BusinessDataNotFoundException(String message, boolean http, Integer code, Object payload) {
        super(message, http, code, payload);
    }

    public BusinessDataNotFoundException(String message, Throwable cause, boolean http) {
        super(message, cause, http);
    }

    public BusinessDataNotFoundException(String message, Throwable cause, boolean http, Object payload) {
        super(message, cause, http, payload);
    }

    public BusinessDataNotFoundException(String message, Integer code) {
        super(message, code);
    }

    public BusinessDataNotFoundException(String message, Integer code, Object payload) {
        super(message, code, payload);
    }
}
