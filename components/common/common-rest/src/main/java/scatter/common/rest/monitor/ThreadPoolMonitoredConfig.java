package scatter.common.rest.monitor;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.jvm.ExecutorServiceMetrics;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * <p>
 * 本意是配置系统的线程池实例，如果标了 {@link ThreadPoolMonitored} 注解，那么就对这些线程池做监控代理
 * 但现在没用到，因为 micrometer 本身已经提供了监控线程池的代理 {@link ExecutorServiceMetrics#monitor(io.micrometer.core.instrument.MeterRegistry, java.util.concurrent.ExecutorService, java.lang.String, io.micrometer.core.instrument.Tag...)}
 * </p>
 *
 * @author yangwei
 * @since 2021-08-24 11:50
 */
public class ThreadPoolMonitoredConfig {

	@ThreadPoolMonitored
	@Autowired(required = false)
	private List<ThreadPoolExecutor> threadPools;

	@Autowired
	private MeterRegistry meterRegistry;

}
