package scatter.common.rest.mapstruct;

/**
 * Created by yangwei
 * Created at 2020/10/27 14:43
 */
public interface IBaseUpdateFormMapStruct<Po,UpdateForm> {
    /**
     * 更新表单转po
     * @param updateForm
     * @return
     */
    Po updateFormToPo(UpdateForm updateForm);

}
