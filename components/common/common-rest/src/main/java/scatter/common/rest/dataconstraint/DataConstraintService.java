package scatter.common.rest.dataconstraint;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * <p>
 * 数据范围约束服务
 * </p>
 *
 * @author yangwei
 * @since 2020-11-04 23:05
 */
public interface DataConstraintService {
    /**
     * 数据范围约束条件包装
     * @param wrapper
     */
    AbstractWrapper dataConstraint(AbstractWrapper wrapper);
}
