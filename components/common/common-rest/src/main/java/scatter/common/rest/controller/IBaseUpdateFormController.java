package scatter.common.rest.controller;

import scatter.common.pojo.form.BaseUpdateIdForm;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.exception.BusinessDataModifiedByOtherException;
import scatter.common.rest.exception.BusinessDataNoPrivilegeException;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.service.IBaseUpdateFormService;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:58
 */
public interface IBaseUpdateFormController<Po extends BasePo,Vo extends BaseVo,UpdateForm> {
    
    IBaseUpdateFormService<Po,UpdateForm> getIBaseUpdateFormService();

    Vo queryById(String id);
    /**
     * 单表更新
     * @param updateForm
     * @return
     */

    default Vo update(UpdateForm updateForm){
        boolean r = getIBaseUpdateFormService().update(updateForm);
        if (!r) {
            Po byId = getIBaseUpdateFormService().getById(((BaseUpdateIdForm) updateForm).getId());
            if (byId != null) {
                if (!byId.getVersion().equals(((BaseUpdateIdForm) updateForm).getVersion())) {
                    throw new BusinessDataModifiedByOtherException(true);
                }
                throw new BusinessDataNoPrivilegeException(true);
            }
            throw new BusinessDataNotFoundException(true);
        }
        return queryById(((BaseUpdateIdForm)updateForm).getId());
    }
}
