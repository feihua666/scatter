package scatter.common.rest.service.mybatisplusfill;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.po.BaseTreePo;
import scatter.common.rest.security.LoginUserTool;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2019/9/23 9:05
 */
@Setter
@Getter
@AllArgsConstructor
public class MpFillConfig {
    private String property;
    private Object value;
    private boolean insert;
    private boolean update;

    private static List<MpFillConfig> config;
    static {
        config = new ArrayList<>();
        config.add(new MpFillConfig(BaseTreePo.PROPERTY_LEVEL,BaseTreePo.INIT_LEVEL,true,false));
        config.add(new MpFillConfig(BasePo.PROPERTY_VERSION,BasePo.INIT_VERSION,true,false));

    }

    public static List<MpFillConfig> getConfig(){
        List<MpFillConfig> configDynamic = new ArrayList<>();
        String userId = LoginUserTool.getLoginUserId();
        LocalDateTime now = LocalDateTime.now();
        configDynamic.add(new MpFillConfig(BasePo.PROPERTY_CREATE_BY, userId,true,false));
        configDynamic.add(new MpFillConfig(BasePo.PROPERTY_CREATE_AT, now,true,false));
        configDynamic.add(new MpFillConfig(BasePo.PROPERTY_UPDATE_BY, userId,false,true));
        configDynamic.add(new MpFillConfig(BasePo.PROPERTY_UPDATE_AT,now,false,true));
        configDynamic.addAll(config);
        return configDynamic;
    }
}
