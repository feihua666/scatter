package scatter.common.rest.validation;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * 字典服务，这是一个全局的字典服务
 * 主要是角色在使用字典服务时不显示依赖
 * 暂时主要由来是在表单验证时根据字典id换编码
 * Created by yangwei
 * Created at 2019/11/27 14:30
 */

public interface DictService {

    public static Logger log = LoggerFactory.getLogger(DictService.class);

    /**
     * 根据字典id获取字典值
     * @param id
     * @return
     */
    String getValueById(String id);

    /**
     * 根据字典id获取字典值
     * @param id
     * @return
     */
    String getNameById(String id);

    /**
     * 根据字典组编号和值获取字典id
     * @param groupCode
     * @param value
     * @return
     */
    String getIdByGroupCodeAndValue(String groupCode, String value);
    /**
     * 根据字典组编号和值获取字典id
     * groupCodeWithValue 是groupCode和value的拼接体，中间用横线分隔
     * @param groupCodeWithValue 格式为 groupCode-value
     * @return
     */
    default String getIdByGroupCodeWithValue(String groupCodeWithValue){
        if (StrUtil.isEmpty(groupCodeWithValue)) {
            return null;
        }
        String[] split = groupCodeWithValue.split("-");
        if (split.length !=2) {
            log.warn("groupCodeWithValue 格式不正确，直接返回空字典id，这很有可能是字典编码或字典值设置了横杠导致，groupCodeWithValue={}",groupCodeWithValue);
            return null;
        }
        return getIdByGroupCodeAndValue(split[0],split[1]);
    }


    /**
     * 检查value是否相等
     * @param dictItemId 字典项id
     * @param checkedValue 检查的值
     * @return
     */
    default boolean isEqualValueByDictItemId(String dictItemId,String checkedValue){

        Assert.hasText(dictItemId,"dictItemId 不能为空");
        Assert.hasText(checkedValue,"checkedValue 不能为空");
        String valueById = getValueById(dictItemId);
        return StrUtil.equals(valueById, checkedValue);
    }
}
