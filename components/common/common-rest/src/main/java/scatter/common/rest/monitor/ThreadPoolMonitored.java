package scatter.common.rest.monitor;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;

import java.lang.annotation.*;

/**
 * 自定义线程池监控注解
 * 注意该类标注了 {@link Qualifier} 注解，只是一个标注注解，该注解参考了 ribbion loadbalance 的实现 {@link LoadBalanced}
 */
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Qualifier
public @interface ThreadPoolMonitored {
}
