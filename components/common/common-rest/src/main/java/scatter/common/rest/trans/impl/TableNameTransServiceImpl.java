package scatter.common.rest.trans.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import scatter.common.rest.service.mapper.NativeSqlMapper;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.tools.StringTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 根据表名翻译，可以不再写翻译实现类，这个实现类有点人脉关系打通了TransHelper，特殊对待了该类的支持
 * 仅支持单表单字段
 * </p>
 *
 * @author yangwei
 * @since 2021-01-23 17:25
 */
@Component
@Order
public class TableNameTransServiceImpl implements ITransService<Object,Object>, InterfaceTool {

    @Autowired
    private NativeSqlMapper nativeSqlMapper;

    public static final String TRANS_BY_TABLE_NAME = "trans_by_table_name";

    @Override
    public boolean support(String type) {
        return false;
    }

    @Override
    public boolean supportBatch(String type) {
        return type.startsWith(TRANS_BY_TABLE_NAME);
    }

    @Override
    public List<TransResult<Object,Object>> transBatch(String type, Set<Object> keys) {
        String meta = type.replaceFirst(TRANS_BY_TABLE_NAME, "");
        String[] metas = meta.split(":");
        String selectColumn = StringTool.humpToLine(metas[0]);
        String tableName = metas[1];
        String whereColumn = StringTool.humpToLine(metas[2]);
        // 如果查询的中字段名是下划线格式，下面需要转一下
        List<Map<String,Object>> list = nativeSqlMapper.selectListByMyWrapper(tableName, Wrappers.query().select(selectColumn,whereColumn).in(whereColumn,keys));
        list = list.stream().map(item -> {
            Map<String, Object> m = new HashMap<>();
            for (String key : item.keySet()) {
                m.put(StringTool.lineToHump(key), item.get(key));
            }
            return m;
        }).collect(Collectors.toList());
        List<TransResult<Object, Object>> r = new ArrayList<>(keys.size());
        List<Map<String, Object>> collect = null;

        for (Object key : keys) {
            collect = list.stream().filter(item -> key.equals(item.get(StringTool.lineToHump(whereColumn)))).collect(Collectors.toList());
            if (!isEmpty(collect)) {
                r.add( new TransResult<Object, Object>(collect,key));
            }
        }
        return r;

    }

    @Override
    public TransResult<Object,Object> trans(String type, Object key) {
        return null;
    }
}
