package scatter.common.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.service.IBaseUpdateFormService;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:48
 */
public class BaseUpdateFormController<Po extends BasePo,Vo extends BaseVo,UpdateForm> extends BaseController<Po,Vo> implements IBaseUpdateFormController<Po,Vo,UpdateForm> {
    @Autowired
    private IBaseUpdateFormService<Po,UpdateForm> iBaseUpdateFormService;

    @Autowired
    private IBaseUpdateFormMapStruct<Po,UpdateForm> updateFormMapStruct;

    @Override
    public IBaseUpdateFormService<Po, UpdateForm> getIBaseUpdateFormService() {
        return iBaseUpdateFormService;
    }
}
