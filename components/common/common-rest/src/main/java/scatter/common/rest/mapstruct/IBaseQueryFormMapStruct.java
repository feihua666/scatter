package scatter.common.rest.mapstruct;

/**
 * Created by yangwei
 * Created at 2020/10/27 14:43
 */
public interface IBaseQueryFormMapStruct<Po,QueryForm> {

    /**
     * 查询表单转po
     * @param queryForm
     * @return
     */
    Po queryFormToPo(QueryForm queryForm);

}
