package scatter.common.rest.trans.impl;

import cn.hutool.core.date.DateUtil;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;

import java.util.Date;

/**
 * 提供时间戳转日期格式
 * Created by yangwei
 * Created at 2019/11/21 17:38
 */
@Component
public class DatetimeTransServiceImpl implements ITransService<String,Object>, InterfaceTool {

    public static final String TRANS_DATE = "date";
    public static final String TRANS_DATETIME = "datetime";

    @Override
    public boolean support(String type) {
        return isEqualAny(type, TRANS_DATE, TRANS_DATETIME);
    }


    @Override
    public TransResult<String,Object> trans(String type, Object key) {
        if (key == null) {
            return null;
        }
        if(isEqual(TRANS_DATE,type)){
            // yyyy-MM-dd
            if(key instanceof Long){
                return new TransResult(DateUtil.formatDate(DateUtil.date(((Long) key))),key);
            }else if(key instanceof Date){
                return new TransResult(DateUtil.formatDate((Date) key),key);
            }
        }else if(isEqual(TRANS_DATETIME,type)){
            // yyyy-MM-dd HH:mm:ss
            if(key instanceof Long){
                return new TransResult(DateUtil.formatDateTime(DateUtil.date(((Long) key))),key);
            }else if(key instanceof Date){
                return new TransResult(DateUtil.formatDateTime((Date) key),key);
            }
        }
        return null;
    }
}
