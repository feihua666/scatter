package scatter.common.rest.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.core.toolkit.support.SerializedLambda;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.tools.StringTool;

/**
 * mybatis plus 工具类
 * Created by yangwei
 * Created at 2020/11/9 18:08
 */
public class SFunctionHelperTool {
    private static EmptyLambda emptyLambda = new EmptyLambda();

    private static class EmptyLambda extends LambdaQueryWrapper{
        @Override
		public String columnToString(SFunction column) {
            return super.columnToString(column);
        }
    }

    /**
     * lambda表达式获取对应的数据库字段名
     * @param column User::getNickname
     * @return 数据库字段名
     */
    public static String columnToString(SFunction column) {
        return emptyLambda.columnToString(column);
    }

    /**
     * lambda表达式获取对应的实体属性名
     * 这里只是将数据库字段转为了下划线转驼峰
     * @param column
     * @return
     */
    public static String columnPropertyString(SFunction column){
        String columnString = columnToString(column);
        if (StrUtil.isEmpty(columnString)) {
            return null;
        }
        return StringTool.lineToHump(columnString);
    }
}
