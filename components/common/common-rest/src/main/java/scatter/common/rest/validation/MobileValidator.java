package scatter.common.rest.validation;

import cn.hutool.core.lang.Validator;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by yangwei
 * Created at 2019/11/27 13:52
 */
@Component
public class MobileValidator implements ConstraintValidator<Mobile, String>, InterfaceTool {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(!isStrEmpty(value)){
            return Validator.isMobile(value);
        }
        return true;
    }
}
