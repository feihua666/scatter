package scatter.common.rest.mapstruct;

/**
 * Created by yangwei
 * Created at 2020/10/27 14:43
 */
public interface IBaseAddFormMapStruct<Po,AddForm> {

    /**
     * 添加表单转po
     * @param addform
     * @return
     */
    Po addFormToPo(AddForm addform);

}
