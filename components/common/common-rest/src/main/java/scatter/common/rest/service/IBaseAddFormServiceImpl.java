package scatter.common.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.po.BaseTreePo;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/5 10:18
 */
public class IBaseAddFormServiceImpl<Mapper extends IBaseMapper<Po>,Po extends BasePo,AddForm> extends IBaseServiceImpl<Mapper,Po> implements IBaseAddFormService<Po,AddForm> {

    @Autowired(required = false)
    private List<IAddServiceListener<Po,AddForm>> addServiceListeners;
    @Autowired
    protected IBaseAddFormMapStruct<Po,AddForm> iBaseAddFormMapStruct;

    @Override
    public IBaseAddFormMapStruct<Po, AddForm> getAddFormMapStruct() {
        return iBaseAddFormMapStruct;
    }

    @Override
    public List<IAddServiceListener<Po, AddForm>> getAddServiceListeners() {
        return addServiceListeners;
    }
}
