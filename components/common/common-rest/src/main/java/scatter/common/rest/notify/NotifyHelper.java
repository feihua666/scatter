package scatter.common.rest.notify;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;

import java.util.List;

/**
 * <p>
 * 注入该 bean 可以直接发起通知
 * </p>
 *
 * @author yangwei
 * @since 2021-08-13 17:19
 */
@Slf4j
@Component
public class NotifyHelper implements InterfaceTool {


	@Autowired(required = false)
	private List<INotifyListener> iNotifyListenerList;

	/**
	 * 发起通知
	 * @param notifyParam
	 */
	void notify(NotifyParam notifyParam){
		if (!isEmpty(iNotifyListenerList)) {
			for (INotifyListener iNotifyListener : iNotifyListenerList) {
				iNotifyListener.notify(notifyParam);
			}
		}else {
			log.warn("未配置 INotifyListener 实例，未能发出通知，notifyParam={}",toJsonStr(notifyParam));
		}
	}





}
