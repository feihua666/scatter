package scatter.common.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.dataconstraint.DataConstraintService;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;

/**
 * Created by yangwei
 * Created at 2021/1/5 10:50
 */
public class IBaseQueryFormServiceImpl<Mapper extends IBaseMapper<Po>,Po extends BasePo,QueryForm> extends IBaseServiceImpl<Mapper,Po> implements IBaseQueryFormService<Po,QueryForm> {

    @Autowired
    protected IBaseQueryFormMapStruct<Po,QueryForm> iBaseQueryFormMapStruct;

    @Override
    public IBaseQueryFormMapStruct<Po, QueryForm> getQueryFormMapStruct() {
        return iBaseQueryFormMapStruct;
    }

    @Override
    public DataConstraintService getQueryDataConstraintService() {
        return dataConstraintService;
    }

}
