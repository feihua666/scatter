package scatter.common.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.dataconstraint.DataConstraintService;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:00
 */
public class IBaseAddUpdateFormServiceImpl<Mapper extends IBaseMapper<Po>,Po extends BasePo,AddForm,UpdateForm> extends IBaseServiceImpl<Mapper,Po> implements IBaseAddFormService<Po,AddForm>,IBaseUpdateFormService<Po,UpdateForm> {

    @Autowired(required = false)
    private List<IAddServiceListener<Po,AddForm>> addServiceListeners;
    @Autowired
    private IBaseAddFormMapStruct<Po,AddForm> iBaseAddFormMapStruct;
    @Autowired(required = false)
    protected List<IUpdateServiceListener<Po,UpdateForm>> updateServiceListeners;

    @Autowired
    protected IBaseUpdateFormMapStruct<Po,UpdateForm> iBaseUpdateFormMapStruct;

    @Override
    public IBaseAddFormMapStruct<Po, AddForm> getAddFormMapStruct() {
        return iBaseAddFormMapStruct;
    }

    @Override
    public List<IAddServiceListener<Po, AddForm>> getAddServiceListeners() {
        return addServiceListeners;
    }

    @Override
    public IBaseUpdateFormMapStruct<Po, UpdateForm> getUpdateFormMapStruct() {
        return iBaseUpdateFormMapStruct;
    }

    @Override
    public List<IUpdateServiceListener<Po, UpdateForm>> getUpdateServiceListeners() {
        return updateServiceListeners;
    }

    @Override
    public DataConstraintService getUpdateDataConstraintService() {
        return dataConstraintService;
    }

}
