package scatter.common.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.service.IBaseAddFormService;
import scatter.common.rest.service.IBaseQueryFormService;
import scatter.common.rest.service.IBaseUpdateFormService;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:48
 */
public class BaseAddUpdateQueryFormController<Po extends BasePo,Vo extends BaseVo,AddForm,UpdateForm,QueryForm> extends BaseController<Po,Vo> implements IBaseAddFormController<Po,Vo,AddForm> ,IBaseUpdateFormController<Po,Vo,UpdateForm>,IBaseQueryFormController<Po,Vo,QueryForm> {
    @Autowired
    private IBaseAddFormService<Po,AddForm> iBaseAddFormService;

    @Autowired
    private IBaseAddFormMapStruct<Po,AddForm> addFormMapStruct;

    @Override
    public IBaseAddFormService<Po, AddForm> getIBaseAddFormService() {
        return iBaseAddFormService;
    }
    @Autowired
    private IBaseUpdateFormService<Po,UpdateForm> iBaseUpdateFormService;

    @Autowired
    private IBaseUpdateFormMapStruct<Po,UpdateForm> updateFormMapStruct;

    @Override
    public IBaseUpdateFormService<Po, UpdateForm> getIBaseUpdateFormService() {
        return iBaseUpdateFormService;
    }
    @Autowired
    private IBaseQueryFormService<Po,QueryForm> iBaseQueryFormService;

    @Autowired
    private IBaseQueryFormMapStruct<Po,QueryForm> queryFormMapStruct;

    @Override
    public IBaseQueryFormService<Po, QueryForm> getIBaseQueryFormService() {
        return iBaseQueryFormService;
    }
}
