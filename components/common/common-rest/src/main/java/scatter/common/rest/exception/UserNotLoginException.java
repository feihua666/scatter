package scatter.common.rest.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 用户未登录异常，需要用户登录
 * Created by yangwei
 * Created at 2020/12/11 14:27
 */
public class UserNotLoginException extends AuthenticationException {

    public UserNotLoginException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public UserNotLoginException(String msg) {
        super(msg);
    }
}
