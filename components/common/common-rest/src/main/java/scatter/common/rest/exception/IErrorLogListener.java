package scatter.common.rest.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 错误异常日志监听，主要用于请求有异常时记录异常信息
 * common组件中只发起调用，不做处理，只允许有一个实例
 * Created by yangwei
 * Created at 2021/1/29 14:49
 */
public interface IErrorLogListener {

    /**
     * 记录异常日志
     * @param request
     * @param response
     * @param handler
     */
    public void onError(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex);
}
