package scatter.common.rest.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p>
 * 添加一个扫描配置类，有助于解决循环依赖
 * </p>
 *
 * @author yangwei
 * @since 2021-08-03 18:05
 */
@ComponentScan({"scatter.common.rest.advise",
		"scatter.common.rest.aspect","scatter.common.rest.trans",
		"scatter.common.rest.validation",
		"scatter.common.rest.tools",
		"scatter.common.boot",
		"scatter.common.rest.concurrency.ratelimit",
		"scatter.common.rest.interceptor",
		"scatter.common.rest.notify",
		"scatter.common.rest.monitor",
		"scatter.common.sqlinit",
})
@MapperScan("scatter.common.rest.service.mapper")
public class CommonComponentScanConfig {
}
