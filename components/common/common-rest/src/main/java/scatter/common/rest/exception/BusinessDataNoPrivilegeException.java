package scatter.common.rest.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 自定义业务异常，无数据权限不存在
 * Created by yangwei
 * Created at 2020/10/27 12:38
 */
public class BusinessDataNoPrivilegeException extends BusinessException{

    public BusinessDataNoPrivilegeException(boolean http) {
        super("数据不存在,没有数据权限",http);
    }
    public BusinessDataNoPrivilegeException(String message) {
        super(message);
    }

    public BusinessDataNoPrivilegeException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessDataNoPrivilegeException(String message, boolean http) {
        super(message, http);
    }

    public BusinessDataNoPrivilegeException(String message, boolean http, Integer code) {
        super(message, http, code);
    }

    public BusinessDataNoPrivilegeException(String message, boolean http, Object payload) {
        super(message, http, payload);
    }

    public BusinessDataNoPrivilegeException(String message, boolean http, Integer code, Object payload) {
        super(message, http, code, payload);
    }

    public BusinessDataNoPrivilegeException(String message, Throwable cause, boolean http) {
        super(message, cause, http);
    }

    public BusinessDataNoPrivilegeException(String message, Throwable cause, boolean http, Object payload) {
        super(message, cause, http, payload);
    }

    public BusinessDataNoPrivilegeException(String message, Integer code) {
        super(message, code);
    }

    public BusinessDataNoPrivilegeException(String message, Integer code, Object payload) {
        super(message, code, payload);
    }
}
