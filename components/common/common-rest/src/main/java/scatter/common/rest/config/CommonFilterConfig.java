package scatter.common.rest.config;

import brave.Tracer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.sleuth.autoconfig.instrument.web.SleuthWebProperties;
import org.springframework.context.annotation.Bean;
import scatter.common.rest.filter.RequestBodyReadableFilter;
import scatter.common.rest.filter.RequestParamValidateFilter;
import scatter.common.rest.filter.RequestResponseLogFilter;
import scatter.common.rest.filter.ResponseTraceIdFilter;

/**
 * <p>
 * 过滤器们配置类
 * </p>
 *
 * @author yangwei
 * @since 2021-08-02 14:21
 */
public class CommonFilterConfig {


	private static int span = 5;


	/**
	 * 响应头添加traceid过滤器
	 * @return
	 */
	@Bean
	public FilterRegistrationBean responseTraceIdFilter(Tracer tracer) {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new ResponseTraceIdFilter(tracer));
		registrationBean.setOrder(SleuthWebProperties.TRACING_FILTER_ORDER + span);
		return registrationBean;
	}
	/**
	 * 可读请求体过滤器
	 * @return
	 */
	@Bean
	public FilterRegistrationBean requestBodyReadableFilter() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new RequestBodyReadableFilter());
		registrationBean.setOrder(SleuthWebProperties.TRACING_FILTER_ORDER + span * 2);
		return registrationBean;
	}
	/**
	 * 参数校验过滤器，如签名等
	 * @return
	 */
	@Bean
	public FilterRegistrationBean requestParamValidateFilter() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new RequestParamValidateFilter());
		registrationBean.setOrder(SleuthWebProperties.TRACING_FILTER_ORDER + span * 3);
		return registrationBean;
	}

	/**
	 * 请求响应日志打印过滤器
	 * @return
	 */
	@Bean
	public FilterRegistrationBean requestResponseLogFilter() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new RequestResponseLogFilter());
		registrationBean.setOrder(SleuthWebProperties.TRACING_FILTER_ORDER + span * 4);
		return registrationBean;
	}
}
