package scatter.common.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.service.IBaseAddFormService;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:48
 */
public class BaseAddFormController<Po extends BasePo,Vo extends BaseVo,AddForm> extends BaseController<Po,Vo> implements IBaseAddFormController<Po,Vo,AddForm> {
    @Autowired
    private IBaseAddFormService<Po,AddForm> iBaseAddFormService;

    @Autowired
    private IBaseAddFormMapStruct<Po,AddForm> addFormMapStruct;

    @Override
    public IBaseAddFormService<Po, AddForm> getIBaseAddFormService() {
        return iBaseAddFormService;
    }

}
