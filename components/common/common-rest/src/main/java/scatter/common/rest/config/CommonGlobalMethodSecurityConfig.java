package scatter.common.rest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.vote.AbstractAccessDecisionManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import scatter.common.rest.security.voter.NoAuthenticationAuthorityStrConfigVoter;
import scatter.common.rest.security.voter.SuperAdminRoleVoter;

/**
 * 自定义注解扩展,开启controller注解权限配置
 * Created by yangwei
 * Created at 2020/1/14 11:42
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CommonGlobalMethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    @Override
    protected AccessDecisionManager accessDecisionManager() {
        AbstractAccessDecisionManager accessDecisionManager = (AbstractAccessDecisionManager) super.accessDecisionManager();
        accessDecisionManager.getDecisionVoters().add(new SuperAdminRoleVoter());
        accessDecisionManager.getDecisionVoters().add(noAuthenticationAuthorityStrConfigVoter());
        return accessDecisionManager;
    }
    @Bean
    public NoAuthenticationAuthorityStrConfigVoter noAuthenticationAuthorityStrConfigVoter() {
        return new NoAuthenticationAuthorityStrConfigVoter();
    }
}
