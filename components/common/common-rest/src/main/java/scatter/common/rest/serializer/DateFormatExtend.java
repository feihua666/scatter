package scatter.common.rest.serializer;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import scatter.common.rest.tools.calendar.CalendarUtils;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description 日期格式处理
 * @Author Ciaj.
 * @Date 2019/5/8 16:42
 * @Version 1.0
 */
@Slf4j
public class DateFormatExtend extends DateFormat {
    private DateFormat dateFormat;

    public DateFormatExtend() {
        this.dateFormat = new SimpleDateFormat(CalendarUtils.DateStyle.YYYY_MM_DD_HH_MM_SS.getValue());
    }


    public DateFormatExtend(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * 序列化时会执行这个方法
     *
     * @param date
     * @param toAppendTo
     * @param fieldPosition
     * @return
     */
    @Override
    public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
        return dateFormat.format(date, toAppendTo, fieldPosition);
    }

    /**
     * 反序列化时执行此方法，我们先让他执行我们自己的format，如果异常则执执行他的
     *
     * @param source
     * @param pos
     * @return
     */
    @Override
    public Date parse(String source, ParsePosition pos) {
        return getParseDate(source);
    }

    /**
     * 反序列化时执行此方法，我们先让他执行我们自己的format，如果异常则执执行他的
     *
     * @param source
     * @return
     */
    @Override
    public Date parse(String source) {
        return getParseDate(source);
    }

    private Date getParseDate(String source) {
        if (StrUtil.isBlank(source)){
            return null;
        }
        try {
            return DateUtil.parse(source, LocalDateTimeDeserializerExtend.FORMATS.toArray(new String[0]));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 此方法在objectmapper 默认的dateformat里边用到，这里也要重写
     *
     * @return
     */
    @Override
    public Object clone() {
        return new DateFormatExtend();
    }
}
