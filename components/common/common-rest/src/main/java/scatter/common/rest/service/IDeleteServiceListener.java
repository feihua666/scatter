package scatter.common.rest.service;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;

import java.util.List;

/**
 * <p>
 * service删除方法监听 {@link IBaseServiceImpl#deleteByColumn(String, SFunction)} {@link IBaseServiceImpl#deleteById(String)}
 * </p>
 *
 * @author yangwei
 * @since 2020-11-22 11:00
 */
public interface IDeleteServiceListener<Po> {

    default void preDeleteById(String id,Po po) {

    }
    default void postDeleteById(String id,Po po) {

    }

    default void preDeleteByColumn(String columnId, SFunction<Po, ?> column, List<Po> pos) {

    }
    default void postDeleteByColumn(String columnId , SFunction<Po, ?> column, List<Po> pos) {

    }
}
