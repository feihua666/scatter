package scatter.common.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.service.IBaseService;
import scatter.common.rest.tools.InterfaceTool;

import java.util.List;

/**
 * <p>
 * 通用带po的内部调用基础controller
 * </p>
 *
 * @author yangwei
 * @since 2020-10-12 12:49
 */

public abstract class BaseInnerController<Po extends BasePo> extends SuperController implements InterfaceTool {


    @Autowired
    private IBaseService<Po> iBaseService;


    @ApiOperation(value = "添加")
    @PostMapping
    public Po save(@RequestBody Po po) {
        boolean r = iBaseService.save(po);
        if(r){
            return  po;
        }
        return null;
    }

    @ApiOperation(value = "根据ID更新")
    @PutMapping
    public Po updateById(@RequestBody Po po) {
        boolean r = iBaseService.updateById(po);
        if(r){
            return  po;
        }
        return null;
    }

    @ApiOperation(value = "根据id查询")
    @GetMapping("/{id}")
    public Po getById(@PathVariable String id) {
        return (Po) iBaseService.getById(id);
    }

    @ApiOperation(value = "根据id删除")
    @DeleteMapping("/{id}")
    public Boolean removeById(@PathVariable String id) {
        return iBaseService.removeById(id);
    }

    @ApiOperation(value = "分页查询列表")
    @GetMapping("/page")
    public IPage<Po> listPage(BasePageQueryForm pageForm, Po po) {
        IPage<Po> page = iBaseService.page(new Page(pageForm.getCurrent(),pageForm.getSize()), Wrappers.query(po));
        return page;
    }

    @ApiOperation(value = "不分页查询列表")
    @GetMapping("/list")
    public List<Po> list(Po po) {
        List<Po> list = iBaseService.list(Wrappers.query(po));
        return list;
    }
}
