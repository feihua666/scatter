package scatter.common.rest.notify;

import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;
import scatter.common.boot.OnApplicationRunnerListener;
import scatter.common.boot.OnApplicationShutdownListener;

/**
 * <p>
 * 应用启动关闭通知监听
 * </p>
 *
 * @author yangwei
 * @since 2021-08-31 10:07
 */
@Component
public class ApplicationStartShutdownNotifyListener implements OnApplicationRunnerListener, OnApplicationShutdownListener {
	@Override
	public void run(ApplicationArguments args) throws Exception {
		NotifyParam notifyParam = NotifyParam.system()
				.setTitle("应用已启动")
				.setContentType("application.boot.start")
				.setContent(StrUtil.format("hostname={},host={}", NetUtil.getLocalHostName(),NetUtil.getLocalhostStr()));
		NotifyTool.notify(notifyParam);
	}

	@Override
	public void shutdown() {
		NotifyParam notifyParam = NotifyParam.system()
				.setTitle("应用已停止")
				.setContentType("application.boot.start")
				.setContent(StrUtil.format("hostname={},host={}", NetUtil.getLocalHostName(),NetUtil.getLocalhostStr()));

		NotifyTool.notify(notifyParam);
	}
}
