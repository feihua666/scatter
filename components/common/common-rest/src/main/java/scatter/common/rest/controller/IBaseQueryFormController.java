package scatter.common.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import scatter.common.pojo.form.BaseUpdateIdForm;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.exception.BusinessDataModifiedByOtherException;
import scatter.common.rest.exception.BusinessDataNoPrivilegeException;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.common.rest.service.IBaseQueryFormService;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:58
 */
public interface IBaseQueryFormController<Po extends BasePo,Vo extends BaseVo,QueryForm> {
    
    IBaseQueryFormService<Po,QueryForm> getIBaseQueryFormService();

    IPage<Vo> pagePoToVo(IPage<Po> poPage);

    List<Vo> posToVos(List<Po> list);
    /**
     * 单表分页查询，默认考虑数据范围约束，一般适用于后台管理
     * @param queryForm
     * @return
     */
    default IPage<Vo> getPage(QueryForm queryForm){
        return getPage(queryForm,true);
    }
    /**
     * 单表分页查询
     * @param queryForm
     * @param dataConstraint
     * @return
     */
    default IPage<Vo> getPage(QueryForm queryForm,boolean dataConstraint){
        Page<Po> poPage = getIBaseQueryFormService().listPage(queryForm,dataConstraint);
        if (poPage.getTotal() == 0) {
            throw new BusinessDataNotFoundException(true);
        }
        return pagePoToVo(poPage);
    }
    /**
     * 单表不分页查询,默认考虑数据权限约束，一般适用于后台管理
     * @param queryForm
     * @return
     */
    default List<Vo> getList(QueryForm queryForm){
        return getList(queryForm,true);
    }
    /**
     * 单表不分页查询
     * @param queryForm
     * @param dataConstraint
     * @return
     */
    default List<Vo> getList(QueryForm queryForm,boolean dataConstraint){
        List<Po> list = getIBaseQueryFormService().list(queryForm,dataConstraint);
        if (list.isEmpty()) {
            throw new BusinessDataNotFoundException(true);
        }
        return posToVos(list);
    }
}
