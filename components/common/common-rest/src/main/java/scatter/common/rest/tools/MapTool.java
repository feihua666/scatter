package scatter.common.rest.tools;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.NumberUtil;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-06-22 21:38
 */
public class MapTool {
	/**
	 * 深度获取map属性值,仅支付key为字符串
	 * @param map
	 * @param deepKey 如：a.b.c 支持集合如：a.0.b
	 * @param <T>
	 * @return
	 */
	public static <T> T getDeep(Map<String, ?> map, String deepKey){
		if (map == null) {
			return null;
		}
		Object value = map;
		for (String key : deepKey.split(".")) {
			if(value instanceof Map){
				value = ((Map) value).get(key);
			}else if(value instanceof List){
				if(NumberUtil.isInteger(key)){
					value = ((List) value).get(NumberUtil.parseInt(key));
				}
			}
		}
		return (T) value;
	}
}
