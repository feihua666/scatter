package scatter.common.rest.exception;

/**
 * 自定义业务异常，由于数据版本控制，数据被他人修改
 * Created by yangwei
 * Created at 2020/10/27 12:38
 */
public class BusinessDataModifiedByOtherException extends BusinessException{

    public BusinessDataModifiedByOtherException(boolean http) {
        super("数据已被他人修改，请刷新后再试",http);
    }
    public BusinessDataModifiedByOtherException(String message) {
        super(message);
    }

    public BusinessDataModifiedByOtherException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessDataModifiedByOtherException(String message, boolean http) {
        super(message, http);
    }

    public BusinessDataModifiedByOtherException(String message, boolean http, Integer code) {
        super(message, http, code);
    }

    public BusinessDataModifiedByOtherException(String message, boolean http, Object payload) {
        super(message, http, payload);
    }

    public BusinessDataModifiedByOtherException(String message, boolean http, Integer code, Object payload) {
        super(message, http, code, payload);
    }

    public BusinessDataModifiedByOtherException(String message, Throwable cause, boolean http) {
        super(message, cause, http);
    }

    public BusinessDataModifiedByOtherException(String message, Throwable cause, boolean http, Object payload) {
        super(message, cause, http, payload);
    }

    public BusinessDataModifiedByOtherException(String message, Integer code) {
        super(message, code);
    }

    public BusinessDataModifiedByOtherException(String message, Integer code, Object payload) {
        super(message, code, payload);
    }
}
