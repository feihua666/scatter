package scatter.common.rest.service;

import cn.hutool.core.util.ReflectUtil;
import scatter.common.pojo.po.BaseTreePo;
import scatter.common.rest.exception.BusinessException;

import static scatter.common.pojo.po.BaseTreePo.INIT_LEVEL;
import static scatter.common.pojo.po.BaseTreePo.MAX_LEVEL;

/**
 * Created by yangwei
 * Created at 2020/11/9 17:24
 */
public class TreeServiceHelperTool {

    /**
     * 根据风节点，设置子节点parentIdx的值包括parentId本身
     * @param child
     * @param parent
     * @param <Po>
     * @return
     */
    public static <Po> Po initParentIdXByParent(Po child, Po parent){
        // 先初始化child
        ((BaseTreePo) child).setLevel(INIT_LEVEL);
        ((BaseTreePo) child).setParentId(null);
        String parentIdx = null;
        for (int i = 1; i < MAX_LEVEL; i++) {
            parentIdx = BaseTreePo.PROPERTY_PARENT_ID + i;
            ReflectUtil.setFieldValue(child,parentIdx,null);
        }

        if (parent != null) {
            ((BaseTreePo) child).setLevel(((BaseTreePo) parent).getLevel() + 1);
            if(((BaseTreePo) child).getLevel() > MAX_LEVEL){
                throw new BusinessException("最大支持的树深度为" + MAX_LEVEL);
            }
            ((BaseTreePo) child).setParentId(((BaseTreePo) parent).getId());
            for (int i = 1; i < MAX_LEVEL; i++) {
                parentIdx = BaseTreePo.PROPERTY_PARENT_ID + i;
                if (((BaseTreePo) child).getLevel().equals(i + 1)) {
                    ReflectUtil.setFieldValue(((BaseTreePo) child),parentIdx,((BaseTreePo) parent).getId());
                    break;
                }else {
                    ReflectUtil.setFieldValue(child,parentIdx, ReflectUtil.getFieldValue(parent,parentIdx));
                }
            }
        }
        return (Po) child;
    }

    /**
     * 根据父级的level设置查询的parentIdx
     * @param child
     * @param parent
     */
    public static <Po> Po initQueryChildrenParentIdXByParentLevel(Po child, Po parent){
        String parentIdx = BaseTreePo.PROPERTY_PARENT_ID + ((BaseTreePo) parent).getLevel();
        ReflectUtil.setFieldValue(child,parentIdx,((BaseTreePo) parent).getId());
        return child;
    }
}
