package scatter.common.rest.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 缓存帮助类，目前该帮助类，只考虑到了字典翻译手动清除的场景
 * </p>
 *
 * @author yangwei
 * @since 2021-11-08 17:43
 */

public class CacheHelper {

	@Autowired
	private CacheManager cacheManager;

	/**
	 * spring cache 默认是SimpleKeyGenerator，它并没有默认注入的spring容器中，如果用户自定义了，这里会有值
	 */
	@Autowired(required = false)
	private KeyGenerator keyGenerator;

	/**
	 * 删除指定的参数缓存，请按缓存的方法来传递参数
	 * @param cacheName
	 * @param params
	 */
	public void removeCacheByParam(String cacheName, Object... params){
		Cache cache = cacheManager.getCache(cacheName);
		if (cache != null) {
			if (keyGenerator == null) {
				keyGenerator = new SimpleKeyGenerator();
			}
			Object generate = keyGenerator.generate(null, null, params);
			cache.evict(generate);
		}
	}

}
