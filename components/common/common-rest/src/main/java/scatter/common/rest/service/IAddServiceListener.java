package scatter.common.rest.service;

/**
 * <p>
 * service添加方法监听 {@link IBaseServiceImpl#add(java.lang.Object)}
 * </p>
 *
 * @author yangwei
 * @since 2020-11-22 11:00
 */
public interface IAddServiceListener<Po,AddForm> {
    default void preAdd(AddForm addForm,Po po) {

    }
    default void postAdd(AddForm addForm,Po po) {

    }
}
