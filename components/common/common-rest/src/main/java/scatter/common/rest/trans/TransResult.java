package scatter.common.rest.trans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import scatter.common.pojo.BasePojo;

/**
 * Created by yangwei
 * Created at 2020/11/27 10:48
 */
@Getter
@Setter
@NoArgsConstructor // 缓存时需要
@AllArgsConstructor
public class TransResult<R,K> extends BasePojo {
    private R transValue;
    private K key;


}
