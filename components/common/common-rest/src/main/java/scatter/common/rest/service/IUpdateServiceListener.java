package scatter.common.rest.service;

/**
 * <p>
 * service更新方法监听 {@link IBaseServiceImpl#update(Object)}
 * </p>
 *
 * @author yangwei
 * @since 2020-11-22 11:00
 */
public interface IUpdateServiceListener<Po,UpdateForm> {
    default void preUpdate(UpdateForm updateForm,Po po) {

    }
    default void postUpdate(UpdateForm updateForm,Po po) {

    }
}
