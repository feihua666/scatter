package scatter.common.rest.tools;

import java.util.HashMap;
import java.util.Map;

/**
 * 异常日志收集工具
 * 仅用户在请求异常时文件输出一些关键信息
 * Created by yangwei
 * Created at 2021/1/29 14:36
 */
public class ErrorLogCollectorTool {

    /**
     * 收集 以map key value存储
     * @param key key值
     * @param value value值
     */
    public static void collect(String key, Object value) {
        Object o = ThreadContextTool.get(ErrorLogCollectorTool.class.getName());
        if (o == null) {
            o = new HashMap<>();
        }
        ThreadContextTool.put(ErrorLogCollectorTool.class.getName(),o);
        ((Map) ThreadContextTool.get(ErrorLogCollectorTool.class.getName())).put(key, value);
    }

    /**
     * 获取所有已收集的数据
     * @return
     */
    public static Map getAll() {
        Object o = ThreadContextTool.get(ErrorLogCollectorTool.class.getName());
        return ((Map) o);
    }
    /**
     * 获取所有已收集的数据
     * @return
     */
    public static Object get(String key) {
        Object o = ThreadContextTool.get(ErrorLogCollectorTool.class.getName());
        if (o == null) {
            return null;
        }
        return ((Map) o).get(key);
    }
}
