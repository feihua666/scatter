package scatter.common.rest.mapstruct;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2020/10/27 14:43
 */
public interface IBaseVoMapStruct<Po,Vo> {
    /**
     * 单表po转vo
     * @param po
     * @return
     */
    Vo poToVo(Po po);

    default List<Vo> posToVos(List<Po> pos) {
        return pos.stream().map(po->(poToVo(po))).collect(Collectors.toList());
    }

    default IPage<Vo> pagePoToVo(IPage<Po> page){
        IPage pageR = page;
        List<Po> records = pageR.getRecords();
        if (pageR != null && !CollectionUtil.isEmpty(records)) {
            pageR.setRecords(posToVos(records));
            return pageR;
        }
        // 原样返回page
        return pageR;
    }
}
