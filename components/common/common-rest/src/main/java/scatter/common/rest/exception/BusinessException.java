package scatter.common.rest.exception;

import lombok.Getter;
import lombok.Setter;
import scatter.common.ErrorCode;

/**
 * 自定义业务异常，如果是业务业务异常请用此异常
 * Created by yangwei
 * Created at 2020/10/27 12:38
 */
@Getter
@Setter
public class BusinessException extends RuntimeException{
    /**
     * 异常负载数据
     */
    private Object payload;
    /**
     * 是否为请求http异常，如果是则直接响应到请求
     */
    private boolean http = false;
    /**
     * 错误代码
     * 如果需要特别编码，请先在{@link ErrorCode} 中定义再使用
     */
    private Integer code;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }


    public BusinessException(String message,boolean http) {
        this(message);
        this.http = http;
    }

    public BusinessException(String message,boolean http,Integer code) {
        this(message,http);
        this.code = code;
    }

    public BusinessException(String message,boolean http,Object payload) {
        this(message,http);
        this.payload = payload;
    }
    public BusinessException(String message,boolean http,Integer code,Object payload) {
        this(message,http,payload);
        this.code = code;
    }
    public BusinessException(String message, Throwable cause,boolean http) {
        this(message, cause);
        this.http = http;
    }
    public BusinessException(String message, Throwable cause,boolean http,Object payload) {
        this(message, cause,http);
        this.payload = payload;
    }

    public BusinessException(String message,Integer code) {
        this(message);
        this.code = code;
    }
    public BusinessException(String message,Integer code,Object payload) {
        this(message,code);
        this.payload = payload;
    }
}
