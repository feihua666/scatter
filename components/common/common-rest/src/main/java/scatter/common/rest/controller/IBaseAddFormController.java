package scatter.common.rest.controller;

import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.service.IBaseAddFormService;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:58
 */
public interface IBaseAddFormController<Po extends BasePo,Vo extends BaseVo,AddForm> {

    IBaseAddFormService<Po,AddForm> getIBaseAddFormService();
    Vo getVo(Po po);
    /**
     * 单表添加
     * @param addForm
     * @return
     */
    default Vo add(AddForm addForm){
        Po r = getIBaseAddFormService().add(addForm);
        if(r == null){
            throw new BusinessException("添加失败",true);
        }
        return getVo(r);
    }
}
