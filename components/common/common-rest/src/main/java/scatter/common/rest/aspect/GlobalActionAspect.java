package scatter.common.rest.aspect;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import scatter.common.rest.tools.RequestTool;

import javax.servlet.http.HttpServletRequest;

/**
 * interceptor 的preHandle比aspect先执行
 * Created by yangwei
 * Created at 2019/11/13 9:41
 */
@Aspect
@Component
@Slf4j
public class GlobalActionAspect {
    @Pointcut("execution(public * *..mvc.*Controller.*(..))")
    public void controllerPointcu() {}

    @Pointcut("@within(org.springframework.web.bind.annotation.RequestMapping) && @within(io.swagger.annotations.Api) && @annotation(io.swagger.annotations.ApiOperation)")
    public void restApiLogPointcut() {}

    /**
     * 在切点之前织入
     * @param joinPoint
     * @throws Throwable
     */
    @Before("restApiLogPointcut()")
    public void doBefore(JoinPoint joinPoint){
        // 开始打印请求日志
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 打印请求相关参数
        log.info("请求参数: requestParam={}", JSONUtil.toJsonStr(joinPoint.getArgs()));
        log.info("请求方法: method={}.{}",
                joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
    }
}
