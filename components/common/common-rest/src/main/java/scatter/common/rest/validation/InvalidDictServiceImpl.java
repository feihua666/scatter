package scatter.common.rest.validation;

/**
 * 提供一个占位DictService不能调用
 * Created by yangwei
 * Created at 2020/12/25 8:48
 */
public class InvalidDictServiceImpl implements DictService {
    @Override
    public String getValueById(String id) {
        throw new RuntimeException("该实现类仅用于测试方便，不用再自己写一个空的实现成不用字典的情况，你必须自己实现一个字典服务类并注入到spring中");
    }

    @Override
    public String getNameById(String id) {
        return null;
    }

    @Override
    public String getIdByGroupCodeAndValue(String groupCode, String value) {
        throw new RuntimeException("该实现类仅用于测试方便，不用再自己写一个空的实现成不用字典的情况，你必须自己实现一个字典服务类并注入到spring中");

    }
}
