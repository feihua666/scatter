package scatter.common.rest.config;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Arrays;

/**
 * Created by yangwei
 * Created at 2020/12/9 16:27
 */
public class CommonSwaggerConfig implements SwaggerConfig {

    @Value("${spring.application.name:未指定应用名称}")
    private String applicationName;

    @Bean
    @ConditionalOnMissingBean(Docket.class)
    public Docket commonDefaultRestApi() {
        return docket()
                .groupName(applicationName)
                .apiInfo(apiInfo(applicationName))
                .select()
                //选择controller包
                .apis(withClassAnnotation(Api.class))
                // 内部调用接口不监控
                .paths(path("inner").negate())
                .build()
                ;
    }
}
