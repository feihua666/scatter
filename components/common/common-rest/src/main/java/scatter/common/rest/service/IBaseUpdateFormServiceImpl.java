package scatter.common.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.dataconstraint.DataConstraintService;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/5 10:44
 */
public class IBaseUpdateFormServiceImpl<Mapper extends IBaseMapper<Po>,Po extends BasePo,UpdateForm> extends IBaseServiceImpl<Mapper,Po> implements IBaseUpdateFormService<Po,UpdateForm> {

    @Autowired(required = false)
    protected List<IUpdateServiceListener<Po,UpdateForm>> updateServiceListeners;

    @Autowired
    protected IBaseUpdateFormMapStruct<Po,UpdateForm> iBaseUpdateFormMapStruct;

    @Override
    public IBaseUpdateFormMapStruct<Po, UpdateForm> getUpdateFormMapStruct() {
        return iBaseUpdateFormMapStruct;
    }

    @Override
    public List<IUpdateServiceListener<Po, UpdateForm>> getUpdateServiceListeners() {
        return updateServiceListeners;
    }

    @Override
    public DataConstraintService getUpdateDataConstraintService() {
        return dataConstraintService;
    }

}
