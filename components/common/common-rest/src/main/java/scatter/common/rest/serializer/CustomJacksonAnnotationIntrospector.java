package scatter.common.rest.serializer;

import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

/**
 * @Description
 * @Date 2020/10/23 18:10
 * @Created by ciaj.
 */
public class CustomJacksonAnnotationIntrospector extends JacksonAnnotationIntrospector {
    @Override
    public Object findSerializer(Annotated annotated) {
        //只对方法有用
        if (annotated instanceof AnnotatedMethod) {
            DecimalFormat formatter = annotated.getAnnotated().getAnnotation(DecimalFormat.class);
            if (formatter != null) {
                return new Decimal2Serializer(formatter.scale(), formatter.roundingMode());
            }
        }
        return super.findSerializer(annotated);
    }
}
