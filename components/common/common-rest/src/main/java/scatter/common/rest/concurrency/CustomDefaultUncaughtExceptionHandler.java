package scatter.common.rest.concurrency;

import cn.hutool.core.exceptions.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import scatter.common.rest.notify.NotifyParam;
import scatter.common.rest.notify.NotifyTool;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-21 15:50
 */
@Slf4j
public class CustomDefaultUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
	@Override
	public void uncaughtException(Thread t, Throwable e) {

		log.error("异步线程执行异常，theadName={}",t.getName(),e);

		NotifyParam notifyParam = NotifyParam.system();
		notifyParam.setContentType("thread.pool.uncaught.exception");
		notifyParam.setTitle("线程池线程异常");
		notifyParam.setSuggest("您可能需要自行捕获该异常，来完善业务逻辑");
		notifyParam.setContent(ExceptionUtil.stacktraceToString(e));
		NotifyTool.notify(notifyParam);
	}
}
