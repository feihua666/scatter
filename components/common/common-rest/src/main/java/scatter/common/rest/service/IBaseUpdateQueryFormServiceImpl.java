package scatter.common.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.dataconstraint.DataConstraintService;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:00
 */
public class IBaseUpdateQueryFormServiceImpl<Mapper extends IBaseMapper<Po>,Po extends BasePo,UpdateForm,QueryForm> extends IBaseServiceImpl<Mapper,Po> implements IBaseUpdateFormService<Po,UpdateForm>,IBaseQueryFormService<Po,QueryForm> {

    @Autowired(required = false)
    protected List<IUpdateServiceListener<Po,UpdateForm>> updateServiceListeners;

    @Autowired
    protected IBaseUpdateFormMapStruct<Po,UpdateForm> iBaseUpdateFormMapStruct;
    @Autowired
    protected IBaseQueryFormMapStruct<Po,QueryForm> iBaseQueryFormMapStruct;

    @Override
    public IBaseUpdateFormMapStruct<Po, UpdateForm> getUpdateFormMapStruct() {
        return iBaseUpdateFormMapStruct;
    }

    @Override
    public List<IUpdateServiceListener<Po, UpdateForm>> getUpdateServiceListeners() {
        return updateServiceListeners;
    }

    @Override
    public DataConstraintService getUpdateDataConstraintService() {
        return dataConstraintService;
    }

    @Override
    public IBaseQueryFormMapStruct<Po, QueryForm> getQueryFormMapStruct() {
        return iBaseQueryFormMapStruct;
    }

    @Override
    public DataConstraintService getQueryDataConstraintService() {
        return dataConstraintService;
    }

}
