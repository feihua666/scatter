package scatter.common.rest.exception;

/**
 * <p>
 * 请求 http 异常
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 22:24
 */
public class BusinessBadRequestException extends BusinessException{
	public BusinessBadRequestException(String message) {
		super(message,true);
	}

	public BusinessBadRequestException(String message, Throwable cause) {
		super(message, cause,true);
	}

	public BusinessBadRequestException(String message, Integer code) {
		super(message, code,true);
	}

	public BusinessBadRequestException(String message, Integer code, Object payload) {
		super(message, code, payload);
		setHttp(true);
	}
}
