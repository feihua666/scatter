package scatter.common.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.rest.dataconstraint.DataConstraintService;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:00
 */
public class IBaseAddQueryFormServiceImpl<Mapper extends IBaseMapper<Po>,Po extends BasePo,AddForm,QueryForm> extends IBaseServiceImpl<Mapper,Po> implements IBaseAddFormService<Po,AddForm>,IBaseQueryFormService<Po,QueryForm> {

    @Autowired(required = false)
    private List<IAddServiceListener<Po,AddForm>> addServiceListeners;
    @Autowired
    protected IBaseAddFormMapStruct<Po,AddForm> iBaseAddFormMapStruct;
    @Autowired
    protected IBaseQueryFormMapStruct<Po,QueryForm> iBaseQueryFormMapStruct;
    @Override
    public IBaseAddFormMapStruct<Po, AddForm> getAddFormMapStruct() {
        return iBaseAddFormMapStruct;
    }

    @Override
    public List<IAddServiceListener<Po, AddForm>> getAddServiceListeners() {
        return addServiceListeners;
    }


    @Override
    public IBaseQueryFormMapStruct<Po, QueryForm> getQueryFormMapStruct() {
        return iBaseQueryFormMapStruct;
    }

    @Override
    public DataConstraintService getQueryDataConstraintService() {
        return dataConstraintService;
    }

}
