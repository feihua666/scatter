package scatter.common.rest.validation.form;

/**
 * 需要验证的表单需实现该接口
 * 配合grape.common.rest.validation.form.Form使用
 * Created by yangwei
 * Created at 2019/11/28 9:02
 */
public interface IFormValid {

    public boolean valid(ValidResult result, ValidContext context);
}
