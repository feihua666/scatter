package scatter.common.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BasePo;
import scatter.common.pojo.vo.BaseVo;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.service.IBaseQueryFormService;

/**
 * Created by yangwei
 * Created at 2021/1/5 13:48
 */
public class BaseQueryFormController<Po extends BasePo,Vo extends BaseVo,QueryForm> extends BaseController<Po,Vo> implements IBaseQueryFormController<Po,Vo,QueryForm> {
    @Autowired
    private IBaseQueryFormService<Po,QueryForm> iBaseQueryFormService;

    @Autowired
    private IBaseQueryFormMapStruct<Po,QueryForm> queryFormMapStruct;
    
    @Override
    public IBaseQueryFormService<Po, QueryForm> getIBaseQueryFormService() {
        return iBaseQueryFormService;
    }
}
