DROP TABLE IF EXISTS common_test;
CREATE TABLE `common_test` (
  `id` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '主键id',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  `use_bool` int(1) DEFAULT NULL COMMENT '布尔值',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='common-rest 测试表';