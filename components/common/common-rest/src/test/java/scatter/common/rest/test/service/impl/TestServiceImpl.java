package scatter.common.rest.test.service.impl;

import org.springframework.stereotype.Service;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.common.rest.test.form.TestAddForm;
import scatter.common.rest.test.form.TestPageQueryForm;
import scatter.common.rest.test.form.TestUpdateForm;
import scatter.common.rest.test.mapper.TestMapper;
import scatter.common.rest.test.po.TestPo;
import scatter.common.rest.test.service.ITestService;

/**
 * Created by yangwei
 * Created at 2020/11/5 19:33
 */
@Service
public class TestServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<TestMapper, TestPo, TestAddForm, TestUpdateForm, TestPageQueryForm> implements ITestService {
}
