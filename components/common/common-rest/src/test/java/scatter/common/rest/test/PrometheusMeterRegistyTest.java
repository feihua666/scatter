package scatter.common.rest.test;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.prometheus.HistogramFlavor;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.export.prometheus.PrometheusProperties;
import org.springframework.boot.actuate.autoconfigure.metrics.export.properties.PropertiesConfigAdapter;

import java.time.Duration;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-24 13:40
 */
public class PrometheusMeterRegistyTest {


	public static void main(String[] args) {
		PrometheusConfig config = new PrometheusPropertiesConfigAdapter(new PrometheusProperties());

		MeterRegistry prometheusMeterRegistry = new PrometheusMeterRegistry(config);
		Integer xxx = prometheusMeterRegistry.gauge("xxx", 2);
		System.out.println(xxx);

	}





	static class PrometheusPropertiesConfigAdapter extends PropertiesConfigAdapter<PrometheusProperties>
			implements PrometheusConfig {

		PrometheusPropertiesConfigAdapter(PrometheusProperties properties) {
			super(properties);
		}

		@Override
		public String prefix() {
			return "management.metrics.export.prometheus";
		}

		@Override
		public String get(String key) {
			return null;
		}

		@Override
		public boolean descriptions() {
			return get(PrometheusProperties::isDescriptions, PrometheusConfig.super::descriptions);
		}

		@Override
		public HistogramFlavor histogramFlavor() {
			return get(PrometheusProperties::getHistogramFlavor, PrometheusConfig.super::histogramFlavor);
		}

		@Override
		public Duration step() {
			return get(PrometheusProperties::getStep, PrometheusConfig.super::step);
		}

	}
}
