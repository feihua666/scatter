package scatter.common.rest.test.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import scatter.common.pojo.po.BasePo;

/**
 * Created by yangwei
 * Created at 2020/10/27 16:59
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("common_test")
public class TestPo extends BasePo {

    private String name;

    private Boolean useBool;

}
