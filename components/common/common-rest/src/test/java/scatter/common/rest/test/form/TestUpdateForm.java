package scatter.common.rest.test.form;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseUpdateForm;
import scatter.common.pojo.form.BaseUpdateIdForm;
import scatter.common.pojo.form.SetNullWhenNull;

/**
 * Created by yangwei
 * Created at 2020/11/5 19:26
 */
@Setter
@Getter
public class TestUpdateForm extends BaseUpdateIdForm {


    private String name;

    @SetNullWhenNull
    private Boolean useBool;
}
