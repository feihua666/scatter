package scatter.common.rest.test;

import scatter.common.rest.test.po.TestPo;
import scatter.common.rest.tools.PojoPropertyDiffTool;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-14 14:11
 */
public class PropertyDiffTest {
	public static void main(String[] args) {
		TestPo newPo = new TestPo();
		newPo.setName("new");
		newPo.setUseBool(true);

		TestPo oldPo = new TestPo();
		oldPo.setName("old");
		oldPo.setUseBool(false);

		List<PojoPropertyDiffTool.PropertyDiffResult> diff = PojoPropertyDiffTool.diff(oldPo, newPo);

		System.out.println(diff);


		System.out.println(PojoPropertyDiffTool.diffString(oldPo,newPo));
	}
}
