package scatter.common.rest.test;

import com.google.common.util.concurrent.RateLimiter;
import lombok.SneakyThrows;

import java.util.concurrent.CyclicBarrier;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-03 14:28
 */
public class RateLimitTest {

	public static void main(String[] args) {

		int threadNum = 10;
		int permit = 8;


		RateLimiter rateLimiter = RateLimiter.create(permit);

		CyclicBarrier cyclicBarrier = new CyclicBarrier(threadNum);
		for (int i = 0; i < threadNum; i++) {
			Thread thread = new Thread(new Runnable() {
				@SneakyThrows
				@Override
				public void run() {
					cyclicBarrier.await();
					Thread.sleep(100);
					if (rateLimiter.tryAcquire()) {
						System.out.println("通过");
					}else {
						System.out.println("未通过");

					}
				}
			});
			thread.start();
		}
	}
}
