package scatter.common.rest.test.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.common.rest.test.form.TestAddForm;
import scatter.common.rest.test.form.TestPageQueryForm;
import scatter.common.rest.test.form.TestUpdateForm;
import scatter.common.rest.test.po.TestPo;
import scatter.common.rest.test.vo.TestVo;

/**
 * Created by yangwei
 * Created at 2020/11/5 19:28
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ITestMapStruct extends
        IBaseVoMapStruct<TestPo, TestVo>,
        IBaseAddFormMapStruct<TestPo,TestAddForm>,
        IBaseUpdateFormMapStruct<TestPo,TestUpdateForm>,
        IBaseQueryFormMapStruct<TestPo,TestPageQueryForm> {
    ITestMapStruct INSTANCE = Mappers.getMapper( ITestMapStruct.class );
}
