package scatter.common.rest.test.form;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

/**
 * Created by yangwei
 * Created at 2020/11/5 19:26
 */
@Setter
@Getter
public class TestAddForm extends BaseAddForm {
    private String name;

    private Boolean useBool;
}
