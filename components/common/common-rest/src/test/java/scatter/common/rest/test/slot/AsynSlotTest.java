package scatter.common.rest.test.slot;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.common.rest.concurrency.AsynSlot;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-18 11:11
 */
@SpringBootTest
public class AsynSlotTest {


	@Test
	void graphTest(){


		TestAsynSlot testAsynSlot = new TestAsynSlot();

		testAsynSlot.setName("root");
		testAsynSlot.setValue("rootValue");

		testAsynSlot.addDepend("depend1",new TestAsynSlot("depend1","depend1 Value"));
		testAsynSlot.addDepend("depend2",new TestAsynSlot("depend2","depend2 Value"));
		testAsynSlot.addDepend("depend3",new TestAsynSlot("depend3","depend3 Value"));
		testAsynSlot.addDepend("depend4",new TestAsynSlot("depend4","depend4 Value"));

		testAsynSlot.getDependencies().getDepend("depend1").addDepend("depend11",new TestAsynSlot("depend11","depend11 Value"));
		testAsynSlot.getDependencies().getDepend("depend2").addDepend("depend12",new TestAsynSlot("depend12","depend12 Value"));
		testAsynSlot.getDependencies().getDepend("depend3").addDepend("depend13",new TestAsynSlot("depend13","depend13 Value"));
		testAsynSlot.getDependencies().getDepend("depend4").addDepend("depend14",new TestAsynSlot("depend14","depend14 Value"));

		System.out.println(testAsynSlot.dependDigraphString());


	}

	@NoArgsConstructor
	public static class TestAsynSlot extends AsynSlot<String>{

		@Setter
		String value;
		public TestAsynSlot(String name,String value){
			super();
			setName(name);
			this.value = value;
		}
		@Override
		protected ListenableFuture<String> evaluate() {
			return Futures.immediateFuture(value);
		}
	}
}
