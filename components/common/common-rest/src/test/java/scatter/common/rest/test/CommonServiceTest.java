package scatter.common.rest.test;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.common.rest.service.mapper.NativeSqlMapper;
import scatter.common.rest.test.form.TestAddForm;
import scatter.common.rest.test.form.TestPageQueryForm;
import scatter.common.rest.test.form.TestUpdateForm;
import scatter.common.rest.test.po.TestPo;
import scatter.common.rest.test.service.ITestService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2020/10/27 16:22
 */
@SpringBootTest
public class CommonServiceTest {

    @Autowired
    private NativeSqlMapper mapper;
    @Autowired
    private ITestService testService;

    private static MockConfig mockConfig = null;

    public static void main(String[] args) {
        System.out.println(ReUtil.get("(?<=\\()(\\S+)(?=\\))","float('10')",0));
    }

    @BeforeAll
    public static void mockConfig() {
        mockConfig = new MockConfig();
    }

    /**
     * 随机数据po
     * @return
     */
    TestPo randomPo() {
        return JMockData.mock(TestPo.class, mockConfig);
    }


    /**
     * native sql 测试
     */
    @Test
    void NativeSqlTest(){
        List<Map<String, Object>> maps = mapper.selectList("select * from role");
        System.out.println(maps);
    }
    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<TestPo> testPos = testService.list();
        Assertions.assertTrue( testPos.size() >= 0);
    }

    @Test
    void addTest() {
        TestAddForm testAddForm = new TestAddForm();

        testAddForm.setName("name");
        testAddForm.setUseBool(true);
        TestPo add = testService.add(testAddForm);
        TestPo byId = testService.getById(add.getId());
        Assertions.assertNotNull(byId);
        testService.removeById(add.getId());
    }

    @Test
    void updateTest() {

        TestPo testPo = new TestPo();
        testPo.setName("name");
        testPo.setUseBool(true);
        testService.save(testPo);

        TestUpdateForm updateForm = new TestUpdateForm();
        updateForm.setName(testPo.getName());
        updateForm.setId(testPo.getId());
        updateForm.setVersion(testPo.getVersion());
        boolean r = testService.update(updateForm);
        Assertions.assertTrue(r);

        TestPo byId = testService.getById(testPo.getId());
        Assertions.assertNull(byId.getUseBool());
        testService.removeById(testPo.getId());
    }

    @Test
    void updateVersionTest() {
        TestPo testPo = new TestPo();
        testPo.setName("name");
        testPo.setUseBool(true);
        testService.save(testPo);

        testService.updateById(testPo);
        Assertions.assertEquals(2,testPo.getVersion());

        testService.removeById(testPo.getId());

    }
    @Test
    void updateFormVersionTest() {
        TestPo testPo = new TestPo();
        testPo.setName("name");
        testPo.setUseBool(true);
        testService.save(testPo);

        TestUpdateForm testUpdateForm = new TestUpdateForm();
        testUpdateForm.setId(testPo.getId());
        testUpdateForm.setName("name1");
        testUpdateForm.setVersion(testPo.getVersion());
        testService.update(testUpdateForm);

        TestPo byId = testService.getById(testPo.getId());

        Assertions.assertEquals(2,byId.getVersion());

        testService.removeById(testPo.getId());
    }

    @Test
    void listQueryFormTest(){

        TestPo testPo = new TestPo();
        testPo.setName("name");
        testPo.setUseBool(true);
        testService.save(testPo);


        TestPageQueryForm testPageQueryForm = new TestPageQueryForm();
        testPageQueryForm.setName("am");


        List<TestPo> list = testService.list(testPageQueryForm);

        Assertions.assertTrue(list.size() > 0);

        testService.removeById(testPo.getId());
    }

    @Test
    void listPageTest(){
        List<TestPo> insertBatch = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            TestPo testPo = new TestPo();
            testPo.setName("insertBatch");
            testPo.setUseBool(true);
            insertBatch.add(testPo);
        }

        testService.saveBatch(insertBatch);

        Page<TestPo> testPoPage = new Page<>();
        TestPo testPo = new TestPo();
        testPo.setName("insertBatch");


        testPoPage.addOrder(OrderItem.asc("id"));
        Page<TestPo> page = testService.page(testPoPage, Wrappers.query(testPo));

        Assertions.assertEquals(10,page.getPages());
        Assertions.assertEquals(100,page.getTotal());

        Assertions.assertEquals(10,testPoPage.getPages());
        Assertions.assertEquals(100,testPoPage.getTotal());

        testService.removeByIds(insertBatch.stream().map(TestPo::getId).collect(Collectors.toList()));

    }
    @Test
    void listPageQueryFormTest(){

        List<TestPo> insertBatch = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            TestPo testPo = new TestPo();
            testPo.setName("insertBatch");
            testPo.setUseBool(true);
            insertBatch.add(testPo);
        }

        testService.saveBatch(insertBatch);

        Assertions.assertNotNull(insertBatch.get(0).getId());

        TestPageQueryForm testPageQueryForm = new TestPageQueryForm();
        testPageQueryForm.setName("insertBatch");
        Page<TestPo> testPoPage = testService.listPage(testPageQueryForm);

        Assertions.assertEquals(10,testPoPage.getPages());
        Assertions.assertEquals(100,testPoPage.getTotal());



        List<TestPo> list = testService.list(testPageQueryForm);

        Assertions.assertTrue(list.size() > 0);

        testService.removeByIds(insertBatch.stream().map(TestPo::getId).collect(Collectors.toList()));
    }

    @Test
    void orderByQueryFormTest(){
        TestPageQueryForm testPageQueryForm = new TestPageQueryForm();
        testPageQueryForm.setOrderBy("id-1,name,useBool-0");
        List<TestPo> list = testService.list(testPageQueryForm);

        // SELECT id,name,use_bool,create_at,create_by,update_at,update_by,version FROM test WHERE (true) ORDER BY id ASC,name ASC,use_bool DESC

    }

    @Test
    void mybatisPlusWrapperTest() {
        TestPo testPo = new TestPo();
        testPo.setName("222");
        QueryWrapper<TestPo> queryWrapper = Wrappers.query(testPo);
        queryWrapper.apply(true, "true");

        testService.convertEntityConditionToWrapper(queryWrapper);
        queryWrapper.or().lambda().eq(TestPo::getId, "1");



        testService.list(queryWrapper);

    }
}
