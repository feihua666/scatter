package scatter.common.rest.test.mapper;

import scatter.common.rest.service.IBaseMapper;
import scatter.common.rest.test.po.TestPo;

/**
 * Created by yangwei
 * Created at 2020/10/27 16:59
 */
public interface TestMapper extends IBaseMapper<TestPo> {
}
