package scatter.common.rest.test.service;

import scatter.common.rest.service.IBaseAddFormService;
import scatter.common.rest.service.IBaseQueryFormService;
import scatter.common.rest.service.IBaseService;
import scatter.common.rest.service.IBaseUpdateFormService;
import scatter.common.rest.test.form.TestAddForm;
import scatter.common.rest.test.form.TestPageQueryForm;
import scatter.common.rest.test.form.TestUpdateForm;
import scatter.common.rest.test.po.TestPo;

/**
 * Created by yangwei
 * Created at 2020/10/27 17:27
 */
public interface ITestService extends IBaseService<TestPo>, IBaseAddFormService<TestPo,TestAddForm>, IBaseUpdateFormService<TestPo,TestUpdateForm>, IBaseQueryFormService<TestPo,TestPageQueryForm> {
}
