package scatter.common.rest.test.form;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;

/**
 * Created by yangwei
 * Created at 2020/11/5 19:26
 */
@Setter
@Getter
public class TestPageQueryForm extends BasePageQueryForm {

    @Like
    private String name;

    @OrderBy
    private Boolean useBool;
}
