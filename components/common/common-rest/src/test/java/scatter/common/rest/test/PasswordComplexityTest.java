package scatter.common.rest.test;

import scatter.common.rest.tools.PasswordComplexityTool;

/**
 * Created by yangwei
 * Created at 2021/3/29 17:03
 */
public class PasswordComplexityTest {
    public static void main(String[] args) {
        PasswordComplexityTool.PasswordComplexityResult complexityLevel = PasswordComplexityTool.getComplexityLevel("11111111");

        System.out.println(complexityLevel.getComplexityLevel());
        for (PasswordComplexityTool.PasswordComplexity passwordComplexity : complexityLevel.getPasswordComplexities()) {
            System.out.println(passwordComplexity.getEvaluate() + ":" + passwordComplexity.getComplexityLevel());
        }
    }
}
