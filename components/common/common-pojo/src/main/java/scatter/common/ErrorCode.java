package scatter.common;

import lombok.Getter;

/**
 * 错误代码
 * Created by yangwei
 * Created at 2021/1/19 10:19
 */
@Getter
public enum ErrorCode implements ECF{
    ERROR_SHOULD_CAPTCHA(4031,"请先通过验证码验证"),
    ERROR_NO_GENER(4032,"无性别信息"),
    ERROR_NO_IDENTIFICATION(4033,"尚未认证");
    private Integer code;
    private String msg;
    ErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
