package scatter.common.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.ErrorCode;

/**
 * 错误响应数据
 * Created by yangwei
 * Created at 2020/10/27 13:39
 */
@Getter
@Setter
@ApiModel(value = "错误响应数据")
public class ErrorVo extends BaseVo {

    /**
     * 如果需要特别编码，请先在{@link ErrorCode} 中定义再使用
     */
    @ApiModelProperty(value = "错误代码")
    private Integer errorCode;

    @ApiModelProperty(value = "错误消息")
    private String errorMsg;

    @ApiModelProperty(value = "错误数据负载")
    private Object payload;


    public static ErrorVo newErrorVo(ErrorCode errorCode) {
        ErrorVo errorVo = new ErrorVo();
        errorVo.setErrorMsg(errorCode.getMsg());
        errorVo.setErrorCode(errorCode.getCode());
        return errorVo;
    }
}
