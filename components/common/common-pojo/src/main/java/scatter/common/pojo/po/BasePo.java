package scatter.common.pojo.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import scatter.common.pojo.BasePojo;

import java.time.LocalDateTime;

/**
 * 所有数据库实体基类
 * Created by yangwei
 * Created at 2020/10/27 13:13
 */
@Accessors(chain = true)
@Getter
@Setter
public class BasePo extends BasePojo {

    public static final String PROPERTY_ID = "id";
    public static final String COLUMN_ID = "id";
    public static final String PROPERTY_CREATE_AT = "createAt";
    public static final String PROPERTY_CREATE_BY = "createBy";
    public static final String PROPERTY_UPDATE_AT = "updateAt";
    public static final String PROPERTY_UPDATE_BY = "updateBy";
    public static final String PROPERTY_VERSION = "version";
    // 初始化版本
    public static final int INIT_VERSION = 1;

    @TableId(type = IdType.ASSIGN_ID)
    private String id;


    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateAt;

    /**
     * 修改人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 乐观锁字段
     */
    @Version
    @TableField(fill = FieldFill.INSERT)
    private Integer version;
}
