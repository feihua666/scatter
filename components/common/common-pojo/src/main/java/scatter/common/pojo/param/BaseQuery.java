package scatter.common.pojo.param;

import scatter.common.pojo.BasePojo;

/**
 * 所有调用service的查询参数基类
 * Created by yangwei
 * Created at 2020/11/5 14:27
 */
public class BaseQuery extends BasePojo {
}
