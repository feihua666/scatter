package scatter.common.pojo.form;

import scatter.common.pojo.BasePojo;

/**
 * rest接口表单的基类，所有表单应继承该类
 * Created by yangwei
 * Created at 2019/7/23 13:17
 */
public class BaseForm extends BasePojo {
}
