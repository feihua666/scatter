package scatter.common.pojo;

import java.io.Serializable;

/**
 * 系统中所有具有pojo特性的基类
 * Created by yangwei
 * Created at 2020/10/27 13:05
 */
public abstract class BasePojo implements Serializable {
}
