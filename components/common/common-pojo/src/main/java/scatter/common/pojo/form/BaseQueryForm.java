package scatter.common.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * rest接口表单的查询基类，所有查询表单应继承该类
 * Created by yangwei
 * Created at 2020/10/27 13:12
 */
@Setter
@Getter
public class BaseQueryForm extends BaseForm {

    @Ignore
    @ApiModelProperty(value = "排序",example = "规则：propertyName[-1|0] 1为升序，0为降序，按id升序排序：id-1或id，多个以逗号分隔：id,name,creatAt-0")
    private String orderBy;
}
