package scatter.common.pojo.param;

import scatter.common.pojo.BasePojo;

/**
 * 所有调用service的参数基类，如果是非查询参数请使用该基类
 * Created by yangwei
 * Created at 2020/10/27 13:33
 */
public class BaseParam extends BasePojo {
}
