package scatter.common.dict;

/**
 * 一个标识字典组的接口
 * Created by yangwei
 * Created at 2019/12/18 11:23
 */
public interface IDictGroup extends IBaseDictEnum {

    /**
     * 字典组编码
     * @return
     */
    public String groupCode();
}
