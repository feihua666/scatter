package scatter.common.pojo.form;

/**
 * 空表单，由于controller和service已经写好了基础模板类供使用
 * 但可能有一些功能不需要某个添加表单等，可以用这个空表单占位
 * 注意，使用了占位表单请把controller对应的功能删除，service中的对应方法也不要再使用，以免出现问题
 * Created by yangwei
 * Created at 2021/1/4 10:25
 */
public class EmptyForm extends BaseForm {
}
