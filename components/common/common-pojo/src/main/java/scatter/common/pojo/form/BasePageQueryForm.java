package scatter.common.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 带有分页的查询表单
 * Created by yangwei
 * Created at 2019/7/27 16:25
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value = "分页查询参数")
public class BasePageQueryForm extends BaseQueryForm {
    /**
     * 请求页码
     */
    @ApiModelProperty(value = "请求页码，从1开始，不传默认为1")
    private Long current = 1L;
    /**
     * 每页条数
     */
    @ApiModelProperty(value = "请求每页条数，不传默认为10")
    private Long size = 10L;

}
