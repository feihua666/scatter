package scatter.common.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created by yangwei
 * Created at 2020/11/4 13:04
 */
@Setter
@Getter
@Accessors(chain = true)
public class BaseIdVo extends BaseVo{

    @ApiModelProperty(value = "id",notes = "主键")
    private String id;

    @ApiModelProperty(value = "数据版本")
    private Integer version;
}
