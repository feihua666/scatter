package scatter.common.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by yangwei
 * Created at 2020/11/4 10:57
 */
@Getter
@Setter
@ApiModel(value = "更新参数")
public class BaseUpdateIdForm extends BaseUpdateForm {


    @Ignore
    @ApiModelProperty(value = "id主键",required = true)
    @NotNull(message = "id主键不能为空")
    private String id;


    @ApiModelProperty(value = "版本",notes = "根据信息数据原样返回",required = true)
    @NotNull(message = "版本不能为空")
    private Integer version;
}
