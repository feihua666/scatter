package scatter.common;

/**
 * <p>
 * 全称 ErrorCodeFlag = ECF
 * 用来标识定义的错误信息
 * </p>
 *
 * @author yangwei
 * @since 2021-11-06 14:23
 */
public interface ECF {
}
