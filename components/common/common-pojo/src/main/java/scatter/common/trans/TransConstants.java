package scatter.common.trans;

/**
 * <p>
 * 由来： 在写多个组件的时候，发现在翻译有相互依赖关系，比如组件可能依赖字典功能，用户组件等翻译的场景，
 * 这样就不得不通过maven依赖，来引用到对应的翻译支持类型
 * 这里主要是把一个常用的翻译类型提取出来，以方便翻译使用如：字典、用户昵称、头像等
 * </p>
 *
 * @author yangwei
 * @since 2021-12-03 11:28
 */
public class TransConstants {
	/**
	 * 根据 id 翻译字典实体
	 * 由字典组件（dict）提供支持
	 */
	public static final String TRANS_DICT_BY_ID = "trans_dict_by_id";

	/**
	 * 根据 id 翻译用户实体 {@link UserForTrans}
	 * 由用户组件（usersimple、user、userunion）提供支持
	 * 定义该翻译结果应该是一个 {@link UserForTrans}
	 * @Trans 注解中支持映射实体字段，所不必对属性进行拆分翻译
	 */
	public static final String TRANS_USER_BY_ID = "trans_user_by_id";

}
