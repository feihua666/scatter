package scatter.common.trans;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.BasePojo;

/**
 * <p>
 * 用户翻译结果
 * </p>
 *
 * @author yangwei
 * @since 2021-12-03 11:36
 */
@Setter
@Getter
public class UserForTrans extends BasePojo {
	/**
	 * 用户的id
	 */
	private String id;
	/**
	 * 用户的头像
	 */
	private String avatar;
	/**
	 * 用户的真实姓名
	 */
	private String name;
	/**
	 * 用户的昵称
	 */
	private String nickname;
	/**
	 * 性别名称
	 */
	private String genderName;
	/**
	 * 性别标识码
	 */
	private String genderValue;
}
