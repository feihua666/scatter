package scatter.common.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import scatter.common.pojo.BasePojo;
import scatter.common.pojo.po.BasePo;

/**
 * 一个关系实体，主要用于关系分配
 * Created by yangwei
 * Created at 2021/2/7 16:12
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RelDto extends BasePojo {

    private String mainId;
    private String otherId;
}
