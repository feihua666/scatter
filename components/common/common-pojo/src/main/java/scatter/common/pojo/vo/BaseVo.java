package scatter.common.pojo.vo;

import scatter.common.pojo.BasePojo;

/**
 * 所有返回数据的json数据包装基类
 * Created by yangwei
 * Created at 2019/7/24 18:25
 */
public class BaseVo extends BasePojo {

}
