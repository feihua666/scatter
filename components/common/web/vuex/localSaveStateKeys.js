let localSaveStateKeys = []

export default localSaveStateKeys
/**
 * 添加
 * @param key
 */
export function add(key){
    localSaveStateKeys.push(key)
}
/**
 * 添加 array
 * @param keys
 */
export function addArray(keys){
    keys.forEach(key => {
        localSaveStateKeys.push(key)
    })
}
/**
 * 删除
 * @param key
 */
export function remove(key){
    let index = this.indexOf(key);
    if (index > -1) {
        localSaveStateKeys.splice(index, 1);
    }
}