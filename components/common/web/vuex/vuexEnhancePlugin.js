// 参见 https://www.uviewui.com/guide/globalVariable.html
// 使用 this.$st.vuex.('name','newName')
import localData from './localData.js'
// 需要永久存储，且下次APP启动需要取出的，在state中的变量名,示例 ['vuex_user', 'vuex_token']
import localSaveStateKeys,{addArray} from './localSaveStateKeys.js'
/**
 * vuex 增量mixin
 */
import mixin from "./mixin";
/**
 * 深度获取对象值
 */
import {getValue} from '../tools/ObjectTools.js'

/**
 * 本地存储 get
 * @param key
 * @returns {any}
 */
const storageGet = function (key){
    if(typeof uni !== 'undefined'){
        return uni.getStorageSync(key)
    }else {
       let {get} =  require('../tools/StorageTools.js')
        return get(key)
    }

}
/**
 * 本地存储 set
 * @param key
 * @param obj
 */
const storageSet = function (key,obj){
    if(typeof uni !== 'undefined'){
        uni.setStorageSync(key,obj)
    }else {
        let {set} =  require('../tools/StorageTools.js')
        return set(key,obj)
    }
}

try{
    // 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
    localData.lifeData = storageGet('lifeData');
}catch(e){

}

// 保存变量到本地存储中
const saveLifeData = function(key, value){
    // 判断变量名是否在需要存储的数组中
    if(localSaveStateKeys.indexOf(key) != -1) {
        // 获取本地存储的lifeData对象，将变量添加到对象中
        let tmp = storageGet('lifeData');
        // 第一次打开APP，不存在lifeData变量，故放一个{}空对象
        tmp = tmp ? tmp : {};
        tmp[key] = value;
        // 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
        storageSet('lifeData', tmp);
    }
}
const mutationsStore = function (state, payload) {
    // 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
    let nameArr = payload.name.split('.');
    let saveKey = '';
    let len = nameArr.length;
    if(nameArr.length >= 2) {
        let obj = state[nameArr[0]];
        for(let i = 1; i < len - 1; i ++) {
            obj = obj[nameArr[i]];
        }
        obj[nameArr[len - 1]] = payload.value;
        saveKey = nameArr[0];
    } else {
        // 单层级变量，在state就是一个普通变量的情况
        state[payload.name] = payload.value;
        saveKey = payload.name;
    }
    // 保存变量到本地，见顶部函数定义
    saveLifeData(saveKey, state[saveKey])
}
// mutation的名称
const mutationName = '$stStore'

/**
 * 默认以 vue 插件的方式导出
 * @type {{install: vuePlugin.install}}
 */
const vuePlugin = {
    /**
     *
     * @param Vue
     * @param options {store,localSaveStateKeys,mutationName,mutations}
     */
    install: function (Vue, options) {
        addArray(options.localSaveStateKeys)
        let name = options.mutationName || mutationName
        // mixin 来全局映射 state
        Vue.mixin(mixin(options.store,name))

        // 自动初始化需要持久化的属性
        if(localData.lifeData){
            localSaveStateKeys.forEach(item => {
                let value = getValue(localData.lifeData,item)
                if (value == null || value == undefined || value == NaN) {
                    return false
                }
                options.store.commit(name, {
                    name: item,
                    value: value
                })
            })
        }
    }
}

export default vuePlugin
/**
 * 导出增强方法，自定义方式，建议直接使用插件方式
 * @param state
 * @param payload
 * @param saveStateKeys
 */
export  {mutationsStore,mixin,mutationName}