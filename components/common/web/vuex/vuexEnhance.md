# vuex 增强 参考 https://www.uviewui.com/guide/globalVariable.html

## 步骤
1. 在 vuex mutations 中添加 vuexEnhancePlugin.js 导出的 mutationsStore 方法  
2. main.js 中添加  vuexEnhancePlugin.js 导出的默认插件，并必填添加选项 store ，持久化数据key等  
3. vuex 中 state 中引入 localData.js 其它字段lifeData为持久化数据  
4. 在vue任意页面直接可以动态响应state中的值，无须再...mapState变量  
5. 方法this.$st.vuex('userInfo.nickname','newNickname') 来设置变更state值  