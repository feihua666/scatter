import StBaiduMap from './StBaiduMap.vue'
import StIframe from './StIframe.vue'
import StTinymceEditor from './tinymceEditor/StTinymceEditor.vue'
import StSlideVerify from './slideVerify/StSlideVerify.vue'

export default {
    install: function (Vue, options) {
        Vue.component(StBaiduMap.name,StBaiduMap)
        Vue.component(StIframe.name,StIframe)
        Vue.component(StTinymceEditor.name,StTinymceEditor)
        Vue.component(StSlideVerify.name,StSlideVerify)
    }
}