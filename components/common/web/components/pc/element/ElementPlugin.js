import StElSelect from './StElSelect.vue'
import StElButtonGroup from './StElButtonGroup.vue'
import StElCheckBoxGroup from './StElCheckBoxGroup.vue'
import StElRadioGroup from './StElRadioGroup.vue'
import StElCascader from './StElCascader.vue'
import StElDynamicForm from './StElDynamicForm.vue'
import StElUpload from './StElUpload.vue'
import StElUploadAvatar from './StElUploadAvatar.vue'
import StElUploadSingleImage from './StElUploadSingleImage.vue'
import StElUploadFileRaw from './StElUploadFileRaw.vue'
import StElUploadInput from './StElUploadInput.vue'
import StElUploadFile from './StElUploadFile.vue'
import StElDynamicFormItem from './StElDynamicFormItem.vue'
import StElPagination from './StElPagination.vue'
import StElTableColumn from './StElTableColumn.vue'
import StElTable from './StElTable.vue'
import StElTree from './StElTree.vue'
import StElMenu from './StElMenu.vue'
import StElButton from './StElButton.vue'
import StElSelectDict from './StElSelectDict.vue'
import StElLogin from './StElLogin.vue'
import StElUpdatePassword from './StElUpdatePassword.vue'
import StElBackedIndex from './StElBackedIndex.vue'
import * as StyleTools from './StyleTools.js'
import StElBreadcrumb from './StElBreadcrumb.vue'
import StElRouterView from './StElRouterView.vue'
import StElRouterViewDrawer from './StElRouterViewDrawer.vue'
// 这里引用了工具模块作为全局组件
import StElCron from '../../../../../tools/web/pc/element/cron/StElCron.vue'
import StElCronInput from '../../../../../tools/web/pc/element/cron/StElCronInput.vue'

export default {
    install: function (Vue, options) {
        // 添加实例方法
        Vue.prototype.$stElStyleTools = StyleTools
        Vue.component(StElSelect.name,StElSelect)
        Vue.component(StElButtonGroup.name,StElButtonGroup)
        Vue.component(StElCheckBoxGroup.name,StElCheckBoxGroup)
        Vue.component(StElRadioGroup.name,StElRadioGroup)
        Vue.component(StElCascader.name,StElCascader)
        Vue.component(StElDynamicForm.name,StElDynamicForm)
        Vue.component(StElUpload.name,StElUpload)
        Vue.component(StElUploadAvatar.name,StElUploadAvatar)
        Vue.component(StElUploadSingleImage.name,StElUploadSingleImage)
        Vue.component(StElUploadFileRaw.name,StElUploadFileRaw)
        Vue.component(StElUploadInput.name,StElUploadInput)
        Vue.component(StElUploadFile.name,StElUploadFile)
        Vue.component(StElDynamicFormItem.name,StElDynamicFormItem)
        Vue.component(StElPagination.name,StElPagination)
        Vue.component(StElTableColumn.name,StElTableColumn)
        Vue.component(StElTable.name,StElTable)
        Vue.component(StElTree.name,StElTree)
        Vue.component(StElMenu.name,StElMenu)
        Vue.component(StElButton.name,StElButton)
        Vue.component(StElSelectDict.name,StElSelectDict)
        Vue.component(StElLogin.name,StElLogin)
        Vue.component(StElUpdatePassword.name,StElUpdatePassword)
        Vue.component(StElBackedIndex.name,StElBackedIndex)
        Vue.component(StElBreadcrumb.name,StElBreadcrumb)
        Vue.component(StElCron.name,StElCron)
        Vue.component(StElCronInput.name,StElCronInput)
        Vue.component(StElRouterView.name,StElRouterView)
        Vue.component(StElRouterViewDrawer.name,StElRouterViewDrawer)


    }
}