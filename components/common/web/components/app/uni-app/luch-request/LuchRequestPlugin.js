// 该工具封装来自以下链接 <a href="https://ext.dcloud.net.cn/plugin?id=392">luch-request</a>
import Request from './index.js'
export default {
    install: function (Vue, options) {
        let re = new Request(options || {})
        // 添加实例方法
        Vue.prototype.$http = re
    }
}