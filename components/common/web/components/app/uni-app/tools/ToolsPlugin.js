import * as ClipboardTools from './ClipboardTools.js'

export default {
    install: function (Vue, options) {
        // 添加实例方法,全部以 stU 前缀开关，表示是 uni-app 方法工具
        Vue.prototype.$stUClipboardTools = ClipboardTools

    }
}