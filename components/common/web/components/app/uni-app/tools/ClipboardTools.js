/**
 * 复制
 * @param str
 */
export function copy(str) {
    uni.setClipboardData({
        data: str,
        success(res) {
            uni.showToast({
                icon: 'none',
                title: '复制成功'
            });
        }
    })
}