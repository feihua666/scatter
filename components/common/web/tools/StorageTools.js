export function set(key, value) {
    localStorage.setItem(key, JSON.stringify(value))
}

export function get(key) {
    return JSON.parse(localStorage.getItem(key))
}

export function remove(key) {
    localStorage.removeItem(key)
}

export function clear() {
    localStorage.clear()
}

export function sessionSet(key, value) {
    sessionStorage.setItem(key, JSON.stringify(value))
}

export function sessionGet(key) {
    return JSON.parse(sessionStorage.getItem(key))
}

export function sessionRemove(key) {
    sessionStorage.removeItem(key)
}

export function sessionClear() {
    sessionStorage.clear()
}

export function sessionClearKeys(array) {
    if (array) {
        array.forEach(itemKey => {
            sessionStorage.removeItem(itemKey)
        })
    }
}
