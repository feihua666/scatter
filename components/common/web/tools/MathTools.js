/**
 * 取两个数的最小值
 * @param x
 * @param y
 * @return {*}
 */
export function min(x,y) {
    return Math.min(x,y)
}
/**
 * 取两个数的最大值
 * @param x
 * @param y
 * @return {*}
 */
export function max(x,y) {
    return Math.max(x,y)
}

/**
 * 向下取整
 * @param x
 * @return {number}
 */
export function floor(x) {
    return Math.floor(x)
}