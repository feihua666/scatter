import * as ArrayTools from './ArrayTools.js'
import * as CommonTools from './CommonTools.js'
import * as DynamicFormTools from './DynamicFormTools.js'
import * as FileTools from './FileTools.js'
import * as FunctionTools from './FunctionTools.js'
import * as MathTools from './MathTools.js'
import * as ObjectTools from './ObjectTools.js'
import * as RegTools from './RegTools.js'
import * as StorageTools from './StorageTools.js'
import * as StringTools from './StringTools.js'
import * as StyleTools from './StyleTools.js'
import * as TimerExcTools from './TimerExcTools.js'
import * as UrlTools from './UrlTools.js'
import * as DateTools from './DateTools.js'

export default {
    install: function (Vue, options) {
        // 添加实例方法
        Vue.prototype.$stArrayTools = ArrayTools
        Vue.prototype.$stCommonTools = CommonTools
        Vue.prototype.$stDynamicFormTools = DynamicFormTools
        Vue.prototype.$stFileTools = FileTools
        Vue.prototype.$stFunctionTools = FunctionTools
        Vue.prototype.$stMathTools = MathTools
        Vue.prototype.$stObjectTools = ObjectTools
        Vue.prototype.$stRegTools = RegTools
        Vue.prototype.$stStorageTools = StorageTools
        Vue.prototype.$stStringTools = StringTools
        Vue.prototype.$stStyleTools = StyleTools
        Vue.prototype.$stTimerExcTools = TimerExcTools
        Vue.prototype.$stUrlTools = UrlTools
        Vue.prototype.$stDateTools = DateTools

    }
}