package scatter.usersimple.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.pojo.form.UserSimpleAddForm;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm;
import scatter.usersimple.pojo.form.UserSimplePageQueryForm;
import scatter.usersimple.rest.test.UserSimpleSuperTest;
import scatter.usersimple.rest.service.IUserSimpleService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 简单用户 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserSimpleServiceTest extends UserSimpleSuperTest{

    @Autowired
    private IUserSimpleService userSimpleService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserSimple> pos = userSimpleService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}