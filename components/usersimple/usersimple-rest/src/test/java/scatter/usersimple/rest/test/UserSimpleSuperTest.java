package scatter.usersimple.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.pojo.form.UserSimpleAddForm;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm;
import scatter.usersimple.pojo.form.UserSimplePageQueryForm;
import scatter.usersimple.rest.service.IUserSimpleService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 简单用户 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserSimpleSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserSimpleService userSimpleService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userSimpleService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userSimpleService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserSimple mockPo() {
        return JMockData.mock(UserSimple.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserSimpleAddForm mockAddForm() {
        return JMockData.mock(UserSimpleAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserSimpleUpdateForm mockUpdateForm() {
        return JMockData.mock(UserSimpleUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserSimplePageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserSimplePageQueryForm.class, mockConfig);
    }
}