package scatter.usersimple.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.usersimple.pojo.form.UserSimpleAddForm;
import scatter.usersimple.pojo.form.UserSimplePageQueryForm;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.rest.mapper.UserSimpleMapper;
import scatter.usersimple.rest.service.IUserSimpleService;
/**
 * <p>
 * 简单用户表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserSimpleServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserSimpleMapper, UserSimple, UserSimpleAddForm, UserSimpleUpdateForm, UserSimplePageQueryForm> implements IUserSimpleService {
    @Override
    public void preAdd(UserSimpleAddForm addForm,UserSimple po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserSimpleUpdateForm updateForm,UserSimple po) {
        super.preUpdate(updateForm,po);

    }
}
