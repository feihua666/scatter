package scatter.usersimple.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.usersimple.rest.ext.UserSimpleLoginUserExtPutServiceImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.usersimple.rest.**.mapper")
public class UsersimpleConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "usersimple";

    @Bean
    @ConditionalOnMissingBean
    public UserSimpleLoginUserExtPutServiceImpl userSimpleLoginUserExtPutServiceImpl(){
        return new UserSimpleLoginUserExtPutServiceImpl();
    }
}
