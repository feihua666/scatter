package scatter.usersimple.rest.ext;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.LoginUser;
import scatter.common.rest.security.LoginUserExtPutService;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.rest.service.IUserSimpleService;

/** 添加
 * Created by yangwei
 * Created at 2021/2/8 11:09
 */
@Slf4j
public class UserSimpleLoginUserExtPutServiceImpl implements LoginUserExtPutService {

    @Autowired
    private IUserSimpleService iUserSimpleService;

    @Override
    public void addExt(LoginUser user) {
        UserSimple byId = iUserSimpleService.getById(user.getId());
        if (byId == null) {
            log.warn("用户登录填充额外信息失败，因为未根据 userId 获取到 UserSimple 实体，userId={}",user.getId());
            return;
        }
        user.setAvatar(byId.getAvatar());
        user.setNickname(byId.getNickname());
    }
}
