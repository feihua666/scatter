package scatter.usersimple.rest.mapper;

import scatter.usersimple.pojo.po.UserSimple;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 简单用户表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserSimpleMapper extends IBaseMapper<UserSimple> {

}
