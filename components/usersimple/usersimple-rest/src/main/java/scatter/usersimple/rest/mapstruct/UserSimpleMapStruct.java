package scatter.usersimple.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.usersimple.pojo.form.UserSimpleAddForm;
import scatter.usersimple.pojo.form.UserSimplePageQueryForm;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm1;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.pojo.vo.UserSimpleVo;

/**
 * <p>
 * 简单用户 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserSimpleMapStruct extends IBaseVoMapStruct<UserSimple, UserSimpleVo>,
        IBaseAddFormMapStruct<UserSimple,UserSimpleAddForm>,
        IBaseUpdateFormMapStruct<UserSimple,UserSimpleUpdateForm>,
        IBaseQueryFormMapStruct<UserSimple,UserSimplePageQueryForm> {
    UserSimpleMapStruct INSTANCE = Mappers.getMapper( UserSimpleMapStruct.class );

    UserSimple userSimpleUpdateForm1ToPo(UserSimpleUpdateForm1 updateForm1);

}
