package scatter.usersimple.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.usersimple.pojo.po.UserSimple;
/**
 * <p>
 * 简单用户表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserSimpleService extends IBaseService<UserSimple> {


}
