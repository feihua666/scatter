package scatter.usersimple.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.cache.CacheHelper;
import scatter.common.rest.validation.DictService;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm1;
import scatter.usersimple.rest.UsersimpleConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.pojo.vo.UserSimpleVo;
import scatter.usersimple.pojo.form.UserSimpleAddForm;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm;
import scatter.usersimple.pojo.form.UserSimplePageQueryForm;
import scatter.usersimple.rest.ext.UserSimpleUpdateSelfListener;
import scatter.usersimple.rest.mapstruct.UserSimpleMapStruct;
import scatter.usersimple.rest.service.IUserSimpleService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 简单用户表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(UsersimpleConfiguration.CONTROLLER_BASE_PATH + "/user-simple")
@Api(tags = "简单用户")
public class UserSimpleController extends BaseAddUpdateQueryFormController<UserSimple, UserSimpleVo, UserSimpleAddForm, UserSimpleUpdateForm, UserSimplePageQueryForm> {

    @Autowired
    private IUserSimpleService iUserSimpleService;
    @Autowired(required = false)
    private List<UserSimpleUpdateSelfListener> userSimpleUpdateSelfListeners;
    @Autowired
    private DictService dictService;

     @Override
	 @ApiOperation("添加简单用户")
     @PreAuthorize("hasAuthority('UserSimple:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserSimpleVo add(@RequestBody @Valid UserSimpleAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询简单用户")
     @PreAuthorize("hasAuthority('UserSimple:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserSimpleVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除简单用户")
     @PreAuthorize("hasAuthority('UserSimple:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新简单用户")
     @PreAuthorize("hasAuthority('UserSimple:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserSimpleVo update(@RequestBody @Valid UserSimpleUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询简单用户")
    @PreAuthorize("hasAuthority('UserSimple:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserSimpleVo> getList(UserSimplePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询简单用户")
    @PreAuthorize("hasAuthority('UserSimple:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserSimpleVo> getPage(UserSimplePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }



    @ApiOperation("更新自己信息")
    @PreAuthorize("hasAuthority('user')")
    @PutMapping("/updateSelf")
    @ResponseStatus(HttpStatus.CREATED)
    public UserSimpleVo update(@RequestBody UserSimpleUpdateForm1 updateForm, @ApiIgnore LoginUser loginUser) {
        UserSimple userSimple = UserSimpleMapStruct.INSTANCE.userSimpleUpdateForm1ToPo(updateForm);
        userSimple.setId(loginUser.getId());
        iUserSimpleService.updateById(userSimple);
        UserSimple byId = iUserSimpleService.getById(loginUser.getId());
        if(!isStrEmpty(updateForm.getAvatar())){
            loginUser.setAvatar(updateForm.getAvatar());
        }
        if(!isStrEmpty(updateForm.getNickname())){
            loginUser.setNickname(updateForm.getNickname());
        }
        if(!isStrEmpty(updateForm.getGenderDictId())){
            String valueById = dictService.getValueById(updateForm.getGenderDictId());
            loginUser.setGender(valueById);
        }

        if (!isEmpty(userSimpleUpdateSelfListeners)) {
            for (UserSimpleUpdateSelfListener userSimpleUpdateSelfListener : userSimpleUpdateSelfListeners) {
                userSimpleUpdateSelfListener.onUserSimpleUpdateSelf(updateForm,loginUser);
            }
        }
        return super.getVo(byId);
    }
}
