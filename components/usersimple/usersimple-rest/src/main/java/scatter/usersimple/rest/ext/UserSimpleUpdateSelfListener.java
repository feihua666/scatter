package scatter.usersimple.rest.ext;

import scatter.common.LoginUser;
import scatter.usersimple.pojo.form.UserSimpleUpdateForm1;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <p>
 * 简单用户自我更新监听
 * </p>
 *
 * @author yangwei
 * @since 2021-11-08 18:20
 */
public interface UserSimpleUpdateSelfListener {
	/**
	 * 更新调用
	 * @param updateForm
	 * @param loginUser
	 */
	public void onUserSimpleUpdateSelf(UserSimpleUpdateForm1 updateForm, LoginUser loginUser);
}
