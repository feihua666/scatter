package scatter.usersimple.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.common.rest.validation.DictService;
import scatter.common.trans.TransConstants;
import scatter.common.trans.UserForTrans;
import scatter.usersimple.pojo.po.UserSimple;
import scatter.usersimple.rest.service.IUserSimpleService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 简单用户翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserSimpleTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private DictService dictService;
    @Autowired
    private IUserSimpleService userSimpleService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,UserSimple.TRANS_USERSIMPLE_BY_ID, TransConstants.TRANS_USER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserSimple.TRANS_USERSIMPLE_BY_ID)) {
            UserSimple byId = userSimpleService.getById(key);
            return new TransResult(byId,key);
        }else
        if (isEqual(type, TransConstants.TRANS_USER_BY_ID)) {
            UserSimple byId = userSimpleService.getById(key);
            UserForTrans userForTrans = userSimpleMapUserForTrans(byId);
            return new TransResult(userForTrans,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserSimple.TRANS_USERSIMPLE_BY_ID)) {
            return userSimpleService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if (isEqual(type,TransConstants.TRANS_USER_BY_ID)) {
            return userSimpleService.listByIds(keys).stream().map(item->new TransResult<Object, String>(userSimpleMapUserForTrans(item),item.getId())).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * 转换实体
     * @param userSimple
     * @return
     */
    private UserForTrans userSimpleMapUserForTrans(UserSimple userSimple){
        UserForTrans userForTrans = new UserForTrans();
        userForTrans.setId(userSimple.getId());
        // user simple 不支持真实姓名
        userForTrans.setName(null);
        userForTrans.setNickname(userSimple.getNickname());
        userForTrans.setAvatar(userSimple.getAvatar());
        String valueById = dictService.getValueById(userSimple.getGenderDictId());
        String nameById = dictService.getNameById(userSimple.getGenderDictId());
        userForTrans.setGenderName(nameById);
        userForTrans.setGenderValue(valueById);
        return userForTrans;
    }
}
