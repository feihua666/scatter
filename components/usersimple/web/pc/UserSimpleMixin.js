import UserSimpleForm from './UserSimpleForm.js'
import UserSimpleTable from './UserSimpleTable.js'
import UserSimpleUrl from './UserSimpleUrl.js'

const UserSimpleMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserSimpleForm,this.$options.name)
        },
        computedTableOptions() {
            return UserSimpleTable
        },
        computedUrl(){
            return UserSimpleUrl
        }
    },
}
export default UserSimpleMixin