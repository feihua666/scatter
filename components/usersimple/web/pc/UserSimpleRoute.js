import UserSimpleUrl from './UserSimpleUrl.js'

const UserSimpleRoute = [
    {
        path: UserSimpleUrl.router.searchList,
        component: () => import('./element/UserSimpleSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserSimpleSearchList',
            name: '简单用户管理'
        }
    },
    {
        path: UserSimpleUrl.router.add,
        component: () => import('./element/UserSimpleAdd'),
        meta: {
            code:'UserSimpleAdd',
            name: '简单用户添加'
        }
    },
    {
        path: UserSimpleUrl.router.update,
        component: () => import('./element/UserSimpleUpdate'),
        meta: {
            code:'UserSimpleUpdate',
            name: '简单用户修改'
        }
    },
]
export default UserSimpleRoute