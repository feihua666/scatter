const UserSimpleForm = [
    {
        UserSimpleSearchList: false,
        field: {
            name: 'avatar'
        },
        element:{
            type: 'uploadAvatar',
            label: '头像',
        }
    },
    {
        UserSimpleSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'nickname'
        },
        element:{
            label: '昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'genderDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'gender'
            },
            label: '性别',
        }
    },

    {
        UserSimpleSearchList: false,
        UserSimpleAdd: false,
        field: {
            name: 'isLock'
        },
        element:{
            type: 'switch',
            label: '锁定状态',
            required: true,
        }
    },
    {
        UserSimpleSearchList: false,
        UserSimpleAdd: false,
        field: {
            name: 'lockReason'
        },
        element:{
            label: '锁定原因',
            disabled: ({form})=>{
                let r = !form.isLock
                if(r){
                    form.lockReason = null
                }
                return r
            },
            required: ({form})=>{
                let r = form.isLock
                return r
            }
        }
    },
    {
        UserSimpleSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'sourceFromDictId'
        },
        element:{
            label: '用户来源',
            type: 'selectDict',
            options: {
                groupCode: 'user_source_from'
            }
        }
    },
]
export default UserSimpleForm