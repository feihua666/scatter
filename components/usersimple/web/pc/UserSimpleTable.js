const UserSimpleTable = [
    {
        prop: 'avatar',
        label: '头像',
        width: '50px',
        stype: 'image'
    },
    {
        prop: 'nickname',
        label: '昵称'
    },
    {
        prop: 'genderDictName',
        label: '性别'
    },
    {
        prop: 'isLock',
        label: '锁定状态'
    },
    {
        prop: 'lockReason',
        label: '锁定原因'
    },
    {
        prop: 'sourceFromDictName',
        label: '用户来源'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
]
export default UserSimpleTable