const basePath = '' + '/user-simple'
const UserSimpleUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    updateSelf: basePath + '/updateSelf',
    router: {
        searchList: '/UserSimpleSearchList',
        add: '/UserSimpleAdd',
        update: '/UserSimpleUpdate',
    }
}
export default UserSimpleUrl