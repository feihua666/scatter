package scatter.usersimple.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 简单用户分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="简单用户分页表单对象")
public class UserSimplePageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "昵称，姓名,模糊查询")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定")
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;
}
