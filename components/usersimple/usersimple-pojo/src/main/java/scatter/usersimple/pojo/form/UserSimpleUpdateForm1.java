package scatter.usersimple.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseUpdateForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 简单用户当前用户自己更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@PropValid
@Setter
@Getter
@ApiModel(value="简单用户当前用户自己更新表单对象")
public class UserSimpleUpdateForm1 extends BaseUpdateForm {

    @NotEmpty(message="昵称不能为空")
    @ApiModelProperty(value = "昵称，姓名",required = true)
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;
}
