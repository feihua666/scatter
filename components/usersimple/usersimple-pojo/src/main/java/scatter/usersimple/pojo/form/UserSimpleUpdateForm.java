package scatter.usersimple.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 简单用户更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@PropValid
@Setter
@Getter
@ApiModel(value="简单用户更新表单对象")
public class UserSimpleUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="昵称不能为空")
    @ApiModelProperty(value = "昵称，姓名,模糊查询",required = true)
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @NotNull(message="锁定状态不能为空")
    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定",required = true)
    private Boolean isLock;

    @PropValid.DependCondition(message = "锁定原因不能为空",dependProp = "isLock",ifEqual = "true")
    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;
}
