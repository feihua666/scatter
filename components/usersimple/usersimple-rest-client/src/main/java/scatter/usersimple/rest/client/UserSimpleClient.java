package scatter.usersimple.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 简单用户表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
@FeignClient(value = "UserSimple-client")
public interface UserSimpleClient {

}
