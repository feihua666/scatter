package scatter.wxcp.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 企业微信应用通知配置表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Component
@FeignClient(value = "WxCpNotifyConfig-client")
public interface WxCpNotifyConfigClient {

}
