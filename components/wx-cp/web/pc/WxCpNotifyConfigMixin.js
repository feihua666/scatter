import WxCpNotifyConfigForm from './WxCpNotifyConfigForm.js'
import WxCpNotifyConfigTable from './WxCpNotifyConfigTable.js'
import WxCpNotifyConfigUrl from './WxCpNotifyConfigUrl.js'
const WxCpNotifyConfigMixin = {
    computed: {
        WxCpNotifyConfigFormOptions() {
            return this.$stDynamicFormTools.options(WxCpNotifyConfigForm,this.$options.name)
        },
        WxCpNotifyConfigTableOptions() {
            return WxCpNotifyConfigTable
        },
        WxCpNotifyConfigUrl(){
            return WxCpNotifyConfigUrl
        }
    },
}
export default WxCpNotifyConfigMixin