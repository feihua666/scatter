import WxCpNotifyConfigUrl from './WxCpNotifyConfigUrl.js'
const WxCpNotifyConfigForm = [
    {
        WxCpNotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'agentId',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '应用id',
            required: true,
        }
    },
    {
        WxCpNotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'agentName',
        },
        element:{
            label: '应用名称',
            required: true,
        }
    },
    {
        WxCpNotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'toUser',
        },
        element:{
            label: '发送用户',
            required: true,
        }
    },

]
export default WxCpNotifyConfigForm