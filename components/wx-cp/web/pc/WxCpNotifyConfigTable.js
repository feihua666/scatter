const WxCpNotifyConfigTable = [
    {
        prop: 'agentId',
        label: '应用id'
    },
    {
        prop: 'agentName',
        label: '应用名称'
    },
    {
        prop: 'toUser',
        label: '发送用户'
    },
]
export default WxCpNotifyConfigTable