import WxCpNotifyConfigUrl from './WxCpNotifyConfigUrl.js'

const WxCpNotifyConfigRoute = [
    {
        path: WxCpNotifyConfigUrl.router.searchList,
        component: () => import('./element/WxCpNotifyConfigSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'WxCpNotifyConfigSearchList',
            name: '企业微信应用通知配置管理'
        }
    },
    {
        path: WxCpNotifyConfigUrl.router.add,
        component: () => import('./element/WxCpNotifyConfigAdd'),
        meta: {
            code:'WxCpNotifyConfigAdd',
            name: '企业微信应用通知配置添加'
        }
    },
    {
        path: WxCpNotifyConfigUrl.router.update,
        component: () => import('./element/WxCpNotifyConfigUpdate'),
        meta: {
            code:'WxCpNotifyConfigUpdate',
            name: '企业微信应用通知配置修改'
        }
    },
]
export default WxCpNotifyConfigRoute