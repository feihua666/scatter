const basePath = '' + '/wx-cp-notify-config'
const WxCpNotifyConfigUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/WxCpNotifyConfigSearchList',
        add: '/WxCpNotifyConfigAdd',
        update: '/WxCpNotifyConfigUpdate',
    }
}
export default WxCpNotifyConfigUrl