package scatter.wxcp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.pojo.vo.WxCpNotifyConfigVo;
import scatter.wxcp.pojo.form.WxCpNotifyConfigAddForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigUpdateForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigPageQueryForm;

/**
 * <p>
 * 企业微信应用通知配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxCpNotifyConfigMapStruct extends IBaseVoMapStruct<WxCpNotifyConfig, WxCpNotifyConfigVo>,
                                  IBaseAddFormMapStruct<WxCpNotifyConfig,WxCpNotifyConfigAddForm>,
                                  IBaseUpdateFormMapStruct<WxCpNotifyConfig,WxCpNotifyConfigUpdateForm>,
                                  IBaseQueryFormMapStruct<WxCpNotifyConfig,WxCpNotifyConfigPageQueryForm>{
    WxCpNotifyConfigMapStruct INSTANCE = Mappers.getMapper( WxCpNotifyConfigMapStruct.class );

}
