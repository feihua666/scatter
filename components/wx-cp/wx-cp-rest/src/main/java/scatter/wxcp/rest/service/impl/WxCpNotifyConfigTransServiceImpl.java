package scatter.wxcp.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.rest.service.IWxCpNotifyConfigService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 企业微信应用通知配置翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Component
public class WxCpNotifyConfigTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IWxCpNotifyConfigService wxCpNotifyConfigService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,WxCpNotifyConfig.TRANS_WXCPNOTIFYCONFIG_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,WxCpNotifyConfig.TRANS_WXCPNOTIFYCONFIG_BY_ID)) {
            WxCpNotifyConfig byId = wxCpNotifyConfigService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,WxCpNotifyConfig.TRANS_WXCPNOTIFYCONFIG_BY_ID)) {
            return wxCpNotifyConfigService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
