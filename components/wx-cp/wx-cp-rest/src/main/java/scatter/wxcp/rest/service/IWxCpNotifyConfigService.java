package scatter.wxcp.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.common.rest.service.IBaseService;
import scatter.wxcp.pojo.form.WxCpNotifyConfigAddForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigUpdateForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigPageQueryForm;
import java.util.List;
/**
 * <p>
 * 企业微信应用通知配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
public interface IWxCpNotifyConfigService extends IBaseService<WxCpNotifyConfig> {


    /**
     * 根据应用id查询
     * @param agentId
     * @return
     */
    default WxCpNotifyConfig getByAgentId(String agentId) {
        Assert.hasText(agentId,"agentId不能为空");
        return getOne(Wrappers.<WxCpNotifyConfig>lambdaQuery().eq(WxCpNotifyConfig::getAgentId, agentId));
    }

}
