package scatter.wxcp.rest.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-25 10:08
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "scatter.wx.cp")
public class WxCpProperties {

	/**
	 * 设置企业微信的corpId
	 */
	private String corpId;

	private List<AppConfig> appConfigs;

	@Getter
	@Setter
	public static class AppConfig {
		/**
		 * 设置企业微信应用的AgentId
		 */
		private Integer agentId;

		/**
		 * 设置企业微信应用的Secret
		 */
		private String secret;

		/**
		 * 设置企业微信应用的token
		 */
		private String token;

		/**
		 * 设置企业微信应用的EncodingAESKey
		 */
		private String aesKey;

	}

}
