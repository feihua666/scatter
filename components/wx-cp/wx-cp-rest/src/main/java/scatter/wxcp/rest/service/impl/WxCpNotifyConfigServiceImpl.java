package scatter.wxcp.rest.service.impl;

import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.rest.mapper.WxCpNotifyConfigMapper;
import scatter.wxcp.rest.service.IWxCpNotifyConfigService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.wxcp.pojo.form.WxCpNotifyConfigAddForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigUpdateForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 企业微信应用通知配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Service
@Transactional
public class WxCpNotifyConfigServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<WxCpNotifyConfigMapper, WxCpNotifyConfig, WxCpNotifyConfigAddForm, WxCpNotifyConfigUpdateForm, WxCpNotifyConfigPageQueryForm> implements IWxCpNotifyConfigService {
    @Override
    public void preAdd(WxCpNotifyConfigAddForm addForm,WxCpNotifyConfig po) {
        super.preAdd(addForm,po);
        if (!isNull(addForm.getAgentId())) {
            // 应用id已存在不能添加
            assertByColumn(addForm.getAgentId().toString(),WxCpNotifyConfig::getAgentId,false);
        }

    }

    @Override
    public void preUpdate(WxCpNotifyConfigUpdateForm updateForm,WxCpNotifyConfig po) {
        super.preUpdate(updateForm,po);
        if (!isNull(updateForm.getAgentId())) {
            WxCpNotifyConfig byId = getById(updateForm.getId());
            // 如果应用id有改动
            if (!isEqual(updateForm.getAgentId(), byId.getAgentId())) {
                // 应用id已存在不能修改
                assertByColumn(updateForm.getAgentId().toString(),WxCpNotifyConfig::getAgentId,false);
            }
        }

    }
}
