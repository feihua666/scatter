package scatter.wxcp.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wxcp.rest.WxCpConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.pojo.vo.WxCpNotifyConfigVo;
import scatter.wxcp.pojo.form.WxCpNotifyConfigAddForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigUpdateForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigPageQueryForm;
import scatter.wxcp.rest.service.IWxCpNotifyConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 企业微信应用通知配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Api(tags = "企业微信应用通知配置相关接口")
@RestController
@RequestMapping(WxCpConfiguration.CONTROLLER_BASE_PATH + "/wx-cp-notify-config")
public class WxCpNotifyConfigController extends BaseAddUpdateQueryFormController<WxCpNotifyConfig, WxCpNotifyConfigVo, WxCpNotifyConfigAddForm, WxCpNotifyConfigUpdateForm, WxCpNotifyConfigPageQueryForm> {
    @Autowired
    private IWxCpNotifyConfigService iWxCpNotifyConfigService;

    @ApiOperation("添加企业微信应用通知配置")
    @PreAuthorize("hasAuthority('WxCpNotifyConfig:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public WxCpNotifyConfigVo add(@RequestBody @Valid WxCpNotifyConfigAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询企业微信应用通知配置")
    @PreAuthorize("hasAuthority('WxCpNotifyConfig:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public WxCpNotifyConfigVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除企业微信应用通知配置")
    @PreAuthorize("hasAuthority('WxCpNotifyConfig:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新企业微信应用通知配置")
    @PreAuthorize("hasAuthority('WxCpNotifyConfig:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public WxCpNotifyConfigVo update(@RequestBody @Valid WxCpNotifyConfigUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询企业微信应用通知配置")
    @PreAuthorize("hasAuthority('WxCpNotifyConfig:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<WxCpNotifyConfigVo> getList(WxCpNotifyConfigPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询企业微信应用通知配置")
    @PreAuthorize("hasAuthority('WxCpNotifyConfig:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<WxCpNotifyConfigVo> getPage(WxCpNotifyConfigPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
