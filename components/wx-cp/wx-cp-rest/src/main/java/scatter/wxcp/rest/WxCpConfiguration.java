package scatter.wxcp.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import scatter.wxcp.rest.config.WxCpConfig;
import scatter.wxcp.rest.notify.DefaultWxCpNotifyAgencyIdResolverImpl;
import scatter.wxcp.rest.notify.DefaultWxCpNotifyListenerImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@Import(WxCpConfig.class)
@ComponentScan
@MapperScan("scatter.wxcp.rest.**.mapper")
public class WxCpConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "wxcp";


    @Bean
    @ConditionalOnMissingBean
    public DefaultWxCpNotifyAgencyIdResolverImpl defaultWxCpNotifyAgencyIdResolverImpl(){
        return new DefaultWxCpNotifyAgencyIdResolverImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public DefaultWxCpNotifyListenerImpl defaultWxCpNotifyListenerImpl(){
        return new DefaultWxCpNotifyListenerImpl();
    }
}
