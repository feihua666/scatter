package scatter.wxcp.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxcp.rest.WxCpConfiguration;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.rest.service.IWxCpNotifyConfigService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 企业微信应用通知配置表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@RestController
@RequestMapping(WxCpConfiguration.CONTROLLER_BASE_PATH + "/inner/wx-cp-notify-config")
public class WxCpNotifyConfigInnerController extends BaseInnerController<WxCpNotifyConfig> {
 @Autowired
 private IWxCpNotifyConfigService wxCpNotifyConfigService;

 public IWxCpNotifyConfigService getService(){
     return wxCpNotifyConfigService;
 }
}
