package scatter.wxcp.rest.notify;

import scatter.common.rest.notify.NotifyParam;

/**
 * <p>
 * 企业微信 agencyId 提供服务
 * </p>
 *
 * @author yangwei
 * @since 2021-08-25 19:38
 */
public interface IWxCpNotifyAgencyIdResolver {

	/**
	 * 根据参数提供 agencyId
	 * @param param
	 * @return
	 */
	public Integer resolve(NotifyParam param);
}
