package scatter.wxcp.rest.mapper;

import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 企业微信应用通知配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
public interface WxCpNotifyConfigMapper extends IBaseMapper<WxCpNotifyConfig> {

}
