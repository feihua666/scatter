DROP TABLE IF EXISTS component_wx_cp_notify_config;
CREATE TABLE `component_wx_cp_notify_config` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `agent_id` int(11) NOT NULL COMMENT '应用id',
  `agent_name` varchar(50) NOT NULL COMMENT '应用名称',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `to_user` text NOT NULL COMMENT '发送用户',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `agent_id` (`agent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业微信应用通知配置表';
