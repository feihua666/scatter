package scatter.wxcp.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.pojo.form.WxCpNotifyConfigAddForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigUpdateForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigPageQueryForm;
import scatter.wxcp.rest.service.IWxCpNotifyConfigService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 企业微信应用通知配置 测试类基类
* </p>
*
* @author yw
* @since 2021-08-25
*/
public class WxCpNotifyConfigSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IWxCpNotifyConfigService wxCpNotifyConfigService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return wxCpNotifyConfigService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return wxCpNotifyConfigService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public WxCpNotifyConfig mockPo() {
        return JMockData.mock(WxCpNotifyConfig.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public WxCpNotifyConfigAddForm mockAddForm() {
        return JMockData.mock(WxCpNotifyConfigAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public WxCpNotifyConfigUpdateForm mockUpdateForm() {
        return JMockData.mock(WxCpNotifyConfigUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public WxCpNotifyConfigPageQueryForm mockPageQueryForm() {
        return JMockData.mock(WxCpNotifyConfigPageQueryForm.class, mockConfig);
    }
}