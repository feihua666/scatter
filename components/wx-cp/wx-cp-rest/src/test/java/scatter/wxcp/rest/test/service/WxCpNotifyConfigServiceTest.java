package scatter.wxcp.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxcp.pojo.po.WxCpNotifyConfig;
import scatter.wxcp.pojo.form.WxCpNotifyConfigAddForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigUpdateForm;
import scatter.wxcp.pojo.form.WxCpNotifyConfigPageQueryForm;
import scatter.wxcp.rest.test.WxCpNotifyConfigSuperTest;
import scatter.wxcp.rest.service.IWxCpNotifyConfigService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 企业微信应用通知配置 服务测试类
* </p>
*
* @author yw
* @since 2021-08-25
*/
@SpringBootTest
public class WxCpNotifyConfigServiceTest extends WxCpNotifyConfigSuperTest{

    @Autowired
    private IWxCpNotifyConfigService wxCpNotifyConfigService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<WxCpNotifyConfig> pos = wxCpNotifyConfigService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}