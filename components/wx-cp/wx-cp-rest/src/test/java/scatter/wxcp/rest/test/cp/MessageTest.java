package scatter.wxcp.rest.test.cp;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.message.WxCpMessage;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxcp.rest.config.WxCpConfig;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-25 10:24
 */
@SpringBootTest
public class MessageTest {

	@Test
	public void messagePushTest() throws WxErrorException {
		WxCpService cpService = WxCpConfig.getCpService(1000004);
		WxCpMessage wxCpMessage = WxCpMessage.TEXT().content("测试").toUser("@all").build();

		cpService.getMessageService().send(wxCpMessage);
	}
}
