package scatter.wxcp.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.wxcp.rest.WxCpConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {ManagementWebSecurityAutoConfiguration.class,SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, WxCpConfiguration.class})
@MapperScan("scatter.wxcp.rest.mapper")
public class WxCpTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(WxCpTestApplication.class, args);
    }
}
