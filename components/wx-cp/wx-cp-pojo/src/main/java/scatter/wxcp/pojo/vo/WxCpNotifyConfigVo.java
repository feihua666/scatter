package scatter.wxcp.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 企业微信应用通知配置响应对象
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Setter
@Getter
@ApiModel(value="企业微信应用通知配置响应对象")
public class WxCpNotifyConfigVo extends BaseIdVo {

    @ApiModelProperty(value = "应用id")
    private Integer agentId;

    @ApiModelProperty(value = "应用名称")
    private String agentName;

    @ApiModelProperty(value = "发送用户")
    private String toUser;

}
