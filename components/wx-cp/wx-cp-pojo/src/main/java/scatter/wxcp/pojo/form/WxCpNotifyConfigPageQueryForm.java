package scatter.wxcp.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 企业微信应用通知配置分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Setter
@Getter
@ApiModel(value="企业微信应用通知配置分页表单对象")
public class WxCpNotifyConfigPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "应用id")
    private Integer agentId;

    @ApiModelProperty(value = "应用名称")
    private String agentName;

    @ApiModelProperty(value = "发送用户")
    private String toUser;

}
