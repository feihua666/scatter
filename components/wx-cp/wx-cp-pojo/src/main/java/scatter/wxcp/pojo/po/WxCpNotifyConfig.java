package scatter.wxcp.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业微信应用通知配置表
 * </p>
 *
 * @author yw
 * @since 2021-08-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_wx_cp_notify_config")
@ApiModel(value="WxCpNotifyConfig对象", description="企业微信应用通知配置表")
public class WxCpNotifyConfig extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_WXCPNOTIFYCONFIG_BY_ID = "trans_wxcpnotifyconfig_by_id_scatter.wxcp.pojo.po";

    @ApiModelProperty(value = "应用id")
    private Integer agentId;

    @ApiModelProperty(value = "应用名称")
    private String agentName;

    @ApiModelProperty(value = "发送用户")
    private String toUser;


}
