const BannerTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'imgUrl',
        label: '图片地址',
        stype: 'image'
    },
    {
        prop: 'imgDescription',
        label: '图片描述'
    },
    {
        prop: 'actionLink',
        label: '跳转地址'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'startAt',
        label: '开始时间'
    },
    {
        prop: 'endAt',
        label: '结束时间'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'seq',
        label: '排序'
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default BannerTable