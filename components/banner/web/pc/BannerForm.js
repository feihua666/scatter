import BannerUrl from './BannerUrl.js'
const BannerForm = [
    {
        BannerSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'imgUrl',
        },
        element:{
            type: 'uploadSingleImage',
            label: '图片地址',
            required: true,
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'imgDescription',
        },
        element:{
            label: '图片描述',
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'actionLink',
        },
        element:{
            label: '跳转地址',
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
            disabled:(form)=>{
                return !form.isDisabled
            },
            required:(form)=>{
                let r = form.isDisabled
                if (!r) {
                    form.disabledReason = ''
                }
                return r
            }
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'startAt',
        },
        element:{
            type: 'datetime',
            label: '开始时间',
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'endAt',
        },
        element:{
            type: 'datetime',
            label: '结束时间',
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'seq',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        }
    },
    {
        BannerSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '备注',
        }
    },
]
export default BannerForm