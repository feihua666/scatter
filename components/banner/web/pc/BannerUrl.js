const basePath = '' + '/banner'
const BannerUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getAvailableList: basePath + '/getAvailableList',
    router: {
        searchList: '/BannerSearchList',
        add: '/BannerAdd',
        update: '/BannerUpdate',
    }
}
export default BannerUrl