import BannerForm from './BannerForm.js'
import BannerTable from './BannerTable.js'
import BannerUrl from './BannerUrl.js'
const BannerMixin = {
    computed: {
        BannerFormOptions() {
            return this.$stDynamicFormTools.options(BannerForm,this.$options.name)
        },
        BannerTableOptions() {
            return BannerTable
        },
        BannerUrl(){
            return BannerUrl
        }
    },
}
export default BannerMixin