import BannerUrl from './BannerUrl.js'
const BannerRoute = [
    {
        path: BannerUrl.router.searchList,
        component: () => import('./element/BannerSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'BannerSearchList',
            name: '横幅管理'
        }
    },
    {
        path: BannerUrl.router.add,
        component: () => import('./element/BannerAdd'),
        meta: {
            code:'BannerAdd',
            name: '横幅添加'
        }
    },
    {
        path: BannerUrl.router.update,
        component: () => import('./element/BannerUpdate'),
        meta: {
            code:'BannerUpdate',
            name: '横幅修改'
        }
    },
]
export default BannerRoute