package scatter.banner.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 横幅表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@Component
@FeignClient(value = "Banner-client")
public interface BannerClient {

}
