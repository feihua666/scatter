package scatter.banner.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.banner.pojo.po.Banner;
import scatter.banner.pojo.form.BannerAddForm;
import scatter.banner.pojo.form.BannerUpdateForm;
import scatter.banner.pojo.form.BannerPageQueryForm;
import scatter.banner.rest.test.BannerSuperTest;
import scatter.banner.rest.service.IBannerService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 横幅 服务测试类
* </p>
*
* @author yw
* @since 2021-01-26
*/
@SpringBootTest
public class BannerServiceTest extends BannerSuperTest{

    @Autowired
    private IBannerService bannerService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Banner> pos = bannerService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}