package scatter.banner.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.banner.rest.test.BannerSuperTest;
/**
* <p>
* 横幅 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-01-26
*/
@SpringBootTest
public class BannerInnerControllerTest extends BannerSuperTest{
    @Test
    void contextLoads() {
    }
}