package scatter.banner.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.banner.pojo.po.Banner;
import scatter.banner.pojo.form.BannerAddForm;
import scatter.banner.pojo.form.BannerUpdateForm;
import scatter.banner.pojo.form.BannerPageQueryForm;
import scatter.banner.rest.service.IBannerService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 横幅 测试类基类
* </p>
*
* @author yw
* @since 2021-01-26
*/
public class BannerSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IBannerService bannerService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return bannerService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return bannerService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Banner mockPo() {
        return JMockData.mock(Banner.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public BannerAddForm mockAddForm() {
        return JMockData.mock(BannerAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public BannerUpdateForm mockUpdateForm() {
        return JMockData.mock(BannerUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public BannerPageQueryForm mockPageQueryForm() {
        return JMockData.mock(BannerPageQueryForm.class, mockConfig);
    }
}