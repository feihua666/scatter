package scatter.banner.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.banner.pojo.form.BannerAddForm;
import scatter.banner.pojo.form.BannerPageQueryForm;
import scatter.banner.pojo.form.BannerUpdateForm;
import scatter.banner.pojo.po.Banner;
import scatter.banner.pojo.vo.BannerVo;
import scatter.banner.rest.BannerConfiguration;
import scatter.banner.rest.service.IBannerService;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 横幅表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@Api(tags = "横幅相关接口")
@RestController
@RequestMapping(BannerConfiguration.CONTROLLER_BASE_PATH + "/banner")
public class BannerController extends BaseAddUpdateQueryFormController<Banner, BannerVo, BannerAddForm, BannerUpdateForm, BannerPageQueryForm> {

    @Autowired
    private IBannerService iBannerService;

     @ApiOperation("添加横幅")
     @PreAuthorize("hasAuthority('Banner:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public BannerVo add(@RequestBody @Valid BannerAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询横幅")
     //@PreAuthorize("hasAuthority('Banner:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public BannerVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除横幅")
     @PreAuthorize("hasAuthority('Banner:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新横幅")
     @PreAuthorize("hasAuthority('Banner:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public BannerVo update(@RequestBody @Valid BannerUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询横幅")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<BannerVo> getList(BannerPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询横幅")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<BannerVo> getPage(BannerPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("获取可用幅")
    @GetMapping("/getAvailableList")
    @ResponseStatus(HttpStatus.OK)
    public List<BannerVo> getAvailableList(String groupFlags) {
        return super.posToVos(iBannerService.listAvailable(groupFlags));
    }

}
