package scatter.banner.rest.service.impl;

import scatter.banner.pojo.po.Banner;
import scatter.banner.rest.mapper.BannerMapper;
import scatter.banner.rest.service.IBannerService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.banner.pojo.form.BannerAddForm;
import scatter.banner.pojo.form.BannerUpdateForm;
import scatter.banner.pojo.form.BannerPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 横幅表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@Service
@Transactional
public class BannerServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<BannerMapper, Banner, BannerAddForm, BannerUpdateForm, BannerPageQueryForm> implements IBannerService {
    @Override
    public void preAdd(BannerAddForm addForm,Banner po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(BannerUpdateForm updateForm,Banner po) {
        super.preUpdate(updateForm,po);

    }
}
