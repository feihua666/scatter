package scatter.banner.rest.mapper;

import scatter.banner.pojo.po.Banner;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 横幅表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
public interface BannerMapper extends IBaseMapper<Banner> {

}
