package scatter.banner.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.banner.pojo.po.Banner;
import scatter.banner.rest.service.IBannerService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 横幅翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@Component
public class BannerTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IBannerService bannerService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Banner.TRANS_BANNER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Banner.TRANS_BANNER_BY_ID)) {
            Banner byId = bannerService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Banner.TRANS_BANNER_BY_ID)) {
            return bannerService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
