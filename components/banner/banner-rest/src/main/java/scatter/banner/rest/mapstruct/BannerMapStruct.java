package scatter.banner.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.banner.pojo.po.Banner;
import scatter.banner.pojo.vo.BannerVo;
import scatter.banner.pojo.form.BannerAddForm;
import scatter.banner.pojo.form.BannerUpdateForm;
import scatter.banner.pojo.form.BannerPageQueryForm;

/**
 * <p>
 * 横幅 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BannerMapStruct extends IBaseVoMapStruct<Banner, BannerVo>,
                                  IBaseAddFormMapStruct<Banner,BannerAddForm>,
                                  IBaseUpdateFormMapStruct<Banner,BannerUpdateForm>,
                                  IBaseQueryFormMapStruct<Banner,BannerPageQueryForm>{
    BannerMapStruct INSTANCE = Mappers.getMapper( BannerMapStruct.class );

}
