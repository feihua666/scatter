DROP TABLE IF EXISTS component_banner;
CREATE TABLE `component_banner` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `name` varchar(255) NOT NULL COMMENT '名称，模糊查询',
  `img_url` varchar(300) NOT NULL COMMENT '图片地址',
  `img_description` varchar(255) DEFAULT NULL COMMENT '图片描述',
  `action_link` varchar(300) DEFAULT NULL COMMENT '跳转地址',
  `is_disabled` tinyint(1) NOT NULL COMMENT '是否禁用',
  `disabled_reason` varchar(255) DEFAULT NULL COMMENT '禁用原因',
  `start_at` datetime DEFAULT NULL COMMENT '开始时间',
  `end_at` datetime DEFAULT NULL COMMENT '结束时间',
  `group_flag` varchar(255) DEFAULT NULL COMMENT '分组标识',
  `seq` int(10) NOT NULL COMMENT '排序,默认按该字段升序排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='横幅表';
