package scatter.banner.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;

import scatter.common.pojo.form.SetNullWhenNull;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 横幅更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@PropValid
@Setter
@Getter
@ApiModel(value="横幅更新表单对象")
public class BannerUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @NotEmpty(message="图片地址不能为空")
    @ApiModelProperty(value = "图片地址",required = true)
    private String imgUrl;

    @SetNullWhenNull
    @ApiModelProperty(value = "图片描述")
    private String imgDescription;

    @SetNullWhenNull
    @ApiModelProperty(value = "跳转地址")
    private String actionLink;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;


    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @SetNullWhenNull
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startAt;

    @SetNullWhenNull
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endAt;

    @SetNullWhenNull
    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序,默认按该字段升序排序",required = true)
    private Integer seq;

    @ApiModelProperty(value = "备注")
    private String remark;


}
