package scatter.banner.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.banner.rest.BannerConfiguration;
import scatter.banner.pojo.po.Banner;
import scatter.banner.rest.service.IBannerService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 横幅表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-01-26
 */
@RestController
@RequestMapping(BannerConfiguration.CONTROLLER_BASE_PATH + "/inner/banner")
public class BannerInnerController extends BaseInnerController<Banner> {
 @Autowired
 private IBannerService bannerService;

 public IBannerService getService(){
     return bannerService;
 }
}
