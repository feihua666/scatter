package scatter.notify.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.notify.pojo.po.NotifyConfig;
import scatter.notify.pojo.form.NotifyConfigAddForm;
import scatter.notify.pojo.form.NotifyConfigUpdateForm;
import scatter.notify.pojo.form.NotifyConfigPageQueryForm;
import scatter.notify.rest.test.NotifyConfigSuperTest;
import scatter.notify.rest.service.INotifyConfigService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 通知配置 服务测试类
* </p>
*
* @author yw
* @since 2021-08-11
*/
@SpringBootTest
public class NotifyConfigServiceTest extends NotifyConfigSuperTest{

    @Autowired
    private INotifyConfigService notifyConfigService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<NotifyConfig> pos = notifyConfigService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}