package scatter.notify.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.notify.pojo.po.NotifyConfig;
import scatter.notify.pojo.vo.NotifyConfigVo;
import scatter.notify.pojo.form.NotifyConfigAddForm;
import scatter.notify.pojo.form.NotifyConfigUpdateForm;
import scatter.notify.pojo.form.NotifyConfigPageQueryForm;

/**
 * <p>
 * 通知配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NotifyConfigMapStruct extends IBaseVoMapStruct<NotifyConfig, NotifyConfigVo>,
                                  IBaseAddFormMapStruct<NotifyConfig,NotifyConfigAddForm>,
                                  IBaseUpdateFormMapStruct<NotifyConfig,NotifyConfigUpdateForm>,
                                  IBaseQueryFormMapStruct<NotifyConfig,NotifyConfigPageQueryForm>{
    NotifyConfigMapStruct INSTANCE = Mappers.getMapper( NotifyConfigMapStruct.class );

}
