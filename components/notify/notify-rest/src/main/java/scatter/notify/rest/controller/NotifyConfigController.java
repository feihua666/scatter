package scatter.notify.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.notify.rest.NotifyConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.notify.pojo.po.NotifyConfig;
import scatter.notify.pojo.vo.NotifyConfigVo;
import scatter.notify.pojo.form.NotifyConfigAddForm;
import scatter.notify.pojo.form.NotifyConfigUpdateForm;
import scatter.notify.pojo.form.NotifyConfigPageQueryForm;
import scatter.notify.rest.service.INotifyConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 通知配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Api(tags = "通知配置相关接口")
@RestController
@RequestMapping(NotifyConfiguration.CONTROLLER_BASE_PATH + "/notify-config")
public class NotifyConfigController extends BaseAddUpdateQueryFormController<NotifyConfig, NotifyConfigVo, NotifyConfigAddForm, NotifyConfigUpdateForm, NotifyConfigPageQueryForm> {
    @Autowired
    private INotifyConfigService iNotifyConfigService;

    @ApiOperation("添加通知配置")
    @PreAuthorize("hasAuthority('NotifyConfig:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public NotifyConfigVo add(@RequestBody @Valid NotifyConfigAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询通知配置")
    @PreAuthorize("hasAuthority('NotifyConfig:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public NotifyConfigVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除通知配置")
    @PreAuthorize("hasAuthority('NotifyConfig:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新通知配置")
    @PreAuthorize("hasAuthority('NotifyConfig:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public NotifyConfigVo update(@RequestBody @Valid NotifyConfigUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询通知配置")
    @PreAuthorize("hasAuthority('NotifyConfig:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<NotifyConfigVo> getList(NotifyConfigPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询通知配置")
    @PreAuthorize("hasAuthority('NotifyConfig:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<NotifyConfigVo> getPage(NotifyConfigPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
