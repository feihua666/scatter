package scatter.notify.rest.notify;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 发件人配置
 * 此配置参考 https://hutool.cn/docs/#/extra/%E9%82%AE%E4%BB%B6%E5%B7%A5%E5%85%B7-MailUtil
 * 默认发送工具也是这个
 * </p>
 *
 * @author yangwei
 * @since 2021-08-26 22:34
 */
@Setter
@Getter
public class EmailAccountProperties {


	// 邮件服务器的SMTP地址
	private String host;
	// 邮件服务器的SMTP端口
	private Integer port;
	// 发件人（必须正确，否则发送失败）
	private String from;
	// 用户名（注意：如果使用foxmail邮箱，此处user为qq号）
	private String user;
	// 密码（注意，某些邮箱需要为SMTP服务单独设置密码，详情查看相关帮助）
	private String pass;
	//使用 STARTTLS安全连接，STARTTLS是对纯文本通信协议的扩展。
	private Boolean starttlsEnable;

	// 使用SSL安全连接
	private Boolean sslEnable;
	// 指定实现javax.net.SocketFactory接口的类的名称,这个类将被用于创建SMTP的套接字
	private String socketFactoryClass;
	// 如果设置为true,未能创建一个套接字使用指定的套接字工厂类将导致使用java.net.Socket创建的套接字类, 默认值为true
	private Boolean socketFactoryFallback;
	// 指定的端口连接到在使用指定的套接字工厂。如果没有设置,将使用默认端口456
	private Integer socketFactoryPort;

	// SMTP超时时长，单位毫秒，缺省值不超时
	private Integer timeout;
	// Socket连接超时值，单位毫秒，缺省值不超时
	private Integer connectionTimeout;
}
