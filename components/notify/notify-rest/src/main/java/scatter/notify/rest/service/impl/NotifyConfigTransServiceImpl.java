package scatter.notify.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.notify.pojo.po.NotifyConfig;
import scatter.notify.rest.service.INotifyConfigService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 通知配置翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Component
public class NotifyConfigTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private INotifyConfigService notifyConfigService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,NotifyConfig.TRANS_NOTIFYCONFIG_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,NotifyConfig.TRANS_NOTIFYCONFIG_BY_ID)) {
            NotifyConfig byId = notifyConfigService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,NotifyConfig.TRANS_NOTIFYCONFIG_BY_ID)) {
            return notifyConfigService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
