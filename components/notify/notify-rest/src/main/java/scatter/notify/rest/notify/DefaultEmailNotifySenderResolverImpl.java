package scatter.notify.rest.notify;

import org.springframework.boot.context.properties.ConfigurationProperties;
import scatter.common.rest.notify.NotifyParam;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-08-26 22:33
 */
@ConfigurationProperties("scatter.notify.resolver.email")
public class DefaultEmailNotifySenderResolverImpl implements IEmailNotifySenderResolver{


	private EmailAccountProperties account;

	@Override
	public EmailAccountProperties resolve(NotifyParam param){
		return account;
	}

}
