package scatter.notify.rest.mapper;

import scatter.notify.pojo.po.NotifyConfig;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 通知配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface NotifyConfigMapper extends IBaseMapper<NotifyConfig> {

}
