package scatter.notify.rest.notify;

import scatter.common.rest.notify.NotifyParam;

/**
 * <p>
 * 邮件通知发件人配置服务
 * </p>
 *
 * @author yangwei
 * @since 2021-08-26 10:02
 */
public interface IEmailNotifySenderResolver {


	/**
	 * 获取发件人配置
	 * @param param
	 * @return
	 */
	public EmailAccountProperties resolve(NotifyParam param);
}
