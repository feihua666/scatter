package scatter.notify.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.notify.rest.notify.DefaultEmailNotifyListenerImpl;
import scatter.notify.rest.notify.DefaultEmailNotifySenderResolverImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.notify.rest.**.mapper")
public class NotifyConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "notify";

    @Bean
    @ConditionalOnMissingBean
    public DefaultEmailNotifyListenerImpl defaultEmailNotifyListenerImpl(){
        return new DefaultEmailNotifyListenerImpl();
    }
    @Bean
    @ConditionalOnMissingBean
    public DefaultEmailNotifySenderResolverImpl defaultEmailNotifySenderResolverImpl(){
        return new DefaultEmailNotifySenderResolverImpl();
    }
}
