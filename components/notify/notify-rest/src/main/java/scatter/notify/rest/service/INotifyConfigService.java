package scatter.notify.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.notify.pojo.po.NotifyConfig;
import scatter.common.rest.service.IBaseService;
import scatter.notify.pojo.form.NotifyConfigAddForm;
import scatter.notify.pojo.form.NotifyConfigUpdateForm;
import scatter.notify.pojo.form.NotifyConfigPageQueryForm;
import java.util.List;
/**
 * <p>
 * 通知配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface INotifyConfigService extends IBaseService<NotifyConfig> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default NotifyConfig getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<NotifyConfig>lambdaQuery().eq(NotifyConfig::getCode, code));
    }

}
