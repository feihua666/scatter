package scatter.notify.rest.service.impl;

import scatter.notify.pojo.po.NotifyConfig;
import scatter.notify.rest.mapper.NotifyConfigMapper;
import scatter.notify.rest.service.INotifyConfigService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.notify.pojo.form.NotifyConfigAddForm;
import scatter.notify.pojo.form.NotifyConfigUpdateForm;
import scatter.notify.pojo.form.NotifyConfigPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 通知配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Service
@Transactional
public class NotifyConfigServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<NotifyConfigMapper, NotifyConfig, NotifyConfigAddForm, NotifyConfigUpdateForm, NotifyConfigPageQueryForm> implements INotifyConfigService {
    @Override
    public void preAdd(NotifyConfigAddForm addForm,NotifyConfig po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),NotifyConfig::getCode,false);
        }

    }

    @Override
    public void preUpdate(NotifyConfigUpdateForm updateForm,NotifyConfig po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            NotifyConfig byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),NotifyConfig::getCode,false);
            }
        }

    }
}
