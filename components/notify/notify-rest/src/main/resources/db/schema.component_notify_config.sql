DROP TABLE IF EXISTS component_notify_config;
CREATE TABLE `component_notify_config` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `name` varchar(255) NOT NULL COMMENT '名称，模糊查询',
  `content` text NOT NULL COMMENT '配置内容，建议json',
  `type_dict_id` varchar(20) NOT NULL COMMENT '类型，字典id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`),
  KEY `type_dict_id` (`type_dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='通知配置表';
