const basePath = '' + '/notify-config'
const NotifyConfigUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/NotifyConfigSearchList',
        add: '/NotifyConfigAdd',
        update: '/NotifyConfigUpdate',
    }
}
export default NotifyConfigUrl