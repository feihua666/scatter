import NotifyConfigForm from './NotifyConfigForm.js'
import NotifyConfigTable from './NotifyConfigTable.js'
import NotifyConfigUrl from './NotifyConfigUrl.js'
const NotifyConfigMixin = {
    computed: {
        NotifyConfigFormOptions() {
            return this.$stDynamicFormTools.options(NotifyConfigForm,this.$options.name)
        },
        NotifyConfigTableOptions() {
            return NotifyConfigTable
        },
        NotifyConfigUrl(){
            return NotifyConfigUrl
        }
    },
}
export default NotifyConfigMixin