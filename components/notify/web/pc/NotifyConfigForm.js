import NotifyConfigUrl from './NotifyConfigUrl.js'
const NotifyConfigForm = [
    {
        NotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        NotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        NotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'content',
        },
        element:{
            label: '配置内容',
            required: true,
        }
    },
    {
        NotifyConfigSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            required: true,
        }
    },
    {

        field: {
            name: 'remark',
        },
        element:{
            label: '备注',
        }
    },

]
export default NotifyConfigForm