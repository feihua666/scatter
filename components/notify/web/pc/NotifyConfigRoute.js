import NotifyConfigUrl from './NotifyConfigUrl.js'

const NotifyConfigRoute = [
    {
        path: NotifyConfigUrl.router.searchList,
        component: () => import('./element/NotifyConfigSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'NotifyConfigSearchList',
            name: '通知配置管理'
        }
    },
    {
        path: NotifyConfigUrl.router.add,
        component: () => import('./element/NotifyConfigAdd'),
        meta: {
            code:'NotifyConfigAdd',
            name: '通知配置添加'
        }
    },
    {
        path: NotifyConfigUrl.router.update,
        component: () => import('./element/NotifyConfigUpdate'),
        meta: {
            code:'NotifyConfigUpdate',
            name: '通知配置修改'
        }
    },
]
export default NotifyConfigRoute