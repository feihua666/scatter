const NotifyConfigTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'content',
        label: '配置内容'
    },
    {
        prop: 'typeDictId',
        label: '类型'
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default NotifyConfigTable