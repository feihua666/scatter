package scatter.notify.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 通知配置表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Component
@FeignClient(value = "NotifyConfig-client")
public interface NotifyConfigClient {

}
