package scatter.notify.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 通知配置表
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_notify_config")
@ApiModel(value="NotifyConfig对象", description="通知配置表")
public class NotifyConfig extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_NOTIFYCONFIG_BY_ID = "trans_notifyconfig_by_id_scatter.notify.pojo.po";

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "配置内容，建议json")
    private String content;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "备注")
    private String remark;


}
