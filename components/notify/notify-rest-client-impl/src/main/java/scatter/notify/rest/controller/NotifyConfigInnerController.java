package scatter.notify.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.notify.rest.NotifyConfiguration;
import scatter.notify.pojo.po.NotifyConfig;
import scatter.notify.rest.service.INotifyConfigService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 通知配置表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@RestController
@RequestMapping(NotifyConfiguration.CONTROLLER_BASE_PATH + "/inner/notify-config")
public class NotifyConfigInnerController extends BaseInnerController<NotifyConfig> {
 @Autowired
 private INotifyConfigService notifyConfigService;

 public INotifyConfigService getService(){
     return notifyConfigService;
 }
}
