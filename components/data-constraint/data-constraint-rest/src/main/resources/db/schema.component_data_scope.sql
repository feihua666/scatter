DROP TABLE IF EXISTS component_data_scope;
CREATE TABLE `component_data_scope` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `code` varchar(50) NOT NULL COMMENT '数据范围约束编码，模糊查询',
  `name` varchar(50) NOT NULL COMMENT '数据范围约束名称，模糊查询',
  `data_object_id` varchar(20) NOT NULL COMMENT '数据对象id，外键',
  `constraint_content` varchar(2000) DEFAULT NULL COMMENT '约束条件，暂时想到的用sql模板',
  `is_custom` tinyint(1) NOT NULL COMMENT '是否自定义，如果自定义=1，否则为0',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `data_object_id` (`data_object_id`) USING BTREE,
  KEY `is_custom` (`is_custom`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据范围约束表';
