DROP TABLE IF EXISTS component_data_scope_custom_rel;
CREATE TABLE `component_data_scope_custom_rel` (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `data_scope_id` varchar(20) NOT NULL COMMENT '数据范围约束id，外键',
  `data_id` varchar(20) NOT NULL COMMENT '自定义数据id，外键',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `data_id_data_scope_id` (`data_id`,`data_scope_id`) USING BTREE,
  KEY `data_id` (`data_id`) USING BTREE,
  KEY `data_scope_id` (`data_scope_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='数据范围约束和自定义数据关系表';
