DROP TABLE IF EXISTS component_data_object;
CREATE TABLE `component_data_object` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `code` varchar(50) NOT NULL COMMENT '数据对象编码，模糊查询',
  `name` varchar(50) NOT NULL COMMENT '数据对象名称，模糊查询',
  `data_custom_url` varchar(300) DEFAULT NULL COMMENT '数据范围自定义时用来绑定自定义数据的url',
  `is_data_lazy` tinyint(1) DEFAULT NULL COMMENT '自定义数据是否懒加载',
  `interview_mode_dict_id` varchar(20) DEFAULT NULL COMMENT '自定义数据交互方式，字典，想到的是表格和树形',
  `interview_mode_content` varchar(255) DEFAULT NULL COMMENT '数据交互方式内容',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `interview_mode_dict_id` (`interview_mode_dict_id`),
  KEY `version` (`version`),
  KEY `is_data_lazy` (`is_data_lazy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据对象表';
