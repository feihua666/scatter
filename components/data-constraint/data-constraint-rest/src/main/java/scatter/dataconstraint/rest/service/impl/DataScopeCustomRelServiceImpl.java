package scatter.dataconstraint.rest.service.impl;

import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.dataconstraint.rest.mapper.DataScopeCustomRelMapper;
import scatter.dataconstraint.rest.service.IDataScopeCustomRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelAddForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 数据范围约束和自定义数据关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Service
@Transactional
public class DataScopeCustomRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<DataScopeCustomRelMapper, DataScopeCustomRel, DataScopeCustomRelAddForm, DataScopeCustomRelUpdateForm, DataScopeCustomRelPageQueryForm> implements IDataScopeCustomRelService {
    @Override
    public void preAdd(DataScopeCustomRelAddForm addForm,DataScopeCustomRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(DataScopeCustomRelUpdateForm updateForm,DataScopeCustomRel po) {
        super.preUpdate(updateForm,po);

    }
}
