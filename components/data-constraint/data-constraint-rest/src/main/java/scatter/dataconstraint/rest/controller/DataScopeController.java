package scatter.dataconstraint.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.dataconstraint.rest.DataConstraintConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.dataconstraint.pojo.po.DataScope;
import scatter.dataconstraint.pojo.vo.DataScopeVo;
import scatter.dataconstraint.pojo.form.DataScopeAddForm;
import scatter.dataconstraint.pojo.form.DataScopeUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopePageQueryForm;
import scatter.dataconstraint.rest.service.IDataScopeService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 数据范围约束表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Api(tags = "数据范围约束相关接口")
@RestController
@RequestMapping(DataConstraintConfiguration.CONTROLLER_BASE_PATH + "/data-scope")
public class DataScopeController extends BaseAddUpdateQueryFormController<DataScope, DataScopeVo, DataScopeAddForm, DataScopeUpdateForm, DataScopePageQueryForm> {
    @Autowired
    private IDataScopeService iDataScopeService;

     @Override
	 @ApiOperation("添加数据范围约束")
     @PreAuthorize("hasAuthority('DataScope:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DataScopeVo add(@RequestBody @Valid DataScopeAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询数据范围约束")
     @PreAuthorize("hasAuthority('DataScope:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public DataScopeVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除数据范围约束")
     @PreAuthorize("hasAuthority('DataScope:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新数据范围约束")
     @PreAuthorize("hasAuthority('DataScope:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DataScopeVo update(@RequestBody @Valid DataScopeUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询数据范围约束")
    @PreAuthorize("hasAuthority('DataScope:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<DataScopeVo> getList(DataScopePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询数据范围约束")
    @PreAuthorize("hasAuthority('DataScope:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DataScopeVo> getPage(DataScopePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
