package scatter.dataconstraint.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dataconstraint.pojo.po.DataScope;
import scatter.dataconstraint.pojo.vo.DataScopeVo;
import scatter.dataconstraint.pojo.form.DataScopeAddForm;
import scatter.dataconstraint.pojo.form.DataScopeUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopePageQueryForm;

/**
 * <p>
 * 数据范围约束 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DataScopeMapStruct extends IBaseVoMapStruct<DataScope, DataScopeVo>,
                                  IBaseAddFormMapStruct<DataScope,DataScopeAddForm>,
                                  IBaseUpdateFormMapStruct<DataScope,DataScopeUpdateForm>,
                                  IBaseQueryFormMapStruct<DataScope,DataScopePageQueryForm>{
    DataScopeMapStruct INSTANCE = Mappers.getMapper( DataScopeMapStruct.class );

}
