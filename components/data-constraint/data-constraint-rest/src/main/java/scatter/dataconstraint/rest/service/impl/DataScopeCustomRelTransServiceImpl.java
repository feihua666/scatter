package scatter.dataconstraint.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.dataconstraint.rest.service.IDataScopeCustomRelService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 数据范围约束和自定义数据翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Component
public class DataScopeCustomRelTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IDataScopeCustomRelService dataScopeCustomRelService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,DataScopeCustomRel.TRANS_DATASCOPECUSTOMREL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,DataScopeCustomRel.TRANS_DATASCOPECUSTOMREL_BY_ID)) {
            DataScopeCustomRel byId = dataScopeCustomRelService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,DataScopeCustomRel.TRANS_DATASCOPECUSTOMREL_BY_ID)) {
            return dataScopeCustomRelService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
