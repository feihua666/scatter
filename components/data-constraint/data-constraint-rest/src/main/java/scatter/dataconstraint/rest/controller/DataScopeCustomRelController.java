package scatter.dataconstraint.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.dataconstraint.rest.DataConstraintConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.dataconstraint.pojo.vo.DataScopeCustomRelVo;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelAddForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelPageQueryForm;
import scatter.dataconstraint.pojo.form.DataScopeAssignDataForm;
import scatter.dataconstraint.pojo.form.DataAssignDataScopeForm;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import java.util.stream.Collectors;
import scatter.dataconstraint.rest.service.IDataScopeCustomRelService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 数据范围约束和自定义数据关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Api(tags = "数据范围约束和自定义数据相关接口")
@RestController
@RequestMapping(DataConstraintConfiguration.CONTROLLER_BASE_PATH + "/data-scope-custom-rel")
public class DataScopeCustomRelController extends BaseAddUpdateQueryFormController<DataScopeCustomRel, DataScopeCustomRelVo, DataScopeCustomRelAddForm, DataScopeCustomRelUpdateForm, DataScopeCustomRelPageQueryForm> {
    @Autowired
    private IDataScopeCustomRelService iDataScopeCustomRelService;

     @Override
	 @ApiOperation("添加数据范围约束和自定义数据")
     @PreAuthorize("hasAuthority('DataScopeCustomRel:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DataScopeCustomRelVo add(@RequestBody @Valid DataScopeCustomRelAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询数据范围约束和自定义数据")
     @PreAuthorize("hasAuthority('DataScopeCustomRel:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public DataScopeCustomRelVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除数据范围约束和自定义数据")
     @PreAuthorize("hasAuthority('DataScopeCustomRel:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新数据范围约束和自定义数据")
     @PreAuthorize("hasAuthority('DataScopeCustomRel:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DataScopeCustomRelVo update(@RequestBody @Valid DataScopeCustomRelUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询数据范围约束和自定义数据")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<DataScopeCustomRelVo> getList(DataScopeCustomRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询数据范围约束和自定义数据")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DataScopeCustomRelVo> getPage(DataScopeCustomRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("数据范围约束分配自定义数据")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:single:dataScopeAssignData')")
    @PostMapping("/datascope/assign/data")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean dataScopeAssignData(@RequestBody @Valid DataScopeAssignDataForm cf) {
        return iBaseService.removeAndAssignRel(cf.getDataScopeId(),cf.getCheckedDataIds(),cf.getUncheckedDataIds(),cf.getIsLazyLoad(),DataScopeCustomRel::getDataScopeId,DataScopeCustomRel::getDataId,(relDto)->new DataScopeCustomRel().setDataScopeId(relDto.getMainId()).setDataId(relDto.getOtherId()));
    }

    @ApiOperation("根据数据范围约束ID查询已分配的自定义数据id")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:single:queryByDataScopeId')")
    @GetMapping("/datascope/{dataScopeId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByDataScopeId(@PathVariable String dataScopeId) {
        List<DataScopeCustomRel> rels = iDataScopeCustomRelService.getByDataScopeId(dataScopeId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }

        return rels.stream().map(item -> item.getDataId()).collect(Collectors.toList());
    }

    @ApiOperation("清空数据范围约束下的所有自定义数据")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:single:deleteByDataScopeId')")
    @DeleteMapping("/datascope/{dataScopeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByDataScopeId(@PathVariable String dataScopeId) {
        return iBaseService.removeAssignRel(dataScopeId,DataScopeCustomRel::getDataScopeId);
    }


    @ApiOperation("自定义数据分配数据范围约束")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:single:dataAssignDataScope')")
    @PostMapping("/data/assign/datascope")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean dataAssignDataScope(@RequestBody @Valid DataAssignDataScopeForm cf) {
        return iDataScopeCustomRelService.removeAndAssignRel(cf.getDataId(),cf.getCheckedDataScopeIds(),cf.getUncheckedDataScopeIds(),cf.getIsLazyLoad(),DataScopeCustomRel::getDataId,DataScopeCustomRel::getDataScopeId,(relDto)->new DataScopeCustomRel().setDataId(relDto.getMainId()).setDataScopeId(relDto.getOtherId()));
    }

    @ApiOperation("根据自定义数据ID查询已分配的数据范围约束")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:single:queryByDataId')")
    @GetMapping("/data/{dataId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByDataId(@PathVariable String dataId) {
        List<DataScopeCustomRel> rels = iDataScopeCustomRelService.getByDataId(dataId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }
        return rels.stream().map(item -> item.getDataId()).collect(Collectors.toList());
    }

    @ApiOperation("清空自定义数据下的所有数据范围约束")
    @PreAuthorize("hasAuthority('DataScopeCustomRel:single:deleteByDataId')")
    @DeleteMapping("/data/{dataId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByDataId(@PathVariable String dataId) {
        return iDataScopeCustomRelService.removeAssignRel(dataId,DataScopeCustomRel::getDataId);
    }
}
