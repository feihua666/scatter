package scatter.dataconstraint.rest.mapper;

import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 数据范围约束和自定义数据关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
public interface DataScopeCustomRelMapper extends IBaseMapper<DataScopeCustomRel> {

}
