package scatter.dataconstraint.rest.service.impl;

import scatter.dataconstraint.pojo.po.DataScope;
import scatter.dataconstraint.rest.mapper.DataScopeMapper;
import scatter.dataconstraint.rest.service.IDataScopeService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.dataconstraint.pojo.form.DataScopeAddForm;
import scatter.dataconstraint.pojo.form.DataScopeUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 数据范围约束表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Service
@Transactional
public class DataScopeServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<DataScopeMapper, DataScope, DataScopeAddForm, DataScopeUpdateForm, DataScopePageQueryForm> implements IDataScopeService {
    @Override
    public void preAdd(DataScopeAddForm addForm,DataScope po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 数据范围约束编码已存在不能添加
            assertByColumn(addForm.getCode(),DataScope::getCode,false);
        }

    }

    @Override
    public void preUpdate(DataScopeUpdateForm updateForm,DataScope po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            DataScope byId = getById(updateForm.getId());
            // 如果数据范围约束编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 数据范围约束编码已存在不能修改
                assertByColumn(updateForm.getCode(),DataScope::getCode,false);
            }
        }

    }
}
