package scatter.dataconstraint.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.dataconstraint.pojo.po.DataScope;
import scatter.dataconstraint.rest.service.IDataScopeService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 数据范围约束翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Component
public class DataScopeTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IDataScopeService dataScopeService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,DataScope.TRANS_DATASCOPE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,DataScope.TRANS_DATASCOPE_BY_ID)) {
            DataScope byId = dataScopeService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,DataScope.TRANS_DATASCOPE_BY_ID)) {
            return dataScopeService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
