package scatter.dataconstraint.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.common.rest.service.IBaseService;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelAddForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 数据范围约束和自定义数据关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
public interface IDataScopeCustomRelService extends IBaseService<DataScopeCustomRel> {

    /**
     * 根据数据范围约束id查询
     * @param dataScopeId
     * @return
     */
    default List<DataScopeCustomRel> getByDataScopeId(String dataScopeId) {
        Assert.hasText(dataScopeId,"dataScopeId不能为空");
        return list(Wrappers.<DataScopeCustomRel>lambdaQuery().eq(DataScopeCustomRel::getDataScopeId, dataScopeId));
    }
    /**
     * 根据自定义数据id查询
     * @param dataId
     * @return
     */
    default List<DataScopeCustomRel> getByDataId(String dataId) {
        Assert.hasText(dataId,"dataId不能为空");
        return list(Wrappers.<DataScopeCustomRel>lambdaQuery().eq(DataScopeCustomRel::getDataId, dataId));
    }
    /**
     * 根据描述查询
     * @param remark
     * @return
     */
    default List<DataScopeCustomRel> getByRemark(String remark) {
        Assert.hasText(remark,"remark不能为空");
        return list(Wrappers.<DataScopeCustomRel>lambdaQuery().eq(DataScopeCustomRel::getRemark, remark));
    }

}
