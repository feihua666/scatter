package scatter.dataconstraint.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.dataconstraint.rest.DataConstraintConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.dataconstraint.pojo.po.DataObject;
import scatter.dataconstraint.pojo.vo.DataObjectVo;
import scatter.dataconstraint.pojo.form.DataObjectAddForm;
import scatter.dataconstraint.pojo.form.DataObjectUpdateForm;
import scatter.dataconstraint.pojo.form.DataObjectPageQueryForm;
import scatter.dataconstraint.rest.service.IDataObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 数据对象表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Api(tags = "数据对象相关接口")
@RestController
@RequestMapping(DataConstraintConfiguration.CONTROLLER_BASE_PATH + "/data-object")
public class DataObjectController extends BaseAddUpdateQueryFormController<DataObject, DataObjectVo, DataObjectAddForm, DataObjectUpdateForm, DataObjectPageQueryForm> {
    @Autowired
    private IDataObjectService iDataObjectService;

     @Override
	 @ApiOperation("添加数据对象")
     @PreAuthorize("hasAuthority('DataObject:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DataObjectVo add(@RequestBody @Valid DataObjectAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询数据对象")
     @PreAuthorize("hasAuthority('DataObject:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public DataObjectVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除数据对象")
     @PreAuthorize("hasAuthority('DataObject:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新数据对象")
     @PreAuthorize("hasAuthority('DataObject:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DataObjectVo update(@RequestBody @Valid DataObjectUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询数据对象")
    @PreAuthorize("hasAuthority('DataObject:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<DataObjectVo> getList(DataObjectPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询数据对象")
    @PreAuthorize("hasAuthority('DataObject:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DataObjectVo> getPage(DataObjectPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
