package scatter.dataconstraint.rest.mapper;

import scatter.dataconstraint.pojo.po.DataObject;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 数据对象表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
public interface DataObjectMapper extends IBaseMapper<DataObject> {

}
