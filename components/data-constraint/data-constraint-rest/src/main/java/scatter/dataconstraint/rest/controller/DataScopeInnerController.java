package scatter.dataconstraint.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dataconstraint.rest.DataConstraintConfiguration;
import scatter.dataconstraint.pojo.po.DataScope;
import scatter.dataconstraint.rest.service.IDataScopeService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 数据范围约束表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@RestController
@RequestMapping(DataConstraintConfiguration.CONTROLLER_BASE_PATH + "/inner/data-scope")
public class DataScopeInnerController extends BaseInnerController<DataScope> {
 @Autowired
 private IDataScopeService dataScopeService;

 public IDataScopeService getService(){
     return dataScopeService;
 }
}
