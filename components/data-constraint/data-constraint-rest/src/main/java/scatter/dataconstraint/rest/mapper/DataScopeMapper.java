package scatter.dataconstraint.rest.mapper;

import scatter.dataconstraint.pojo.po.DataScope;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 数据范围约束表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
public interface DataScopeMapper extends IBaseMapper<DataScope> {

}
