package scatter.dataconstraint.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dataconstraint.rest.DataConstraintConfiguration;
import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.dataconstraint.rest.service.IDataScopeCustomRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 数据范围约束和自定义数据关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@RestController
@RequestMapping(DataConstraintConfiguration.CONTROLLER_BASE_PATH + "/inner/data-scope-custom-rel")
public class DataScopeCustomRelInnerController extends BaseInnerController<DataScopeCustomRel> {
 @Autowired
 private IDataScopeCustomRelService dataScopeCustomRelService;

 public IDataScopeCustomRelService getService(){
     return dataScopeCustomRelService;
 }
}
