package scatter.dataconstraint.rest.service.impl;

import scatter.dataconstraint.pojo.po.DataObject;
import scatter.dataconstraint.rest.mapper.DataObjectMapper;
import scatter.dataconstraint.rest.service.IDataObjectService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.dataconstraint.pojo.form.DataObjectAddForm;
import scatter.dataconstraint.pojo.form.DataObjectUpdateForm;
import scatter.dataconstraint.pojo.form.DataObjectPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 数据对象表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Service
@Transactional
public class DataObjectServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<DataObjectMapper, DataObject, DataObjectAddForm, DataObjectUpdateForm, DataObjectPageQueryForm> implements IDataObjectService {
    @Override
    public void preAdd(DataObjectAddForm addForm,DataObject po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 数据对象编码已存在不能添加
            assertByColumn(addForm.getCode(),DataObject::getCode,false);
        }

    }

    @Override
    public void preUpdate(DataObjectUpdateForm updateForm,DataObject po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            DataObject byId = getById(updateForm.getId());
            // 如果数据对象编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 数据对象编码已存在不能修改
                assertByColumn(updateForm.getCode(),DataObject::getCode,false);
            }
        }

    }
}
