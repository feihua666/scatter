package scatter.dataconstraint.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.dataconstraint.pojo.po.DataScope;
import scatter.common.rest.service.IBaseService;
import scatter.dataconstraint.pojo.form.DataScopeAddForm;
import scatter.dataconstraint.pojo.form.DataScopeUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopePageQueryForm;
import java.util.List;
/**
 * <p>
 * 数据范围约束表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
public interface IDataScopeService extends IBaseService<DataScope> {


    /**
     * 根据数据范围约束编码查询
     * @param code
     * @return
     */
    default DataScope getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<DataScope>lambdaQuery().eq(DataScope::getCode, code));
    }
    /**
     * 根据数据对象id查询
     * @param dataObjectId
     * @return
     */
    default List<DataScope> getByDataObjectId(String dataObjectId) {
        Assert.hasText(dataObjectId,"dataObjectId不能为空");
        return list(Wrappers.<DataScope>lambdaQuery().eq(DataScope::getDataObjectId, dataObjectId));
    }

}
