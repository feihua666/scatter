package scatter.dataconstraint.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dataconstraint.pojo.po.DataObject;
import scatter.dataconstraint.pojo.vo.DataObjectVo;
import scatter.dataconstraint.pojo.form.DataObjectAddForm;
import scatter.dataconstraint.pojo.form.DataObjectUpdateForm;
import scatter.dataconstraint.pojo.form.DataObjectPageQueryForm;

/**
 * <p>
 * 数据对象 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DataObjectMapStruct extends IBaseVoMapStruct<DataObject, DataObjectVo>,
                                  IBaseAddFormMapStruct<DataObject,DataObjectAddForm>,
                                  IBaseUpdateFormMapStruct<DataObject,DataObjectUpdateForm>,
                                  IBaseQueryFormMapStruct<DataObject,DataObjectPageQueryForm>{
    DataObjectMapStruct INSTANCE = Mappers.getMapper( DataObjectMapStruct.class );

}
