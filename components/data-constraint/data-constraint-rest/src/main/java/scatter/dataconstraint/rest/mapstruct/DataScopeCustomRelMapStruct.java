package scatter.dataconstraint.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.dataconstraint.pojo.vo.DataScopeCustomRelVo;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelAddForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelPageQueryForm;

/**
 * <p>
 * 数据范围约束和自定义数据 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DataScopeCustomRelMapStruct extends IBaseVoMapStruct<DataScopeCustomRel, DataScopeCustomRelVo>,
                                  IBaseAddFormMapStruct<DataScopeCustomRel,DataScopeCustomRelAddForm>,
                                  IBaseUpdateFormMapStruct<DataScopeCustomRel,DataScopeCustomRelUpdateForm>,
                                  IBaseQueryFormMapStruct<DataScopeCustomRel,DataScopeCustomRelPageQueryForm>{
    DataScopeCustomRelMapStruct INSTANCE = Mappers.getMapper( DataScopeCustomRelMapStruct.class );

}
