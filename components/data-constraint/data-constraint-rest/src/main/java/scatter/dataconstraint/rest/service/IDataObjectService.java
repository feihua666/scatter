package scatter.dataconstraint.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.dataconstraint.pojo.po.DataObject;
import scatter.common.rest.service.IBaseService;
import scatter.dataconstraint.pojo.form.DataObjectAddForm;
import scatter.dataconstraint.pojo.form.DataObjectUpdateForm;
import scatter.dataconstraint.pojo.form.DataObjectPageQueryForm;
import java.util.List;
/**
 * <p>
 * 数据对象表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
public interface IDataObjectService extends IBaseService<DataObject> {


    /**
     * 根据数据对象编码查询
     * @param code
     * @return
     */
    default DataObject getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<DataObject>lambdaQuery().eq(DataObject::getCode, code));
    }

}
