package scatter.dataconstraint.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dataconstraint.pojo.po.DataObject;
import scatter.dataconstraint.pojo.form.DataObjectAddForm;
import scatter.dataconstraint.pojo.form.DataObjectUpdateForm;
import scatter.dataconstraint.pojo.form.DataObjectPageQueryForm;
import scatter.dataconstraint.rest.service.IDataObjectService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 数据对象 测试类基类
* </p>
*
* @author yw
* @since 2021-03-31
*/
public class DataObjectSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IDataObjectService dataObjectService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return dataObjectService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return dataObjectService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public DataObject mockPo() {
        return JMockData.mock(DataObject.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public DataObjectAddForm mockAddForm() {
        return JMockData.mock(DataObjectAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public DataObjectUpdateForm mockUpdateForm() {
        return JMockData.mock(DataObjectUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public DataObjectPageQueryForm mockPageQueryForm() {
        return JMockData.mock(DataObjectPageQueryForm.class, mockConfig);
    }
}