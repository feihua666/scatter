package scatter.dataconstraint.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dataconstraint.rest.test.DataObjectSuperTest;
/**
* <p>
* 数据对象 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-03-31
*/
@SpringBootTest
public class DataObjectInnerControllerTest extends DataObjectSuperTest{
    @Test
    void contextLoads() {
    }
}