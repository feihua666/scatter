package scatter.dataconstraint.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dataconstraint.pojo.po.DataScopeCustomRel;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelAddForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopeCustomRelPageQueryForm;
import scatter.dataconstraint.rest.service.IDataScopeCustomRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 数据范围约束和自定义数据 测试类基类
* </p>
*
* @author yw
* @since 2021-03-31
*/
public class DataScopeCustomRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IDataScopeCustomRelService dataScopeCustomRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return dataScopeCustomRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return dataScopeCustomRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public DataScopeCustomRel mockPo() {
        return JMockData.mock(DataScopeCustomRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public DataScopeCustomRelAddForm mockAddForm() {
        return JMockData.mock(DataScopeCustomRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public DataScopeCustomRelUpdateForm mockUpdateForm() {
        return JMockData.mock(DataScopeCustomRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public DataScopeCustomRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(DataScopeCustomRelPageQueryForm.class, mockConfig);
    }
}