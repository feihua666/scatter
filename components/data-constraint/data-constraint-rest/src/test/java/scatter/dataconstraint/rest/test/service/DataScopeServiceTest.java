package scatter.dataconstraint.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dataconstraint.pojo.po.DataScope;
import scatter.dataconstraint.pojo.form.DataScopeAddForm;
import scatter.dataconstraint.pojo.form.DataScopeUpdateForm;
import scatter.dataconstraint.pojo.form.DataScopePageQueryForm;
import scatter.dataconstraint.rest.test.DataScopeSuperTest;
import scatter.dataconstraint.rest.service.IDataScopeService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 数据范围约束 服务测试类
* </p>
*
* @author yw
* @since 2021-03-31
*/
@SpringBootTest
public class DataScopeServiceTest extends DataScopeSuperTest{

    @Autowired
    private IDataScopeService dataScopeService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<DataScope> pos = dataScopeService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}