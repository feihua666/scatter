package scatter.dataconstraint.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dataconstraint.pojo.po.DataObject;
import scatter.dataconstraint.pojo.form.DataObjectAddForm;
import scatter.dataconstraint.pojo.form.DataObjectUpdateForm;
import scatter.dataconstraint.pojo.form.DataObjectPageQueryForm;
import scatter.dataconstraint.rest.test.DataObjectSuperTest;
import scatter.dataconstraint.rest.service.IDataObjectService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 数据对象 服务测试类
* </p>
*
* @author yw
* @since 2021-03-31
*/
@SpringBootTest
public class DataObjectServiceTest extends DataObjectSuperTest{

    @Autowired
    private IDataObjectService dataObjectService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<DataObject> pos = dataObjectService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}