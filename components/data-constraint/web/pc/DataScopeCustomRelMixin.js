import DataScopeCustomRelForm from './DataScopeCustomRelForm.js'
import DataScopeCustomRelTable from './DataScopeCustomRelTable.js'
import DataScopeCustomRelUrl from './DataScopeCustomRelUrl.js'
const DataScopeCustomRelMixin = {
    computed: {
        DataScopeCustomRelFormOptions() {
            return this.$stDynamicFormTools.options(DataScopeCustomRelForm,this.$options.name)
        },
        DataScopeCustomRelTableOptions() {
            return DataScopeCustomRelTable
        },
        DataScopeCustomRelUrl(){
            return DataScopeCustomRelUrl
        }
    },
}
export default DataScopeCustomRelMixin