import DataObjectUrl from './DataObjectUrl.js'
const DataObjectForm = [
    {
        DataObjectSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '数据对象编码',
            required: true,
        }
    },
    {
        DataObjectSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '数据对象名称',
            required: true,
        }
    },
    {

        DataObjectSearchList: false,

        field: {
            name: 'dataCustomUrl',
        },
        element:{
            label: '自定义数据url',
            options: {
                placeholder: '数据范围自定义时用来绑定自定义数据的url'
            },
        }
    },
    {

        DataObjectSearchList: false,

        field: {
            name: 'isDataLazy',
            value: false,
        },
        element:{
            type: 'switch',
            label: '否懒加载',
            tips: '自定义数据是否懒加载',
        }
    },
    {

        DataObjectSearchList: false,

        field: {
            name: 'interviewModeDictId',
        },
        element:{
            label: '交互方式',
            tips: '自定义数据交互方式',
            type: 'selectDict',
            options: {
                groupCode: 'data_constraint'
            }
        }
    },
    {


        DataObjectSearchList: false,
        field: {
            name: 'interviewModeContent',
        },
        element:{
            label: '内容',
            tips: '数据交互方式内容',
            type: 'textarea',
            options: {
                placeholder: '请按正确的格式配置JSON'
            }
        }
    },
    {
        DataObjectSearchList: false,

        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default DataObjectForm