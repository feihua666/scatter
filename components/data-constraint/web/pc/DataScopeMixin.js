import DataScopeForm from './DataScopeForm.js'
import DataScopeTable from './DataScopeTable.js'
import DataScopeUrl from './DataScopeUrl.js'
const DataScopeMixin = {
    computed: {
        DataScopeFormOptions() {
            return this.$stDynamicFormTools.options(DataScopeForm,this.$options.name)
        },
        DataScopeTableOptions() {
            return DataScopeTable
        },
        DataScopeUrl(){
            return DataScopeUrl
        }
    },
}
export default DataScopeMixin