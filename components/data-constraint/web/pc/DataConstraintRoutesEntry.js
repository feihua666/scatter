import DataObjectRoute from './DataObjectRoute.js'
import DataScopeCustomRelRoute from './DataScopeCustomRelRoute.js'
import DataScopeRoute from './DataScopeRoute.js'

const DataConstraintRoutesEntry = [
]
.concat(DataObjectRoute)
.concat(DataScopeCustomRelRoute)
.concat(DataScopeRoute)

export default DataConstraintRoutesEntry