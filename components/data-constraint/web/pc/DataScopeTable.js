const DataScopeTable = [
    {
        prop: 'code',
        label: '数据范围约束编码'
    },
    {
        prop: 'name',
        label: '数据范围约束名称'
    },
    {
        prop: 'dataObjectId',
        label: '数据对象id'
    },
    {
        prop: 'constraintContent',
        label: '约束条件'
    },
    {
        prop: 'isCustom',
        label: '是否自定义'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default DataScopeTable