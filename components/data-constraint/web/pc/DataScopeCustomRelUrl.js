const basePath = '' + '/data-scope-custom-rel'
const DataScopeCustomRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    dataScopeAssignData: basePath + '/datascope/assign/data',
    checkedDataIds: basePath + '/datascope/{dataScopeId}',
    dataAssignDataScope: basePath + '/data/assign/datascope',
    checkedDataScopeIds: basePath + '/data/{dataId}',
    router: {
        searchList: '/DataScopeCustomRelSearchList',
        add: '/DataScopeCustomRelAdd',
        update: '/DataScopeCustomRelUpdate',
        dataScopeAssignDataRel: '/dataScopeAssignDataRel',
        dataAssignDataScopeRel: '/dataAssignDataScopeRel',
    }
}
export default DataScopeCustomRelUrl