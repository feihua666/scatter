import DataScopeCustomRelUrl from './DataScopeCustomRelUrl.js'
const DataScopeCustomRelForm = [
    {
        DataScopeCustomRelSearchList: {
            element:{
                required: false
            }
        },

        DataScopeAssignDataRel: false,
        DataAssignDataScopeRel: false,
        field: {
            name: 'dataScopeId',
        },
        element:{
            label: '数据范围约束id',
            required: true,
        }
    },
    {
        DataScopeCustomRelSearchList: {
            element:{
                required: false
            }
        },

        DataScopeAssignDataRel: false,
        DataAssignDataScopeRel: false,
        field: {
            name: 'dataId',
        },
        element:{
            label: '自定义数据id',
            required: true,
        }
    },
    {

        DataScopeAssignDataRel: false,
        DataAssignDataScopeRel: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

    {
        DataScopeCustomRelSearchList: false,
        DataScopeCustomRelAdd: false,
        DataScopeCustomRelUpdate: false,
        DataAssignDataScopeRel: false,

        field: {
            name: 'dataScopeId',
        },
        element:{
            label: '数据范围约束',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo 用于回显，这里可以是一个列表地址，如：RoleUrl.list
            }

        }
    },
    {
        DataScopeCustomRelSearchList: false,
        DataScopeCustomRelAdd: false,
        DataScopeCustomRelUpdate: false,
        DataAssignDataScopeRel: false,

        field: {
            name: 'checkedDataIds',
            value: []
        },
        element:{
            label: '自定义数据',
            type: 'tree',
            options: {
                datas: [], // todo 用于显示分配的数据，这里可以是一个列表地址，如：RoleUrl.list
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        DataScopeCustomRelSearchList: false,
        DataScopeCustomRelAdd: false,
        DataScopeCustomRelUpdate: false,
        DataScopeAssignDataRel: false,

        field: {
            name: 'dataId',
        },
        element:{
            label: '自定义数据',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        DataScopeCustomRelSearchList: false,
        DataScopeCustomRelAdd: false,
        DataScopeCustomRelUpdate: false,
        DataAssignDataScopeRel: false,

        field: {
            name: 'checkedDataScopes',
            value: []
        },
        element:{
            label: '数据范围约束',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default DataScopeCustomRelForm