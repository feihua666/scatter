import DataScopeCustomRelUrl from './DataScopeCustomRelUrl.js'

const DataScopeCustomRelRoute = [
    {
        path: DataScopeCustomRelUrl.router.searchList,
        component: () => import('./element/DataScopeCustomRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DataScopeCustomRelSearchList',
            name: '数据范围约束和自定义数据管理'
        }
    },
    {
        path: DataScopeCustomRelUrl.router.add,
        component: () => import('./element/DataScopeCustomRelAdd'),
        meta: {
            code:'DataScopeCustomRelAdd',
            name: '数据范围约束和自定义数据添加'
        }
    },
    {
        path: DataScopeCustomRelUrl.router.update,
        component: () => import('./element/DataScopeCustomRelUpdate'),
        meta: {
            code:'DataScopeCustomRelUpdate',
            name: '数据范围约束和自定义数据修改'
        }
    },
    {
        path: DataScopeCustomRelUrl.router.dataScopeAssignDataRel,
        component: () => import('./element/DataScopeAssignDataRel'),
        meta: {
            code:'DataScopeAssignDataRel',
            name: '数据范围约束分配自定义数据'
        }
    },
    {
        path: DataScopeCustomRelUrl.router.dataAssignDataScopeRel,
        component: () => import('./element/DataAssignDataScopeRel'),
        meta: {
            code:'DataAssignDataScopeRel',
            name: '自定义数据分配数据范围约束'
        }
    },
]
export default DataScopeCustomRelRoute