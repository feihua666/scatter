const DataObjectTable = [
    {
        prop: 'code',
        label: '数据对象编码'
    },
    {
        prop: 'name',
        label: '数据对象名称'
    },
    {
        prop: 'dataCustomUrl',
        label: '自定义数据url'
    },
    {
        prop: 'isDataLazy',
        label: '否懒加载'
    },
    {
        prop: 'interviewModeDictId',
        label: '交互方式'
    },
    {
        prop: 'interviewModeContent',
        label: '交互方式内容'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default DataObjectTable