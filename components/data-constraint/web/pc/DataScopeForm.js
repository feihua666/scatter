import DataScopeUrl from './DataScopeUrl.js'
import DataObjectUrl from './DataObjectUrl.js'
const DataScopeForm = [
    {
        DataScopeSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '数据范围约束编码',
            required: true,
        }
    },
    {
        DataScopeSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '数据范围约束名称',
            required: true,
        }
    },
    {
        DataScopeSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'dataObjectId',
        },
        element:{
            label: '数据对象',
            required: true,
            type: 'select',
            options: {
                datas: DataObjectUrl.list
            }
        }
    },
    {
        DataScopeSearchList: false,
        field: {
            name: 'constraintContent',
        },
        element:{
            label: '约束条件',
            type: 'textarea'
        }
    },
    {
        DataScopeSearchList: false,

        field: {
            name: 'isCustom',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否自定义',
            required: true,
        }
    },
    {
        DataScopeSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default DataScopeForm