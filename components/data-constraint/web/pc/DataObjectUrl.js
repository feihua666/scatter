const basePath = '' + '/data-object'
const DataObjectUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/DataObjectSearchList',
        add: '/DataObjectAdd',
        update: '/DataObjectUpdate',
    }
}
export default DataObjectUrl