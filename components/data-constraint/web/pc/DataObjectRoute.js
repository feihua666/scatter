import DataObjectUrl from './DataObjectUrl.js'

const DataObjectRoute = [
    {
        path: DataObjectUrl.router.searchList,
        component: () => import('./element/DataObjectSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DataObjectSearchList',
            name: '数据对象管理'
        }
    },
    {
        path: DataObjectUrl.router.add,
        component: () => import('./element/DataObjectAdd'),
        meta: {
            code:'DataObjectAdd',
            name: '数据对象添加'
        }
    },
    {
        path: DataObjectUrl.router.update,
        component: () => import('./element/DataObjectUpdate'),
        meta: {
            code:'DataObjectUpdate',
            name: '数据对象修改'
        }
    },
]
export default DataObjectRoute