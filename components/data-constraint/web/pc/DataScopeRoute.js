import DataScopeUrl from './DataScopeUrl.js'

const DataScopeRoute = [
    {
        path: DataScopeUrl.router.searchList,
        component: () => import('./element/DataScopeSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DataScopeSearchList',
            name: '数据范围约束管理'
        }
    },
    {
        path: DataScopeUrl.router.add,
        component: () => import('./element/DataScopeAdd'),
        meta: {
            code:'DataScopeAdd',
            name: '数据范围约束添加'
        }
    },
    {
        path: DataScopeUrl.router.update,
        component: () => import('./element/DataScopeUpdate'),
        meta: {
            code:'DataScopeUpdate',
            name: '数据范围约束修改'
        }
    },
]
export default DataScopeRoute