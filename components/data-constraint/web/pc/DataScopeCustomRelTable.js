const DataScopeCustomRelTable = [
    {
        prop: 'dataScopeId',
        label: '数据范围约束id'
    },
    {
        prop: 'dataId',
        label: '自定义数据id'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default DataScopeCustomRelTable