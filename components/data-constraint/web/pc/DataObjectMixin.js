import DataObjectForm from './DataObjectForm.js'
import DataObjectTable from './DataObjectTable.js'
import DataObjectUrl from './DataObjectUrl.js'
const DataObjectMixin = {
    computed: {
        DataObjectFormOptions() {
            return this.$stDynamicFormTools.options(DataObjectForm,this.$options.name)
        },
        DataObjectTableOptions() {
            return DataObjectTable
        },
        DataObjectUrl(){
            return DataObjectUrl
        }
    },
}
export default DataObjectMixin