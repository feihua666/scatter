const basePath = '' + '/data-scope'
const DataScopeUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/DataScopeSearchList',
        add: '/DataScopeAdd',
        update: '/DataScopeUpdate',
    }
}
export default DataScopeUrl