package scatter.dataconstraint.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 数据对象响应对象
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Setter
@Getter
@ApiModel(value="数据对象响应对象")
public class DataObjectVo extends BaseIdVo {

    @ApiModelProperty(value = "数据对象编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "数据对象名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "数据范围自定义时用来绑定自定义数据的url")
    private String dataCustomUrl;

    @ApiModelProperty(value = "自定义数据是否懒加载")
    private Boolean isDataLazy;

    @ApiModelProperty(value = "自定义数据交互方式，字典，想到的是表格和树形")
    private String interviewModeDictId;

    @ApiModelProperty(value = "数据交互方式内容")
    private String interviewModeContent;

    @ApiModelProperty(value = "描述")
    private String remark;

}
