package scatter.dataconstraint.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 数据范围约束分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Setter
@Getter
@ApiModel(value="数据范围约束分页表单对象")
public class DataScopePageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "数据范围约束编码，模糊查询")
    private String code;

    @Like
    @ApiModelProperty(value = "数据范围约束名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "数据对象id，外键")
    private String dataObjectId;

    @ApiModelProperty(value = "约束条件，暂时想到的用sql模板")
    private String constraintContent;

    @ApiModelProperty(value = "是否自定义，如果自定义=1，否则为0")
    private Boolean isCustom;

    @ApiModelProperty(value = "描述")
    private String remark;

}
