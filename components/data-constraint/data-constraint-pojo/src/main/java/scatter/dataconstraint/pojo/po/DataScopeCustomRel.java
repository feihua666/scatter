package scatter.dataconstraint.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据范围约束和自定义数据关系表
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_data_scope_custom_rel")
@ApiModel(value="DataScopeCustomRel对象", description="数据范围约束和自定义数据关系表")
public class DataScopeCustomRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_DATASCOPECUSTOMREL_BY_ID = "trans_datascopecustomrel_by_id_scatter.dataconstraint.pojo.po";

    @ApiModelProperty(value = "数据范围约束id，外键")
    private String dataScopeId;

    @ApiModelProperty(value = "自定义数据id，外键")
    private String dataId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
