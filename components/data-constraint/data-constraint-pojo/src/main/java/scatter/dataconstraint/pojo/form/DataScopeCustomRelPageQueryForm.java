package scatter.dataconstraint.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 数据范围约束和自定义数据分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Setter
@Getter
@ApiModel(value="数据范围约束和自定义数据分页表单对象")
public class DataScopeCustomRelPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "数据范围约束id，外键")
    private String dataScopeId;

    @ApiModelProperty(value = "自定义数据id，外键")
    private String dataId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
