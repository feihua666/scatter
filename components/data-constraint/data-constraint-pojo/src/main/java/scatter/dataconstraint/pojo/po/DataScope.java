package scatter.dataconstraint.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据范围约束表
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_data_scope")
@ApiModel(value="DataScope对象", description="数据范围约束表")
public class DataScope extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_DATASCOPE_BY_ID = "trans_datascope_by_id_scatter.dataconstraint.pojo.po";

    @ApiModelProperty(value = "数据范围约束编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "数据范围约束名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "数据对象id，外键")
    private String dataObjectId;

    @ApiModelProperty(value = "约束条件，暂时想到的用sql模板")
    private String constraintContent;

    @ApiModelProperty(value = "是否自定义，如果自定义=1，否则为0")
    private Boolean isCustom;

    @ApiModelProperty(value = "描述")
    private String remark;


}
