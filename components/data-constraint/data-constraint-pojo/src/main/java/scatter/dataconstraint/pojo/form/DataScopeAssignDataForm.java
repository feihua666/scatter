package scatter.dataconstraint.pojo.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
/**
 * <p>
 * 数据范围约束分配自定义数据表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@PropValid
@Setter
@Getter
@ApiModel(value="数据范围约束分配自定义数据表单对象")
public class DataScopeAssignDataForm extends BaseForm {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "数据范围约束id不能为空")
    @ApiModelProperty(value = "数据范围约束id")
    private String dataScopeId;

    @ApiModelProperty(value = "选择的自定义数据id")
    private List<String> checkedDataIds;

    @PropValid.DependCondition(message = "未选择的自定义数据id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的自定义数据id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedDataIds;

    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
