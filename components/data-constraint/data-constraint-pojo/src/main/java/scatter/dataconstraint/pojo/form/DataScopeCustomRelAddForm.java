package scatter.dataconstraint.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 数据范围约束和自定义数据添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Setter
@Getter
@ApiModel(value="数据范围约束和自定义数据添加表单对象")
public class DataScopeCustomRelAddForm extends BaseAddForm {

    @NotEmpty(message="数据范围约束id不能为空")
    @ApiModelProperty(value = "数据范围约束id，外键",required = true)
    private String dataScopeId;

    @NotEmpty(message="自定义数据id不能为空")
    @ApiModelProperty(value = "自定义数据id，外键",required = true)
    private String dataId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
