package scatter.dataconstraint.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 数据对象表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Component
@FeignClient(value = "DataObject-client")
public interface DataObjectClient {

}
