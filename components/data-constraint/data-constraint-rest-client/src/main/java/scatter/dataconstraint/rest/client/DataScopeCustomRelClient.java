package scatter.dataconstraint.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 数据范围约束和自定义数据关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-03-31
 */
@Component
@FeignClient(value = "DataScopeCustomRel-client")
public interface DataScopeCustomRelClient {

}
