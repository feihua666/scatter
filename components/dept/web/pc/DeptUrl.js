const basePath = '' + '/dept'
const DeptUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    masterUser: basePath + '/masteruser/getPage',
    router: {
        searchList: '/DeptSearchList',
        add: '/DeptAdd',
        update: '/DeptUpdate',
    }
}
export default DeptUrl