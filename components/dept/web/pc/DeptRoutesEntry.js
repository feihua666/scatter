import DeptRoute from './DeptRoute.js'
import DeptTreeNameRoute from './DeptTreeNameRoute.js'
import DeptTreeRoute from './DeptTreeRoute.js'

const DeptRoutesEntry = [
]
.concat(DeptRoute)
.concat(DeptTreeNameRoute)
.concat(DeptTreeRoute)

export default DeptRoutesEntry