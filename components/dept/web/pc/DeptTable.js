const DeptTable = [
    {
        prop: 'code',
        label: '部门编码'
    },
    {
        prop: 'name',
        label: '部门名称'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'masterUserNickname',
        label: '负责人'
    },
    {
        prop: 'compName',
        label: '归属公司'
    },
    {
        prop: 'isVirtual',
        label: '是否虚拟部门'
    },
    {
        prop: 'remark',
        label: '描述'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default DeptTable