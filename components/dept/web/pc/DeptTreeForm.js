import DeptTreeUrl from './DeptTreeUrl.js'
import DeptTreeNameUrl from './DeptTreeNameUrl.js'
import DeptUrl from './DeptUrl.js'

const DeptTreeForm = [
    {
        DeptTreeSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'deptId'
        },
        element:{
            label: '部门',
            required: true,
            type: 'cascader',
            options: {
                datas: DeptUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false,
                },
            },
        }
    },
    {
        DeptTreeSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'deptTreeNameId'
        },
        element:{
            label: '部门树名',
            required: true,
            type: 'select',
            options: {
                datas: DeptTreeNameUrl.list
            }
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: DeptTreeUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false,
                    label: 'deptName',
                },
            },
            label: '父级',
        }
    },
]
export default DeptTreeForm