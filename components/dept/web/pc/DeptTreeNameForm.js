const DeptTreeNameForm = [
    {

        DeptTreeNameSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        DeptTreeNameSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {

        DeptTreeNameSearchList: false,
        field: {
            name: 'isDefaultTree',
            valueDefault: false
        },
        element:{
            label: '默认部门名称',
            type: 'switch',
            required: true,
        }
    },
    {
        DeptTreeNameSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default DeptTreeNameForm