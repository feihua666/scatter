import DeptTreeNameUrl from './DeptTreeNameUrl.js'

const DeptTreeNameRoute = [
    {
        path: DeptTreeNameUrl.router.searchList,
        component: () => import('./element/DeptTreeNameSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DeptTreeNameSearchList',
            name: '组织树名称管理'
        }
    },
    {
        path: DeptTreeNameUrl.router.add,
        component: () => import('./element/DeptTreeNameAdd'),
        meta: {
            code:'DeptTreeNameAdd',
            name: '组织树名称添加'
        }
    },
    {
        path: DeptTreeNameUrl.router.update,
        component: () => import('./element/DeptTreeNameUpdate'),
        meta: {
            code:'DeptTreeNameUpdate',
            name: '组织树名称修改'
        }
    },
]
export default DeptTreeNameRoute