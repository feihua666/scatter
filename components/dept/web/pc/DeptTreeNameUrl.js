const basePath = '' + '/dept-tree-name'
const DeptTreeNameUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/DeptTreeNameSearchList',
        add: '/DeptTreeNameAdd',
        update: '/DeptTreeNameUpdate',
    }
}
export default DeptTreeNameUrl