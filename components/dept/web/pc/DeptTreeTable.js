const DeptTreeTable = [
    {
        prop: 'deptName',
        label: '部门名称'
    },
    {
        prop: 'deptTreeNameName',
        label: '部门树名称'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default DeptTreeTable