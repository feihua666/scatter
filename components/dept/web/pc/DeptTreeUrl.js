const basePath = '' + '/dept-tree'
const DeptTreeUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/DeptTreeSearchList',
        add: '/DeptTreeAdd',
        update: '/DeptTreeUpdate',
    }
}
export default DeptTreeUrl