import DeptTreeForm from './DeptTreeForm.js'
import DeptTreeTable from './DeptTreeTable.js'
import DeptTreeUrl from './DeptTreeUrl.js'

const DeptTreeMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(DeptTreeForm,this.$options.name)
        },
        computedTableOptions() {
            return DeptTreeTable
        },
        computedUrl(){
            return DeptTreeUrl
        }
    },
}
export default DeptTreeMixin