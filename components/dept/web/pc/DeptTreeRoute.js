import DeptTreeUrl from './DeptTreeUrl.js'

const DeptTreeRoute = [
    {
        path: DeptTreeUrl.router.searchList,
        component: () => import('./element/DeptTreeSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DeptTreeSearchList',
            name: '部门树管理'
        }
    },
    {
        path: DeptTreeUrl.router.add,
        component: () => import('./element/DeptTreeAdd'),
        meta: {
            code:'DeptTreeAdd',
            name: '部门树添加'
        }
    },
    {
        path: DeptTreeUrl.router.update,
        component: () => import('./element/DeptTreeUpdate'),
        meta: {
            code:'DeptTreeUpdate',
            name: '部门树修改'
        }
    },
]
export default DeptTreeRoute