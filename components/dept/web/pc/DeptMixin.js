import DeptForm from './DeptForm.js'
import DeptTable from './DeptTable.js'
import DeptUrl from './DeptUrl.js'
const DeptMixin = {
    computed: {
        DeptFormOptions() {
            return this.$stDynamicFormTools.options(DeptForm,this.$options.name)
        },
        DeptTableOptions() {
            return DeptTable
        },
        DeptUrl(){
            return DeptUrl
        }
    },
}
export default DeptMixin