import DeptUrl from './DeptUrl.js'

const DeptRoute = [
    {
        path: DeptUrl.router.searchList,
        component: () => import('./element/DeptSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DeptSearchList',
            name: '部门管理'
        }
    },
    {
        path: DeptUrl.router.add,
        component: () => import('./element/DeptAdd'),
        meta: {
            code:'DeptAdd',
            name: '部门添加'
        }
    },
    {
        path: DeptUrl.router.update,
        component: () => import('./element/DeptUpdate'),
        meta: {
            code:'DeptUpdate',
            name: '部门修改'
        }
    },
]
export default DeptRoute