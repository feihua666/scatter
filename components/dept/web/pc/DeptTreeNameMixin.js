import DeptTreeNameForm from './DeptTreeNameForm.js'
import DeptTreeNameTable from './DeptTreeNameTable.js'
import DeptTreeNameUrl from './DeptTreeNameUrl.js'

const DeptTreeNameMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(DeptTreeNameForm,this.$options.name)
        },
        computedTableOptions() {
            return DeptTreeNameTable
        },
        computedUrl(){
            return DeptTreeNameUrl
        }
    },
}
export default DeptTreeNameMixin