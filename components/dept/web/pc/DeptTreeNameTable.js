const DeptTreeNameTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'isDefaultTree',
        label: '默认部门名称'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default DeptTreeNameTable