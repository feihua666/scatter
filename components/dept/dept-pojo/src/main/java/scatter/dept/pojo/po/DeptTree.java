package scatter.dept.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门树表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_dept_tree")
@ApiModel(value="DeptTree对象", description="部门树表")
public class DeptTree extends BaseTreePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_DEPTTREE_BY_ID = "trans_depttree_by_id_scatter.dept.pojo.po";
    public static final String TRANS_DEPTTREE_DEPT_BY_ID = "trans_depttree_dept_by_id_scatter.dept.pojo.po";

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "部门树名id")
    private String deptTreeNameId;


}
