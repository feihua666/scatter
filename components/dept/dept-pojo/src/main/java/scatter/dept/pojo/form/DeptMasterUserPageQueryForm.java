package scatter.dept.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;

import javax.validation.constraints.NotEmpty;


/**
 * <p>
 * 部门负责人分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Setter
@Getter
@ApiModel(value="部门负责人分页表单对象")
public class DeptMasterUserPageQueryForm extends BasePageQueryForm {

    @NotEmpty(message = "用户昵称不能为空")
    @ApiModelProperty(value = "用户昵称")
    private String nickname;

}
