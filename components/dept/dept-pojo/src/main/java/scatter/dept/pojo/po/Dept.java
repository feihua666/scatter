package scatter.dept.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_dept")
@ApiModel(value="Dept对象", description="部门表")
public class Dept extends BaseTreePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_DEPT_BY_ID = "trans_dept_by_id_scatter.dept.pojo.po";
    // 翻译负责人昵称
    public static final String TRANS_DEPT_MASTER_USER_NICKNAME_BY_MASTER_USER_ID = "trans_dept_master_user_nickname_by_master_user_id";
    @ApiModelProperty(value = "部门编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "部门名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "类型,字典id")
    private String typeDictId;

    @ApiModelProperty(value = "负责人用户id，该id可用来填充审批人")
    private String masterUserId;

    @ApiModelProperty(value = "归属公司id")
    private String compId;

    @ApiModelProperty(value = "是否虚拟部门")
    private Boolean isVirtual;

    @ApiModelProperty(value = "描述")
    private String remark;


}
