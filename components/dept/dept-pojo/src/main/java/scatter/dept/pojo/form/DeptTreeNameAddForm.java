package scatter.dept.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 组织树名称添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="组织树名称添加表单对象")
public class DeptTreeNameAddForm extends BaseAddForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码,模糊查询",required = true)
    private String code;

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称,模糊查询",required = true)
    private String name;

    @NotNull(message="是否默认部门名称不能为空")
    @ApiModelProperty(value = "是否默认部门名称，只能有一个",required = true)
    private Boolean isDefaultTree;

    @ApiModelProperty(value = "描述")
    private String remark;

}
