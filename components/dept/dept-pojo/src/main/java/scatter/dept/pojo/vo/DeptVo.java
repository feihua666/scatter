package scatter.dept.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.comp.pojo.po.Comp;
import scatter.dept.pojo.po.Dept;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 部门响应对象
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Setter
@Getter
@ApiModel(value="部门响应对象")
public class DeptVo extends BaseIdVo {

    @ApiModelProperty(value = "部门编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "部门名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "类型,字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="name")
    @ApiModelProperty(value = "类型，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "负责人用户id，该id可用来填充审批人")
    private String masterUserId;

    @TransBy(type = Dept.TRANS_DEPT_MASTER_USER_NICKNAME_BY_MASTER_USER_ID,byFieldName = "masterUserId")
    @ApiModelProperty(value = "负责人用户昵称")
    private String masterUserNickname;

    @ApiModelProperty(value = "归属公司id")
    private String compId;


    @TransBy(type = Comp.TRANS_COMP_BY_ID,byFieldName = "compId",mapValueField="name")
    @ApiModelProperty(value = "归属公司名称")
    private String compName;

    @ApiModelProperty(value = "是否虚拟部门")
    private Boolean isVirtual;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;


    @TransBy(type = Dept.TRANS_DEPT_BY_ID,byFieldName = "parentId",mapValueField="name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
