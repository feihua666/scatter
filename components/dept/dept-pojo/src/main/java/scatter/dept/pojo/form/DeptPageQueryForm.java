package scatter.dept.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 部门分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Setter
@Getter
@ApiModel(value="部门分页表单对象")
public class DeptPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "部门编码,模糊查询")
    private String code;

    @Like
    @ApiModelProperty(value = "部门名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "类型,字典id")
    private String typeDictId;

    @ApiModelProperty(value = "负责人用户id，该id可用来填充审批人")
    private String masterUserId;

    @ApiModelProperty(value = "归属公司id")
    private String compId;

    @ApiModelProperty(value = "是否虚拟部门")
    private Boolean isVirtual;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
