package scatter.dept.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 部门树添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="部门树添加表单对象")
public class DeptTreeAddForm extends BaseAddForm {

    @NotEmpty(message="部门ID不能为空")
    @ApiModelProperty(value = "部门ID",required = true)
    private String deptId;

    @NotEmpty(message="部门树名id不能为空")
    @ApiModelProperty(value = "部门树名id",required = true)
    private String deptTreeNameId;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
