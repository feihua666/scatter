package scatter.dept.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 组织树名称响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="组织树名称响应对象")
public class DeptTreeNameVo extends BaseIdVo {

    @ApiModelProperty(value = "编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "是否默认部门名称，只能有一个")
    private Boolean isDefaultTree;

    @ApiModelProperty(value = "描述")
    private String remark;

}
