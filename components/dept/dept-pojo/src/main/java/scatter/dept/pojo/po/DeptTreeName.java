package scatter.dept.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 组织树名称表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_dept_tree_name")
@ApiModel(value="DeptTreeName对象", description="组织树名称表")
public class DeptTreeName extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_DEPTTREENAME_BY_ID = "trans_depttreename_by_id_scatter.dept.pojo.po";

    @ApiModelProperty(value = "编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "是否默认部门名称，只能有一个")
    private Boolean isDefaultTree;

    @ApiModelProperty(value = "描述")
    private String remark;


}
