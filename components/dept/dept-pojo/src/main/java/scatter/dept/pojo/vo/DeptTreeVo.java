package scatter.dept.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.pojo.po.DeptTreeName;


/**
 * <p>
 * 部门树响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="部门树响应对象")
public class DeptTreeVo extends BaseIdVo {

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @TransBy(type = Dept.TRANS_DEPT_BY_ID,byFieldName = "deptId",mapValueField = "name")
    @ApiModelProperty(value = "部门名称")
    private String deptName;


    @ApiModelProperty(value = "部门树名id")
    private String deptTreeNameId;


    @TransBy(type = DeptTreeName.TRANS_DEPTTREENAME_BY_ID,byFieldName = "deptTreeNameId",mapValueField = "name")
    @ApiModelProperty(value = "部门树名名称")
    private String deptTreeNameName;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @TransBy(type = DeptTree.TRANS_DEPTTREE_DEPT_BY_ID,byFieldName = "parentId",mapValueField = "name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
