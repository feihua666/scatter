package scatter.dept.pojo.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;

/**
 * Created by yangwei
 * Created at 2021/3/23 16:58
 */
@Setter
@Getter
@ApiModel(value="部门负责人响应对象")
public class DeptMasterUserVo extends BaseIdVo {

    private String nickname;
}
