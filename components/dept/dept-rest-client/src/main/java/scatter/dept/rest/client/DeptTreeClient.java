package scatter.dept.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 部门树表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
@FeignClient(value = "DeptTree-client")
public interface DeptTreeClient {

}
