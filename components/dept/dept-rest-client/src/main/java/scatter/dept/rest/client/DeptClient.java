package scatter.dept.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 部门表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Component
@FeignClient(value = "Dept-client")
public interface DeptClient {

}
