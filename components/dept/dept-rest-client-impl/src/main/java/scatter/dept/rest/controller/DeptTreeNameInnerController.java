package scatter.dept.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.dept.rest.service.IDeptTreeNameService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 组织树名称表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/dept-tree-name")
public class DeptTreeNameInnerController extends BaseInnerController<DeptTreeName> {
 @Autowired
 private IDeptTreeNameService deptTreeNameService;

 public IDeptTreeNameService getService(){
     return deptTreeNameService;
 }
}
