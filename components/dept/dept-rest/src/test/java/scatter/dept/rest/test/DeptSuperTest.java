package scatter.dept.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.form.DeptAddForm;
import scatter.dept.pojo.form.DeptUpdateForm;
import scatter.dept.pojo.form.DeptPageQueryForm;
import scatter.dept.rest.service.IDeptService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 部门 测试类基类
* </p>
*
* @author yw
* @since 2021-03-24
*/
public class DeptSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IDeptService deptService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return deptService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return deptService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Dept mockPo() {
        return JMockData.mock(Dept.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public DeptAddForm mockAddForm() {
        return JMockData.mock(DeptAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public DeptUpdateForm mockUpdateForm() {
        return JMockData.mock(DeptUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public DeptPageQueryForm mockPageQueryForm() {
        return JMockData.mock(DeptPageQueryForm.class, mockConfig);
    }
}