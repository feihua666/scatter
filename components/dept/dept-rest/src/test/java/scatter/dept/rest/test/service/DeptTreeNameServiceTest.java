package scatter.dept.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.dept.pojo.form.DeptTreeNameAddForm;
import scatter.dept.pojo.form.DeptTreeNameUpdateForm;
import scatter.dept.pojo.form.DeptTreeNamePageQueryForm;
import scatter.dept.rest.test.DeptTreeNameSuperTest;
import scatter.dept.rest.service.IDeptTreeNameService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 组织树名称 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class DeptTreeNameServiceTest extends DeptTreeNameSuperTest{

    @Autowired
    private IDeptTreeNameService deptTreeNameService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<DeptTreeName> pos = deptTreeNameService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}