package scatter.dept.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.dept.pojo.form.DeptTreeNameAddForm;
import scatter.dept.pojo.form.DeptTreeNameUpdateForm;
import scatter.dept.pojo.form.DeptTreeNamePageQueryForm;
import scatter.dept.rest.service.IDeptTreeNameService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 组织树名称 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class DeptTreeNameSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IDeptTreeNameService deptTreeNameService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return deptTreeNameService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return deptTreeNameService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public DeptTreeName mockPo() {
        return JMockData.mock(DeptTreeName.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public DeptTreeNameAddForm mockAddForm() {
        return JMockData.mock(DeptTreeNameAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public DeptTreeNameUpdateForm mockUpdateForm() {
        return JMockData.mock(DeptTreeNameUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public DeptTreeNamePageQueryForm mockPageQueryForm() {
        return JMockData.mock(DeptTreeNamePageQueryForm.class, mockConfig);
    }
}