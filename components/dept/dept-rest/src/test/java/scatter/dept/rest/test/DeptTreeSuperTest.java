package scatter.dept.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.pojo.form.DeptTreeAddForm;
import scatter.dept.pojo.form.DeptTreeUpdateForm;
import scatter.dept.pojo.form.DeptTreePageQueryForm;
import scatter.dept.rest.service.IDeptTreeService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 部门树 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class DeptTreeSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IDeptTreeService deptTreeService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return deptTreeService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return deptTreeService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public DeptTree mockPo() {
        return JMockData.mock(DeptTree.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public DeptTreeAddForm mockAddForm() {
        return JMockData.mock(DeptTreeAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public DeptTreeUpdateForm mockUpdateForm() {
        return JMockData.mock(DeptTreeUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public DeptTreePageQueryForm mockPageQueryForm() {
        return JMockData.mock(DeptTreePageQueryForm.class, mockConfig);
    }
}