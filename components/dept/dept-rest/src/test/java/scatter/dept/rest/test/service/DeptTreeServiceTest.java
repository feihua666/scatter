package scatter.dept.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.pojo.form.DeptTreeAddForm;
import scatter.dept.pojo.form.DeptTreeUpdateForm;
import scatter.dept.pojo.form.DeptTreePageQueryForm;
import scatter.dept.rest.test.DeptTreeSuperTest;
import scatter.dept.rest.service.IDeptTreeService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 部门树 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class DeptTreeServiceTest extends DeptTreeSuperTest{

    @Autowired
    private IDeptTreeService deptTreeService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<DeptTree> pos = deptTreeService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}