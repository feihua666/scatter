package scatter.dept.rest.test.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dept.rest.test.DeptTreeSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.pojo.form.DeptTreeAddForm;
import scatter.dept.pojo.vo.DeptTreeVo;
import scatter.dept.rest.DeptConfiguration;
/**
* <p>
* 部门树 前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
@AutoConfigureMockMvc
public class DeptTreeControllerTest extends DeptTreeSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = DeptConfiguration.CONTROLLER_BASE_PATH + "/dept-tree";

        // 请求表单
        DeptTreeAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        DeptTreeVo vo = JSONUtil.toBean(response.getContentAsString(), DeptTreeVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}