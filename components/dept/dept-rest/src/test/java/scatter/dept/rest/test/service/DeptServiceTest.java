package scatter.dept.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.form.DeptAddForm;
import scatter.dept.pojo.form.DeptUpdateForm;
import scatter.dept.pojo.form.DeptPageQueryForm;
import scatter.dept.rest.test.DeptSuperTest;
import scatter.dept.rest.service.IDeptService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 部门 服务测试类
* </p>
*
* @author yw
* @since 2021-03-24
*/
@SpringBootTest
public class DeptServiceTest extends DeptSuperTest{

    @Autowired
    private IDeptService deptService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Dept> pos = deptService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}