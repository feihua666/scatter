package scatter.dept.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.dept.pojo.po.DeptTreeName;
/**
 * <p>
 * 组织树名称表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IDeptTreeNameService extends IBaseService<DeptTreeName> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default DeptTreeName getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<DeptTreeName>lambdaQuery().eq(DeptTreeName::getCode, code));
    }

}
