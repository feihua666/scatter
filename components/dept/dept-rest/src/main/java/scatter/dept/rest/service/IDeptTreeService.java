package scatter.dept.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.dept.pojo.po.DeptTree;
/**
 * <p>
 * 部门树表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IDeptTreeService extends IBaseService<DeptTree> {


}
