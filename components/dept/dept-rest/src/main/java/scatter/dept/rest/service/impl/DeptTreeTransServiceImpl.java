package scatter.dept.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.rest.service.IDeptTreeService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门树翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
public class DeptTreeTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IDeptTreeService deptTreeService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,DeptTree.TRANS_DEPTTREE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,DeptTree.TRANS_DEPTTREE_BY_ID)) {
            DeptTree byId = deptTreeService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,DeptTree.TRANS_DEPTTREE_BY_ID)) {
            return deptTreeService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
