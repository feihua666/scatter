package scatter.dept.rest.masteruser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.SuperController;
import scatter.dept.pojo.form.DeptMasterUserPageQueryForm;
import scatter.dept.pojo.vo.DeptMasterUserVo;
import scatter.dept.rest.DeptConfiguration;

import javax.validation.Valid;

/**
 * 公司负责人相关
 * Created by yangwei
 * Created at 2021/3/23 16:56
 */
@RestController
@RequestMapping(DeptConfiguration.CONTROLLER_BASE_PATH + "/dept")
@Api(tags = "公司")
public class DeptMasterUserController extends SuperController {

    @Autowired(required = false)
    private IDeptMasterUserService iMasterUserService;


    @ApiOperation("分页查询公司负责人")
    @PreAuthorize("hasAuthority('Comp:masteruser:getPage')")
    @GetMapping("/masteruser/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DeptMasterUserVo> getPage(@Valid DeptMasterUserPageQueryForm listPageForm) {
        Assert.notNull(iMasterUserService,"你必须实现接口 scatter.comp.rest.masteruser.IMasterUserService 来查询用户");
        return iMasterUserService.getPage(listPageForm);
    }
}
