package scatter.dept.rest.masteruser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import scatter.dept.pojo.form.DeptMasterUserPageQueryForm;
import scatter.dept.pojo.vo.DeptMasterUserVo;

/**
 * 为了解藕这里定义一个service，实现留给扩展
 * Created by yangwei
 * Created at 2021/3/23 17:01
 */
public interface IDeptMasterUserService {

    IPage<DeptMasterUserVo> getPage(DeptMasterUserPageQueryForm listPageForm);
}
