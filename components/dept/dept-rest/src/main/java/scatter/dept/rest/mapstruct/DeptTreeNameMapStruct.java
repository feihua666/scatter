package scatter.dept.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dept.pojo.form.DeptTreeNameAddForm;
import scatter.dept.pojo.form.DeptTreeNamePageQueryForm;
import scatter.dept.pojo.form.DeptTreeNameUpdateForm;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.dept.pojo.vo.DeptTreeNameVo;

/**
 * <p>
 * 组织树名称 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeptTreeNameMapStruct extends IBaseVoMapStruct<DeptTreeName, DeptTreeNameVo>,
        IBaseAddFormMapStruct<DeptTreeName,DeptTreeNameAddForm>,
        IBaseUpdateFormMapStruct<DeptTreeName,DeptTreeNameUpdateForm>,
        IBaseQueryFormMapStruct<DeptTreeName,DeptTreeNamePageQueryForm> {
    DeptTreeNameMapStruct INSTANCE = Mappers.getMapper( DeptTreeNameMapStruct.class );

}
