package scatter.dept.rest.mapper;

import scatter.dept.pojo.po.DeptTreeName;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 组织树名称表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface DeptTreeNameMapper extends IBaseMapper<DeptTreeName> {

}
