package scatter.dept.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.rest.service.IDeptTreeService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 部门树表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/dept-tree")
public class DeptTreeInnerController extends BaseInnerController<DeptTree> {
 @Autowired
 private IDeptTreeService deptTreeService;

 public IDeptTreeService getService(){
     return deptTreeService;
 }
}
