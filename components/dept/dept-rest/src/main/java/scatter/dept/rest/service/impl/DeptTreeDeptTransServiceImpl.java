package scatter.dept.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.rest.service.IDeptService;
import scatter.dept.rest.service.IDeptTreeService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门树对部门的翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
public class DeptTreeDeptTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IDeptTreeService deptTreeService;
    @Autowired
    private IDeptService deptService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,DeptTree.TRANS_DEPTTREE_DEPT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,DeptTree.TRANS_DEPTTREE_DEPT_BY_ID)) {
            DeptTree byId = deptTreeService.getById(key);
            if (byId != null) {
                Dept byId1 = deptService.getById(byId.getDeptId());
                return new TransResult(byId1,key);
            }

        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,DeptTree.TRANS_DEPTTREE_DEPT_BY_ID)) {
            List<DeptTree> deptTrees = deptTreeService.listByIds(keys);
            List<Dept> depts = deptService.listByIds(deptTrees.stream().map(DeptTree::getDeptId).collect(Collectors.toSet()));
            return depts.stream().map(item->new TransResult<Object, String>(item,
                    deptTrees.stream().filter(tree->isEqual(item.getId(),tree.getDeptId())).findFirst().map(DeptTree::getId).orElse(null)
            ))
                    .collect(Collectors.toList());
        }
        return null;
    }
}
