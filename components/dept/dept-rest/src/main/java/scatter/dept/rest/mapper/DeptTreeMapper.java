package scatter.dept.rest.mapper;

import scatter.dept.pojo.po.DeptTree;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 部门树表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface DeptTreeMapper extends IBaseMapper<DeptTree> {

}
