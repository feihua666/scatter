package scatter.dept.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.dept.pojo.po.Dept;
import scatter.common.rest.service.IBaseService;
import scatter.dept.pojo.form.DeptAddForm;
import scatter.dept.pojo.form.DeptUpdateForm;
import scatter.dept.pojo.form.DeptPageQueryForm;
import java.util.List;
/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
public interface IDeptService extends IBaseService<Dept> {


    /**
     * 根据部门编码查询
     * @param code
     * @return
     */
    default Dept getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Dept>lambdaQuery().eq(Dept::getCode, code));
    }

}
