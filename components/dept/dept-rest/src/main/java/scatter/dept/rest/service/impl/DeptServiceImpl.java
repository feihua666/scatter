package scatter.dept.rest.service.impl;

import scatter.dept.pojo.po.Dept;
import scatter.dept.rest.mapper.DeptMapper;
import scatter.dept.rest.service.IDeptService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.dept.pojo.form.DeptAddForm;
import scatter.dept.pojo.form.DeptUpdateForm;
import scatter.dept.pojo.form.DeptPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Service
@Transactional
public class DeptServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<DeptMapper, Dept, DeptAddForm, DeptUpdateForm, DeptPageQueryForm> implements IDeptService {
    @Override
    public void preAdd(DeptAddForm addForm,Dept po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 部门编码已存在不能添加
            assertByColumn(addForm.getCode(),Dept::getCode,false);
        }

    }

    @Override
    public void preUpdate(DeptUpdateForm updateForm,Dept po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            Dept byId = getById(updateForm.getId());
            // 如果部门编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 部门编码已存在不能修改
                assertByColumn(updateForm.getCode(),Dept::getCode,false);
            }
        }

    }
}
