package scatter.dept.rest.mapper;

import scatter.dept.pojo.po.Dept;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
public interface DeptMapper extends IBaseMapper<Dept> {

}
