package scatter.dept.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.vo.DeptVo;
import scatter.dept.pojo.form.DeptAddForm;
import scatter.dept.pojo.form.DeptUpdateForm;
import scatter.dept.pojo.form.DeptPageQueryForm;

/**
 * <p>
 * 部门 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeptMapStruct extends IBaseVoMapStruct<Dept, DeptVo>,
                                  IBaseAddFormMapStruct<Dept,DeptAddForm>,
                                  IBaseUpdateFormMapStruct<Dept,DeptUpdateForm>,
                                  IBaseQueryFormMapStruct<Dept,DeptPageQueryForm>{
    DeptMapStruct INSTANCE = Mappers.getMapper( DeptMapStruct.class );

}
