package scatter.dept.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.dept.pojo.form.DeptTreeNameAddForm;
import scatter.dept.pojo.form.DeptTreeNamePageQueryForm;
import scatter.dept.pojo.form.DeptTreeNameUpdateForm;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.dept.rest.mapper.DeptTreeNameMapper;
import scatter.dept.rest.service.IDeptTreeNameService;
/**
 * <p>
 * 组织树名称表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class DeptTreeNameServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<DeptTreeNameMapper, DeptTreeName, DeptTreeNameAddForm, DeptTreeNameUpdateForm, DeptTreeNamePageQueryForm> implements IDeptTreeNameService {
    @Override
    public void preAdd(DeptTreeNameAddForm addForm,DeptTreeName po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),DeptTreeName::getCode,false);
        }

    }

    @Override
    public void preUpdate(DeptTreeNameUpdateForm updateForm,DeptTreeName po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            DeptTreeName byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),DeptTreeName::getCode,false);
            }
        }

    }
}
