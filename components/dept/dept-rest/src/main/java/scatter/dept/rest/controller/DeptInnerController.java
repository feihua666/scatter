package scatter.dept.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dept.rest.DeptConfiguration;
import scatter.dept.pojo.po.Dept;
import scatter.dept.rest.service.IDeptService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 部门表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@RestController
@RequestMapping(DeptConfiguration.CONTROLLER_BASE_PATH + "/inner/dept")
public class DeptInnerController extends BaseInnerController<Dept> {
 @Autowired
 private IDeptService deptService;

 public IDeptService getService(){
     return deptService;
 }
}
