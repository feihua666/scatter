package scatter.dept.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.dept.rest.DeptConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.dept.pojo.vo.DeptTreeNameVo;
import scatter.dept.pojo.form.DeptTreeNameAddForm;
import scatter.dept.pojo.form.DeptTreeNameUpdateForm;
import scatter.dept.pojo.form.DeptTreeNamePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 组织树名称表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(DeptConfiguration.CONTROLLER_BASE_PATH + "/dept-tree-name")
@Api(tags = "组织树名称")
public class DeptTreeNameController extends BaseAddUpdateQueryFormController<DeptTreeName, DeptTreeNameVo, DeptTreeNameAddForm, DeptTreeNameUpdateForm, DeptTreeNamePageQueryForm> {


     @Override
	 @ApiOperation("添加组织树名称")
     @PreAuthorize("hasAuthority('DeptTreeName:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DeptTreeNameVo add(@RequestBody @Valid DeptTreeNameAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询组织树名称")
     @PreAuthorize("hasAuthority('DeptTreeName:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public DeptTreeNameVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除组织树名称")
     @PreAuthorize("hasAuthority('DeptTreeName:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新组织树名称")
     @PreAuthorize("hasAuthority('DeptTreeName:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DeptTreeNameVo update(@RequestBody @Valid DeptTreeNameUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询组织树名称")
    @PreAuthorize("hasAuthority('DeptTreeName:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<DeptTreeNameVo> getList(DeptTreeNamePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询组织树名称")
    @PreAuthorize("hasAuthority('DeptTreeName:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DeptTreeNameVo> getPage(DeptTreeNamePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
