package scatter.dept.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.dept.rest.DeptConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.pojo.vo.DeptTreeVo;
import scatter.dept.pojo.form.DeptTreeAddForm;
import scatter.dept.pojo.form.DeptTreeUpdateForm;
import scatter.dept.pojo.form.DeptTreePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 部门树表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(DeptConfiguration.CONTROLLER_BASE_PATH + "/dept-tree")
@Api(tags = "部门树")
public class DeptTreeController extends BaseAddUpdateQueryFormController<DeptTree, DeptTreeVo, DeptTreeAddForm, DeptTreeUpdateForm, DeptTreePageQueryForm> {


     @Override
	 @ApiOperation("添加部门树")
     @PreAuthorize("hasAuthority('DeptTree:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DeptTreeVo add(@RequestBody @Valid DeptTreeAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询部门树")
     @PreAuthorize("hasAuthority('DeptTree:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public DeptTreeVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除部门树")
     @PreAuthorize("hasAuthority('DeptTree:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新部门树")
     @PreAuthorize("hasAuthority('DeptTree:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DeptTreeVo update(@RequestBody @Valid DeptTreeUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询部门树")
    @PreAuthorize("hasAuthority('DeptTree:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<DeptTreeVo> getList(DeptTreePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询部门树")
    @PreAuthorize("hasAuthority('DeptTree:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DeptTreeVo> getPage(DeptTreePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
