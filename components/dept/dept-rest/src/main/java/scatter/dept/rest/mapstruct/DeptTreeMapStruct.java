package scatter.dept.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dept.pojo.form.DeptTreeAddForm;
import scatter.dept.pojo.form.DeptTreePageQueryForm;
import scatter.dept.pojo.form.DeptTreeUpdateForm;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.pojo.vo.DeptTreeVo;

/**
 * <p>
 * 部门树 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeptTreeMapStruct extends IBaseVoMapStruct<DeptTree, DeptTreeVo>,
        IBaseAddFormMapStruct<DeptTree,DeptTreeAddForm>,
        IBaseUpdateFormMapStruct<DeptTree,DeptTreeUpdateForm>,
        IBaseQueryFormMapStruct<DeptTree,DeptTreePageQueryForm> {
    DeptTreeMapStruct INSTANCE = Mappers.getMapper( DeptTreeMapStruct.class );

}
