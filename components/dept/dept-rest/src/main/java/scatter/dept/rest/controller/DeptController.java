package scatter.dept.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.dept.rest.DeptConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.vo.DeptVo;
import scatter.dept.pojo.form.DeptAddForm;
import scatter.dept.pojo.form.DeptUpdateForm;
import scatter.dept.pojo.form.DeptPageQueryForm;
import scatter.dept.rest.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Api(tags = "部门相关接口")
@RestController
@RequestMapping(DeptConfiguration.CONTROLLER_BASE_PATH + "/dept")
public class DeptController extends BaseAddUpdateQueryFormController<Dept, DeptVo, DeptAddForm, DeptUpdateForm, DeptPageQueryForm> {
    @Autowired
    private IDeptService iDeptService;

     @Override
	 @ApiOperation("添加部门")
     @PreAuthorize("hasAuthority('Dept:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DeptVo add(@RequestBody @Valid DeptAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询部门")
     @PreAuthorize("hasAuthority('Dept:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public DeptVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除部门")
     @PreAuthorize("hasAuthority('Dept:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新部门")
     @PreAuthorize("hasAuthority('Dept:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public DeptVo update(@RequestBody @Valid DeptUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询部门")
    @PreAuthorize("hasAuthority('Dept:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<DeptVo> getList(DeptPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询部门")
    @PreAuthorize("hasAuthority('Dept:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<DeptVo> getPage(DeptPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
