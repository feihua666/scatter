package scatter.dept.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.dept.pojo.form.DeptTreeAddForm;
import scatter.dept.pojo.form.DeptTreePageQueryForm;
import scatter.dept.pojo.form.DeptTreeUpdateForm;
import scatter.dept.pojo.po.DeptTree;
import scatter.dept.rest.mapper.DeptTreeMapper;
import scatter.dept.rest.service.IDeptTreeService;
/**
 * <p>
 * 部门树表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class DeptTreeServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<DeptTreeMapper, DeptTree, DeptTreeAddForm, DeptTreeUpdateForm, DeptTreePageQueryForm> implements IDeptTreeService {

}
