package scatter.dept.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.dept.pojo.po.Dept;
import scatter.dept.rest.service.IDeptService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-03-24
 */
@Component
public class DeptTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IDeptService deptService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Dept.TRANS_DEPT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Dept.TRANS_DEPT_BY_ID)) {
            Dept byId = deptService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Dept.TRANS_DEPT_BY_ID)) {
            return deptService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
