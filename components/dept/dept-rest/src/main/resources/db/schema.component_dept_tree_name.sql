DROP TABLE IF EXISTS component_dept_tree_name;
CREATE TABLE `component_dept_tree_name` (
  `id` varchar(20) NOT NULL COMMENT '主键ID',
  `code` varchar(100) NOT NULL COMMENT '编码,模糊查询',
  `name` varchar(100) NOT NULL COMMENT '名称,模糊查询',
  `is_default_tree` tinyint(1) NOT NULL COMMENT '是否默认部门名称，只能有一个',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='组织树名称表';
