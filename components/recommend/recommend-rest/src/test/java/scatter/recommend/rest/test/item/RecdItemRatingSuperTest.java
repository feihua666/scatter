package scatter.recommend.rest.test.item;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.recommend.pojo.item.form.RecdItemRatingAddForm;
import scatter.recommend.pojo.item.form.RecdItemRatingUpdateForm;
import scatter.recommend.pojo.item.form.RecdItemRatingPageQueryForm;
import scatter.recommend.rest.item.service.IRecdItemRatingService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 物品评分 测试类基类
* </p>
*
* @author yw
* @since 2021-09-29
*/
public class RecdItemRatingSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IRecdItemRatingService recdItemRatingService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return recdItemRatingService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return recdItemRatingService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public RecdItemRating mockPo() {
        return JMockData.mock(RecdItemRating.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public RecdItemRatingAddForm mockAddForm() {
        return JMockData.mock(RecdItemRatingAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public RecdItemRatingUpdateForm mockUpdateForm() {
        return JMockData.mock(RecdItemRatingUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public RecdItemRatingPageQueryForm mockPageQueryForm() {
        return JMockData.mock(RecdItemRatingPageQueryForm.class, mockConfig);
    }
}