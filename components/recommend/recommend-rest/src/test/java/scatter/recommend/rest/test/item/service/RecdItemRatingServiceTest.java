package scatter.recommend.rest.test.item.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.recommend.pojo.item.form.RecdItemRatingAddForm;
import scatter.recommend.pojo.item.form.RecdItemRatingUpdateForm;
import scatter.recommend.pojo.item.form.RecdItemRatingPageQueryForm;
import scatter.recommend.rest.test.item.RecdItemRatingSuperTest;
import scatter.recommend.rest.item.service.IRecdItemRatingService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 物品评分 服务测试类
* </p>
*
* @author yw
* @since 2021-09-29
*/
@SpringBootTest
public class RecdItemRatingServiceTest extends RecdItemRatingSuperTest{

    @Autowired
    private IRecdItemRatingService recdItemRatingService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<RecdItemRating> pos = recdItemRatingService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}