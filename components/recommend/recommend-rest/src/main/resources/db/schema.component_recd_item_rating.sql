DROP TABLE IF EXISTS component_recd_item_rating;
CREATE TABLE `component_recd_item_rating` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `item_id` varchar(20) NOT NULL COMMENT '数据项id',
  `rating` int NOT NULL COMMENT '评分，0-100',
  `item_type_dict_id` varchar(20) NOT NULL COMMENT '商品类型，字典id，视频、图片等类型',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='物品评分表';
