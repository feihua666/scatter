package scatter.recommend.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.recommend.rest.recommend.impl.DefaultIRecommendFrameworkImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.recommend.rest.**.mapper")
public class RecommendConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "recommend";

    @Bean
    @ConditionalOnMissingBean
    public DefaultIRecommendFrameworkImpl DefaultIRecommendFrameworkImpl(){
        return new DefaultIRecommendFrameworkImpl();
    }


}
