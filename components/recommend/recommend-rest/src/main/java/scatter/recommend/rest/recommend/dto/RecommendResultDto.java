package scatter.recommend.rest.recommend.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 推荐结果
 * </p>
 *
 * @author yangwei
 * @since 2021-08-28 13:01
 */
@Getter
@Setter
@Accessors(chain = true)
public class RecommendResultDto {
	/**
	 * 属性id
	 * 也可以认为是商品id
	 */
	private String itemId;

	/**
	 * 权重
	 * 也可以认为是对商品的偏好程度
	 */
	private double weight;

	/**
	 * 推荐器的标识，可以是名称
	 * 用来标识该推荐项是来自哪个推荐器
	 */
	private String recommender;
}
