package scatter.recommend.rest.recommend;

import scatter.recommend.rest.recommend.dto.RecommendResultDto;

import java.util.List;

/**
 * <p>
 * 推荐器
 * </p>
 *
 * @author yangwei
 * @since 2021-08-28 13:02
 */
public interface IRecommender {



	/**
	 * 推荐
	 * @param userId 用户id
	 * @param recommendNum 推荐数量
	 * @param recommendNum 推荐类别
	 * @return
	 */
	List<RecommendResultDto> recommend(String userId, int recommendNum,String category);
}
