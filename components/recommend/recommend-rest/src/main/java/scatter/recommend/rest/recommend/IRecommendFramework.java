package scatter.recommend.rest.recommend;

import scatter.recommend.rest.recommend.dto.RecommendResultDto;

import java.util.List;

/**
 * <p>
 * 推荐框架
 * 主要用来做系统推荐，比如：基排行的、基于内容的、基于模型的、CF等多种方式
 * </p>
 *
 * @author yangwei
 * @since 2021-08-28 12:55
 */
public interface IRecommendFramework {


	/**
	 * 推荐
	 * @param userId 用户id
	 * @param recommendNum 推荐数量
	 * @param category 推荐类别
	 * @return
	 */
	List<RecommendResultDto> recommend(String userId, int recommendNum,String category);
}
