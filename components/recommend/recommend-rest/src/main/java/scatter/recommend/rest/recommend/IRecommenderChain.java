package scatter.recommend.rest.recommend;

import scatter.recommend.rest.recommend.dto.RecommendResultDto;

import java.util.List;

/**
 * <p>
 * 推荐器链
 * </p>
 *
 * @author yangwei
 * @since 2021-08-28 13:33
 */
public interface IRecommenderChain {


	/**
	 * 进行推荐
	 * @param userId 推荐用户的id
	 * @param recommendNum 需要推荐的数量
	 * @param result 推荐的结果容器
	 */
	void doRecommend(String userId, int recommendNum,List<RecommendResultDto> result);

}
