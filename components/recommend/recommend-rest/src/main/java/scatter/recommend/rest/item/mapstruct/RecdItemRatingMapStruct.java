package scatter.recommend.rest.item.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.recommend.pojo.item.vo.RecdItemRatingVo;
import scatter.recommend.pojo.item.form.RecdItemRatingAddForm;
import scatter.recommend.pojo.item.form.RecdItemRatingUpdateForm;
import scatter.recommend.pojo.item.form.RecdItemRatingPageQueryForm;

/**
 * <p>
 * 物品评分 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RecdItemRatingMapStruct extends IBaseVoMapStruct<RecdItemRating, RecdItemRatingVo>,
                                  IBaseAddFormMapStruct<RecdItemRating,RecdItemRatingAddForm>,
                                  IBaseUpdateFormMapStruct<RecdItemRating,RecdItemRatingUpdateForm>,
                                  IBaseQueryFormMapStruct<RecdItemRating,RecdItemRatingPageQueryForm>{
    RecdItemRatingMapStruct INSTANCE = Mappers.getMapper( RecdItemRatingMapStruct.class );

}
