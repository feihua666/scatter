package scatter.recommend.rest.recommend.impl.recommender;

import org.springframework.stereotype.Component;
import scatter.recommend.rest.recommend.IRecommender;
import scatter.recommend.rest.recommend.dto.RecommendResultDto;

import java.util.List;

/**
 * <p>
 * 分享排名推荐
 * </p>
 *
 * @author yangwei
 * @since 2021-08-28 22:26
 */
@Component
public class ShareRankRecommender implements IRecommender {
	@Override
	public List<RecommendResultDto> recommend(String userId, int recommendNum, String category) {
		return null;
	}
}
