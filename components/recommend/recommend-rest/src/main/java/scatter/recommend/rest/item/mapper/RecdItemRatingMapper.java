package scatter.recommend.rest.item.mapper;

import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 物品评分表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
public interface RecdItemRatingMapper extends IBaseMapper<RecdItemRating> {

}
