package scatter.recommend.rest.item.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.common.rest.service.IBaseService;
import scatter.recommend.pojo.item.form.RecdItemRatingAddForm;
import scatter.recommend.pojo.item.form.RecdItemRatingUpdateForm;
import scatter.recommend.pojo.item.form.RecdItemRatingPageQueryForm;
import java.util.List;
/**
 * <p>
 * 物品评分表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
public interface IRecdItemRatingService extends IBaseService<RecdItemRating> {


    /**
     * 根据数据项id查询
     * @param itemId
     * @return
     */
    default RecdItemRating getByItemId(String itemId) {
        Assert.hasText(itemId,"itemId不能为空");
        return getOne(Wrappers.<RecdItemRating>lambdaQuery().eq(RecdItemRating::getItemId, itemId));
    }

}
