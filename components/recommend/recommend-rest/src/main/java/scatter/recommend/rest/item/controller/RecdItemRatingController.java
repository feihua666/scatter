package scatter.recommend.rest.item.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.recommend.rest.RecommendConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.recommend.pojo.item.vo.RecdItemRatingVo;
import scatter.recommend.pojo.item.form.RecdItemRatingAddForm;
import scatter.recommend.pojo.item.form.RecdItemRatingUpdateForm;
import scatter.recommend.pojo.item.form.RecdItemRatingPageQueryForm;
import scatter.recommend.rest.item.service.IRecdItemRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 物品评分表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Api(tags = "物品评分相关接口")
@RestController
@RequestMapping(RecommendConfiguration.CONTROLLER_BASE_PATH + "/item/recd-item-rating")
public class RecdItemRatingController extends BaseAddUpdateQueryFormController<RecdItemRating, RecdItemRatingVo, RecdItemRatingAddForm, RecdItemRatingUpdateForm, RecdItemRatingPageQueryForm> {
    @Autowired
    private IRecdItemRatingService iRecdItemRatingService;

    @ApiOperation("添加物品评分")
    @PreAuthorize("hasAuthority('RecdItemRating:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public RecdItemRatingVo add(@RequestBody @Valid RecdItemRatingAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询物品评分")
    @PreAuthorize("hasAuthority('RecdItemRating:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public RecdItemRatingVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除物品评分")
    @PreAuthorize("hasAuthority('RecdItemRating:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新物品评分")
    @PreAuthorize("hasAuthority('RecdItemRating:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public RecdItemRatingVo update(@RequestBody @Valid RecdItemRatingUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询物品评分")
    @PreAuthorize("hasAuthority('RecdItemRating:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<RecdItemRatingVo> getList(RecdItemRatingPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询物品评分")
    @PreAuthorize("hasAuthority('RecdItemRating:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<RecdItemRatingVo> getPage(RecdItemRatingPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
