package scatter.recommend.rest.recommend.impl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.recommend.rest.recommend.IRecommender;
import scatter.recommend.rest.recommend.IRecommenderChain;

import java.util.List;

/**
 * <p>
 * 默认的推荐框架实现
 * </p>
 *
 * @author yangwei
 * @since 2021-08-28 14:00
 */
public class DefaultIRecommendFrameworkImpl extends AbstractRecommendFramework{

	@Autowired
	private List<IRecommender> recommenderList;


	@Override
	protected IRecommenderChain newRecommenderChain(String category) {
		return new CompositeRecommenderChain(recommenderList,category);
	}
}
