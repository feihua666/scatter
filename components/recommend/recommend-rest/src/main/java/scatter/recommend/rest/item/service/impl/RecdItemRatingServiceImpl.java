package scatter.recommend.rest.item.service.impl;

import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.recommend.rest.item.mapper.RecdItemRatingMapper;
import scatter.recommend.rest.item.service.IRecdItemRatingService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.recommend.pojo.item.form.RecdItemRatingAddForm;
import scatter.recommend.pojo.item.form.RecdItemRatingUpdateForm;
import scatter.recommend.pojo.item.form.RecdItemRatingPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 物品评分表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Service
@Transactional
public class RecdItemRatingServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<RecdItemRatingMapper, RecdItemRating, RecdItemRatingAddForm, RecdItemRatingUpdateForm, RecdItemRatingPageQueryForm> implements IRecdItemRatingService {
    @Override
    public void preAdd(RecdItemRatingAddForm addForm,RecdItemRating po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getItemId())) {
            // 数据项id已存在不能添加
            assertByColumn(addForm.getItemId(),RecdItemRating::getItemId,false);
        }

    }

    @Override
    public void preUpdate(RecdItemRatingUpdateForm updateForm,RecdItemRating po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getItemId())) {
            RecdItemRating byId = getById(updateForm.getId());
            // 如果数据项id有改动
            if (!isEqual(updateForm.getItemId(), byId.getItemId())) {
                // 数据项id已存在不能修改
                assertByColumn(updateForm.getItemId(),RecdItemRating::getItemId,false);
            }
        }

    }
}
