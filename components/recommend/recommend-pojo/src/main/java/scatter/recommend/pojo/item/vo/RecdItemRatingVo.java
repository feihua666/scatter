package scatter.recommend.pojo.item.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 物品评分响应对象
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Setter
@Getter
@ApiModel(value="物品评分响应对象")
public class RecdItemRatingVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "数据项id")
    private String itemId;

    @ApiModelProperty(value = "评分，0-100")
    private Integer rating;

    @ApiModelProperty(value = "商品类型，字典id，视频、图片等类型")
    private String itemTypeDictId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
