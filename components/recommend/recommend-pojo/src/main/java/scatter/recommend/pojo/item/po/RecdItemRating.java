package scatter.recommend.pojo.item.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 物品评分表
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_recd_item_rating")
@ApiModel(value="RecdItemRating对象", description="物品评分表")
public class RecdItemRating extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_RECDITEMRATING_BY_ID = "trans_recditemrating_by_id_scatter.recommend.pojo.item.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "数据项id")
    private String itemId;

    @ApiModelProperty(value = "评分，0-100")
    private Integer rating;

    @ApiModelProperty(value = "商品类型，字典id，视频、图片等类型")
    private String itemTypeDictId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


}
