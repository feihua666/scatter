package scatter.recommend.pojo.item.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 物品评分更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Setter
@Getter
@ApiModel(value="物品评分更新表单对象")
public class RecdItemRatingUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="数据项id不能为空")
    @ApiModelProperty(value = "数据项id",required = true)
    private String itemId;

    @NotNull(message="评分不能为空")
    @ApiModelProperty(value = "评分，0-100",required = true)
    private Integer rating;

    @NotEmpty(message="商品类型不能为空")
    @ApiModelProperty(value = "商品类型，字典id，视频、图片等类型",required = true)
    private String itemTypeDictId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
