package scatter.recommend.rest.item.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.recommend.rest.RecommendConfiguration;
import scatter.recommend.pojo.item.po.RecdItemRating;
import scatter.recommend.rest.item.service.IRecdItemRatingService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 物品评分表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@RestController
@RequestMapping(RecommendConfiguration.CONTROLLER_BASE_PATH + "/inner/item/recd-item-rating")
public class RecdItemRatingInnerController extends BaseInnerController<RecdItemRating> {
 @Autowired
 private IRecdItemRatingService recdItemRatingService;

 public IRecdItemRatingService getService(){
     return recdItemRatingService;
 }
}
