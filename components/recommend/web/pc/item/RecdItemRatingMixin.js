import RecdItemRatingForm from './RecdItemRatingForm.js'
import RecdItemRatingTable from './RecdItemRatingTable.js'
import RecdItemRatingUrl from './RecdItemRatingUrl.js'
const RecdItemRatingMixin = {
    computed: {
        RecdItemRatingFormOptions() {
            return this.$stDynamicFormTools.options(RecdItemRatingForm,this.$options.name)
        },
        RecdItemRatingTableOptions() {
            return RecdItemRatingTable
        },
        RecdItemRatingUrl(){
            return RecdItemRatingUrl
        }
    },
}
export default RecdItemRatingMixin