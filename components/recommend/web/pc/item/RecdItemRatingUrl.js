const basePath = '' + '/item/recd-item-rating'
const RecdItemRatingUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/RecdItemRatingSearchList',
        add: '/RecdItemRatingAdd',
        update: '/RecdItemRatingUpdate',
    }
}
export default RecdItemRatingUrl