import RecdItemRatingUrl from './RecdItemRatingUrl.js'
const RecdItemRatingForm = [
    {
        RecdItemRatingSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        RecdItemRatingSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'itemId',
        },
        element:{
            label: '数据项id',
            required: true,
        }
    },
    {
        RecdItemRatingSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'rating',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '评分',
            required: true,
        }
    },
    {
        RecdItemRatingSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'itemTypeDictId',
        },
        element:{
            label: '商品类型',
            required: true,
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },

]
export default RecdItemRatingForm