import RecdItemRatingUrl from './RecdItemRatingUrl.js'

const RecdItemRatingRoute = [
    {
        path: RecdItemRatingUrl.router.searchList,
        component: () => import('./element/RecdItemRatingSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'RecdItemRatingSearchList',
            name: '物品评分管理'
        }
    },
    {
        path: RecdItemRatingUrl.router.add,
        component: () => import('./element/RecdItemRatingAdd'),
        meta: {
            code:'RecdItemRatingAdd',
            name: '物品评分添加'
        }
    },
    {
        path: RecdItemRatingUrl.router.update,
        component: () => import('./element/RecdItemRatingUpdate'),
        meta: {
            code:'RecdItemRatingUpdate',
            name: '物品评分修改'
        }
    },
]
export default RecdItemRatingRoute