const RecdItemRatingTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'itemId',
        label: '数据项id'
    },
    {
        prop: 'rating',
        label: '评分'
    },
    {
        prop: 'itemTypeDictId',
        label: '商品类型'
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
]
export default RecdItemRatingTable