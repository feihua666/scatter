package scatter.recommend.rest.item.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 物品评分表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-09-29
 */
@Component
@FeignClient(value = "RecdItemRating-client")
public interface RecdItemRatingClient {

}
