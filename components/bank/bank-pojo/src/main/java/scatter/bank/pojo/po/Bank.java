package scatter.bank.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 银行表
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_bank")
@ApiModel(value="Bank对象", description="银行表")
public class Bank extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_BANK_BY_ID = "trans_bank_by_id_scatter.bank.pojo.po";

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "银行名称")
    private String name;

    @ApiModelProperty(value = "银行全称")
    private String fullname;


}
