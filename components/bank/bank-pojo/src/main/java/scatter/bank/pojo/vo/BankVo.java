package scatter.bank.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 银行响应对象
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Setter
@Getter
@ApiModel(value="银行响应对象")
public class BankVo extends BaseIdVo {

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "银行名称")
    private String name;

    @ApiModelProperty(value = "银行全称")
    private String fullname;

}
