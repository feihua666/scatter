package scatter.bank.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 银行添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Setter
@Getter
@ApiModel(value="银行添加表单对象")
public class BankAddForm extends BaseAddForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码",required = true)
    private String code;

    @NotEmpty(message="银行名称不能为空")
    @ApiModelProperty(value = "银行名称",required = true)
    private String name;

    @NotEmpty(message="银行全称不能为空")
    @ApiModelProperty(value = "银行全称",required = true)
    private String fullname;

}
