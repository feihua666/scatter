package scatter.bank.rest.service.impl;

import scatter.bank.pojo.po.BankCard;
import scatter.bank.rest.mapper.BankCardMapper;
import scatter.bank.rest.service.IBankCardService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.bank.pojo.form.BankCardAddForm;
import scatter.bank.pojo.form.BankCardUpdateForm;
import scatter.bank.pojo.form.BankCardPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 银行表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Service
@Transactional
public class BankCardServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<BankCardMapper, BankCard, BankCardAddForm, BankCardUpdateForm, BankCardPageQueryForm> implements IBankCardService {
    @Override
    public void preAdd(BankCardAddForm addForm,BankCard po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 名称已存在不能添加
            assertByColumn(addForm.getName(),BankCard::getName,false);
        }

    }

    @Override
    public void preUpdate(BankCardUpdateForm updateForm,BankCard po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            BankCard byId = getById(updateForm.getId());
            // 如果名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 名称已存在不能修改
                assertByColumn(updateForm.getName(),BankCard::getName,false);
            }
        }

    }
}
