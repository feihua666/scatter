package scatter.bank.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.bank.pojo.po.BankCard;
import scatter.common.rest.service.IBaseService;
import scatter.bank.pojo.form.BankCardAddForm;
import scatter.bank.pojo.form.BankCardUpdateForm;
import scatter.bank.pojo.form.BankCardPageQueryForm;
import java.util.List;
/**
 * <p>
 * 银行表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
public interface IBankCardService extends IBaseService<BankCard> {


    /**
     * 根据名称查询
     * @param name
     * @return
     */
    default BankCard getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<BankCard>lambdaQuery().eq(BankCard::getName, name));
    }

}
