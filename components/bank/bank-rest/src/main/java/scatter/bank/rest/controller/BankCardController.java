package scatter.bank.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.bank.rest.BankConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.bank.pojo.po.BankCard;
import scatter.bank.pojo.vo.BankCardVo;
import scatter.bank.pojo.form.BankCardAddForm;
import scatter.bank.pojo.form.BankCardUpdateForm;
import scatter.bank.pojo.form.BankCardPageQueryForm;
import scatter.bank.rest.service.IBankCardService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 银行表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Api(tags = "银行相关接口")
@RestController
@RequestMapping(BankConfiguration.CONTROLLER_BASE_PATH + "/bank-card")
public class BankCardController extends BaseAddUpdateQueryFormController<BankCard, BankCardVo, BankCardAddForm, BankCardUpdateForm, BankCardPageQueryForm> {
    @Autowired
    private IBankCardService iBankCardService;

     @ApiOperation("添加银行")
     @PreAuthorize("hasAuthority('BankCard:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public BankCardVo add(@RequestBody @Valid BankCardAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询银行")
     @PreAuthorize("hasAuthority('BankCard:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public BankCardVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除银行")
     @PreAuthorize("hasAuthority('BankCard:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新银行")
     @PreAuthorize("hasAuthority('BankCard:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public BankCardVo update(@RequestBody @Valid BankCardUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询银行")
    @PreAuthorize("hasAuthority('BankCard:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<BankCardVo> getList(BankCardPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询银行")
    @PreAuthorize("hasAuthority('BankCard:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<BankCardVo> getPage(BankCardPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
