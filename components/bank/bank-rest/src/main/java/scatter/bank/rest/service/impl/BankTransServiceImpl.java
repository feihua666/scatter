package scatter.bank.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.bank.pojo.po.Bank;
import scatter.bank.rest.service.IBankService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 银行翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Component
public class BankTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IBankService bankService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Bank.TRANS_BANK_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Bank.TRANS_BANK_BY_ID)) {
            Bank byId = bankService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Bank.TRANS_BANK_BY_ID)) {
            return bankService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
