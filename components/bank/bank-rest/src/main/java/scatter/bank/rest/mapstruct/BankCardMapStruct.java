package scatter.bank.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.bank.pojo.po.BankCard;
import scatter.bank.pojo.vo.BankCardVo;
import scatter.bank.pojo.form.BankCardAddForm;
import scatter.bank.pojo.form.BankCardUpdateForm;
import scatter.bank.pojo.form.BankCardPageQueryForm;

/**
 * <p>
 * 银行 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BankCardMapStruct extends IBaseVoMapStruct<BankCard, BankCardVo>,
                                  IBaseAddFormMapStruct<BankCard,BankCardAddForm>,
                                  IBaseUpdateFormMapStruct<BankCard,BankCardUpdateForm>,
                                  IBaseQueryFormMapStruct<BankCard,BankCardPageQueryForm>{
    BankCardMapStruct INSTANCE = Mappers.getMapper( BankCardMapStruct.class );

}
