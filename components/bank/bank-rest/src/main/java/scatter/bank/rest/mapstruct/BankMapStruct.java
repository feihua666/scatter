package scatter.bank.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.bank.pojo.po.Bank;
import scatter.bank.pojo.vo.BankVo;
import scatter.bank.pojo.form.BankAddForm;
import scatter.bank.pojo.form.BankUpdateForm;
import scatter.bank.pojo.form.BankPageQueryForm;

/**
 * <p>
 * 银行 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BankMapStruct extends IBaseVoMapStruct<Bank, BankVo>,
                                  IBaseAddFormMapStruct<Bank,BankAddForm>,
                                  IBaseUpdateFormMapStruct<Bank,BankUpdateForm>,
                                  IBaseQueryFormMapStruct<Bank,BankPageQueryForm>{
    BankMapStruct INSTANCE = Mappers.getMapper( BankMapStruct.class );

}
