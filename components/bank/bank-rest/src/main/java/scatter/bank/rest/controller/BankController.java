package scatter.bank.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.bank.rest.BankConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.bank.pojo.po.Bank;
import scatter.bank.pojo.vo.BankVo;
import scatter.bank.pojo.form.BankAddForm;
import scatter.bank.pojo.form.BankUpdateForm;
import scatter.bank.pojo.form.BankPageQueryForm;
import scatter.bank.rest.service.IBankService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 银行表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Api(tags = "银行相关接口")
@RestController
@RequestMapping(BankConfiguration.CONTROLLER_BASE_PATH + "/bank")
public class BankController extends BaseAddUpdateQueryFormController<Bank, BankVo, BankAddForm, BankUpdateForm, BankPageQueryForm> {
    @Autowired
    private IBankService iBankService;

     @ApiOperation("添加银行")
     @PreAuthorize("hasAuthority('Bank:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public BankVo add(@RequestBody @Valid BankAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询银行")
     @PreAuthorize("hasAuthority('Bank:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public BankVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除银行")
     @PreAuthorize("hasAuthority('Bank:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新银行")
     @PreAuthorize("hasAuthority('Bank:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public BankVo update(@RequestBody @Valid BankUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询银行")
    @PreAuthorize("hasAuthority('Bank:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<BankVo> getList(BankPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询银行")
    @PreAuthorize("hasAuthority('Bank:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<BankVo> getPage(BankPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
