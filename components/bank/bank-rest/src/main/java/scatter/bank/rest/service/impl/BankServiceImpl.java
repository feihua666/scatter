package scatter.bank.rest.service.impl;

import scatter.bank.pojo.po.Bank;
import scatter.bank.rest.mapper.BankMapper;
import scatter.bank.rest.service.IBankService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.bank.pojo.form.BankAddForm;
import scatter.bank.pojo.form.BankUpdateForm;
import scatter.bank.pojo.form.BankPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 银行表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Service
@Transactional
public class BankServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<BankMapper, Bank, BankAddForm, BankUpdateForm, BankPageQueryForm> implements IBankService {
    @Override
    public void preAdd(BankAddForm addForm,Bank po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getName())) {
            // 银行名称已存在不能添加
            assertByColumn(addForm.getName(),Bank::getName,false);
        }

    }

    @Override
    public void preUpdate(BankUpdateForm updateForm,Bank po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getName())) {
            Bank byId = getById(updateForm.getId());
            // 如果银行名称有改动
            if (!isEqual(updateForm.getName(), byId.getName())) {
                // 银行名称已存在不能修改
                assertByColumn(updateForm.getName(),Bank::getName,false);
            }
        }

    }
}
