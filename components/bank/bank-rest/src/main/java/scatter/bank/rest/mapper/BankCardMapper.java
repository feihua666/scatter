package scatter.bank.rest.mapper;

import scatter.bank.pojo.po.BankCard;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 银行表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
public interface BankCardMapper extends IBaseMapper<BankCard> {

}
