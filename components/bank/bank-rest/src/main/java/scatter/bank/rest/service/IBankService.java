package scatter.bank.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.bank.pojo.po.Bank;
import scatter.common.rest.service.IBaseService;
import scatter.bank.pojo.form.BankAddForm;
import scatter.bank.pojo.form.BankUpdateForm;
import scatter.bank.pojo.form.BankPageQueryForm;
import java.util.List;
/**
 * <p>
 * 银行表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
public interface IBankService extends IBaseService<Bank> {


    /**
     * 根据银行名称查询
     * @param name
     * @return
     */
    default Bank getByName(String name) {
        Assert.hasText(name,"name不能为空");
        return getOne(Wrappers.<Bank>lambdaQuery().eq(Bank::getName, name));
    }

}
