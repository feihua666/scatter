package scatter.bank.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.bank.rest.BankConfiguration;
import scatter.bank.pojo.po.Bank;
import scatter.bank.rest.service.IBankService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 银行表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@RestController
@RequestMapping(BankConfiguration.CONTROLLER_BASE_PATH + "/inner/bank")
public class BankInnerController extends BaseInnerController<Bank> {
 @Autowired
 private IBankService bankService;

 public IBankService getService(){
     return bankService;
 }
}
