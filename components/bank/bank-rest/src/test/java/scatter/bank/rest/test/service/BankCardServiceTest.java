package scatter.bank.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.bank.pojo.po.BankCard;
import scatter.bank.pojo.form.BankCardAddForm;
import scatter.bank.pojo.form.BankCardUpdateForm;
import scatter.bank.pojo.form.BankCardPageQueryForm;
import scatter.bank.rest.test.BankCardSuperTest;
import scatter.bank.rest.service.IBankCardService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 银行 服务测试类
* </p>
*
* @author yw
* @since 2021-06-22
*/
@SpringBootTest
public class BankCardServiceTest extends BankCardSuperTest{

    @Autowired
    private IBankCardService bankCardService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<BankCard> pos = bankCardService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}