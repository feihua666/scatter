package scatter.bank.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.bank.pojo.po.Bank;
import scatter.bank.pojo.form.BankAddForm;
import scatter.bank.pojo.form.BankUpdateForm;
import scatter.bank.pojo.form.BankPageQueryForm;
import scatter.bank.rest.service.IBankService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 银行 测试类基类
* </p>
*
* @author yw
* @since 2021-06-22
*/
public class BankSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IBankService bankService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return bankService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return bankService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Bank mockPo() {
        return JMockData.mock(Bank.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public BankAddForm mockAddForm() {
        return JMockData.mock(BankAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public BankUpdateForm mockUpdateForm() {
        return JMockData.mock(BankUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public BankPageQueryForm mockPageQueryForm() {
        return JMockData.mock(BankPageQueryForm.class, mockConfig);
    }
}