package scatter.bank.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.bank.rest.test.BankCardSuperTest;
/**
* <p>
* 银行 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-06-22
*/
@SpringBootTest
public class BankCardInnerControllerTest extends BankCardSuperTest{
    @Test
    void contextLoads() {
    }
}