package scatter.bank.rest.test.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.bank.rest.test.BankSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.bank.pojo.form.BankAddForm;
import scatter.bank.pojo.vo.BankVo;
import scatter.bank.rest.BankConfiguration;
/**
* <p>
* 银行 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-06-22
*/
@SpringBootTest
@AutoConfigureMockMvc
public class BankControllerTest extends BankSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = BankConfiguration.CONTROLLER_BASE_PATH + "/bank";

        // 请求表单
        BankAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        BankVo vo = JSONUtil.toBean(response.getContentAsString(), BankVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}