const basePath = '' + '/bank-card'
const BankCardUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/BankCardSearchList',
        add: '/BankCardAdd',
        update: '/BankCardUpdate',
    }
}
export default BankCardUrl