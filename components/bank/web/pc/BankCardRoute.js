import BankCardUrl from './BankCardUrl.js'

const BankCardRoute = [
    {
        path: BankCardUrl.router.searchList,
        component: () => import('./element/BankCardSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'BankCardSearchList',
            name: '银行管理'
        }
    },
    {
        path: BankCardUrl.router.add,
        component: () => import('./element/BankCardAdd'),
        meta: {
            code:'BankCardAdd',
            name: '银行添加'
        }
    },
    {
        path: BankCardUrl.router.update,
        component: () => import('./element/BankCardUpdate'),
        meta: {
            code:'BankCardUpdate',
            name: '银行修改'
        }
    },
]
export default BankCardRoute