const BankCardTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'bankId',
        label: '银行id'
    },
]
export default BankCardTable