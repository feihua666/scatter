import BankUrl from './BankUrl.js'

const BankRoute = [
    {
        path: BankUrl.router.searchList,
        component: () => import('./element/BankSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'BankSearchList',
            name: '银行管理'
        }
    },
    {
        path: BankUrl.router.add,
        component: () => import('./element/BankAdd'),
        meta: {
            code:'BankAdd',
            name: '银行添加'
        }
    },
    {
        path: BankUrl.router.update,
        component: () => import('./element/BankUpdate'),
        meta: {
            code:'BankUpdate',
            name: '银行修改'
        }
    },
]
export default BankRoute