const BankTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '银行名称'
    },
    {
        prop: 'fullname',
        label: '银行全称'
    },
]
export default BankTable