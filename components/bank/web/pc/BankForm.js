import BankUrl from './BankUrl.js'
const BankForm = [
    {
        BankSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        BankSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '银行名称',
            required: true,
        }
    },
    {
        BankSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'fullname',
        },
        element:{
            label: '银行全称',
            required: true,
        }
    },

]
export default BankForm