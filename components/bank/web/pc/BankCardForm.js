import BankCardUrl from './BankCardUrl.js'
const BankCardForm = [
    {
        BankCardSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        BankCardSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        BankCardSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'bankId',
        },
        element:{
            label: '银行id',
            required: true,
        }
    },

]
export default BankCardForm