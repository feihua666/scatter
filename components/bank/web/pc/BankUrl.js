const basePath = '' + '/bank'
const BankUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/BankSearchList',
        add: '/BankAdd',
        update: '/BankUpdate',
    }
}
export default BankUrl