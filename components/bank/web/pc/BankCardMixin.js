import BankCardForm from './BankCardForm.js'
import BankCardTable from './BankCardTable.js'
import BankCardUrl from './BankCardUrl.js'
const BankCardMixin = {
    computed: {
        BankCardFormOptions() {
            return this.$stDynamicFormTools.options(BankCardForm,this.$options.name)
        },
        BankCardTableOptions() {
            return BankCardTable
        },
        BankCardUrl(){
            return BankCardUrl
        }
    },
}
export default BankCardMixin