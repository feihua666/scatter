import BankForm from './BankForm.js'
import BankTable from './BankTable.js'
import BankUrl from './BankUrl.js'
const BankMixin = {
    computed: {
        BankFormOptions() {
            return this.$stDynamicFormTools.options(BankForm,this.$options.name)
        },
        BankTableOptions() {
            return BankTable
        },
        BankUrl(){
            return BankUrl
        }
    },
}
export default BankMixin