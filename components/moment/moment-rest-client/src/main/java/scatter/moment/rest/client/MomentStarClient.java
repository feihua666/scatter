package scatter.moment.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 时刻动态点赞表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Component
@FeignClient(value = "MomentStar-client")
public interface MomentStarClient {

}
