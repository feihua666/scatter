package scatter.moment.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 时刻动态图片表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
@FeignClient(value = "MomentPic-client")
public interface MomentPicClient {

}
