package scatter.moment.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.moment.pojo.po.Moment;
import scatter.moment.pojo.form.MomentAddForm;
import scatter.moment.pojo.form.MomentUpdateForm;
import scatter.moment.pojo.form.MomentPageQueryForm;
import scatter.moment.rest.service.IMomentService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 时刻动态 测试类基类
* </p>
*
* @author yw
* @since 2021-02-09
*/
public class MomentSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IMomentService momentService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return momentService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return momentService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Moment mockPo() {
        return JMockData.mock(Moment.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public MomentAddForm mockAddForm() {
        return JMockData.mock(MomentAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public MomentUpdateForm mockUpdateForm() {
        return JMockData.mock(MomentUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public MomentPageQueryForm mockPageQueryForm() {
        return JMockData.mock(MomentPageQueryForm.class, mockConfig);
    }
}