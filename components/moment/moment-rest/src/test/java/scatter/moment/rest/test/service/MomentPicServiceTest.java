package scatter.moment.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.moment.pojo.po.MomentPic;
import scatter.moment.pojo.form.MomentPicAddForm;
import scatter.moment.pojo.form.MomentPicUpdateForm;
import scatter.moment.pojo.form.MomentPicPageQueryForm;
import scatter.moment.rest.test.MomentPicSuperTest;
import scatter.moment.rest.service.IMomentPicService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 时刻动态图片 服务测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class MomentPicServiceTest extends MomentPicSuperTest{

    @Autowired
    private IMomentPicService momentPicService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<MomentPic> pos = momentPicService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}