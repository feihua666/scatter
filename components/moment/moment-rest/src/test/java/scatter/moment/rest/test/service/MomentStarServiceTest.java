package scatter.moment.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.moment.pojo.po.MomentStar;
import scatter.moment.pojo.form.MomentStarAddForm;
import scatter.moment.pojo.form.MomentStarUpdateForm;
import scatter.moment.pojo.form.MomentStarPageQueryForm;
import scatter.moment.rest.test.MomentStarSuperTest;
import scatter.moment.rest.service.IMomentStarService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 时刻动态点赞 服务测试类
* </p>
*
* @author yw
* @since 2021-10-26
*/
@SpringBootTest
public class MomentStarServiceTest extends MomentStarSuperTest{

    @Autowired
    private IMomentStarService momentStarService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<MomentStar> pos = momentStarService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}