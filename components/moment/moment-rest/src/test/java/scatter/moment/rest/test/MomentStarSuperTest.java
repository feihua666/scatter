package scatter.moment.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.moment.pojo.po.MomentStar;
import scatter.moment.pojo.form.MomentStarAddForm;
import scatter.moment.pojo.form.MomentStarUpdateForm;
import scatter.moment.pojo.form.MomentStarPageQueryForm;
import scatter.moment.rest.service.IMomentStarService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 时刻动态点赞 测试类基类
* </p>
*
* @author yw
* @since 2021-10-26
*/
public class MomentStarSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IMomentStarService momentStarService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return momentStarService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return momentStarService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public MomentStar mockPo() {
        return JMockData.mock(MomentStar.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public MomentStarAddForm mockAddForm() {
        return JMockData.mock(MomentStarAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public MomentStarUpdateForm mockUpdateForm() {
        return JMockData.mock(MomentStarUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public MomentStarPageQueryForm mockPageQueryForm() {
        return JMockData.mock(MomentStarPageQueryForm.class, mockConfig);
    }
}