package scatter.moment.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.moment.pojo.po.Moment;
import scatter.moment.pojo.form.MomentAddForm;
import scatter.moment.pojo.form.MomentUpdateForm;
import scatter.moment.pojo.form.MomentPageQueryForm;
import scatter.moment.rest.test.MomentSuperTest;
import scatter.moment.rest.service.IMomentService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 时刻动态 服务测试类
* </p>
*
* @author yw
* @since 2021-02-09
*/
@SpringBootTest
public class MomentServiceTest extends MomentSuperTest{

    @Autowired
    private IMomentService momentService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Moment> pos = momentService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}