DROP TABLE IF EXISTS component_moment_pic;
CREATE TABLE `component_moment_pic` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `moment_id` varchar(20) NOT NULL COMMENT '时刻动态id',
  `pic_url` varchar(300) NOT NULL COMMENT '图片地址',
  `seq` int(2) NOT NULL COMMENT '排序，默认升序',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `subject_id` (`moment_id`),
  KEY `seq` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='时刻动态图片表';
