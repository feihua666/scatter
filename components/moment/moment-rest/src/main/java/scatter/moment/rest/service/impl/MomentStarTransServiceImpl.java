package scatter.moment.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.moment.pojo.po.MomentStar;
import scatter.moment.rest.service.IMomentStarService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 时刻动态点赞翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Component
public class MomentStarTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IMomentStarService momentStarService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,MomentStar.TRANS_MOMENTSTAR_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,MomentStar.TRANS_MOMENTSTAR_BY_ID)) {
            MomentStar byId = momentStarService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,MomentStar.TRANS_MOMENTSTAR_BY_ID)) {
            return momentStarService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
