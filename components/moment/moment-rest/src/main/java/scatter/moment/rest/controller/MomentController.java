package scatter.moment.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.moment.pojo.form.*;
import scatter.moment.pojo.param.MomentPublishParam;
import scatter.moment.pojo.po.Moment;
import scatter.moment.pojo.vo.AvailableMomentVo;
import scatter.moment.pojo.vo.MomentVo;
import scatter.moment.rest.MomentConfiguration;
import scatter.moment.rest.mapstruct.MomentMapStruct;
import scatter.moment.rest.service.IMomentService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 时刻动态表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "时刻动态相关接口")
@RestController
@RequestMapping(MomentConfiguration.CONTROLLER_BASE_PATH + "/moment")
public class MomentController extends BaseAddUpdateQueryFormController<Moment, MomentVo, MomentAddForm, MomentUpdateForm, MomentPageQueryForm> {


    @Autowired
    private IMomentService iMomentService;



    @Override
	 @ApiOperation("添加时刻动态")
     @PreAuthorize("hasAuthority('Moment:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public MomentVo add(@RequestBody @Valid MomentAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询时刻动态")
     @PreAuthorize("hasAuthority('Moment:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public MomentVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除时刻动态")
     @PreAuthorize("hasAuthority('Moment:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新时刻动态")
     @PreAuthorize("hasAuthority('Moment:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public MomentVo update(@RequestBody @Valid MomentUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询时刻动态")
    @PreAuthorize("hasAuthority('Moment:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<MomentVo> getList(MomentPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询时刻动态")
    @PreAuthorize("hasAuthority('Moment:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<MomentVo> getPage(MomentPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("分页查询仅可展示时刻动态")
    @GetMapping("/getAvailablePage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<AvailableMomentVo> getAvailablePage(MomentAvailablePageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) throws InterruptedException {
        IPage<AvailableMomentVo> momentIPage = iMomentService.listAvailablePage(listPageForm, Optional.ofNullable(loginUser).map(l ->l.getId()).orElse(null));

        return momentIPage;
    }
    @ApiOperation("分页查询我的仅可展示时刻动态")
    @PreAuthorize("hasAuthority('appclient')")
    @GetMapping("/my/getAvailablePage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<AvailableMomentVo> listAvailablePageByUserId(MomentAvailablePageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) throws InterruptedException {
        IPage<AvailableMomentVo> momentIPage = iMomentService.listAvailablePageByUserId(listPageForm, Optional.ofNullable(loginUser).map(l ->l.getId()).orElse(null));

        return momentIPage;
    }

    @ApiOperation("添加时刻动态")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/publish")
    @ResponseStatus(HttpStatus.CREATED)
    public MomentVo publish(@RequestBody @Valid MomentPublishForm publishForm, @ApiIgnore LoginUser loginUser) {
        MomentPublishParam momentPublishParam = MomentMapStruct.INSTANCE.publishFormToParam(publishForm);
        momentPublishParam.setOwnerUserId(loginUser.getId());
        momentPublishParam.setOwnerUserAvatar(loginUser.getAvatar());
        momentPublishParam.setOwnerUserNickname(loginUser.getNickname());
        Moment publish = iMomentService.publish(momentPublishParam);
        return super.getVo(publish);
    }

}
