package scatter.moment.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.moment.pojo.po.Moment;
import scatter.moment.rest.service.IMomentService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 时刻动态翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
public class MomentTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IMomentService momentService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Moment.TRANS_MOMENT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Moment.TRANS_MOMENT_BY_ID)) {
            Moment byId = momentService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Moment.TRANS_MOMENT_BY_ID)) {
            return momentService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
