package scatter.moment.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.moment.pojo.po.MomentPic;
import scatter.moment.pojo.vo.MomentPicVo;
import scatter.moment.pojo.form.MomentPicAddForm;
import scatter.moment.pojo.form.MomentPicUpdateForm;
import scatter.moment.pojo.form.MomentPicPageQueryForm;

/**
 * <p>
 * 时刻动态图片 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MomentPicMapStruct extends IBaseVoMapStruct<MomentPic, MomentPicVo>,
                                  IBaseAddFormMapStruct<MomentPic,MomentPicAddForm>,
                                  IBaseUpdateFormMapStruct<MomentPic,MomentPicUpdateForm>,
                                  IBaseQueryFormMapStruct<MomentPic,MomentPicPageQueryForm>{
    MomentPicMapStruct INSTANCE = Mappers.getMapper( MomentPicMapStruct.class );

}
