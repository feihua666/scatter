package scatter.moment.rest.mapper;

import scatter.moment.pojo.po.Moment;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 时刻动态表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface MomentMapper extends IBaseMapper<Moment> {

}
