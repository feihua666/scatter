package scatter.moment.rest.mapper;

import scatter.moment.pojo.po.MomentPic;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 时刻动态图片表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface MomentPicMapper extends IBaseMapper<MomentPic> {

}
