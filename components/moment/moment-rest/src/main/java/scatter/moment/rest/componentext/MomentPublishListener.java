package scatter.moment.rest.componentext;

import scatter.moment.pojo.param.MomentPublishParam;
import scatter.moment.pojo.po.Moment;

/**
 * <p>
 * 动态发布监听
 * </p>
 *
 * @author yangwei
 * @since 2021-11-11 15:59
 */
public interface MomentPublishListener {

	/**
	 * 发布之前
	 * @param param
	 * @param moment
	 */
	public void preMomentPublish(MomentPublishParam param, Moment moment);

	/**
	 * 发布之后
	 * @param param
	 * @param moment
	 */
	public void postMomentPublish(MomentPublishParam param, Moment moment);
}
