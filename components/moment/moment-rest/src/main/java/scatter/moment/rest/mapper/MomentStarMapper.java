package scatter.moment.rest.mapper;

import scatter.moment.pojo.po.MomentStar;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 时刻动态点赞表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
public interface MomentStarMapper extends IBaseMapper<MomentStar> {

}
