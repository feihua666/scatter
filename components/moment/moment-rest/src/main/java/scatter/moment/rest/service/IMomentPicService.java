package scatter.moment.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.moment.pojo.po.MomentPic;
import scatter.common.rest.service.IBaseService;
import scatter.moment.pojo.form.MomentPicAddForm;
import scatter.moment.pojo.form.MomentPicUpdateForm;
import scatter.moment.pojo.form.MomentPicPageQueryForm;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * 时刻动态图片表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface IMomentPicService extends IBaseService<MomentPic> {


	/**
	 * 添加图片
	 * @param picUrls
	 * @param momentId
	 * @return
	 */
	default boolean addPics(List<String> picUrls,String momentId){
		if (isEmpty(picUrls)) {
			return false;
		}
		List<MomentPic> toBeInsertedList = new ArrayList<>(picUrls.size());
		for (int i = 0; i < picUrls.size(); i++) {
			toBeInsertedList.add(new MomentPic().setMomentId(momentId).setPicUrl(picUrls.get(i)).setSeq(i *10));
		}
		return saveBatch(toBeInsertedList);
	}
}
