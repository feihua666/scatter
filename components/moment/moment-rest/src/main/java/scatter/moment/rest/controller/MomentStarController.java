package scatter.moment.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.moment.pojo.form.*;
import scatter.moment.pojo.param.MomentCancelStarParam;
import scatter.moment.pojo.param.MomentStarAddStarParam;
import scatter.moment.pojo.po.MomentStar;
import scatter.moment.pojo.vo.MomentStarVo;
import scatter.moment.rest.MomentConfiguration;
import scatter.moment.rest.service.IMomentStarService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 时刻动态点赞表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Api(tags = "时刻动态点赞相关接口")
@RestController
@RequestMapping(MomentConfiguration.CONTROLLER_BASE_PATH + "/moment-star")
public class MomentStarController extends BaseAddUpdateQueryFormController<MomentStar, MomentStarVo, MomentStarAddForm, MomentStarUpdateForm, MomentStarPageQueryForm> {
    @Autowired
    private IMomentStarService iMomentStarService;

    @ApiOperation("添加时刻动态点赞")
    @PreAuthorize("hasAuthority('MomentStar:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MomentStarVo add(@RequestBody @Valid MomentStarAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询时刻动态点赞")
    @PreAuthorize("hasAuthority('MomentStar:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public MomentStarVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除时刻动态点赞")
    @PreAuthorize("hasAuthority('MomentStar:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新时刻动态点赞")
    @PreAuthorize("hasAuthority('MomentStar:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MomentStarVo update(@RequestBody @Valid MomentStarUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询时刻动态点赞")
    @PreAuthorize("hasAuthority('MomentStar:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<MomentStarVo> getList(MomentStarPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询时刻动态点赞")
    @PreAuthorize("hasAuthority('MomentStar:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<MomentStarVo> getPage(MomentStarPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("分页查询时刻动态可用点赞")
    @GetMapping("/getPage1")
    @ResponseStatus(HttpStatus.OK)
    public IPage<MomentStarVo> getPage1(@Valid MomentStarPageQueryForm1 listPageForm) {
        IPage<MomentStar> momentStarIPage = iMomentStarService.listPage1(listPageForm);
        return super.pagePoToVo(momentStarIPage);
    }


    @ApiOperation("添加时刻动态一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/addStar")
    @ResponseStatus(HttpStatus.CREATED)
    public MomentStarVo addStar(@RequestBody @Valid MomentStarAddForm1 addForm1, @ApiIgnore LoginUser loginUser) {
        MomentStarAddStarParam addStartParam = new MomentStarAddStarParam();
        addStartParam.setMomentId(addForm1.getMomentId());
        addStartParam.setOwnerUserId(loginUser.getId());
        addStartParam.setOwnerUserAvatar(loginUser.getAvatar());
        addStartParam.setOwnerUserNickname(loginUser.getNickname());
        return getVo(iMomentStarService.addStar(addStartParam));
    }
    @ApiOperation("取消时刻动态一个点赞")
    @PreAuthorize("hasAuthority('appclient')")
    @PostMapping("/cancelStar")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean addStar(@RequestBody @Valid MomentStarCancelForm cancelForm, @ApiIgnore LoginUser loginUser) {
        MomentCancelStarParam cancelStarParam = new MomentCancelStarParam();
        cancelStarParam.setMomentId(cancelForm.getMomentId());
        cancelStarParam.setOwnerUserId(loginUser.getId());
        return iMomentStarService.cancelStar(cancelStarParam);
    }
}
