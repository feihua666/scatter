package scatter.moment.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.moment.pojo.po.MomentPic;
import scatter.moment.rest.service.IMomentPicService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 时刻动态图片翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Component
public class MomentPicTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IMomentPicService momentPicService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,MomentPic.TRANS_MOMENTPIC_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,MomentPic.TRANS_MOMENTPIC_BY_ID)) {
            MomentPic byId = momentPicService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,MomentPic.TRANS_MOMENTPIC_BY_ID)) {
            return momentPicService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
