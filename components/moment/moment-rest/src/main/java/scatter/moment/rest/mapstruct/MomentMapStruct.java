package scatter.moment.rest.mapstruct;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.moment.pojo.form.MomentAddForm;
import scatter.moment.pojo.form.MomentPageQueryForm;
import scatter.moment.pojo.form.MomentPublishForm;
import scatter.moment.pojo.form.MomentUpdateForm;
import scatter.moment.pojo.param.MomentPublishParam;
import scatter.moment.pojo.po.Moment;
import scatter.moment.pojo.vo.AvailableMomentVo;
import scatter.moment.pojo.vo.MomentVo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 时刻动态 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MomentMapStruct extends IBaseVoMapStruct<Moment, MomentVo>,
                                  IBaseAddFormMapStruct<Moment,MomentAddForm>,
                                  IBaseUpdateFormMapStruct<Moment,MomentUpdateForm>,
                                  IBaseQueryFormMapStruct<Moment,MomentPageQueryForm>{
    MomentMapStruct INSTANCE = Mappers.getMapper( MomentMapStruct.class );

    MomentPublishParam publishFormToParam(MomentPublishForm form);

    Moment publishParamToPo(MomentPublishParam param);


    AvailableMomentVo poToAvailableMomentVo(Moment po);

    default List<AvailableMomentVo> posToAvailableMomentVos(List<Moment> pos) {
        return pos.stream().map(po->(poToAvailableMomentVo(po))).collect(Collectors.toList());
    }

    default IPage<AvailableMomentVo> pagePoToAvailableMomentVo(IPage<Moment> page){
        IPage pageR = page;
        List<Moment> records = pageR.getRecords();
        if (pageR != null && !CollectionUtil.isEmpty(records)) {
            pageR.setRecords(posToAvailableMomentVos(records));
            return pageR;
        }
        // 原样返回page
        return pageR;
    }
}
