package scatter.moment.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.moment.rest.MomentConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.moment.pojo.po.MomentPic;
import scatter.moment.pojo.vo.MomentPicVo;
import scatter.moment.pojo.form.MomentPicAddForm;
import scatter.moment.pojo.form.MomentPicUpdateForm;
import scatter.moment.pojo.form.MomentPicPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 时刻动态图片表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Api(tags = "时刻动态图片相关接口")
@RestController
@RequestMapping(MomentConfiguration.CONTROLLER_BASE_PATH + "/moment-pic")
public class MomentPicController extends BaseAddUpdateQueryFormController<MomentPic, MomentPicVo, MomentPicAddForm, MomentPicUpdateForm, MomentPicPageQueryForm> {


     @Override
	 @ApiOperation("添加时刻动态图片")
     @PreAuthorize("hasAuthority('MomentPic:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public MomentPicVo add(@RequestBody @Valid MomentPicAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询时刻动态图片")
     @PreAuthorize("hasAuthority('MomentPic:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public MomentPicVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除时刻动态图片")
     @PreAuthorize("hasAuthority('MomentPic:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新时刻动态图片")
     @PreAuthorize("hasAuthority('MomentPic:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public MomentPicVo update(@RequestBody @Valid MomentPicUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询时刻动态图片")
    @PreAuthorize("hasAuthority('MomentPic:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<MomentPicVo> getList(MomentPicPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询时刻动态图片")
    @PreAuthorize("hasAuthority('MomentPic:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<MomentPicVo> getPage(MomentPicPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
