package scatter.moment.rest.service.impl;

import scatter.moment.pojo.po.MomentPic;
import scatter.moment.rest.mapper.MomentPicMapper;
import scatter.moment.rest.service.IMomentPicService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.moment.pojo.form.MomentPicAddForm;
import scatter.moment.pojo.form.MomentPicUpdateForm;
import scatter.moment.pojo.form.MomentPicPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 时刻动态图片表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Service
@Transactional
public class MomentPicServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<MomentPicMapper, MomentPic, MomentPicAddForm, MomentPicUpdateForm, MomentPicPageQueryForm> implements IMomentPicService {
    @Override
    public void preAdd(MomentPicAddForm addForm,MomentPic po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(MomentPicUpdateForm updateForm,MomentPic po) {
        super.preUpdate(updateForm,po);

    }
}
