package scatter.moment.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.moment.pojo.param.MomentStarAddStarParam;
import scatter.moment.pojo.po.MomentStar;
import scatter.moment.pojo.vo.MomentStarVo;
import scatter.moment.pojo.form.MomentStarAddForm;
import scatter.moment.pojo.form.MomentStarUpdateForm;
import scatter.moment.pojo.form.MomentStarPageQueryForm;

/**
 * <p>
 * 时刻动态点赞 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MomentStarMapStruct extends IBaseVoMapStruct<MomentStar, MomentStarVo>,
                                  IBaseAddFormMapStruct<MomentStar,MomentStarAddForm>,
                                  IBaseUpdateFormMapStruct<MomentStar,MomentStarUpdateForm>,
                                  IBaseQueryFormMapStruct<MomentStar,MomentStarPageQueryForm>{
    MomentStarMapStruct INSTANCE = Mappers.getMapper( MomentStarMapStruct.class );

    MomentStar addStartParamToPo(MomentStarAddStarParam startParam);
}
