package scatter.moment.rest.componentext;

import scatter.moment.pojo.vo.AvailableMomentVo;
import scatter.moment.pojo.vo.MomentVo;

import java.util.List;

/**
 * <p>
 * 获取可用动态添加额外数据处理器
 * </p>
 *
 * @author yangwei
 * @since 2021-10-28 16:07
 */
public interface MomentAvailableExtProvider {
	/**
	 * 提供额外补充
	 * @param momentVoList
	 */
	public void provideMoment(List<AvailableMomentVo> momentVoList);
}
