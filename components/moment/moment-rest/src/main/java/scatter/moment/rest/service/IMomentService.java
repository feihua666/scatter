package scatter.moment.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.moment.pojo.form.MomentAvailablePageQueryForm;
import scatter.moment.pojo.param.MomentPublishParam;
import scatter.moment.pojo.po.Moment;
import scatter.common.rest.service.IBaseService;
import scatter.moment.pojo.form.MomentAddForm;
import scatter.moment.pojo.form.MomentUpdateForm;
import scatter.moment.pojo.form.MomentPageQueryForm;
import scatter.moment.pojo.vo.AvailableMomentVo;
import scatter.moment.pojo.vo.MomentVo;

import java.util.List;
/**
 * <p>
 * 时刻动态表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
public interface IMomentService extends IBaseService<Moment> {

	/**
	 * 获取可用动态,排序按发布时间倒序
	 * @param pageQueryForm
	 * @param loginUserId
	 * @return
	 */
	IPage<AvailableMomentVo> listAvailablePage(MomentAvailablePageQueryForm pageQueryForm,String loginUserId) throws InterruptedException;
	/**
	 * 获取可用动态,排序按发布时间倒序
	 * @param pageQueryForm
	 * @param loginUserId
	 * @return
	 */
	IPage<AvailableMomentVo> listAvailablePageByUserId(MomentAvailablePageQueryForm pageQueryForm,String loginUserId) throws InterruptedException;

	/**
	 * 发布动态
	 * @param param
	 * @return
	 */
	Moment publish(MomentPublishParam param);
}
