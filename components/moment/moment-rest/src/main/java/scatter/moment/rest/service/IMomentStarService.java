package scatter.moment.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.moment.pojo.form.MomentAvailablePageQueryForm;
import scatter.moment.pojo.form.MomentStarPageQueryForm1;
import scatter.moment.pojo.param.MomentStarAddStarParam;
import scatter.moment.pojo.param.MomentCancelStarParam;
import scatter.moment.pojo.po.Moment;
import scatter.moment.pojo.po.MomentStar;
import scatter.common.rest.service.IBaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 时刻动态点赞表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
public interface IMomentStarService extends IBaseService<MomentStar> {

	/**
	 * 添加一个点点赞
	 * @param addStartParam
	 * @return
	 */
	public MomentStar addStar(MomentStarAddStarParam addStartParam);

	/**
	 * 取消点赞
	 * @param cancelStarParam
	 * @return
	 */
	public boolean cancelStar(MomentCancelStarParam cancelStarParam);

	/**
	 * 是否已点赞
	 * @param momentId
	 * @param userId
	 * @return
	 */
	default public boolean hasStared(String momentId,String userId){
		Assert.hasText(momentId,"momentId 不能为空");

		return hasStared(newArrayList(momentId),userId).get(momentId);
	}
	/**
	 * 是否已点赞
	 * @param momentIds
	 * @param userId
	 * @return
	 */
	default public Map<String,Boolean> hasStared(List<String> momentIds, String userId){
		Assert.notEmpty(momentIds,"momentIds 不能为空");
		if (isStrEmpty(userId)) {
			return momentIds.stream().collect(Collectors.toMap(Function.identity(), Boolean::new));
		}
		List<MomentStar> list = list(Wrappers.<MomentStar>lambdaQuery().in(MomentStar::getMomentId, momentIds).eq(MomentStar::getOwnerUserId, userId));
		Map<String,Boolean> result = new HashMap<>();
		for (String momentId : momentIds) {
			result.put(momentId,list.stream().filter(item->isEqual(momentId,item.getMomentId())).count() > 0);
		}
		return result;
	}

	/**
	 * 列出每一个时刻动态最新的点赞数据
	 * @param momentIds
	 * @param count
	 * @return
	 */
	public Map<String,Page<MomentStar> > listLatestStarForPerMoment(List<String> momentIds, Integer count);

	/**
	 * 获取momentId对应的点赞数据
	 * @param pageQueryForm
	 * @return
	 */
	default public IPage<MomentStar> listPage1(MomentStarPageQueryForm1 pageQueryForm) {
		return page(convertPage(pageQueryForm), Wrappers.<MomentStar>lambdaQuery()
				.eq(MomentStar::getMomentId,pageQueryForm.getMomentId())
				.orderByDesc(MomentStar::getTimeAt).orderByDesc(MomentStar::getId));
	}
}
