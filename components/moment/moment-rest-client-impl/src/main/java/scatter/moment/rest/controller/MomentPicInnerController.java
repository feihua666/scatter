package scatter.moment.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.moment.rest.MomentConfiguration;
import scatter.moment.pojo.po.MomentPic;
import scatter.moment.rest.service.IMomentPicService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 时刻动态图片表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@RestController
@RequestMapping(MomentConfiguration.CONTROLLER_BASE_PATH + "/inner/moment-pic")
public class MomentPicInnerController extends BaseInnerController<MomentPic> {
 @Autowired
 private IMomentPicService momentPicService;

 public IMomentPicService getService(){
     return momentPicService;
 }
}
