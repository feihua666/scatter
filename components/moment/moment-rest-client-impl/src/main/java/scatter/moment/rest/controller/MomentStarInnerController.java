package scatter.moment.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.moment.rest.MomentConfiguration;
import scatter.moment.pojo.po.MomentStar;
import scatter.moment.rest.service.IMomentStarService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 时刻动态点赞表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@RestController
@RequestMapping(MomentConfiguration.CONTROLLER_BASE_PATH + "/inner/moment-star")
public class MomentStarInnerController extends BaseInnerController<MomentStar> {
 @Autowired
 private IMomentStarService momentStarService;

 public IMomentStarService getService(){
     return momentStarService;
 }
}
