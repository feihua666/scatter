package scatter.moment.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.moment.rest.MomentConfiguration;
import scatter.moment.pojo.po.Moment;
import scatter.moment.rest.service.IMomentService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 时刻动态表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@RestController
@RequestMapping(MomentConfiguration.CONTROLLER_BASE_PATH + "/inner/moment")
public class MomentInnerController extends BaseInnerController<Moment> {
 @Autowired
 private IMomentService momentService;

 public IMomentService getService(){
     return momentService;
 }
}
