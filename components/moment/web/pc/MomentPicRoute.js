import MomentPicUrl from './MomentPicUrl.js'
const MomentPicRoute = [
    {
        path: MomentPicUrl.router.searchList,
        component: () => import('./element/MomentPicSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MomentPicSearchList',
            name: '时刻动态图片管理'
        }
    },
    {
        path: MomentPicUrl.router.add,
        component: () => import('./element/MomentPicAdd'),
        meta: {
            code:'MomentPicAdd',
            name: '时刻动态图片添加'
        }
    },
    {
        path: MomentPicUrl.router.update,
        component: () => import('./element/MomentPicUpdate'),
        meta: {
            code:'MomentPicUpdate',
            name: '时刻动态图片修改'
        }
    },
]
export default MomentPicRoute