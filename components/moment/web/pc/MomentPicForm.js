import MomentPicUrl from './MomentPicUrl.js'
const MomentPicForm = [
    {
        MomentPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'momentId',
        },
        element:{
            label: '时刻动态id',
            required: true,
        }
    },
    {
        MomentPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'picUrl',
        },
        element:{
            label: '图片地址',
            required: true,
        }
    },
    {
        MomentPicSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'seq',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        }
    },
]
export default MomentPicForm