import MomentStarUrl from './MomentStarUrl.js'

const MomentStarRoute = [
    {
        path: MomentStarUrl.router.searchList,
        component: () => import('./element/MomentStarSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MomentStarSearchList',
            name: '时刻动态点赞管理'
        }
    },
    {
        path: MomentStarUrl.router.add,
        component: () => import('./element/MomentStarAdd'),
        meta: {
            code:'MomentStarAdd',
            name: '时刻动态点赞添加'
        }
    },
    {
        path: MomentStarUrl.router.update,
        component: () => import('./element/MomentStarUpdate'),
        meta: {
            code:'MomentStarUpdate',
            name: '时刻动态点赞修改'
        }
    },
]
export default MomentStarRoute