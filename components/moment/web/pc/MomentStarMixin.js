import MomentStarForm from './MomentStarForm.js'
import MomentStarTable from './MomentStarTable.js'
import MomentStarUrl from './MomentStarUrl.js'
const MomentStarMixin = {
    computed: {
        MomentStarFormOptions() {
            return this.$stDynamicFormTools.options(MomentStarForm,this.$options.name)
        },
        MomentStarTableOptions() {
            return MomentStarTable
        },
        MomentStarUrl(){
            return MomentStarUrl
        }
    },
}
export default MomentStarMixin