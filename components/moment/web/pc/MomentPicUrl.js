const basePath = '' + '/moment-pic'
const MomentPicUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/MomentPicSearchList',
        add: '/MomentPicAdd',
        update: '/MomentPicUpdate',
    }
}
export default MomentPicUrl