const MomentPicTable = [
    {
        prop: 'momentId',
        label: '时刻动态id'
    },
    {
        prop: 'picUrl',
        label: '图片地址'
    },
    {
        prop: 'seq',
        label: '排序'
    },
]
export default MomentPicTable