import MomentUrl from './MomentUrl.js'
const MomentRoute = [
    {
        path: MomentUrl.router.searchList,
        component: () => import('./element/MomentSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MomentSearchList',
            name: '时刻动态管理'
        }
    },
    {
        path: MomentUrl.router.add,
        component: () => import('./element/MomentAdd'),
        meta: {
            code:'MomentAdd',
            name: '时刻动态添加'
        }
    },
    {
        path: MomentUrl.router.update,
        component: () => import('./element/MomentUpdate'),
        meta: {
            code:'MomentUpdate',
            name: '时刻动态修改'
        }
    },
]
export default MomentRoute