import MomentPicForm from './MomentPicForm.js'
import MomentPicTable from './MomentPicTable.js'
import MomentPicUrl from './MomentPicUrl.js'
const MomentPicMixin = {
    computed: {
        MomentPicFormOptions() {
            return this.$stDynamicFormTools.options(MomentPicForm,this.$options.name)
        },
        MomentPicTableOptions() {
            return MomentPicTable
        },
        MomentPicUrl(){
            return MomentPicUrl
        }
    },
}
export default MomentPicMixin