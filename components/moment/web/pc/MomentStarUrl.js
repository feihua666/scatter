const basePath = '' + '/moment-star'
const MomentStarUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    addStar: basePath + '/addStar',
    cancelStar: basePath + '/cancelStar',
    // 点赞列表，
    getPage1: basePath + '/getPage1',
    router: {
        searchList: '/MomentStarSearchList',
        add: '/MomentStarAdd',
        update: '/MomentStarUpdate',
    }
}
export default MomentStarUrl