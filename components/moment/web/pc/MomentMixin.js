import MomentForm from './MomentForm.js'
import MomentTable from './MomentTable.js'
import MomentUrl from './MomentUrl.js'
const MomentMixin = {
    computed: {
        MomentFormOptions() {
            return this.$stDynamicFormTools.options(MomentForm,this.$options.name)
        },
        MomentTableOptions() {
            return MomentTable
        },
        MomentUrl(){
            return MomentUrl
        }
    },
}
export default MomentMixin