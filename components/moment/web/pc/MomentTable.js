const MomentTable = [
    {
        prop: 'content',
        label: '动态内容'
    },
    {
        prop: 'timeAt',
        label: '动态发布时间'
    },
    {
        prop: 'ownerUserId',
        label: '发布人用户id'
    },
    {
        prop: 'ownerUserNickname',
        label: '发布人用户昵称'
    },
    {
        prop: 'ownerUserAvatar',
        label: '发布人用户头像'
    },
    {
        prop: 'commentCount',
        label: '子一级评论数'
    },
    {
        prop: 'starCount',
        label: '点赞数'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'linkPicUrl',
        label: '链接图片地址'
    },
    {
        prop: 'linkContent',
        label: '链接内容'
    },
    {
        prop: 'linkUrl',
        label: '链接地址'
    },
]
export default MomentTable