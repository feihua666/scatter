import MomentStarUrl from './MomentStarUrl.js'
const MomentStarForm = [
    {
        MomentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'momentId',
        },
        element:{
            label: '时刻动态id',
            required: true,
        }
    },
    {
        MomentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'timeAt',
        },
        element:{
            label: '点赞时间',
            required: true,
        }
    },
    {
        MomentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '点赞人用户id',
            required: true,
        }
    },
    {
        MomentStarSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '点赞人用户昵称',
            required: true,
        }
    },
    {

        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '点赞人用户头像',
        }
    },
    {

        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },

]
export default MomentStarForm