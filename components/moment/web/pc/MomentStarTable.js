const MomentStarTable = [
    {
        prop: 'momentId',
        label: '时刻动态id'
    },
    {
        prop: 'timeAt',
        label: '点赞时间'
    },
    {
        prop: 'ownerUserId',
        label: '点赞人用户id'
    },
    {
        prop: 'ownerUserNickname',
        label: '点赞人用户昵称'
    },
    {
        prop: 'ownerUserAvatar',
        label: '点赞人用户头像'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
]
export default MomentStarTable