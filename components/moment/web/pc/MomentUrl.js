const basePath = '' + '/moment'
const MomentUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getAvailablePage: basePath + '/getAvailablePage',
    myGetAvailablePage: basePath + '/my/getAvailablePage',
    publish: basePath + '/publish',
    router: {
        searchList: '/MomentSearchList',
        add: '/MomentAdd',
        update: '/MomentUpdate',
    }
}
export default MomentUrl