import MomentUrl from './MomentUrl.js'
const MomentForm = [
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'content',
        },
        element:{
            label: '动态内容',
            required: true,
        }
    },
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'timeAt',
        },
        element:{
            label: '动态发布时间',
            required: true,
        }
    },
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserId',
        },
        element:{
            label: '发布人用户id',
            required: true,
        }
    },
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'ownerUserNickname',
        },
        element:{
            label: '发布人用户昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'ownerUserAvatar',
        },
        element:{
            label: '发布人用户头像',
        }
    },
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'commentCount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '子一级评论数',
            required: true,
        }
    },
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'starCount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '点赞数',
            required: true,
        }
    },
    {
        MomentSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
    {
        field: {
            name: 'linkPicUrl',
        },
        element:{
            label: '链接图片地址',
        }
    },
    {
        field: {
            name: 'linkContent',
        },
        element:{
            label: '链接内容',
        }
    },
    {
        field: {
            name: 'linkUrl',
        },
        element:{
            label: '链接地址',
        }
    },
]
export default MomentForm