package scatter.moment.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 时刻动态点赞取消表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Setter
@Getter
@ApiModel(value="时刻动态点赞取消表单对象")
public class MomentStarCancelForm extends BaseAddForm {

    @NotEmpty(message="时刻动态id不能为空")
    @ApiModelProperty(value = "时刻动态id",required = true)
    private String momentId;

}
