package scatter.moment.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.util.Map;

import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 时刻动态表
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_moment")
@ApiModel(value="Moment对象", description="时刻动态表")
public class Moment extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_MOMENT_BY_ID = "trans_moment_by_id_scatter.moment.pojo.po";

    @ApiModelProperty(value = "动态内容")
    private String content;

    @ApiModelProperty(value = "动态发布时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "发布人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "发布人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "发布人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "点赞数")
    private Integer starCount;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "链接图片地址")
    private String linkPicUrl;

    @ApiModelProperty(value = "链接内容")
    private String linkContent;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

}
