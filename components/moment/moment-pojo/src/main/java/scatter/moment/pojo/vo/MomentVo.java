package scatter.moment.pojo.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.comment.pojo.subject.vo.CommentSubjectVo;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.impl.TableNameTransServiceImpl;
import scatter.moment.pojo.po.MomentPic;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <p>
 * 时刻动态响应对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="时刻动态响应对象")
public class MomentVo extends BaseIdVo {

    @ApiModelProperty(value = "动态内容")
    private String content;

    @ApiModelProperty(value = "动态发布时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "发布人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "发布人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "发布人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "子一级评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "点赞数")
    private Integer starCount;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "链接图片地址")
    private String linkPicUrl;

    @ApiModelProperty(value = "链接内容")
    private String linkContent;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

}
