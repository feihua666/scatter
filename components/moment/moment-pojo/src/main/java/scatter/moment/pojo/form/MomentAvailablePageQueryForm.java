package scatter.moment.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.common.pojo.form.OrderBy;

import java.time.LocalDateTime;


/**
 * <p>
 * 可用时刻动态分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@OrderBy(value = "timeAt",asc = false)
@Setter
@Getter
@ApiModel(value="可用时刻动态分页表单对象")
public class MomentAvailablePageQueryForm extends BasePageQueryForm {
	@ApiModelProperty(value = "分组标识")
	private String groupFlag;
}
