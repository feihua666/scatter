package scatter.moment.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 时刻动态点赞表
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_moment_star")
@ApiModel(value="MomentStar对象", description="时刻动态点赞表")
public class MomentStar extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_MOMENTSTAR_BY_ID = "trans_momentstar_by_id_scatter.moment.pojo.po";

    @ApiModelProperty(value = "时刻动态id")
    private String momentId;

    @ApiModelProperty(value = "点赞时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "点赞人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "点赞人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;

}
