package scatter.moment.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-26 11:03
 */
@Setter
@Getter
public class MomentStarAddStarParam extends BaseParam {

	@NotEmpty(message="时刻动态id不能为空")
	@ApiModelProperty(value = "时刻动态id",required = true)
	private String momentId;

	@NotEmpty(message="点赞人用户id不能为空")
	@ApiModelProperty(value = "点赞人用户id",required = true)
	private String ownerUserId;

	@NotEmpty(message="点赞人用户昵称不能为空")
	@ApiModelProperty(value = "点赞人用户昵称",required = true)
	private String ownerUserNickname;

	@ApiModelProperty(value = "点赞人用户头像")
	private String ownerUserAvatar;

}
