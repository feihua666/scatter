package scatter.moment.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 时刻动态点赞更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Setter
@Getter
@ApiModel(value="时刻动态点赞更新表单对象")
public class MomentStarUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="时刻动态id不能为空")
    @ApiModelProperty(value = "时刻动态id",required = true)
    private String momentId;

    @NotNull(message="点赞时间不能为空")
    @ApiModelProperty(value = "点赞时间",required = true)
    private LocalDateTime timeAt;

    @NotEmpty(message="点赞人用户id不能为空")
    @ApiModelProperty(value = "点赞人用户id",required = true)
    private String ownerUserId;

    @NotEmpty(message="点赞人用户昵称不能为空")
    @ApiModelProperty(value = "点赞人用户昵称",required = true)
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;


}
