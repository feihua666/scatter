package scatter.moment.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 时刻动态点赞响应对象
 * </p>
 *
 * @author yw
 * @since 2021-10-26
 */
@Setter
@Getter
@ApiModel(value="时刻动态点赞响应对象")
public class MomentStarVo extends BaseIdVo {

    @ApiModelProperty(value = "时刻动态id")
    private String momentId;

    @ApiModelProperty(value = "点赞时间")
    private LocalDateTime timeAt;

    @ApiModelProperty(value = "点赞人用户id")
    private String ownerUserId;

    @ApiModelProperty(value = "点赞人用户昵称")
    private String ownerUserNickname;

    @ApiModelProperty(value = "点赞人用户头像")
    private String ownerUserAvatar;


}
