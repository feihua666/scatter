package scatter.moment.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 时刻动态添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="时刻动态添加表单对象")
public class MomentAddForm extends BaseAddForm {

    @NotEmpty(message="动态内容不能为空")
    @ApiModelProperty(value = "动态内容",required = true)
    private String content;

    @NotNull(message="动态发布时间不能为空")
    @ApiModelProperty(value = "动态发布时间",required = true)
    private LocalDateTime timeAt;

    @NotEmpty(message="发布人用户id不能为空")
    @ApiModelProperty(value = "发布人用户id",required = true)
    private String ownerUserId;

    @NotEmpty(message="发布人用户昵称不能为空")
    @ApiModelProperty(value = "发布人用户昵称",required = true)
    private String ownerUserNickname;

    @ApiModelProperty(value = "发布人用户头像")
    private String ownerUserAvatar;

    @NotNull(message="子一级评论数不能为空")
    @ApiModelProperty(value = "子一级评论数",required = true)
    private Integer commentCount;

    @NotNull(message="点赞数不能为空")
    @ApiModelProperty(value = "点赞数",required = true)
    private Integer starCount;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "链接图片地址")
    private String linkPicUrl;

    @ApiModelProperty(value = "链接内容")
    private String linkContent;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

}
