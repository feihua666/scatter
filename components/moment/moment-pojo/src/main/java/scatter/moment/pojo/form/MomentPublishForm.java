package scatter.moment.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 时刻动态发布添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="时刻动态发布添加表单对象")
public class MomentPublishForm extends BaseAddForm {

    @NotEmpty(message="动态内容不能为空")
    @ApiModelProperty(value = "动态内容",required = true)
    private String content;

    @ApiModelProperty(value = "链接图片地址")
    private String linkPicUrl;

    @ApiModelProperty(value = "链接内容")
    private String linkContent;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "图片地址")
    private List<String> momentPicUrls;

}
