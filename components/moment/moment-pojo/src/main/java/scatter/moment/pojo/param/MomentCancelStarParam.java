package scatter.moment.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-26 11:03
 */
@Setter
@Getter
public class MomentCancelStarParam {

	@NotEmpty(message="时刻动态id不能为空")
	@ApiModelProperty(value = "时刻动态id",required = true)
	private String momentId;

	@NotEmpty(message="点赞人用户id不能为空")
	@ApiModelProperty(value = "点赞人用户id",required = true)
	private String ownerUserId;

}
