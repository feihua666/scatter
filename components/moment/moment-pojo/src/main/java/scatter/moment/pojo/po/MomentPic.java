package scatter.moment.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 时刻动态图片表
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_moment_pic")
@ApiModel(value="MomentPic对象", description="时刻动态图片表")
public class MomentPic extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_MOMENTPIC_BY_ID = "trans_momentpic_by_id_scatter.moment.pojo.po";

    @ApiModelProperty(value = "时刻动态id")
    private String momentId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "排序，默认升序")
    private Integer seq;


}
