package scatter.moment.pojo.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.comment.pojo.subject.vo.CommentSubjectVo;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.impl.TableNameTransServiceImpl;
import scatter.moment.pojo.po.MomentPic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-26 00:10
 */
@Setter
@Getter
@ApiModel(value="可用时刻动态响应对象")
public class AvailableMomentVo extends MomentVo{


	@TransBy(type = TableNameTransServiceImpl.TRANS_BY_TABLE_NAME,byFieldName = "id",tableField="momentId",mapValueField = "picUrl",tableNameClass = MomentPic.class,isJoin = false)
	@ApiModelProperty(value = "图片")
	private List<String> momentPics;
	/**
	 * 额外信息，需要单独处理
	 * 扩展信息并行处理，这里使用防并发 ConcurrentHashMap
	 */
	@ApiModelProperty(value = "额外信息")
	private Map<String,Object> ext = new HashMap<>();

	@ApiModelProperty(value = "我是否已点过赞")
	private Boolean isMeHasStared;

	@ApiModelProperty(value = "最新的点赞数据")
	private IPage<MomentStarVo> latestMomentStars;

	@ApiModelProperty(value = "最新的评论数据")
	private IPage<CommentSubjectVo> latestCommentSubjects;
}
