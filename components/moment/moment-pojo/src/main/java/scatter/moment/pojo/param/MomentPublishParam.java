package scatter.moment.pojo.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 时刻动态添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="时刻动态添加表单对象")
public class MomentPublishParam extends BaseAddForm {

    @NotEmpty(message="动态内容不能为空")
    @ApiModelProperty(value = "动态内容",required = true)
    private String content;

    @NotEmpty(message="发布人用户id不能为空")
    @ApiModelProperty(value = "发布人用户id",required = true)
    private String ownerUserId;

    @NotEmpty(message="发布人用户昵称不能为空")
    @ApiModelProperty(value = "发布人用户昵称",required = true)
    private String ownerUserNickname;

    @ApiModelProperty(value = "发布人用户头像")
    private String ownerUserAvatar;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "链接图片地址")
    private String linkPicUrl;

    @ApiModelProperty(value = "链接内容")
    private String linkContent;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

    @ApiModelProperty(value = "图片地址")
    private List<String> momentPicUrls;

}
