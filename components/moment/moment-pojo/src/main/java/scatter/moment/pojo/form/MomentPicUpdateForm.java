package scatter.moment.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 时刻动态图片更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-02-09
 */
@Setter
@Getter
@ApiModel(value="时刻动态图片更新表单对象")
public class MomentPicUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="时刻动态id不能为空")
    @ApiModelProperty(value = "时刻动态id",required = true)
    private String momentId;

    @NotEmpty(message="图片地址不能为空")
    @ApiModelProperty(value = "图片地址",required = true)
    private String picUrl;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序，默认升序",required = true)
    private Integer seq;

}
