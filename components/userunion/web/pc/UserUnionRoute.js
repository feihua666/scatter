import UserUnionUrl from './UserUnionUrl.js'

const UserUnionRoute = [
    {
        path: UserUnionUrl.router.searchList,
        component: () => import('./element/UserUnionSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserUnionSearchList',
            name: '统一用户管理'
        }
    },
    {
        path: UserUnionUrl.router.add,
        component: () => import('./element/UserUnionAdd'),
        meta: {
            code:'UserUnionAdd',
            name: '统一用户添加'
        }
    },
    {
        path: UserUnionUrl.router.update,
        component: () => import('./element/UserUnionUpdate'),
        meta: {
            code:'UserUnionUpdate',
            name: '统一用户修改'
        }
    },
]
export default UserUnionRoute