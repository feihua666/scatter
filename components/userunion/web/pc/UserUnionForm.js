import UserUnionUrl from './UserUnionUrl.js'
const UserUnionForm = [
    {
        UserUnionSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'nickname',
        },
        element:{
            label: '昵称',
            required: true,
        }
    },
    {

        field: {
            name: 'genderDictId',
        },
        element:{
            label: '性别',
        }
    },
    {

        field: {
            name: 'avatar',
        },
        element:{
            label: '头像',
        }
    },
    {
        UserUnionSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'isLock',
            value: false,
        },
        element:{
            type: 'switch',
            label: '锁定状态',
            required: true,
        }
    },
    {

        field: {
            name: 'lockReason',
        },
        element:{
            label: '锁定原因',
        }
    },
    {

        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
    {
        UserUnionSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'sourceFromDictId'
        },
        element:{
            label: '用户来源',
            type: 'selectDict',
            options: {
                groupCode: 'user_source_from'
            }
        }
    },
]
export default UserUnionForm