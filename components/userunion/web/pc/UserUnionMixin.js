import UserUnionForm from './UserUnionForm.js'
import UserUnionTable from './UserUnionTable.js'
import UserUnionUrl from './UserUnionUrl.js'
const UserUnionMixin = {
    computed: {
        UserUnionFormOptions() {
            return this.$stDynamicFormTools.options(UserUnionForm,this.$options.name)
        },
        UserUnionTableOptions() {
            return UserUnionTable
        },
        UserUnionUrl(){
            return UserUnionUrl
        }
    },
}
export default UserUnionMixin