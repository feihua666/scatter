const UserUnionTable = [
    {
        prop: 'nickname',
        label: '昵称'
    },
    {
        prop: 'genderDictId',
        label: '性别'
    },
    {
        prop: 'avatar',
        label: '头像'
    },
    {
        prop: 'isLock',
        label: '锁定状态'
    },
    {
        prop: 'lockReason',
        label: '锁定原因'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'sourceFromDictName',
        label: '用户来源'
    },
]
export default UserUnionTable