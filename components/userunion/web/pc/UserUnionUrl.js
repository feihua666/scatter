const basePath = '' + '/user-union'
const UserUnionUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserUnionSearchList',
        add: '/UserUnionAdd',
        update: '/UserUnionUpdate',
    }
}
export default UserUnionUrl