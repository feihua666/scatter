package scatter.userunion.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;

import java.time.LocalDateTime;


/**
 * <p>
 * 统一用户响应对象
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Setter
@Getter
@ApiModel(value="统一用户响应对象")
public class UserUnionVo extends BaseIdVo {

    @ApiModelProperty(value = "昵称，姓名,模糊查询")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定")
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceFromDictId",mapValueField="name")
    @ApiModelProperty(value = "用户来源，字典名称")
    private String sourceFromDictName;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDateTime lastLoginAt;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;

}
