package scatter.userunion.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 统一用户表
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_user_union")
@ApiModel(value="UserUnion对象", description="统一用户表")
public class UserUnion extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERUNION_BY_ID = "trans_userunion_by_id_scatter.userunion.pojo.po";

    /**
     * groupFlag可能的值，仅选用
     */
    public enum GroupFlag{
        // 后台
        backend,
        // 前台
        frontend,
        // app
        app,
        // h5
        h5,
        // 小程序
        miniProgram
    }

    @ApiModelProperty(value = "昵称，姓名,模糊查询")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定")
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDateTime lastLoginAt;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;

}
