package scatter.userunion.rest.mapper;

import scatter.userunion.pojo.po.UserUnion;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 统一用户表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
public interface UserUnionMapper extends IBaseMapper<UserUnion> {

}
