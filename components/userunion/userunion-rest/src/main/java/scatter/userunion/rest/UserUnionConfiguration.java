package scatter.userunion.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.userunion.rest.login.UserUnionLoginUserExtPutServiceImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.userunion.rest.**.mapper")
public class UserUnionConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "userunion";


    @Bean
    @ConditionalOnMissingBean
    public UserUnionLoginUserExtPutServiceImpl userUnionLoginUserExtPutServiceImpl(){
        return new UserUnionLoginUserExtPutServiceImpl();
    }
}
