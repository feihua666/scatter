package scatter.userunion.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.common.rest.validation.DictService;
import scatter.common.trans.TransConstants;
import scatter.common.trans.UserForTrans;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.rest.service.IUserUnionService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 统一用户翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Component
public class UserUnionTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private DictService dictService;
    @Autowired
    private IUserUnionService userUnionService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,UserUnion.TRANS_USERUNION_BY_ID, TransConstants.TRANS_USER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserUnion.TRANS_USERUNION_BY_ID)) {
            UserUnion byId = userUnionService.getById(key);
            return new TransResult(byId,key);
        }else if (isEqual(type,TransConstants.TRANS_USER_BY_ID)) {
            UserUnion byId = userUnionService.getById(key);
            return new TransResult(userUnionMapUserForTrans(byId),key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserUnion.TRANS_USERUNION_BY_ID)) {
            return userUnionService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if (isEqual(type,TransConstants.TRANS_USER_BY_ID)) {
            return userUnionService.listByIds(keys).stream().map(item->new TransResult<Object, String>(userUnionMapUserForTrans(item),item.getId())).collect(Collectors.toList());
        }
        return null;
    }


    /**
     * 转换实体
     * @param userUnion
     * @return
     */
    private UserForTrans userUnionMapUserForTrans(UserUnion userUnion){
        UserForTrans userForTrans = new UserForTrans();
        userForTrans.setId(userUnion.getId());
        // userUnion 不支持真实姓名
        userForTrans.setName(null);
        userForTrans.setNickname(userUnion.getNickname());
        userForTrans.setAvatar(userUnion.getAvatar());
        String valueById = dictService.getValueById(userUnion.getGenderDictId());
        String nameById = dictService.getNameById(userUnion.getGenderDictId());
        userForTrans.setGenderName(nameById);
        userForTrans.setGenderValue(valueById);
        return userForTrans;
    }
}
