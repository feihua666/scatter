package scatter.userunion.rest.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.LoginUser;
import scatter.common.rest.security.LoginUserExtPutService;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.rest.service.IUserUnionService;

/**
 * 添加登录用户信息
 * Created by yangwei
 * Created at 2021/9/17 11:09
 */
@Slf4j
public class UserUnionLoginUserExtPutServiceImpl implements LoginUserExtPutService {

    public static String LOGIN_USER_EXT_KEY_USER = "userUnion";

    @Autowired
    private IUserUnionService iUserUnionService;

    @Override
    public void addExt(LoginUser user) {
        UserUnion byId = iUserUnionService.getById(user.getId());
        if (byId == null) {
            log.warn("用户登录填充额外信息失败，因为未根据 userId 获取到 UserUnion 实体，userId={}",user.getId());
            return;
        }
        user.setAvatar(byId.getAvatar());
        user.setNickname(byId.getNickname());
        user.addExt(LOGIN_USER_EXT_KEY_USER,byId);
    }
}
