package scatter.userunion.rest.login;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.extra.servlet.ServletUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import scatter.common.LoginUser;
import scatter.common.rest.security.IAuthenticationResultService;
import scatter.common.rest.security.LoginUserTool;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.rest.service.IUserUnionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * identifier 登录事件
 * </p>
 *
 * @author yangwei
 * @since 2021-09-28 10:26
 */
@Slf4j
@Component
public class UserUnionAuthenticationResultServiceImpl implements IAuthenticationResultService {

	@Autowired
	private IUserUnionService iUserUnionService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {
		LoginUser loginUser = LoginUserTool.getLoginUser();
		if (loginUser != null) {

			log.info("用户认证成功，更新用户 userUnion 登录时间和ip，userId={}",loginUser.getId());
			// 更新上次登录时间和登录ip
			UserUnion userUnion = new UserUnion();
			userUnion.setId(loginUser.getId());

			userUnion.setLastLoginAt(LocalDateTimeUtil.now());
			userUnion.setLastLoginIp(ServletUtil.getClientIP(httpServletRequest));
			iUserUnionService.updateById(userUnion);
		}
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {

	}
}
