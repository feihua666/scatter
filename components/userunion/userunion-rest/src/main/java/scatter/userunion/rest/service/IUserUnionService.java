package scatter.userunion.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.userunion.pojo.po.UserUnion;
import scatter.common.rest.service.IBaseService;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.form.UserUnionUpdateForm;
import scatter.userunion.pojo.form.UserUnionPageQueryForm;
import java.util.List;
/**
 * <p>
 * 统一用户表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
public interface IUserUnionService extends IBaseService<UserUnion> {


}
