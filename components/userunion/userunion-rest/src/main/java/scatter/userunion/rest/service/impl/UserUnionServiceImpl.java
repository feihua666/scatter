package scatter.userunion.rest.service.impl;

import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.rest.mapper.UserUnionMapper;
import scatter.userunion.rest.service.IUserUnionService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.form.UserUnionUpdateForm;
import scatter.userunion.pojo.form.UserUnionPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 统一用户表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Service
@Transactional
public class UserUnionServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserUnionMapper, UserUnion, UserUnionAddForm, UserUnionUpdateForm, UserUnionPageQueryForm> implements IUserUnionService {
    @Override
    public void preAdd(UserUnionAddForm addForm,UserUnion po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserUnionUpdateForm updateForm,UserUnion po) {
        super.preUpdate(updateForm,po);

    }
}
