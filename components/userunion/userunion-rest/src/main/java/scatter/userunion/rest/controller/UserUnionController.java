package scatter.userunion.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.userunion.rest.UserUnionConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.pojo.vo.UserUnionVo;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.form.UserUnionUpdateForm;
import scatter.userunion.pojo.form.UserUnionPageQueryForm;
import scatter.userunion.rest.service.IUserUnionService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 统一用户表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Api(tags = "统一用户相关接口")
@RestController
@RequestMapping(UserUnionConfiguration.CONTROLLER_BASE_PATH + "/user-union")
public class UserUnionController extends BaseAddUpdateQueryFormController<UserUnion, UserUnionVo, UserUnionAddForm, UserUnionUpdateForm, UserUnionPageQueryForm> {
    @Autowired
    private IUserUnionService iUserUnionService;

     @Override
	 @ApiOperation("添加统一用户")
     @PreAuthorize("hasAuthority('UserUnion:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserUnionVo add(@RequestBody @Valid UserUnionAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询统一用户")
     @PreAuthorize("hasAuthority('UserUnion:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserUnionVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除统一用户")
     @PreAuthorize("hasAuthority('UserUnion:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新统一用户")
     @PreAuthorize("hasAuthority('UserUnion:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserUnionVo update(@RequestBody @Valid UserUnionUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询统一用户")
    @PreAuthorize("hasAuthority('UserUnion:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserUnionVo> getList(UserUnionPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询统一用户")
    @PreAuthorize("hasAuthority('UserUnion:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserUnionVo> getPage(UserUnionPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
