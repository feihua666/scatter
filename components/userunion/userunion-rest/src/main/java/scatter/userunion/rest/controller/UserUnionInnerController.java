package scatter.userunion.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.userunion.rest.UserUnionConfiguration;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.rest.service.IUserUnionService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 统一用户表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@RestController
@RequestMapping(UserUnionConfiguration.CONTROLLER_BASE_PATH + "/inner/user-union")
public class UserUnionInnerController extends BaseInnerController<UserUnion> {
 @Autowired
 private IUserUnionService userUnionService;

 public IUserUnionService getService(){
     return userUnionService;
 }
}
