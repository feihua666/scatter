package scatter.userunion.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.pojo.vo.UserUnionVo;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.form.UserUnionUpdateForm;
import scatter.userunion.pojo.form.UserUnionPageQueryForm;

/**
 * <p>
 * 统一用户 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserUnionMapStruct extends IBaseVoMapStruct<UserUnion, UserUnionVo>,
                                  IBaseAddFormMapStruct<UserUnion,UserUnionAddForm>,
                                  IBaseUpdateFormMapStruct<UserUnion,UserUnionUpdateForm>,
                                  IBaseQueryFormMapStruct<UserUnion,UserUnionPageQueryForm>{
    UserUnionMapStruct INSTANCE = Mappers.getMapper( UserUnionMapStruct.class );

}
