package scatter.userunion.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.form.UserUnionUpdateForm;
import scatter.userunion.pojo.form.UserUnionPageQueryForm;
import scatter.userunion.rest.service.IUserUnionService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 统一用户 测试类基类
* </p>
*
* @author yw
* @since 2021-03-23
*/
public class UserUnionSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserUnionService userUnionService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userUnionService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userUnionService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserUnion mockPo() {
        return JMockData.mock(UserUnion.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserUnionAddForm mockAddForm() {
        return JMockData.mock(UserUnionAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserUnionUpdateForm mockUpdateForm() {
        return JMockData.mock(UserUnionUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserUnionPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserUnionPageQueryForm.class, mockConfig);
    }
}