package scatter.userunion.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.userunion.pojo.po.UserUnion;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.form.UserUnionUpdateForm;
import scatter.userunion.pojo.form.UserUnionPageQueryForm;
import scatter.userunion.rest.test.UserUnionSuperTest;
import scatter.userunion.rest.service.IUserUnionService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 统一用户 服务测试类
* </p>
*
* @author yw
* @since 2021-03-23
*/
@SpringBootTest
public class UserUnionServiceTest extends UserUnionSuperTest{

    @Autowired
    private IUserUnionService userUnionService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserUnion> pos = userUnionService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}