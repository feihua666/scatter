package scatter.userunion.rest.test.controller;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.userunion.rest.test.UserUnionSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.userunion.pojo.form.UserUnionAddForm;
import scatter.userunion.pojo.vo.UserUnionVo;
import scatter.userunion.rest.UserUnionConfiguration;
/**
* <p>
* 统一用户 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-03-23
*/
@SpringBootTest
@AutoConfigureMockMvc
public class UserUnionControllerTest extends UserUnionSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = UserUnionConfiguration.CONTROLLER_BASE_PATH + "/user-union";

        // 请求表单
        UserUnionAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        UserUnionVo vo = JSONUtil.toBean(response.getContentAsString(), UserUnionVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}