package scatter.userunion.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 统一用户表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-03-23
 */
@Component
@FeignClient(value = "UserUnion-client")
public interface UserUnionClient {

}
