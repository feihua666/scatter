package scatter.func.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 功能组表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_func_group")
@ApiModel(value="FuncGroup对象", description="功能组表")
public class FuncGroup extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_FUNCGROUP_BY_ID = "trans_funcgroup_by_id_scatter.func.pojo.po";

    @ApiModelProperty(value = "编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "描述")
    private String remark;


}
