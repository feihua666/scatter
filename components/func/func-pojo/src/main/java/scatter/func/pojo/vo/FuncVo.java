package scatter.func.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.func.pojo.po.FuncGroup;


/**
 * <p>
 * 菜单功能响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="菜单功能响应对象")
public class FuncVo extends BaseIdVo {

    @ApiModelProperty(value = "编码，模糊查询")
    private String code;

    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "功能组id")
    private String funcGroupId;

    @TransBy(type = FuncGroup.TRANS_FUNCGROUP_BY_ID,byFieldName = "funcGroupId",mapValueField = "name")
    @ApiModelProperty(value = "功能组名称")
    private String funcGroupName;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "地址")
    private String url;

    @ApiModelProperty(value = "shiro权限串，多个以逗号分隔")
    private String permissions;

    @ApiModelProperty(value = "类型,字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="value")
    @ApiModelProperty(value = "类型,字典编码")
    private String typeDictValue;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="name")
    @ApiModelProperty(value = "类型,字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
