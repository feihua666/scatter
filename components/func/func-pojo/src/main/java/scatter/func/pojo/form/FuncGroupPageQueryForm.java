package scatter.func.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 功能组分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="功能组分页表单对象")
public class FuncGroupPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "编码，模糊查询")
    private String code;

    @Like
    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "描述")
    private String remark;

}
