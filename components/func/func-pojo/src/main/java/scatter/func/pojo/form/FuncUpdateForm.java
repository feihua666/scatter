package scatter.func.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 菜单功能更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@PropValid
@Setter
@Getter
@ApiModel(value="菜单功能更新表单对象")
public class FuncUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码，模糊查询",required = true)
    private String code;

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @ApiModelProperty(value = "功能分组id")
    private String funcGroupId;

    @ApiModelProperty(value = "图标")
    private String icon;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "地址")
    private String url;

    @ApiModelProperty(value = "shiro权限串，多个以逗号分隔")
    private String permissions;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型,字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序,默认按该字段升序排序",required = true)
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
