package scatter.func.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 菜单功能表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
@FeignClient(value = "Func-client")
public interface FuncClient {

}
