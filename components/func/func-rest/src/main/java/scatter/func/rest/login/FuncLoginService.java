package scatter.func.rest.login;

import scatter.func.pojo.po.Func;

import java.util.List;

/**
 * 获取用户的功能，获取当前登录用户的功能
 * Created by yangwei
 * Created at 2020/12/14 17:33
 */
public interface FuncLoginService {

    /**
     * 获取用户权限信息
     * @param userId
     * @return
     */
    List<Func> retrieveFuncsByUserId(String userId);
}
