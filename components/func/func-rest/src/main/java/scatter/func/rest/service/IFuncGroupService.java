package scatter.func.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.func.pojo.po.FuncGroup;
import scatter.common.rest.service.IBaseService;
import scatter.func.pojo.form.FuncGroupAddForm;
import scatter.func.pojo.form.FuncGroupUpdateForm;
import scatter.func.pojo.form.FuncGroupPageQueryForm;
/**
 * <p>
 * 功能组表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IFuncGroupService extends IBaseService<FuncGroup> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default FuncGroup getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<FuncGroup>lambdaQuery().eq(FuncGroup::getCode, code));
    }

}
