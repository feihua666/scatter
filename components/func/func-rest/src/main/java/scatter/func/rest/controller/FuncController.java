package scatter.func.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.func.rest.FuncConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.func.pojo.po.Func;
import scatter.func.pojo.vo.FuncVo;
import scatter.func.pojo.form.FuncAddForm;
import scatter.func.pojo.form.FuncUpdateForm;
import scatter.func.pojo.form.FuncPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 菜单功能表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(FuncConfiguration.CONTROLLER_BASE_PATH + "/func")
@Api(tags = "菜单功能")
public class FuncController extends BaseAddUpdateQueryFormController<Func, FuncVo, FuncAddForm, FuncUpdateForm, FuncPageQueryForm> {


     @Override
	 @ApiOperation("添加菜单功能")
     @PreAuthorize("hasAuthority('Func:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public FuncVo add(@RequestBody @Valid FuncAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询菜单功能")
     @PreAuthorize("hasAuthority('Func:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public FuncVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除菜单功能")
     @PreAuthorize("hasAuthority('Func:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新菜单功能")
     @PreAuthorize("hasAuthority('Func:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public FuncVo update(@RequestBody @Valid FuncUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询菜单功能")
    @PreAuthorize("hasAuthority('Func:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<FuncVo> getList(FuncPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询菜单功能")
    @PreAuthorize("hasAuthority('Func:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<FuncVo> getPage(FuncPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
