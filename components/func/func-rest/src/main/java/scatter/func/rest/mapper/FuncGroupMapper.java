package scatter.func.rest.mapper;

import scatter.func.pojo.po.FuncGroup;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 功能组表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface FuncGroupMapper extends IBaseMapper<FuncGroup> {

}
