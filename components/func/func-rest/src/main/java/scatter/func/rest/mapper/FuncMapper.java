package scatter.func.rest.mapper;

import scatter.func.pojo.po.Func;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 菜单功能表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface FuncMapper extends IBaseMapper<Func> {

}
