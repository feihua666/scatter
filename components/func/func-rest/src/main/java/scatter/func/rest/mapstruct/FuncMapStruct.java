package scatter.func.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.func.pojo.form.FuncAddForm;
import scatter.func.pojo.form.FuncPageQueryForm;
import scatter.func.pojo.form.FuncUpdateForm;
import scatter.func.pojo.po.Func;
import scatter.func.pojo.vo.FuncVo;

/**
 * <p>
 * 菜单功能 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FuncMapStruct extends IBaseVoMapStruct<Func, FuncVo>,
        IBaseAddFormMapStruct<Func,FuncAddForm>,
        IBaseUpdateFormMapStruct<Func,FuncUpdateForm>,
        IBaseQueryFormMapStruct<Func,FuncPageQueryForm> {
    FuncMapStruct INSTANCE = Mappers.getMapper( FuncMapStruct.class );

}
