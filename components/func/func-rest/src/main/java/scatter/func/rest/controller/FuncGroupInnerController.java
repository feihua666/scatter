package scatter.func.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.func.pojo.po.FuncGroup;
import scatter.func.rest.service.IFuncGroupService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 功能组表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/func-group")
public class FuncGroupInnerController extends BaseInnerController<FuncGroup> {
 @Autowired
 private IFuncGroupService funcGroupService;

 public IFuncGroupService getService(){
     return funcGroupService;
 }
}
