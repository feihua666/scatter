package scatter.func.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.func.rest.FuncConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.func.pojo.po.FuncGroup;
import scatter.func.pojo.vo.FuncGroupVo;
import scatter.func.pojo.form.FuncGroupAddForm;
import scatter.func.pojo.form.FuncGroupUpdateForm;
import scatter.func.pojo.form.FuncGroupPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 功能组表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(FuncConfiguration.CONTROLLER_BASE_PATH + "/func-group")
@Api(tags = "功能分组")
public class FuncGroupController extends BaseAddUpdateQueryFormController<FuncGroup, FuncGroupVo, FuncGroupAddForm, FuncGroupUpdateForm, FuncGroupPageQueryForm> {


     @Override
	 @ApiOperation("添加功能组")
     @PreAuthorize("hasAuthority('FuncGroup:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public FuncGroupVo add(@RequestBody @Valid FuncGroupAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询功能组")
     @PreAuthorize("hasAuthority('FuncGroup:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public FuncGroupVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除功能组")
     @PreAuthorize("hasAuthority('FuncGroup:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新功能组")
     @PreAuthorize("hasAuthority('FuncGroup:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public FuncGroupVo update(@RequestBody @Valid FuncGroupUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询功能组")
    @PreAuthorize("hasAuthority('FuncGroup:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<FuncGroupVo> getList(FuncGroupPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询功能组")
    @PreAuthorize("hasAuthority('FuncGroup:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<FuncGroupVo> getPage(FuncGroupPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
