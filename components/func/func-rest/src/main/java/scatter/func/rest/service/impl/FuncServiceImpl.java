package scatter.func.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.func.pojo.form.FuncAddForm;
import scatter.func.pojo.form.FuncPageQueryForm;
import scatter.func.pojo.form.FuncUpdateForm;
import scatter.func.pojo.po.Func;
import scatter.func.rest.mapper.FuncMapper;
import scatter.func.rest.service.IFuncService;
/**
 * <p>
 * 菜单功能表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class FuncServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<FuncMapper, Func, FuncAddForm, FuncUpdateForm, FuncPageQueryForm> implements IFuncService {
    @Override
    public void preAdd(FuncAddForm addForm,Func po) {
        super.preAdd(addForm,po);
        // 添加默认不禁用
        po.setIsDisabled(false);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),Func::getCode,false);
        }

    }

    @Override
    public void preUpdate(FuncUpdateForm updateForm,Func po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            Func byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),Func::getCode,false);
            }
        }

    }
}
