package scatter.func.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.func.pojo.form.FuncGroupAddForm;
import scatter.func.pojo.form.FuncGroupPageQueryForm;
import scatter.func.pojo.form.FuncGroupUpdateForm;
import scatter.func.pojo.po.FuncGroup;
import scatter.func.pojo.vo.FuncGroupVo;

/**
 * <p>
 * 功能组 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FuncGroupMapStruct extends IBaseVoMapStruct<FuncGroup, FuncGroupVo>,
        IBaseAddFormMapStruct<FuncGroup,FuncGroupAddForm>,
        IBaseUpdateFormMapStruct<FuncGroup,FuncGroupUpdateForm>,
        IBaseQueryFormMapStruct<FuncGroup,FuncGroupPageQueryForm> {
    FuncGroupMapStruct INSTANCE = Mappers.getMapper( FuncGroupMapStruct.class );

}
