package scatter.func.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.func.pojo.form.FuncGroupAddForm;
import scatter.func.pojo.form.FuncGroupPageQueryForm;
import scatter.func.pojo.form.FuncGroupUpdateForm;
import scatter.func.pojo.po.FuncGroup;
import scatter.func.rest.mapper.FuncGroupMapper;
import scatter.func.rest.service.IFuncGroupService;
/**
 * <p>
 * 功能组表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class FuncGroupServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<FuncGroupMapper, FuncGroup, FuncGroupAddForm, FuncGroupUpdateForm, FuncGroupPageQueryForm> implements IFuncGroupService {
    @Override
    public void preAdd(FuncGroupAddForm addForm,FuncGroup po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),FuncGroup::getCode,false);
        }

    }

    @Override
    public void preUpdate(FuncGroupUpdateForm updateForm,FuncGroup po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            FuncGroup byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),FuncGroup::getCode,false);
            }
        }

    }
}
