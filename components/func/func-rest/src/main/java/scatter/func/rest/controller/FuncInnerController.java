package scatter.func.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.func.pojo.po.Func;
import scatter.func.rest.service.IFuncService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 菜单功能表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/func")
public class FuncInnerController extends BaseInnerController<Func> {
 @Autowired
 private IFuncService funcService;

 public IFuncService getService(){
     return funcService;
 }
}
