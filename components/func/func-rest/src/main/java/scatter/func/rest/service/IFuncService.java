package scatter.func.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.func.pojo.po.Func;
/**
 * <p>
 * 菜单功能表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IFuncService extends IBaseService<Func> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Func getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Func>lambdaQuery().eq(Func::getCode, code));
    }

}
