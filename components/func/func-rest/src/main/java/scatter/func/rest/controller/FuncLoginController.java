package scatter.func.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.common.rest.controller.SuperController;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import scatter.func.pojo.form.FuncAddForm;
import scatter.func.pojo.form.FuncPageQueryForm;
import scatter.func.pojo.form.FuncUpdateForm;
import scatter.func.pojo.po.Func;
import scatter.func.pojo.vo.FuncVo;
import scatter.func.rest.FuncConfiguration;
import scatter.func.rest.login.FuncLoginService;
import scatter.func.rest.mapstruct.FuncMapStruct;
import scatter.func.rest.service.IFuncService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 菜单功能表 前端控制器 登录用户相关
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(FuncConfiguration.CONTROLLER_BASE_PATH + "/func/login")
@Api(tags = "菜单功能，登录用户相关")
public class FuncLoginController extends SuperController {


    @Autowired
    private IFuncService iFuncService;
    @Autowired(required = false)
    private FuncLoginService funcLoginService;

    @ApiOperation("当前登录用户的功能")
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<FuncVo> getList(@ApiIgnore LoginUser loginUser) {
        if (loginUser.getIsSuperAdmin()) {
            List<Func> list = iFuncService.list(Wrappers.<Func>lambdaQuery().eq(Func::getIsDisabled, false));
            if (isEmpty(list)) {
                throw new BusinessDataNotFoundException(true);
            }
            return FuncMapStruct.INSTANCE.posToVos(list);
        }
        if (funcLoginService == null) {
            throw new BusinessDataNotFoundException(true);
        }
        List<Func> funcs = funcLoginService.retrieveFuncsByUserId(loginUser.getId());
        if (isEmpty(funcs)) {
            throw new BusinessDataNotFoundException(true);
        }
        return FuncMapStruct.INSTANCE.posToVos(funcs);
    }

}
