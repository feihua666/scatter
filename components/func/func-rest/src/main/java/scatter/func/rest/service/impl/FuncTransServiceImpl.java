package scatter.func.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.func.pojo.po.Func;
import scatter.func.rest.service.IFuncService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单功能翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
public class FuncTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IFuncService funcService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Func.TRANS_FUNC_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Func.TRANS_FUNC_BY_ID)) {
            Func byId = funcService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Func.TRANS_FUNC_BY_ID)) {
            return funcService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
