package scatter.func.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.func.pojo.po.FuncGroup;
import scatter.func.pojo.form.FuncGroupAddForm;
import scatter.func.pojo.form.FuncGroupUpdateForm;
import scatter.func.pojo.form.FuncGroupPageQueryForm;
import scatter.func.rest.service.IFuncGroupService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 功能组 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class FuncGroupSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IFuncGroupService funcGroupService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return funcGroupService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return funcGroupService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public FuncGroup mockPo() {
        return JMockData.mock(FuncGroup.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public FuncGroupAddForm mockAddForm() {
        return JMockData.mock(FuncGroupAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public FuncGroupUpdateForm mockUpdateForm() {
        return JMockData.mock(FuncGroupUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public FuncGroupPageQueryForm mockPageQueryForm() {
        return JMockData.mock(FuncGroupPageQueryForm.class, mockConfig);
    }
}