package scatter.func.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.func.pojo.po.Func;
import scatter.func.pojo.form.FuncAddForm;
import scatter.func.pojo.form.FuncUpdateForm;
import scatter.func.pojo.form.FuncPageQueryForm;
import scatter.func.rest.service.IFuncService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 菜单功能 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class FuncSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IFuncService funcService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return funcService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return funcService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Func mockPo() {
        return JMockData.mock(Func.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public FuncAddForm mockAddForm() {
        return JMockData.mock(FuncAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public FuncUpdateForm mockUpdateForm() {
        return JMockData.mock(FuncUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public FuncPageQueryForm mockPageQueryForm() {
        return JMockData.mock(FuncPageQueryForm.class, mockConfig);
    }
}