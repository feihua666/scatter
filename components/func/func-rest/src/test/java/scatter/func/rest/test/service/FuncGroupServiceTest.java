package scatter.func.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.func.pojo.po.FuncGroup;
import scatter.func.pojo.form.FuncGroupAddForm;
import scatter.func.pojo.form.FuncGroupUpdateForm;
import scatter.func.pojo.form.FuncGroupPageQueryForm;
import scatter.func.rest.test.FuncGroupSuperTest;
import scatter.func.rest.service.IFuncGroupService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 功能组 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class FuncGroupServiceTest extends FuncGroupSuperTest{

    @Autowired
    private IFuncGroupService funcGroupService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
        String identifier;
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<FuncGroup> pos = funcGroupService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}