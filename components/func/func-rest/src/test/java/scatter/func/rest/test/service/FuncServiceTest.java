package scatter.func.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.func.pojo.po.Func;
import scatter.func.pojo.form.FuncAddForm;
import scatter.func.pojo.form.FuncUpdateForm;
import scatter.func.pojo.form.FuncPageQueryForm;
import scatter.func.rest.test.FuncSuperTest;
import scatter.func.rest.service.IFuncService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 菜单功能 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class FuncServiceTest extends FuncSuperTest{

    @Autowired
    private IFuncService funcService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Func> pos = funcService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}