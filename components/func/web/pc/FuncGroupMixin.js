import FuncGroupForm from './FuncGroupForm.js'
import FuncGroupTable from './FuncGroupTable.js'
import FuncGroupUrl from './FuncGroupUrl.js'

const FuncGroupMixin = {
    computed: {
        FuncGroupFormOptions() {
            return this.$stDynamicFormTools.options(FuncGroupForm,this.$options.name)
        },
        FuncGroupTableOptions() {
            return FuncGroupTable
        },
        FuncGroupUrl(){
            return FuncGroupUrl
        }
    },
}
export default FuncGroupMixin