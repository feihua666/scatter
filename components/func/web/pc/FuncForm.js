import FuncUrl from './FuncUrl.js'
import FuncGroupUrl from './FuncGroupUrl.js'

const FuncForm = [
    {
        FuncSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'typeDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'func_type'
            },
            label: '类型',
            required: true,
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: FuncUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },
    {
        field: {
            name: 'funcGroupId'
        },
        element:{
            type: 'select',
            options: {
                datas: FuncGroupUrl.list
            },
            label: '功能分组',
            disabled: ({form,formExtData})=>{
                if ( formExtData && formExtData.parentId) {
                    form.funcGroupId = formExtData.parentId.funcGroupId
                }

                if (form.parentId) {
                    return true
                }
                return false
            },
            required: false
        }
    },
    {
        FuncSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        FuncSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '名称',
            required: true,
        }
    },

    {
        FuncSearchList: false,
        field: {
            name: 'icon',
            value: 'el-icon-document-copy'
        },
        element:{
            label: '图标',
            options: {
                placeholder: '类名如：el-icon-phone'
            },
            tips: '<a href="https://element.eleme.cn/#/zh-CN/component/icon" target="_blank">支持的图标</a>'
        }
    },
    {
        FuncSearchList: false,
        FuncAdd: false,
        field: {
            name: 'isDisabled',
            value: false
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        FuncSearchList: false,
        FuncAdd: false,
        field: {
            name: 'disabledReason'
        },
        element:{
            label: '禁用原因',
            disabled: ({form})=>{
                let r = !form.isDisabled
                if(r){
                    form.disabledReason = null
                }
                return r
            },
            required: ({form})=>{
                let r = form.isDisabled
                return r
            }
        }
    },
    {
        FuncSearchList: false,
        field: {
            name: 'url'
        },
        element:{
            label: '地址',
            disabled: ({form,formExtData})=>{
                if ( formExtData && formExtData.typeDictId && formExtData.typeDictId.value == 'page') {
                    return false
                }
                return true
            },
            required: ({form,formExtData})=>{
                if (formExtData && formExtData.typeDictId && formExtData.typeDictId.value == 'page') {
                    return true
                }
                // 在修改数据回显时，可能会因为类型的值影响地址清空，这里判断一下
                if (formExtData && formExtData.typeDictId){
                    form.url = ''
                }
                return false
            }
        }
    },
    {
        FuncSearchList: false,
        field: {
            name: 'permissions'
        },
        element:{
            label: '权限',
        }
    },

    {
        FuncSearchList: false,
        field: {
            name: 'seq',
            value: 10
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        }
    },

    {
        FuncSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default FuncForm