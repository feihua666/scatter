import FuncForm from './FuncForm.js'
import FuncTable from './FuncTable.js'
import FuncUrl from './FuncUrl.js'

const FuncMixin = {
    computed: {
        FuncFormOptions() {
            return this.$stDynamicFormTools.options(FuncForm,this.$options.name)
        },
        FuncTableOptions() {
            return FuncTable
        },
        FuncUrl(){
            return FuncUrl
        }
    },
}
export default FuncMixin