const FuncGroupTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default FuncGroupTable