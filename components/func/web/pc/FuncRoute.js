import FuncUrl from './FuncUrl.js'

const FuncRoute = [
    {
        path: FuncUrl.router.searchList,
        component: () => import('./element/FuncSearchList'),
        meta: {
            root: true,
            code:'FuncSearchList',
            name: '菜单功能管理',
            keepAlive: true
        }
    },
    {
        path: FuncUrl.router.add,
        component: () => import('./element/FuncAdd'),
        meta: {
            code:'FuncAdd',
            name: '菜单功能添加'
        }
    },
    {
        path: FuncUrl.router.update,
        component: () => import('./element/FuncUpdate'),
        meta: {
            code:'FuncUpdate',
            name: '菜单功能修改'
        }
    },
]
export default FuncRoute