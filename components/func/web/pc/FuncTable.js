const FuncTable = [
    {
        prop: 'id',
        label: 'id',
        showOverflowTooltip: true,
    },
    {
        prop: 'code',
        label: '编码',
        showOverflowTooltip: true,
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'funcGroupName',
        label: '功能分组'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
    {
        prop: 'icon',
        label: '图标',
        showOverflowTooltip: true,
    },
    {
        prop: 'isDisabled',
        label: '是否禁用',
        formatter(row){
            if(row.isDisabled){
                let r = '是'
                r += `(${row.disabledReason})`
                return r
            }else {
                return row.isDisabled
            }
        }
    },
    {
        prop: 'url',
        label: '地址',
        showOverflowTooltip: true,
    },
    {
        prop: 'permissions',
        label: '权限',
        showOverflowTooltip: true,
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'seq',
        label: '排序'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default FuncTable