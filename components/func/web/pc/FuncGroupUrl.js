const basePath = '' + '/func-group'
const FuncGroupUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/FuncGroupSearchList',
        add: '/FuncGroupAdd',
        update: '/FuncGroupUpdate',
    }
}
export default FuncGroupUrl