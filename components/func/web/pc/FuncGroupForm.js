const FuncGroupForm = [
    {
        FuncGroupSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'code'
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        FuncGroupSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'name'
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        FuncGroupSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default FuncGroupForm