import FuncGroupUrl from './FuncGroupUrl.js'

const FuncGroupRoute = [
    {
        path: FuncGroupUrl.router.searchList,
        component: () => import('./element/FuncGroupSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'FuncGroupSearchList',
            name: '功能组管理'
        }
    },
    {
        path: FuncGroupUrl.router.add,
        component: () => import('./element/FuncGroupAdd'),
        meta: {
            code:'FuncGroupAdd',
            name: '功能组添加'
        }
    },
    {
        path: FuncGroupUrl.router.update,
        component: () => import('./element/FuncGroupUpdate'),
        meta: {
            code:'FuncGroupUpdate',
            name: '功能组修改'
        }
    },
]
export default FuncGroupRoute