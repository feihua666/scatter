const basePath = '' + '/func'
const FuncUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/FuncSearchList',
        add: '/FuncAdd',
        update: '/FuncUpdate',
    }
}
export default FuncUrl