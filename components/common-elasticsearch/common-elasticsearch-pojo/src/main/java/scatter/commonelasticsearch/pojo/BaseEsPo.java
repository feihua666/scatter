package scatter.commonelasticsearch.pojo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import scatter.common.pojo.BasePojo;

/**
 * es的基础po类
 * Created by yangwei
 * Created at 2021/1/29 17:50
 */
@Setter
@Getter
public class BaseEsPo extends BasePojo {

    @Id
    @Field(type= FieldType.Auto)
    private String id;
}
