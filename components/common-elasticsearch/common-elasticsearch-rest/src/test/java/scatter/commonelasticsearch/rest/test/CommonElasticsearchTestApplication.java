package scatter.commonelasticsearch.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import scatter.common.rest.config.CommonRestConfig;
import scatter.commonelasticsearch.rest.CommonElasticsearchConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, CommonElasticsearchConfiguration.class})
public class CommonElasticsearchTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommonElasticsearchTestApplication.class, args);
    }
}
