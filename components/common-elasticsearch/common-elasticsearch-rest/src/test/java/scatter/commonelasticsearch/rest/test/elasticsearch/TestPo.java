package scatter.commonelasticsearch.rest.test.elasticsearch;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import scatter.commonelasticsearch.pojo.BaseEsPo;

/**
 * Created by yangwei
 * Created at 2021/1/29 18:01
 */
@Setter
@Getter
@Document(indexName = "scatter.commonelasticsearch.rest.test.elasticsearch.testpo_index")
public class TestPo extends BaseEsPo {
    private String name;
    private String nickname;

    private String homeAddress;

    private Boolean isLock;
    private Boolean isLock1;
    private boolean leave;
    private Integer age;
    private Integer age1;
    private int height;

}
