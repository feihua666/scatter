package scatter.commonelasticsearch.rest.test.elasticsearch.repository;

import scatter.commonelasticsearch.rest.repository.BaseEsRepository;
import scatter.commonelasticsearch.rest.test.elasticsearch.TestPo;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/29 18:00
 */
public interface TestRepository extends BaseEsRepository<TestPo> {

    /**
     * 根据名称名字查询
     * @param name
     * @return
     */
    List<TestPo> findByName(String name);
}
