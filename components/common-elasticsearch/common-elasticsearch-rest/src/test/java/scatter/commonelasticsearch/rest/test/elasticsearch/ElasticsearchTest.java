package scatter.commonelasticsearch.rest.test.elasticsearch;

import cn.hutool.json.JSONUtil;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import scatter.common.rest.tools.InterfaceTool;
import scatter.commonelasticsearch.rest.test.elasticsearch.repository.TestRepository;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/29 17:59
 */

@SpringBootTest
public class ElasticsearchTest implements InterfaceTool {

    @Autowired
    private TestRepository testRepository;

    @Test
    public void testSave() {
        TestPo testPo = new TestPo();
        //testPo.setId("testid");
        testPo.setName("name");
        testPo.setNickname("nickname");

        testPo.setHomeAddress("中国北京");
        testPo.setAge(18);
        testPo.setHeight(33);
        testPo.setIsLock(true);
        testPo.setLeave(true);


        TestPo save = testRepository.save(testPo);
        System.out.println(JSONUtil.toJsonStr(save));
        Assertions.assertNotNull(save);
    }

    @Test
    public void testQuery() {
        List<TestPo> me = testRepository.findByName("name");
        System.out.println(JSONUtil.toJsonStr(me));
        //Assertions.assertEquals(1,me.size());
    }

    @Test
    public void testAll() {
        Iterable<TestPo> me =testRepository.findAll();
        System.out.println(JSONUtil.toJsonStr(iterableToList(me)));
    }

    /**
     * 相似度查询
     */
    @Test
    public void testSimilar() {
        TestPo testPo = new TestPo();
        testPo.setId("testid");
        testPo.setHomeAddress("北京");
        Iterable<TestPo> me =testRepository.searchSimilar(testPo,new String[]{"homeAddress"},PageRequest.of(1,10));
        System.out.println(JSONUtil.toJsonStr(iterableToList(me)));
    }
}
