package scatter.commonelasticsearch.rest.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import scatter.commonelasticsearch.pojo.BaseEsPo;

/**
 * es 基础 repository
 * Created by yangwei
 * Created at 2021/1/29 17:53
 */
public interface BaseEsRepository<T extends BaseEsPo>  extends ElasticsearchRepository<T,String> {

}
