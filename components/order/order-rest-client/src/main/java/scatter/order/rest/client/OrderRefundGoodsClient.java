package scatter.order.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 退款商品表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Component
@FeignClient(value = "OrderRefundGoods-client")
public interface OrderRefundGoodsClient {

}
