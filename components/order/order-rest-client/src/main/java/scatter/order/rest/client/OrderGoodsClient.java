package scatter.order.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 订单商品表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Component
@FeignClient(value = "OrderGoods-client")
public interface OrderGoodsClient {

}
