package scatter.order.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 订单表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Component
@FeignClient(value = "Order-client")
public interface OrderClient {

}
