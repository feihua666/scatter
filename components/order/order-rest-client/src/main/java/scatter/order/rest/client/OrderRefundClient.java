package scatter.order.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 订单退款单表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Component
@FeignClient(value = "OrderRefund-client")
public interface OrderRefundClient {

}
