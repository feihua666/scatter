package scatter.order.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.dict.PublicDictEnums;
import scatter.common.pojo.form.BaseAddForm;
import scatter.order.pojo.constant.OrderConstants;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单关闭表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-27
 */
@Setter
@Getter
@ApiModel(value="订单关闭表单对象")
public class OrderCloseForm extends BaseAddForm {


    /**
     * 订单号
     */
    @NotEmpty(message = "订单号不能为空")
    @ApiModelProperty(value = "订单号")
    private String orderNo;

    /**
     * 原因
     */
    @NotEmpty(message = "原因不能为空")
    @ApiModelProperty(value = "原因")
    private String reason;
}
