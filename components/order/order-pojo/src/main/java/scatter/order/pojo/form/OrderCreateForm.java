package scatter.order.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.dict.PublicDictEnums;
import scatter.common.pojo.form.BaseAddForm;
import scatter.order.pojo.constant.OrderConstants;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单创建表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-27
 */
@Setter
@Getter
@ApiModel(value="订单创建表单对象")
public class OrderCreateForm extends BaseAddForm {


    /**
     * 订单渠道，主要是区分是哪了个渠道的订单，没有可以不填
     * 只是预留，暂未处理
     */
    @ApiModelProperty(value = "渠道")
    private String channelCode;

    @NotEmpty(message="订单标题不能为空")
    @ApiModelProperty(value = "订单标题，比description更精简",required = true)
    private String title;

    @ApiModelProperty(value = "订单描述，文本")
    private String description;

    @NotNull(message="订单总金额不能为空")
    @ApiModelProperty(value = "订单总金额，单位为分",required = true)
    private Integer totalAmount;

    /**
     * {@link PublicDictEnums.CurrencyTypeDictItem}
     */
    @ApiModelProperty(value = "货币类型,字典值")
    private String currencyDictValue;

    /**
     * {@link OrderConstants.OrderCategoryDictItem}
     */
    @NotEmpty(message="订单分类不能为空")
    @ApiModelProperty(value = "订单分类，字典值",required = true)
    private String categoryDictValue;

    /**
     * {@link OrderConstants.OrderPayTypeDictItem}
     */
    @NotEmpty(message="支付类型不能为空")
    @ApiModelProperty(value = "支付类型，字典值",required = true)
    private String payTypeDictValue;

    /**
     * {@link OrderConstants.OrderPayChannelDictItem}
     */
    @NotEmpty(message="支付渠道不能为空")
    @ApiModelProperty(value = "支付渠道，字典值",required = true)
    private String payChannelDictValue;


    @Valid
    @NotEmpty(message = "商品信息不能为空")
    @ApiModelProperty(value = "商品信息")
    private List<OrderCreateGoodsForm> goodsForms;


    /**
     * 自定义配置的微信公众号 mpAppCode
     * 如果不是微信公众号jsapi支付，可以不用填写
     */
    @ApiModelProperty(value = "自定义配置的微信公众号 mpAppCode")
    private String mpAppCode;

    /**
     * 额外信息
     */
    private Map<String,Object> ext;


}
