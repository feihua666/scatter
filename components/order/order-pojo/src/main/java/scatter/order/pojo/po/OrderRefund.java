package scatter.order.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单退款单表
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_order_refund")
@ApiModel(value="OrderRefund对象", description="订单退款单表")
public class OrderRefund extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ORDERREFUND_BY_ID = "trans_orderrefund_by_id_scatter.order.pojo.po";

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "退款单号")
    private String refundNo;

    @ApiModelProperty(value = "退款描述，文本")
    private String description;

    @ApiModelProperty(value = "退款金额，单位为分")
    private Integer amount;

    @ApiModelProperty(value = "是否分多次退款")
    private Boolean isMultiple;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @ApiModelProperty(value = "退款状态,字典id")
    private String statusDictId;

    @ApiModelProperty(value = "发起退款客户端ip")
    private String clientIp;


}
