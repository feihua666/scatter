package scatter.order.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 订单响应对象
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Setter
@Getter
@ApiModel(value="订单响应对象")
public class OrderVo extends BaseIdVo {

    // 为了解耦 并未实现翻译，需要自动实现
    public final static String TRANS_ORDER_USER_NICKNAME_BY_USER_ID = "Trans_order_user_Nickname_by_user_id";

    @ApiModelProperty(value = "订单标题，比description更精简")
    private String title;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "订单描述，文本")
    private String description;

    @ApiModelProperty(value = "订单失效时间，超时时间")
    private LocalDateTime expireAt;

    @ApiModelProperty(value = "订单总金额，单位为分")
    private Integer totalAmount;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "currencyDictId",mapValueField = "name")
    @ApiModelProperty(value = "货币类型,字典名称")
    private String currencyDictName;


    @ApiModelProperty(value = "订单状态，字典id")
    private String statusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "statusDictId",mapValueField = "name")
    @ApiModelProperty(value = "订单状态，字典名称")
    private String statusDictName;

    @ApiModelProperty(value = "订单分类，字典id")
    private String categoryDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "categoryDictId",mapValueField = "name")
    @ApiModelProperty(value = "订单分类，字典名称")
    private String categoryDictName;

    @ApiModelProperty(value = "付款方式，字典id")
    private String payTypeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "payTypeDictId",mapValueField = "name")
    @ApiModelProperty(value = "付款方式，字典名称")
    private String payTypeDictName;


    @ApiModelProperty(value = "支付渠道，字典id")
    private String payChannelDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "payChannelDictId",mapValueField = "name")
    @ApiModelProperty(value = "支付渠道，字典名称")
    private String payChannelDictName;


    @ApiModelProperty(value = "订单渠道")
    private String channelCode;


    @ApiModelProperty(value = "下单归属的用户id")
    private String userId;

    // 请自定义实现该翻译
    @TransBy(type = TRANS_ORDER_USER_NICKNAME_BY_USER_ID,byFieldName = "userId")
    @ApiModelProperty(value = "下单归属的用户昵称")
    private String userNickname;

    @ApiModelProperty(value = "下单客户端ip")
    private String clientIp;

}
