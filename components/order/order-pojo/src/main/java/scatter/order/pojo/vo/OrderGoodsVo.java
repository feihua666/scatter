package scatter.order.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 订单商品响应对象
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Setter
@Getter
@ApiModel(value="订单商品响应对象")
public class OrderGoodsVo extends BaseIdVo {

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "商品id,外键")
    private String goodsId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品图片")
    private String imageUrl;

    @ApiModelProperty(value = "商品描述")
    private String goodsDescription;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品单价，单位分")
    private Integer amount;

    @ApiModelProperty(value = "归属用户id，和订单表的用户id相同")
    private String userId;

    @ApiModelProperty(value = "商品订单类型")
    private String categoryDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "categoryDictId",mapValueField = "name")
    @ApiModelProperty(value = "商品订单类型名称")
    private String categoryDictName;

    @ApiModelProperty(value = "订单状态，字典id")
    private String statusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "statusDictId",mapValueField = "name")
    @ApiModelProperty(value = "订单状态，字典名称")
    private String statusDictName;

    @ApiModelProperty(value = "额外信息")
    private String additional;
}
