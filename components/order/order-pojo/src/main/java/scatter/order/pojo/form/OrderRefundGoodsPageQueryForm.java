package scatter.order.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 退款商品分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Setter
@Getter
@ApiModel(value="退款商品分页表单对象")
public class OrderRefundGoodsPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "退款单id,外键")
    private String orderRefundId;

    @ApiModelProperty(value = "退款单号")
    private String orderRefundNo;

    @ApiModelProperty(value = "订单商品id")
    private String goodsId;

}
