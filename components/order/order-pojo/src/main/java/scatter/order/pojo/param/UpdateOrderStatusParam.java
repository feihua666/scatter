package scatter.order.pojo.param;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;
import scatter.order.pojo.constant.OrderConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 更新订单状态参数
 * </p>
 *
 * @author yangwei
 * @since 2021-08-14 01:10
 */
@Setter
@Getter
public class UpdateOrderStatusParam extends BaseParam {
	/**
	 * 订单号
	 */
	private String orderNo;
	/**
	 * 订单状态 {@link OrderConstants.OrderStatusItem}
	 */
	private String status;

	/**
	 * 退款单号，仅在退款时使用
	 */
	private String orderRefundNo;
	/**
	 * 订单渠道
	 */
	private String channel;
	/**
	 * 订单分类，字典值
	 */
	private String categoryDictValue;

	/**
	 * 额外参数
	 */
	private Map<String,Object> ext;

	/**
	 * 添加额外参数
	 * @param key
	 * @param value
	 */
	public void addExt(String key,Object value){
		if (ext == null) {
			ext = new HashMap<>();
		}
		ext.put(key,value);
	}
}
