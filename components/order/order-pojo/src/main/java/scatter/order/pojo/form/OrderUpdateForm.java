package scatter.order.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Setter
@Getter
@ApiModel(value="订单更新表单对象")
public class OrderUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="订单标题不能为空")
    @ApiModelProperty(value = "订单标题，比description更精简",required = true)
    private String title;

    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号",required = true)
    private String orderNo;

    @NotEmpty(message="订单描述不能为空")
    @ApiModelProperty(value = "订单描述，文本",required = true)
    private String description;

    @ApiModelProperty(value = "订单失效时间，超时时间")
    private LocalDateTime expireAt;

    @NotNull(message="订单总金额不能为空")
    @ApiModelProperty(value = "订单总金额，单位为分",required = true)
    private Integer totalAmount;

    @NotEmpty(message="货币类型不能为空")
    @ApiModelProperty(value = "货币类型,字典id",required = true)
    private String currencyDictId;

    @NotEmpty(message="订单状态不能为空")
    @ApiModelProperty(value = "订单状态，字典id",required = true)
    private String statusDictId;

    @NotEmpty(message="订单分类不能为空")
    @ApiModelProperty(value = "订单分类，字典id",required = true)
    private String categoryDictId;

    @NotEmpty(message="下单归属的用户id不能为空")
    @ApiModelProperty(value = "下单归属的用户id",required = true)
    private String userId;

    @NotEmpty(message="下单客户端ip不能为空")
    @ApiModelProperty(value = "下单客户端ip",required = true)
    private String clientIp;

}
