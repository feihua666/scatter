package scatter.order.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Setter
@Getter
@ApiModel(value="订单添加表单对象")
public class OrderAddForm extends BaseAddForm {

    @NotEmpty(message="订单标题不能为空")
    @ApiModelProperty(value = "订单标题，比description更精简",required = true)
    private String title;

    @ApiModelProperty(value = "订单描述，文本")
    private String description;

    @ApiModelProperty(value = "订单失效时间，超时时间")
    private LocalDateTime expireAt;

    @NotNull(message="订单总金额不能为空")
    @ApiModelProperty(value = "订单总金额，单位为分",required = true)
    private Integer totalAmount;

    @ApiModelProperty(value = "货币类型,字典值")
    private String currencyDictValue;

    @NotEmpty(message="订单分类不能为空")
    @ApiModelProperty(value = "订单分类，字典值",required = true)
    private String categoryDictValue;

    @ApiModelProperty(value = "下单归属的用户id")
    private String userId;

    @ApiModelProperty(value = "下单客户端ip")
    private String clientIp;
}
