package scatter.order.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 订单商品分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Setter
@Getter
@ApiModel(value="订单商品分页表单对象")
public class OrderGoodsPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "商品id,外键")
    private String goodsId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品描述")
    private String goodsDescription;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品单价，单位分")
    private Integer amount;

    @ApiModelProperty(value = "归属用户id，和订单表的用户id相同")
    private String userId;

    @ApiModelProperty(value = "商品订单类型")
    private String categoryDictId;

}
