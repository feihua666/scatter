package scatter.order.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 退款商品更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Setter
@Getter
@ApiModel(value="退款商品更新表单对象")
public class OrderRefundGoodsUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="订单id不能为空")
    @ApiModelProperty(value = "订单id,外键",required = true)
    private String orderId;

    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号",required = true)
    private String orderNo;

    @NotEmpty(message="退款单id不能为空")
    @ApiModelProperty(value = "退款单id,外键",required = true)
    private String orderRefundId;

    @NotEmpty(message="退款单号不能为空")
    @ApiModelProperty(value = "退款单号",required = true)
    private String orderRefundNo;

    @NotEmpty(message="订单商品id不能为空")
    @ApiModelProperty(value = "订单商品id",required = true)
    private String goodsId;

}
