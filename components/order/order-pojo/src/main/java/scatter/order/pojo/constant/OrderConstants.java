package scatter.order.pojo.constant;

import scatter.common.dict.IBaseDictEnum;
import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;

/**
 * <p>
 * 订单相关字典
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 16:28
 */
public class OrderConstants implements IBaseDictEnum {

	/**
	 * 订单状态字典组
	 */
	public enum OrderStatusGroupCode implements IDictGroup {
		order_status;

		@Override
		public String groupCode() {
			return this.name();
		}
	}

	/**
	 * 订单状态字典值
	 */
	public enum OrderStatusItem implements IDictItem {
		wait_pay, //待支付
		closed, // 关单，用户自己关单
		closed_expire, // 关单,支付超时关单
		paid, // 已支付
		refunding, // 已申请退款，退款中
		refund, // 已退款
		refunding_part_goods, // 部分商品退款中
		refund_part_goods, // 部分商品已退款
		;

		@Override
		public String itemValue() {
			return this.name();
		}

		@Override
		public String groupCode() {
			return OrderStatusGroupCode.order_status.groupCode();
		}
	}


	/**
	 * 订单类别字典组,表示商品订单，活动订单等
	 */
	public enum OrderCategoryGroupCode implements IDictGroup {
		order_category;

		@Override
		public String groupCode() {
			return this.name();
		}
	}



	/**
	 * 订单类别字典项
	 */
	public enum OrderCategoryDictItem implements IDictItem {
		order_category_defalut; // 默认订单类型，没有意义，只是占位

		@Override
		public String itemValue() {
			return this.name();
		}

		@Override
		public String groupCode() {
			return OrderCategoryGroupCode.order_category.groupCode();
		}
	}

	/**
	 * 支付方式字典组
	 */
	public enum OrderPayTypeGroupCode implements IDictGroup {
		pay_type;

		@Override
		public String groupCode() {
			return this.name();
		}
	}


	/**
	 * 支付方式字典项
	 */
	public enum OrderPayTypeDictItem implements IDictItem {
		on_line_pay, //线上付
		off_line_pay, // 现付
		;

		@Override
		public String itemValue() {
			return this.name();
		}

		@Override
		public String groupCode() {
			return OrderPayTypeGroupCode.pay_type.groupCode();
		}

	}

	/**
	 * 支付渠道字典组
	 */
	public enum OrderPayChannelGroupCode implements IDictGroup {
		pay_channel;

		@Override
		public String groupCode() {
			return this.name();
		}
	}


	/**
	 * 支付渠道字典项
	 */
	public enum OrderPayChannelDictItem implements IDictItem {
		wx_jsapi, // 微信jsapi 支付,公众号或小程序
		wx_native, // 微信扫码支付
		wx_app, // 微信app 支付
		wx_micropay, // 微信 付款码支付
		wx_mweb, // 微信 H5支付
		wx_facepay, // 微信 H5支付
		;

		@Override
		public String itemValue() {
			return this.name();
		}

		@Override
		public String groupCode() {
			return OrderPayChannelGroupCode.pay_channel.groupCode();
		}


		/**
		 * 交易类型，枚举值：
		 * JSAPI：公众号支付
		 * NATIVE：扫码支付
		 * APP：APP支付
		 * MICROPAY：付款码支付
		 * MWEB：H5支付
		 * FACEPAY：刷脸支付
		 * @param tradeType
		 * @return
		 */
		public static OrderPayChannelDictItem ofWxTradeType(String tradeType){
			return OrderPayChannelDictItem.valueOf("wx_" + tradeType.toLowerCase());
		}
	}

}
