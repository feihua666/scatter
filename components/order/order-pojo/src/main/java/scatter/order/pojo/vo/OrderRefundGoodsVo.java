package scatter.order.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.common.rest.trans.TransField;


/**
 * <p>
 * 退款商品响应对象
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Setter
@Getter
@ApiModel(value="退款商品响应对象")
public class OrderRefundGoodsVo extends BaseIdVo {

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "退款单id,外键")
    private String orderRefundId;

    @ApiModelProperty(value = "退款单号")
    private String orderRefundNo;

    @ApiModelProperty(value = "订单商品id")
    private String goodsId;

    @TransField
    @ApiModelProperty(value = "订单商品信息")
    private OrderGoodsVo orderGoods;

}
