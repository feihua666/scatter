package scatter.order.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单退款单添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Setter
@Getter
@ApiModel(value="订单退款单添加表单对象")
public class OrderRefundAddForm extends BaseAddForm {

    @NotEmpty(message="订单id不能为空")
    @ApiModelProperty(value = "订单id",required = true)
    private String orderId;

    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号",required = true)
    private String orderNo;

    @NotEmpty(message="退款单号不能为空")
    @ApiModelProperty(value = "退款单号",required = true)
    private String refundNo;

    @NotEmpty(message="退款描述不能为空")
    @ApiModelProperty(value = "退款描述，文本",required = true)
    private String description;

    @NotNull(message="退款金额不能为空")
    @ApiModelProperty(value = "退款金额，单位为分",required = true)
    private Integer amount;

    @NotNull(message="是否分多次退款不能为空")
    @ApiModelProperty(value = "是否分多次退款",required = true)
    private Boolean isMultiple;

    @NotEmpty(message="货币类型不能为空")
    @ApiModelProperty(value = "货币类型,字典id",required = true)
    private String currencyDictId;

    @NotEmpty(message="退款状态不能为空")
    @ApiModelProperty(value = "退款状态,字典id",required = true)
    private String statusDictId;

    @NotEmpty(message="发起退款客户端ip不能为空")
    @ApiModelProperty(value = "发起退款客户端ip",required = true)
    private String clientIp;

}
