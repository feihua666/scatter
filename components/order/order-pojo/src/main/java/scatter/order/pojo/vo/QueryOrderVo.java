package scatter.order.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 19:38
 */
@ApiModel("订单vo")
@Setter
@Getter
public class QueryOrderVo extends BaseVo {

	/**
	 * 订单信息
	 */
	@ApiModelProperty(value = "订单信息")
	private OrderVo order;

	@ApiModelProperty(value = "订单商品信息")
	private List<OrderGoodsVo> orderGoods;

	/**
	 * 需要额外返回的信息
	 */
	@ApiModelProperty(value = "额外需要返回的")
	private Map<String,Object> ext;
}
