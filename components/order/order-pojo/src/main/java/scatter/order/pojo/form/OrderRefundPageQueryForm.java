package scatter.order.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 订单退款单分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Setter
@Getter
@ApiModel(value="订单退款单分页表单对象")
public class OrderRefundPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "退款单号")
    private String refundNo;

    @ApiModelProperty(value = "退款描述，文本")
    private String description;

    @ApiModelProperty(value = "退款金额，单位为分")
    private Integer amount;

    @ApiModelProperty(value = "是否分多次退款")
    private Boolean isMultiple;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @ApiModelProperty(value = "退款状态,字典id")
    private String statusDictId;

    @ApiModelProperty(value = "发起退款客户端ip")
    private String clientIp;

}
