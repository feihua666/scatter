package scatter.order.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.form.OrderCreateGoodsForm;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单退款参数
 * </p>
 *
 * @author yangwei
 * @since 2021-08-06 14:06
 */
@Getter
@Setter
public class RefundOrderParam extends BaseParam {

	/**
	 * 订单渠道，主要是区分是哪了个渠道的订单，没有可以不填
	 * 只是预留，暂未处理
	 */
	private String channel;
	/**
	 * {@link OrderConstants.OrderCategoryDictItem}
	 */
	private String categoryDictValue;

	/**
	 * 订单号
	 */
	private String orderNo;

	/**
	 * 退款描述
	 */
	private String remark;
	/**
	 * 订单退款金额，单位为分 必填
	 */
	private Integer amount;
	/**
	 * 退款的商品id，如果不填表示订单全部退款
	 */
	private List<String> goodsIds;

	/**
	 * 退款客户端ip 必填
	 */
	private String clientIp;

	/**
	 * 额外信息,没有可以不用填
	 */
	private Map<String,Object> ext;
}
