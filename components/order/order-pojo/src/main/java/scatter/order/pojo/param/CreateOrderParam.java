package scatter.order.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;
import scatter.order.pojo.form.OrderCreateGoodsForm;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单创建参数
 * </p>
 *
 * @author yangwei
 * @since 2021-07-03 13:35
 */
@Getter
@Setter
public class CreateOrderParam extends BaseParam {



	/**
	 * 订单渠道，主要是区分是哪了个渠道的订单，对应订单表的channel_code
	 * 只是预留，暂未处理
	 */
	private String channelCode;

	/**
	 * 订单标题 必填
	 */
	private String title;


	/**
	 * 订单描述，文本
	 */
	private String description;

	/**
	 * 订单失效时间，超时时间 必填
	 */
	private LocalDateTime expireAt;

	/**
	 * 订单总金额，单位为分 必填
	 */
	private Integer totalAmount;


	/**
	 * 币种 必填
	 */
	private String currencyDictValue;


	/**
	 * 订单分类，字典值 必填
	 */
	private String categoryDictValue;

	/**
	 * 支付类型，字典值 必填
	 */
	private String payTypeDictValue;

	/**
	 * 支付渠道
	 */
	private String payChannelDictValue;


	/**
	 * 下单归属的用户id 必填
	 */
	private String userId;

	/**
	 * 下单客户端ip 必填
	 */
	private String clientIp;

	/**
	 * 商品信息 必填
	 */
	private List<OrderCreateGoodsForm> goodsForms;

	/**
	 * 自定义配置的微信公众号 mpAppCode
	 * 如果不是微信公众号jsapi支付，可以不用填写
	 */
	private String mpAppCode;


	/**
	 * 额外信息,没有可以不用填
	 */
	private Map<String,Object> ext;
}
