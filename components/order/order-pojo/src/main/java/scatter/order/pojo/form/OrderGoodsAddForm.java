package scatter.order.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单商品添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Setter
@Getter
@ApiModel(value="订单商品添加表单对象")
public class OrderGoodsAddForm extends BaseAddForm {

    @NotEmpty(message="订单id不能为空")
    @ApiModelProperty(value = "订单id,外键",required = true)
    private String orderId;

    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号",required = true)
    private String orderNo;

    @NotEmpty(message="商品id不能为空")
    @ApiModelProperty(value = "商品id,外键",required = true)
    private String goodsId;

    @NotEmpty(message="商品名称不能为空")
    @ApiModelProperty(value = "商品名称",required = true)
    private String goodsName;

    @ApiModelProperty(value = "商品描述")
    private String goodsDescription;

    @NotNull(message="商品数量不能为空")
    @ApiModelProperty(value = "商品数量",required = true)
    private Integer count;

    @NotNull(message="商品单价不能为空")
    @ApiModelProperty(value = "商品单价，单位分",required = true)
    private Integer amount;

    @NotEmpty(message="归属用户id不能为空")
    @ApiModelProperty(value = "归属用户id，和订单表的用户id相同",required = true)
    private String userId;

    @NotEmpty(message="商品订单类型不能为空")
    @ApiModelProperty(value = "商品订单类型",required = true)
    private String categoryDictId;

    @ApiModelProperty(value = "额外信息")
    private String additional;

    @NotNull(message="商品图片不能为空")
    @ApiModelProperty(value = "商品图片")
    private String imageUrl;

}
