package scatter.order.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.order.pojo.constant.OrderConstants;


/**
 * <p>
 * 订单分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Setter
@Getter
@ApiModel(value="订单分页表单对象")
public class OrderPageQueryForm extends BasePageQueryForm {

    /**
     * 订单渠道，主要是区分是哪了个渠道的订单
     */
    @ApiModelProperty(value = "渠道")
    private String channel;
    /**
     * {@link OrderConstants.OrderCategoryDictItem}
     */
    @NotEmpty(message="订单分类不能为空")
    @ApiModelProperty(value = "订单分类，字典值",required = true)
    private String categoryDictValue;


    @ApiModelProperty(value = "订单标题，比description更精简")
    private String title;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "订单描述，文本")
    private String description;

    @ApiModelProperty(value = "订单失效时间，超时时间")
    private LocalDateTime expireAt;

    @ApiModelProperty(value = "订单总金额，单位为分")
    private Integer totalAmount;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @ApiModelProperty(value = "订单状态，字典id")
    private String statusDictId;

    @ApiModelProperty(value = "订单分类，字典id")
    private String categoryDictId;

    @ApiModelProperty(value = "下单归属的用户id")
    private String userId;

    @ApiModelProperty(value = "下单客户端ip")
    private String clientIp;

}
