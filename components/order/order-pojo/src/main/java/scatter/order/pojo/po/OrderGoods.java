package scatter.order.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单商品表
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_order_goods")
@ApiModel(value="OrderGoods对象", description="订单商品表")
public class OrderGoods extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ORDERGOODS_BY_ID = "trans_ordergoods_by_id_scatter.order.pojo.po";

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "商品id,外键")
    private String goodsId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品图片")
    private String imageUrl;

    @ApiModelProperty(value = "商品描述")
    private String goodsDescription;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品单价，单位分")
    private Integer amount;

    @ApiModelProperty(value = "归属用户id，和订单表的用户id相同")
    private String userId;

    @ApiModelProperty(value = "商品订单类型")
    private String categoryDictId;

    @ApiModelProperty(value = "订单状态，字典id")
    private String statusDictId;

    @ApiModelProperty(value = "额外信息")
    private String additional;
}
