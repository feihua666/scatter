package scatter.order.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 商品信息
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 15:39
 */
@Setter
@Getter
public class OrderCreateGoodsForm extends BaseForm {


	@NotEmpty(message="商品id不能为空")
	@ApiModelProperty(value = "商品id",required = true)
	private String id;

	@NotEmpty(message="商品名称不能为空")
	@ApiModelProperty(value = "商品名称",required = true)
	private String name;

	@ApiModelProperty(value = "商品描述")
	private String description;

	@Min(message="商品数量不能小于1",value = 1)
	@ApiModelProperty(value = "商品数量",required = true)
	private Integer count;

	@NotNull(message="商品单价不能为空")
	@ApiModelProperty(value = "商品单价,单位为分",required = true)
	private Integer amount;


	@ApiModelProperty(value = "额外信息")
	private String additional;

	@ApiModelProperty(value = "商品图片")
	private String imageUrl;

}
