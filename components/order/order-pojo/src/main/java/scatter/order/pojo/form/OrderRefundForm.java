package scatter.order.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;
import scatter.order.pojo.constant.OrderConstants;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单退款表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-07 22:11
 */
@Setter
@Getter
@ApiModel(value="订单退款表单对象")
public class OrderRefundForm extends BaseAddForm {


    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号",required = true)
    private String orderNo;

    @NotEmpty(message="备注不能为空")
    @ApiModelProperty(value = "备注，退款原因等")
    private String remark;

    /**
     * 退款金额,不填写取商品 goodsIds 的总金额，否则默认全部退款
     */
    @ApiModelProperty(value = "退款金额，单位为分")
    private Integer amount;

    /**
     * 退款的商品id
     */
    @ApiModelProperty(value = "退款商品")
    private List<String> goodsIds;


    /**
     * 额外信息
     */
    private Map<String,Object> ext;


}
