package scatter.order.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 订单退款单响应对象
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Setter
@Getter
@ApiModel(value="订单退款单响应对象")
public class OrderRefundVo extends BaseIdVo {

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "退款单号")
    private String refundNo;

    @ApiModelProperty(value = "退款描述，文本")
    private String description;

    @ApiModelProperty(value = "退款金额，单位为分")
    private Integer amount;

    @ApiModelProperty(value = "是否分多次退款")
    private Boolean isMultiple;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "currencyDictId",mapValueField = "name")
    @ApiModelProperty(value = "货币类型,字典名称")
    private String currencyDictName;

    @ApiModelProperty(value = "退款状态,字典id")
    private String statusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "statusDictId",mapValueField = "name")
    @ApiModelProperty(value = "退款状态,字典名称")
    private String statusDictName;

    @ApiModelProperty(value = "发起退款客户端ip")
    private String clientIp;

}
