package scatter.order.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_order")
@ApiModel(value="Order对象", description="订单表")
public class Order extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ORDER_BY_ID = "trans_order_by_id_scatter.order.pojo.po";

    @ApiModelProperty(value = "订单标题，比description更精简")
    private String title;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "订单描述，文本")
    private String description;

    @ApiModelProperty(value = "订单失效时间，超时时间")
    private LocalDateTime expireAt;

    @ApiModelProperty(value = "订单总金额，单位为分")
    private Integer totalAmount;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @ApiModelProperty(value = "订单状态，字典id")
    private String statusDictId;

    @ApiModelProperty(value = "订单分类，字典id")
    private String categoryDictId;

    @ApiModelProperty(value = "付款方式，预付，现付，信用付等")
    private String payTypeDictId;

    @ApiModelProperty(value = "支付渠道，字典id")
    private String payChannelDictId;

    @ApiModelProperty(value = "订单渠道")
    private String channelCode;

    @ApiModelProperty(value = "下单归属的用户id")
    private String userId;

    @ApiModelProperty(value = "下单客户端ip")
    private String clientIp;


}
