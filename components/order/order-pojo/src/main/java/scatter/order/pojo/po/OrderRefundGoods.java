package scatter.order.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 退款商品表
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_order_refund_goods")
@ApiModel(value="OrderRefundGoods对象", description="退款商品表")
public class OrderRefundGoods extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ORDERREFUNDGOODS_BY_ID = "trans_orderrefundgoods_by_id_scatter.order.pojo.po";

    @ApiModelProperty(value = "订单id,外键")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "退款单id,外键")
    private String orderRefundId;

    @ApiModelProperty(value = "退款单号")
    private String orderRefundNo;

    @ApiModelProperty(value = "订单商品id")
    private String goodsId;


}
