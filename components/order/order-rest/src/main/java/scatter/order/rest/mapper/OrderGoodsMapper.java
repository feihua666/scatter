package scatter.order.rest.mapper;

import scatter.order.pojo.po.OrderGoods;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 订单商品表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
public interface OrderGoodsMapper extends IBaseMapper<OrderGoods> {

}
