package scatter.order.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.pojo.vo.OrderGoodsVo;
import scatter.order.rest.OrderConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.order.pojo.po.OrderRefundGoods;
import scatter.order.pojo.vo.OrderRefundGoodsVo;
import scatter.order.pojo.form.OrderRefundGoodsAddForm;
import scatter.order.pojo.form.OrderRefundGoodsUpdateForm;
import scatter.order.pojo.form.OrderRefundGoodsPageQueryForm;
import scatter.order.rest.mapstruct.OrderGoodsMapStruct;
import scatter.order.rest.mapstruct.OrderRefundGoodsMapStruct;
import scatter.order.rest.service.IOrderGoodsService;
import scatter.order.rest.service.IOrderRefundGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 退款商品表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Api(tags = "退款商品相关接口")
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/order-refund-goods")
public class OrderRefundGoodsController extends BaseAddUpdateQueryFormController<OrderRefundGoods, OrderRefundGoodsVo, OrderRefundGoodsAddForm, OrderRefundGoodsUpdateForm, OrderRefundGoodsPageQueryForm> {
    @Autowired
    private IOrderRefundGoodsService iOrderRefundGoodsService;

    @Autowired
    private IOrderGoodsService iOrderGoodsService;

    @ApiOperation("添加退款商品")
    @PreAuthorize("hasAuthority('OrderRefundGoods:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public OrderRefundGoodsVo add(@RequestBody @Valid OrderRefundGoodsAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询退款商品")
    @PreAuthorize("hasAuthority('OrderRefundGoods:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public OrderRefundGoodsVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除退款商品")
    @PreAuthorize("hasAuthority('OrderRefundGoods:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新退款商品")
    @PreAuthorize("hasAuthority('OrderRefundGoods:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public OrderRefundGoodsVo update(@RequestBody @Valid OrderRefundGoodsUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询退款商品")
    @PreAuthorize("hasAuthority('OrderRefundGoods:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<OrderRefundGoodsVo> getList(OrderRefundGoodsPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询退款商品")
    @PreAuthorize("hasAuthority('OrderRefundGoods:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<OrderRefundGoodsVo> getPage(OrderRefundGoodsPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @Override
    public OrderRefundGoodsVo transVo(OrderRefundGoodsVo vo) {
        OrderRefundGoodsVo orderRefundGoodsVo = super.transVo(vo);
        OrderGoods byGoodsIdAndOrderId = iOrderGoodsService.getByGoodsIdAndOrderId(orderRefundGoodsVo.getGoodsId(), orderRefundGoodsVo.getOrderId());
        OrderGoodsVo orderGoodsVo = OrderGoodsMapStruct.INSTANCE.poToVo(byGoodsIdAndOrderId);
        orderRefundGoodsVo.setOrderGoods(orderGoodsVo);
        return orderRefundGoodsVo;
    }
}
