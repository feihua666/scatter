package scatter.order.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.order.pojo.form.*;
import scatter.order.pojo.param.CreateOrderParam;
import scatter.order.pojo.param.RefundOrderParam;
import scatter.order.pojo.po.OrderRefund;
import scatter.order.pojo.vo.OrderRefundVo;

/**
 * <p>
 * 订单退款单 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderRefundMapStruct extends IBaseVoMapStruct<OrderRefund, OrderRefundVo>,
                                  IBaseAddFormMapStruct<OrderRefund,OrderRefundAddForm>,
                                  IBaseUpdateFormMapStruct<OrderRefund,OrderRefundUpdateForm>,
                                  IBaseQueryFormMapStruct<OrderRefund,OrderRefundPageQueryForm>{
    OrderRefundMapStruct INSTANCE = Mappers.getMapper( OrderRefundMapStruct.class );



    RefundOrderParam map (OrderRefundForm form);

}
