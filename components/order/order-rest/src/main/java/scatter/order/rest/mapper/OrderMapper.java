package scatter.order.rest.mapper;

import scatter.order.pojo.po.Order;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
public interface OrderMapper extends IBaseMapper<Order> {

}
