package scatter.order.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.order.rest.OrderConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.pojo.vo.OrderGoodsVo;
import scatter.order.pojo.form.OrderGoodsAddForm;
import scatter.order.pojo.form.OrderGoodsUpdateForm;
import scatter.order.pojo.form.OrderGoodsPageQueryForm;
import scatter.order.rest.service.IOrderGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 订单商品表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Api(tags = "订单商品相关接口")
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/order-goods")
public class OrderGoodsController extends BaseAddUpdateQueryFormController<OrderGoods, OrderGoodsVo, OrderGoodsAddForm, OrderGoodsUpdateForm, OrderGoodsPageQueryForm> {
    @Autowired
    private IOrderGoodsService iOrderGoodsService;

     @Override
	 @ApiOperation("添加订单商品")
     @PreAuthorize("hasAuthority('OrderGoods:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public OrderGoodsVo add(@RequestBody @Valid OrderGoodsAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询订单商品")
     @PreAuthorize("hasAuthority('OrderGoods:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public OrderGoodsVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除订单商品")
     @PreAuthorize("hasAuthority('OrderGoods:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新订单商品")
     @PreAuthorize("hasAuthority('OrderGoods:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public OrderGoodsVo update(@RequestBody @Valid OrderGoodsUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询订单商品")
    @PreAuthorize("hasAuthority('OrderGoods:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderGoodsVo> getList(OrderGoodsPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询订单商品")
    @PreAuthorize("hasAuthority('OrderGoods:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<OrderGoodsVo> getPage(OrderGoodsPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
