package scatter.order.rest.service.impl;

import scatter.order.pojo.po.OrderRefundGoods;
import scatter.order.rest.mapper.OrderRefundGoodsMapper;
import scatter.order.rest.service.IOrderRefundGoodsService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.order.pojo.form.OrderRefundGoodsAddForm;
import scatter.order.pojo.form.OrderRefundGoodsUpdateForm;
import scatter.order.pojo.form.OrderRefundGoodsPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 退款商品表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Service
@Transactional
public class OrderRefundGoodsServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<OrderRefundGoodsMapper, OrderRefundGoods, OrderRefundGoodsAddForm, OrderRefundGoodsUpdateForm, OrderRefundGoodsPageQueryForm> implements IOrderRefundGoodsService {
    @Override
    public void preAdd(OrderRefundGoodsAddForm addForm,OrderRefundGoods po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(OrderRefundGoodsUpdateForm updateForm,OrderRefundGoods po) {
        super.preUpdate(updateForm,po);

    }
}
