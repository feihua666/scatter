package scatter.order.rest.mapper;

import scatter.order.pojo.po.OrderRefundGoods;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 退款商品表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
public interface OrderRefundGoodsMapper extends IBaseMapper<OrderRefundGoods> {

}
