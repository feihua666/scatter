package scatter.order.rest.service;

/**
 * <p>
 * 订单号生成器
 * </p>
 *
 * @author yangwei
 * @since 2021-06-26 19:58
 */
public interface IOrderNoGeneratorService {
	/**
	 * 生成订单号方法
	 * @param categoryDictValue 订单分类id
	 * @return
	 */
	String generateOrderNo(String categoryDictValue);
}
