package scatter.order.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.order.pojo.po.OrderRefundGoods;
import scatter.common.rest.service.IBaseService;
import scatter.order.pojo.form.OrderRefundGoodsAddForm;
import scatter.order.pojo.form.OrderRefundGoodsUpdateForm;
import scatter.order.pojo.form.OrderRefundGoodsPageQueryForm;
import java.util.List;
/**
 * <p>
 * 退款商品表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
public interface IOrderRefundGoodsService extends IBaseService<OrderRefundGoods> {

    /**
     * 根据订单id查询
     * @param orderId
     * @return
     */
    default List<OrderRefundGoods> getByOrderId(String orderId) {
        Assert.hasText(orderId,"orderId不能为空");
        return list(Wrappers.<OrderRefundGoods>lambdaQuery().eq(OrderRefundGoods::getOrderId, orderId));
    }
    /**
     * 根据退款单id查询
     * @param orderRefundId
     * @return
     */
    default List<OrderRefundGoods> getByOrderRefundId(String orderRefundId) {
        Assert.hasText(orderRefundId,"orderRefundId不能为空");
        return list(Wrappers.<OrderRefundGoods>lambdaQuery().eq(OrderRefundGoods::getOrderRefundId, orderRefundId));
    }

}
