package scatter.order.rest.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import scatter.order.rest.service.IOrderNoGeneratorService;
import scatter.order.rest.service.IOrderRefundNoGeneratorService;

/**
 * <p>
 * 默认订单退款单号生成器
 * </p>
 *
 * @author yangwei
 * @since 2021-08-07 16:01
 */
@Slf4j
public class DefaultOrderRefundNoGeneratorServiceImpl implements IOrderRefundNoGeneratorService {
	@Override
	public String generateOrderRefundNo(String categoryDictValue) {
		String orderRefundNO = IdWorker.getIdStr();
		log.info("默认订单退款单号生成器生成订单退款单号，categoryDictValue={},orderNo={}",categoryDictValue,orderRefundNO);
		return orderRefundNO;
	}
}
