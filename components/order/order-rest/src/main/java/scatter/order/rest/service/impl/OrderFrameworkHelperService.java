package scatter.order.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.order.pojo.form.OrderCreateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import scatter.order.pojo.param.CreateOrderParam;
import scatter.order.pojo.vo.QueryOrderVo;
import scatter.order.rest.service.ICreateOrderFrameworkService;
import scatter.order.rest.service.IOrderStatusFrameworkService;
import scatter.order.rest.service.IQueryOrderFrameworkService;

import java.util.List;

/**
 * <p>
 * 订单框架帮忙服务类
 * </p>
 *
 * @author yangwei
 * @since 2021-08-08 12:34
 */
@Component
public class OrderFrameworkHelperService {

	@Autowired
	private List<ICreateOrderFrameworkService<CreateOrderParam>> createOrderFrameworkServices;

	@Autowired
	private List<IQueryOrderFrameworkService<OrderPageQueryForm, QueryOrderVo>> iQueryOrderFrameworkServices;

	@Autowired
	private List<IOrderStatusFrameworkService> iOrderStatusFrameworkServices;

	/**
	 * 获取创建订单服务
	 * @param channel
	 * @param categoryDictValue
	 * @return
	 */
	public ICreateOrderFrameworkService getICreateOrderFrameworkService(String channel,String categoryDictValue){
		for (ICreateOrderFrameworkService createOrderFrameworkService : createOrderFrameworkServices) {
			if (createOrderFrameworkService.support(channel,categoryDictValue)) {
				return createOrderFrameworkService;
			}
		}
		throw new RuntimeException("未能处理订单，未找到订单服务");
	}

	/**
	 * 获取查询订单服务
	 * @param channel
	 * @param categoryDictValue
	 * @return
	 */
	public IQueryOrderFrameworkService<OrderPageQueryForm,QueryOrderVo> getIQueryOrderFrameworkService(String channel,String categoryDictValue){

		for (IQueryOrderFrameworkService queryOrderFrameworkService : iQueryOrderFrameworkServices) {
			if (queryOrderFrameworkService.support(channel,categoryDictValue)) {
				return queryOrderFrameworkService;
			}
		}
		throw new RuntimeException("未能处理订单查询，未找到订单查询服务");
	}


	/**
	 * 订单状态服务获取
	 * @param channel
	 * @param categoryDictValue
	 * @return
	 */
	public IOrderStatusFrameworkService getIOrderStatusFrameworkService(String channel, String categoryDictValue){

		for (IOrderStatusFrameworkService orderStatusFrameworkService : iOrderStatusFrameworkServices) {
			if (orderStatusFrameworkService.support(channel,categoryDictValue)) {
				return orderStatusFrameworkService;
			}
		}
		throw new RuntimeException("未能处理订单状态，未找到订单状态服务");
	}
}
