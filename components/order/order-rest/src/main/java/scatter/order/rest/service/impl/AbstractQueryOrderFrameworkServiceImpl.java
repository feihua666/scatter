package scatter.order.rest.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import scatter.common.pojo.form.BasePageQueryForm;
import scatter.order.rest.service.IQueryOrderFrameworkService;

/**
 * <p>
 * 订单查询框架抽象实现类
 * </p>
 *
 * @author yangwei
 * @since 2021-07-05 00:41
 */
public abstract class AbstractQueryOrderFrameworkServiceImpl<T extends BasePageQueryForm,R> implements IQueryOrderFrameworkService<T,R> {


	/**
	 * 查询前调用，旨在查询前处理查询参数
	 * @param param
	 */
	protected void preQueryOrder(T param){

	}

	@Override
	public R getOrderByOrderNo(String orderNo) {
		R r = doGetOrderByOrderNo(orderNo);
		postQueryOrder(r);
		return r;
	}

	protected abstract R doGetOrderByOrderNo(String orderNo);
	@Override
	public Page<R> queryOrderPage(T param) {
		preQueryOrder(param);
		Page<R> r = doQueryOrderPage(param);
		r.getRecords().parallelStream().peek(item -> postQueryOrder(item))
		;
		return r;
	}

	/**
	 * 实际要执行查询逻辑
	 * @param param
	 * @return
	 */
	protected abstract Page<R> doQueryOrderPage(T param);

	/**
	 * 查询完成之后执行
	 * @param r
	 */
	protected void postQueryOrder(R r){

	}
}
