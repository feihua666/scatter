package scatter.order.rest.service.impl;

import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.exception.BusinessException;
import scatter.order.rest.service.IRefundOrderFrameworkService;

/**
 * <p>
 * 抽象退单，退款服务
 * </p>
 *
 * @author yangwei
 * @since 2021-07-05 04:45
 */
public abstract class AbstractRefundOrderFrameworkServiceImpl<T> implements IRefundOrderFrameworkService<T> {

	/**
	 * 主要用来退之前的逻辑，校验等
	 * @param param
	 */
	protected boolean preRefund(T param){
		return true;
	}


	@Transactional
	@Override
	public boolean refund(T param) {
		boolean preRefund = preRefund(param);
		if (!preRefund) {
			throw new BusinessException("订单退款失败，订单退款前置处理返回 false");
		}
		boolean b = doRefund(param);
		if (b) {
			postRefund(param);
		}
		return b;
	}

	/**
	 * 实际退单，退款的逻辑操作
	 * @param param
	 * @return
	 */
	protected abstract boolean doRefund(T param);

	/**
	 * 退款以后的逻辑
	 * @param param
	 */
	protected void postRefund(T param){

	}
}
