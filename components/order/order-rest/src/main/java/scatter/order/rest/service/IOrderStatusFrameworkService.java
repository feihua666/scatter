package scatter.order.rest.service;

import scatter.order.pojo.param.UpdateOrderStatusParam;

/**
 * <p>
 * 订单状态框架服务
 * 适用场景：
 * 1. 用户已下单，但未支付自己取消订单
 * 2. 用户已下单，但未支付导致的超时关单
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 12:29
 */
public interface IOrderStatusFrameworkService extends IOrderFrameworkService {

	/**
	 * 修改订单状态
	 * @param param
	 * @return
	 */
	boolean updateOrderStatus(UpdateOrderStatusParam param);

}
