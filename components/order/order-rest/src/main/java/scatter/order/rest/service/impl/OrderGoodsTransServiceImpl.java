package scatter.order.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.rest.service.IOrderGoodsService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单商品翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Component
public class OrderGoodsTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IOrderGoodsService orderGoodsService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,OrderGoods.TRANS_ORDERGOODS_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,OrderGoods.TRANS_ORDERGOODS_BY_ID)) {
            OrderGoods byId = orderGoodsService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,OrderGoods.TRANS_ORDERGOODS_BY_ID)) {
            return orderGoodsService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
