package scatter.order.rest.service;

import scatter.common.rest.tools.InterfaceTool;
import scatter.order.pojo.constant.OrderConstants;

/**
 * <p>
 * 订单框架
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 12:28
 */
public interface IOrderFrameworkService extends InterfaceTool {

	public static String DEFAULT_SUPPORT_CHANNEL = "defalut";
	public static String DEFAULT_SUPPORT_CATEGORYDICTVALUE = "defalut";

	/**
	 * 用于查找支持的订单服务
	 * @param channel 这是一个渠道标识字符串，用来解释是哪个渠道过来的订单
	 * @param categoryDictValue 订单分类字典值{@link OrderConstants.OrderCategoryDictItem}
	 * @return
	 */
	default boolean support(String channel,String categoryDictValue){
		return DEFAULT_SUPPORT_CHANNEL.equals(channel) && DEFAULT_SUPPORT_CATEGORYDICTVALUE.equals(categoryDictValue);
	}

}
