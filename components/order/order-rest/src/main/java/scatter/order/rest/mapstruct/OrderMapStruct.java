package scatter.order.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.order.pojo.form.OrderCreateForm;
import scatter.order.pojo.param.CreateOrderParam;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.vo.OrderVo;
import scatter.order.pojo.form.OrderAddForm;
import scatter.order.pojo.form.OrderUpdateForm;
import scatter.order.pojo.form.OrderPageQueryForm;

/**
 * <p>
 * 订单 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderMapStruct extends IBaseVoMapStruct<Order, OrderVo>,
                                  IBaseAddFormMapStruct<Order,OrderAddForm>,
                                  IBaseUpdateFormMapStruct<Order,OrderUpdateForm>,
                                  IBaseQueryFormMapStruct<Order,OrderPageQueryForm>{
    OrderMapStruct INSTANCE = Mappers.getMapper( OrderMapStruct.class );


    CreateOrderParam map (OrderCreateForm form);

}
