package scatter.order.rest.mapper;

import scatter.order.pojo.po.OrderRefund;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 订单退款单表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
public interface OrderRefundMapper extends IBaseMapper<OrderRefund> {

}
