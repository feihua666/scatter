package scatter.order.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.pojo.vo.OrderGoodsVo;
import scatter.order.pojo.form.OrderGoodsAddForm;
import scatter.order.pojo.form.OrderGoodsUpdateForm;
import scatter.order.pojo.form.OrderGoodsPageQueryForm;

/**
 * <p>
 * 订单商品 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderGoodsMapStruct extends IBaseVoMapStruct<OrderGoods, OrderGoodsVo>,
                                  IBaseAddFormMapStruct<OrderGoods,OrderGoodsAddForm>,
                                  IBaseUpdateFormMapStruct<OrderGoods,OrderGoodsUpdateForm>,
                                  IBaseQueryFormMapStruct<OrderGoods,OrderGoodsPageQueryForm>{
    OrderGoodsMapStruct INSTANCE = Mappers.getMapper( OrderGoodsMapStruct.class );

}
