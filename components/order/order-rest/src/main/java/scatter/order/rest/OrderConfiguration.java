package scatter.order.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.order.rest.service.*;
import scatter.order.rest.service.impl.*;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.order.rest.**.mapper")
public class OrderConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "order";

    /**
     * 订单号生成服务
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public IOrderNoGeneratorService defaultOrderNoGenerator(){
        return new DefaultOrderNoGeneratorServiceImpl();
    }
    /**
     * 订单退款单号生成服务
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public IOrderRefundNoGeneratorService defaultOrderRefundNoGenerator(){
        return new DefaultOrderRefundNoGeneratorServiceImpl();
    }
    /**
     * 创建订单服务
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(ICreateOrderFrameworkService.class)
    public DefaultCreateOrderFrameworkServiceImpl defaultCreateOrderFrameworkServiceImpl(){
        return new DefaultCreateOrderFrameworkServiceImpl();
    }

    /**
     * 订单查询服务
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(IQueryOrderFrameworkService.class)
    public DefaultQueryOrderFrameworkServiceImpl defaultQueryOrderFrameworkServiceImpl(){
        return new DefaultQueryOrderFrameworkServiceImpl();
    }

    /**
     * 订单状态变更服务
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(IOrderStatusFrameworkService.class)
    public DefaultOrderStatusFrameworkServiceImpl defaultOrderStatusFrameworkServiceImpl(){
        return new DefaultOrderStatusFrameworkServiceImpl();
    }

    /**
     * 退单退款服务
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(IRefundOrderFrameworkService.class)
    public DefaultRefundOrderFrameworkServiceImpl defaultRefundOrderFrameworkServiceImpl(){
        return new DefaultRefundOrderFrameworkServiceImpl();
    }
}
