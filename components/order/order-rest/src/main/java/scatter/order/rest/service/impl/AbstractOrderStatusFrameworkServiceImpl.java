package scatter.order.rest.service.impl;

import org.springframework.transaction.annotation.Transactional;
import scatter.order.pojo.param.UpdateOrderStatusParam;
import scatter.order.rest.service.IOrderStatusFrameworkService;

/**
 * <p>
 * 订单状态抽象服务
 * </p>
 *
 * @author yangwei
 * @since 2021-07-05 03:58
 */
public abstract class AbstractOrderStatusFrameworkServiceImpl implements IOrderStatusFrameworkService {


	/**
	 * 更新之前调用，一般用于参数检查等
	 * @param param
	 * @return
	 */
	protected boolean preUpdate(UpdateOrderStatusParam param){return true;}


	@Transactional
	@Override
	public boolean updateOrderStatus(UpdateOrderStatusParam param) {
		boolean preUpdate = preUpdate(param);
		if (!preUpdate) {
			return false;
		}
		boolean doUpdateOrderStatus = doUpdateOrderStatus(param);
		if (doUpdateOrderStatus) {
			postUpdate(param);
		}
		return doUpdateOrderStatus;
	}
	public abstract boolean doUpdateOrderStatus(UpdateOrderStatusParam param);
	/**
	 * 更新之前调用，一般用于参数检查等
	 * @param param
	 * @return
	 */
	protected void postUpdate(UpdateOrderStatusParam param){}

}
