package scatter.order.rest.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.security.LoginUserTool;
import scatter.common.rest.tools.RequestTool;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.form.OrderCloseForm;
import scatter.order.pojo.form.OrderCreateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import scatter.order.pojo.param.CreateOrderParam;
import scatter.order.pojo.param.UpdateOrderStatusParam;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.vo.QueryOrderVo;
import scatter.order.rest.OrderConfiguration;
import scatter.order.rest.mapstruct.OrderMapStruct;
import scatter.order.rest.service.IOrderService;
import scatter.order.rest.service.IOrderStatusFrameworkService;
import scatter.order.rest.service.impl.OrderFrameworkHelperService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * <p>
 * 订单框架相关接口
 * </p>
 *
 * @author yangwei
 * @since 2021-08-11 22:20
 */
@Slf4j
@Api(tags = "订单框架相关接口")
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/order")
public class OrderFrameworkController extends SuperController {


	@Autowired
	private OrderFrameworkHelperService orderFrameworkHelperService;

	@Autowired
	private DictService dictService;

	@Autowired
	private IOrderService iOrderService;


	@ApiOperation("创建订单")
	@PreAuthorize("hasAuthority('appclient')")
	@PostMapping("/create")
	@ResponseStatus(HttpStatus.CREATED)
	public QueryOrderVo createOrder(@RequestBody @Valid OrderCreateForm addForm, HttpServletRequest request) {
		CreateOrderParam param = OrderMapStruct.INSTANCE.map(addForm);
		// ip这设置
		String remoteAddr = RequestTool.getRemoteAddr(request);
		param.setClientIp(remoteAddr);
		// 当前登录用户
		param.setUserId(LoginUserTool.getLoginUserId());
		String order = orderFrameworkHelperService.getICreateOrderFrameworkService(addForm.getChannelCode(), addForm.getCategoryDictValue()).createOrder(param);
		return orderFrameworkHelperService.getIQueryOrderFrameworkService(addForm.getChannelCode(), addForm.getCategoryDictValue()).getOrderByOrderNo(order);

	}


	@ApiOperation("关闭订单")
	@PreAuthorize("hasAuthority('appclient')")
	@PostMapping("/close")
	@ResponseStatus(HttpStatus.CREATED)
	public Boolean close(@RequestBody @Valid OrderCloseForm form) {
		Order byOrderNo = iOrderService.getByOrderNo(form.getOrderNo());

		String categoryDictValue = dictService.getValueById(byOrderNo.getCategoryDictId());


		IOrderStatusFrameworkService iOrderStatusFrameworkService = orderFrameworkHelperService.getIOrderStatusFrameworkService(byOrderNo.getChannelCode(), categoryDictValue);
		UpdateOrderStatusParam updateOrderStatusParam = new UpdateOrderStatusParam();
		updateOrderStatusParam.setOrderNo(form.getOrderNo());
		updateOrderStatusParam.setStatus(OrderConstants.OrderStatusItem.closed.itemValue());
		updateOrderStatusParam.setChannel(byOrderNo.getChannelCode());
		updateOrderStatusParam.setCategoryDictValue(categoryDictValue);

		boolean b = iOrderStatusFrameworkService.updateOrderStatus(updateOrderStatusParam);
		if (!b) {
			log.error("订单关闭失败，orderNo={}",form.getOrderNo());
			throw new BusinessException(StrUtil.format("订单关闭失败，orderNo={}",form.getOrderNo()));

		}
		return b;
	}

	@ApiOperation("分页查询订单")
	@PreAuthorize("hasAuthority('appclient')")
	@GetMapping("/queryPage")
	@ResponseStatus(HttpStatus.OK)
	public IPage<QueryOrderVo> queryPage(OrderPageQueryForm listPageForm) {
		return orderFrameworkHelperService.getIQueryOrderFrameworkService(listPageForm.getChannel(),listPageForm.getCategoryDictValue()).queryOrderPage(listPageForm);
	}

	@ApiOperation("根据订单号查询订单")
	@PreAuthorize("hasAuthority('appclient')")
	@GetMapping("/getByOrderNo/{orderNo}")
	@ResponseStatus(HttpStatus.OK)
	public QueryOrderVo getByOrderNo(@PathVariable String orderNo,String channel,String categoryDictValue) {
		return orderFrameworkHelperService.getIQueryOrderFrameworkService(channel,categoryDictValue).getOrderByOrderNo(orderNo);
	}
}
