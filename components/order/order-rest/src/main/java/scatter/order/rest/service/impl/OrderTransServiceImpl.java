package scatter.order.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.order.pojo.po.Order;
import scatter.order.rest.service.IOrderService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Component
public class OrderTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IOrderService orderService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Order.TRANS_ORDER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Order.TRANS_ORDER_BY_ID)) {
            Order byId = orderService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Order.TRANS_ORDER_BY_ID)) {
            return orderService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
