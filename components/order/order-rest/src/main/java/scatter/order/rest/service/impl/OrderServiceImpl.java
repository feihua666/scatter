package scatter.order.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.rest.mapper.OrderMapper;
import scatter.order.rest.service.IOrderGoodsService;
import scatter.order.rest.service.IOrderNoGeneratorService;
import scatter.order.rest.service.IOrderRefundService;
import scatter.order.rest.service.IOrderService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.order.pojo.form.OrderAddForm;
import scatter.order.pojo.form.OrderUpdateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Service
@Transactional
public class OrderServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<OrderMapper, Order, OrderAddForm, OrderUpdateForm, OrderPageQueryForm> implements IOrderService {

    @Autowired
    private List<IOrderNoGeneratorService> orderNoGeneratorServices;

    @Autowired
    protected DictService dictService;
    @Autowired
    protected IOrderRefundService iOrderRefundService;
    @Autowired
    protected IOrderGoodsService iOrderGoodsService;


    @Override
    public void preAdd(OrderAddForm addForm,Order po) {
        super.preAdd(addForm,po);
        // 设置订单号
        String orderNo = generateOrderNO(addForm.getCategoryDictValue());
        po.setOrderNo(orderNo);

    }

    @Override
    public void postAdd(OrderAddForm orderAddForm, Order po) {

    }

    /**
     * 生成订单号
     * @param categoryDictValue
     * @return
     */
    @Override
    public String generateOrderNO(String categoryDictValue){
        String orderNO = null;
        for (IOrderNoGeneratorService orderNoGeneratorService : orderNoGeneratorServices) {
            orderNO = orderNoGeneratorService.generateOrderNo(categoryDictValue);
            if (orderNO != null) {
                break;
            }
        }
        if (orderNO == null) {
            throw new BusinessException("订单号生成失败");
        }
        return orderNO;
    }

    @Override
    public String computeOrderStatusByGoodsStatus(String orderId) {
        List<OrderGoods> allOrderGoods = iOrderGoodsService.getByOrderId(orderId);
        String refundStatusDictId = dictService.getIdByGroupCodeAndValue(OrderConstants.OrderStatusGroupCode.order_status.groupCode(), OrderConstants.OrderStatusItem.refund.itemValue());
        String refundingStatusDictId = dictService.getIdByGroupCodeAndValue(OrderConstants.OrderStatusGroupCode.order_status.groupCode(), OrderConstants.OrderStatusItem.refunding.itemValue());

        String result = null;
        // 判断商品中是否
        boolean isAllGoodsRefunding = allOrderGoods.stream().allMatch(g -> isEqual(g.getStatusDictId(), refundingStatusDictId));
        if (isAllGoodsRefunding) {
            // 如果全部在退单中，直接订单改为退单中
            result = refundingStatusDictId;
        }else if(allOrderGoods.stream().allMatch(g -> isEqual(g.getStatusDictId(), refundStatusDictId))){
            // 如果全部已退单，直接订单改为已退单
            result = refundStatusDictId;
        }else if(allOrderGoods.stream().anyMatch(g -> isEqual(g.getStatusDictId(), refundStatusDictId))
                && allOrderGoods.stream().anyMatch(g -> isEqual(g.getStatusDictId(), refundStatusDictId))){
            // 即有已退款也有退款中，状态记为部分退款中
            result = dictService.getIdByGroupCodeAndValue(OrderConstants.OrderStatusGroupCode.order_status.groupCode(), OrderConstants.OrderStatusItem.refunding_part_goods.itemValue());
        }else if(allOrderGoods.stream().anyMatch(g -> isEqual(g.getStatusDictId(), refundStatusDictId))){
            // 状态记为部分退款
            result = dictService.getIdByGroupCodeAndValue(OrderConstants.OrderStatusGroupCode.order_status.groupCode(), OrderConstants.OrderStatusItem.refund_part_goods.itemValue());
        }else if(allOrderGoods.stream().anyMatch(g -> isEqual(g.getStatusDictId(), refundingStatusDictId))){
            // 状态记为部分退款中
            result = dictService.getIdByGroupCodeAndValue(OrderConstants.OrderStatusGroupCode.order_status.groupCode(), OrderConstants.OrderStatusItem.refunding_part_goods.itemValue());
        }

        return result;
    }


    @Override
    public void preUpdate(OrderUpdateForm updateForm,Order po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getOrderNo())) {
            Order byId = getById(updateForm.getId());
            // 如果订单号有改动
            if (!isEqual(updateForm.getOrderNo(), byId.getOrderNo())) {
                // 订单号已存在不能修改
                assertByColumn(updateForm.getOrderNo(),Order::getOrderNo,false);
            }
        }

    }
}
