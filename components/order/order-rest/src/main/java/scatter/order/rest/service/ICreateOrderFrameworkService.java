package scatter.order.rest.service;

/**
 * <p>
 * 创建订单框架服务
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 12:29
 */
public interface ICreateOrderFrameworkService<T> extends IOrderFrameworkService {


	/**
	 * 创建订单
	 * @param param
	 * @return 返回订单号
	 */
	String createOrder(T param);
}
