package scatter.order.rest.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import scatter.order.rest.service.IOrderNoGeneratorService;

/**
 * <p>
 * 默认订单号生成器
 * </p>
 *
 * @author yangwei
 * @since 2021-06-26 20:01
 */
@Slf4j
public class DefaultOrderNoGeneratorServiceImpl implements IOrderNoGeneratorService {
	@Override
	public String generateOrderNo(String categoryDictValue) {
		String orderNO = IdWorker.getIdStr();
		log.info("默认订单号生成器生成订单号，categoryDictValue={},orderNo={}",categoryDictValue,orderNO);
		return orderNO;
	}
}
