package scatter.order.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.order.pojo.po.OrderRefundGoods;
import scatter.order.pojo.vo.OrderRefundGoodsVo;
import scatter.order.pojo.form.OrderRefundGoodsAddForm;
import scatter.order.pojo.form.OrderRefundGoodsUpdateForm;
import scatter.order.pojo.form.OrderRefundGoodsPageQueryForm;

/**
 * <p>
 * 退款商品 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderRefundGoodsMapStruct extends IBaseVoMapStruct<OrderRefundGoods, OrderRefundGoodsVo>,
                                  IBaseAddFormMapStruct<OrderRefundGoods,OrderRefundGoodsAddForm>,
                                  IBaseUpdateFormMapStruct<OrderRefundGoods,OrderRefundGoodsUpdateForm>,
                                  IBaseQueryFormMapStruct<OrderRefundGoods,OrderRefundGoodsPageQueryForm>{
    OrderRefundGoodsMapStruct INSTANCE = Mappers.getMapper( OrderRefundGoodsMapStruct.class );

}
