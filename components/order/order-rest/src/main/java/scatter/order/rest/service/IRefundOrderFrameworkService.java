package scatter.order.rest.service;

import scatter.order.pojo.constant.OrderConstants;

/**
 * <p>
 * 退单，退款服务
 * </p>
 *
 * @author yangwei
 * @since 2021-07-05 04:40
 */
public interface IRefundOrderFrameworkService<T> extends IOrderFrameworkService{


	/**
	 * 用于查找支持的退款服务
	 * @param channel 这是一个渠道标识字符串，用来解释是哪个渠道过来的订单
	 * @param CategoryDictValue 订单分类字典值{@link OrderConstants.OrderCategoryDictItem}
	 * @return
	 */
	boolean support(String channel,String CategoryDictValue);

	/**
	 * 退款
	 * @param param 退款可用参数
	 * @return
	 */
	boolean refund(T param);

}
