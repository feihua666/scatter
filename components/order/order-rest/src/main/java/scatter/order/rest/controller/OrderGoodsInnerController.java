package scatter.order.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.rest.OrderConfiguration;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.rest.service.IOrderGoodsService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 订单商品表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/inner/order-goods")
public class OrderGoodsInnerController extends BaseInnerController<OrderGoods> {
 @Autowired
 private IOrderGoodsService orderGoodsService;

 public IOrderGoodsService getService(){
     return orderGoodsService;
 }
}
