package scatter.order.rest.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 * 查询订单框架服务
 * 适用场景：
 * 1. 用户查询自己的订单信息
 * 2. 后台管理查询订单信息
 * </p>
 *
 * @author yangwei
 * @since 2021-06-27 12:29
 */
public interface IQueryOrderFrameworkService<T,R> extends IOrderFrameworkService {


	/**
	 * 查询订单
	 * @param param 请求参数对象
	 * @return 返回分页数据
	 */

	Page<R> queryOrderPage(T param);

	/**
	 * 根据订单号获取订单
	 * @param orderNo
	 * @return
	 */
	R getOrderByOrderNo(String orderNo);

}
