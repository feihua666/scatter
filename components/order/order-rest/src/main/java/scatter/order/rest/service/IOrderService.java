package scatter.order.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.order.pojo.po.Order;
import scatter.common.rest.service.IBaseService;
import scatter.order.pojo.form.OrderAddForm;
import scatter.order.pojo.form.OrderUpdateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import java.util.List;
/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
public interface IOrderService extends IBaseService<Order> {


    /**
     * 根据订单号查询
     * @param orderNo
     * @return
     */
    default Order getByOrderNo(String orderNo) {
        Assert.hasText(orderNo,"orderNo不能为空");
        return getOne(Wrappers.<Order>lambdaQuery().eq(Order::getOrderNo, orderNo));
    }
    /**
     * 生成订单号
     * @param categoryDictValue
     * @return
     */
    String generateOrderNO(String categoryDictValue);

    /**
     * 根据订单商品状态计算订单状态
     * @param orderId
     * @return 订单状态字典id 可能返回为空，请自行处理为空的情况
     */
    String computeOrderStatusByGoodsStatus(String orderId);
}
