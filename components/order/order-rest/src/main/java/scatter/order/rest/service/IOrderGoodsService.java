package scatter.order.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.order.pojo.po.OrderGoods;
import scatter.common.rest.service.IBaseService;
import scatter.order.pojo.form.OrderGoodsAddForm;
import scatter.order.pojo.form.OrderGoodsUpdateForm;
import scatter.order.pojo.form.OrderGoodsPageQueryForm;
import java.util.List;
/**
 * <p>
 * 订单商品表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
public interface IOrderGoodsService extends IBaseService<OrderGoods> {

    /**
     * 根据订单id查询
     * @param orderId
     * @return
     */
    default List<OrderGoods> getByOrderId(String orderId) {
        Assert.hasText(orderId,"orderId不能为空");
        return list(Wrappers.<OrderGoods>lambdaQuery().eq(OrderGoods::getOrderId, orderId));
    }
    /**
     * 根据订单号查询
     * @param orderNo
     * @return
     */
    List<OrderGoods> getByOrderNo(String orderNo);
    /**
     * 根据订单orderIds查询
     * @param orderIds
     * @return
     */
    default List<OrderGoods> getByOrderIds(List<String> orderIds) {
        Assert.notEmpty(orderIds,"orderIds 不能为空");
        return list(Wrappers.<OrderGoods>lambdaQuery().in(OrderGoods::getOrderId, orderIds));
    }
    /**
     * 根据商品id查询
     * @param goodsId
     * @return
     */
    default List<OrderGoods> getByGoodsId(String goodsId) {
        Assert.hasText(goodsId,"goodsId 不能为空");
        return list(Wrappers.<OrderGoods>lambdaQuery().eq(OrderGoods::getGoodsId, goodsId));
    }
    /**
     * 根据商品id和用户id查询
     * @param goodsId
     * @return
     */
    default List<OrderGoods> getByGoodsIdAndUserId(String goodsId,String userId) {
        Assert.hasText(goodsId,"goodsId 不能为空");
        Assert.hasText(userId,"userId 不能为空");
        return list(Wrappers.<OrderGoods>lambdaQuery().eq(OrderGoods::getGoodsId, goodsId).eq(OrderGoods::getUserId,userId));
    }
    /**
     * 根据商品id和订单id查询
     * @param goodsId
     * @return
     */
    default OrderGoods getByGoodsIdAndOrderId(String goodsId,String orderId) {
        Assert.hasText(goodsId,"goodsId 不能为空");
        Assert.hasText(orderId,"orderId 不能为空");
        return getOne(Wrappers.<OrderGoods>lambdaQuery().eq(OrderGoods::getGoodsId, goodsId).eq(OrderGoods::getOrderId,orderId));
    }
    /**
     * 根据商品 id 和订单 id 查询
     * @param goodsIds
     * @param orderId
     * @return
     */
    default List<OrderGoods> getByGoodsIdsAndOrderId(List<String> goodsIds,String orderId) {
        Assert.notEmpty(goodsIds,"goodsIds 不能为空");
        Assert.hasText(orderId,"orderId 不能为空");
        return list(Wrappers.<OrderGoods>lambdaQuery().in(OrderGoods::getGoodsId, goodsIds).eq(OrderGoods::getOrderId,orderId));
    }
    /**
     * 根据商品 id 和订单号 查询
     * @param goodsIds
     * @param orderNo
     * @return
     */
    default List<OrderGoods> getByGoodsIdsAndOrderNo(List<String> goodsIds,String orderNo) {
        Assert.notEmpty(goodsIds,"goodsIds 不能为空");
        Assert.hasText(orderNo,"orderNo 不能为空");
        return list(Wrappers.<OrderGoods>lambdaQuery().in(OrderGoods::getGoodsId, goodsIds).eq(OrderGoods::getOrderNo,orderNo));
    }
}
