package scatter.order.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.security.LoginUserTool;
import scatter.common.rest.tools.RequestTool;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.form.*;
import scatter.order.pojo.param.CreateOrderParam;
import scatter.order.pojo.param.RefundOrderParam;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.vo.QueryOrderVo;
import scatter.order.rest.OrderConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.order.pojo.po.OrderRefund;
import scatter.order.pojo.vo.OrderRefundVo;
import scatter.order.rest.mapstruct.OrderMapStruct;
import scatter.order.rest.mapstruct.OrderRefundMapStruct;
import scatter.order.rest.service.ICreateOrderFrameworkService;
import scatter.order.rest.service.IOrderRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.rest.service.IOrderService;
import scatter.order.rest.service.IRefundOrderFrameworkService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
/**
 * <p>
 * 订单退款单表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Api(tags = "订单退款单相关接口")
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/order-refund")
public class OrderRefundController extends BaseAddUpdateQueryFormController<OrderRefund, OrderRefundVo, OrderRefundAddForm, OrderRefundUpdateForm, OrderRefundPageQueryForm> {
    @Autowired
    private IOrderRefundService iOrderRefundService;

    @Autowired
    private List<IRefundOrderFrameworkService> refundOrderFrameworkServices;

    @Autowired
    private IOrderService iOrderService;
    @Autowired
    private DictService dictService;

    @ApiOperation("添加订单退款单")
    @PreAuthorize("hasAuthority('OrderRefund:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public OrderRefundVo add(@RequestBody @Valid OrderRefundAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询订单退款单")
    @PreAuthorize("hasAuthority('OrderRefund:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public OrderRefundVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除订单退款单")
    @PreAuthorize("hasAuthority('OrderRefund:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新订单退款单")
    @PreAuthorize("hasAuthority('OrderRefund:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public OrderRefundVo update(@RequestBody @Valid OrderRefundUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询订单退款单")
    @PreAuthorize("hasAuthority('OrderRefund:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<OrderRefundVo> getList(OrderRefundPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询订单退款单")
    @PreAuthorize("hasAuthority('OrderRefund:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<OrderRefundVo> getPage(OrderRefundPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    @ApiOperation("订单退款")
    @PreAuthorize("hasAnyAuthority('appclient')")
    @PostMapping("/refund")
    @ResponseStatus(HttpStatus.CREATED)
    public boolean refundOrder(@RequestBody @Valid OrderRefundForm refundForm, HttpServletRequest request) {
        RefundOrderParam param = OrderRefundMapStruct.INSTANCE.map(refundForm);
        // ip这设置
        String remoteAddr = RequestTool.getRemoteAddr(request);
        param.setClientIp(remoteAddr);

        Order order = iOrderService.getByOrderNo(refundForm.getOrderNo());
        String categoryDictValue = dictService.getValueById(order.getCategoryDictId());


        for (IRefundOrderFrameworkService refundOrderFrameworkService : refundOrderFrameworkServices) {
            if (refundOrderFrameworkService.support(order.getChannelCode(),categoryDictValue)) {
                boolean refundResult =  refundOrderFrameworkService.refund(param);
                return refundResult;
            }
        }
        throw new RuntimeException("未能处理退款，未找到退款服务");
    }

}
