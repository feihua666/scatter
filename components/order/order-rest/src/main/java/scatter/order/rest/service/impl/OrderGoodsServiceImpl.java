package scatter.order.rest.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.Assert;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.rest.mapper.OrderGoodsMapper;
import scatter.order.rest.service.IOrderGoodsService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.order.pojo.form.OrderGoodsAddForm;
import scatter.order.pojo.form.OrderGoodsUpdateForm;
import scatter.order.pojo.form.OrderGoodsPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
import scatter.order.rest.service.IOrderService;

import java.util.List;

/**
 * <p>
 * 订单商品表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-07-03
 */
@Service
@Transactional
public class OrderGoodsServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<OrderGoodsMapper, OrderGoods, OrderGoodsAddForm, OrderGoodsUpdateForm, OrderGoodsPageQueryForm> implements IOrderGoodsService {

    @Lazy
    @Autowired
    private IOrderService iOrderService;

    @Override
    public void preAdd(OrderGoodsAddForm addForm,OrderGoods po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(OrderGoodsUpdateForm updateForm,OrderGoods po) {
        super.preUpdate(updateForm,po);

    }

    @Override
    public List<OrderGoods> getByOrderNo(String orderNo) {
        Assert.hasText(orderNo,"orderNo 不能为空");
        Order byOrderNo = iOrderService.getByOrderNo(orderNo);
        if (byOrderNo == null) {
            return null;
        }
        return list(Wrappers.<OrderGoods>lambdaQuery().eq(OrderGoods::getOrderId, byOrderNo.getId()));
    }
}
