package scatter.order.rest.service;

/**
 * <p>
 * 订单退款单号生成器
 * </p>
 *
 * @author yangwei
 * @since 2021-08-07 16:58
 */
public interface IOrderRefundNoGeneratorService {
	/**
	 * 生成订单退款单号方法
	 * @param categoryDictValue 订单分类id
	 * @return
	 */
	String generateOrderRefundNo(String categoryDictValue);
}
