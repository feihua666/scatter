package scatter.order.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.common.rest.security.LoginUserTool;
import scatter.common.rest.tools.RequestTool;
import scatter.order.pojo.form.OrderAddForm;
import scatter.order.pojo.form.OrderCreateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import scatter.order.pojo.form.OrderUpdateForm;
import scatter.order.pojo.param.CreateOrderParam;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.vo.OrderVo;
import scatter.order.pojo.vo.QueryOrderVo;
import scatter.order.rest.OrderConfiguration;
import scatter.order.rest.mapstruct.OrderMapStruct;
import scatter.order.rest.service.ICreateOrderFrameworkService;
import scatter.order.rest.service.IOrderService;
import scatter.order.rest.service.IQueryOrderFrameworkService;
import scatter.order.rest.service.impl.OrderFrameworkHelperService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@Api(tags = "订单相关接口")
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/order")
public class OrderController extends BaseAddUpdateQueryFormController<Order, OrderVo, OrderAddForm, OrderUpdateForm, OrderPageQueryForm> {
    @Autowired
    private IOrderService iOrderService;


     @ApiOperation("添加订单")
     @PreAuthorize("hasAnyAuthority('Order:add','appclient')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public OrderVo add(@RequestBody @Valid OrderAddForm addForm, HttpServletRequest request) {
         // ip这设置
         String remoteAddr = RequestTool.getRemoteAddr(request);
         addForm.setClientIp(remoteAddr);
         // 当前登录用户
         addForm.setUserId(LoginUserTool.getLoginUserId());
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询订单")
     @PreAuthorize("hasAuthority('Order:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public OrderVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除订单")
     @PreAuthorize("hasAuthority('Order:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新订单")
     @PreAuthorize("hasAuthority('Order:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public OrderVo update(@RequestBody @Valid OrderUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询订单")
    @PreAuthorize("hasAuthority('Order:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderVo> getList(OrderPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询订单")
    @PreAuthorize("hasAuthority('Order:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<OrderVo> getPage(OrderPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

}
