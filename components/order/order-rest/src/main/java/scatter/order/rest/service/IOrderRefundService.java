package scatter.order.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.order.pojo.po.OrderRefund;
import scatter.common.rest.service.IBaseService;
import scatter.order.pojo.form.OrderRefundAddForm;
import scatter.order.pojo.form.OrderRefundUpdateForm;
import scatter.order.pojo.form.OrderRefundPageQueryForm;
import java.util.List;
/**
 * <p>
 * 订单退款单表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
public interface IOrderRefundService extends IBaseService<OrderRefund> {


    /**
     * 根据退款单号查询
     * @param refundNo
     * @return
     */
    default OrderRefund getByRefundNo(String refundNo) {
        Assert.hasText(refundNo,"refundNo 不能为空");
        return getOne(Wrappers.<OrderRefund>lambdaQuery().eq(OrderRefund::getRefundNo, refundNo));
    }
    /**
     * 根据订单单号查询
     * @param orderNo
     * @return
     */
    default List<OrderRefund> getByOrderNo(String orderNo) {
        Assert.hasText(orderNo,"orderNo 不能为空");
        return list(Wrappers.<OrderRefund>lambdaQuery().eq(OrderRefund::getOrderNo, orderNo));
    }
    /**
     * 生成订单退款单号
     * @param categoryDictValue
     * @return
     */
    String generateOrderRefundNO(String categoryDictValue);
}
