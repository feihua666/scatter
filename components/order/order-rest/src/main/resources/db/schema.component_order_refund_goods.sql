DROP TABLE IF EXISTS component_order_refund_goods;
CREATE TABLE `component_order_refund_goods` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `order_id` varchar(20) NOT NULL COMMENT '订单id,外键',
  `order_no` varchar(50) NOT NULL COMMENT '订单号',
  `order_refund_id` varchar(20) NOT NULL COMMENT '退款单id,外键',
  `order_refund_no` varchar(32) NOT NULL COMMENT '退款单号',
  `goods_id` varchar(20) NOT NULL COMMENT '订单商品id',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_refund_id` (`order_refund_id`),
  KEY `order_id` (`order_id`),
  KEY `order_no` (`order_no`),
  KEY `order_refund_no` (`order_refund_no`),
  KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款商品表';
