DROP TABLE IF EXISTS component_order_refund;
CREATE TABLE `component_order_refund` (
  `id` varchar(20) NOT NULL COMMENT '退款id',
  `order_id` varchar(20) NOT NULL COMMENT '订单id',
  `order_no` varchar(50) NOT NULL COMMENT '订单号',
  `refund_no` varchar(50) NOT NULL COMMENT '退款单号',
  `description` varchar(255) NOT NULL COMMENT '退款描述，文本',
  `amount` int(11) NOT NULL COMMENT '退款金额，单位为分',
  `is_multiple` tinyint(1) NOT NULL COMMENT '是否分多次退款',
  `currency_dict_id` varchar(20) NOT NULL COMMENT '货币类型,字典id',
  `status_dict_id` varchar(20) NOT NULL COMMENT '退款状态,字典id',
  `client_ip` varchar(50) NOT NULL COMMENT '发起退款客户端ip',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `refund_no` (`refund_no`),
  KEY `order_id` (`order_id`),
  KEY `order_no` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单退款单表';
