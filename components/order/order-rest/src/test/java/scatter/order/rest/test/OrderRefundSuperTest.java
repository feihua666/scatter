package scatter.order.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.pojo.po.OrderRefund;
import scatter.order.pojo.form.OrderRefundAddForm;
import scatter.order.pojo.form.OrderRefundUpdateForm;
import scatter.order.pojo.form.OrderRefundPageQueryForm;
import scatter.order.rest.service.IOrderRefundService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 订单退款单 测试类基类
* </p>
*
* @author yw
* @since 2021-08-07
*/
public class OrderRefundSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IOrderRefundService orderRefundService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return orderRefundService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return orderRefundService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public OrderRefund mockPo() {
        return JMockData.mock(OrderRefund.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public OrderRefundAddForm mockAddForm() {
        return JMockData.mock(OrderRefundAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public OrderRefundUpdateForm mockUpdateForm() {
        return JMockData.mock(OrderRefundUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public OrderRefundPageQueryForm mockPageQueryForm() {
        return JMockData.mock(OrderRefundPageQueryForm.class, mockConfig);
    }
}