package scatter.order.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.order.pojo.po.OrderRefundGoods;
import scatter.order.pojo.form.OrderRefundGoodsAddForm;
import scatter.order.pojo.form.OrderRefundGoodsUpdateForm;
import scatter.order.pojo.form.OrderRefundGoodsPageQueryForm;
import scatter.order.rest.test.OrderRefundGoodsSuperTest;
import scatter.order.rest.service.IOrderRefundGoodsService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 退款商品 服务测试类
* </p>
*
* @author yw
* @since 2021-08-16
*/
@SpringBootTest
public class OrderRefundGoodsServiceTest extends OrderRefundGoodsSuperTest{

    @Autowired
    private IOrderRefundGoodsService orderRefundGoodsService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<OrderRefundGoods> pos = orderRefundGoodsService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}