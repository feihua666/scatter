package scatter.order.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.pojo.form.OrderGoodsAddForm;
import scatter.order.pojo.form.OrderGoodsUpdateForm;
import scatter.order.pojo.form.OrderGoodsPageQueryForm;
import scatter.order.rest.test.OrderGoodsSuperTest;
import scatter.order.rest.service.IOrderGoodsService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 订单商品 服务测试类
* </p>
*
* @author yw
* @since 2021-07-03
*/
@SpringBootTest
public class OrderGoodsServiceTest extends OrderGoodsSuperTest{

    @Autowired
    private IOrderGoodsService orderGoodsService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<OrderGoods> pos = orderGoodsService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}