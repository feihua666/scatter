package scatter.order.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.form.OrderAddForm;
import scatter.order.pojo.form.OrderUpdateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import scatter.order.rest.test.OrderSuperTest;
import scatter.order.rest.service.IOrderService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 订单 服务测试类
* </p>
*
* @author yw
* @since 2021-06-20
*/
@SpringBootTest
public class OrderServiceTest extends OrderSuperTest{

    @Autowired
    private IOrderService orderService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Order> pos = orderService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}