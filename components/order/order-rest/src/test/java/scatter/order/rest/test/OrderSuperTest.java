package scatter.order.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.form.OrderAddForm;
import scatter.order.pojo.form.OrderUpdateForm;
import scatter.order.pojo.form.OrderPageQueryForm;
import scatter.order.rest.service.IOrderService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 订单 测试类基类
* </p>
*
* @author yw
* @since 2021-06-20
*/
public class OrderSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IOrderService orderService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return orderService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return orderService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Order mockPo() {
        return JMockData.mock(Order.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public OrderAddForm mockAddForm() {
        return JMockData.mock(OrderAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public OrderUpdateForm mockUpdateForm() {
        return JMockData.mock(OrderUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public OrderPageQueryForm mockPageQueryForm() {
        return JMockData.mock(OrderPageQueryForm.class, mockConfig);
    }
}