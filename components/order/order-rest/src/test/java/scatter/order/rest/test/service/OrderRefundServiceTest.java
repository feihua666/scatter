package scatter.order.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.order.pojo.po.OrderRefund;
import scatter.order.pojo.form.OrderRefundAddForm;
import scatter.order.pojo.form.OrderRefundUpdateForm;
import scatter.order.pojo.form.OrderRefundPageQueryForm;
import scatter.order.rest.test.OrderRefundSuperTest;
import scatter.order.rest.service.IOrderRefundService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 订单退款单 服务测试类
* </p>
*
* @author yw
* @since 2021-08-07
*/
@SpringBootTest
public class OrderRefundServiceTest extends OrderRefundSuperTest{

    @Autowired
    private IOrderRefundService orderRefundService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<OrderRefund> pos = orderRefundService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}