package scatter.order.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.pojo.po.OrderGoods;
import scatter.order.pojo.form.OrderGoodsAddForm;
import scatter.order.pojo.form.OrderGoodsUpdateForm;
import scatter.order.pojo.form.OrderGoodsPageQueryForm;
import scatter.order.rest.service.IOrderGoodsService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 订单商品 测试类基类
* </p>
*
* @author yw
* @since 2021-07-03
*/
public class OrderGoodsSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IOrderGoodsService orderGoodsService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return orderGoodsService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return orderGoodsService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public OrderGoods mockPo() {
        return JMockData.mock(OrderGoods.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public OrderGoodsAddForm mockAddForm() {
        return JMockData.mock(OrderGoodsAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public OrderGoodsUpdateForm mockUpdateForm() {
        return JMockData.mock(OrderGoodsUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public OrderGoodsPageQueryForm mockPageQueryForm() {
        return JMockData.mock(OrderGoodsPageQueryForm.class, mockConfig);
    }
}