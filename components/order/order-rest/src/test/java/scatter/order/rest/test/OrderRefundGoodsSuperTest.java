package scatter.order.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.pojo.po.OrderRefundGoods;
import scatter.order.pojo.form.OrderRefundGoodsAddForm;
import scatter.order.pojo.form.OrderRefundGoodsUpdateForm;
import scatter.order.pojo.form.OrderRefundGoodsPageQueryForm;
import scatter.order.rest.service.IOrderRefundGoodsService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 退款商品 测试类基类
* </p>
*
* @author yw
* @since 2021-08-16
*/
public class OrderRefundGoodsSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IOrderRefundGoodsService orderRefundGoodsService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return orderRefundGoodsService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return orderRefundGoodsService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public OrderRefundGoods mockPo() {
        return JMockData.mock(OrderRefundGoods.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public OrderRefundGoodsAddForm mockAddForm() {
        return JMockData.mock(OrderRefundGoodsAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public OrderRefundGoodsUpdateForm mockUpdateForm() {
        return JMockData.mock(OrderRefundGoodsUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public OrderRefundGoodsPageQueryForm mockPageQueryForm() {
        return JMockData.mock(OrderRefundGoodsPageQueryForm.class, mockConfig);
    }
}