package scatter.order.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.rest.OrderConfiguration;
import scatter.order.pojo.po.Order;
import scatter.order.rest.service.IOrderService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 订单表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-20
 */
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/inner/order")
public class OrderInnerController extends BaseInnerController<Order> {
 @Autowired
 private IOrderService orderService;

 public IOrderService getService(){
     return orderService;
 }
}
