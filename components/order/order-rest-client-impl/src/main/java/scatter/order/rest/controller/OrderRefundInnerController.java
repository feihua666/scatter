package scatter.order.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.rest.OrderConfiguration;
import scatter.order.pojo.po.OrderRefund;
import scatter.order.rest.service.IOrderRefundService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 订单退款单表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/inner/order-refund")
public class OrderRefundInnerController extends BaseInnerController<OrderRefund> {
 @Autowired
 private IOrderRefundService orderRefundService;

 public IOrderRefundService getService(){
     return orderRefundService;
 }
}
