package scatter.order.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.rest.OrderConfiguration;
import scatter.order.pojo.po.OrderRefundGoods;
import scatter.order.rest.service.IOrderRefundGoodsService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 退款商品表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-16
 */
@RestController
@RequestMapping(OrderConfiguration.CONTROLLER_BASE_PATH + "/inner/order-refund-goods")
public class OrderRefundGoodsInnerController extends BaseInnerController<OrderRefundGoods> {
 @Autowired
 private IOrderRefundGoodsService orderRefundGoodsService;

 public IOrderRefundGoodsService getService(){
     return orderRefundGoodsService;
 }
}
