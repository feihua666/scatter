import OrderUrl from './OrderUrl.js'
const OrderForm = [
    {
        OrderSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'title',
        },
        element:{
            label: '订单标题',
            required: true,
        }
    },
    {
        OrderSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderNo',
        },
        element:{
            label: '订单号',
            required: true,
        }
    },
    {
        OrderSearchList: false,


        field: {
            name: 'description',
        },
        element:{
            label: '订单描述',
            required: true,
        }
    },
    {
        OrderSearchList: false,

        field: {
            name: 'expireAt',
        },
        element:{
            label: '订单失效时间',
        }
    },
    {
        OrderSearchList: false,


        field: {
            name: 'totalAmount',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '订单总金额',
            required: true,
        }
    },
    {
        OrderSearchList: false,


        field: {
            name: 'currencyDictId',
        },
        element:{
            label: '货币类型',
            required: true,
        }
    },
    {
        OrderSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'statusDictId',
        },
        element:{
            label: '订单状态',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'order_status'
            }
        }
    },
    {
        OrderSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'categoryDictId',
        },
        element:{
            label: '订单分类',
            required: true,
            type: 'selectDict',
            options:{
                groupCode: 'order_category'
            }
        }
    },
    {
        OrderSearchList: false,

        field: {
            name: 'userId',
        },
        element:{
            label: '下单归属的用户id',
            required: true,
        }
    },
    {
        OrderSearchList: false,

        field: {
            name: 'clientIp',
        },
        element:{
            label: '下单客户端ip',
            required: true,
        }
    },

]
export default OrderForm