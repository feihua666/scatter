const base = ''

const basePath = base + '/order-goods'

const OrderGoodsUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    // 退款
    refund: base + '/order-refund/refund',
    router: {
        searchList: '/OrderGoodsSearchList',
        add: '/OrderGoodsAdd',
        update: '/OrderGoodsUpdate',
    }
}
export default OrderGoodsUrl