import OrderRefundUrl from './OrderRefundUrl.js'

const OrderRefundRoute = [
    {
        path: OrderRefundUrl.router.searchList,
        component: () => import('./element/OrderRefundSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'OrderRefundSearchList',
            name: '订单退款单管理'
        }
    },
    {
        path: OrderRefundUrl.router.add,
        component: () => import('./element/OrderRefundAdd'),
        meta: {
            code:'OrderRefundAdd',
            name: '订单退款单添加'
        }
    },
    {
        path: OrderRefundUrl.router.update,
        component: () => import('./element/OrderRefundUpdate'),
        meta: {
            code:'OrderRefundUpdate',
            name: '订单退款单修改'
        }
    },
]
export default OrderRefundRoute