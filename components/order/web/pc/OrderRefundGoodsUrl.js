const basePath = '' + '/order-refund-goods'
const OrderRefundGoodsUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/OrderRefundGoodsSearchList',
        add: '/OrderRefundGoodsAdd',
        update: '/OrderRefundGoodsUpdate',
    }
}
export default OrderRefundGoodsUrl