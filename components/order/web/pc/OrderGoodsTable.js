const OrderGoodsTable = [

    {
        prop: 'imageUrl',
        stype: 'image'
    },
    {
        prop: 'goodsName',
        label: '商品名称'
    },
    {
        prop: 'orderNo',
        label: '订单号'
    },
    {
        prop: 'goodsDescription',
        label: '商品描述'
    },
    {
        prop: 'count',
        label: '商品数量'
    },
    {
        prop: 'amount',
        label: '商品单价'
    },
    {
        prop: 'categoryDictName',
        label: '商品订单类型'
    },
    {
        prop: 'statusDictName',
        label: '支付状态'
    },
]
export default OrderGoodsTable