import OrderUrl from './OrderUrl.js'

const OrderRoute = [
    {
        path: OrderUrl.router.searchList,
        component: () => import('./element/OrderSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'OrderSearchList',
            name: '订单管理'
        }
    },
    {
        path: OrderUrl.router.add,
        component: () => import('./element/OrderAdd'),
        meta: {
            code:'OrderAdd',
            name: '订单添加'
        }
    },
    {
        path: OrderUrl.router.update,
        component: () => import('./element/OrderUpdate'),
        meta: {
            code:'OrderUpdate',
            name: '订单修改'
        }
    },
]
export default OrderRoute