const basePath = '' + '/order'
const OrderUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/OrderSearchList',
        add: '/OrderAdd',
        update: '/OrderUpdate',
    }
}
export default OrderUrl