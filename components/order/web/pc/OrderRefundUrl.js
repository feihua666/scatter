const basePath = '' + '/order-refund'
const OrderRefundUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/OrderRefundSearchList',
        add: '/OrderRefundAdd',
        update: '/OrderRefundUpdate',
    }
}
export default OrderRefundUrl