import OrderGoodsUrl from './OrderGoodsUrl.js'

const OrderGoodsRoute = [
    {
        path: OrderGoodsUrl.router.searchList,
        component: () => import('./element/OrderGoodsSearchList'),
        meta: {
            root: false,
            keepAlive: false,
            code:'OrderGoodsSearchList',
            name: '订单商品管理'
        }
    },
    {
        path: OrderGoodsUrl.router.add,
        component: () => import('./element/OrderGoodsAdd'),
        meta: {
            code:'OrderGoodsAdd',
            name: '订单商品添加'
        }
    },
    {
        path: OrderGoodsUrl.router.update,
        component: () => import('./element/OrderGoodsUpdate'),
        meta: {
            code:'OrderGoodsUpdate',
            name: '订单商品修改'
        }
    },
]
export default OrderGoodsRoute