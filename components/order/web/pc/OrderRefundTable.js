const OrderRefundTable = [

    {
        prop: 'orderNo',
        label: '订单号'
    },
    {
        prop: 'refundNo',
        label: '退款单号'
    },
    {
        prop: 'description',
        label: '退款描述'
    },
    {
        prop: 'amount',
        label: '退款金额'
    },
    {
        prop: 'isMultiple',
        label: '是否分多次退款'
    },
    {
        prop: 'currencyDictName',
        label: '货币类型'
    },
    {
        prop: 'statusDictName',
        label: '退款状态'
    },
    {
        prop: 'clientIp',
        label: '退款客户端ip'
    },
]
export default OrderRefundTable