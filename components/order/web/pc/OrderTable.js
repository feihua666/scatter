const OrderTable = [
    {
        prop: 'title',
        label: '订单标题'
    },
    {
        prop: 'orderNo',
        label: '订单号'
    },
    {
        prop: 'description',
        label: '订单描述'
    },
    {
        prop: 'expireAt',
        label: '订单失效时间'
    },
    {
        prop: 'totalAmount',
        label: '订单总金额'
    },
    {
        prop: 'currencyDictName',
        label: '货币类型'
    },
    {
        prop: 'statusDictName',
        label: '订单状态'
    },
    {
        prop: 'categoryDictName',
        label: '订单分类'
    },
    {
        prop: 'payTypeDictName',
        label: '付款方式'
    },
    {
        prop: 'payChannelDictName',
        label: '支付渠道'
    },
    {
        prop: 'channelCode',
        label: '订单渠道'
    },
    {
        prop: 'userNickname',
        label: '下单用户'
    },
    {
        prop: 'clientIp',
        label: '下单客户端ip'
    },
]
export default OrderTable