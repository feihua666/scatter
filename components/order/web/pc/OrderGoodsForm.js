import OrderGoodsUrl from './OrderGoodsUrl.js'
const OrderGoodsForm = [
    {
        OrderGoodsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderNo',
        },
        element:{
            label: '订单号',
            required: true,
        }
    },
    {
        OrderGoodsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'goodsName',
        },
        element:{
            label: '商品名称',
            required: true,
        }
    },

    {
        OrderGoodsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'categoryDictId',
        },
        element:{
            label: '商品订单类型',
            type: 'selectDict',
            options: {
                groupCode: 'order_category'
            },
            required: true,
        }
    },

]
export default OrderGoodsForm