import OrderRefundGoodsUrl from './OrderRefundGoodsUrl.js'
const OrderRefundGoodsForm = [

    {
        OrderRefundGoodsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderNo',
        },
        element:{
            label: '订单号',
            required: true,
        }
    },

    {
        OrderRefundGoodsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderRefundNo',
        },
        element:{
            label: '退款单号',
            required: true,
        }
    },


]
export default OrderRefundGoodsForm