import OrderRefundGoodsForm from './OrderRefundGoodsForm.js'
import OrderRefundGoodsTable from './OrderRefundGoodsTable.js'
import OrderRefundGoodsUrl from './OrderRefundGoodsUrl.js'
const OrderRefundGoodsMixin = {
    computed: {
        OrderRefundGoodsFormOptions() {
            return this.$stDynamicFormTools.options(OrderRefundGoodsForm,this.$options.name)
        },
        OrderRefundGoodsTableOptions() {
            return OrderRefundGoodsTable
        },
        OrderRefundGoodsUrl(){
            return OrderRefundGoodsUrl
        }
    },
}
export default OrderRefundGoodsMixin