import OrderGoodsForm from './OrderGoodsForm.js'
import OrderGoodsTable from './OrderGoodsTable.js'
import OrderGoodsUrl from './OrderGoodsUrl.js'
const OrderGoodsMixin = {
    computed: {
        OrderGoodsFormOptions() {
            return this.$stDynamicFormTools.options(OrderGoodsForm,this.$options.name)
        },
        OrderGoodsTableOptions() {
            return OrderGoodsTable
        },
        OrderGoodsUrl(){
            return OrderGoodsUrl
        }
    },
}
export default OrderGoodsMixin