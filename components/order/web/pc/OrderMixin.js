import OrderForm from './OrderForm.js'
import OrderTable from './OrderTable.js'
import OrderUrl from './OrderUrl.js'
const OrderMixin = {
    computed: {
        OrderFormOptions() {
            return this.$stDynamicFormTools.options(OrderForm,this.$options.name)
        },
        OrderTableOptions() {
            return OrderTable
        },
        OrderUrl(){
            return OrderUrl
        }
    },
}
export default OrderMixin