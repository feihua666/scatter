import OrderRefundGoodsUrl from './OrderRefundGoodsUrl.js'

const OrderRefundGoodsRoute = [
    {
        path: OrderRefundGoodsUrl.router.searchList,
        component: () => import('./element/OrderRefundGoodsSearchList'),
        meta: {
            root: false,
            keepAlive: false,
            code:'OrderRefundGoodsSearchList',
            name: '退款商品管理'
        }
    },
    {
        path: OrderRefundGoodsUrl.router.add,
        component: () => import('./element/OrderRefundGoodsAdd'),
        meta: {
            code:'OrderRefundGoodsAdd',
            name: '退款商品添加'
        }
    },
    {
        path: OrderRefundGoodsUrl.router.update,
        component: () => import('./element/OrderRefundGoodsUpdate'),
        meta: {
            code:'OrderRefundGoodsUpdate',
            name: '退款商品修改'
        }
    },
]
export default OrderRefundGoodsRoute