const OrderRefundGoodsTable = [

    {
        prop: 'orderNo',
        label: '订单号'
    },
    {
        prop: 'orderRefundNo',
        label: '退款单号'
    },
    {
        prop: 'orderGoods.goodsName',
        label: '订单商品名称'
    },
]
export default OrderRefundGoodsTable