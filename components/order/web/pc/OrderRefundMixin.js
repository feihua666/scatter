import OrderRefundForm from './OrderRefundForm.js'
import OrderRefundTable from './OrderRefundTable.js'
import OrderRefundUrl from './OrderRefundUrl.js'
const OrderRefundMixin = {
    computed: {
        OrderRefundFormOptions() {
            return this.$stDynamicFormTools.options(OrderRefundForm,this.$options.name)
        },
        OrderRefundTableOptions() {
            return OrderRefundTable
        },
        OrderRefundUrl(){
            return OrderRefundUrl
        }
    },
}
export default OrderRefundMixin