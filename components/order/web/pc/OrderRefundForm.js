import OrderRefundUrl from './OrderRefundUrl.js'
const OrderRefundForm = [

    {
        OrderRefundSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderNo',
        },
        element:{
            label: '订单号',
            required: true,
        }
    },
    {
        OrderRefundSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'refundNo',
        },
        element:{
            label: '退款单号',
            required: true,
        }
    },

    {
        OrderRefundSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'statusDictId',
        },
        element:{
            label: '退款状态',
            type: 'selectDict',
            options: {
                groupCode: 'order_status'
            },
            required: true,
        }
    },


]
export default OrderRefundForm