import ErrorLogForm from './ErrorLogForm.js'
import ErrorLogTable from './ErrorLogTable.js'
import ErrorLogUrl from './ErrorLogUrl.js'
const ErrorLogMixin = {
    computed: {
        ErrorLogFormOptions() {
            return this.$stDynamicFormTools.options(ErrorLogForm,this.$options.name)
        },
        ErrorLogTableOptions() {
            return ErrorLogTable
        },
        ErrorLogUrl(){
            return ErrorLogUrl
        }
    },
}
export default ErrorLogMixin