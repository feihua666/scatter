const ErrorLogTable = [
    {
        type: 'expand'
    },
    {
        prop: 'name',
        label: '操作名称'
    },
    {
        prop: 'userId',
        label: '用户ID'
    },
    {
        prop: 'userNickname',
        label: '用户昵称'
    },
    {
        prop: 'requestId',
        label: '请求id'
    },
    {
        prop: 'requestUrl',
        label: '请求的url',
        showOverflowTooltip: true
    },
    {
        prop: 'requestMethod',
        label: '请求方法'
    },
    {
        prop: 'requestHeaders',
        label: '请求头信息',
        showOverflowTooltip: true
    },
    {
        prop: 'requestParmas',
        label: '请求参数',
        showInDetail: true
    },
    {
        prop: 'requestBody',
        label: '请求内容',
        showInDetail: true
    },
    {
        prop: 'requestIp',
        label: '请求ip'
    },
    {
        prop: 'responseStatus',
        label: '响应的状态码'
    },
    {
        prop: 'responseHeaders',
        label: '响应头信息',
        showInDetail: true
    },
    {
        prop: 'responseBody',
        label: '响应内容',
        showInDetail: true
    },
    {
        prop: 'exceptionStackTrace',
        label: '异常栈内容',
        showInDetail: true
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default ErrorLogTable