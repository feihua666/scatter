const basePath = '' + '/error-log'
const ErrorLogUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ErrorLogSearchList',
        add: '/ErrorLogAdd',
    }
}
export default ErrorLogUrl