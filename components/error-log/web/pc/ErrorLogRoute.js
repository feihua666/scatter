import ErrorLogUrl from './ErrorLogUrl.js'
const ErrorLogRoute = [
    {
        path: ErrorLogUrl.router.searchList,
        component: () => import('./element/ErrorLogSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ErrorLogSearchList',
            name: '异常日志管理'
        }
    },
]
export default ErrorLogRoute