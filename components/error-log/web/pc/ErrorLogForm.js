import ErrorLogUrl from './ErrorLogUrl.js'
const ErrorLogForm = [
    {
        field: {
            name: 'startAt',
        },
        element:{
            label: '开始时间',
            type: 'datetime'
        }
    },
    {
        field: {
            name: 'endAt',
        },
        element:{
            label: '结束时间',
            type: 'datetime'
        }
    },
    {
        field: {
            name: 'name',
        },
        element:{
            label: '操作名称',
        }
    },
    {
        field: {
            name: 'userId',
        },
        element:{
            label: '用户ID',
        }
    },
    {
        field: {
            name: 'userNickname',
        },
        element:{
            label: '用户昵称',
        }
    },
    {
        field: {
            name: 'requestId',
        },
        element:{
            label: '请求id',
        }
    },
    {
        field: {
            name: 'requestUrl',
        },
        element:{
            label: '请求的url',
        }
    },
    {
        field: {
            name: 'requestMethod',
        },
        element:{
            label: '请求方法',
        }
    },
    {
        field: {
            name: 'requestHeaders',
        },
        element:{
            label: '请求头信息',
        }
    },
    {
        field: {
            name: 'requestParmas',
        },
        element:{
            label: '请求参数',
        }
    },
    {
        field: {
            name: 'requestBody',
        },
        element:{
            label: '请求内容',
        }
    },
    {
        field: {
            name: 'requestIp',
        },
        element:{
            label: '请求ip',
        }
    },
    {
        field: {
            name: 'responseStatus',
        },
        element:{
            label: '响应的状态码',
        }
    },
    {
        field: {
            name: 'responseHeaders',
        },
        element:{
            label: '响应头信息',
        }
    },
    {
        field: {
            name: 'responseBody',
        },
        element:{
            label: '响应内容',
        }
    },
    {
        field: {
            name: 'exceptionStackTrace',
        },
        element:{
            label: '异常栈内容'
        }
    },
]
export default ErrorLogForm