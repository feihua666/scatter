import ErrorLogUrl from '../../pc/ErrorLogUrl.js'
export default {
    methods: {
        // data = {name: '必填',userId: '',userNickname: '',requestUrl: '',remark: ''}
        logError(data){
            let http = this.axios
            if(!http){
                // 兼容两种http，目前主要是uni-app使用
                http = this.$http
                http.post(ErrorLogUrl.add,data).catch(e =>{
                    console.error(e)
                })
            }
        }
    }
}