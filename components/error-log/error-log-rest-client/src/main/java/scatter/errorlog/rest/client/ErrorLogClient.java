package scatter.errorlog.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 异常日志表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@Component
@FeignClient(value = "ErrorLog-client")
public interface ErrorLogClient {

}
