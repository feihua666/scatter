package scatter.errorlog.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.errorlog.rest.ErrorLogConfiguration;
import scatter.errorlog.pojo.po.ErrorLog;
import scatter.errorlog.rest.service.IErrorLogService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 异常日志表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@RestController
@RequestMapping(ErrorLogConfiguration.CONTROLLER_BASE_PATH + "/inner/error-log")
public class ErrorLogInnerController extends BaseInnerController<ErrorLog> {
 @Autowired
 private IErrorLogService errorLogService;

 public IErrorLogService getService(){
     return errorLogService;
 }
}
