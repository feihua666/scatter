package scatter.errorlog.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.errorlog.rest.test.ErrorLogSuperTest;
/**
* <p>
* 异常日志 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-01-29
*/
@SpringBootTest
public class ErrorLogInnerControllerTest extends ErrorLogSuperTest{
    @Test
    void contextLoads() {
    }
}