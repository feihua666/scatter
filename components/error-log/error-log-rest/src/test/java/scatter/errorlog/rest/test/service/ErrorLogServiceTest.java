package scatter.errorlog.rest.test.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.errorlog.pojo.po.ErrorLog;
import scatter.errorlog.rest.service.IErrorLogService;
import scatter.errorlog.rest.test.ErrorLogSuperTest;

import java.util.List;
/**
* <p>
* 异常日志 服务测试类
* </p>
*
* @author yw
* @since 2021-01-29
*/
@SpringBootTest
public class ErrorLogServiceTest extends ErrorLogSuperTest{

    @Autowired
    private IErrorLogService errorLogService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ErrorLog> pos = errorLogService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}