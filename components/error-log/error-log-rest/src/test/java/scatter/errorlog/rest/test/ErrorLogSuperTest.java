package scatter.errorlog.rest.test;

import cn.hutool.core.util.ReflectUtil;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.pojo.po.BaseTreePo;
import scatter.errorlog.pojo.form.ErrorLogAddForm;
import scatter.errorlog.pojo.form.ErrorLogPageQueryForm;
import scatter.errorlog.pojo.po.ErrorLog;
import scatter.errorlog.rest.service.IErrorLogService;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
/**
* <p>
* 异常日志 测试类基类
* </p>
*
* @author yw
* @since 2021-01-29
*/
public class ErrorLogSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IErrorLogService errorLogService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return errorLogService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return errorLogService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ErrorLog mockPo() {
        return JMockData.mock(ErrorLog.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ErrorLogAddForm mockAddForm() {
        return JMockData.mock(ErrorLogAddForm.class, mockConfig);
    }


    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ErrorLogPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ErrorLogPageQueryForm.class, mockConfig);
    }
}