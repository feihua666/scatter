package scatter.errorlog.rest.mapper;

import scatter.errorlog.pojo.po.ErrorLog;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 异常日志表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
public interface ErrorLogMapper extends IBaseMapper<ErrorLog> {

}
