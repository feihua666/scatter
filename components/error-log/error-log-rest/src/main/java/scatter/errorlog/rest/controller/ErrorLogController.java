package scatter.errorlog.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddQueryFormController;
import scatter.errorlog.pojo.form.ErrorLogAddForm;
import scatter.errorlog.pojo.form.ErrorLogPageQueryForm;
import scatter.errorlog.pojo.po.ErrorLog;
import scatter.errorlog.pojo.vo.ErrorLogVo;
import scatter.errorlog.rest.ErrorLogConfiguration;

import javax.validation.Valid;
/**
 * <p>
 * 异常日志表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@Api(tags = "异常日志相关接口")
@RestController
@RequestMapping(ErrorLogConfiguration.CONTROLLER_BASE_PATH + "/error-log")
public class ErrorLogController extends BaseAddQueryFormController<ErrorLog, ErrorLogVo, ErrorLogAddForm, ErrorLogPageQueryForm> {


     @Override
	 @ApiOperation("添加异常日志")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ErrorLogVo add(@RequestBody @Valid ErrorLogAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询异常日志")
     @PreAuthorize("hasAuthority('ErrorLog:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ErrorLogVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除异常日志")
     @PreAuthorize("hasAuthority('ErrorLog:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

    @Override
	@ApiOperation("分页查询异常日志")
    @PreAuthorize("hasAuthority('ErrorLog:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ErrorLogVo> getPage(ErrorLogPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
