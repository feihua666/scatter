package scatter.errorlog.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.errorlog.pojo.form.ErrorLogAddForm;
import scatter.errorlog.pojo.form.ErrorLogPageQueryForm;
import scatter.errorlog.pojo.po.ErrorLog;
import scatter.errorlog.pojo.vo.ErrorLogVo;

/**
 * <p>
 * 异常日志 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ErrorLogMapStruct extends IBaseVoMapStruct<ErrorLog, ErrorLogVo>,
                                  IBaseAddFormMapStruct<ErrorLog,ErrorLogAddForm>,
                                  IBaseQueryFormMapStruct<ErrorLog,ErrorLogPageQueryForm>{
    ErrorLogMapStruct INSTANCE = Mappers.getMapper( ErrorLogMapStruct.class );

}
