package scatter.errorlog.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.errorlog.pojo.po.ErrorLog;
/**
 * <p>
 * 异常日志表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
public interface IErrorLogService extends IBaseService<ErrorLog> {


}
