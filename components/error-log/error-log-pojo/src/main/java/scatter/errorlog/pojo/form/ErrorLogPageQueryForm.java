package scatter.errorlog.pojo.form;

import scatter.common.pojo.form.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * <p>
 * 异常日志分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@Setter
@Getter
@ApiModel(value="异常日志分页表单对象")
@OrderBy(value = "createAt",asc = false)
public class ErrorLogPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "操作名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @Like
    @ApiModelProperty(value = "用户昵称，模糊查询")
    private String userNickname;

    @ApiModelProperty(value = "请求id")
    private String requestId;

    @Like
    @ApiModelProperty(value = "请求的url，模糊查询")
    private String requestUrl;

    @Like
    @ApiModelProperty(value = "请求方法，模糊查询")
    private String requestMethod;

    @Like
    @ApiModelProperty(value = "请求头信息，模糊查询")
    private String requestHeaders;

    @Like
    @ApiModelProperty(value = "请求参数，模糊查询")
    private String requestParmas;

    @Like
    @ApiModelProperty(value = "请求内容，模糊查询")
    private String requestBody;

    @ApiModelProperty(value = "请求ip")
    private String requestIp;

    @Like
    @ApiModelProperty(value = "响应的状态码，模糊查询")
    private String responseStatus;

    @Like
    @ApiModelProperty(value = "响应头信息，模糊查询")
    private String responseHeaders;

    @Like
    @ApiModelProperty(value = "响应内容，模糊查询")
    private String responseBody;

    @ApiModelProperty(value = "异常栈内容")
    private String exceptionStackTrace;

    @Lt(value = "createAt",eq = true)
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startAt;

    @Gt(value = "createAt",eq = true)
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endAt;

}
