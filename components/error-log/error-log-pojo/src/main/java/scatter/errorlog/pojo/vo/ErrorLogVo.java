package scatter.errorlog.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 异常日志响应对象
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@Setter
@Getter
@ApiModel(value="异常日志响应对象")
public class ErrorLogVo extends BaseIdVo {

    @ApiModelProperty(value = "操作名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "用户昵称，模糊查询")
    private String userNickname;

    @ApiModelProperty(value = "请求id")
    private String requestId;

    @ApiModelProperty(value = "请求的url，模糊查询")
    private String requestUrl;

    @ApiModelProperty(value = "请求方法，模糊查询")
    private String requestMethod;

    @ApiModelProperty(value = "请求头信息，模糊查询")
    private String requestHeaders;

    @ApiModelProperty(value = "请求参数，模糊查询")
    private String requestParmas;

    @ApiModelProperty(value = "请求内容，模糊查询")
    private String requestBody;

    @ApiModelProperty(value = "请求ip")
    private String requestIp;

    @ApiModelProperty(value = "响应的状态码，模糊查询")
    private String responseStatus;

    @ApiModelProperty(value = "响应头信息，模糊查询")
    private String responseHeaders;

    @ApiModelProperty(value = "响应内容，模糊查询")
    private String responseBody;

    @ApiModelProperty(value = "异常栈内容")
    private String exceptionStackTrace;

    @ApiModelProperty(value = "备注")
    private String remark;

}
