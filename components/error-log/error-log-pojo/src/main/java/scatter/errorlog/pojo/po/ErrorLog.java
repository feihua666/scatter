package scatter.errorlog.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 异常日志表
 * </p>
 *
 * @author yw
 * @since 2021-01-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_error_log")
@ApiModel(value="ErrorLog对象", description="异常日志表")
public class ErrorLog extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ERRORLOG_BY_ID = "trans_errorlog_by_id_scatter.errorlog.pojo.po";

    @ApiModelProperty(value = "操作名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "用户昵称，模糊查询")
    private String userNickname;

    @ApiModelProperty(value = "请求id")
    private String requestId;

    @ApiModelProperty(value = "请求的url，模糊查询")
    private String requestUrl;

    @ApiModelProperty(value = "请求方法，模糊查询")
    private String requestMethod;

    @ApiModelProperty(value = "请求头信息，模糊查询")
    private String requestHeaders;

    @ApiModelProperty(value = "请求参数，模糊查询")
    private String requestParmas;

    @ApiModelProperty(value = "请求内容，模糊查询")
    private String requestBody;

    @ApiModelProperty(value = "请求ip")
    private String requestIp;

    @ApiModelProperty(value = "响应的状态码，模糊查询")
    private String responseStatus;

    @ApiModelProperty(value = "响应头信息，模糊查询")
    private String responseHeaders;

    @ApiModelProperty(value = "响应内容，模糊查询")
    private String responseBody;

    @ApiModelProperty(value = "异常栈内容")
    private String exceptionStackTrace;

    @ApiModelProperty(value = "备注")
    private String remark;


}
