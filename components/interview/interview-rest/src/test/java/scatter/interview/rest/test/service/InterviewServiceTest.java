package scatter.interview.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.interview.pojo.po.Interview;
import scatter.interview.pojo.form.InterviewAddForm;
import scatter.interview.pojo.form.InterviewUpdateForm;
import scatter.interview.pojo.form.InterviewPageQueryForm;
import scatter.interview.rest.test.InterviewSuperTest;
import scatter.interview.rest.service.IInterviewService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 面试 服务测试类
* </p>
*
* @author yw
* @since 2020-12-02
*/
@SpringBootTest
public class InterviewServiceTest extends InterviewSuperTest{

    @Autowired
    private IInterviewService interviewService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Interview> pos = interviewService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}