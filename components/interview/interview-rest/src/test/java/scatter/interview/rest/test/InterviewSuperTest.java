package scatter.interview.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.interview.pojo.po.Interview;
import scatter.interview.pojo.form.InterviewAddForm;
import scatter.interview.pojo.form.InterviewUpdateForm;
import scatter.interview.pojo.form.InterviewPageQueryForm;
import scatter.interview.rest.service.IInterviewService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 面试 测试类基类
* </p>
*
* @author yw
* @since 2020-12-02
*/
public class InterviewSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IInterviewService interviewService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return interviewService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return interviewService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Interview mockPo() {
        return JMockData.mock(Interview.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public InterviewAddForm mockAddForm() {
        return JMockData.mock(InterviewAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public InterviewUpdateForm mockUpdateForm() {
        return JMockData.mock(InterviewUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public InterviewPageQueryForm mockPageQueryForm() {
        return JMockData.mock(InterviewPageQueryForm.class, mockConfig);
    }
}