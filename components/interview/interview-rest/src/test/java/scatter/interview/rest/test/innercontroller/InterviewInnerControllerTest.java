package scatter.interview.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.interview.rest.test.InterviewSuperTest;
/**
* <p>
* 面试 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-02
*/
@SpringBootTest
public class InterviewInnerControllerTest extends InterviewSuperTest{
    @Test
    void contextLoads() {
    }
}