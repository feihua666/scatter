package scatter.interview.rest.interview.i2_sort;

/**
 * 定义一个排序接口
 * Created by yangwei
 * Created at 2021/4/9 11:05
 */
public interface ISort<T> {

    /**
     * 排序
     * @param array
     * @return
     */
    public T sort(T array);

    /**
     * 交换
     * @param array
     * @param i
     * @param j
     */
    default public void swap(T array, int i, int j){}
}
