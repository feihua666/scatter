package scatter.interview.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.interview.pojo.po.Interview;
import scatter.interview.rest.service.IInterviewService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 面试翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
@Component
public class InterviewTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IInterviewService interviewService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Interview.TRANS_INTERVIEW_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Interview.TRANS_INTERVIEW_BY_ID)) {
            Interview byId = interviewService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Interview.TRANS_INTERVIEW_BY_ID)) {
            return interviewService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
