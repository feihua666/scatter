package scatter.interview.rest.interview.syntax;

/**
 * ++x 语法
 * Created by yangwei
 * Created at 2021/4/9 14:06
 */
public class PlusPlus {

    public static void main(String[] args) {
        int a = 10 >> 1; // a =5

        int b = a++; //a=6;b=5
        int c = ++a; // c=7;a=7
        int d = b * a++; // b=5,a=8,d=35

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);

        System.out.println(~6);
    }
}
