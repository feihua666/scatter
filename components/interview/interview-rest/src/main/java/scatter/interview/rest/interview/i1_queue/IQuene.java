package scatter.interview.rest.interview.i1_queue;

/**
 * Created by yangwei
 * Created at 2020/12/2 16:14
 */
public interface IQuene<T> {

    /**
     * 入队
     * @param t
     * @return
     */
    boolean push(T t) ;

    /**
     * 出队
     * @return
     */
    T pop();
}
