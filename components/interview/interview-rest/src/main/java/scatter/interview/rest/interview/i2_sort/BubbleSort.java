package scatter.interview.rest.interview.i2_sort;

/**
 * 冒泡排序
 * Created by yangwei
 * Created at 2021/4/9 13:47
 */
public class BubbleSort implements ICompareSort<int[]>{

    /**
     * 冒泡排序
     *
     * @param array
     * @return
     */
    private int[] bubbleSort(int[] array) {
        if (array.length == 0){
            return array;}
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j + 1] < array[j]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
    @Override
    public int[] sort(int[] array) {
        bubbleSort(array);
        return array;
    }

    public static void main(String[] args) {
        int[] arr = {10, 7, 2, 4, 7, 62, 3, 4, 2, 1,4, 2, 1,4, 2, 1, 8, 9, 19};
        new BubbleSort().sort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
