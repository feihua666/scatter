package scatter.interview.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.interview.pojo.form.InterviewAddForm;
import scatter.interview.pojo.form.InterviewPageQueryForm;
import scatter.interview.pojo.form.InterviewUpdateForm;
import scatter.interview.pojo.po.Interview;
import scatter.interview.rest.mapper.InterviewMapper;
import scatter.interview.rest.service.IInterviewService;
/**
 * <p>
 * 面试表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
@Service
@Transactional
public class InterviewServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<InterviewMapper, Interview, InterviewAddForm, InterviewUpdateForm, InterviewPageQueryForm> implements IInterviewService {
    @Override
    public void preAdd(InterviewAddForm addForm,Interview po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),Interview::getCode,false);
        }

    }

    @Override
    public void preUpdate(InterviewUpdateForm updateForm,Interview po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            Interview byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),Interview::getCode,false);
            }
        }

    }
}
