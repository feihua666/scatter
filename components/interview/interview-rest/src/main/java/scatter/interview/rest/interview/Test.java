package scatter.interview.rest.interview;

import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONUtil;
import lombok.Getter;
import lombok.Setter;
import scatter.common.LoginUser;

import java.time.LocalDateTime;

/**
 * Created by yangwei
 * Created at 2020/12/4 17:33
 */
public class Test {
    public static void main(String[] args) {

        String s3 = "china";

        String ss1 = new String("china");
        String s = ss1.intern();
        System.out.println(s3 == s);
        System.out.println(s3 == ss1);

    }

    @Setter
    @Getter
    public static class TimeTest{
        LocalDateTime ttt;
    }
}
