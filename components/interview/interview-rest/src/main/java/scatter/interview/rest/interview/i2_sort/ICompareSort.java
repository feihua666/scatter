package scatter.interview.rest.interview.i2_sort;

/**
 * 比较排序
 * Created by yangwei
 * Created at 2021/4/9 11:21
 */
public interface ICompareSort<T> extends ISort<T>{

    /**
     * 比较
     * @param array
     * @param i
     * @param j
     * @return 1= i>j,0= i=j,-1= i<j
     */
    default public int compare(T array, int i, int j){
        return 0;
    }
}
