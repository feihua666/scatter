package scatter.interview.rest.interview.i4_socket;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by yangwei
 * Created at 2020/12/4 10:25
 */
public class SocketClientSimple {
    public static final int PORT = 8088;
    public static void main( String[] args ) throws Exception
    {
        InetAddress addr = InetAddress.getByName( "127.0.0.1" );
        Socket socket = new Socket();
        try
        {
            socket.connect( new InetSocketAddress( addr, PORT ), 30000 );
            socket.setSendBufferSize(100);

            BufferedWriter out = new BufferedWriter( new OutputStreamWriter(socket.getOutputStream() ) );
            int i = 0;

            while( true )
            {
                System.out.println( "client sent --- hello *** " + i++ );
                // 注意换行符不要丢掉
                out.write( "client sent --- hello *** " + i  + "\r\n");
                out.flush();

                Thread.sleep( 1000 );
            }
        }
        finally
        {
            socket.close();
        }
    }
}
