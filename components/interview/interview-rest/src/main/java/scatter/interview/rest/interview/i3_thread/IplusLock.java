package scatter.interview.rest.interview.i3_thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by yangwei
 * Created at 2020/12/3 15:11
 */
public class IplusLock {
    public static volatile int i=0;
    static Lock lock=new ReentrantLock();


    public static void add() {
        lock.lock();
        try {
            Thread.sleep(1);
            i=i+1;
            action();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public static void action(){
        System.out.println("==>"+Thread.currentThread().getName()+":"+i);
    }
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(IplusLock::add,"t1");
        Thread t2= new Thread(IplusLock::add,"t2");
        t1.start();
        t2.start();
    }
}
