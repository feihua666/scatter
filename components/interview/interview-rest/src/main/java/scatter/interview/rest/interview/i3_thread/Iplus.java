package scatter.interview.rest.interview.i3_thread;

/**
 * 关于两个线程对变量i进行加1操作，结果如何？为什么？如何解决？
 *
 * 缺点：加锁粒度大，性能低下，分布式环境，多JVM条件，synchronized失效，synchronized 只是本地锁，锁的也只是当前jvm下的对象，在分布式场景下，要用分布式锁
 * Created by yangwei
 * Created at 2020/12/3 15:05
 */
public class Iplus {
    public static volatile int i=0;
    public static void add(){
        //synchronized (Iplus.class){
            i=i+1;
            action();
        //}
    }
    public static void action(){
        System.out.println("==>"+Thread.currentThread().getName()+":"+i);
    }
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(Iplus::add,"t1");
        Thread t2= new Thread(Iplus::add,"t2");
        t1.start();
        t2.start();

    }
}
