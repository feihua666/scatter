package scatter.interview.rest.mapper;

import scatter.interview.pojo.po.Interview;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 面试表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
public interface InterviewMapper extends IBaseMapper<Interview> {

}
