package scatter.interview.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.interview.rest.InterviewConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.interview.pojo.po.Interview;
import scatter.interview.pojo.vo.InterviewVo;
import scatter.interview.pojo.form.InterviewAddForm;
import scatter.interview.pojo.form.InterviewUpdateForm;
import scatter.interview.pojo.form.InterviewPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 面试表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
@RestController
@RequestMapping(InterviewConfiguration.CONTROLLER_BASE_PATH + "/interview")
@Api(tags = "面试")
public class InterviewController extends BaseAddUpdateQueryFormController<Interview, InterviewVo, InterviewAddForm, InterviewUpdateForm, InterviewPageQueryForm> {


     @Override
	 @ApiOperation("添加面试")
     @PreAuthorize("hasAuthority('Interview:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public InterviewVo add(@RequestBody @Valid InterviewAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询面试")
     @PreAuthorize("hasAuthority('Interview:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public InterviewVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除面试")
     @PreAuthorize("hasAuthority('Interview:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新面试")
     @PreAuthorize("hasAuthority('Interview:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public InterviewVo update(@RequestBody @Valid InterviewUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询面试")
    @PreAuthorize("hasAuthority('Interview:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<InterviewVo> getList(InterviewPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询面试")
    @PreAuthorize("hasAuthority('Interview:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<InterviewVo> getPage(InterviewPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
