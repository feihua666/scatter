package scatter.interview.rest.interview.i2_sort;

/**
 * 选择排序
 * Created by yangwei
 * Created at 2021/4/9 13:50
 */
public class SelectionSort implements ICompareSort<int[]>{


    /**
     * 选择排序
     * @param array
     * @return
     */
    public static int[] selectionSort(int[] array) {
        if (array.length == 0){
            return array;
        }
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[minIndex]){ //找到最小的数
                    minIndex = j; //将最小数的索引保存
                }
            }
            int temp = array[minIndex];
            array[minIndex] = array[i];
            array[i] = temp;
        }
        return array;
    }
    @Override
    public int[] sort(int[] array) {
        selectionSort(array);
        return array;
    }


    public static void main(String[] args) {
        int[] arr = {10, 7, 2, 4, 7, 62, 3, 4, 2, 1,4, 2, 1,4, 2, 1, 8, 9, 19};
        new SelectionSort().sort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
