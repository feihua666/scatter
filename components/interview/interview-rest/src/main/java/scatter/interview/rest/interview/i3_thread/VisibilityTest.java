package scatter.interview.rest.interview.i3_thread;
/**
 * @author yangwei
 * @since 2021-04-19 21:13
 */
public class VisibilityTest {

    private boolean flag = true;
    public void updateFalse(){
        flag = false;
        System.out.println("u");
    }
    public void whileTest(){
        int i = 0;
        while (flag){
            i++;
            //continue; // 情况 A
            //System.out.println(); // 情况B
            //Thread.sleep(1); // 情况 C
             wwait(1000000);// 情况 D
            //wwait(1000);// 情况 D
        }
        System.out.println("wt");
    }
    public void wwait(long w){
        long start = System.nanoTime();
        long end;
        do{
            end = System.nanoTime();
        }while ((start+w) >= end);
    }

    public static void main(String[] args) {
        VisibilityTest visibilityTest = new VisibilityTest();

        Thread t1 = new Thread(()->{
            visibilityTest.whileTest();
        });
        Thread t2 = new Thread(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            visibilityTest.updateFalse();
        });
        t1.start();
        t2.start();
    }
}
