package scatter.interview.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.interview.pojo.po.Interview;
/**
 * <p>
 * 面试表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
public interface IInterviewService extends IBaseService<Interview> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Interview getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Interview>lambdaQuery().eq(Interview::getCode, code));
    }

}
