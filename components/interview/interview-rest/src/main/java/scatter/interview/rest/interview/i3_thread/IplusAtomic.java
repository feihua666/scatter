package scatter.interview.rest.interview.i3_thread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 原子操作
 * Created by yangwei
 * Created at 2020/12/3 15:09
 */
public class IplusAtomic {
    private static AtomicInteger num = new AtomicInteger(0);
    public static void add(){
        int i = num.getAndIncrement();
        action(i);
    }
    public static void action(int i){
        System.out.println("由"+i+"==>"+Thread.currentThread().getName()+":"+num);
    }
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(IplusAtomic::add,"t1");
        Thread t2= new Thread(IplusAtomic::add,"t2");
        t1.start();
        t2.start();
    }
}
