package scatter.interview.rest.interview.i1_queue;

/**
 * 队列抽象类
 * Created by yangwei
 * Created at 2020/12/2 16:16
 */
public abstract class AbstractQuene<R,F,T> implements  IQuene<T>{

    protected int size;
    /**
     * 队尾
     */
    protected R rear;
    /**
     * 队首
     */
    protected F front;

}
