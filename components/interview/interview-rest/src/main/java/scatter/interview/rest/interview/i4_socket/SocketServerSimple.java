package scatter.interview.rest.interview.i4_socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by yangwei
 * Created at 2020/12/4 10:27
 */
public class SocketServerSimple {
    public static final int PORT = 8088;
    public static final int BACKLOG = 2;
    public static void main( String[] args ) throws IOException, InterruptedException {
        InetAddress addr = InetAddress.getByName( "127.0.0.1" );
        ServerSocket server = new ServerSocket( PORT );
        System.out.println("started: " + server);
        try
        {
            Socket socket = server.accept();
            try
            {
                System.out.println(
                        "Connection accepted: "+ socket);
                BufferedReader in =
                        new BufferedReader(
                                new InputStreamReader(
                                        socket.getInputStream()));
                String info = null;

                while( true)
                {
                    System.out.println("server ** " +  in.readLine() );
                    if ((info = in.readLine()) != null) {

                    }
                    Thread.sleep( 1000 );
                }
            }
            finally
            {
                socket.close();
            }
        }
        finally
        {
            server.close();
        }
    }
}
