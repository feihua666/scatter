package scatter.interview.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.interview.pojo.form.InterviewAddForm;
import scatter.interview.pojo.form.InterviewPageQueryForm;
import scatter.interview.pojo.form.InterviewUpdateForm;
import scatter.interview.pojo.po.Interview;
import scatter.interview.pojo.vo.InterviewVo;

/**
 * <p>
 * 面试 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface InterviewMapStruct extends IBaseVoMapStruct<Interview, InterviewVo>,
        IBaseAddFormMapStruct<Interview,InterviewAddForm>,
        IBaseUpdateFormMapStruct<Interview,InterviewUpdateForm>,
        IBaseQueryFormMapStruct<Interview,InterviewPageQueryForm> {
    InterviewMapStruct INSTANCE = Mappers.getMapper( InterviewMapStruct.class );

}
