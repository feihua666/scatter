import InterviewUrl from './InterviewUrl.js'

const InterviewRoute = [
    {
        path: InterviewUrl.router.searchList,
        component: () => import('./element/InterviewSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'InterviewSearchList',
            name: '面试管理'
        }
    },
    {
        path: InterviewUrl.router.add,
        component: () => import('./element/InterviewAdd'),
        meta: {
            code:'InterviewAdd',
            name: '面试添加'
        }
    },
    {
        path: InterviewUrl.router.update,
        component: () => import('./element/InterviewUpdate'),
        meta: {
            code:'InterviewUpdate',
            name: '面试修改'
        }
    },
]
export default InterviewRoute