import InterviewForm from './InterviewForm.js'
import InterviewTable from './InterviewTable.js'
import InterviewUrl from './InterviewUrl.js'

const InterviewMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(InterviewForm,this.$options.name)
        },
        computedTableOptions() {
            return InterviewTable
        },
        computedUrl(){
            return InterviewUrl
        }
    },
}
export default InterviewMixin