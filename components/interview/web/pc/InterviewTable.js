const InterviewTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'remark',
        label: '描述'
    },
    {
        prop: 'seq',
        label: '排序'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default InterviewTable