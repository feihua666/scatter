import InterviewUrl from './InterviewUrl.js'

const InterviewForm = [
    {
        field: {
            name: 'code'
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        field: {
            name: 'name'
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
    {
        field: {
            name: 'seq'
        },
        element:{
            label: '排序',
            required: true,
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: InterviewUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },
]
export default InterviewForm