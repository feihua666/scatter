const basePath = '' + '/interview'
const InterviewUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/InterviewSearchList',
        add: '/InterviewAdd',
        update: '/InterviewUpdate',
    }
}
export default InterviewUrl