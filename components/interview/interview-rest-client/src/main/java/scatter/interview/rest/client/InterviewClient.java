package scatter.interview.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 面试表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
@Component
@FeignClient(value = "Interview-client")
public interface InterviewClient {

}
