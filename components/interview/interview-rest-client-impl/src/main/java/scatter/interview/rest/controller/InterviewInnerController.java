package scatter.interview.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.interview.pojo.po.Interview;
import scatter.interview.rest.service.IInterviewService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 面试表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-02
 */
@RestController
@RequestMapping("/inner/interview")
public class InterviewInnerController extends BaseInnerController<Interview> {
 @Autowired
 private IInterviewService interviewService;

 public IInterviewService getService(){
     return interviewService;
 }
}
