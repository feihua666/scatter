package scatter.area.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 区域添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Setter
@Getter
@ApiModel(value="区域添加表单对象")
public class AreaAddForm extends BaseAddForm {

    @ApiModelProperty(value = "编码，唯一,模糊查询")
    private String code;

    @NotEmpty(message="区域名称不能为空")
    @ApiModelProperty(value = "区域名称,模糊查询",required = true)
    private String name;

    @ApiModelProperty(value = "第一个字的首字母")
    private String spellFirst;

    @ApiModelProperty(value = "每个字的首字母")
    private String spellSimple;

    @ApiModelProperty(value = "全拼")
    private String spell;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "备注")
    private String remark;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序,默认按该字段升序排序",required = true)
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
