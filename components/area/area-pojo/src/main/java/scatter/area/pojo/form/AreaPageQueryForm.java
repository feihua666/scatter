package scatter.area.pojo.form;

import scatter.common.pojo.form.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 区域分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Setter
@Getter
@ApiModel(value="区域分页表单对象")
public class AreaPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "编码，唯一,模糊查询")
    private String code;

    @Like
    @ApiModelProperty(value = "区域名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "第一个字的首字母")
    private String spellFirst;

    @ApiModelProperty(value = "每个字的首字母")
    private String spellSimple;

    @ApiModelProperty(value = "全拼")
    private String spell;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "备注")
    private String remark;

    @Gt("level")
    private Integer levelMin;
    @Lt("level")
    private Integer levelMax;

    @OrderBy
    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
