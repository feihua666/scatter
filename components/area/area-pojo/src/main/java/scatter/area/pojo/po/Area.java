package scatter.area.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 区域表
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_area")
@ApiModel(value="Area对象", description="区域表")
public class Area extends BaseTreePo {

    private static final long serialVersionUID = 1L;

    public static final String TRANS_AREA_BY_ID = "trans_area_by_id_scatter.area.pojo.po";
    public static final String TRANS_AREA_PARENT_BY_ID = "trans_area_parent_by_id_scatter.area.pojo.po";
    //所有父级加自己
    public static final String TRANS_AREA_AND_PARENTS_BY_ID = "trans_area_and_parents_by_id_scatter.area.pojo.po";

    /**
     * 区域类型字典组编码
     */
    public enum TypeDictGroup implements IDictGroup {
        /**
         * 区域类型
         */
        area_type;

        @Override
        public String groupCode() {
            return this.name();
        }}

    /**
     * 区域类型字典项编码
     */
    public enum TypeDictItem implements IDictItem {
        /**
         * 国家
         */
        country,
        /**
         * 省
         */
        province,
        /**
         * 城市
         */
        city,
        /**
         * 县
         */
        county;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return TypeDictGroup.area_type.groupCode();
        }

    }

    @ApiModelProperty(value = "编码，唯一,模糊查询")
    private String code;

    @ApiModelProperty(value = "区域名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "区域名称,简称")
    private String nameSimple;

    @ApiModelProperty(value = "第一个字的首字母")
    private String spellFirst;

    @ApiModelProperty(value = "每个字的首字母")
    private String spellSimple;

    @ApiModelProperty(value = "全拼")
    private String spell;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;


}
