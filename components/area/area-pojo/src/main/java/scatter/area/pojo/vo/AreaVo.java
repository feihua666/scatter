package scatter.area.pojo.vo;

import scatter.area.pojo.po.Area;
import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 区域响应对象
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Setter
@Getter
@ApiModel(value="区域响应对象")
public class AreaVo extends BaseIdVo {

    @ApiModelProperty(value = "编码，唯一,模糊查询")
    private String code;

    @ApiModelProperty(value = "区域名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "第一个字的首字母")
    private String spellFirst;

    @ApiModelProperty(value = "每个字的首字母")
    private String spellSimple;

    @ApiModelProperty(value = "全拼")
    private String spell;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="name")
    @ApiModelProperty(value = "类型，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @TransBy(type = Area.TRANS_AREA_BY_ID,byFieldName = "parentId",mapValueField = "name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
