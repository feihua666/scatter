package scatter.area.pojo.dto;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.dto.BaseDto;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 14:05
 */
@Getter
@Setter
public class AreaIdResolveDto extends BaseDto {

	/**
	 * 区id
	 */
	private String districtId;
	/**
	 * 城市id
	 */
	private String cityId;
	/**
	 * 省id
	 */
	private String provinceId;


	/**
	 * 国家id
	 */
	private String countryId;
}
