package scatter.area.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.area.pojo.po.Area;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 区域响应对象
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Setter
@Getter
@ApiModel(value="区域响应对象")
public class AppAreaVo extends BaseIdVo {

    @ApiModelProperty(value = "编码，唯一,模糊查询")
    private String code;

    @ApiModelProperty(value = "区域名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
