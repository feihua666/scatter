import AreaForm from './AreaForm.js'
import AreaTable from './AreaTable.js'
import AreaUrl from './AreaUrl.js'

const AreaMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(AreaForm,this.$options.name)
        },
        computedTableOptions() {
            return AreaTable
        },
        computedUrl(){
            return AreaUrl
        }
    },
}
export default AreaMixin