import AreaUrl from './AreaUrl.js'

const AreaRoute = [
    {
        path: AreaUrl.router.searchList,
        component: () => import('./element/AreaSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'AreaSearchList',
            name: '区域管理'
        }
    },
    {
        path: AreaUrl.router.add,
        component: () => import('./element/AreaAdd'),
        meta: {
            code:'AreaAdd',
            name: '区域添加'
        }
    },
    {
        path: AreaUrl.router.update,
        component: () => import('./element/AreaUpdate'),
        meta: {
            code:'AreaUpdate',
            name: '区域修改'
        }
    },
]
export default AreaRoute