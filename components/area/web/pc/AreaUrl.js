const basePath = '' + '/area'
const AreaUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',

    router: {
        searchList:'/AreaSearchList',
        add:'/AreaAdd',
        update:'/AreaUpdate',
    }
}
export default AreaUrl