import AreaUrl from './AreaUrl.js'

const AreaForm = [
    {
        field: {
            name: 'code'
        },
        element:{
            label: '编码',
        }
    },
    {
        field: {
            name: 'name'
        },
        element:{
            label: '区域名称',
            required: true,
        },
        AreaSearchList: {
            element:{
                required: false,
            }
        },
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: AreaUrl.list,
                cacheKey: 'areaList',
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
                originProp: {
                    filterMethod(node, keyword) {
                        if (!keyword) return true;
                        return node.data.name.indexOf(keyword) !== -1
                            || node.data.spellFirst.indexOf(keyword) !== -1
                            || node.data.spellSimple.indexOf(keyword) !== -1
                            || node.data.spell.indexOf(keyword) !== -1
                            ;
                    }
                }
            },
            label: '父级',
        }
    },
    {
        field: {
            name: 'typeDictId'
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'area_type'
            },
            label: '类型',
            required: true,
        },
        AreaSearchList: {
            element:{
                required: false,
            }
        },
    },
    {

        field: {
            name: 'seq',
            value: 10
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        },
        AreaSearchList: false,
    },
    {
        AreaSearchList: false,
        field: {
            name: 'remark'
        },
        element:{
            label: '备注',
        }
    },

]
export default AreaForm