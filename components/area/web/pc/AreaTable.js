const AreaTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '区域名称'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
    {
        prop: 'seq',
        label: '排序'
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default AreaTable