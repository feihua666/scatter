# area 区域组件

区域组件提供省、市、区的区域数据支持，是一个基础数据组件  

::: tip
区域组件表类型为树
:::
## 区域组件数据来源
数据初始化代码位置：scatter.area.rest.test.AreaImportTest.importTest  
当前的数据版本： [国家统计局 http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/index.html](http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/index.html) 