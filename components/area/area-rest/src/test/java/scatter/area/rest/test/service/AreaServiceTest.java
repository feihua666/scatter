package scatter.area.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.area.pojo.po.Area;
import scatter.area.pojo.form.AreaAddForm;
import scatter.area.pojo.form.AreaUpdateForm;
import scatter.area.pojo.form.AreaPageQueryForm;
import scatter.area.rest.test.AreaSuperTest;
import scatter.area.rest.service.IAreaService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 区域 服务测试类
* </p>
*
* @author yw
* @since 2020-11-12
*/
@SpringBootTest
public class AreaServiceTest extends AreaSuperTest{

    @Autowired
    private IAreaService areaService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Area> pos = areaService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}