package scatter.area.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.area.pojo.po.Area;
import scatter.area.pojo.form.AreaAddForm;
import scatter.area.pojo.form.AreaUpdateForm;
import scatter.area.pojo.form.AreaPageQueryForm;
import scatter.area.rest.service.IAreaService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 区域 测试类基类
* </p>
*
* @author yw
* @since 2020-11-12
*/
public class AreaSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IAreaService areaService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return areaService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return areaService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Area mockPo() {
        return JMockData.mock(Area.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public AreaAddForm mockAddForm() {
        return JMockData.mock(AreaAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public AreaUpdateForm mockUpdateForm() {
        return JMockData.mock(AreaUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public AreaPageQueryForm mockPageQueryForm() {
        return JMockData.mock(AreaPageQueryForm.class, mockConfig);
    }
}