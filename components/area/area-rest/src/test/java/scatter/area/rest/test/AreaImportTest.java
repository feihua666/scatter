package scatter.area.rest.test;

import cn.hutool.json.JSONUtil;
import lombok.Getter;
import lombok.Setter;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.filters.CssSelectorNodeFilter;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.area.pojo.po.Area;
import scatter.area.rest.service.IAreaService;
import scatter.common.rest.tools.BaiduMapTool;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.tools.Pinyin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2020/11/19 17:48
 */
@SpringBootTest
public class AreaImportTest extends AreaSuperTest implements InterfaceTool {

    @Getter
    @Setter
    public static class AreaTree{
        private Area area;
        private StringJoiner parentNames  = new StringJoiner(" ");
        private List<AreaTree> children;
        private String childrenUrl;
    }

    @Autowired
    private IAreaService areaService;

    public static void main(String[] args) throws IOException, ParserException, BadHanyuPinyinOutputFormatCombination {
    }

    public void insert(AreaTree areaTree,String parentId){

        Map<String, Object> r = BaiduMapTool.getGeo(areaTree.getParentNames().toString() + " " + areaTree.getArea().getName(), "69a5a1e7260cd2cd298c33666e436530");

        if ("0".equals(r.get("status").toString())) {
            Map<String, Object> result = (Map<String, Object>) r.get("result");
            Map<String, Object> location = (Map<String, Object>) result.get("location");
            areaTree.getArea().setLongitude(location.get("lng").toString());
            areaTree.getArea().setLatitude(location.get("lat").toString());
        }

        System.out.println(areaTree.getParentNames().toString());
        areaService.insertChild(areaTree.getArea(),parentId);
        if (!isEmpty(areaTree.getChildren())) {
            for (AreaTree child : areaTree.getChildren()) {
                insert(child,areaTree.getArea().getId());
            }
        }
    }
    /**
     * 省
     * @return
     * @throws IOException
     * @throws ParserException
     * @throws BadHanyuPinyinOutputFormatCombination
     */
    public List<AreaTree> province() throws IOException, ParserException, BadHanyuPinyinOutputFormatCombination {

        List<AreaTree> result = new ArrayList<>();

        Parser parser = new Parser("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/index.html");
        parser.setEncoding("GBK");
        CssSelectorNodeFilter paraFilter =new CssSelectorNodeFilter(".provincetable .provincetr a");
        NodeList nodeList = parser.extractAllNodesThatMatch(paraFilter);
        for (int i = 0; i < nodeList.size(); i++) {
            Node node =  nodeList.elementAt(i);
            LinkTag linkTag = ((LinkTag) node);
            AreaTree areaTree = newAreaTree(newArea(node.toPlainTextString(),null, Area.TypeDictItem.province.itemValue(),i * 10));
            areaTree.setChildrenUrl(linkTag.getLink());
            city(areaTree);
            result.add(areaTree);
        }
        return result;
    }

    /**
     * 市
     * @param provinceTree
     * @return
     * @throws IOException
     * @throws ParserException
     * @throws BadHanyuPinyinOutputFormatCombination
     */
    public List<AreaTree> city(AreaTree provinceTree) throws IOException, ParserException, BadHanyuPinyinOutputFormatCombination {

        List<AreaTree> result = new ArrayList<>();

        Parser parser = new Parser(provinceTree.getChildrenUrl());
        parser.setEncoding("GBK");
        CssSelectorNodeFilter paraFilter =new CssSelectorNodeFilter(".citytable .citytr");
        NodeList nodeList = parser.extractAllNodesThatMatch(paraFilter);
        for (int i = 0; i < nodeList.size(); i++) {
            Node node =  nodeList.elementAt(i);
            NodeList children = node.getChildren();

            // 编码
            Node node1 = children.elementAt(0);
            Node node1_1 = node1.getChildren().elementAt(0);
            LinkTag linkTag = ((LinkTag) node1_1);
            // 名称
            Node node2 = children.elementAt(1);
            Node node2_2 = node2.getChildren().elementAt(0);

            provinceTree.getArea().setCode(node1.toPlainTextString().substring(0, 6));
            AreaTree areaTree = newAreaTree(newArea(node2.toPlainTextString(),node1.toPlainTextString(), Area.TypeDictItem.city.itemValue(),provinceTree.getArea().getSeq() + i * 10));
            areaTree.setChildrenUrl(linkTag.getLink());
            areaTree.getParentNames().merge(provinceTree.getParentNames());
            areaTree.getParentNames().add(provinceTree.getArea().getName());
            county(areaTree);
            result.add(areaTree);
        }
        provinceTree.setChildren(result);
        return result;
    }

    /**
     * 区
     * @param cityTree
     * @return
     * @throws IOException
     * @throws ParserException
     * @throws BadHanyuPinyinOutputFormatCombination
     */
    public List<AreaTree> county(AreaTree cityTree) throws IOException, ParserException, BadHanyuPinyinOutputFormatCombination {

        System.out.println(cityTree.getChildrenUrl());
        List<AreaTree> result = new ArrayList<>();

        Parser parser = new Parser(cityTree.getChildrenUrl());
        parser.setEncoding("GBK");
        CssSelectorNodeFilter paraFilter =new CssSelectorNodeFilter(".countytable .countytr");
        NodeList nodeList = parser.extractAllNodesThatMatch(paraFilter);
        for (int i = 0; i < nodeList.size(); i++) {
            Node node =  nodeList.elementAt(i);
            NodeList children = node.getChildren();

            // 编码
            Node node1 = children.elementAt(0);
            Node node1_1 = node1.getChildren().elementAt(0);
            LinkTag linkTag = null;
            if(node1_1 instanceof LinkTag){
                linkTag = ((LinkTag) node1_1);
            }
            // 名称
            Node node2 = children.elementAt(1);
            Node node2_2 = node2.getChildren().elementAt(0);

            AreaTree areaTree = newAreaTree(newArea(node2.toPlainTextString(),node1.toPlainTextString(), Area.TypeDictItem.county.itemValue(),cityTree.getArea().getSeq() + i * 10));
            areaTree.setChildrenUrl(linkTag == null? null: linkTag.getLink());
            areaTree.getParentNames().merge(cityTree.getParentNames());
            areaTree.getParentNames().add(cityTree.getArea().getName());
            result.add(areaTree);
        }
        cityTree.setChildren(result);
        return result;
    }
    public  AreaTree newAreaTree(Area area) {
        AreaTree areaTree = new AreaTree();
        areaTree.setArea(area);

        return areaTree;
    }
    public  Area newArea(String name,String code,String type,Integer seq) throws BadHanyuPinyinOutputFormatCombination {
        Pinyin pinyin = getPinyin(name);
        Area area = new Area();
        area.setName(name)
                .setCode(code)
                .setSpellFirst(pinyin.getFirst())
                .setSpellSimple(pinyin.getSimple())
                .setSpell(pinyin.getFull())
                .setTypeDictId(type)
        .setSeq(seq)
                ;
        return area;
    }

    /**
     * 抓取数据
     * 数据地址 国家统计局 http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/index.html
     */
    //@Test
    void importTest() throws IOException, ParserException, BadHanyuPinyinOutputFormatCombination {

        AreaTree areaTree = newAreaTree(newArea("中华人民共和国","1", Area.TypeDictItem.country.itemValue(),1));

        List<AreaTree> province = province();
        areaTree.setChildren(province);
        insert(areaTree,null);

        System.out.println(JSONUtil.toJsonStr(province));

    }
}
