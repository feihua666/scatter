package scatter.area.rest.mapper;

import scatter.area.pojo.po.Area;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 区域表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
public interface AreaMapper extends IBaseMapper<Area> {

}
