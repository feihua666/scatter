package scatter.area.rest.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.area.pojo.dto.AreaIdResolveDto;
import scatter.area.pojo.param.AreaIdResolveParam;
import scatter.area.pojo.po.Area;
import scatter.common.rest.service.IBaseService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 区域表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
public interface IAreaService extends IBaseService<Area> {
    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Area getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Area>lambdaQuery().eq(Area::getCode, code));
    }
    /**
     * 根据查询所有父级包括自己名称拼接
     * @param id
     * @return
     */
    default Area getAllById(String id) {

        if(id== null){
            return null;
        }

        List<Area> allParentsAndSelf = getAllParentsAndSelf(id);
        Area area = allParentsAndSelf.stream().filter(f -> f.getId().equals(id)).findFirst().orElse(allParentsAndSelf.get(0));
        String names = allParentsAndSelf.stream().map(Area::getName).collect(Collectors.joining("•"));
        area.setName(names.replace("•市辖区",""));
        return area;
    }
    /**
     * 根据编码查询
     * @param ids
     * @return
     */
    default List<Area> getAllByIds(Set<String> ids) {
        if(CollUtil.isEmpty(ids)){
            return null;
        }

        List<Area> areas = new ArrayList<>();
        ids.stream().distinct().forEach(id->{
            areas.add(getAllById(id));
        });
        return areas;
    }

    /**
     * 根据名称尝试获取
     * @param name
     * @param typeDictItem
     * @param parentId 父级id，更加精准
     * @return 返回值只有一个才精准，返回多个可能有重名
     */
    public List<Area> getListByName(String name,Area.TypeDictItem typeDictItem,String parentId);

    /**
     * 同 getListByName 只返回一个，如果有多个返回 null
     * @param name
     * @param typeDictItem
     * @param parentId
     * @return
     */
    default public Area getOneByName(String name,Area.TypeDictItem typeDictItem,String parentId){
        List<Area> listByName = getListByName(name, typeDictItem, parentId);
        if (isEmpty(listByName)) {
            return null;
        }
        if (listByName.size() == 1) {
            return listByName.iterator().next();
        }
        return null;
    }

    /**
     * 根据名称，翻译id
     * @param resolveParam
     * @return
     */
    public AreaIdResolveDto resolveName(AreaIdResolveParam resolveParam);
}
