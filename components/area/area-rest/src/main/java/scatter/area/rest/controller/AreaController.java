package scatter.area.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.area.pojo.form.AreaAddForm;
import scatter.area.pojo.form.AreaPageQueryForm;
import scatter.area.pojo.form.AreaUpdateForm;
import scatter.area.pojo.po.Area;
import scatter.area.pojo.vo.AppAreaVo;
import scatter.area.pojo.vo.AreaVo;
import scatter.area.rest.AreaConfiguration;
import scatter.area.rest.mapstruct.AreaMapStruct;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 区域表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@RestController
@RequestMapping(AreaConfiguration.CONTROLLER_BASE_PATH + "/area")
@Api(tags = "区域")
public class AreaController extends BaseAddUpdateQueryFormController<Area, AreaVo, AreaAddForm, AreaUpdateForm, AreaPageQueryForm> {


     @ApiOperation("添加区域")
     @PreAuthorize("hasAuthority('Area:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public AreaVo add(@RequestBody @Valid AreaAddForm addForm) {
         return super.add(addForm);
     }

     @ApiOperation("根据ID查询区域")
     @PreAuthorize("hasAuthority('Area:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public AreaVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @ApiOperation("根据ID删除区域")
     @PreAuthorize("hasAuthority('Area:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     @Override
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @ApiOperation("根据ID更新区域")
     @PreAuthorize("hasAuthority('Area:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     @Override
     public AreaVo update(@RequestBody @Valid AreaUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @ApiOperation("不分页查询区域")
    //@PreAuthorize("hasAuthority('Area:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<AreaVo> getList(AreaPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("不分页查询区域")
    @GetMapping("/app/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<AppAreaVo> getAppList(AreaPageQueryForm listPageForm) {
         List<Area> list = getIBaseQueryFormService().list(listPageForm);
        return AreaMapStruct.INSTANCE.poToAppVo(list);
    }

    @ApiOperation("分页查询区域")
    @PreAuthorize("hasAuthority('Area:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<AreaVo> getPage(AreaPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
