package scatter.area.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.area.pojo.po.Area;
import scatter.area.rest.service.IAreaService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 区域表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@RestController
@RequestMapping("/inner/area")
public class AreaInnerController extends BaseInnerController<Area> {
 @Autowired
 private IAreaService areaService;

 public IAreaService getService(){
     return areaService;
 }
}
