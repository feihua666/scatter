package scatter.area.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.area.pojo.form.AreaAddForm;
import scatter.area.pojo.form.AreaPageQueryForm;
import scatter.area.pojo.form.AreaUpdateForm;
import scatter.area.pojo.po.Area;
import scatter.area.pojo.vo.AppAreaVo;
import scatter.area.pojo.vo.AreaVo;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;

import java.util.List;

/**
 * <p>
 * 区域 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AreaMapStruct extends
        IBaseVoMapStruct<Area, AreaVo>,
        IBaseAddFormMapStruct<Area, AreaAddForm>,
        IBaseUpdateFormMapStruct<Area, AreaUpdateForm>,
        IBaseQueryFormMapStruct<Area, AreaPageQueryForm> {
    AreaMapStruct INSTANCE = Mappers.getMapper(AreaMapStruct.class);

    AppAreaVo poToAppVo(Area area);

    List<AppAreaVo> poToAppVo(List<Area> list);
}
