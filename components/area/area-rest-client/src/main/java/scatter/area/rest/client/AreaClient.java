package scatter.area.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 区域表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-11-12
 */
@Component
@FeignClient(value = "Area-client")
public interface AreaClient {

}
