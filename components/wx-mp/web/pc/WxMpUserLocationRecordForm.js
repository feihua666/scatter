const WxMpUserLocationRecordForm = [
    {
        WxMpUserLocationRecordSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'wxUserId',
        },
        element:{
            label: '微信用户id',
            required: true,
        }
    },
    {
        field: {
            name: 'longitude',
        },
        element:{
            label: '经度',
        }
    },
    {
        field: {
            name: 'latitude',
        },
        element:{
            label: '纬度',
        }
    },
    {
        field: {
            name: 'precisions',
        },
        element:{
            label: '经纬度精度',
        }
    },
    {
        field: {
            name: 'locationDesc',
        },
        element:{
            label: '地理位置描述',
        }
    },
]
export default WxMpUserLocationRecordForm