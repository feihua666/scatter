import MpUrl from './element/mp/MpUrl.js'

const WxMpMsgReplyForm = [
    {
        field: {
            name: 'name',
        },
        element:{
            label: '名称',
        }
    },
    {
        WxMpMsgReplySearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'appCode'
        },
        element:{
            type: 'select',
            required: true,
            options:{
                datas: MpUrl.configList,
                optionProp:{
                    value: 'appCode',
                    label: 'appName'
                }
            },
            label: '公众号',
        }
    },

    {
        WxMpMsgReplySearchList: false,
        field: {
            name: 'matchFromUser',
        },
        element:{
            label: '匹配接收人',
        }
    },
    {
        field: {
            name: 'matchMsgTypeDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'wx_mp_push_xml_msg_type'
            },
            label: '匹配消息类型',
        }
    },
    {
        field: {
            name: 'matchEventTypeDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'wx_mp_push_event_type'
            },
            label: '匹配事件类型',
        }
    },
    {
        WxMpMsgReplySearchList: false,
        field: {
            name: 'matchEventKey',
        },
        element:{
            label: '匹配事件key',
        }
    },
    {
        field: {
            name: 'matchEventKeyMatchDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'match_type'
            },
            label: '匹配event_key的匹配方式',
            required: ({form})=>{return !!form.matchEventKey}
        }
    },
    {
        field: {
            name: 'matchContent',
        },
        element:{
            label: '匹配内容',
        }
    },
    {
        field: {
            name: 'matchContentMatchDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'match_type'
            },
            label: '匹配内容的匹配方式',
        }
    },
    {
        WxMpMsgReplySearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'replyMsgTypeDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'wx_mp_push_xml_msg_type'
            },
            label: '回复的消息类型',
            required: true,
        }
    },
    {
        WxMpMsgReplySearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'replyContent',
        },
        element:{
            label: '回复内容',
            required: true,
        }
    },
    {
        WxMpMsgReplySearchList: false,
        field: {
            name: 'priority',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '优先级',
        }
    },
    {
        WxMpMsgReplySearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '备注',
        }
    },
]
export default WxMpMsgReplyForm