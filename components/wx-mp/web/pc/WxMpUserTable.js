const WxMpUserTable = [
    {
        prop: 'avatar',
        label: '头像地址',
        stype: 'image'
    },
    {
        prop: 'appCode',
        label: '公众号编码'
    },
    {
        prop: 'appName',
        label: '公众号名称'
    },
    {
        prop: 'openId',
        label: 'openId'
    },
    {
        prop: 'unionId',
        label: 'unionId'
    },
    {
        prop: 'nickname',
        label: '微信昵称'
    },
    {
        prop: 'genderDictName',
        label: '性别'
    },
    {
        prop: 'cityName',
        label: '城市名称'
    },
    {
        prop: 'provinceName',
        label: '省名称'
    },
    {
        prop: 'countryName',
        label: '国家名称'
    },

    {
        prop: 'language',
        label: '语言'
    },
    {
        prop: 'longitude',
        label: '经度'
    },
    {
        prop: 'latitude',
        label: '纬度'
    },
    {
        prop: 'precisions',
        label: '经纬度精度'
    },
    {
        prop: 'locationDesc',
        label: '地理位置描述'
    },
    {
        prop: 'isSubscribe',
        label: '是否关注'
    },
    {
        prop: 'sourceDictName',
        label: '来源'
    },
]
export default WxMpUserTable