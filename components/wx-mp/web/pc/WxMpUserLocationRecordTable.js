const WxMpUserLocationRecordTable = [
    {
        prop: 'wxUserId',
        label: '微信用户id'
    },
    {
        prop: 'longitude',
        label: '经度'
    },
    {
        prop: 'latitude',
        label: '纬度'
    },
    {
        prop: 'precisions',
        label: '经纬度精度'
    },
    {
        prop: 'locationDesc',
        label: '地理位置描述'
    },
]
export default WxMpUserLocationRecordTable