const basePath = '' + '/wx-mp-msg-reply'
const WxMpMsgReplyUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/WxMpMsgReplySearchList',
        add: '/WxMpMsgReplyAdd',
        update: '/WxMpMsgReplyUpdate',
    }
}
export default WxMpMsgReplyUrl