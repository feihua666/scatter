const WxMpUserForm = [
    {

        field: {
            name: 'appCode',
        },
        element:{
            label: '公众号编码',
        }
    },
    {
        field: {
            name: 'appName',
        },
        element:{
            label: '公众号名称',
        }
    },
    {
        field: {
            name: 'openId',
        },
        element:{
            label: 'openId',
        }
    },
    {
        field: {
            name: 'unionId',
        },
        element:{
            label: 'unionId',
        }
    },
    {
        field: {
            name: 'nickname',
        },
        element:{
            label: '微信昵称',
        }
    },
    {
        field: {
            name: 'genderDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'gender'
            },
            label: '性别',
        }
    },
    {
        field: {
            name: 'sourceDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'wx_mp_user_how_from'
            },
            label: '来源',
        }
    },
]
export default WxMpUserForm