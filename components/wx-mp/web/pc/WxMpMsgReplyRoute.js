import WxMpMsgReplyUrl from './WxMpMsgReplyUrl.js'

const WxMpMsgReplyRoute = [
    {
        path: WxMpMsgReplyUrl.router.searchList,
        component: () => import('./element/WxMpMsgReplySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'WxMpMsgReplySearchList',
            name: '公众号消息回复管理'
        }
    },
    {
        path: WxMpMsgReplyUrl.router.add,
        component: () => import('./element/WxMpMsgReplyAdd'),
        meta: {
            code:'WxMpMsgReplyAdd',
            name: '公众号消息回复添加'
        }
    },
    {
        path: WxMpMsgReplyUrl.router.update,
        component: () => import('./element/WxMpMsgReplyUpdate'),
        meta: {
            code:'WxMpMsgReplyUpdate',
            name: '公众号消息回复修改'
        }
    },
]
export default WxMpMsgReplyRoute