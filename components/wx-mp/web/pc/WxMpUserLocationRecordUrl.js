const basePath = '' + '/wx-mp-user-location-record'
const WxMpUserLocationRecordUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/WxMpUserLocationRecordSearchList',
        add: '/WxMpUserLocationRecordAdd',
        update: '/WxMpUserLocationRecordUpdate',
    }
}
export default WxMpUserLocationRecordUrl