const basePath = '' + '/wx-mp-user'
const WxMpUserUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/WxMpUserSearchList',
    }
}
export default WxMpUserUrl