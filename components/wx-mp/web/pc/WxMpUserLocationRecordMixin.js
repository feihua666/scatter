import WxMpUserLocationRecordForm from './WxMpUserLocationRecordForm.js'
import WxMpUserLocationRecordTable from './WxMpUserLocationRecordTable.js'
import WxMpUserLocationRecordUrl from './WxMpUserLocationRecordUrl.js'

const WxMpUserLocationRecordMixin = {
    computed: {
        WxMpUserLocationRecordFormOptions() {
            return this.$stDynamicFormTools.options(WxMpUserLocationRecordForm,this.$options.name)
        },
        WxMpUserLocationRecordTableOptions() {
            return WxMpUserLocationRecordTable
        },
        WxMpUserLocationRecordUrl(){
            return WxMpUserLocationRecordUrl
        }
    },
}
export default WxMpUserLocationRecordMixin