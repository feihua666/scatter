const basePath = ''
const MpUrl = {
    MpUserTagList: basePath + '/wx/mp/usertag/{appCode}/list',
    MpUserTagAdd: basePath + '/wx/mp/usertag/{appCode}/create',
    MpUserTagUpdate: basePath + '/wx/mp/usertag/{appCode}/update/{id}',
    MpUserTagDelete: basePath + '/wx/mp/usertag/{appCode}/delete/{id}',
    configList: basePath + '/wx/mp/config/list',

    MpTempMsgList: basePath + '/wx/mp/tempmsg/{appCode}/templateList',
    MpTempMsgDelete: basePath + '/wx/mp/tempmsg/{appCode}/deleteTemplate/{templateId}',
    MpTempMsgAdd: basePath + '/wx/mp/tempmsg/{appCode}/addTemplate/{templateId}',
    MpTempMsgSend: basePath + '/wx/mp/tempmsg/{appCode}/sendTemplateMsg',
    MpTempMsgIndustryGet: basePath + '/wx/mp/tempmsg/{appCode}/industry',
    MpTempMsgIndustrySet: basePath + '/wx/mp/tempmsg/{appCode}/industry',
    MpTempMsgPrimaryIndustry: basePath + '/wx/mp/tempmsg/industryDict',
    MpTempMsgSecondIndustry: basePath + '/wx/mp/tempmsg/industryDict',


    MpHelperClearQuota: basePath + '/wx/mp/helper/{appCode}/clear/quota',
    MpSelfMenuGet: basePath + '/wx/mp/menu/{appCode}/getSelfMenuInfoOrigin',
    MpSelfMenuDelete: basePath + '/wx/mp/menu/{appCode}/delete',
    MpSelfMenuPushToWxServer: basePath + '/wx/mp/menu/{appCode}/createByJsonStr',

    MpSelfConditionalMenuGet: basePath + '/wx/mp/menu/{appCode}/getOrigin',
    MpSelfConditionalMenuDelete: basePath + '/wx/mp/menu/{appCode}/delete/{menuId}',

    PermanentNewsListPage: basePath + '/wx/mp/material/{appCode}/news/listPage',
    PermanentNewsDelete: basePath + '/wx/mp/material/{appCode}/delete/{mediaId}',
    PermanentNewsAdd: basePath + '/wx/mp/material/{appCode}/create/news',
    PermanentNewsUpdate: basePath + '/wx/mp/material/{appCode}/update/news',
    PermanentNewsGetByMediaId: basePath + '/wx/mp/material/{appCode}/news/{mediaId}',

    PermanentOtherListPage: basePath + '/wx/mp/material/{appCode}/media/{type}/listPage',
    PermanentOtherDelete: basePath + '/wx/mp/material/{appCode}/delete/{mediaId}',
    PermanentOtherAdd: basePath + '/wx/mp/material/{appCode}/create/file/{type}',

    materialUpload: basePath + '/wx/mp/material/{appCode}/create/file/{type}',
    TemporaryMaterialAdd: basePath + '/wx/mp/material/{appCode}/create/media/{type}',
    TemporaryMaterialGet: basePath + '/wx/mp/material/{appCode}/get/media/{mediaId}',

    MassHairNewsAdd: basePath + '/wx/mp/mass/{appCode}/uploadnews',
    MassHairVideoAdd: basePath + '/wx/mp/mass/{appCode}/uploadvideo',

    MassHairSendTag: basePath + '/wx/mp/mass/{appCode}/sendTag',
    MassHairSendOpenIds: basePath + '/wx/mp/mass/{appCode}/sendOpenIds',
    MassHairSendPreview: basePath + '/wx/mp/mass/{appCode}/preview',
    // 用户授权登录构造授权url
    Oauth2buildAuthorizationUrl: basePath + '/wx/mp/oauth2/{appCode}/oauth2buildAuthorizationUrl',

    router: {
        MpUserTag: '/wx/mpUserTag',
        MpTempMsg: '/wx/mpTempMsg',
        MpHelper: '/wx/mpHelper',
        MpSelfMenu: '/wx/mpSelfMenu',
        MpSelfConditionalMenu: '/wx/mpConditionalMenu',
        PermanentMaterial: '/wx/mpPermanentMaterial',
        PermanentMaterialNewsAdd: '/wx/mpPermanentMaterialNewsAdd',
        PermanentMaterialNewsEdit: '/wx/mpPermanentMaterialNewsEdit/:mediaId',
        PermanentMaterialOtherAdd: '/wx/mpPermanentMaterialOtherAdd',
        TemporaryMaterial: '/wx/mpTemporaryMaterial',
        MpMassHair: '/wx/mpMassHair',
    }
}
export default MpUrl