import MpUrl from './MpUrl.js'
const MpRoute = [
    {
        path: MpUrl.router.MpUserTag,
        component: () => import('./usertag/MpUserTag.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MpUserTagList',
            name: '公众号用户标签'
        }
    },
    {
        path: MpUrl.router.MpTempMsg,
        component: () => import('./tempmsg/MpTempMsg.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MpTempMsg',
            name: '公众号模板消息'
        }
    },
    {
        path: MpUrl.router.MpHelper,
        component: () => import('./helper/MpHelper.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MpHelper',
            name: '公众号辅助功能'
        }
    },
    {
        path: MpUrl.router.MpSelfMenu,
        component: () => import('./menu/MpSelfMenu'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MpSelfMenu',
            name: '公众号自定义菜单'
        }
    },
    {
        path: MpUrl.router.MpSelfConditionalMenu,
        component: () => import('./menu/MpSelfConditionalMenu.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MpSelfConditionalMenu',
            name: '公众号个性化菜单'
        }
    },
    {
        path: MpUrl.router.PermanentMaterial,
        component: () => import('./material/permanent/PermanentMaterial.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PermanentMaterial',
            name: '公众号永久素材'
        }
    },
    {
        path: MpUrl.router.PermanentMaterialNewsAdd,
        component: () => import('./material/permanent/PermanentNewsAdd.vue'),
        meta: {
            code:'PermanentMaterialNewsAdd',
            name: '公众号永久素材图文添加'
        }
    },
    {
        path: MpUrl.router.PermanentMaterialNewsEdit,
        component: () => import('./material/permanent/PermanentNewsEdit.vue'),
        meta: {
            code:'PermanentMaterialNewsEdit',
            name: '公众号永久素材图文修改'
        }
    },
    {
        path: MpUrl.router.PermanentMaterialOtherAdd,
        component: () => import('./material/permanent/PermanentOtherAdd.vue'),
        meta: {
            code:'PermanentMaterialOtherAdd',
            name: '公众号永久素材其它类型添加'
        }
    },
    {
        path: MpUrl.router.TemporaryMaterial,
        component: () => import('./material/temporary/TemporaryMaterial.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'TemporaryMaterial',
            name: '公众号临时素材'
        }
    },
    {
        path: MpUrl.router.MpMassHair,
        component: () => import('./masshair/MassHair.vue'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MpMassHair',
            name: '公众号消息群发'
        }
    }
]
export default MpRoute