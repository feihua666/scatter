import WxMpUserForm from './WxMpUserForm.js'
import WxMpUserTable from './WxMpUserTable.js'
import WxMpUserUrl from './WxMpUserUrl.js'

const WxMpUserMixin = {
    computed: {
        WxMpUserFormOptions() {
            return this.$stDynamicFormTools.options(WxMpUserForm,this.$options.name)
        },
        WxMpUserTableOptions() {
            return WxMpUserTable
        },
        WxMpUserUrl(){
            return WxMpUserUrl
        }
    },
}
export default WxMpUserMixin