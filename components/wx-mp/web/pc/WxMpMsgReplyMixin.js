import WxMpMsgReplyForm from './WxMpMsgReplyForm.js'
import WxMpMsgReplyTable from './WxMpMsgReplyTable.js'
import WxMpMsgReplyUrl from './WxMpMsgReplyUrl.js'

const WxMpMsgReplyMixin = {
    computed: {
        WxMpMsgReplyFormOptions() {
            return this.$stDynamicFormTools.options(WxMpMsgReplyForm,this.$options.name)
        },
        WxMpMsgReplyTableOptions() {
            return WxMpMsgReplyTable
        },
        WxMpMsgReplyUrl(){
            return WxMpMsgReplyUrl
        }
    },
}
export default WxMpMsgReplyMixin