import WxMpUserLocationRecordUrl from './WxMpUserLocationRecordUrl.js'

const WxMpUserLocationRecordRoute = [
    {
        path: WxMpUserLocationRecordUrl.router.searchList,
        component: () => import('./element/WxMpUserLocationRecordSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'WxMpUserLocationRecordSearchList',
            name: '公众号微信用户位置记录管理'
        }
    },
    {
        path: WxMpUserLocationRecordUrl.router.add,
        component: () => import('./element/WxMpUserLocationRecordAdd'),
        meta: {
            code:'WxMpUserLocationRecordAdd',
            name: '公众号微信用户位置记录添加'
        }
    },
    {
        path: WxMpUserLocationRecordUrl.router.update,
        component: () => import('./element/WxMpUserLocationRecordUpdate'),
        meta: {
            code:'WxMpUserLocationRecordUpdate',
            name: '公众号微信用户位置记录修改'
        }
    },
]
export default WxMpUserLocationRecordRoute