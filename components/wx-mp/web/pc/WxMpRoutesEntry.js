import WxMpUserRoute from './WxMpUserRoute.js'
import WxMpUserLocationRecordRoute from './WxMpUserLocationRecordRoute.js'
import WxMpMsgReplyRoute from './WxMpMsgReplyRoute.js'
import MpRoute from './element/mp/MpRoute.js'

const WxMpRoutesEntry = [
]
.concat(WxMpUserRoute)
.concat(WxMpUserLocationRecordRoute)
.concat(WxMpMsgReplyRoute)
.concat(MpRoute)
export default WxMpRoutesEntry