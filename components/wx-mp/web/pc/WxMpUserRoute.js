import WxMpUserUrl from './WxMpUserUrl.js'

const WxMpUserRoute = [
    {
        path: WxMpUserUrl.router.searchList,
        component: () => import('./element/WxMpUserSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'WxMpUserSearchList',
            name: '公众号微信用户管理'
        }
    }
]
export default WxMpUserRoute