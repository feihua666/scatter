package scatter.wxmp.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.common.rest.validation.DictService;
import scatter.common.rest.validation.InvalidDictServiceImpl;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, WxMpConfiguration.class})
@MapperScan("scatter.wxmp.rest.mapper")
public class WxMpTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(WxMpTestApplication.class, args);
    }

    @Bean
    public DictService dictService() {
        return new InvalidDictServiceImpl();
    }
}
