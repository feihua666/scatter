package scatter.wxmp.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.pojo.form.WxMpUserAddForm;
import scatter.wxmp.pojo.form.WxMpUserUpdateForm;
import scatter.wxmp.pojo.form.WxMpUserPageQueryForm;
import scatter.wxmp.rest.service.IWxMpUserService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 公众号微信用户 测试类基类
* </p>
*
* @author yw
* @since 2020-12-24
*/
public class WxMpUserSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IWxMpUserService wxMpUserService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return wxMpUserService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return wxMpUserService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public WxMpUser mockPo() {
        return JMockData.mock(WxMpUser.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public WxMpUserAddForm mockAddForm() {
        return JMockData.mock(WxMpUserAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public WxMpUserUpdateForm mockUpdateForm() {
        return JMockData.mock(WxMpUserUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public WxMpUserPageQueryForm mockPageQueryForm() {
        return JMockData.mock(WxMpUserPageQueryForm.class, mockConfig);
    }
}