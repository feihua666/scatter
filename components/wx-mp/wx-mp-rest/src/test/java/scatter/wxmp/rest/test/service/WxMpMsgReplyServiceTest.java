package scatter.wxmp.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.pojo.form.WxMpMsgReplyAddForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyUpdateForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyPageQueryForm;
import scatter.wxmp.rest.test.WxMpMsgReplySuperTest;
import scatter.wxmp.rest.service.IWxMpMsgReplyService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 公众号消息回复 服务测试类
* </p>
*
* @author yw
* @since 2020-12-24
*/
@SpringBootTest
public class WxMpMsgReplyServiceTest extends WxMpMsgReplySuperTest{

    @Autowired
    private IWxMpMsgReplyService wxMpMsgReplyService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<WxMpMsgReply> pos = wxMpMsgReplyService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}