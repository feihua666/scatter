package scatter.wxmp.rest.test.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * Created by yangwei
 * Created at 2020/12/24 19:08
 */
@SpringBootTest
@AutoConfigureMockMvc
public class MpConfigControllerTest {
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testWxMpConfig() throws Exception {
        // 请求url
        String url = WxMpConfiguration.CONTROLLER_BASE_PATH + "/wx/mp/config/list";

        // 请求表单
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON)
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        //List<MpConfigVo> vo = JSONUtil.toBean(response.getContentAsString(), MpConfigVo.class);
        //Assertions.assertEquals(201,response.getStatus());
        System.out.println(response.getContentAsString());
        System.out.println();
    }
}
