package scatter.wxmp.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordAddForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordUpdateForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordPageQueryForm;
import scatter.wxmp.rest.test.WxMpUserLocationRecordSuperTest;
import scatter.wxmp.rest.service.IWxMpUserLocationRecordService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 公众号微信用户位置记录 服务测试类
* </p>
*
* @author yw
* @since 2020-12-24
*/
@SpringBootTest
public class WxMpUserLocationRecordServiceTest extends WxMpUserLocationRecordSuperTest{

    @Autowired
    private IWxMpUserLocationRecordService wxMpUserLocationRecordService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<WxMpUserLocationRecord> pos = wxMpUserLocationRecordService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}