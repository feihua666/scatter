package scatter.wxmp.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordAddForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordUpdateForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordPageQueryForm;
import scatter.wxmp.rest.service.IWxMpUserLocationRecordService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 公众号微信用户位置记录 测试类基类
* </p>
*
* @author yw
* @since 2020-12-24
*/
public class WxMpUserLocationRecordSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IWxMpUserLocationRecordService wxMpUserLocationRecordService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return wxMpUserLocationRecordService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return wxMpUserLocationRecordService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public WxMpUserLocationRecord mockPo() {
        return JMockData.mock(WxMpUserLocationRecord.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public WxMpUserLocationRecordAddForm mockAddForm() {
        return JMockData.mock(WxMpUserLocationRecordAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public WxMpUserLocationRecordUpdateForm mockUpdateForm() {
        return JMockData.mock(WxMpUserLocationRecordUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public WxMpUserLocationRecordPageQueryForm mockPageQueryForm() {
        return JMockData.mock(WxMpUserLocationRecordPageQueryForm.class, mockConfig);
    }
}