package scatter.wxmp.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.pojo.form.WxMpMsgReplyAddForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyUpdateForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyPageQueryForm;
import scatter.wxmp.rest.service.IWxMpMsgReplyService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 公众号消息回复 测试类基类
* </p>
*
* @author yw
* @since 2020-12-24
*/
public class WxMpMsgReplySuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IWxMpMsgReplyService wxMpMsgReplyService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return wxMpMsgReplyService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return wxMpMsgReplyService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public WxMpMsgReply mockPo() {
        return JMockData.mock(WxMpMsgReply.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public WxMpMsgReplyAddForm mockAddForm() {
        return JMockData.mock(WxMpMsgReplyAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public WxMpMsgReplyUpdateForm mockUpdateForm() {
        return JMockData.mock(WxMpMsgReplyUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public WxMpMsgReplyPageQueryForm mockPageQueryForm() {
        return JMockData.mock(WxMpMsgReplyPageQueryForm.class, mockConfig);
    }
}