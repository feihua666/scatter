package scatter.wxmp.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxmp.rest.test.WxMpMsgReplySuperTest;
/**
* <p>
* 公众号消息回复 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-24
*/
@SpringBootTest
public class WxMpMsgReplyInnerControllerTest extends WxMpMsgReplySuperTest{
    @Test
    void contextLoads() {
    }
}