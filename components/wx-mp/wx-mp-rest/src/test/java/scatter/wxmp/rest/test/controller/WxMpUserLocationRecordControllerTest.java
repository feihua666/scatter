package scatter.wxmp.rest.test.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxmp.rest.test.WxMpUserLocationRecordSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordAddForm;
import scatter.wxmp.pojo.vo.WxMpUserLocationRecordVo;
import scatter.wxmp.rest.WxMpConfiguration;
/**
* <p>
* 公众号微信用户位置记录 前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-24
*/
@SpringBootTest
@AutoConfigureMockMvc
public class WxMpUserLocationRecordControllerTest extends WxMpUserLocationRecordSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = WxMpConfiguration.CONTROLLER_BASE_PATH + "/wx-mp-user-location-record";

        // 请求表单
        WxMpUserLocationRecordAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        WxMpUserLocationRecordVo vo = JSONUtil.toBean(response.getContentAsString(), WxMpUserLocationRecordVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}