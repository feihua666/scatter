DROP TABLE IF EXISTS component_wx_mp_user_location_record;
CREATE TABLE `component_wx_mp_user_location_record` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `wx_user_id` varchar(20) NOT NULL COMMENT '微信用户id',
  `longitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `precisions` varchar(255) DEFAULT NULL COMMENT '经纬度精度',
  `location_desc` varchar(255) DEFAULT NULL COMMENT '地理位置描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`),
  KEY `wx_user_id` (`wx_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公众号微信用户位置记录表';
