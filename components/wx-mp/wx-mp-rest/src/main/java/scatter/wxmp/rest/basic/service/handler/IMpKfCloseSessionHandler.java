package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 客服关闭会话handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpKfCloseSessionHandler extends WxMpMessageHandler {
}
