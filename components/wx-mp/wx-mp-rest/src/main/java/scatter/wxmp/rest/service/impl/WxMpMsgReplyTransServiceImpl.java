package scatter.wxmp.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.rest.service.IWxMpMsgReplyService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 公众号消息回复翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Component
public class WxMpMsgReplyTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IWxMpMsgReplyService wxMpMsgReplyService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,WxMpMsgReply.TRANS_WXMPMSGREPLY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,WxMpMsgReply.TRANS_WXMPMSGREPLY_BY_ID)) {
            WxMpMsgReply byId = wxMpMsgReplyService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,WxMpMsgReply.TRANS_WXMPMSGREPLY_BY_ID)) {
            return wxMpMsgReplyService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
