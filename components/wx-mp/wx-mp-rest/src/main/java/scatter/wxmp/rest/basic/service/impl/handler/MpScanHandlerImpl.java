package scatter.wxmp.rest.basic.service.impl.handler;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Service;
import scatter.wxmp.rest.basic.service.handler.IMpScanHandler;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2020/7/21 18:42
 */
@Service
public class MpScanHandlerImpl implements IMpScanHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
        return null;
    }
}
