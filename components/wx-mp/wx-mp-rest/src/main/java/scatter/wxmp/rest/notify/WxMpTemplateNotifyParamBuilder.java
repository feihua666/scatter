package scatter.wxmp.rest.notify;

import lombok.Setter;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import scatter.common.rest.notify.NotifyParam;

import java.util.List;

import static scatter.wxmp.rest.notify.DefaultWxMpTemplateNotifyListenerImpl.*;

/**
 * <p>
 * 微信公众号模板消息参数构建器
 * </p>
 *
 * @author yangwei
 * @since 2021-09-02 22:24
 */
public class WxMpTemplateNotifyParamBuilder {

	@Setter
	private NotifyParam param;

	public static WxMpTemplateNotifyParamBuilder create(){
		WxMpTemplateNotifyParamBuilder wxMpTemplateNotifyParamBuilder = new WxMpTemplateNotifyParamBuilder();
		NotifyParam notifyParam = new NotifyParam();
		notifyParam.setTypes(NotifyParam.Type.wxmpTemplate.name());
		wxMpTemplateNotifyParamBuilder.setParam(notifyParam);
		return wxMpTemplateNotifyParamBuilder;
	}
	public WxMpTemplateNotifyParamBuilder templateId(String templateId){
		param.addExt(TEMPLATE_ID_KEY,templateId);
		return this;
	}
	public WxMpTemplateNotifyParamBuilder appId(String appId){
		param.addExt(APPID_KEY,appId);
		return this;
	}
	public WxMpTemplateNotifyParamBuilder data(List<WxMpTemplateData> data){
		param.addExt(DATA_KEY,data);
		return this;
	}
	public WxMpTemplateNotifyParamBuilder miniProgram(WxMpTemplateMessage.MiniProgram miniProgram){
		param.addExt(MINI_PROGRAM_KEY,miniProgram);
		return this;
	}
	public WxMpTemplateNotifyParamBuilder toUser(List<String> openIds){
		param.setToUser(openIds);
		return this;
	}

	public NotifyParam build(){
		return param;
	}
}
