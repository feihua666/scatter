package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 默认的处理handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpDefaultHandler extends WxMpMessageHandler {
}
