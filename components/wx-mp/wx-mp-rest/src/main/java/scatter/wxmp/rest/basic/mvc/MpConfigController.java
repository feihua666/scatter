package scatter.wxmp.rest.basic.mvc;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.SuperController;
import scatter.wxmp.rest.WxMpConfiguration;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.basic.mapstruct.MpConfigWebMapper;
import scatter.wxmp.rest.basic.vo.MpConfigVo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH +"/wx/mp/config")
@Api(tags = "公众号jsapi相关接口")
public class MpConfigController extends SuperController {

    @Autowired
    private WxMpService wxMpService;
    @Autowired
    private MpProperties mpProperties;
    @Autowired
    private MpConfigWebMapper mpConfigWebMapper;

    /**
     * 公众号配置列表，主要用来下拉选择或列表查看
     * @return
     */
    @ApiOperation("公众号配置列表")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/list")
    public List<MpConfigVo> configList() {
        return mpProperties.getConfigs().stream().map(mpConfigWebMapper::configToVo).collect(Collectors.toList());
    }
}
