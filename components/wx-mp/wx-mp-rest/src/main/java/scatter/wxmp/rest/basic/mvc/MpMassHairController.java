package scatter.wxmp.rest.basic.mvc;

import scatter.common.rest.controller.SuperController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.*;
import me.chanjar.weixin.mp.bean.result.WxMpMassGetResult;
import me.chanjar.weixin.mp.bean.result.WxMpMassSendResult;
import me.chanjar.weixin.mp.bean.result.WxMpMassSpeedGetResult;
import me.chanjar.weixin.mp.bean.result.WxMpMassUploadResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * 公众号群发相关接口
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH +"/wx/mp/mass/{appId}")
@Api(tags = "公众号群发相关接口")
public class MpMassHairController extends SuperController {

    @Autowired
    private WxMpService wxMpService;

    /**
     * 根据标签进行群发接口
     * @return
     */
    @ApiOperation("根据标签进行群发接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:sendTag')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/sendTag")
    public WxMpMassSendResult sendTag(@PathVariable String appId, @RequestBody WxMpMassTagMessage wxMpMassTagMessage) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().massGroupMessageSend(wxMpMassTagMessage);
    }

    /**
     * 根据OpenID列表群发【订阅号不可用，服务号认证后可用】
     * @return
     */
    @ApiOperation("根据OpenID列表群发接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:sendOpenIds')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/sendOpenIds")
    public WxMpMassSendResult sendOpenIds(@PathVariable String appId, @RequestBody WxMpMassOpenIdsMessage wxMpMassOpenIdsMessage) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().massOpenIdsMessageSend(wxMpMassOpenIdsMessage);
    }
    /**
     * 预览接口【订阅号与服务号认证后均可用】
     * @return
     */
    @ApiOperation("群发预览接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:preview')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/preview")
    public WxMpMassSendResult preview(@PathVariable String appId, @RequestBody WxMpMassPreviewMessage wxMpMassPreviewMessage) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().massMessagePreview(wxMpMassPreviewMessage);
    }
    /**
     * 上传图文消息素材【订阅号与服务号认证后均可用】
     * @return
     */
    @ApiOperation("上传图文消息素材接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:uploadnews')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/uploadnews")
    public WxMpMassUploadResult uploadnews(@PathVariable String appId, @RequestBody WxMpMassNews news) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().massNewsUpload(news);
    }
    /**
     * 上传视频素材
     * @return
     */
    @ApiOperation("上传视频素材素材接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:uploadvideo')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/uploadvideo")
    public WxMpMassUploadResult uploadvideo(@PathVariable String appId, @RequestBody WxMpMassVideo video) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().massVideoUpload(video);
    }
    /**
     * 设置群发速度
     * @return
     */
    @ApiOperation("设置群发速度接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:speedset')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/speedset")
    public WxMpMassSpeedGetResult speedset(@PathVariable String appId, Integer speed) throws WxErrorException {
        this.wxMpService.switchoverTo(appId).getMassMessageService().messageMassSpeedSet(speed);
        return speedget(appId);
    }
    /**
     * 设置群发速度
     * @return
     */
    @ApiOperation("获取群发速度接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:speedget')")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/speedget")
    public WxMpMassSpeedGetResult speedget(@PathVariable String appId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().messageMassSpeedGet();
    }
    /**
     *  删除群发【订阅号与服务号认证后均可用】
     * @return
     */
    @ApiOperation("删除群发接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:delete')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteMass(@PathVariable String appId, Long msgId, Integer articleIndex) throws WxErrorException {
        this.wxMpService.switchoverTo(appId).getMassMessageService().delete(msgId,articleIndex);
    }

    /**
     *  查询群发消息发送状态【订阅号与服务号认证后均可用】
     * @return
     */
    @ApiOperation("查询群发消息发送状态接口")
    @PreAuthorize("hasAuthority('wx:mp:mass:get')")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/get")
    public WxMpMassGetResult get(@PathVariable String appId, Long msgId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMassMessageService().messageMassGet(msgId);
    }
}
