package scatter.wxmp.rest.mapper;

import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 公众号微信用户位置记录表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
public interface WxMpUserLocationRecordMapper extends IBaseMapper<WxMpUserLocationRecord> {

}
