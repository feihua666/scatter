package scatter.wxmp.rest.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by yangwei
 * Created at 2020/7/28 13:46
 */
public class WxMpCodeAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public WxMpCodeAuthenticationToken(Authentication authenticationToken) {
        this(authenticationToken.getPrincipal(), authenticationToken.getCredentials());
    }
    public WxMpCodeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public WxMpCodeAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
