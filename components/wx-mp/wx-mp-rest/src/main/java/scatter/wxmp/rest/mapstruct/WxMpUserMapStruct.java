package scatter.wxmp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wxmp.pojo.form.WxMpUserAddForm;
import scatter.wxmp.pojo.form.WxMpUserPageQueryForm;
import scatter.wxmp.pojo.form.WxMpUserUpdateForm;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.pojo.vo.WxMpUserVo;

/**
 * <p>
 * 公众号微信用户 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxMpUserMapStruct extends IBaseVoMapStruct<WxMpUser, WxMpUserVo>,
        IBaseAddFormMapStruct<WxMpUser,WxMpUserAddForm>,
        IBaseUpdateFormMapStruct<WxMpUser,WxMpUserUpdateForm>,
        IBaseQueryFormMapStruct<WxMpUser,WxMpUserPageQueryForm> {
    WxMpUserMapStruct INSTANCE = Mappers.getMapper( WxMpUserMapStruct.class );

}
