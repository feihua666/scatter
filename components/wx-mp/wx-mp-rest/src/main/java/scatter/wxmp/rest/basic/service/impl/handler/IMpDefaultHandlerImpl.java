package scatter.wxmp.rest.basic.service.impl.handler;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.rest.basic.service.handler.IMpDefaultEnhanceHander;
import scatter.wxmp.rest.basic.service.handler.IMpDefaultHandler;
import scatter.wxmp.rest.componentext.IMpMsgReplyParser;
import scatter.wxmp.rest.service.IWxMpMsgReplyService;

import java.util.List;
import java.util.Map;

/**
 * Created by yangwei
 * Created at 2020/7/21 18:57
 */
@Slf4j
@Service
public class IMpDefaultHandlerImpl implements IMpDefaultHandler {

    @Autowired(required = false)
    private List<IMpDefaultEnhanceHander> iMpDefaultEnhanceHanderList;
    @Autowired
    private IWxMpMsgReplyService iMpMsgReplyService;


    @Autowired(required = false)
    private List<IMpMsgReplyParser> iMpMsgReplyParsers;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {

        // 优先执行增强处理器
        if (iMpDefaultEnhanceHanderList != null) {
            for (IMpDefaultEnhanceHander iMpDefaultEnhanceHander : iMpDefaultEnhanceHanderList) {
                if (iMpDefaultEnhanceHander.support(wxMessage,context,wxMpService,sessionManager)) {
                    return iMpDefaultEnhanceHander.handle(wxMessage,context,wxMpService,sessionManager);
                }
            }
        }
        // 自定义规则处理
        if (iMpMsgReplyParsers != null) {
            WxMpMsgReply matched = iMpMsgReplyService.getMatched(wxMessage, context, wxMpService, sessionManager);
            if (matched != null) {
                for (IMpMsgReplyParser iMpMsgReplyParser : iMpMsgReplyParsers) {
                    if (iMpMsgReplyParser.support(matched)) {
                        return iMpMsgReplyParser.parse(matched, wxMessage, context, wxMpService, sessionManager);
                    }
                }
            }
        }


        return null;
    }
}
