package scatter.wxmp.rest.componentext;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 13:31
 */
public interface WxMpCityIdResolver {

	/**
	 * 根据给定的名字换取id
	 * @param param
	 * @return
	 */
	WxMpCityIdResolveDto resolve(WxMpCityIdResolveParam param);
}
