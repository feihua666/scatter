package scatter.wxmp.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
/**
 * <p>
 * 公众号微信用户位置记录表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
public interface IWxMpUserLocationRecordService extends IBaseService<WxMpUserLocationRecord> {


}
