package scatter.wxmp.rest.basic.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseVo;

/**
 * Created by yangwei
 * Created at 2020/7/22 12:38
 */
@Setter
@Getter
@ApiModel("公众号配置数据响应对象")
public class MpConfigVo  extends BaseVo {

    /**
     * 自定义编码
     */
    @ApiModelProperty("编码")
    private String appCode;
    /**
     * 自定义名称
     */
    @ApiModelProperty("名称")
    private String appName;

    /**
     * 设置微信公众号的appId
     */
    @ApiModelProperty("appId")
    private String appId;


    @ApiModelProperty("token")
    private String token;

    @ApiModelProperty("aesKey")
    private String aesKey;
}
