package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 地理位置上报地理位置事件handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpReportLocationHandler extends WxMpMessageHandler {
}
