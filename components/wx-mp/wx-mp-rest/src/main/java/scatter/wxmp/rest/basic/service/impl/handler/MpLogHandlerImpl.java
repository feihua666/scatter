package scatter.wxmp.rest.basic.service.impl.handler;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Service;
import scatter.wxmp.rest.basic.service.handler.IMpLogHandler;

import java.util.Map;

/**
 * 只是一个打印日志handler
 * Created by yangwei
 * Created at 2020/7/21 18:40
 */
@Slf4j
@Service
public class MpLogHandlerImpl implements IMpLogHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
        log.info("wxMessage={},wxMessage={}", JSONUtil.toJsonStr(wxMessage),context);
        return null;
    }
}
