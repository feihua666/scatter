package scatter.wxmp.rest.basic.mvc;

import scatter.common.rest.controller.SuperController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.service.IWxMpUserService;

import javax.validation.Valid;

/**
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@Slf4j
@RestController
@RequestMapping("/wx/mp/oauth2/{appId}")
@Api(tags = "公众号oauth2相关接口")
public class MpOAuth2Controller extends SuperController {

    @Autowired
    private WxMpService wxMpService;
    @Autowired
    private IWxMpUserService iMpUserService;
    @Autowired
    private MpProperties mpProperties;

    @ApiOperation("构造网页授权url")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/oauth2buildAuthorizationUrl")
    public String oauth2buildAuthorizationUrl(@PathVariable String appId,
                       @Valid @RequestParam String url) {
        wxMpService.switchover(appId);
        String rUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url, WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
        return  rUrl;
    }
}
