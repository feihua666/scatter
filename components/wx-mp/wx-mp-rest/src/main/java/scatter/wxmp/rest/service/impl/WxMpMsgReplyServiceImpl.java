package scatter.wxmp.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.common.rest.validation.DictService;
import scatter.wxmp.pojo.form.WxMpMsgReplyAddForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyPageQueryForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyUpdateForm;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.mapper.WxMpMsgReplyMapper;
import scatter.wxmp.rest.service.IWxMpMsgReplyService;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * <p>
 * 公众号消息回复表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Service
@Transactional
public class WxMpMsgReplyServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<WxMpMsgReplyMapper, WxMpMsgReply, WxMpMsgReplyAddForm, WxMpMsgReplyUpdateForm, WxMpMsgReplyPageQueryForm> implements IWxMpMsgReplyService {

    @Autowired
    private DictService dictService;

    @Autowired
    private MpProperties mpProperties;

    @Override
    public void preAdd(WxMpMsgReplyAddForm addForm,WxMpMsgReply po) {
        if (!isStrEmpty(po.getAppCode())) {
            po.setAppName(mpProperties.abtainAppNameByAppCodeOrAppId(po.getAppCode()));
        }
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(WxMpMsgReplyUpdateForm updateForm,WxMpMsgReply po) {
        if (!isStrEmpty(po.getAppCode())) {
            po.setAppName(mpProperties.abtainAppNameByAppCodeOrAppId(po.getAppCode()));
        }
        super.preUpdate(updateForm,po);

    }


    @Override
    public WxMpMsgReply getMatched(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) {
        String appId = WxMpConfigStorageHolder.get();
        Assert.hasText(appId,"appId未获取到，这可能是一个bug");
        MpProperties.MpConfig mpConfig = mpProperties.abtainConfigByAppCodeOrAppId(appId);
        Assert.notNull(mpConfig,"未获取到公众号配置，请检查配置");
        Assert.hasText(mpConfig.getAppCode(),"appCode必须配置，appCode用来唯一标识一个公众号配置");
        WxMpMsgReply mpMsgReplyQuery = new WxMpMsgReply();
        mpMsgReplyQuery.setAppCode(mpConfig.getAppCode());
        QueryWrapper<WxMpMsgReply> queryWrapper = Wrappers.query(mpMsgReplyQuery).and(mpMsgReplyQueryWrapper -> {
            mpMsgReplyQueryWrapper.lambda().isNull(WxMpMsgReply::getMatchFromUser);
        });
        // 接收人
        if(isStrEmpty(wxMessage.getFromUser())){
            queryWrapper.lambda().isNull(WxMpMsgReply::getMatchFromUser);
        }else {
            queryWrapper.and(qw-> {
                qw.lambda().isNull(WxMpMsgReply::getMatchFromUser).or().eq(WxMpMsgReply::getMatchFromUser, wxMessage.getFromUser());
            });
        }
        // xml消息类型
        if(isStrEmpty(wxMessage.getMsgType())){
            queryWrapper.lambda().isNull(WxMpMsgReply::getMatchMsgTypeDictId);
        }else {
            String msgType = WxMpMsgReply.WX_MP_PUSH_XML_MSG_TYPE + wxMessage.getMsgType();
            String dictId = dictService.getIdByGroupCodeAndValue(WxMpMsgReply.XmlMsgTypeDictGroup.wx_mp_push_xml_msg_type.groupCode(),msgType.toLowerCase());
            // 没有字典，直接返回空
            if (dictId == null) {
                return null;
            }
            queryWrapper.and(qw-> {
                qw.lambda().isNull(WxMpMsgReply::getMatchMsgTypeDictId).or().eq(WxMpMsgReply::getMatchMsgTypeDictId, dictId);
            });
        }
        // 事件类型
        if(isStrEmpty(wxMessage.getEvent())){
            queryWrapper.lambda().isNull(WxMpMsgReply::getMatchEventTypeDictId);
        }else {
            String msgType = WxMpMsgReply.EVENT_TYPE_DICT_ITEM_PREFIX + wxMessage.getEvent();
            String dictId = dictService.getIdByGroupCodeAndValue(WxMpMsgReply.EventTypeDictGroup.wx_mp_push_event_type.groupCode(),msgType.toLowerCase());
            // 没有字典，直接返回空
            if (dictId == null) {
                return null;
            }
            queryWrapper.lambda().eq(WxMpMsgReply::getMatchEventTypeDictId, dictId);
        }
        // 按权重优先级和更新时间倒序排序
        queryWrapper.lambda().orderByDesc(WxMpMsgReply::getPriority).orderByDesc(WxMpMsgReply::getUpdateAt);

        List<WxMpMsgReply> list = list(queryWrapper);

        for (WxMpMsgReply mpMsgReply : list) {
            if(test(wxMessage,mpMsgReply)){
                return mpMsgReply;
            }
        }
        return null;
    }

    /**
     * 判断是否匹配
     * @param wxMessage
     * @param mpMsgReply
     * @return
     */
    protected boolean test(WxMpXmlMessage wxMessage,WxMpMsgReply mpMsgReply) {
        return
                (mpMsgReply.getMatchFromUser() == null || mpMsgReply.getMatchFromUser().equals(wxMessage.getFromUser()))
                        &&
                        (mpMsgReply.getMatchMsgTypeDictId() == null || (WxMpMsgReply.WX_MP_PUSH_XML_MSG_TYPE +wxMessage.getMsgType()).equalsIgnoreCase(dictService.getValueById(mpMsgReply.getMatchMsgTypeDictId())))
                        &&
                        (mpMsgReply.getMatchEventTypeDictId() == null || (WxMpMsgReply.EVENT_TYPE_DICT_ITEM_PREFIX +wxMessage.getEvent()).equalsIgnoreCase(dictService.getValueById(mpMsgReply.getMatchEventTypeDictId())))
                        &&
                        (mpMsgReply.getMatchEventKey() == null || matchType(mpMsgReply.getMatchEventKeyMatchDictId(),mpMsgReply.getMatchEventKey(),wxMessage.getEventKey()))
                        &&
                        (mpMsgReply.getMatchContent() == null || matchType(mpMsgReply.getMatchContentMatchDictId(),mpMsgReply.getMatchContent(), StringUtils.trimToNull(wxMessage.getContent())))

                ;
    }

    /**
     * 根据匹配方式匹配
     * @param matchTypeDictId
     * @param matchRuleValue
     * @param originValue
     * @return
     */
    private boolean matchType(String matchTypeDictId,String matchRuleValue,String originValue){
        String dictValue = dictService.getValueById(matchTypeDictId);
        // 相等
        if (PublicDictEnums.DictMatchType.match_eq.itemValue().equals(dictValue)) {
            return matchRuleValue.equalsIgnoreCase(originValue);
        }
        // 模糊
        if (PublicDictEnums.DictMatchType.match_like.itemValue().equals(dictValue)) {
            if (originValue != null) {
                return originValue.contains(matchRuleValue);
            }
        }
        // 左模糊匹配
        if (PublicDictEnums.DictMatchType.match_like_left.itemValue().equals(dictValue)) {
            if (originValue != null) {
                return originValue.startsWith(matchRuleValue);
            }
        }
        // 右模糊匹配
        if (PublicDictEnums.DictMatchType.match_like_right.itemValue().equals(dictValue)) {
            if (originValue != null) {
                return originValue.endsWith(matchRuleValue);
            }
        }
        // 正则匹配
        if (PublicDictEnums.DictMatchType.match_regex.itemValue().equals(dictValue)) {
            return Pattern.matches(matchRuleValue, StringUtils.trimToEmpty(originValue));
        }
        return false;
    }
}
