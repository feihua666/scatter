package scatter.wxmp.rest.notify;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.rest.notify.NotifyParam;
import scatter.wxmp.rest.basic.config.MpProperties;

/**
 * <p>
 * 默认的公众号appid获取实现
 * </p>
 *
 * @author yangwei
 * @since 2021-09-02 19:51
 */
public class DefaultWxMpNotifyAppIdResolverImpl implements IWxMpNotifyAppIdResolver{

	@Autowired
	private MpProperties mpProperties;

	@Override
	public String resolve(NotifyParam param) {
		if (mpProperties.getConfigs() == null) {
			return null;
		}
		if (mpProperties.getConfigs().size() == 1) {
			return mpProperties.getConfigs().get(0).getAppId();
		}

		return null;
	}
}
