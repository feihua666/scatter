package scatter.wxmp.rest.service.impl;

import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.common.rest.validation.DictService;
import scatter.wxmp.pojo.form.WxMpUserAddForm;
import scatter.wxmp.pojo.form.WxMpUserPageQueryForm;
import scatter.wxmp.pojo.form.WxMpUserUpdateForm;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.componentext.WxMpCityIdResolveDto;
import scatter.wxmp.rest.componentext.WxMpCityIdResolveParam;
import scatter.wxmp.rest.componentext.WxMpCityIdResolver;
import scatter.wxmp.rest.mapper.WxMpUserMapper;
import scatter.wxmp.rest.service.IWxMpUserService;

import java.util.Optional;

/**
 * <p>
 * 公众号微信用户表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Service
@Transactional
public class WxMpUserServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<WxMpUserMapper, WxMpUser, WxMpUserAddForm, WxMpUserUpdateForm, WxMpUserPageQueryForm> implements IWxMpUserService {

    @Autowired
    private DictService dictService;
    @Autowired
    private WxMpCityIdResolver wxMpCityIdResolver;

    @Override
    public void preAdd(WxMpUserAddForm addForm,WxMpUser po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(WxMpUserUpdateForm updateForm,WxMpUser po) {
        super.preUpdate(updateForm,po);

    }



    @Override
    public WxMpUser insert(me.chanjar.weixin.mp.bean.result.WxMpUser  wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser.DictSourceItem dictHowFrom) {

        WxMpUser mpUserForInsert = new WxMpUser();
        mpUserForInsert = map(wxMpUser, mpUserForInsert, mpConfig);

        mpUserForInsert.setSourceDictId(dictService.getIdByGroupCodeAndValue(WxMpUser.DictSourceGroup.wx_mp_user_how_from.groupCode(),dictHowFrom.itemValue()));

        WxMpCityIdResolveParam wxMpCityIdResolveParam = new WxMpCityIdResolveParam();
        wxMpCityIdResolveParam.setProvinceName(mpUserForInsert.getProvinceName());
        wxMpCityIdResolveParam.setCityName(mpUserForInsert.getCityName());

        WxMpCityIdResolveDto resolve = wxMpCityIdResolver.resolve(wxMpCityIdResolveParam);
        mpUserForInsert.setCountryAreaId(resolve.getCountryId());
        mpUserForInsert.setProvinceAreaId(resolve.getProvinceId());
        mpUserForInsert.setCityAreaId(resolve.getCityId());

        // 用户来源
        save(mpUserForInsert);
        return mpUserForInsert;
    }

    @Override
    public WxMpUser insert(WxOAuth2UserInfo wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser.DictSourceItem sourceItem) {
        me.chanjar.weixin.mp.bean.result.WxMpUser wxMpUserR = map(wxMpUser);
        return insert(wxMpUserR,mpConfig,sourceItem);
    }

    @Override
    public WxMpUser updateById(me.chanjar.weixin.mp.bean.result.WxMpUser wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser mpUser) {
        Assert.notNull(mpUser,"mpUser不能为空");
        Assert.hasText(mpUser.getId(),"mpUser.id不能为空");
        mpUser = map(wxMpUser, mpUser, mpConfig);

        WxMpCityIdResolveParam wxMpCityIdResolveParam = new WxMpCityIdResolveParam();
        wxMpCityIdResolveParam.setProvinceName(mpUser.getProvinceName());
        wxMpCityIdResolveParam.setCityName(mpUser.getCityName());

        WxMpCityIdResolveDto resolve = wxMpCityIdResolver.resolve(wxMpCityIdResolveParam);
        mpUser.setCountryAreaId(resolve.getCountryId());
        mpUser.setProvinceAreaId(resolve.getProvinceId());
        mpUser.setCityAreaId(resolve.getCityId());
        updateById(mpUser);
        return mpUser;
    }

    @Override
    public WxMpUser updateById(WxOAuth2UserInfo wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser mpUser) {
        me.chanjar.weixin.mp.bean.result.WxMpUser wxMpUserR = map(wxMpUser);
        return updateById(wxMpUserR,mpConfig,mpUser);
    }

    /**
     *
     * 实体转换
     * @param wxMpUser
     * @return
     */
    private me.chanjar.weixin.mp.bean.result.WxMpUser map(WxOAuth2UserInfo wxMpUser) {
        me.chanjar.weixin.mp.bean.result.WxMpUser wxMpUserR = new me.chanjar.weixin.mp.bean.result.WxMpUser();
        wxMpUserR.setOpenId(wxMpUser.getOpenid());
        wxMpUserR.setNickname(wxMpUser.getNickname());
        // 省市区性别已不再支持
        wxMpUserR.setHeadImgUrl(wxMpUser.getHeadImgUrl());
        wxMpUserR.setUnionId(wxMpUser.getUnionId());
        wxMpUserR.setPrivileges(wxMpUser.getPrivileges());

        return wxMpUserR;
    }

    /**
     * 实体转换
     * @param wxMpUser
     * @param mpUser
     * @param mpConfig
     * @return
     */
    private WxMpUser map(me.chanjar.weixin.mp.bean.result.WxMpUser  wxMpUser,WxMpUser mpUser, MpProperties.MpConfig mpConfig) {
        mpUser.setAppCode(mpConfig.getAppCode());
        mpUser.setAppName(mpConfig.getAppName());

        mpUser.setIsSubscribe(Optional.ofNullable(wxMpUser.getSubscribe()).orElse(false));
        mpUser.setOpenId(wxMpUser.getOpenId());


        mpUser.setAvatar(wxMpUser.getHeadImgUrl());
        mpUser.setNickname(wxMpUser.getNickname());
        //省市区性别已不再支持

        mpUser.setUnionId(wxMpUser.getUnionId());

        return mpUser;
    }
}
