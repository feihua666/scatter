package scatter.wxmp.rest.basic.service.impl.handler;


import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.basic.service.handler.IMpUnsubscribeHandler;
import scatter.wxmp.rest.service.IWxMpUserService;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2020/7/21 18:44
 */
@Service
public class MpUnsubscribeHandlerImpl implements IMpUnsubscribeHandler {

    @Autowired
    private IWxMpUserService iMpUserService;
    @Autowired
    private MpProperties mpProperties;
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
        MpProperties.MpConfig mpConfig = mpProperties.abtainConfigByAppCodeOrAppId(WxMpConfigStorageHolder.get());
        WxMpUser byOpenidAndAppCode = iMpUserService.getByOpenidAndAppCode(wxMessage.getFromUser(), mpConfig.getAppCode());
        if (byOpenidAndAppCode != null) {
            WxMpUser mpUser = new  WxMpUser();
            mpUser.setId(byOpenidAndAppCode.getId());
            mpUser.setIsSubscribe(false);
            iMpUserService.updateById(mpUser);
        }
        return null;
    }
}
