package scatter.wxmp.rest.basic.config;

import me.chanjar.weixin.mp.api.WxMpMessageRouter;

/**
 * Created by yangwei
 * Created at 2020/7/22 11:33
 */
public interface IMpHandlerConfigure {

    public void configure(WxMpMessageRouter newRouter);
}
