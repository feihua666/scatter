package scatter.wxmp.rest.service;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import scatter.common.rest.service.IBaseService;
import scatter.wxmp.pojo.po.WxMpMsgReply;

import java.util.Map;

/**
 * <p>
 * 公众号消息回复表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
public interface IWxMpMsgReplyService extends IBaseService<WxMpMsgReply> {

    /**
     * 获取匹配的消息回复内容
     * @param wxMessage
     * @param context
     * @param wxMpService
     * @param sessionManager
     * @return
     */
    public WxMpMsgReply getMatched(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager);
}
