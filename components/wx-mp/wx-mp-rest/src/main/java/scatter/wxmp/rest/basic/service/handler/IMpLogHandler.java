package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 用来打印日志的handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpLogHandler extends WxMpMessageHandler {
}
