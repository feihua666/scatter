package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 客服接入会话handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpKfCreateSessionHandler extends WxMpMessageHandler {
}
