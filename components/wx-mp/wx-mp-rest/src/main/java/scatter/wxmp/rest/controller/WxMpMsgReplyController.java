package scatter.wxmp.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxmp.rest.WxMpConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.pojo.vo.WxMpMsgReplyVo;
import scatter.wxmp.pojo.form.WxMpMsgReplyAddForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyUpdateForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 公众号消息回复表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Api(tags = "公众号消息回复相关接口")
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH + "/wx-mp-msg-reply")
public class WxMpMsgReplyController extends BaseAddUpdateQueryFormController<WxMpMsgReply, WxMpMsgReplyVo, WxMpMsgReplyAddForm, WxMpMsgReplyUpdateForm, WxMpMsgReplyPageQueryForm> {


     @Override
	 @ApiOperation("添加公众号消息回复")
     @PreAuthorize("hasAuthority('WxMpMsgReply:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public WxMpMsgReplyVo add(@RequestBody @Valid WxMpMsgReplyAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询公众号消息回复")
     @PreAuthorize("hasAuthority('WxMpMsgReply:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public WxMpMsgReplyVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除公众号消息回复")
     @PreAuthorize("hasAuthority('WxMpMsgReply:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新公众号消息回复")
     @PreAuthorize("hasAuthority('WxMpMsgReply:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public WxMpMsgReplyVo update(@RequestBody @Valid WxMpMsgReplyUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询公众号消息回复")
    @PreAuthorize("hasAuthority('WxMpMsgReply:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<WxMpMsgReplyVo> getList(WxMpMsgReplyPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询公众号消息回复")
    @PreAuthorize("hasAuthority('WxMpMsgReply:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<WxMpMsgReplyVo> getPage(WxMpMsgReplyPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
