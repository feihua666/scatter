package scatter.wxmp.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxmp.rest.WxMpConfiguration;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.rest.service.IWxMpMsgReplyService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 公众号消息回复表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH + "/inner/wx-mp-msg-reply")
public class WxMpMsgReplyInnerController extends BaseInnerController<WxMpMsgReply> {
 @Autowired
 private IWxMpMsgReplyService wxMpMsgReplyService;

 public IWxMpMsgReplyService getService(){
     return wxMpMsgReplyService;
 }
}
