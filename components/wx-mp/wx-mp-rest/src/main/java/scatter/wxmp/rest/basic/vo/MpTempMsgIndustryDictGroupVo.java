package scatter.wxmp.rest.basic.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseVo;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2020/8/19 11:33
 */
@Setter
@Getter
public class MpTempMsgIndustryDictGroupVo extends BaseVo {
    private String name;

    private List<MpTempMsgIndustryDictItemVo> items;

    @Data
    public static class MpTempMsgIndustryDictItemVo{
        private String id;
        private String name;
    }
}
