package scatter.wxmp.rest.basic.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import scatter.common.pojo.vo.BaseVo;

/**
 * Created by yangwei
 * Created at 2020/8/19 14:46
 */
@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel("模板消息行业数据响应对象")
public class WxMpTemplateIndustryVo extends BaseVo {


    @ApiModelProperty(value = "主行业编码")
    private Integer primaryIndustry;

    @ApiModelProperty(value = "主行业名称")
    private String primaryIndustryName;

    @ApiModelProperty(value = "主行业类别")
    private String primaryIndustryClass;

    @ApiModelProperty(value = "副行业编码")
    private Integer secondIndustry;

    @ApiModelProperty(value = "副行业名称")
    private String secondIndustryName;

    @ApiModelProperty(value = "副行业类别")
    private String secondIndustryClass;
}
