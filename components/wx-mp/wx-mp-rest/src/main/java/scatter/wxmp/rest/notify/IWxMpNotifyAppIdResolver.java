package scatter.wxmp.rest.notify;

import scatter.common.rest.notify.NotifyParam;

/**
 * <p>
 * 微信公众号appid解决方案提供服务
 * </p>
 *
 * @author yangwei
 * @since 2021-09-02 19:51
 */
public interface IWxMpNotifyAppIdResolver {

	/**
	 * 根据参数提供 String
	 * @param param
	 * @return
	 */
	public String resolve(NotifyParam param);
}
