package scatter.wxmp.rest.basic.config;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.wxmp.rest.basic.service.handler.*;

/**
 * Created by yangwei
 * Created at 2020/7/22 11:44
 */
@Component
public class DefaultMpHandlerConfigure implements IMpHandlerConfigure {

    @Autowired
    private IMpDefaultHandler iMpDefaultHandler;
    @Autowired
    private IMpLogHandler iMpLogHandler;
    @Autowired
    private IMpSubscribeHandler iMpSubscribeHandler;
    @Autowired
    private IMpUnsubscribeHandler iMpUnsubscribeHandler;
    @Autowired
    private IMpReportLocationHandler iMpReportLocationHandler;
    @Override
    public void configure(WxMpMessageRouter newRouter) {
        // 记录所有事件的日志 （异步执行）
        newRouter.rule().handler(this.iMpLogHandler).next();

        // 关注事件 （异步执行）
        newRouter.rule().async(false).msgType(WxConsts.XmlMsgType.EVENT).event(WxConsts.EventType.SUBSCRIBE)
                .handler(this.iMpSubscribeHandler).next();

        // 取消关注事件 （异步执行）
        newRouter.rule().async(false).msgType(WxConsts.XmlMsgType.EVENT).event(WxConsts.EventType.UNSUBSCRIBE)
                .handler(this.iMpUnsubscribeHandler).next();

        // 上报地理位置事件（异步执行）
        newRouter.rule().async(false).msgType(WxConsts.XmlMsgType.EVENT).event(WxConsts.EventType.LOCATION)
                .handler(this.iMpReportLocationHandler).next();

        // 默认
        newRouter.rule().async(false).handler(this.iMpDefaultHandler).end();
    }
}
