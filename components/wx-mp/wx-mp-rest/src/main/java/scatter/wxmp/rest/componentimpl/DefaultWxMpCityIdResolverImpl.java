package scatter.wxmp.rest.componentimpl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.area.pojo.dto.AreaIdResolveDto;
import scatter.area.pojo.param.AreaIdResolveParam;
import scatter.area.rest.service.IAreaService;
import scatter.wxmp.rest.componentext.WxMpCityIdResolveDto;
import scatter.wxmp.rest.componentext.WxMpCityIdResolver;
import scatter.wxmp.rest.componentext.WxMpCityIdResolveParam;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 可选配置
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 13:39
 */
public class DefaultWxMpCityIdResolverImpl implements WxMpCityIdResolver {

	private static Map<String,String> cacheMap = new HashMap<>();


	@Autowired
	private IAreaService iAreaService;

	@Override
	public WxMpCityIdResolveDto resolve(WxMpCityIdResolveParam param) {

		AreaIdResolveParam areaIdResolveParam = new AreaIdResolveParam();
		areaIdResolveParam.setProvinceName(param.getProvinceName());
		areaIdResolveParam.setCityName(param.getCityName());
		areaIdResolveParam.setDistrictName(param.getDistrictName());
		AreaIdResolveDto areaIdResolveDto = iAreaService.resolveName(areaIdResolveParam);

		WxMpCityIdResolveDto wxCityDto = new WxMpCityIdResolveDto();
		wxCityDto.setCountryId(areaIdResolveDto.getCountryId());
		wxCityDto.setProvinceId(areaIdResolveDto.getProvinceId());
		wxCityDto.setCityId(areaIdResolveDto.getCityId());
		wxCityDto.setDistrictId(areaIdResolveDto.getDistrictId());


		return wxCityDto;
	}


}
