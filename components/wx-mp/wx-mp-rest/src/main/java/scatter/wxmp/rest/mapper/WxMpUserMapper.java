package scatter.wxmp.rest.mapper;

import scatter.wxmp.pojo.po.WxMpUser;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 公众号微信用户表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
public interface WxMpUserMapper extends IBaseMapper<WxMpUser> {

}
