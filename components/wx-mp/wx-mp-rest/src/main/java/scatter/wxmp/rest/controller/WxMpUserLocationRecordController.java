package scatter.wxmp.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxmp.rest.WxMpConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.wxmp.pojo.vo.WxMpUserLocationRecordVo;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordAddForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordUpdateForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 公众号微信用户位置记录表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Api(tags = "公众号微信用户位置记录相关接口")
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH + "/wx-mp-user-location-record")
public class WxMpUserLocationRecordController extends BaseAddUpdateQueryFormController<WxMpUserLocationRecord, WxMpUserLocationRecordVo, WxMpUserLocationRecordAddForm, WxMpUserLocationRecordUpdateForm, WxMpUserLocationRecordPageQueryForm> {


     @Override
	 @ApiOperation("添加公众号微信用户位置记录")
     @PreAuthorize("hasAuthority('WxMpUserLocationRecord:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public WxMpUserLocationRecordVo add(@RequestBody @Valid WxMpUserLocationRecordAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询公众号微信用户位置记录")
     @PreAuthorize("hasAuthority('WxMpUserLocationRecord:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public WxMpUserLocationRecordVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除公众号微信用户位置记录")
     @PreAuthorize("hasAuthority('WxMpUserLocationRecord:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新公众号微信用户位置记录")
     @PreAuthorize("hasAuthority('WxMpUserLocationRecord:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public WxMpUserLocationRecordVo update(@RequestBody @Valid WxMpUserLocationRecordUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询公众号微信用户位置记录")
    @PreAuthorize("hasAuthority('WxMpUserLocationRecord:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<WxMpUserLocationRecordVo> getList(WxMpUserLocationRecordPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询公众号微信用户位置记录")
    @PreAuthorize("hasAuthority('WxMpUserLocationRecord:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<WxMpUserLocationRecordVo> getPage(WxMpUserLocationRecordPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
