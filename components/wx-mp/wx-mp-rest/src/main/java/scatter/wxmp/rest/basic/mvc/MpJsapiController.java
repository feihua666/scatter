package scatter.wxmp.rest.basic.mvc;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.wxmp.rest.WxMpConfiguration;

/**
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH +"/wx/mp/config/{appId}")
@Api(tags = "公众号配置相关接口")
public class MpJsapiController extends SuperController {

    @Autowired
    private WxMpService wxMpService;

    @ApiOperation("js签名配置")
    @GetMapping("/jsapiSignature")
    @ResponseStatus(HttpStatus.OK)
    public WxJsapiSignature getJsapiSignature(@PathVariable String appId, String url) throws WxErrorException {
        wxMpService.switchover(appId);
        final WxJsapiSignature jsapiSignature = wxMpService.createJsapiSignature(url);
        return jsapiSignature;
    }

}
