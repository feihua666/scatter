package scatter.wxmp.rest.basic.mapstruct;


import me.chanjar.weixin.mp.bean.material.WxMpMaterial;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.wxmp.rest.basic.form.WxMpMaterialForm;

/**
 * Created by yangwei
 * Created at 2020/7/22 12:41
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MpMaterialWebMapper  {
    MpMaterialWebMapper INSTANCE = Mappers.getMapper( MpMaterialWebMapper.class );

    WxMpMaterial map(WxMpMaterialForm form);
}
