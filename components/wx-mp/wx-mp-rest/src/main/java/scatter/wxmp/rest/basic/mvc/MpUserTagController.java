package scatter.wxmp.rest.basic.mvc;

import scatter.common.rest.controller.SuperController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@RestController
@RequestMapping("/wx/mp/usertag/{appId}")
@Api(tags = "公众号用户标签相关接口")
public class MpUserTagController extends SuperController {

    @Autowired
    private WxMpService wxMpService;


    /**
     * 用户标签创建接口
     * @param appId
     * @param tagName
     * @return
     * @throws WxErrorException
     */
    @ApiOperation("用户标签创建接口")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('wx:mp:usertag:create')")
    @PostMapping("/create")
    public WxUserTag tagCreate(@PathVariable String appId, @RequestParam String tagName) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getUserTagService().tagCreate(tagName);
    }

    /**
     * 用户标签修改接口
     * @param appId
     * @param tagName
     * @return
     * @throws WxErrorException
     */
    @ApiOperation("用户标签修改接口")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('wx:mp:usertag:update')")
    @PutMapping("/update/{tagId}")
    public Boolean tagUpdate(@PathVariable String appId, @PathVariable Long tagId, @RequestParam String tagName) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getUserTagService().tagUpdate(tagId,tagName);
    }

    /**
     * 用户标签删除接口
     * @param appId
     * @return
     * @throws WxErrorException
     */
    @ApiOperation("用户标签删除接口")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('wx:mp:usertag:delete')")
    @DeleteMapping("/delete/{tagId}")
    public Boolean tagDelete(@PathVariable String appId, @PathVariable Long tagId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getUserTagService().tagDelete(tagId);
    }

    /**
     * 获取公众号已创建的标签接口
     * @param appId
     * @return
     * @throws WxErrorException
     */
    @ApiOperation("获取公众号已创建的标签接口")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('wx:mp:usertag:List')")
    @GetMapping("/list")
    public List<WxUserTag> tagList(@PathVariable String appId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getUserTagService().tagGet();
    }
}
