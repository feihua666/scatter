package scatter.wxmp.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxmp.rest.WxMpConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.pojo.vo.WxMpUserVo;
import scatter.wxmp.pojo.form.WxMpUserAddForm;
import scatter.wxmp.pojo.form.WxMpUserUpdateForm;
import scatter.wxmp.pojo.form.WxMpUserPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 公众号微信用户表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Api(tags = "公众号微信用户相关接口")
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH + "/wx-mp-user")
public class WxMpUserController extends BaseAddUpdateQueryFormController<WxMpUser, WxMpUserVo, WxMpUserAddForm, WxMpUserUpdateForm, WxMpUserPageQueryForm> {

     @Override
	 @ApiOperation("根据ID查询公众号微信用户")
     @PreAuthorize("hasAuthority('WxMpUser:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public WxMpUserVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除公众号微信用户")
     @PreAuthorize("hasAuthority('WxMpUser:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

    @Override
	@ApiOperation("不分页查询公众号微信用户")
    @PreAuthorize("hasAuthority('WxMpUser:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<WxMpUserVo> getList(WxMpUserPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询公众号微信用户")
    @PreAuthorize("hasAuthority('WxMpUser:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<WxMpUserVo> getPage(WxMpUserPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
