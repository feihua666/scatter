package scatter.wxmp.rest.basic.mvc;

import cn.hutool.json.JSONUtil;
import org.springframework.http.MediaType;
import scatter.common.rest.controller.SuperController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.menu.WxMpGetSelfMenuInfoResult;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@RestController
@RequestMapping("/wx/mp/menu/{appId}")
@Api(tags = "公众号自定义菜单相关接口")
public class MpMenuController extends SuperController {

    @Autowired
    private WxMpService wxMpService;


    /**
     * <pre>
     * 自定义菜单创建接口
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
     * 如果要创建个性化菜单，请设置matchrule属性
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @return 如果是个性化菜单，则返回menuid，否则返回null
     */
    @ApiOperation("自定义菜单创建接口")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('wx:mp:menu:create')")
    @PostMapping("/create")
    public String menuCreate(@PathVariable String appId, @RequestBody WxMenu menu) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMenuService().menuCreate(menu);
    }


    /**
     * <pre>
     * 自定义菜单创建接口
     * 详情请见： https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
     * 如果要创建个性化菜单，请设置matchrule属性
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @return 如果是个性化菜单，则返回menuid，否则返回null
     */
    @ApiOperation("自定义菜单创建接口json方式")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('wx:mp:menu:create')")
    @PostMapping(value = "/createByJsonStr",consumes = MediaType.TEXT_PLAIN_VALUE)
    public String menuCreate(@PathVariable String appId, @RequestBody String json) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMenuService().menuCreate(json);
    }

    /**
     * <pre>
     * 自定义菜单删除接口
     * 详情请见: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141015&token=&lang=zh_CN
     * </pre>
     */
    @ApiOperation("自定义菜单删除接口")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('wx:mp:menu:delete')")
    @DeleteMapping("/delete")
    public void menuDelete(@PathVariable String appId) throws WxErrorException {
        this.wxMpService.switchoverTo(appId).getMenuService().menuDelete();
    }

    /**
     * <pre>
     * 删除个性化菜单接口
     * 详情请见: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @param menuId 个性化菜单的menuid
     */
    @ApiOperation("删除个性化菜单接口")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('wx:mp:menu:delete')")
    @DeleteMapping("/delete/{menuId}")
    public void menuDelete(@PathVariable String appId, @PathVariable String menuId) throws WxErrorException {
        this.wxMpService.switchoverTo(appId).getMenuService().menuDelete(menuId);
    }

    /**
     * <pre>
     * 自定义个性化菜单查询接口
     * 详情请见： https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Getting_Custom_Menu_Configurations.html
     * </pre>
     */
    @ApiOperation("自定义个性化菜单查询接口")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/get")
    public WxMpMenu menuGet(@PathVariable String appId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMenuService().menuGet();
    }
    @ApiOperation("自定义个性化菜单查询接口官网,返回数据文档参考官网")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getOrigin")
    public Map getOrigin(@PathVariable String appId) throws WxErrorException {
        WxMpMenu wxMpMenu = menuGet(appId);
        return wxMpMenu == null ? null : JSONUtil.toBean(wxMpMenu.toString(), Map.class);
    }
    /**
     * <pre>
     * 测试个性化菜单匹配结果
     * 详情请见: http://mp.weixin.qq.com/wiki/0/c48ccd12b69ae023159b4bfaa7c39c20.html
     * </pre>
     *
     * @param userId 可以是粉丝的OpenID，也可以是粉丝的微信号。
     */
    @ApiOperation("测试个性化菜单匹配结果接口")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/menuTryMatch/{userId}")
    public WxMenu menuTryMatch(@PathVariable String appId, @PathVariable String userId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMenuService().menuTryMatch(userId);
    }

    /**
     * <pre>
     * 获取自定义菜单配置接口
     * 本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
     * 请注意：
     * 1、第三方平台开发者可以通过本接口，在旗下公众号将业务授权给你后，立即通过本接口检测公众号的自定义菜单配置，并通过接口再次给公众号设置好自动回复规则，以提升公众号运营者的业务体验。
     * 2、本接口与自定义菜单查询接口的不同之处在于，本接口无论公众号的接口是如何设置的，都能查询到接口，而自定义菜单查询接口则仅能查询到使用API设置的菜单配置。
     * 3、认证/未认证的服务号/订阅号，以及接口测试号，均拥有该接口权限。
     * 4、从第三方平台的公众号登录授权机制上来说，该接口从属于消息与菜单权限集。
     * 5、本接口中返回的图片/语音/视频为临时素材（临时素材每次获取都不同，3天内有效，通过素材管理-获取临时素材接口来获取这些素材），本接口返回的图文消息为永久素材素材（通过素材管理-获取永久素材接口来获取这些素材）。
     *  接口调用请求说明:
     * http请求方式: GET（请使用https协议）
     * https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN
     * </pre>
     */
    @ApiOperation("获取自定义菜单配置接口")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getSelfMenuInfo")
    public WxMpGetSelfMenuInfoResult getSelfMenuInfo(@PathVariable String appId) throws WxErrorException {
        return this.wxMpService.switchoverTo(appId).getMenuService().getSelfMenuInfo();
    }
    @ApiOperation("获取自定义菜单配置接口官网,返回数据文档参考官网")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getSelfMenuInfoOrigin")
    public Map getSelfMenuInfoOrigin(@PathVariable String appId) throws WxErrorException {
        WxMpGetSelfMenuInfoResult selfMenuInfo = getSelfMenuInfo(appId);

        return selfMenuInfo == null ? null : JSONUtil.toBean(selfMenuInfo.toString(), Map.class);
    }
}
