package scatter.wxmp.rest.basic.mvc;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.wxmp.rest.WxMpConfiguration;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.basic.mapstruct.MpConfigWebMapper;

/**
 * 公众号辅助相关接口
 * Created by yangwei
 * Created at 2020/7/20 20:48
 */
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH +"/wx/mp/helper")
@Api(tags = "公众号辅助相关接口")
public class MpHelperController extends SuperController {

    @Autowired
    private WxMpService wxMpService;
    @Autowired
    private MpProperties mpProperties;
    @Autowired
    private MpConfigWebMapper mpConfigWebMapper;

    /**
     * 公众号api调用次数清零接口
     * @return
     */
    @ApiOperation("公众号api调用次数清零接口")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/{appId}/clear/quota")
    public Boolean clearQuota(@PathVariable String appId) throws WxErrorException {
        this.wxMpService.switchoverTo(appId).clearQuota(this.wxMpService.switchoverTo(appId).getWxMpConfigStorage().getAppId());
        return true;
    }
}
