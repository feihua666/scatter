package scatter.wxmp.rest.basic.mapstruct;


import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.basic.vo.MpConfigVo;

/**
 * Created by yangwei
 * Created at 2020/7/22 12:41
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MpConfigWebMapper {
    MpConfigWebMapper INSTANCE = Mappers.getMapper( MpConfigWebMapper.class );
    MpConfigVo configToVo(MpProperties.MpConfig config);
}
