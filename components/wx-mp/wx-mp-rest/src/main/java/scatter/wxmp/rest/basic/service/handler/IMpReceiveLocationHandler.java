package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 地理位置接收地理位置消息handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpReceiveLocationHandler extends WxMpMessageHandler {
}
