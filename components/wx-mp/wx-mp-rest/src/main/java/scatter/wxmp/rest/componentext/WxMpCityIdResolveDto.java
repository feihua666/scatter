package scatter.wxmp.rest.componentext;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.dto.BaseDto;

/**
 * <p>
 * 返回的 scatter 的解决id，一般为area组件支持
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 13:32
 */
@Setter
@Getter
public class WxMpCityIdResolveDto extends BaseDto {
	/**
	 * 区id
	 */
	private String districtId;
	/**
	 * 城市id
	 */
	private String cityId;
	/**
	 * 省id
	 */
	private String provinceId;

	/**
	 * 国家id
	 */
	private String countryId;
}
