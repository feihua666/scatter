package scatter.wxmp.rest.basic.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotNull;

/**
 * Created by yangwei
 * Created at 2020/8/19 14:04
 */
@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel("模板消息行业设置表单")
public class WxMpTemplateIndustrySetForm extends BaseForm {

    @NotNull(message = "主行业编码不能为空")
    @ApiModelProperty(value = "主行业编码",required = true)
    private Integer primaryIndustry;

    @NotNull(message = "副行业编码不能为空")
    @ApiModelProperty(value = "副行业编码",required = true)
    private Integer secondIndustry;
}
