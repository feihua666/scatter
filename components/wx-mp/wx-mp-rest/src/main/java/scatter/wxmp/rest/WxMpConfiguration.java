package scatter.wxmp.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import scatter.area.rest.service.IAreaService;
import scatter.wxmp.rest.basic.config.MpConfiguration;
import scatter.wxmp.rest.componentimpl.DefaultWxMpCityIdResolverImpl;
import scatter.wxmp.rest.notify.DefaultWxMpNotifyAppIdResolverImpl;
import scatter.wxmp.rest.notify.DefaultWxMpTemplateNotifyListenerImpl;
import scatter.wxmp.rest.security.WxMpWebSecurityConfig;
import scatter.wxmp.rest.service.impl.DefaultMpMsgReplyParserImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@Import({MpConfiguration.class, WxMpWebSecurityConfig.class})
@MapperScan("scatter.wxmp.rest.**.mapper")
public class WxMpConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "wxmp";


    @Bean
    @ConditionalOnMissingBean
    public DefaultWxMpTemplateNotifyListenerImpl defaultWxMpTemplateNotifyListenerImpl(){
        return new DefaultWxMpTemplateNotifyListenerImpl();
    }
    @Bean
    @ConditionalOnMissingBean
    public DefaultWxMpNotifyAppIdResolverImpl defaultWxMpNotifyAppIdResolverImpl(){
        return new DefaultWxMpNotifyAppIdResolverImpl();
    }
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(IAreaService.class)
    public DefaultWxMpCityIdResolverImpl defaultWxMpCityIdResolver(){
        return new DefaultWxMpCityIdResolverImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public DefaultMpMsgReplyParserImpl iMpMsgReplyParser(){
        return new DefaultMpMsgReplyParserImpl();
    }
}
