package scatter.wxmp.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.rest.service.IWxMpUserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 公众号微信用户翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Component
public class WxMpUserTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IWxMpUserService wxMpUserService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,WxMpUser.TRANS_WXMPUSER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,WxMpUser.TRANS_WXMPUSER_BY_ID)) {
            WxMpUser byId = wxMpUserService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,WxMpUser.TRANS_WXMPUSER_BY_ID)) {
            return wxMpUserService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
