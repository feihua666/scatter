package scatter.wxmp.rest.basic.service.handler;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * 自定义菜单事件handler
 * Created by yangwei
 * Created at 2020/7/20 20:24
 */
public interface IMpEventMenuClickHandler extends WxMpMessageHandler {
}
