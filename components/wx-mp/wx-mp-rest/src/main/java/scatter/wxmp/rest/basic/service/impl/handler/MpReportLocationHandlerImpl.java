package scatter.wxmp.rest.basic.service.impl.handler;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.wxmp.rest.basic.config.MpProperties;
import scatter.wxmp.rest.basic.service.handler.IMpReportLocationHandler;
import scatter.wxmp.rest.service.IWxMpUserLocationRecordService;
import scatter.wxmp.rest.service.IWxMpUserService;

import java.util.Map;

/**
 * 上报地理位置事件
 * Created by yangwei
 * Created at 2020/7/21 18:40
 */
@Slf4j
@Service
public class MpReportLocationHandlerImpl implements IMpReportLocationHandler {
    @Autowired
    private IWxMpUserService iMpUserService;
    @Autowired
    private IWxMpUserLocationRecordService iMpUserLocationRecordService;
    @Autowired
    private MpProperties mpProperties;
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
        MpProperties.MpConfig mpConfig = mpProperties.abtainConfigByAppCodeOrAppId(WxMpConfigStorageHolder.get());
        WxMpUser byOpenidAndAppCode = iMpUserService.getByOpenidAndAppCode(wxMessage.getFromUser(), mpConfig.getAppCode());
        if (byOpenidAndAppCode != null) {
            WxMpUser mpUser = new WxMpUser();
            mpUser.setId(byOpenidAndAppCode.getId());
            mpUser.setLatitude(wxMessage.getLatitude().toString());
            mpUser.setLongitude(wxMessage.getLongitude().toString());
            mpUser.setPrecisions(wxMessage.getPrecision().toString());
            iMpUserService.updateById(mpUser);
            // 添加位置记录
            WxMpUserLocationRecord mpUserLocationRecord = new WxMpUserLocationRecord();
            mpUserLocationRecord.setWxUserId(byOpenidAndAppCode.getId());
            mpUserLocationRecord.setLatitude(wxMessage.getLatitude().toString());
            mpUserLocationRecord.setLongitude(wxMessage.getLongitude().toString());
            mpUserLocationRecord.setPrecisions(wxMessage.getPrecision().toString());
            iMpUserLocationRecordService.save(mpUserLocationRecord);
        }else {
            log.warn("用户上报地理位置，但未找到微信用户数据,wxMessage={}",wxMessage.toString());
        }
        return null;
    }
}
