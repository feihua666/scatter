package scatter.wxmp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wxmp.pojo.form.WxMpMsgReplyAddForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyPageQueryForm;
import scatter.wxmp.pojo.form.WxMpMsgReplyUpdateForm;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.pojo.vo.WxMpMsgReplyVo;

/**
 * <p>
 * 公众号消息回复 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxMpMsgReplyMapStruct extends IBaseVoMapStruct<WxMpMsgReply, WxMpMsgReplyVo>,
                                        IBaseAddFormMapStruct<WxMpMsgReply,WxMpMsgReplyAddForm>,
                                        IBaseUpdateFormMapStruct<WxMpMsgReply,WxMpMsgReplyUpdateForm>,
                                        IBaseQueryFormMapStruct<WxMpMsgReply,WxMpMsgReplyPageQueryForm> {
    WxMpMsgReplyMapStruct INSTANCE = Mappers.getMapper( WxMpMsgReplyMapStruct.class );

}
