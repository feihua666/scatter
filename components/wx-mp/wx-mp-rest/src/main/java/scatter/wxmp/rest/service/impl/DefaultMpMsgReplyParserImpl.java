package scatter.wxmp.rest.service.impl;


import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import scatter.common.rest.validation.DictService;
import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.wxmp.rest.componentext.IMpMsgReplyParser;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2020/7/23 20:56
 */
@Slf4j
@Order
public class DefaultMpMsgReplyParserImpl implements IMpMsgReplyParser {

    @Autowired
    private DictService dictService;

    @Override
    public boolean support(WxMpMsgReply reply) {
        return true;
    }

    @Override
    public WxMpXmlOutMessage parse(WxMpMsgReply reply, WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) {
        String valueById = dictService.getValueById(reply.getReplyMsgTypeDictId());
        if (StrUtil.isEmpty(valueById)) {
            return null;
        }
        String xmlMsgType = valueById.replace(WxMpMsgReply.WX_MP_PUSH_XML_MSG_TYPE, "");
        switch (xmlMsgType){
            case WxConsts.XmlMsgType.TEXT: {
                return WxMpXmlOutMessage.TEXT()
                        .content(reply.getReplyContent())
                        .fromUser(wxMessage.getToUser()).toUser(wxMessage.getFromUser())
                        .build();
            }
            default: {
                // 其它暂不支持
                log.warn("不受支持的消息类型 xmlMsgType={}",xmlMsgType);
            }
        }
        return null;
    }
}
