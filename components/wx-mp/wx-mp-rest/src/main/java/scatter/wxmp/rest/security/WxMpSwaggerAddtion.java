package scatter.wxmp.rest.security;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiDescriptionBuilder;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.ApiDescription;
import springfox.documentation.service.Operation;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.DocumentationContext;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/7 12:50
 */
@Component
public class WxMpSwaggerAddtion implements ApiListingScannerPlugin {
    @Override
    public List<ApiDescription> apply(DocumentationContext context) {
        return Arrays.asList(
                login()
        );
    }

    /**
     * 登录接口
     *
     * @return
     */
    private ApiDescription login() {
        ApiDescriptionBuilder apiDescriptionBuilder = new ApiDescriptionBuilder(Comparator.comparing(Operation::getMethod));
        return apiDescriptionBuilder
                .path(WxMpWebSecurityConfig.wxMpLoginUrl)
                .description("微信公众号登录相关接口")
                .operations(Arrays.asList(
                        new OperationBuilder(new CachingOperationNameGenerator())
                                .uniqueId("wxMpLogin")
                                .method(HttpMethod.POST)//http请求类型
                                .produces(new HashSet<>(Arrays.asList(MediaType.APPLICATION_JSON_VALUE)))
                                .tags(new HashSet<>(Arrays.asList("微信公众号登录接口")))//归类标签
                                .summary("微信公众号登录接口")
                                .requestParameters(
                                        Arrays.asList(
                                                new RequestParameterBuilder()
                                                        .name("username")
                                                        .description("appId")
                                                        .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                                                        .required(true)
                                                        .in(ParameterType.QUERY)
                                                        .build(),
                                                new RequestParameterBuilder()
                                                        .name("password")
                                                        .description("code")
                                                        .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                                                        .required(true)
                                                        .in(ParameterType.QUERY)
                                                        .build()
                                        )
                                )
                                /*.responses(Arrays.asList(
                                        new ResponseBuilder().representation(MediaType.APPLICATION_JSON).apply(b -> b.model(m -> m.scalarModel(ScalarType.OBJECT))).build()
                                ))*/
                                .build()
                ))
                .build();

    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
