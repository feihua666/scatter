package scatter.wxmp.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.rest.basic.config.MpProperties;
/**
 * <p>
 * 公众号微信用户表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
public interface IWxMpUserService extends IBaseService<WxMpUser> {
    /**
     * 根据openId和appCode判断是否存在
     * @param openId
     * @param appCode
     * @return
     */
    default boolean existByOpenidAndAppCode(String openId,String appCode) {
        return getByOpenidAndAppCode(openId,appCode) != null;
    }

    /**
     * 根据openId和appCode获取
     * @param openId
     * @param appCode
     * @return
     */
    default WxMpUser getByOpenidAndAppCode(String openId,String appCode) {
        Assert.hasText(openId,"openId不能为空");
        Assert.hasText(appCode,"appCode不能为空");
        return getOne(Wrappers.<WxMpUser>lambdaQuery().eq(WxMpUser::getAppCode, appCode).eq(WxMpUser::getOpenId, openId));
    }
    /**
     * 添加用户
     * @param wxMpUser
     * @param mpConfig
     * @return
     */
    WxMpUser insert(me.chanjar.weixin.mp.bean.result.WxMpUser wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser.DictSourceItem sourceItem);
    WxMpUser insert(WxOAuth2UserInfo wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser.DictSourceItem sourceItem);
    /**
     * 更新用户
     * @param wxMpUser
     * @param mpConfig
     * @param mpUser
     * @return
     */
    WxMpUser updateById(me.chanjar.weixin.mp.bean.result.WxMpUser wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser mpUser);
    WxMpUser updateById(WxOAuth2UserInfo wxMpUser, MpProperties.MpConfig mpConfig, WxMpUser mpUser);

}
