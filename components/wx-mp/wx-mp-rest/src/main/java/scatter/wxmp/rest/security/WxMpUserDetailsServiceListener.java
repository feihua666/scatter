package scatter.wxmp.rest.security;

import scatter.common.LoginUser;
import scatter.wxmp.pojo.login.WxMpLoginUser;
import scatter.wxmp.pojo.po.WxMpUser;

/**
 * Created by yangwei
 * Created at 2021/1/6 18:26
 */
public interface WxMpUserDetailsServiceListener {

    /**
     * 微信公众号登录获取用户信息后调用
     * @param mpUser
     * @param appId
     * @return 返回登录信息
     */
    LoginUser onUserInfoReady(WxMpUser mpUser, String appId, WxMpLoginUser wxMpLoginUser);
}
