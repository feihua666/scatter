package scatter.wxmp.rest.basic.form;

import lombok.Data;
import lombok.EqualsAndHashCode;
import scatter.common.pojo.form.BaseForm;

/**
 * Created by yangwei
 * Created at 2020/8/14 20:59
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class WxMpMaterialForm extends BaseForm {
    private String name;
    private String videoTitle;
    private String videoIntroduction;
}
