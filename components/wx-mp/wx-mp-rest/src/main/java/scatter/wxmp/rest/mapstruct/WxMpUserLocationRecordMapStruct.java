package scatter.wxmp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordAddForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordPageQueryForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordUpdateForm;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.wxmp.pojo.vo.WxMpUserLocationRecordVo;

/**
 * <p>
 * 公众号微信用户位置记录 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxMpUserLocationRecordMapStruct extends IBaseVoMapStruct<WxMpUserLocationRecord, WxMpUserLocationRecordVo>,
        IBaseAddFormMapStruct<WxMpUserLocationRecord,WxMpUserLocationRecordAddForm>,
        IBaseUpdateFormMapStruct<WxMpUserLocationRecord,WxMpUserLocationRecordUpdateForm>,
        IBaseQueryFormMapStruct<WxMpUserLocationRecord,WxMpUserLocationRecordPageQueryForm> {
    WxMpUserLocationRecordMapStruct INSTANCE = Mappers.getMapper( WxMpUserLocationRecordMapStruct.class );

}
