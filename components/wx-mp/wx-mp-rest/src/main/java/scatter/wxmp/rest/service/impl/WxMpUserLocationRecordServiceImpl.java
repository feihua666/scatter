package scatter.wxmp.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordAddForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordPageQueryForm;
import scatter.wxmp.pojo.form.WxMpUserLocationRecordUpdateForm;
import scatter.wxmp.pojo.po.WxMpUserLocationRecord;
import scatter.wxmp.rest.mapper.WxMpUserLocationRecordMapper;
import scatter.wxmp.rest.service.IWxMpUserLocationRecordService;
/**
 * <p>
 * 公众号微信用户位置记录表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Service
@Transactional
public class WxMpUserLocationRecordServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<WxMpUserLocationRecordMapper, WxMpUserLocationRecord, WxMpUserLocationRecordAddForm, WxMpUserLocationRecordUpdateForm, WxMpUserLocationRecordPageQueryForm> implements IWxMpUserLocationRecordService {
    @Override
    public void preAdd(WxMpUserLocationRecordAddForm addForm,WxMpUserLocationRecord po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(WxMpUserLocationRecordUpdateForm updateForm,WxMpUserLocationRecord po) {
        super.preUpdate(updateForm,po);

    }
}
