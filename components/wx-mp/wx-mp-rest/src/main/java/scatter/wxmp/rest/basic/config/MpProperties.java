package scatter.wxmp.rest.basic.config;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by yangwei
 * Created at 2020/7/20 20:01
 */
@Data
@Component
@ConfigurationProperties(prefix = "scatter.wx.mp")
public class MpProperties {

    private List<MpConfig> configs;

    @Data
    public static class MpConfig {
        /**
         * 自定义编码
         */
        private String appCode;
        /**
         * 自定义名称
         */
        private String appName;


        /**
         * 设置微信公众号的appId
         */
        private String appId;

        /**
         * 设置微信公众号的app secret
         */
        private String secret;

        /**
         * 设置微信公众号的token
         */
        private String token;

        /**
         * 设置微信公众号的EncodingAESKey
         */
        private String aesKey;
    }

    /**
     * 获取appname
     * @param appCodeOrAppId
     * @return
     */
    public String abtainAppNameByAppCodeOrAppId(String appCodeOrAppId){
        return Optional.ofNullable(abtainConfigByAppCodeOrAppId(appCodeOrAppId)).map(MpConfig::getAppName).orElse(null);
    }
    /**
     * 获取appname
     * @param appCodeOrAppId
     * @return
     */
    public MpConfig abtainConfigByAppCodeOrAppId(String appCodeOrAppId){
        return this.getConfigs().stream()
                .filter(item -> StringUtils.equals(appCodeOrAppId, item.getAppCode()) || StringUtils.equals(appCodeOrAppId, item.getAppId()))
                .findFirst()
                .orElse(null);
    }
}
