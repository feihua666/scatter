package scatter.wxmp.rest.mapper;

import scatter.wxmp.pojo.po.WxMpMsgReply;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 公众号消息回复表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
public interface WxMpMsgReplyMapper extends IBaseMapper<WxMpMsgReply> {

}
