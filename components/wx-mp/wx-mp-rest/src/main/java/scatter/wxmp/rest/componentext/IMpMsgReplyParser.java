package scatter.wxmp.rest.componentext;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import scatter.wxmp.pojo.po.WxMpMsgReply;

import java.util.Map;

/**
 * 消息回复输入消息解析
 * Created by yangwei
 * Created at 2020/7/23 20:53
 */
public interface IMpMsgReplyParser {
    /**
     * 是否支持
     * @param reply
     * @return
     */
    boolean support(WxMpMsgReply reply);

    /**
     * 解析
     * @param reply
     * @param wxMessage
     * @param context
     * @param wxMpService
     * @param sessionManager
     * @return
     */
    WxMpXmlOutMessage parse(WxMpMsgReply reply, WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager);
}
