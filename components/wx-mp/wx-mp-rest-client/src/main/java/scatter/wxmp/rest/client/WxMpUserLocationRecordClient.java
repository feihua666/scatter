package scatter.wxmp.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 公众号微信用户位置记录表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Component
@FeignClient(value = "WxMpUserLocationRecord-client")
public interface WxMpUserLocationRecordClient {

}
