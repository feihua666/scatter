package scatter.wxmp.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 公众号消息回复表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Component
@FeignClient(value = "WxMpMsgReply-client")
public interface WxMpMsgReplyClient {

}
