package scatter.wxmp.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxmp.rest.WxMpConfiguration;
import scatter.wxmp.pojo.po.WxMpUser;
import scatter.wxmp.rest.service.IWxMpUserService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 公众号微信用户表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@RestController
@RequestMapping(WxMpConfiguration.CONTROLLER_BASE_PATH + "/inner/wx-mp-user")
public class WxMpUserInnerController extends BaseInnerController<WxMpUser> {
 @Autowired
 private IWxMpUserService wxMpUserService;

 public IWxMpUserService getService(){
     return wxMpUserService;
 }
}
