package scatter.wxmp.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 公众号微信用户更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Setter
@Getter
@ApiModel(value="公众号微信用户更新表单对象")
public class WxMpUserUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="公众号编码不能为空")
    @ApiModelProperty(value = "公众号编码",required = true)
    private String appCode;

    @NotEmpty(message="公众号名称不能为空")
    @ApiModelProperty(value = "公众号名称",required = true)
    private String appName;

    @NotEmpty(message="微信openId不能为空")
    @ApiModelProperty(value = "微信openId",required = true)
    private String openId;

    @ApiModelProperty(value = "微信全局id")
    private String unionId;

    @ApiModelProperty(value = "微信昵称")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "城市区域id")
    private String cityAreaId;

    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @ApiModelProperty(value = "省区域id")
    private String provinceAreaId;

    @ApiModelProperty(value = "省名称")
    private String provinceName;

    @ApiModelProperty(value = "国家区域id")
    private String countryAreaId;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "语言")
    private String language;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;

    @NotNull(message="是否关注不能为空")
    @ApiModelProperty(value = "是否关注",required = true)
    private Boolean isSubscribe;

    @NotEmpty(message="来源不能为空")
    @ApiModelProperty(value = "来源，方式",required = true)
    private String sourceDictId;

}
