package scatter.wxmp.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 公众号微信用户位置记录响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Setter
@Getter
@ApiModel(value="公众号微信用户位置记录响应对象")
public class WxMpUserLocationRecordVo extends BaseIdVo {

    @ApiModelProperty(value = "微信用户id")
    private String wxUserId;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;

}
