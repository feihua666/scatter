package scatter.wxmp.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 公众号微信用户分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Setter
@Getter
@ApiModel(value="公众号微信用户分页表单对象")
public class WxMpUserPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "公众号编码")
    private String appCode;

    @Like
    @ApiModelProperty(value = "公众号名称")
    private String appName;

    @ApiModelProperty(value = "微信openId")
    private String openId;

    @ApiModelProperty(value = "微信全局id")
    private String unionId;

    @Like
    @ApiModelProperty(value = "微信昵称")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "是否关注")
    private Boolean isSubscribe;

    @ApiModelProperty(value = "来源，方式")
    private String sourceDictId;

}
