package scatter.wxmp.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 公众号消息回复响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Setter
@Getter
@ApiModel(value="公众号消息回复响应对象")
public class WxMpMsgReplyVo extends BaseIdVo {

    @ApiModelProperty(value = "名称，仅标识是什么消息，模糊查询")
    private String name;

    @ApiModelProperty(value = "公众号编码，模糊查询")
    private String appCode;

    @ApiModelProperty(value = "公众号名称，模糊查询")
    private String appName;

    @ApiModelProperty(value = "匹配接收人")
    private String matchFromUser;

    @ApiModelProperty(value = "匹配消息类型")
    private String matchMsgTypeDictId;

    @ApiModelProperty(value = "匹配事件类型")
    private String matchEventTypeDictId;

    @ApiModelProperty(value = "匹配事件key")
    private String matchEventKey;

    @ApiModelProperty(value = "匹配event_key的匹配方式")
    private String matchEventKeyMatchDictId;

    @ApiModelProperty(value = "匹配内容")
    private String matchContent;

    @ApiModelProperty(value = "匹配内容的匹配方式")
    private String matchContentMatchDictId;

    @ApiModelProperty(value = "回复的消息类型")
    private String replyMsgTypeDictId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "优先级，数值越大优先级越高")
    private Integer priority;

    @ApiModelProperty(value = "备注")
    private String remark;

}
