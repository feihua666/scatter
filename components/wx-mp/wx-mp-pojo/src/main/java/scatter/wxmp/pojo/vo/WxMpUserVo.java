package scatter.wxmp.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 公众号微信用户响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Setter
@Getter
@ApiModel(value="公众号微信用户响应对象")
public class WxMpUserVo extends BaseIdVo {

    @ApiModelProperty(value = "公众号编码")
    private String appCode;

    @ApiModelProperty(value = "公众号名称")
    private String appName;

    @ApiModelProperty(value = "微信openId")
    private String openId;

    @ApiModelProperty(value = "微信全局id")
    private String unionId;

    @ApiModelProperty(value = "微信昵称")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "genderDictId",mapValueField = "name")
    @ApiModelProperty(value = "性别，字典名称")
    private String genderDictName;

    @ApiModelProperty(value = "城市区域id")
    private String cityAreaId;

    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @ApiModelProperty(value = "省区域id")
    private String provinceAreaId;

    @ApiModelProperty(value = "省名称")
    private String provinceName;

    @ApiModelProperty(value = "国家区域id")
    private String countryAreaId;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "语言")
    private String language;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;

    @ApiModelProperty(value = "是否关注")
    private Boolean isSubscribe;

    @ApiModelProperty(value = "来源，方式,字典id")
    private String sourceDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceDictId",mapValueField = "name")
    @ApiModelProperty(value = "来源，方式，字典名称")
    private String sourceDictName;
}
