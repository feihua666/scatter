package scatter.wxmp.pojo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.LoginUser;
import scatter.wxmp.pojo.po.WxMpUser;

/**
 * Created by yangwei
 * Created at 2020/12/10 20:38
 */
@Getter
@Setter
@ApiModel("登录用户信息")
public class WxMpLoginUser extends LoginUser  {

    @ApiModelProperty(value = "用户登录信息")
    private WxMpUser wxMpUser;
}
