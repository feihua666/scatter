package scatter.wxmp.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公众号微信用户表
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_wx_mp_user")
@ApiModel(value="WxMpUser对象", description="公众号微信用户表")
public class WxMpUser extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_WXMPUSER_BY_ID = "trans_wxmpuser_by_id_scatter.wxmp.pojo.po";

    public enum DictSourceGroup implements IDictGroup {
        wx_mp_user_how_from;

        @Override
        public String groupCode() {
            return this.name();
        }
    }

    public static enum DictSourceItem implements IDictItem {
        ADD_SCENE_SEARCH, //公众号搜索
        ADD_SCENE_ACCOUNT_MIGRATION, // 公众号迁移
        ADD_SCENE_QR_CODE, //         扫描二维码
        ADD_SCENE_PROFILE_LINK, // 图文页内名称点击
        ADD_SCENE_PROFILE_ITEM, //  图文页右上角菜单
        ADD_SCENE_PAID, // 支付后关注
        ADD_SCENE_WECHAT_ADVERTISEMENT, // 微信广告
        ADD_SCENE_PROFILE_CARD, // 名片分享
        ADD_SCENE_OTHERS, // 其他
        wx_mp_user_how_from_oauth2,
        ; // 网页授权获取用户信息

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return DictSourceGroup.wx_mp_user_how_from.groupCode();
        }
    }

    @ApiModelProperty(value = "公众号编码")
    private String appCode;

    @ApiModelProperty(value = "公众号名称")
    private String appName;

    @ApiModelProperty(value = "微信openId")
    private String openId;

    @ApiModelProperty(value = "微信全局id")
    private String unionId;

    @ApiModelProperty(value = "微信昵称")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "城市区域id")
    private String cityAreaId;

    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @ApiModelProperty(value = "省区域id")
    private String provinceAreaId;

    @ApiModelProperty(value = "省名称")
    private String provinceName;

    @ApiModelProperty(value = "国家区域id")
    private String countryAreaId;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "语言")
    private String language;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;

    @ApiModelProperty(value = "是否关注")
    private Boolean isSubscribe;

    @ApiModelProperty(value = "来源，方式")
    private String sourceDictId;


}
