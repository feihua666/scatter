package scatter.wxmp.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公众号微信用户位置记录表
 * </p>
 *
 * @author yw
 * @since 2020-12-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_wx_mp_user_location_record")
@ApiModel(value="WxMpUserLocationRecord对象", description="公众号微信用户位置记录表")
public class WxMpUserLocationRecord extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_WXMPUSERLOCATIONRECORD_BY_ID = "trans_wxmpuserlocationrecord_by_id_scatter.wxmp.pojo.po";

    @ApiModelProperty(value = "微信用户id")
    private String wxUserId;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;


}
