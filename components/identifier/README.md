# identifier 用户登录标识组件

提供用户的登录数据存储和密码数据存储与管理功能，将用户的登录拆分开来  

+ 支持同一个用户多个不同类型的账号


配置
```yaml
scatter
  identifier
    reset-password: 123456 #重置密码默认的密码
```

