package scatter.identifier.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户密码分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Setter
@Getter
@ApiModel(value="用户密码分页表单对象")
public class IdentifierPwdPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户标识id")
    private String identifierId;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "是否过期")
    private Boolean isExpired;


    @ApiModelProperty(value = "是否需要提示修改密码")
    private Boolean isNeedUpdate;

    @ApiModelProperty(value = "密码的修改时间")
    private LocalDateTime pwdModifiedAt;

    @ApiModelProperty(value = "复杂度，数字越高越复杂")
    private Integer complexity;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;
}
