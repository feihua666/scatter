package scatter.identifier.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户登录标识表
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_identifier")
@ApiModel(value="Identifier对象", description="用户登录标识表")
public class Identifier extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_IDENTIFIER_BY_ID = "trans_identifier_by_id_scatter.identifier.pojo.po";

    /**
     * 用户登录类型字典组编码
     */
    public enum TypeDictGroup implements IDictGroup {
        account_type;

        @Override
        public String groupCode() {
            return this.name();
        }}

    /**
     * 用户登录类型字典项编码
     */
    public enum TypeDictItem implements IDictItem {
        front_account, // 帐号
        front_mobile, // 手机验证码登录
        front_email, // 邮箱登录
        front_wx, // 微信登录
        front_qq, // qq登录
        front_douyin, // 抖音登录
        front_sina_weibo, // 新浪微博登录
        wx_mp, // 微信公众号登录
        wx_ma, // 微信小程序登录
        wx_ma_mobile, // 微信小程序手机俣登录
        ;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return TypeDictGroup.account_type.groupCode();
        }
    }

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "登录标识")
    private String identifier;

    @ApiModelProperty(value = "授权类型,字典id")
    private String identityTypeDictId;

    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定")
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "unionId，支持第三方登录unionId")
    private String unionId;

    @ApiModelProperty(value = "是否过期")
    private Boolean isExpired;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDateTime lastLoginAt;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;


}
