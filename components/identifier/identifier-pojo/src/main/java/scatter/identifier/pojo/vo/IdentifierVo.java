package scatter.identifier.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;

import java.time.LocalDateTime;


/**
 * <p>
 * 用户登录标识响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Setter
@Getter
@ApiModel(value="用户登录标识响应对象")
public class IdentifierVo extends BaseIdVo {

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "登录标识")
    private String identifier;

    @ApiModelProperty(value = "授权类型,字典id")
    private String identityTypeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID, byFieldName = "identityTypeDictId", mapValueField = "name")
    @ApiModelProperty(value = "授权类型,字典名称")
    private String identityTypeDictName;

    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定")
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "unionId，支持第三方登录unionId")
    private String unionId;

    @ApiModelProperty(value = "是否过期")
    private Boolean isExpired;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceFromDictId",mapValueField="name")
    @ApiModelProperty(value = "用户来源，字典名称")
    private String sourceFromDictName;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDateTime lastLoginAt;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;
}
