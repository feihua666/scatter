package scatter.identifier.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * Created by yangwei
 * Created at 2021/3/29 14:14
 */
@Setter
@Getter
@ApiModel(value="更新密码表单对象")
public class UpdatePasswordForm extends BaseForm {

    @NotEmpty(message="新密码不能为空")
    @ApiModelProperty(value = "新密码",required = true)
    private String newPassword;
}
