package scatter.identifier.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 用户登录标识更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@PropValid
@Setter
@Getter
@ApiModel(value="用户登录标识更新表单对象")
public class IdentifierUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户ID不能为空")
    @ApiModelProperty(value = "用户ID",required = true)
    private String userId;

    @NotEmpty(message="登录标识不能为空")
    @ApiModelProperty(value = "登录标识",required = true)
    private String identifier;

    @NotEmpty(message="授权类型不能为空")
    @ApiModelProperty(value = "授权类型,字典id",required = true)
    private String identityTypeDictId;

    @NotNull(message="锁定状态不能为空")
    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定",required = true)
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "unionId，支持第三方登录unionId")
    private String unionId;

    @NotNull(message="是否过期不能为空")
    @ApiModelProperty(value = "是否过期",required = true)
    private Boolean isExpired;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "密码")
    private String password;


    @ApiModelProperty(value = "密码复杂度")
    private Integer passwordComplexity;
}
