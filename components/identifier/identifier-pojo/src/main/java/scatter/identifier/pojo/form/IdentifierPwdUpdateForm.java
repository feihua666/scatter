package scatter.identifier.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户密码更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Setter
@Getter
@ApiModel(value="用户密码更新表单对象")
public class IdentifierPwdUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户标识id不能为空")
    @ApiModelProperty(value = "用户标识id",required = true)
    private String identifierId;

    @NotEmpty(message="密码不能为空")
    @ApiModelProperty(value = "密码",required = true)
    private String pwd;

    @NotNull(message="是否过期不能为空")
    @ApiModelProperty(value = "是否过期",required = true)
    private Boolean isExpired;


    @ApiModelProperty(value = "是否需要提示修改密码")
    private Boolean isNeedUpdate;

    @NotNull(message="密码的修改时间不能为空")
    @ApiModelProperty(value = "密码的修改时间",required = true)
    private LocalDateTime pwdModifiedAt;

    @NotNull(message="复杂度不能为空")
    @ApiModelProperty(value = "复杂度，数字越高越复杂",required = true)
    private Integer complexity;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;
}
