package scatter.identifier.pojo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.LoginUser;
import scatter.identifier.pojo.po.Identifier;

/**
 * Created by yangwei
 * Created at 2020/12/10 20:38
 */
@Getter
@Setter
@ApiModel("登录用户信息")
public class IdentifierLoginUser extends LoginUser  {


    @ApiModelProperty(value = "用户登录标识信息")
    private Identifier identifier;

}
