package scatter.identifier.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户密码表
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_identifier_pwd")
@ApiModel(value="IdentifierPwd对象", description="用户密码表")
public class IdentifierPwd extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_IDENTIFIERPWD_BY_ID = "trans_identifierpwd_by_id_scatter.identifier.pojo.po";

    @ApiModelProperty(value = "用户标识id")
    private String identifierId;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "是否过期")
    private Boolean isExpired;

    @ApiModelProperty(value = "是否需要提示修改密码")
    private Boolean isNeedUpdate;

    @ApiModelProperty(value = "密码的修改时间")
    private LocalDateTime pwdModifiedAt;

    @ApiModelProperty(value = "复杂度，数字越高越复杂")
    private Integer complexity;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;
}
