import IdentifierUrl from './IdentifierUrl.js'

const IdentifierRoute = [
    {
        path: IdentifierUrl.router.searchList,
        component: () => import('./element/IdentifierSearchList'),
        meta: {
            // root: true,
            // keepAlive: true,
            code:'IdentifierSearchList',
            name: '用户登录标识管理'
        }
    },
    {
        path: IdentifierUrl.router.add,
        component: () => import('./element/IdentifierAdd'),
        meta: {
            code:'IdentifierAdd',
            name: '用户登录标识添加'
        }
    },
    {
        path: IdentifierUrl.router.update,
        component: () => import('./element/IdentifierUpdate'),
        meta: {
            code:'IdentifierUpdate',
            name: '用户登录标识修改'
        }
    },
]
export default IdentifierRoute