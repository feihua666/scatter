import IdentifierPwdForm from './IdentifierPwdForm.js'
import IdentifierPwdTable from './IdentifierPwdTable.js'
import IdentifierPwdUrl from './IdentifierPwdUrl.js'

const IdentifierPwdMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(IdentifierPwdForm,this.$options.name)
        },
        computedTableOptions() {
            return IdentifierPwdTable
        },
        computedUrl(){
            return IdentifierPwdUrl
        }
    },
}
export default IdentifierPwdMixin