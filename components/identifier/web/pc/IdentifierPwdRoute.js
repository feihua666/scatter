import IdentifierPwdUrl from './IdentifierPwdUrl.js'

const IdentifierPwdRoute = [
    {
        path: IdentifierPwdUrl.router.searchList,
        component: () => import('./element/IdentifierPwdSearchList'),
        meta: {
            code:'IdentifierPwdSearchList',
            name: '用户密码管理'
        }
    },
    {
        path: IdentifierPwdUrl.router.add,
        component: () => import('./element/IdentifierPwdAdd'),
        meta: {
            code:'IdentifierPwdAdd',
            name: '用户密码添加'
        }
    },
    {
        path: IdentifierPwdUrl.router.update,
        component: () => import('./element/IdentifierPwdUpdate'),
        meta: {
            code:'IdentifierPwdUpdate',
            name: '用户密码修改'
        }
    },
]
export default IdentifierPwdRoute