const IdentifierTable = [
    {
        prop: 'identifier',
        label: '登录标识'
    },
    {
        prop: 'identityTypeDictName',
        label: '授权类型'
    },
    {
        prop: 'isLock',
        label: '锁定状态'
    },
    {
        prop: 'lockReason',
        label: '锁定原因'
    },
    {
        prop: 'unionId',
        label: 'unionId'
    },
    {
        prop: 'isExpired',
        label: '是否过期'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'lastLoginAt',
        label: '最后一次登录时间'
    },
    {
        prop: 'lastLoginIp',
        label: '最后一次登录ip'
    },
]
export default IdentifierTable