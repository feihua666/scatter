const IdentifierForm = [
    {
        IdentifierSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'userId',
        },
        element:{
            label: '用户',
            required: true,
            disabled: true,
            type: 'select',
            options: ({$vm})=>{
                return {
                    datas: [
                        {
                            id: $vm.$route.query.userId,
                            name: $vm.$route.query.userNickname
                        }
                    ]
                }
            }
        }
    },
    {
        IdentifierSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'identifier',
        },
        element:{
            label: '登录标识',
            required: true,
        }
    },
    {
        IdentifierSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'identityTypeDictId',
        },
        element:{
            label: '授权类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'account_type'
            }
        }
    },
    {
        IdentifierSearchList: false,
        field: {
            name: 'isLock',
            value: false,
        },
        element:{
            type: 'switch',
            label: '锁定状态',
            required: true,
        }
    },
    {
        IdentifierSearchList: false,
        field: {
            name: 'lockReason',
        },
        element:{
            label: '锁定原因',
            required: ({form})=>{
                return form.isLock
            }
        }
    },
    {
        field: {
            name: 'unionId',
        },
        element:{
            label: 'unionId',
        }
    },
    {
        IdentifierSearchList: false,
        field: {
            name: 'isExpired',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否过期',
            required: true,
        }
    },
    {
        IdentifierSearchList: false,
        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {
        IdentifierSearchList: false,
        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
            required: ({form})=>{
                return form.isDisabled
            }
        }
    },
    {

        IdentifierSearchList: false,
        field: {
            name: 'password',
        },
        element:{
            label: '密码',
            options: {
                placeholder: '不添加或不修改请留空'
            }
        }
    },
    {
        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
]
export default IdentifierForm