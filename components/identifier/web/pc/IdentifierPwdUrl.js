const basePath = '' + '/identifier-pwd'
const IdentifierPwdUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    resetUserPwd: basePath + '/resetUserPwd/{userId}',
    resetIdentifierPwd: basePath + '/resetIdentifierPwd/{identifierId}',
    router: {
        searchList: '/IdentifierPwdSearchList',
        add: '/IdentifierPwdAdd',
        update: '/IdentifierPwdUpdate',
    }
}
export default IdentifierPwdUrl