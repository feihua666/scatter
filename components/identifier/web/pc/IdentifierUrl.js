const basePath = '' + '/identifier'
const IdentifierUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/IdentifierSearchList',
        add: '/IdentifierAdd',
        update: '/IdentifierUpdate',
    }
}
export default IdentifierUrl