import IdentifierUserUrl from './IdentifierUserUrl.js'

const IdentifierUserMixin = {
    computed: {
        computedUrl(){
            return IdentifierUserUrl
        }
    },
}
export default IdentifierUserMixin