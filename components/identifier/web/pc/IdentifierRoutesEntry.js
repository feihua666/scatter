import IdentifierRoute from './IdentifierRoute.js'
import IdentifierPwdRoute from './IdentifierPwdRoute.js'

const IdentifierRoutesEntry = [
]
.concat(IdentifierRoute)
.concat(IdentifierPwdRoute)

export default IdentifierRoutesEntry