const IdentifierPwdTable = [
    {
        prop: 'identifierId',
        label: '用户标识id'
    },
    {
        prop: 'pwd',
        label: '密码'
    },
    {
        prop: 'isExpired',
        label: '是否过期'
    },
    {
        prop: 'pwdModifiedAt',
        label: '密码的修改时间'
    },
    {
        prop: 'complexity',
        label: '复杂度'
    },
]
export default IdentifierPwdTable