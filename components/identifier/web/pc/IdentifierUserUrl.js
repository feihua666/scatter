const basePath = ''
const IdentifierUrl = {
    login: '/login',
    hasLogin: basePath + '/identifier/login/hasLogin',
    loginUserinfo: basePath + '/identifier/login/userinfo',
}
export default IdentifierUrl