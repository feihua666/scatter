const IdentifierPwdForm = [
    {
        IdentifierPwdSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'identifierId',
        },
        element:{
            label: '用户标识id',
            required: true,
        }
    },
    {
        IdentifierPwdSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'pwd',
        },
        element:{
            label: '密码',
            required: true,
        }
    },
    {
        IdentifierPwdSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'isExpired',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否过期',
            required: true,
        }
    },
    {
        IdentifierPwdSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'pwdModifiedAt',
        },
        element:{
            label: '密码的修改时间',
            required: true,
        }
    },
    {
        IdentifierPwdSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'complexity',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '复杂度',
            required: true,
        }
    },
]
export default IdentifierPwdForm