import IdentifierForm from './IdentifierForm.js'
import IdentifierTable from './IdentifierTable.js'
import IdentifierUrl from './IdentifierUrl.js'

const IdentifierMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(IdentifierForm,this.$options.name)
        },
        computedTableOptions() {
            return IdentifierTable
        },
        computedUrl(){
            return IdentifierUrl
        }
    },
}
export default IdentifierMixin