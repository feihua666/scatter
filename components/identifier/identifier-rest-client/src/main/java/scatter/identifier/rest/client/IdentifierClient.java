package scatter.identifier.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户登录标识表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Component
@FeignClient(value = "Identifier-client")
public interface IdentifierClient {

}
