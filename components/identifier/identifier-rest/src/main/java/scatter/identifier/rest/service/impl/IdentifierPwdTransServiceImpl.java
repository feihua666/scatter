package scatter.identifier.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.rest.service.IIdentifierPwdService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户密码翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Component
public class IdentifierPwdTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IIdentifierPwdService identifierPwdService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,IdentifierPwd.TRANS_IDENTIFIERPWD_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,IdentifierPwd.TRANS_IDENTIFIERPWD_BY_ID)) {
            IdentifierPwd byId = identifierPwdService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,IdentifierPwd.TRANS_IDENTIFIERPWD_BY_ID)) {
            return identifierPwdService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
