package scatter.identifier.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.identifier.pojo.form.IdentifierAddForm;
import scatter.identifier.pojo.form.IdentifierPageQueryForm;
import scatter.identifier.pojo.form.IdentifierUpdateForm;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.pojo.vo.IdentifierVo;

/**
 * <p>
 * 用户登录标识 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IdentifierMapStruct extends IBaseVoMapStruct<Identifier, IdentifierVo>,
        IBaseAddFormMapStruct<Identifier,IdentifierAddForm>,
        IBaseUpdateFormMapStruct<Identifier,IdentifierUpdateForm>,
        IBaseQueryFormMapStruct<Identifier,IdentifierPageQueryForm> {
    IdentifierMapStruct INSTANCE = Mappers.getMapper( IdentifierMapStruct.class );

}
