package scatter.identifier.rest.mapper;

import scatter.identifier.pojo.po.Identifier;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户登录标识表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
public interface IdentifierMapper extends IBaseMapper<Identifier> {

}
