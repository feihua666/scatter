package scatter.identifier.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.common.rest.tools.PasswordComplexityTool;
import scatter.identifier.rest.IdentifierConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.pojo.vo.IdentifierVo;
import scatter.identifier.pojo.form.IdentifierAddForm;
import scatter.identifier.pojo.form.IdentifierUpdateForm;
import scatter.identifier.pojo.form.IdentifierPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户登录标识表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Api(tags = "用户登录标识相关接口")
@RestController
@RequestMapping(IdentifierConfiguration.CONTROLLER_BASE_PATH + "/identifier")
public class IdentifierController extends BaseAddUpdateQueryFormController<Identifier, IdentifierVo, IdentifierAddForm, IdentifierUpdateForm, IdentifierPageQueryForm> {

    @Autowired
    private PasswordEncoder passwordEncoder;

     @Override
	 @ApiOperation("添加用户登录标识")
     @PreAuthorize("hasAuthority('Identifier:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public IdentifierVo add(@RequestBody @Valid IdentifierAddForm addForm) {
         if (!isStrEmpty(addForm.getPassword())) {
             if (addForm.getPasswordComplexity() == null) {
                 addForm.setPasswordComplexity(PasswordComplexityTool.getComplexityLevel(addForm.getPassword()).getComplexityLevel());
             }
             addForm.setPassword(passwordEncoder.encode(addForm.getPassword()));

         }
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户登录标识")
     @PreAuthorize("hasAuthority('Identifier:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public IdentifierVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户登录标识")
     @PreAuthorize("hasAuthority('Identifier:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户登录标识")
     @PreAuthorize("hasAuthority('Identifier:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public IdentifierVo update(@RequestBody @Valid IdentifierUpdateForm updateForm) {
         if (!isStrEmpty(updateForm.getPassword())) {
             if (updateForm.getPasswordComplexity() == null) {
                 updateForm.setPasswordComplexity(PasswordComplexityTool.getComplexityLevel(updateForm.getPassword()).getComplexityLevel());
             }
             updateForm.setPassword(passwordEncoder.encode(updateForm.getPassword()));
         }
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户登录标识")
    @PreAuthorize("hasAuthority('Identifier:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<IdentifierVo> getList(IdentifierPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户登录标识")
    @PreAuthorize("hasAuthority('Identifier:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<IdentifierVo> getPage(IdentifierPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
