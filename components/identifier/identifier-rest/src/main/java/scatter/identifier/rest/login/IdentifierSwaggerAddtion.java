package scatter.identifier.rest.login;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiDescriptionBuilder;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.ApiDescription;
import springfox.documentation.service.Operation;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.DocumentationContext;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Created by yangwei
 * Created at 2021/1/7 12:50
 */
@Component
public class IdentifierSwaggerAddtion implements ApiListingScannerPlugin {
    @Override
    public List<ApiDescription> apply(DocumentationContext context) {
        return Arrays.asList(
                login()
        );
    }

    /**
     * 登录接口
     *
     * @return
     */
    private ApiDescription login() {
        ApiDescriptionBuilder apiDescriptionBuilder = new ApiDescriptionBuilder(Comparator.comparing(Operation::getMethod));
        return apiDescriptionBuilder
                .path("/login")
                .description("后台登录相关接口")
                .operations(Arrays.asList(
                        new OperationBuilder(new CachingOperationNameGenerator())
                                .uniqueId("identifierLogin")
                                .method(HttpMethod.POST)//http请求类型
                                .produces(new HashSet<>(Arrays.asList(MediaType.APPLICATION_JSON_VALUE)))
                                .tags(new HashSet<>(Arrays.asList("后台登录接口")))//归类标签
                                .summary("后台登录接口")
                                .requestParameters(
                                        Arrays.asList(
                                                new RequestParameterBuilder()
                                                        .name("username")
                                                        .description("用户名")
                                                        .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                                                        .required(true)
                                                        .in(ParameterType.QUERY)
                                                        .build(),
                                                new RequestParameterBuilder()
                                                        .name("password")
                                                        .description("密码")
                                                        .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                                                        .required(true)
                                                        .in(ParameterType.QUERY)
                                                        .build()
                                        )
                                )
                                /*.responses(Arrays.asList(
                                        new ResponseBuilder().representation(MediaType.APPLICATION_JSON).apply(b -> b.model(m -> m.scalarModel(ScalarType.OBJECT))).build()
                                ))*/
                                .build()
                ))
                .build();

    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
