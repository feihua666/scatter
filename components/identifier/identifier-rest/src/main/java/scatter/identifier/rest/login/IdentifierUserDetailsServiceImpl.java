package scatter.identifier.rest.login;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import scatter.common.LoginUser;
import scatter.common.rest.security.LoginUserExtPutService;
import scatter.identifier.pojo.login.IdentifierLoginUser;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.rest.service.IIdentifierPwdService;
import scatter.identifier.rest.service.IIdentifierService;

import java.util.List;

/**
 * 这个bean如果放到配置文件中通过bean注解方式会导致其依赖的注入为空
 * Created by yangwei
 * Created at 2020/12/10 20:59
 */
@Service
public class IdentifierUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private IIdentifierService iIdentifierService;

    @Autowired
    private IIdentifierPwdService iIdentifierPwdService;

    @Autowired(required = false)
    private UserAuthorityService userAuthorityService;

    @Autowired(required = false)
    private List<LoginUserExtPutService> loginUserExtPutServices;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Identifier byIdentifier = iIdentifierService.getByIdentifier(username);
        if (byIdentifier == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        IdentifierPwd byIdentifierId = iIdentifierPwdService.getByIdentifierId(byIdentifier.getId());
        IdentifierLoginUser identifierUser = new IdentifierLoginUser();

        // 帐号信息
        identifierUser.setId(byIdentifier.getUserId());
        identifierUser.setUsername(username);
        identifierUser.setIsEnabled(!byIdentifier.getIsDisabled());
        identifierUser.setIsLocked(byIdentifier.getIsLock());
        identifierUser.setIsExpired(byIdentifier.getIsExpired());

        // 密码信息
        identifierUser.setPassword(byIdentifierId.getPwd());
        identifierUser.setIsCredentialsExpired(byIdentifierId.getIsExpired());

        // 总体信息
        identifierUser.setIdentifier(byIdentifier);

        // 默认添加用户权限
        identifierUser.addAuthority("user");
        if (userAuthorityService != null) {
            List<String> list = userAuthorityService.retrieveUserAuthoritiesByUserId(identifierUser.getId());
            identifierUser.addAuthority(list);
            if (list != null && list.stream().filter(item -> StrUtil.equals(LoginUser.super_admin_role, item)).count() > 0) {
                identifierUser.setIsSuperAdmin(true);
            }
        }

        if (loginUserExtPutServices != null) {
            for (LoginUserExtPutService loginUserExtPutService : loginUserExtPutServices) {
                loginUserExtPutService.addExt(identifierUser);
            }
        }
        return identifierUser;
    }
}
