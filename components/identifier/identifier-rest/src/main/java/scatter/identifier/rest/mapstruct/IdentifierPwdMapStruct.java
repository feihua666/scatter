package scatter.identifier.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.identifier.pojo.form.IdentifierPwdAddForm;
import scatter.identifier.pojo.form.IdentifierPwdPageQueryForm;
import scatter.identifier.pojo.form.IdentifierPwdUpdateForm;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.pojo.vo.IdentifierPwdVo;

/**
 * <p>
 * 用户密码 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IdentifierPwdMapStruct extends IBaseVoMapStruct<IdentifierPwd, IdentifierPwdVo>,
        IBaseAddFormMapStruct<IdentifierPwd,IdentifierPwdAddForm>,
        IBaseUpdateFormMapStruct<IdentifierPwd,IdentifierPwdUpdateForm>,
        IBaseQueryFormMapStruct<IdentifierPwd,IdentifierPwdPageQueryForm> {
    IdentifierPwdMapStruct INSTANCE = Mappers.getMapper( IdentifierPwdMapStruct.class );

}
