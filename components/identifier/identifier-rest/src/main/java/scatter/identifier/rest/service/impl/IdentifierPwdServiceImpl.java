package scatter.identifier.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.identifier.pojo.form.IdentifierPwdAddForm;
import scatter.identifier.pojo.form.IdentifierPwdPageQueryForm;
import scatter.identifier.pojo.form.IdentifierPwdUpdateForm;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.rest.mapper.IdentifierPwdMapper;
import scatter.identifier.rest.service.IIdentifierPwdService;
import scatter.identifier.rest.service.IIdentifierService;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户密码表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Service
@Transactional
public class IdentifierPwdServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<IdentifierPwdMapper, IdentifierPwd, IdentifierPwdAddForm, IdentifierPwdUpdateForm, IdentifierPwdPageQueryForm> implements IIdentifierPwdService {

    @Autowired
    private IIdentifierService iIdentifierService;


    @Override
    public void preAdd(IdentifierPwdAddForm addForm,IdentifierPwd po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getIdentifierId())) {
            // 用户标识id已存在不能添加
            assertByColumn(addForm.getIdentifierId(),IdentifierPwd::getIdentifierId,false);
        }

    }

    @Override
    public void preUpdate(IdentifierPwdUpdateForm updateForm,IdentifierPwd po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getIdentifierId())) {
            IdentifierPwd byId = getById(updateForm.getId());
            // 如果用户标识id有改动
            if (!isEqual(updateForm.getIdentifierId(), byId.getIdentifierId())) {
                // 用户标识id已存在不能修改
                assertByColumn(updateForm.getIdentifierId(),IdentifierPwd::getIdentifierId,false);
            }
        }

    }

    @Override
    public List<IdentifierPwd> getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        List<Identifier> byUserId = iIdentifierService.getByUserId(userId);
        if (!isEmpty(byUserId)) {
            List<IdentifierPwd> byIdentifierIds = getByIdentifierIds(byUserId.stream().map(Identifier::getId).collect(Collectors.toList()));
            return byIdentifierIds;
        }
        return null;
    }

    @Override
    public boolean updatePasswordByUserId(String userId,String encodedPassword) {
        List<IdentifierPwd> byUserId = getByUserId(userId);
        if (!isEmpty(byUserId)) {
            for (IdentifierPwd identifierPwd : byUserId) {
                identifierPwd.setPwd(encodedPassword);
                identifierPwd.setIsNeedUpdate(true);
                identifierPwd.setPwdModifiedAt(LocalDateTime.now());
            }
            return updateBatchById(byUserId);
        }
        return false;
    }


    @Override
    public boolean updatePasswordByIdentifierId(String identifierId,String encodedPassword) {
        IdentifierPwd identifierPwd = getByIdentifierId(identifierId);
        if (identifierPwd != null) {
            identifierPwd.setPwd(encodedPassword);
            identifierPwd.setIsNeedUpdate(true);
            identifierPwd.setPwdModifiedAt(LocalDateTime.now());
            return updateById(identifierPwd);
        }
        return false;
    }

}
