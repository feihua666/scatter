package scatter.identifier.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.identifier.pojo.po.IdentifierPwd;

import java.util.List;

/**
 * <p>
 * 用户密码表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
public interface IIdentifierPwdService extends IBaseService<IdentifierPwd> {

    /**
     * 根据编码查询
     * @param identifierId
     * @return
     */
    default IdentifierPwd getByIdentifierId(String identifierId) {
        Assert.hasText(identifierId,"identifierId不能为空");
        return getOne(Wrappers.<IdentifierPwd>lambdaQuery().eq(IdentifierPwd::getIdentifierId, identifierId));
    }
    /**
     * 根据编码查询多个
     * @param identifierIds
     * @return
     */
    default List<IdentifierPwd> getByIdentifierIds(List<String> identifierIds) {
        Assert.notEmpty(identifierIds,"identifierIds不能为空");
        return list(Wrappers.<IdentifierPwd>lambdaQuery().in(IdentifierPwd::getIdentifierId, identifierIds));
    }
    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    List<IdentifierPwd> getByUserId(String userId);

    /**
     * 根据用户id重置用户密码
     * @param userId
     * @return
     */
    boolean updatePasswordByUserId(String userId,String encodedPassword);

    /**
     * 根据identifierId重置用户密码
     * @param identifierId
     * @return
     */
    boolean updatePasswordByIdentifierId(String identifierId,String encodedPassword);
}
