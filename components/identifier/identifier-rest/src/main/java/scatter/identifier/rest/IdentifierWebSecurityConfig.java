package scatter.identifier.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import scatter.common.rest.config.CommonRestSecurityConfigure;
import scatter.common.rest.security.DefaultAuthenticationFailureHandler;
import scatter.common.rest.security.DefaultAuthenticationSuccessHandler;
import scatter.identifier.rest.login.IdentifierUserDetailsServiceImpl;

/**
 * Created by yangwei
 * Created at 2020/12/10 20:31
 */
public class IdentifierWebSecurityConfig implements CommonRestSecurityConfigure {

    @Autowired
    private IdentifierUserDetailsServiceImpl identifierUserService;

    @Override
	public void configure(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {
        auth.userDetailsService(identifierUserService).passwordEncoder(passwordEncoder);
    }

    @Override
	public void configure(HttpSecurity http, AuthenticationManager authenticationManager) throws Exception {
        http.authorizeRequests()
                .and()
                .formLogin().successHandler(new DefaultAuthenticationSuccessHandler())
                .failureHandler(new DefaultAuthenticationFailureHandler())
                .loginProcessingUrl("/login")
                .permitAll()
                .and()
                .logout().permitAll()
                .and().csrf().disable();
    }
}
