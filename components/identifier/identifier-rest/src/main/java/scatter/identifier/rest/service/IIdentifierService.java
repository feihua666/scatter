package scatter.identifier.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.identifier.pojo.po.Identifier;

import java.util.List;

/**
 * <p>
 * 用户登录标识表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
public interface IIdentifierService extends IBaseService<Identifier> {

    /**
     * 根据编码查询
     * @param identifier
     * @return
     */
    default Identifier getByIdentifier(String identifier) {
        Assert.hasText(identifier,"identifier不能为空");
        return getOne(Wrappers.<Identifier>lambdaQuery().eq(Identifier::getIdentifier, identifier));
    }

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<Identifier> getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return list(Wrappers.<Identifier>lambdaQuery().eq(Identifier::getUserId, userId));
    }
    /**
     * 根据用户id和分组标识查询
     * @param userId
     * @return
     */
    default List<Identifier> getByUserIdAndGroupFlag(String userId,String groupFlag) {
        Assert.hasText(userId,"userId不能为空");
        Assert.hasText(groupFlag,"groupFlag不能为空");
        return list(Wrappers.<Identifier>lambdaQuery().eq(Identifier::getUserId, userId).eq(Identifier::getGroupFlag,groupFlag));
    }
    /**
     * 主要用于查询用户的identifier字段
     * 谨慎使用，请注意数据准备性，有可能返回多条，尽量使用 {@link IIdentifierService#getByUserIdAndIdentifierTypeDictIdAndGroupFlag(java.lang.String, java.lang.String, java.lang.String)}
     * @param userId
     * @param identifierTypeDictId
     * @return
     */
    default Identifier getByUserIdAndIdentifierTypeDictId(String userId,String identifierTypeDictId) {
        Assert.hasText(userId,"userId 不能为空");
        Assert.hasText(identifierTypeDictId,"identifierType 不能为空");
        return getOne(Wrappers.<Identifier>lambdaQuery().eq(Identifier::getUserId, userId).eq(Identifier::getIdentityTypeDictId,identifierTypeDictId));
    }
    /**
     * 主要用于查询用户的identifier字段
     * @param userId
     * @param identifierTypeDictId
     * @return
     */
    default Identifier getByUserIdAndIdentifierTypeDictIdAndGroupFlag(String userId,String identifierTypeDictId,String groupFlag) {
        Assert.hasText(userId,"userId 不能为空");
        Assert.hasText(identifierTypeDictId,"identifierType 不能为空");
        Assert.hasText(groupFlag,"groupFlag不能为空");
        return getOne(Wrappers.<Identifier>lambdaQuery().eq(Identifier::getUserId, userId)
                .eq(Identifier::getIdentityTypeDictId,identifierTypeDictId)
                .eq(Identifier::getGroupFlag,groupFlag));
    }
}
