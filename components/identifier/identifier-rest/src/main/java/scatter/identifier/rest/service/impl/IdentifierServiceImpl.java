package scatter.identifier.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.identifier.pojo.form.IdentifierAddForm;
import scatter.identifier.pojo.form.IdentifierPageQueryForm;
import scatter.identifier.pojo.form.IdentifierUpdateForm;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.rest.mapper.IdentifierMapper;
import scatter.identifier.rest.service.IIdentifierService;
/**
 * <p>
 * 用户登录标识表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Service
@Transactional
public class IdentifierServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<IdentifierMapper, Identifier, IdentifierAddForm, IdentifierUpdateForm, IdentifierPageQueryForm> implements IIdentifierService {
    @Override
    public void preAdd(IdentifierAddForm addForm,Identifier po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(IdentifierUpdateForm updateForm,Identifier po) {
        super.preUpdate(updateForm,po);

    }


}
