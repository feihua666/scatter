package scatter.identifier.rest.mapper;

import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户密码表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
public interface IdentifierPwdMapper extends IBaseMapper<IdentifierPwd> {

}
