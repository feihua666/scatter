package scatter.identifier.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.identifier.rest.IdentifierConfiguration;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.rest.service.IIdentifierPwdService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户密码表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@RestController
@RequestMapping(IdentifierConfiguration.CONTROLLER_BASE_PATH + "/inner/identifier-pwd")
public class IdentifierPwdInnerController extends BaseInnerController<IdentifierPwd> {
 @Autowired
 private IIdentifierPwdService identifierPwdService;

 public IIdentifierPwdService getService(){
     return identifierPwdService;
 }
}
