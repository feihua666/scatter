package scatter.identifier.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.rest.service.IIdentifierService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户登录标识翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@Component
public class IdentifierTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IIdentifierService identifierService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Identifier.TRANS_IDENTIFIER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Identifier.TRANS_IDENTIFIER_BY_ID)) {
            Identifier byId = identifierService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Identifier.TRANS_IDENTIFIER_BY_ID)) {
            return identifierService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
