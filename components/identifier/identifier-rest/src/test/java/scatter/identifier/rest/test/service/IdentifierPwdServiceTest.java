package scatter.identifier.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.pojo.form.IdentifierPwdAddForm;
import scatter.identifier.pojo.form.IdentifierPwdUpdateForm;
import scatter.identifier.pojo.form.IdentifierPwdPageQueryForm;
import scatter.identifier.rest.test.IdentifierPwdSuperTest;
import scatter.identifier.rest.service.IIdentifierPwdService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户密码 服务测试类
* </p>
*
* @author yw
* @since 2020-12-10
*/
@SpringBootTest
public class IdentifierPwdServiceTest extends IdentifierPwdSuperTest{

    @Autowired
    private IIdentifierPwdService identifierPwdService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<IdentifierPwd> pos = identifierPwdService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}