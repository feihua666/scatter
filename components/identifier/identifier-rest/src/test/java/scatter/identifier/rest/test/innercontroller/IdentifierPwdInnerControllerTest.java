package scatter.identifier.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.identifier.rest.test.IdentifierPwdSuperTest;
/**
* <p>
* 用户密码 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-10
*/
@SpringBootTest
public class IdentifierPwdInnerControllerTest extends IdentifierPwdSuperTest{
    @Test
    void contextLoads() {
    }
}