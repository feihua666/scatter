package scatter.identifier.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.identifier.pojo.po.IdentifierPwd;
import scatter.identifier.pojo.form.IdentifierPwdAddForm;
import scatter.identifier.pojo.form.IdentifierPwdUpdateForm;
import scatter.identifier.pojo.form.IdentifierPwdPageQueryForm;
import scatter.identifier.rest.service.IIdentifierPwdService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户密码 测试类基类
* </p>
*
* @author yw
* @since 2020-12-10
*/
public class IdentifierPwdSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IIdentifierPwdService identifierPwdService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return identifierPwdService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return identifierPwdService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public IdentifierPwd mockPo() {
        return JMockData.mock(IdentifierPwd.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public IdentifierPwdAddForm mockAddForm() {
        return JMockData.mock(IdentifierPwdAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public IdentifierPwdUpdateForm mockUpdateForm() {
        return JMockData.mock(IdentifierPwdUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public IdentifierPwdPageQueryForm mockPageQueryForm() {
        return JMockData.mock(IdentifierPwdPageQueryForm.class, mockConfig);
    }
}