package scatter.identifier.rest.test.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.identifier.rest.test.IdentifierPwdSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.identifier.pojo.form.IdentifierPwdAddForm;
import scatter.identifier.pojo.vo.IdentifierPwdVo;
import scatter.identifier.rest.IdentifierConfiguration;
/**
* <p>
* 用户密码 前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-10
*/
@SpringBootTest
@AutoConfigureMockMvc
public class IdentifierPwdControllerTest extends IdentifierPwdSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = IdentifierConfiguration.CONTROLLER_BASE_PATH + "/identifier-pwd";

        // 请求表单
        IdentifierPwdAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        IdentifierPwdVo vo = JSONUtil.toBean(response.getContentAsString(), IdentifierPwdVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}