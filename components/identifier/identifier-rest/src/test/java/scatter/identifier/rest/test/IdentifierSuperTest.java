package scatter.identifier.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.pojo.form.IdentifierAddForm;
import scatter.identifier.pojo.form.IdentifierUpdateForm;
import scatter.identifier.pojo.form.IdentifierPageQueryForm;
import scatter.identifier.rest.service.IIdentifierService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户登录标识 测试类基类
* </p>
*
* @author yw
* @since 2020-12-10
*/
public class IdentifierSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IIdentifierService identifierService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return identifierService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return identifierService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Identifier mockPo() {
        return JMockData.mock(Identifier.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public IdentifierAddForm mockAddForm() {
        return JMockData.mock(IdentifierAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public IdentifierUpdateForm mockUpdateForm() {
        return JMockData.mock(IdentifierUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public IdentifierPageQueryForm mockPageQueryForm() {
        return JMockData.mock(IdentifierPageQueryForm.class, mockConfig);
    }
}