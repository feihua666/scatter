package scatter.identifier.rest.test;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.common.rest.dataconstraint.DataConstraintService;
import scatter.identifier.rest.IdentifierConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, IdentifierConfiguration.class})
@MapperScan("scatter.identifier.rest.mapper")
public class IdentifierTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(IdentifierTestApplication.class, args);
    }
}
