package scatter.identifier.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.identifier.rest.IdentifierConfiguration;
import scatter.identifier.pojo.po.Identifier;
import scatter.identifier.rest.service.IIdentifierService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户登录标识表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-10
 */
@RestController
@RequestMapping(IdentifierConfiguration.CONTROLLER_BASE_PATH + "/inner/identifier")
public class IdentifierInnerController extends BaseInnerController<Identifier> {
 @Autowired
 private IIdentifierService identifierService;

 public IIdentifierService getService(){
     return identifierService;
 }
}
