DROP TABLE IF EXISTS component_grid;
CREATE TABLE `component_grid` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `name` varchar(255) NOT NULL COMMENT '名称，模糊查询',
  `title` varchar(50) DEFAULT NULL COMMENT '主标题',
  `title_icon` varchar(300) DEFAULT NULL COMMENT '主标题图标',
  `sub_title` varchar(50) DEFAULT NULL COMMENT '副标题',
  `sub_title_icon` varchar(300) DEFAULT NULL COMMENT '副标题图标',
  `action_link` varchar(300) DEFAULT NULL COMMENT '跳转地址',
  `is_disabled` tinyint(1) NOT NULL COMMENT '是否禁用',
  `disabled_reason` varchar(255) DEFAULT NULL COMMENT '禁用原因',
  `start_at` datetime DEFAULT NULL COMMENT '开始时间',
  `end_at` datetime DEFAULT NULL COMMENT '结束时间',
  `group_flag` varchar(255) DEFAULT NULL COMMENT '分组标识',
  `seq` int NOT NULL COMMENT '排序,默认按该字段升序排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `extParam` varchar(1000) DEFAULT NULL COMMENT '扩展参数，json',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='宫格配置表，主要用于app使用宫格的可配置场景';
