package scatter.grid.rest.mapper;

import scatter.grid.pojo.po.Grid;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
public interface GridMapper extends IBaseMapper<Grid> {

}
