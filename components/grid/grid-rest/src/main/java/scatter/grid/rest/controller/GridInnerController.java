package scatter.grid.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.grid.rest.GridConfiguration;
import scatter.grid.pojo.po.Grid;
import scatter.grid.rest.service.IGridService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@RestController
@RequestMapping(GridConfiguration.CONTROLLER_BASE_PATH + "/inner/grid")
public class GridInnerController extends BaseInnerController<Grid> {
 @Autowired
 private IGridService gridService;

 public IGridService getService(){
     return gridService;
 }
}
