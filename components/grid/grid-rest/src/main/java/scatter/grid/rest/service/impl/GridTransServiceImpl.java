package scatter.grid.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.grid.pojo.po.Grid;
import scatter.grid.rest.service.IGridService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Component
public class GridTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IGridService gridService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Grid.TRANS_GRID_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Grid.TRANS_GRID_BY_ID)) {
            Grid byId = gridService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Grid.TRANS_GRID_BY_ID)) {
            return gridService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
