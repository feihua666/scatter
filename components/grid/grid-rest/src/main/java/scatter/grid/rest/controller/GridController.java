package scatter.grid.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.grid.rest.GridConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.grid.pojo.po.Grid;
import scatter.grid.pojo.vo.GridVo;
import scatter.grid.pojo.form.GridAddForm;
import scatter.grid.pojo.form.GridUpdateForm;
import scatter.grid.pojo.form.GridPageQueryForm;
import scatter.grid.rest.service.IGridService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Api(tags = "宫格配置表，主要用于app使用宫格的可配置场景相关接口")
@RestController
@RequestMapping(GridConfiguration.CONTROLLER_BASE_PATH + "/grid")
public class GridController extends BaseAddUpdateQueryFormController<Grid, GridVo, GridAddForm, GridUpdateForm, GridPageQueryForm> {
    @Autowired
    private IGridService iGridService;

    @ApiOperation("添加宫格配置表，主要用于app使用宫格的可配置场景")
    @PreAuthorize("hasAuthority('Grid:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GridVo add(@RequestBody @Valid GridAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询宫格配置表，主要用于app使用宫格的可配置场景")
    @PreAuthorize("hasAuthority('Grid:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public GridVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除宫格配置表，主要用于app使用宫格的可配置场景")
    @PreAuthorize("hasAuthority('Grid:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新宫格配置表，主要用于app使用宫格的可配置场景")
    @PreAuthorize("hasAuthority('Grid:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GridVo update(@RequestBody @Valid GridUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询宫格配置表，主要用于app使用宫格的可配置场景")
    @PreAuthorize("hasAuthority('Grid:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<GridVo> getList(GridPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询宫格配置表，主要用于app使用宫格的可配置场景")
    @PreAuthorize("hasAuthority('Grid:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<GridVo> getPage(GridPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("获取可用宫格配置")
    @GetMapping("/getAvailableList")
    @ResponseStatus(HttpStatus.OK)
    public List<GridVo> getAvailableList(String groupFlags) {
        return super.posToVos(iGridService.listAvailable(groupFlags));
    }
}
