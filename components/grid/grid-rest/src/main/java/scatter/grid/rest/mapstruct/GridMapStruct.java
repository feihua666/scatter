package scatter.grid.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.grid.pojo.po.Grid;
import scatter.grid.pojo.vo.GridVo;
import scatter.grid.pojo.form.GridAddForm;
import scatter.grid.pojo.form.GridUpdateForm;
import scatter.grid.pojo.form.GridPageQueryForm;

/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GridMapStruct extends IBaseVoMapStruct<Grid, GridVo>,
                                  IBaseAddFormMapStruct<Grid,GridAddForm>,
                                  IBaseUpdateFormMapStruct<Grid,GridUpdateForm>,
                                  IBaseQueryFormMapStruct<Grid,GridPageQueryForm>{
    GridMapStruct INSTANCE = Mappers.getMapper( GridMapStruct.class );

}
