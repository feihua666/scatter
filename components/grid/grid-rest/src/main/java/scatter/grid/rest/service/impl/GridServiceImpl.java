package scatter.grid.rest.service.impl;

import scatter.grid.pojo.po.Grid;
import scatter.grid.rest.mapper.GridMapper;
import scatter.grid.rest.service.IGridService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.grid.pojo.form.GridAddForm;
import scatter.grid.pojo.form.GridUpdateForm;
import scatter.grid.pojo.form.GridPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Service
@Transactional
public class GridServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<GridMapper, Grid, GridAddForm, GridUpdateForm, GridPageQueryForm> implements IGridService {
    @Override
    public void preAdd(GridAddForm addForm,Grid po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(GridUpdateForm updateForm,Grid po) {
        super.preUpdate(updateForm,po);

    }
}
