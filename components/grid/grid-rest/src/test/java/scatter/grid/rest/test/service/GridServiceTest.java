package scatter.grid.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.grid.pojo.po.Grid;
import scatter.grid.pojo.form.GridAddForm;
import scatter.grid.pojo.form.GridUpdateForm;
import scatter.grid.pojo.form.GridPageQueryForm;
import scatter.grid.rest.test.GridSuperTest;
import scatter.grid.rest.service.IGridService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 宫格配置表，主要用于app使用宫格的可配置场景 服务测试类
* </p>
*
* @author yw
* @since 2021-10-25
*/
@SpringBootTest
public class GridServiceTest extends GridSuperTest{

    @Autowired
    private IGridService gridService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Grid> pos = gridService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}