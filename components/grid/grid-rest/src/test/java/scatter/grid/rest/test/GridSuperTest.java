package scatter.grid.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.grid.pojo.po.Grid;
import scatter.grid.pojo.form.GridAddForm;
import scatter.grid.pojo.form.GridUpdateForm;
import scatter.grid.pojo.form.GridPageQueryForm;
import scatter.grid.rest.service.IGridService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 宫格配置表，主要用于app使用宫格的可配置场景 测试类基类
* </p>
*
* @author yw
* @since 2021-10-25
*/
public class GridSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IGridService gridService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return gridService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return gridService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Grid mockPo() {
        return JMockData.mock(Grid.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public GridAddForm mockAddForm() {
        return JMockData.mock(GridAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public GridUpdateForm mockUpdateForm() {
        return JMockData.mock(GridUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public GridPageQueryForm mockPageQueryForm() {
        return JMockData.mock(GridPageQueryForm.class, mockConfig);
    }
}