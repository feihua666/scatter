package scatter.grid.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.grid.rest.test.GridSuperTest;
/**
* <p>
* 宫格配置表，主要用于app使用宫格的可配置场景 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-10-25
*/
@SpringBootTest
public class GridInnerControllerTest extends GridSuperTest{
    @Test
    void contextLoads() {
    }
}