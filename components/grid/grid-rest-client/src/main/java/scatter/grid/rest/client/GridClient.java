package scatter.grid.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Component
@FeignClient(value = "Grid-client")
public interface GridClient {

}
