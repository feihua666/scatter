package scatter.grid.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Setter
@Getter
@ApiModel(value="宫格配置表，主要用于app使用宫格的可配置场景添加表单对象")
public class GridAddForm extends BaseAddForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，模糊查询",required = true)
    private String name;

    @ApiModelProperty(value = "主标题")
    private String title;

    @ApiModelProperty(value = "主标题图标")
    private String titleIcon;

    @ApiModelProperty(value = "副标题")
    private String subTitle;

    @ApiModelProperty(value = "副标题图标")
    private String subTitleIcon;

    @ApiModelProperty(value = "跳转地址")
    private String actionLink;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序,默认按该字段升序排序",required = true)
    private Integer seq;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "扩展参数，json")
    private String extParam;

}
