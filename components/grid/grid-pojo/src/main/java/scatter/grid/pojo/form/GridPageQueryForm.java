package scatter.grid.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 宫格配置表，主要用于app使用宫格的可配置场景分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-25
 */
@Setter
@Getter
@ApiModel(value="宫格配置表，主要用于app使用宫格的可配置场景分页表单对象")
public class GridPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "主标题")
    private String title;

    @ApiModelProperty(value = "主标题图标")
    private String titleIcon;

    @ApiModelProperty(value = "副标题")
    private String subTitle;

    @ApiModelProperty(value = "副标题图标")
    private String subTitleIcon;

    @ApiModelProperty(value = "跳转地址")
    private String actionLink;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startAt;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endAt;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @OrderBy(asc = true)
    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "扩展参数，json")
    private String extParam;

}
