const basePath = '' + '/grid'
const GridUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/GridSearchList',
        add: '/GridAdd',
        update: '/GridUpdate',
    }
}
export default GridUrl