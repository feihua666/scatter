import GridUrl from './GridUrl.js'
const GridForm = [
    {
        GridSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {

        field: {
            name: 'title',
        },
        element:{
            label: '主标题',
        }
    },
    {

        field: {
            name: 'titleIcon',
        },
        element:{
            label: '主标题图标',
        }
    },
    {

        field: {
            name: 'subTitle',
        },
        element:{
            label: '副标题',
        }
    },
    {

        field: {
            name: 'subTitleIcon',
        },
        element:{
            label: '副标题图标',
        }
    },
    {

        field: {
            name: 'actionLink',
        },
        element:{
            label: '跳转地址',
        }
    },
    {
        GridSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'isDisabled',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            required: true,
        }
    },
    {

        field: {
            name: 'disabledReason',
        },
        element:{
            label: '禁用原因',
        }
    },
    {

        field: {
            name: 'startAt',
        },
        element:{
            label: '开始时间',
        }
    },
    {

        field: {
            name: 'endAt',
        },
        element:{
            label: '结束时间',
        }
    },
    {

        field: {
            name: 'groupFlag',
        },
        element:{
            label: '分组标识',
        }
    },
    {
        GridSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'seq',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        }
    },
    {

        field: {
            name: 'remark',
        },
        element:{
            label: '备注',
        }
    },
    {

        field: {
            name: 'extParam',
        },
        element:{
            label: '扩展参数',
        }
    },

]
export default GridForm