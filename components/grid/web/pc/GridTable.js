const GridTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'title',
        label: '主标题'
    },
    {
        prop: 'titleIcon',
        label: '主标题图标'
    },
    {
        prop: 'subTitle',
        label: '副标题'
    },
    {
        prop: 'subTitleIcon',
        label: '副标题图标'
    },
    {
        prop: 'actionLink',
        label: '跳转地址'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用原因'
    },
    {
        prop: 'startAt',
        label: '开始时间'
    },
    {
        prop: 'endAt',
        label: '结束时间'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'seq',
        label: '排序'
    },
    {
        prop: 'remark',
        label: '备注'
    },
    {
        prop: 'extParam',
        label: '扩展参数'
    },
]
export default GridTable