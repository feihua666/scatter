import GridUrl from './GridUrl.js'

const GridRoute = [
    {
        path: GridUrl.router.searchList,
        component: () => import('./element/GridSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'GridSearchList',
            name: '宫格配置表，主要用于app使用宫格的可配置场景管理'
        }
    },
    {
        path: GridUrl.router.add,
        component: () => import('./element/GridAdd'),
        meta: {
            code:'GridAdd',
            name: '宫格配置表，主要用于app使用宫格的可配置场景添加'
        }
    },
    {
        path: GridUrl.router.update,
        component: () => import('./element/GridUpdate'),
        meta: {
            code:'GridUpdate',
            name: '宫格配置表，主要用于app使用宫格的可配置场景修改'
        }
    },
]
export default GridRoute