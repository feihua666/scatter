import GridForm from './GridForm.js'
import GridTable from './GridTable.js'
import GridUrl from './GridUrl.js'
const GridMixin = {
    computed: {
        GridFormOptions() {
            return this.$stDynamicFormTools.options(GridForm,this.$options.name)
        },
        GridTableOptions() {
            return GridTable
        },
        GridUrl(){
            return GridUrl
        }
    },
}
export default GridMixin