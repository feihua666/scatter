package scatter.config.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 参数配置分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="参数配置分页表单对象")
public class ConfigPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "参数配置名称")
    private String name;

    @ApiModelProperty(value = "参数配置值")
    private String value;

    @ApiModelProperty(value = "配置值的类型，字典id")
    private String valueTypeDictId;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用/启用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;

}
