package scatter.config.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 参数配置响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Setter
@Getter
@ApiModel(value="参数配置响应对象")
public class ConfigVo extends BaseIdVo {

    @ApiModelProperty(value = "编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "参数配置名称")
    private String name;

    @ApiModelProperty(value = "参数配置值")
    private String value;

    @ApiModelProperty(value = "配置值的类型，字典id")
    private String valueTypeDictId;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用/启用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;

}
