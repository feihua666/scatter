package scatter.config.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 参数配置表
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_config")
@ApiModel(value="Config对象", description="参数配置表")
public class Config extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_CONFIG_BY_ID = "trans_config_by_id_scatter.config.pojo.po";

    @ApiModelProperty(value = "编码,模糊查询")
    private String code;

    @ApiModelProperty(value = "参数配置名称")
    private String name;

    @ApiModelProperty(value = "参数配置值")
    private String value;

    @ApiModelProperty(value = "配置值的类型，字典id")
    private String valueTypeDictId;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用/启用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;


}
