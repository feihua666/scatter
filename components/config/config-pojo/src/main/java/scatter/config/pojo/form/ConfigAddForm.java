package scatter.config.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 参数配置添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@PropValid
@Setter
@Getter
@ApiModel(value="参数配置添加表单对象")
public class ConfigAddForm extends BaseAddForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码,模糊查询",required = true)
    private String code;

    @NotEmpty(message="参数配置名称不能为空")
    @ApiModelProperty(value = "参数配置名称",required = true)
    private String name;

    @NotEmpty(message="参数配置值不能为空")
    @ApiModelProperty(value = "参数配置值",required = true)
    private String value;

    @NotEmpty(message="配置值的类型不能为空")
    @ApiModelProperty(value = "配置值的类型，字典id",required = true)
    private String valueTypeDictId;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用原因不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用/启用原因")
    private String disabledReason;

    @ApiModelProperty(value = "描述")
    private String remark;

}
