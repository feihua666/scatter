package scatter.config.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.config.rest.ConfigConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.config.pojo.po.Config;
import scatter.config.pojo.vo.ConfigVo;
import scatter.config.pojo.form.ConfigAddForm;
import scatter.config.pojo.form.ConfigUpdateForm;
import scatter.config.pojo.form.ConfigPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping(ConfigConfiguration.CONTROLLER_BASE_PATH + "/config")
@Api(tags = "参数配置")
public class ConfigController extends BaseAddUpdateQueryFormController<Config, ConfigVo, ConfigAddForm, ConfigUpdateForm, ConfigPageQueryForm> {


     @Override
	 @ApiOperation("添加参数配置")
     @PreAuthorize("hasAuthority('Config:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigVo add(@RequestBody @Valid ConfigAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询参数配置")
     @PreAuthorize("hasAuthority('Config:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public ConfigVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除参数配置")
     @PreAuthorize("hasAuthority('Config:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新参数配置")
     @PreAuthorize("hasAuthority('Config:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public ConfigVo update(@RequestBody @Valid ConfigUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询参数配置")
    @PreAuthorize("hasAuthority('Config:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<ConfigVo> getList(ConfigPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询参数配置")
    @PreAuthorize("hasAuthority('Config:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ConfigVo> getPage(ConfigPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
