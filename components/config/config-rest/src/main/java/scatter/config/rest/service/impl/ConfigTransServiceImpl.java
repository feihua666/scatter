package scatter.config.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.config.pojo.po.Config;
import scatter.config.rest.service.IConfigService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 参数配置翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Component
public class ConfigTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IConfigService configService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Config.TRANS_CONFIG_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Config.TRANS_CONFIG_BY_ID)) {
            Config byId = configService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Config.TRANS_CONFIG_BY_ID)) {
            return configService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
