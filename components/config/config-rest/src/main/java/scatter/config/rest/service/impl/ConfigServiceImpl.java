package scatter.config.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.config.pojo.form.ConfigAddForm;
import scatter.config.pojo.form.ConfigPageQueryForm;
import scatter.config.pojo.form.ConfigUpdateForm;
import scatter.config.pojo.po.Config;
import scatter.config.rest.mapper.ConfigMapper;
import scatter.config.rest.service.IConfigService;
/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Service
@Transactional
public class ConfigServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ConfigMapper, Config, ConfigAddForm, ConfigUpdateForm, ConfigPageQueryForm> implements IConfigService {
    @Override
    public void preAdd(ConfigAddForm addForm,Config po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),Config::getCode,false);
        }

    }

    @Override
    public void preUpdate(ConfigUpdateForm updateForm,Config po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            Config byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),Config::getCode,false);
            }
        }

    }
}
