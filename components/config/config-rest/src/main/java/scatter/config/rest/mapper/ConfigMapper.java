package scatter.config.rest.mapper;

import scatter.config.pojo.po.Config;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface ConfigMapper extends IBaseMapper<Config> {

}
