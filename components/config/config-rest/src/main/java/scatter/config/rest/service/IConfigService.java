package scatter.config.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.config.pojo.po.Config;
/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
public interface IConfigService extends IBaseService<Config> {

    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Config getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Config>lambdaQuery().eq(Config::getCode, code));
    }

}
