package scatter.config.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.config.pojo.form.ConfigAddForm;
import scatter.config.pojo.form.ConfigPageQueryForm;
import scatter.config.pojo.form.ConfigUpdateForm;
import scatter.config.pojo.po.Config;
import scatter.config.pojo.vo.ConfigVo;

/**
 * <p>
 * 参数配置 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConfigMapStruct extends IBaseVoMapStruct<Config, ConfigVo>,
        IBaseAddFormMapStruct<Config,ConfigAddForm>,
        IBaseUpdateFormMapStruct<Config,ConfigUpdateForm>,
        IBaseQueryFormMapStruct<Config,ConfigPageQueryForm> {
    ConfigMapStruct INSTANCE = Mappers.getMapper( ConfigMapStruct.class );

}
