package scatter.config.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.config.pojo.po.Config;
import scatter.config.rest.service.IConfigService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 参数配置表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-07
 */
@RestController
@RequestMapping("/inner/config")
public class ConfigInnerController extends BaseInnerController<Config> {
 @Autowired
 private IConfigService configService;

 public IConfigService getService(){
     return configService;
 }
}
