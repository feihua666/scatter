DROP TABLE IF EXISTS component_config;
CREATE TABLE `component_config` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `code` varchar(50) NOT NULL COMMENT '编码,模糊查询',
  `name` varchar(50) NOT NULL COMMENT '参数配置名称',
  `value` varchar(500) NOT NULL COMMENT '参数配置值',
  `value_type_dict_id` varchar(20) NOT NULL COMMENT '配置值的类型，字典id',
  `is_disabled` tinyint(1) NOT NULL COMMENT '是否禁用',
  `disabled_reason` varchar(255) DEFAULT NULL COMMENT '禁用/启用原因',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `is_disabled` (`is_disabled`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='参数配置表';
