package scatter.config.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.config.pojo.po.Config;
import scatter.config.pojo.form.ConfigAddForm;
import scatter.config.pojo.form.ConfigUpdateForm;
import scatter.config.pojo.form.ConfigPageQueryForm;
import scatter.config.rest.service.IConfigService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 参数配置 测试类基类
* </p>
*
* @author yw
* @since 2020-12-07
*/
public class ConfigSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IConfigService configService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return configService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return configService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Config mockPo() {
        return JMockData.mock(Config.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ConfigAddForm mockAddForm() {
        return JMockData.mock(ConfigAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ConfigUpdateForm mockUpdateForm() {
        return JMockData.mock(ConfigUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ConfigPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ConfigPageQueryForm.class, mockConfig);
    }
}