package scatter.config.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.config.pojo.po.Config;
import scatter.config.pojo.form.ConfigAddForm;
import scatter.config.pojo.form.ConfigUpdateForm;
import scatter.config.pojo.form.ConfigPageQueryForm;
import scatter.config.rest.test.ConfigSuperTest;
import scatter.config.rest.service.IConfigService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 参数配置 服务测试类
* </p>
*
* @author yw
* @since 2020-12-07
*/
@SpringBootTest
public class ConfigServiceTest extends ConfigSuperTest{

    @Autowired
    private IConfigService configService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Config> pos = configService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}