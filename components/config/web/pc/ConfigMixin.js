import ConfigForm from './ConfigForm.js'
import ConfigTable from './ConfigTable.js'
import ConfigUrl from './ConfigUrl.js'

const ConfigMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(ConfigForm,this.$options.name)
        },
        computedTableOptions() {
            return ConfigTable
        },
        computedUrl(){
            return ConfigUrl
        }
    },
}
export default ConfigMixin