const basePath = '' + '/config'
const AreaUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ConfigSearchList',
        add: '/ConfigAdd',
        update: '/ConfigUpdate',
    }
}
export default AreaUrl