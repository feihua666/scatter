const ConfigForm = [
    {
        field: {
            name: 'code'
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        field: {
            name: 'name'
        },
        element:{
            label: '参数配置名称',
            required: true,
        }
    },
    {
        field: {
            name: 'value'
        },
        element:{
            label: '参数配置值',
            required: true,
        }
    },
    {
        field: {
            name: 'valueTypeDictId'
        },
        element:{
            label: '配置值的类型',
            required: true,
        }
    },
    {
        field: {
            name: 'isDisabled'
        },
        element:{
            label: '是否禁用',
            required: true,
        }
    },
    {
        field: {
            name: 'disabledReason'
        },
        element:{
            label: '禁用/启用原因',
        }
    },
    {
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        }
    },
]
export default ConfigForm