import ConfigUrl from './ConfigUrl.js'

const ConfigRoute = [
    {
        path: ConfigUrl.router.searchList,
        component: () => import('./element/ConfigSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ConfigSearchList',
            name: '参数配置管理'
        }
    },
    {
        path: ConfigUrl.router.add,
        component: () => import('./element/ConfigAdd'),
        meta: {
            code:'ConfigAdd',
            name: '参数配置添加'
        }
    },
    {
        path: ConfigUrl.router.update,
        component: () => import('./element/ConfigUpdate'),
        meta: {
            code:'ConfigUpdate',
            name: '参数配置修改'
        }
    },
]
export default ConfigRoute