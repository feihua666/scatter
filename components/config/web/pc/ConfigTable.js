const ConfigTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '参数配置名称'
    },
    {
        prop: 'value',
        label: '参数配置值'
    },
    {
        prop: 'valueTypeDictId',
        label: '配置值的类型'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用'
    },
    {
        prop: 'disabledReason',
        label: '禁用/启用原因'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ConfigTable