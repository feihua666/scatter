import PayUrl from './PayUrl.js'
const PayForm = [
    {
        PaySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'serialNo',
        },
        element:{
            label: '流水号',
            required: true,
        }
    },
    {
        PaySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderNo',
        },
        element:{
            label: '订单号',
            required: true,
        }
    },

    {
        PaySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'payStatusDictId',
        },
        element:{
            label: '支付状态',
            required: true,
            type: 'selectDict',
            options:{
                groupCode: 'order_status'
            }
        }
    },
    {
        PaySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'payChannelDictId',
        },
        element:{
            label: '支付渠道',
            required: true,
            type: 'selectDict',
            options:{
                groupCode: 'pay_channel'
            }
        }
    },


]
export default PayForm