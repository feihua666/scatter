import PayForm from './PayForm.js'
import PayTable from './PayTable.js'
import PayUrl from './PayUrl.js'
const PayMixin = {
    computed: {
        PayFormOptions() {
            return this.$stDynamicFormTools.options(PayForm,this.$options.name)
        },
        PayTableOptions() {
            return PayTable
        },
        PayUrl(){
            return PayUrl
        }
    },
}
export default PayMixin