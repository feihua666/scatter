const PayTable = [
    {
        prop: 'serialNo',
        label: '流水号'
    },
    {
        prop: 'orderNo',
        label: '订单号'
    },
    {
        prop: 'currencyDictName',
        label: '货币类型'
    },
    {
        prop: 'payerCurrencyDictName',
        label: '用户支付货币类型'
    },
    {
        prop: 'payStatusDictName',
        label: '支付状态'
    },
    {
        prop: 'payChannelDictName',
        label: '支付渠道'
    },
    {
        prop: 'userNickname',
        label: '支付的用户'
    },
    {
        prop: 'amount',
        label: '支付金额'
    },
    {
        prop: 'bankId',
        label: '付款银行'
    },
    {
        prop: 'bankCardId',
        label: '付款银行卡'
    },
    {
        prop: 'successAt',
        label: '支付完成时间'
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default PayTable