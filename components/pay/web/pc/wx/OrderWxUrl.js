const basePath = '' + '/wx/order-wx'
const OrderWxUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/OrderWxSearchList',
    }
}
export default OrderWxUrl