import OrderWxUrl from './OrderWxUrl.js'
const OrderWxForm = [
    {
        OrderWxSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'orderId',
        },
        element:{
            label: '订单id',
            required: true,
        }
    },
    {
        OrderWxSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'outTradeNo',
        },
        element:{
            label: '订单号',
            required: true,
        }
    },

    {
        OrderWxSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'spAppid',
        },
        element:{
            label: '服务商应用ID',
            required: true,
        }
    },
    {
        OrderWxSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'spMchid',
        },
        element:{
            label: '服务商户号',
            required: true,
        }
    },

    {

        field: {
            name: 'subAppid',
        },
        element:{
            label: '子商户应用ID',
        }
    },
    {

        field: {
            name: 'subMchid',
        },
        element:{
            label: '子商户号',
        }
    },
    {

        field: {
            name: 'prepayId',
        },
        element:{
            label: '预支付id',
        }
    },


]
export default OrderWxForm