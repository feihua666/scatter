const OrderWxTable = [
    {
        prop: 'orderId',
        label: '订单id'
    },
    {
        prop: 'outTradeNo',
        label: '订单号'
    },
    {
        prop: 'spAppCode',
        label: '自定义app_code'
    },
    {
        prop: 'spAppid',
        label: '服务商应用ID'
    },
    {
        prop: 'spMchid',
        label: '服务商户号'
    },
    {
        prop: 'subAppCode',
        label: '自定义子app_code'
    },
    {
        prop: 'subAppid',
        label: '子商户应用ID'
    },
    {
        prop: 'subMchid',
        label: '子商户号'
    },
    {
        prop: 'prepayId',
        label: '预支付id'
    },
    {
        prop: 'prepayIdExpireAt',
        label: '预支付id失效时间'
    },
    {
        prop: 'isPrepayIdExpire',
        label: '预支付id是否过期失效'
    },
]
export default OrderWxTable