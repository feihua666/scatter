import OrderWxForm from './OrderWxForm.js'
import OrderWxTable from './OrderWxTable.js'
import OrderWxUrl from './OrderWxUrl.js'
const OrderWxMixin = {
    computed: {
        OrderWxFormOptions() {
            return this.$stDynamicFormTools.options(OrderWxForm,this.$options.name)
        },
        OrderWxTableOptions() {
            return OrderWxTable
        },
        OrderWxUrl(){
            return OrderWxUrl
        }
    },
}
export default OrderWxMixin