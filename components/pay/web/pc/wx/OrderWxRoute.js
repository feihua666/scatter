import OrderWxUrl from './OrderWxUrl.js'

const OrderWxRoute = [
    {
        path: OrderWxUrl.router.searchList,
        component: () => import('./element/OrderWxSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'OrderWxSearchList',
            name: '订单微信扩展管理'
        }
    },
]
export default OrderWxRoute