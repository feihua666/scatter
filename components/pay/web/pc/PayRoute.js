import PayUrl from './PayUrl.js'

const PayRoute = [
    {
        path: PayUrl.router.searchList,
        component: () => import('./element/PaySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PaySearchList',
            name: '支付管理'
        }
    },

]
export default PayRoute