package scatter.pay.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.pay.rest.PayConfiguration;
import scatter.pay.pojo.po.Pay;
import scatter.pay.rest.service.IPayService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 支付表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@RestController
@RequestMapping(PayConfiguration.CONTROLLER_BASE_PATH + "/inner/pay")
public class PayInnerController extends BaseInnerController<Pay> {
 @Autowired
 private IPayService payService;

 public IPayService getService(){
     return payService;
 }
}
