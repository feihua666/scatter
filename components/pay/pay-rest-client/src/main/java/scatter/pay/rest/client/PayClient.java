package scatter.pay.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 支付表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Component
@FeignClient(value = "Pay-client")
public interface PayClient {

}
