package scatter.pay.rest.wx.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 订单微信扩展表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Component
@FeignClient(value = "OrderWx-client")
public interface OrderWxClient {

}
