package scatter.pay.rest.service;

/**
 * <p>
 * 支付流水号生成
 * </p>
 *
 * @author yangwei
 * @since 2021-09-08 15:55
 */
public interface IPaySeriaNoNoResolver<T>{
	/**
	 * 获取支付流水号
	 * @param param
	 * @return
	 */
	public String resolve(T param);
}
