package scatter.pay.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.pay.pojo.po.Pay;
import scatter.pay.pojo.vo.PayVo;
import scatter.pay.pojo.form.PayAddForm;
import scatter.pay.pojo.form.PayUpdateForm;
import scatter.pay.pojo.form.PayPageQueryForm;

/**
 * <p>
 * 支付 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PayMapStruct extends IBaseVoMapStruct<Pay, PayVo>,
                                  IBaseAddFormMapStruct<Pay,PayAddForm>,
                                  IBaseUpdateFormMapStruct<Pay,PayUpdateForm>,
                                  IBaseQueryFormMapStruct<Pay,PayPageQueryForm>{
    PayMapStruct INSTANCE = Mappers.getMapper( PayMapStruct.class );

}
