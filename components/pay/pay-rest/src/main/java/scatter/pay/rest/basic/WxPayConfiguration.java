package scatter.pay.rest.basic;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * <p>
 * 微信支付相关配置
 * </p>
 *
 * @author yangwei
 * @since 2021-07-03 16:00
 */
public class WxPayConfiguration {

	@Autowired
	private WxPayProperties wxPayProperties;

	@Bean
	public WxPayService wxPayService(){


		final List<WxPayProperties.WxPayConfig> configs = this.wxPayProperties.getConfigs();
		if (configs == null) {
			throw new RuntimeException("请配置微信支付相关配置");
		}

		CustomWxPayServiceImpl customWxPayService = new CustomWxPayServiceImpl(wxPayProperties);
		WxPayConfig payConfig = null;
		for (WxPayProperties.WxPayConfig config : configs) {
			payConfig = new WxPayConfig();
			payConfig.setAppId(config.getAppId());
			payConfig.setMchId(config.getMchId());
			payConfig.setMchKey(config.getMchKey());
			payConfig.setSubMchId(config.getSubMchId());
			payConfig.setSubAppId(config.getSubAppId());
			payConfig.setKeyPath(config.getKeyPath());
			customWxPayService.addConfig(config.getMchId(),payConfig);
		}

		return customWxPayService;
	}
}
