package scatter.pay.rest.wx.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.pojo.wx.vo.OrderWxVo;
import scatter.pay.pojo.wx.form.OrderWxAddForm;
import scatter.pay.pojo.wx.form.OrderWxUpdateForm;
import scatter.pay.pojo.wx.form.OrderWxPageQueryForm;

/**
 * <p>
 * 订单微信扩展 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderWxMapStruct extends IBaseVoMapStruct<OrderWx, OrderWxVo>,
                                  IBaseAddFormMapStruct<OrderWx,OrderWxAddForm>,
                                  IBaseUpdateFormMapStruct<OrderWx,OrderWxUpdateForm>,
                                  IBaseQueryFormMapStruct<OrderWx,OrderWxPageQueryForm>{
    OrderWxMapStruct INSTANCE = Mappers.getMapper( OrderWxMapStruct.class );

}
