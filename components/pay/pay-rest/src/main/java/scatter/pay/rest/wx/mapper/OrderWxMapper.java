package scatter.pay.rest.wx.mapper;

import scatter.pay.pojo.wx.po.OrderWx;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 订单微信扩展表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
public interface OrderWxMapper extends IBaseMapper<OrderWx> {

}
