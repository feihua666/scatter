package scatter.pay.rest.service.impl;

import scatter.pay.pojo.po.Pay;
import scatter.pay.rest.mapper.PayMapper;
import scatter.pay.rest.service.IPayService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.pay.pojo.form.PayAddForm;
import scatter.pay.pojo.form.PayUpdateForm;
import scatter.pay.pojo.form.PayPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 支付表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Service
@Transactional
public class PayServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PayMapper, Pay, PayAddForm, PayUpdateForm, PayPageQueryForm> implements IPayService {
    @Override
    public void preAdd(PayAddForm addForm,Pay po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getOrderNo())) {
            // 订单号已存在不能添加
            assertByColumn(addForm.getOrderNo(),Pay::getOrderNo,false);
        }

    }

    @Override
    public void preUpdate(PayUpdateForm updateForm,Pay po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getOrderNo())) {
            Pay byId = getById(updateForm.getId());
            // 如果订单号有改动
            if (!isEqual(updateForm.getOrderNo(), byId.getOrderNo())) {
                // 订单号已存在不能修改
                assertByColumn(updateForm.getOrderNo(),Pay::getOrderNo,false);
            }
        }

    }
}
