package scatter.pay.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.pay.pojo.po.Pay;
import scatter.common.rest.service.IBaseService;
import scatter.pay.pojo.form.PayAddForm;
import scatter.pay.pojo.form.PayUpdateForm;
import scatter.pay.pojo.form.PayPageQueryForm;
import java.util.List;
/**
 * <p>
 * 支付表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
public interface IPayService extends IBaseService<Pay> {


    /**
     * 根据订单号查询
     * @param orderNo
     * @return
     */
    default Pay getByOrderNo(String orderNo) {
        Assert.hasText(orderNo,"orderNo 不能为空");
        return getOne(Wrappers.<Pay>lambdaQuery().eq(Pay::getOrderNo, orderNo));
    }

    /**
     * 根据订单ids查询
     * @param orderIds
     * @return
     */
    default List<Pay> getByOrderIds(List<String> orderIds) {
        Assert.notEmpty(orderIds,"orderIds 不能为空");
        return list(Wrappers.<Pay>lambdaQuery().in(Pay::getOrderId, orderIds));
    }
}
