package scatter.pay.rest.service.impl;

import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.bean.result.WxPayRefundResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.rest.exception.BusinessException;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.param.RefundOrderParam;
import scatter.order.pojo.po.Order;
import scatter.order.pojo.po.OrderRefund;
import scatter.order.rest.service.impl.DefaultRefundOrderFrameworkServiceImpl;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.rest.service.IPayService;
import scatter.pay.rest.wx.service.IOrderWxService;
import scatter.pay.rest.service.IWxOpenIdResolver;
import scatter.pay.rest.third.ThirdNotifyUrlHelper;

/**
 * <p>
 * 三方支付退款
 * </p>
 *
 * @author yangwei
 * @since 2021-08-06 13:44
 */
@Slf4j
public class DefaultThirdPayRefundOrderServiceImpl extends DefaultRefundOrderFrameworkServiceImpl {

	@Autowired
	private ThirdNotifyUrlHelper thirdNotifyUrlHelper;

	@Autowired
	private DictService dictService;
	@Autowired
	private WxPayService wxPayService;

	@Autowired
	private IOrderWxService iOrderWxService;

	@Autowired
	private IWxOpenIdResolver iWxOpenIdResolver;
	@Autowired
	private IPayService iPayService;

	@Override
	protected void thirdOtherOrder(RefundOrderParam param, Order order, OrderRefund orderRefund) {
		// 三方退款
		super.thirdOtherOrder(param, order,orderRefund);
		boolean equalValueByDictItemId = dictService.isEqualValueByDictItemId(order.getPayTypeDictId(), OrderConstants.OrderPayTypeDictItem.off_line_pay.itemValue());
		if (equalValueByDictItemId) {
			log.info("线下支付订单，不发起三方退款");
			return;
		}
		// 判断支付渠道
		String payChannelDictValue = dictService.getValueById(order.getPayChannelDictId());
		if (OrderConstants.OrderPayChannelDictItem.wx_jsapi.itemValue().equals(payChannelDictValue)) {
			ThirdNotifyUrlHelper.NotifyUrlParam notifyUrlParam = new ThirdNotifyUrlHelper.NotifyUrlParam();
			notifyUrlParam.setThirdType(ThirdNotifyUrlHelper.ThirdType.wx_refund);

			OrderWx byOutTradeNo = iOrderWxService.getByOutTradeNo(order.getOrderNo());

			wxPayService.switchover(byOutTradeNo.getSpMchid());
			notifyUrlParam.setWxAppCode(byOutTradeNo.getSpMchid());
			// 发起退款
			WxPayRefundRequest wxPayRefundRequest = new WxPayRefundRequest();
			wxPayRefundRequest.setOutTradeNo(order.getOrderNo());
			wxPayRefundRequest.setOutRefundNo(orderRefund.getRefundNo());
			wxPayRefundRequest.setRefundDesc(param.getRemark());
			wxPayRefundRequest.setRefundFee(orderRefund.getAmount());
			wxPayRefundRequest.setTotalFee(order.getTotalAmount());
			wxPayRefundRequest.setNotifyUrl(thirdNotifyUrlHelper.getNotifyUrl(notifyUrlParam));
			wxPayService.switchover(byOutTradeNo.getSpMchid());

			log.info("微信jsapi退款请求参数={}",toJsonStr(wxPayRefundRequest));
			WxPayRefundResult wxPayRefundResult = null;
			try {
				wxPayRefundResult = wxPayService.refund(wxPayRefundRequest);
				log.info("微信jsapi退款返回结果={}",toJsonStr(wxPayRefundResult));
			} catch (WxPayException e) {
				log.error("微信jsapi退款异常,直接抛出",e);
				throw new BusinessException("退款失败，微信api异常",e);
			}


		}else {
			log.warn("退款不支持的支付渠道，orderId={},payChannelDictValue={}",order.getId(),payChannelDictValue);
			throw new BusinessException("退款失败，退款不支持的支付渠道,payChannelDictValue=" + payChannelDictValue);
		}
	}
}
