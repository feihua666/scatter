package scatter.pay.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.pay.pojo.po.Pay;
import scatter.pay.rest.service.IPayService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 支付翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Component
public class PayTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IPayService payService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Pay.TRANS_PAY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Pay.TRANS_PAY_BY_ID)) {
            Pay byId = payService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Pay.TRANS_PAY_BY_ID)) {
            return payService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
