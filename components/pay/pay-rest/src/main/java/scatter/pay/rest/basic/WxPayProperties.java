package scatter.pay.rest.basic;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by yangwei
 * Created at 2021-07-03 15:29:55
 */
@Data
@Component
@ConfigurationProperties(prefix = "scatter.pay.wx")
public class WxPayProperties {

    private List<WxPayConfig> configs;

    @Data
    public static class WxPayConfig {
        /**
         * 自定义编码
         */
        private String appCode;
        /**
         * 自定义名称
         */
        private String appName;


        /**
         * 设置微信公众号的appId
         */
        private String appId;

        /**
         * 微信支付商户号
         */
        private String mchId;
        /**
         * 微信支付商户密钥
         */
        private String mchKey;
        /**
         * 服务商模式下的子商户公众账号ID
         */
        private String subAppId;

        /**
         * 服务商模式下的子商户号
         */
        private String subMchId;

        /**
         * keyPath p12证书的位置，可以指定绝对路径，也可以指定类路径（以classpath:开头）
         */
        private String keyPath;

    }

    /**
     * 获取appname
     * @param appCodeOrAppId
     * @return
     */
    public String abtainAppNameByAppCodeOrAppId(String appCodeOrAppId){
        return Optional.ofNullable(abtainConfigByAppCodeOrAppId(appCodeOrAppId)).map(WxPayConfig::getAppName).orElse(null);
    }

    /**
     * 获取 WxPayConfig
     * @param appCodeOrAppId
     * @return
     */
    public WxPayConfig abtainConfigByAppCodeOrAppId(String appCodeOrAppId){
        return this.getConfigs().stream()
                .filter(item -> StringUtils.equals(appCodeOrAppId, item.getAppCode()) || StringUtils.equals(appCodeOrAppId, item.getAppId()))
                .findFirst()
                .orElse(null);
    }

    /**
     * 获取 appname
     * @param appCodeOrMchId
     * @return
     */
    public String abtainAppNameByAppCodeOrMchId(String appCodeOrMchId){
        return Optional.ofNullable(abtainConfigByAppCodeOrMchId(appCodeOrMchId)).map(WxPayConfig::getAppName).orElse(null);
    }
    /**
     * 获取 WxPayConfig
     * @param appCodeOrMchId
     * @return
     */
    public WxPayConfig abtainConfigByAppCodeOrMchId(String appCodeOrMchId){
        return this.getConfigs().stream()
                .filter(item -> StringUtils.equals(appCodeOrMchId, item.getAppCode()) || StringUtils.equals(appCodeOrMchId, item.getMchId()))
                .findFirst()
                .orElse(null);
    }
}
