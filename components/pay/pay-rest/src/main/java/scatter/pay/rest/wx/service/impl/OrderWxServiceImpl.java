package scatter.pay.rest.wx.service.impl;

import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.rest.wx.mapper.OrderWxMapper;
import scatter.pay.rest.wx.service.IOrderWxService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.pay.pojo.wx.form.OrderWxAddForm;
import scatter.pay.pojo.wx.form.OrderWxUpdateForm;
import scatter.pay.pojo.wx.form.OrderWxPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 订单微信扩展表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Service
@Transactional
public class OrderWxServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<OrderWxMapper, OrderWx, OrderWxAddForm, OrderWxUpdateForm, OrderWxPageQueryForm> implements IOrderWxService {
    @Override
    public void preAdd(OrderWxAddForm addForm,OrderWx po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getOutTradeNo())) {
            // 订单号已存在不能添加
            assertByColumn(addForm.getOutTradeNo(),OrderWx::getOutTradeNo,false);
        }

    }

    @Override
    public void preUpdate(OrderWxUpdateForm updateForm,OrderWx po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getOutTradeNo())) {
            OrderWx byId = getById(updateForm.getId());
            // 如果订单号有改动
            if (!isEqual(updateForm.getOutTradeNo(), byId.getOutTradeNo())) {
                // 订单号已存在不能修改
                assertByColumn(updateForm.getOutTradeNo(),OrderWx::getOutTradeNo,false);
            }
        }

    }
}
