package scatter.pay.rest.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.notify.WxPayRefundNotifyResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.rest.validation.DictService;
import scatter.order.pojo.constant.OrderConstants;
import scatter.order.pojo.param.UpdateOrderStatusParam;
import scatter.order.pojo.po.Order;
import scatter.order.rest.service.IOrderService;
import scatter.order.rest.service.impl.DefaultOrderStatusFrameworkServiceImpl;
import scatter.pay.pojo.po.Pay;
import scatter.pay.rest.service.IPaySeriaNoNoResolver;
import scatter.pay.rest.service.IPayService;

import java.util.Optional;

/**
 * <p>
 * 支付流水支持
 * </p>
 *
 * @author yangwei
 * @since 2021-09-08 13:50
 */
@Slf4j
public class DefaultThirdPayOrderStatusFrameworkServiceImpl extends DefaultOrderStatusFrameworkServiceImpl {


	@Autowired
	private IPayService iPayService;
	@Autowired
	private IOrderService iOrderService;

	@Autowired(required = false)
	private IPaySeriaNoNoResolver iPaySerialNoResolver;


	@Autowired
	private DictService dictService;


	@Override
	protected void postUpdate(UpdateOrderStatusParam param) {
		super.postUpdate(param);
		// 要更新的状态
		OrderConstants.OrderStatusItem orderStatusItem = OrderConstants.OrderStatusItem.valueOf(param.getStatus());
		// 如果为已支付，生成支付流水
		Order order = iOrderService.getByOrderNo(param.getOrderNo());

		// 支付成功添加支付流水
		if (orderStatusItem == OrderConstants.OrderStatusItem.paid) {
			WxPayOrderNotifyResult wxPayOrderNotifyResult = (WxPayOrderNotifyResult) Optional.ofNullable(param.getExt()).map(i -> i.get("wxPayOrderNotifyResult")).orElse(null);
			if (wxPayOrderNotifyResult != null) {
				handleWxPayOrderNotifyResult(param,order,orderStatusItem,wxPayOrderNotifyResult);
			}
		}
		// 退款成功添加退款流水
		else if (orderStatusItem == OrderConstants.OrderStatusItem.refund) {
			WxPayRefundNotifyResult wxPayRefundNotifyResult =  (WxPayRefundNotifyResult) Optional.ofNullable(param.getExt()).map(i -> i.get("wxPayRefundNotifyResult")).orElse(null);
			if (wxPayRefundNotifyResult != null) {
				handleWxPayRefundNotifyResult(param,order,orderStatusItem,wxPayRefundNotifyResult);
			}
		}


	}

	/**
	 * 处理微信支付回调通知流水
	 * @param param
	 * @param order
	 * @param orderStatusItem
	 * @param wxPayOrderNotifyResult
	 */
	private void handleWxPayOrderNotifyResult(UpdateOrderStatusParam param, Order order, OrderConstants.OrderStatusItem orderStatusItem, WxPayOrderNotifyResult wxPayOrderNotifyResult){

		Pay pay = new Pay();
		pay.setOrderNo(param.getOrderNo());
		pay.setOrderId(order.getId());


		pay.setCurrencyDictId(order.getCurrencyDictId());
		// 微信旧版本api旧版本不支持，直接设置为null
		pay.setPayerCurrencyDictId(null);
		String statusDictId = dictService.getIdByGroupCodeAndValue(orderStatusItem.groupCode(), orderStatusItem.itemValue());
		pay.setPayStatusDictId(statusDictId);

		// 支付渠道
		OrderConstants.OrderPayChannelDictItem orderPayChannelDictItem = OrderConstants.OrderPayChannelDictItem.ofWxTradeType(wxPayOrderNotifyResult.getTradeType());
		String payChannelDictId = dictService.getIdByGroupCodeAndValue(orderPayChannelDictItem.groupCode(), orderPayChannelDictItem.itemValue());
		pay.setPayChannelDictId(payChannelDictId);


		// 取订单的用户id
		pay.setUserId(order.getUserId());
		pay.setAmount(order.getTotalAmount());



		if (iPaySerialNoResolver != null) {
			String seriaNo = iPaySerialNoResolver.resolve(param);
			pay.setSerialNo(seriaNo);
		}else {
			log.warn("iPaySerialNoResolver 未注入，已默认用idworker生成serialNo流水号");
			pay.setSerialNo(IdWorker.getIdStr());
		}

		pay.setSuccessAt(LocalDateTimeUtil.of(DateUtil.parse(wxPayOrderNotifyResult.getTimeEnd(), DatePattern.PURE_DATETIME_PATTERN)));
		pay.setRemark("微信支付回调生成支付流水");
		iPayService.save(pay);
	}

	/**
	 * 处理微信退款回调通知流水
	 * @param param
	 * @param order
	 * @param orderStatusItem
	 * @param wxPayRefundNotifyResult
	 */
	private void handleWxPayRefundNotifyResult(UpdateOrderStatusParam param, Order order, OrderConstants.OrderStatusItem orderStatusItem, WxPayRefundNotifyResult wxPayRefundNotifyResult){

		Pay pay = new Pay();
		pay.setOrderNo(param.getOrderNo());
		pay.setOrderId(order.getId());


		pay.setCurrencyDictId(order.getCurrencyDictId());
		// 微信旧版本api旧版本不支持，直接设置为null
		pay.setPayerCurrencyDictId(null);
		String statusDictId = dictService.getIdByGroupCodeAndValue(orderStatusItem.groupCode(), orderStatusItem.itemValue());
		pay.setPayStatusDictId(statusDictId);

		// 支付渠道,取订单的
		pay.setPayChannelDictId(order.getPayChannelDictId());


		// 取订单的用户id
		pay.setUserId(order.getUserId());
		pay.setAmount(wxPayRefundNotifyResult.getReqInfo().getRefundFee());



		if (iPaySerialNoResolver != null) {
			String seriaNo = iPaySerialNoResolver.resolve(param);
			pay.setSerialNo(seriaNo);
		}else {
			log.warn("iPaySerialNoResolver 未注入，已默认用idworker生成serialNo流水号");
			pay.setSerialNo(IdWorker.getIdStr());
		}

		pay.setSuccessAt(LocalDateTimeUtil.of(DateUtil.parse(wxPayRefundNotifyResult.getReqInfo().getSuccessTime(), DatePattern.NORM_DATETIME_PATTERN)));
		pay.setRemark("微信支付回调生成支付流水");

	}
}
