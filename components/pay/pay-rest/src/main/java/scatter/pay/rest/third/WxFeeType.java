package scatter.pay.rest.third;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import scatter.common.dict.PublicDictEnums;

/**
 * <p>
 * 微信货币类型
 * </p>
 *
 * @author yangwei
 * @since 2021-07-04 16:14
 */
@Slf4j
public enum WxFeeType {

	CNY;

	/**
	 * 根据字典值转微信货币类型
	 * @param dictItem
	 * @return
	 */
	public static WxFeeType getByCurrencyTypeDictItem(PublicDictEnums.CurrencyTypeDictItem dictItem){
		switch (dictItem){
			case CNY: {
				return WxFeeType.CNY;
			}
			default: {
				log.warn("暂不支持的字典货币类型，dictItem={}",dictItem);
			}
		}
		log.warn("暂不支持的字典货币类型，dictItem={}",dictItem);
		return null;
	}
	/**
	 * 根据字典值转微信货币类型
	 * @param currencyDictValue
	 * @return
	 */
	public static WxFeeType getByCurrencyDictValue(String currencyDictValue){
		if (StrUtil.isEmpty(currencyDictValue)) {
			return null;
		}

		return getByCurrencyTypeDictItem(PublicDictEnums.CurrencyTypeDictItem.valueOf(currencyDictValue));
	}
}
