package scatter.pay.rest.mapper;

import scatter.pay.pojo.po.Pay;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 支付表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
public interface PayMapper extends IBaseMapper<Pay> {

}
