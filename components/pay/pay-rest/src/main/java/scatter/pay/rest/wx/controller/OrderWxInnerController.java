package scatter.pay.rest.wx.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.pay.rest.PayConfiguration;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.rest.wx.service.IOrderWxService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 订单微信扩展表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@RestController
@RequestMapping(PayConfiguration.CONTROLLER_BASE_PATH + "/inner/wx/order-wx")
public class OrderWxInnerController extends BaseInnerController<OrderWx> {
 @Autowired
 private IOrderWxService orderWxService;

 public IOrderWxService getService(){
     return orderWxService;
 }
}
