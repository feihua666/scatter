package scatter.pay.rest.service;

/**
 * <p>
 * 用户openId获取器
 * </p>
 *
 * @author yangwei
 * @since 2021-07-03 14:29
 */
public interface IWxOpenIdResolver {


	public enum Type{
		mp, // 公众号
		minip // 小程序
	}

	/**
	 * 获取用户openid
	 * @param type 调用类型 如：mp公众号
	 * @param userId 登录用户的id
	 * @param appCode 多个公众号或小程序时可用，标识是哪个
	 * @return
	 */
	String resolve(Type type,String userId,String appCode);
}
