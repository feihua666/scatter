package scatter.pay.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.binarywang.wxpay.service.WxPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.order.rest.service.IOrderService;
import scatter.order.rest.service.impl.OrderFrameworkHelperService;
import scatter.pay.rest.PayConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.pay.pojo.po.Pay;
import scatter.pay.pojo.vo.PayVo;
import scatter.pay.pojo.form.PayAddForm;
import scatter.pay.pojo.form.PayUpdateForm;
import scatter.pay.pojo.form.PayPageQueryForm;
import scatter.pay.rest.service.IPayService;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.pay.rest.third.WxPaySignHelper;

import javax.validation.Valid;
/**
 * <p>
 * 支付表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Slf4j
@Api(tags = "支付相关接口")
@RestController
@RequestMapping(PayConfiguration.CONTROLLER_BASE_PATH + "/pay")
public class PayController extends BaseAddUpdateQueryFormController<Pay, PayVo, PayAddForm, PayUpdateForm, PayPageQueryForm> {
    @Autowired
    private IPayService iPayService;

    @Autowired
    private WxPaySignHelper wxPaySignHelper;

    @Autowired
    private WxPayService wxPayService;

    @Autowired
    private IOrderService iOrderService;
    @Autowired
    private OrderFrameworkHelperService orderFrameworkHelperService;


     @Override
	 @ApiOperation("添加支付")
     @PreAuthorize("hasAuthority('Pay:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PayVo add(@RequestBody @Valid PayAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询支付")
     @PreAuthorize("hasAuthority('Pay:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public PayVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除支付")
     @PreAuthorize("hasAuthority('Pay:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新支付")
     @PreAuthorize("hasAuthority('Pay:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public PayVo update(@RequestBody @Valid PayUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询支付")
    @PreAuthorize("hasAuthority('Pay:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<PayVo> getList(PayPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询支付")
    @PreAuthorize("hasAuthority('Pay:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<PayVo> getPage(PayPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


}
