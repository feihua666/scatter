package scatter.pay.rest.wx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.pay.rest.PayConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.pojo.wx.vo.OrderWxVo;
import scatter.pay.pojo.wx.form.OrderWxAddForm;
import scatter.pay.pojo.wx.form.OrderWxUpdateForm;
import scatter.pay.pojo.wx.form.OrderWxPageQueryForm;
import scatter.pay.rest.wx.service.IOrderWxService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 订单微信扩展表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Api(tags = "订单微信扩展相关接口")
@RestController
@RequestMapping(PayConfiguration.CONTROLLER_BASE_PATH + "/wx/order-wx")
public class OrderWxController extends BaseAddUpdateQueryFormController<OrderWx, OrderWxVo, OrderWxAddForm, OrderWxUpdateForm, OrderWxPageQueryForm> {
    @Autowired
    private IOrderWxService iOrderWxService;

    @ApiOperation("添加订单微信扩展")
    @PreAuthorize("hasAuthority('OrderWx:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public OrderWxVo add(@RequestBody @Valid OrderWxAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询订单微信扩展")
    @PreAuthorize("hasAuthority('OrderWx:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public OrderWxVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除订单微信扩展")
    @PreAuthorize("hasAuthority('OrderWx:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新订单微信扩展")
    @PreAuthorize("hasAuthority('OrderWx:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public OrderWxVo update(@RequestBody @Valid OrderWxUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询订单微信扩展")
    @PreAuthorize("hasAuthority('OrderWx:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<OrderWxVo> getList(OrderWxPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询订单微信扩展")
    @PreAuthorize("hasAuthority('OrderWx:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<OrderWxVo> getPage(OrderWxPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
