package scatter.pay.rest.wx.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.common.rest.service.IBaseService;
import scatter.pay.pojo.wx.form.OrderWxAddForm;
import scatter.pay.pojo.wx.form.OrderWxUpdateForm;
import scatter.pay.pojo.wx.form.OrderWxPageQueryForm;
import java.util.List;
/**
 * <p>
 * 订单微信扩展表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
public interface IOrderWxService extends IBaseService<OrderWx> {


    /**
     * 根据订单号查询
     * @param outTradeNo
     * @return
     */
    default OrderWx getByOutTradeNo(String outTradeNo) {
        Assert.hasText(outTradeNo,"outTradeNo不能为空");
        return getOne(Wrappers.<OrderWx>lambdaQuery().eq(OrderWx::getOutTradeNo, outTradeNo));
    }

    /**
     * 查询微信相关订单
     * @param orderIds
     * @return
     */
    default List<OrderWx> getByOrderIds(List<String> orderIds){
        if (isEmpty(orderIds)) {
            return null;
        }
        return list(Wrappers.<OrderWx>lambdaQuery().in(OrderWx::getOrderId, orderIds));

    }

}
