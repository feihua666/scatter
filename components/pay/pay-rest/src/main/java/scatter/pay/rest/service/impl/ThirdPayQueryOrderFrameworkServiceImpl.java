package scatter.pay.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.order.rest.service.impl.DefaultQueryOrderFrameworkServiceImpl;
import scatter.pay.pojo.po.Pay;
import scatter.pay.pojo.vo.PayVo;
import scatter.pay.pojo.wx.vo.OrderWxVo;
import scatter.pay.rest.mapstruct.PayMapStruct;
import scatter.pay.rest.service.IPayService;
import scatter.pay.rest.wx.mapstruct.OrderWxMapStruct;
import scatter.pay.rest.wx.service.IOrderWxService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * <p>
 * 三方支付查询订单
 * </p>
 *
 * @author yangwei
 * @since 2021-08-08 13:54
 */
public class ThirdPayQueryOrderFrameworkServiceImpl extends DefaultQueryOrderFrameworkServiceImpl {

	@Autowired
	private IOrderWxService iOrderWxService;
	@Autowired
	private IPayService iPayService;

	@Override
	protected List<Future<List<ExtFutureDto>>> extFutures(List<String> orderIds) {
		Future<List<ExtFutureDto>> orderWxFuture = executorService.submit(()-> {
			List<OrderWxVo> orderWxVos = OrderWxMapStruct.INSTANCE.posToVos(iOrderWxService.getByOrderIds(orderIds));

			return orderWxVos.stream().map(item-> {
				Map<String,Object> map = new HashMap<>();
				map.put("orderWx",item);
				return new ExtFutureDto(item.getOrderId(),map);
			}).collect(Collectors.toList());
		});
		Future<List<ExtFutureDto>> orderPayFuture = executorService.submit(()-> {
			List<PayVo> orderPayVos = PayMapStruct.INSTANCE.posToVos(iPayService.getByOrderIds(orderIds));

			return orderPayVos.stream().map(item-> {
				Map<String,Object> map = new HashMap<>();
				map.put("orderPay",item);
				return new ExtFutureDto(item.getOrderId(),map);
			}).collect(Collectors.toList());
		});
		return newArrayList(orderWxFuture,orderPayFuture);
	}
}
