package scatter.pay.rest.wx.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.rest.wx.service.IOrderWxService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单微信扩展翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Component
public class OrderWxTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IOrderWxService orderWxService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,OrderWx.TRANS_ORDERWX_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,OrderWx.TRANS_ORDERWX_BY_ID)) {
            OrderWx byId = orderWxService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,OrderWx.TRANS_ORDERWX_BY_ID)) {
            return orderWxService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
