DROP TABLE IF EXISTS component_order_wx;
CREATE TABLE `component_order_wx` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `order_id` varchar(20) NOT NULL COMMENT '订单id',
  `out_trade_no` varchar(50) NOT NULL COMMENT '订单号,吐给微信的订单号，此订单号跟订单表中的订单号可以不同',
  `sp_app_code` varchar(50) DEFAULT NULL COMMENT '自定义app_code,针对appid的编码',
  `sp_appid` varchar(32) NOT NULL COMMENT '服务商应用ID',
  `sp_mchid` varchar(32) NOT NULL COMMENT '服务商户号',
  `sub_app_code` varchar(50) DEFAULT NULL COMMENT '自定义app_code,针对appid的编码',
  `sub_appid` varchar(32) DEFAULT NULL COMMENT '子商户应用ID',
  `sub_mchid` varchar(32) DEFAULT NULL COMMENT '子商户号',
  `prepay_id` varchar(64) DEFAULT NULL COMMENT '预支付id，有效时间两个小时，如果超过两个小时不能进行支付，只能撤销订单，重新下单',
  `prepay_id_expire_at` datetime DEFAULT NULL COMMENT '预支付id失效时间',
  `is_prepay_id_expire` tinyint(1) NOT NULL COMMENT '预支付id是否过期失效',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `order_no` (`out_trade_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='订单微信扩展表';
