package scatter.pay.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.pay.pojo.po.Pay;
import scatter.pay.pojo.form.PayAddForm;
import scatter.pay.pojo.form.PayUpdateForm;
import scatter.pay.pojo.form.PayPageQueryForm;
import scatter.pay.rest.service.IPayService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 支付 测试类基类
* </p>
*
* @author yw
* @since 2021-06-22
*/
public class PaySuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPayService payService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return payService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return payService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Pay mockPo() {
        return JMockData.mock(Pay.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PayAddForm mockAddForm() {
        return JMockData.mock(PayAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PayUpdateForm mockUpdateForm() {
        return JMockData.mock(PayUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PayPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PayPageQueryForm.class, mockConfig);
    }
}