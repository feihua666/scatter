package scatter.pay.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.pay.pojo.po.Pay;
import scatter.pay.pojo.form.PayAddForm;
import scatter.pay.pojo.form.PayUpdateForm;
import scatter.pay.pojo.form.PayPageQueryForm;
import scatter.pay.rest.test.PaySuperTest;
import scatter.pay.rest.service.IPayService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 支付 服务测试类
* </p>
*
* @author yw
* @since 2021-06-22
*/
@SpringBootTest
public class PayServiceTest extends PaySuperTest{

    @Autowired
    private IPayService payService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Pay> pos = payService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}