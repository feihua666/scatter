package scatter.pay.rest.test.wx.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.pojo.wx.form.OrderWxAddForm;
import scatter.pay.pojo.wx.form.OrderWxUpdateForm;
import scatter.pay.pojo.wx.form.OrderWxPageQueryForm;
import scatter.pay.rest.test.wx.OrderWxSuperTest;
import scatter.pay.rest.wx.service.IOrderWxService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 订单微信扩展 服务测试类
* </p>
*
* @author yw
* @since 2021-08-07
*/
@SpringBootTest
public class OrderWxServiceTest extends OrderWxSuperTest{

    @Autowired
    private IOrderWxService orderWxService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<OrderWx> pos = orderWxService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}