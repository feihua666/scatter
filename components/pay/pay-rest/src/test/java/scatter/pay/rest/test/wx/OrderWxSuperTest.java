package scatter.pay.rest.test.wx;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.pay.pojo.wx.po.OrderWx;
import scatter.pay.pojo.wx.form.OrderWxAddForm;
import scatter.pay.pojo.wx.form.OrderWxUpdateForm;
import scatter.pay.pojo.wx.form.OrderWxPageQueryForm;
import scatter.pay.rest.wx.service.IOrderWxService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 订单微信扩展 测试类基类
* </p>
*
* @author yw
* @since 2021-08-07
*/
public class OrderWxSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IOrderWxService orderWxService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return orderWxService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return orderWxService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public OrderWx mockPo() {
        return JMockData.mock(OrderWx.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public OrderWxAddForm mockAddForm() {
        return JMockData.mock(OrderWxAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public OrderWxUpdateForm mockUpdateForm() {
        return JMockData.mock(OrderWxUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public OrderWxPageQueryForm mockPageQueryForm() {
        return JMockData.mock(OrderWxPageQueryForm.class, mockConfig);
    }
}