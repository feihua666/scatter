package scatter.pay.pojo.wx.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 订单微信扩展分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Setter
@Getter
@ApiModel(value="订单微信扩展分页表单对象")
public class OrderWxPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号,吐给微信的订单号，此订单号跟订单表中的订单号可以不同")
    private String outTradeNo;

    @ApiModelProperty(value = "自定义app_code,针对appid的编码")
    private String spAppCode;

    @ApiModelProperty(value = "服务商应用ID")
    private String spAppid;

    @ApiModelProperty(value = "服务商户号")
    private String spMchid;

    @ApiModelProperty(value = "自定义app_code,针对appid的编码")
    private String subAppCode;

    @ApiModelProperty(value = "子商户应用ID")
    private String subAppid;

    @ApiModelProperty(value = "子商户号")
    private String subMchid;

    @ApiModelProperty(value = "预支付id，有效时间两个小时，如果超过两个小时不能进行支付，只能撤销订单，重新下单")
    private String prepayId;

    @ApiModelProperty(value = "预支付id失效时间")
    private LocalDateTime prepayIdExpireAt;

    @ApiModelProperty(value = "预支付id是否过期失效")
    private Boolean isPrepayIdExpire;

}
