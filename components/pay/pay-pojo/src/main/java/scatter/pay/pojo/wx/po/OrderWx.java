package scatter.pay.pojo.wx.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单微信扩展表
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_order_wx")
@ApiModel(value="OrderWx对象", description="订单微信扩展表")
public class OrderWx extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_ORDERWX_BY_ID = "trans_orderwx_by_id_scatter.pay.pojo.wx.po";

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号,吐给微信的订单号，此订单号跟订单表中的订单号可以不同")
    private String outTradeNo;

    @ApiModelProperty(value = "自定义app_code,针对appid的编码")
    private String spAppCode;

    @ApiModelProperty(value = "服务商应用ID")
    private String spAppid;

    @ApiModelProperty(value = "服务商户号")
    private String spMchid;

    @ApiModelProperty(value = "自定义app_code,针对appid的编码")
    private String subAppCode;

    @ApiModelProperty(value = "子商户应用ID")
    private String subAppid;

    @ApiModelProperty(value = "子商户号")
    private String subMchid;

    @ApiModelProperty(value = "预支付id，有效时间两个小时，如果超过两个小时不能进行支付，只能撤销订单，重新下单")
    private String prepayId;

    @ApiModelProperty(value = "预支付id失效时间")
    private LocalDateTime prepayIdExpireAt;

    @ApiModelProperty(value = "预支付id是否过期失效")
    private Boolean isPrepayIdExpire;


}
