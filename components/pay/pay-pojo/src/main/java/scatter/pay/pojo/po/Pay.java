package scatter.pay.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付表
 * 用户记录用户支付情况
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_pay")
@ApiModel(value="Pay对象", description="支付表")
public class Pay extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_PAY_BY_ID = "trans_pay_by_id_scatter.pay.pojo.po";

    @ApiModelProperty(value = "流水号")
    private String serialNo;

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @ApiModelProperty(value = "用户支付货币类型,字典id")
    private String payerCurrencyDictId;

    @ApiModelProperty(value = "支付状态，字典id,和订单状态部分一致")
    private String payStatusDictId;

    @ApiModelProperty(value = "支付渠道，字典id")
    private String payChannelDictId;

    @ApiModelProperty(value = "支付的用户id")
    private String userId;

    @ApiModelProperty(value = "支付金额，单位为分")
    private Integer amount;

    @ApiModelProperty(value = "付款银行id")
    private String bankId;

    @ApiModelProperty(value = "付款银行卡id")
    private String bankCardId;

    @ApiModelProperty(value = "支付完成时间")
    private LocalDateTime successAt;

    @ApiModelProperty(value = "备注")
    private String remark;


}
