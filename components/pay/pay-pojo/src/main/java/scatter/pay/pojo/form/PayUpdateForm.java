package scatter.pay.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 支付更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Setter
@Getter
@ApiModel(value="支付更新表单对象")
public class PayUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="流水号不能为空")
    @ApiModelProperty(value = "流水号",required = true)
    private String serialNo;

    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号",required = true)
    private String orderNo;

    @NotEmpty(message="货币类型不能为空")
    @ApiModelProperty(value = "货币类型,字典id",required = true)
    private String currencyDictId;


    @ApiModelProperty(value = "支付状态，字典id,和订单状态部分一致")
    private String payStatusDictId;

    @ApiModelProperty(value = "支付渠道，字典id")
    private String payChannelDictId;

    @NotEmpty(message="支付的用户id不能为空")
    @ApiModelProperty(value = "支付的用户id",required = true)
    private String userId;

    @NotNull(message="支付金额不能为空")
    @ApiModelProperty(value = "支付金额，单位为分",required = true)
    private Integer amount;

    @ApiModelProperty(value = "付款银行id")
    private String bankId;

    @ApiModelProperty(value = "付款银行卡id")
    private String bankCardId;

    @NotNull(message="支付完成时间不能为空")
    @ApiModelProperty(value = "支付完成时间",required = true)
    private LocalDateTime successAt;

    @ApiModelProperty(value = "备注")
    private String remark;

}
