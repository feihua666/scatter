package scatter.pay.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-07-03 18:29
 */
@Setter
@Getter
@PropValid
@ApiModel("微信支付签名表单")
public class WxPaySignForm {

	@NotEmpty(message = "appCode不能为空")
	@ApiModelProperty(value = "商户id或appCode",required = true)
	private String appCode;

	@ApiModelProperty(value = "订单号",notes = "订单号和预支付id二选一")
	private String orderNo;

	@ApiModelProperty(value = "预支付id",notes = "订单号和预支付id二选一")
	private String prepayId;

	@NotEmpty(message = "支付渠道不能为空")
	@ApiModelProperty(value = "支付渠道",required = true)
	private String payChannelDictValue;

	/**
	 * {@link WxPayConstants.SignType}
	 */
	private String signType;
}
