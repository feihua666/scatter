package scatter.pay.pojo.wx.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单微信扩展更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-07
 */
@Setter
@Getter
@ApiModel(value="订单微信扩展更新表单对象")
public class OrderWxUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="订单id不能为空")
    @ApiModelProperty(value = "订单id",required = true)
    private String orderId;

    @NotEmpty(message="订单号不能为空")
    @ApiModelProperty(value = "订单号,吐给微信的订单号，此订单号跟订单表中的订单号可以不同",required = true)
    private String outTradeNo;

    @ApiModelProperty(value = "自定义app_code,针对appid的编码")
    private String spAppCode;

    @NotEmpty(message="服务商应用ID不能为空")
    @ApiModelProperty(value = "服务商应用ID",required = true)
    private String spAppid;

    @NotEmpty(message="服务商户号不能为空")
    @ApiModelProperty(value = "服务商户号",required = true)
    private String spMchid;

    @ApiModelProperty(value = "自定义app_code,针对appid的编码")
    private String subAppCode;

    @ApiModelProperty(value = "子商户应用ID")
    private String subAppid;

    @ApiModelProperty(value = "子商户号")
    private String subMchid;

    @ApiModelProperty(value = "预支付id，有效时间两个小时，如果超过两个小时不能进行支付，只能撤销订单，重新下单")
    private String prepayId;

    @ApiModelProperty(value = "预支付id失效时间")
    private LocalDateTime prepayIdExpireAt;

    @NotNull(message="预支付id是否过期失效不能为空")
    @ApiModelProperty(value = "预支付id是否过期失效",required = true)
    private Boolean isPrepayIdExpire;

}
