package scatter.pay.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 支付响应对象
 * </p>
 *
 * @author yw
 * @since 2021-06-22
 */
@Setter
@Getter
@ApiModel(value="支付响应对象")
public class PayVo extends BaseIdVo {

    /**
     * 翻译用户的昵称，解耦自动实现，因为项目中存在多个用户体系
     * 翻译结果请设置为字符串
     */
    public static final String TRANS_USER_NICKNAME_BY_USER_ID = "pay_trans_user_nickname_by_user_id";


    @ApiModelProperty(value = "流水号")
    private String serialNo;

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "货币类型,字典id")
    private String currencyDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "currencyDictId",mapValueField = "name")
    @ApiModelProperty(value = "货币类型,字典名称")
    private String currencyDictName;

    @ApiModelProperty(value = "用户支付货币类型,字典id")
    private String payerCurrencyDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "payerCurrencyDictId",mapValueField = "name")
    @ApiModelProperty(value = "用户支付货币类型,字典名称")
    private String payerCurrencyDictName;

    @ApiModelProperty(value = "支付状态，字典id,和订单状态部分一致")
    private String payStatusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "payStatusDictId",mapValueField = "name")
    @ApiModelProperty(value = "支付状态，字典名称")
    private String payStatusDictName;

    @ApiModelProperty(value = "支付渠道，字典id")
    private String payChannelDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "payChannelDictId",mapValueField = "name")
    @ApiModelProperty(value = "支付渠道，字典名称")
    private String payChannelDictName;

    @ApiModelProperty(value = "支付的用户id")
    private String userId;

    @TransBy(type = TRANS_USER_NICKNAME_BY_USER_ID,byFieldName = "userId")
    @ApiModelProperty(value = "支付的用户昵称")
    private String userNickname;

    @ApiModelProperty(value = "支付金额，单位为分")
    private Integer amount;

    @ApiModelProperty(value = "付款银行id")
    private String bankId;

    @ApiModelProperty(value = "付款银行卡id")
    private String bankCardId;

    @ApiModelProperty(value = "支付完成时间")
    private LocalDateTime successAt;

    @ApiModelProperty(value = "备注")
    private String remark;

}
