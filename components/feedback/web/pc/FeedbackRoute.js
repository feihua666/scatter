import FeedbackUrl from './FeedbackUrl.js'

const FeedbackRoute = [
    {
        path: FeedbackUrl.router.searchList,
        component: () => import('./element/FeedbackSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'FeedbackSearchList',
            name: '意见反馈管理'
        }
    },
    {
        path: FeedbackUrl.router.add,
        component: () => import('./element/FeedbackAdd'),
        meta: {
            code:'FeedbackAdd',
            name: '意见反馈添加'
        }
    },
    {
        path: FeedbackUrl.router.reply,
        component: () => import('./element/FeedbackReply'),
        meta: {
            code:'FeedbackReply',
            name: '意见反馈回复'
        }
    },
]
export default FeedbackRoute