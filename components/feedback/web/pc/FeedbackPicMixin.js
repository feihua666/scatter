import FeedbackPicForm from './FeedbackPicForm.js'
import FeedbackPicTable from './FeedbackPicTable.js'
import FeedbackPicUrl from './FeedbackPicUrl.js'
const FeedbackPicMixin = {
    computed: {
        FeedbackPicFormOptions() {
            return this.$stDynamicFormTools.options(FeedbackPicForm,this.$options.name)
        },
        FeedbackPicTableOptions() {
            return FeedbackPicTable
        },
        FeedbackPicUrl(){
            return FeedbackPicUrl
        }
    },
}
export default FeedbackPicMixin