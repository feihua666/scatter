const basePath = '' + '/feedback'
const FeedbackUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    reply: basePath + '/reply',
    router: {
        searchList: '/FeedbackSearchList',
        add: '/FeedbackAdd',
        reply: '/FeedbackReply',
    }
}
export default FeedbackUrl