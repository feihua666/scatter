const FeedbackTable = [
    {
        prop: 'contactUsername',
        label: '联系人姓名'
    },
    {
        prop: 'contactMobile',
        label: '联系人手机号'
    },
    {
        prop: 'contactEmail',
        label: '联系人邮箱'
    },
    {
        prop: 'suggestionContent',
        label: '问题建议内容'
    },
    {
        prop: 'suggestionPicUrls',
        label: '问题建议配图',
        stype: 'image'
    },
    {
        prop: 'suggestionAt',
        label: '问题建议时间'
    },
    {
        prop: 'suggestionUserId',
        label: '用户id'
    },
    {
        prop: 'replyContent',
        label: '回复内容'
    },
    {
        prop: 'replyPicUrls',
        label: '回复建议配图',
        stype: 'image'
    },
    {
        prop: 'replyAt',
        label: '回复时间'
    },
    {
        prop: 'replyUserId',
        label: '回复用户id'
    },
    {
        prop: 'firstFeedbackId',
        label: '第一条意见反馈id'
    },
]
export default FeedbackTable