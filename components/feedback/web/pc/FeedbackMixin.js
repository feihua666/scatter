import FeedbackForm from './FeedbackForm.js'
import FeedbackTable from './FeedbackTable.js'
import FeedbackUrl from './FeedbackUrl.js'
const FeedbackMixin = {
    computed: {
        FeedbackFormOptions() {
            return this.$stDynamicFormTools.options(FeedbackForm,this.$options.name)
        },
        FeedbackTableOptions() {
            return FeedbackTable
        },
        FeedbackUrl(){
            return FeedbackUrl
        }
    },
}
export default FeedbackMixin