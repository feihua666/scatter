import FeedbackUrl from './FeedbackUrl.js'
const FeedbackForm = [
    {
        FeedbackReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'contactUsername',
        },
        element:{
            label: '联系人姓名',
        }
    },
    {
        FeedbackReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'contactMobile',
        },
        element:{
            label: '联系人手机号',
            rules: [
                { pattern:/^[1][0-9]{10}$/, message: '请输入正确的手机号', trigger: ['blur'] }
            ]
        }
    },
    {
        FeedbackReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'contactEmail',
        },
        element:{
            label: '联系人邮箱',
            rules: [
                { type: 'email', message: '请输入正确的邮箱地址', trigger: ['blur'] }
            ]
        }
    },
    {
        FeedbackSearchList: false,
        FeedbackReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'suggestionContent',
        },
        element:{
            label: '问题建议内容',
            type: 'textarea',
            isBlock: true,
            required: true,
        }
    },
    {
        // 该字段添加建议时用
        FeedbackSearchList:false,
        FeedbackReply: false,
        field: {
            name: 'picUrls',
        },
        element:{
            label: '建议配图',
            type: 'uploadFile',
            options: {
                triggerType: 'card',
                accept: 'image/*',
                listType: 'picture-card'
            },
        }
    },
    {
        // 该字段回复用，仅做展示用
        FeedbackSearchList:false,
        FeedbackAdd: false,
        field: {
            name: 'suggestionPicUrls',
        },
        element:{
            label: '建议配图',
            type: 'image',
        }
    },
    {
        FeedbackSearchList: false,
        FeedbackAdd:false,
        field: {
            name: 'replyContent',
        },
        element:{
            label: '回复内容',
            type: 'textarea',
            required: true,
            isBlock: true,
        }
    },
    {
        // 该字段回复用配图
        FeedbackSearchList:false,
        FeedbackAdd: false,
        field: {
            name: 'picUrls',
        },
        element:{
            label: '回复配图',
            type: 'uploadFile',
            options: {
                triggerType: 'card',
                accept: 'image/*',
                listType: 'picture-card'
            },
        }
    },
    {
        FeedbackAdd:false,
        FeedbackReply: false,
        field: {
            name: 'firstFeedbackId',
        },
        element:{
            label: '第一条意见反馈id',
        }
    },

]
export default FeedbackForm