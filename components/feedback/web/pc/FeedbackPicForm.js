import FeedbackPicUrl from './FeedbackPicUrl.js'
const FeedbackPicForm = [
    {
        FeedbackPicSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'feedbackId',
        },
        element:{
            label: '意见反馈id',
            required: true,
        }
    },
    {
        FeedbackPicSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'picUrl',
        },
        element:{
            label: '图片地址',
            required: true,
        }
    },
    {
        FeedbackPicSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'isSuggestion',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否意见图片',
            required: true,
        }
    },

]
export default FeedbackPicForm