const FeedbackPicTable = [
    {
        prop: 'feedbackId',
        label: '意见反馈id'
    },
    {
        prop: 'picUrl',
        label: '图片地址'
    },
    {
        prop: 'isSuggestion',
        label: '是否意见图片'
    },
]
export default FeedbackPicTable