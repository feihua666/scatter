const basePath = '' + '/feedback-pic'
const FeedbackPicUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/FeedbackPicSearchList',
        add: '/FeedbackPicAdd',
        update: '/FeedbackPicUpdate',
    }
}
export default FeedbackPicUrl