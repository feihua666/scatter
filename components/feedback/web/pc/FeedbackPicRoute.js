import FeedbackPicUrl from './FeedbackPicUrl.js'

const FeedbackPicRoute = [
    {
        path: FeedbackPicUrl.router.searchList,
        component: () => import('./element/FeedbackPicSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'FeedbackPicSearchList',
            name: '意见反馈图片管理'
        }
    },
    {
        path: FeedbackPicUrl.router.add,
        component: () => import('./element/FeedbackPicAdd'),
        meta: {
            code:'FeedbackPicAdd',
            name: '意见反馈图片添加'
        }
    },
    {
        path: FeedbackPicUrl.router.update,
        component: () => import('./element/FeedbackPicUpdate'),
        meta: {
            code:'FeedbackPicUpdate',
            name: '意见反馈图片修改'
        }
    },
]
export default FeedbackPicRoute