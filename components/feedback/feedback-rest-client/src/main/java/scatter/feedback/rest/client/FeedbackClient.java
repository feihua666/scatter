package scatter.feedback.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 意见反馈表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
@FeignClient(value = "Feedback-client")
public interface FeedbackClient {

}
