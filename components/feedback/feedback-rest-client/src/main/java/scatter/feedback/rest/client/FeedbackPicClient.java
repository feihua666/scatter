package scatter.feedback.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 意见反馈图片表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
@FeignClient(value = "FeedbackPic-client")
public interface FeedbackPicClient {

}
