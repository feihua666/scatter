DROP TABLE IF EXISTS component_feedback;
CREATE TABLE `component_feedback` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `contact_username` varchar(255) DEFAULT NULL COMMENT '联系人姓名，直接填写的',
  `contact_mobile` varchar(11) DEFAULT NULL COMMENT '联系人手机号',
  `contact_email` varchar(255) DEFAULT NULL COMMENT '联系人邮箱',
  `suggestion_content` varchar(2000) NOT NULL COMMENT '问题建议内容',
  `suggestion_at` datetime NOT NULL COMMENT '问题建议时间',
  `suggestion_user_id` varchar(20) DEFAULT NULL COMMENT '用户id，如果存在登录用户，记录提建议的用户id',
  `reply_content` varchar(2000) DEFAULT NULL COMMENT '回复内容',
  `reply_at` datetime DEFAULT NULL COMMENT '回复时间',
  `reply_user_id` varchar(20) DEFAULT NULL COMMENT '回复用户id',
  `first_feedback_id` varchar(20) DEFAULT NULL COMMENT '第一条意见反馈id，主要串联起一个话题',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `first_feedback_id` (`first_feedback_id`),
  KEY `user_id` (`suggestion_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='意见反馈表';
