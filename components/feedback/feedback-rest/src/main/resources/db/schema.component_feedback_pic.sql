DROP TABLE IF EXISTS component_feedback_pic;
CREATE TABLE `component_feedback_pic` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `feedback_id` varchar(20) NOT NULL COMMENT '意见反馈id',
  `pic_url` varchar(300) NOT NULL COMMENT '图片地址',
  `is_suggestion` tinyint(1) NOT NULL COMMENT '是否意见图片，1=反馈图片，0=回复图片',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `feedback_id` (`feedback_id`,`is_suggestion`),
  KEY `feedback_id_2` (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='意见反馈图片表';
