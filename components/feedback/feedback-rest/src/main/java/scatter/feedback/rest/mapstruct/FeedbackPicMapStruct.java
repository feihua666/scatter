package scatter.feedback.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.feedback.pojo.po.FeedbackPic;
import scatter.feedback.pojo.vo.FeedbackPicVo;
import scatter.feedback.pojo.form.FeedbackPicAddForm;
import scatter.feedback.pojo.form.FeedbackPicUpdateForm;
import scatter.feedback.pojo.form.FeedbackPicPageQueryForm;

/**
 * <p>
 * 意见反馈图片 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FeedbackPicMapStruct extends IBaseVoMapStruct<FeedbackPic, FeedbackPicVo>,
                                  IBaseAddFormMapStruct<FeedbackPic,FeedbackPicAddForm>,
                                  IBaseUpdateFormMapStruct<FeedbackPic,FeedbackPicUpdateForm>,
                                  IBaseQueryFormMapStruct<FeedbackPic,FeedbackPicPageQueryForm>{
    FeedbackPicMapStruct INSTANCE = Mappers.getMapper( FeedbackPicMapStruct.class );

}
