package scatter.feedback.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.feedback.rest.FeedbackConfiguration;
import scatter.feedback.pojo.po.FeedbackPic;
import scatter.feedback.rest.service.IFeedbackPicService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 意见反馈图片表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@RestController
@RequestMapping(FeedbackConfiguration.CONTROLLER_BASE_PATH + "/inner/feedback-pic")
public class FeedbackPicInnerController extends BaseInnerController<FeedbackPic> {
 @Autowired
 private IFeedbackPicService feedbackPicService;

 public IFeedbackPicService getService(){
     return feedbackPicService;
 }
}
