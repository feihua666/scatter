package scatter.feedback.rest.service.impl;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import scatter.common.rest.notify.NotifyParam;
import scatter.common.rest.notify.NotifyTool;
import scatter.common.rest.service.IBaseAddQueryFormServiceImpl;
import scatter.feedback.pojo.form.FeedbackReplyForm;
import scatter.feedback.pojo.po.Feedback;
import scatter.feedback.rest.mapper.FeedbackMapper;
import scatter.feedback.rest.mapstruct.FeedbackMapStruct;
import scatter.feedback.rest.service.IFeedbackPicService;
import scatter.feedback.rest.service.IFeedbackService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.feedback.pojo.form.FeedbackAddForm;
import scatter.feedback.pojo.form.FeedbackUpdateForm;
import scatter.feedback.pojo.form.FeedbackPageQueryForm;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p>
 * 意见反馈表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class FeedbackServiceImpl extends IBaseAddQueryFormServiceImpl<FeedbackMapper, Feedback, FeedbackAddForm, FeedbackPageQueryForm> implements IFeedbackService {

    @Lazy
    @Autowired
    private IFeedbackPicService iFeedbackPicService;



    @Override
    public void preAdd(FeedbackAddForm addForm,Feedback po) {
        po.setSuggestionAt(LocalDateTime.now());
        super.preAdd(addForm,po);

    }

    @Override
    public void postAdd(FeedbackAddForm feedbackAddForm, Feedback po) {
        // 设置首次意见反馈id，以中联起一个话题
        if (isStrEmpty(po.getFirstFeedbackId())) {
            po.setFirstFeedbackId(po.getId());
            updateById(po);
        }
        /**
         * 如果图片不为空，添加图片
         */
        if(!isEmpty(feedbackAddForm.getPicUrls())){
            iFeedbackPicService.addPicUrls(feedbackAddForm.getPicUrls(), po.getId(),true);
        }

        // 添加完意见反馈，发一下消息提醒回复
        NotifyParam notifyParam = NotifyParam.business().setContentType("feedback.add")
                .setTitle("意见反馈提醒")
                .setContent(StrUtil.format("有新的意见反馈，请尽快回复，内容={}", po.getSuggestionContent()));
        NotifyTool.notify(notifyParam);
    }

    @Override
    protected void postDeleteById(String id, Feedback po) {
        iFeedbackPicService.deletePicUrls(id,null);
    }

    @Override
    public boolean reply(FeedbackReplyForm replyForm) {
        Feedback feedback = FeedbackMapStruct.INSTANCE.replyFormToPo(replyForm);
        feedback.setReplyAt(LocalDateTime.now());
        boolean b = updateById(feedback);
        if (b) {
            if (!isEmpty(replyForm.getPicUrls())) {
                iFeedbackPicService.addPicUrls(replyForm.getPicUrls(),replyForm.getId(),false);
            }
            // 添加完意见反馈，发一下消息提醒回复
            NotifyParam notifyParam = NotifyParam.business().setContentType("feedback.reply")
                    .setTitle("意见反馈回复提醒")
                    .setContent(StrUtil.format("意见反馈已回复，请知悉，内容={}", replyForm.getReplyContent()));
            NotifyTool.notify(notifyParam);
        }
        return b;
    }
}
