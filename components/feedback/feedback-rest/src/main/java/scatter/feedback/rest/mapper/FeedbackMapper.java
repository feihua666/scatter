package scatter.feedback.rest.mapper;

import scatter.feedback.pojo.po.Feedback;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 意见反馈表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface FeedbackMapper extends IBaseMapper<Feedback> {

}
