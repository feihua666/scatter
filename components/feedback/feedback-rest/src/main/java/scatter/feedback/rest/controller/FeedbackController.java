package scatter.feedback.rest.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.controller.BaseAddQueryFormController;
import scatter.common.rest.exception.BusinessBadRequestException;
import scatter.common.rest.exception.BusinessException;
import scatter.feedback.pojo.form.*;
import scatter.feedback.rest.FeedbackConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.feedback.pojo.po.Feedback;
import scatter.feedback.pojo.vo.FeedbackVo;
import scatter.feedback.rest.service.IFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 意见反馈表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "意见反馈相关接口")
@RestController
@RequestMapping(FeedbackConfiguration.CONTROLLER_BASE_PATH + "/feedback")
public class FeedbackController extends BaseAddQueryFormController<Feedback, FeedbackVo, FeedbackAddForm, FeedbackPageQueryForm> {
    @Autowired
    private IFeedbackService iFeedbackService;


    /**
     * 一般添加意见反馈，不是在后台添加，而是用户添加的，这里不需要权限
     * @param addForm
     * @return
     */
    @ApiOperation("添加意见反馈")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public FeedbackVo add(@RequestBody @Valid FeedbackAddForm addForm, @ApiIgnore LoginUser loginUser) {
        addForm.setSuggestionUserId(Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));

        // 进行验证
        if (isStrEmpty(addForm.getSuggestionUserId())) {
            if (StrUtil.isAllEmpty(addForm.getContactEmail(),addForm.getContactMobile())) {
                throw new BusinessBadRequestException("请填写您的一个联系方式，以方便联系您");
            }
            if (isStrEmpty(addForm.getContactUsername())) {
                throw new BusinessBadRequestException("请填写您的姓名");
            }
        }
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询意见反馈")
    @PreAuthorize("hasAuthority('Feedback:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public FeedbackVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除意见反馈")
    @PreAuthorize("hasAuthority('Feedback:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("不分页查询意见反馈")
    @PreAuthorize("hasAuthority('Feedback:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<FeedbackVo> getList(FeedbackPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询意见反馈")
    @PreAuthorize("hasAuthority('Feedback:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<FeedbackVo> getPage(FeedbackPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    /**
     * 后台回复
     * @param replyForm
     * @return
     */
    @ApiOperation("回复意见反馈")
    @PreAuthorize("hasAuthority('Feedback:reply')")
    @PostMapping("/reply")
    @ResponseStatus(HttpStatus.CREATED)
    public FeedbackVo reply(@RequestBody @Valid FeedbackReplyForm replyForm, @ApiIgnore LoginUser loginUser) {
        replyForm.setReplyUserId(Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
        boolean reply = iFeedbackService.reply(replyForm);
        if (!reply) {
            throw new BusinessException("回复失败，请刷新后尝试");
        }
        return queryById(replyForm.getId());
    }

    @ApiOperation("获取我的意见反馈")
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getMyFeedbackPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<FeedbackVo> myFeedback(FeedbackForMePageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) {
        return super.pagePoToVo(iFeedbackService.getPageBySuggestUserId(listPageForm,loginUser.getId()));
    }

}
