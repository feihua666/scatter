package scatter.feedback.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.feedback.pojo.form.*;
import scatter.feedback.pojo.po.Feedback;
import scatter.common.rest.service.IBaseService;
import scatter.feedback.rest.mapstruct.FeedbackMapStruct;

import java.util.List;
/**
 * <p>
 * 意见反馈表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface IFeedbackService extends IBaseService<Feedback> {


	/**
	 * 回复
	 * @param replyForm
	 * @return
	 */
	public boolean reply(FeedbackReplyForm replyForm);

	/**
	 * 查询用户发起的意见反馈
	 * @param queryForm
	 * @param suggestionUserId
	 * @return
	 */
	default public IPage<Feedback> getPageBySuggestUserId(FeedbackForMePageQueryForm queryForm, String suggestionUserId){
		Assert.hasText(suggestionUserId,"suggestionUserId 不能为空");
		Page page = convertPage(queryForm);
		Feedback feedback = FeedbackMapStruct.INSTANCE.feedbackForMePageQueryFormToPo(queryForm);
		feedback.setSuggestionUserId(suggestionUserId);
		return page(page,Wrappers.<Feedback>query(feedback).lambda().orderByDesc(Feedback::getSuggestionAt));
	}
}
