package scatter.feedback.rest.service.impl;

import scatter.feedback.pojo.po.FeedbackPic;
import scatter.feedback.rest.mapper.FeedbackPicMapper;
import scatter.feedback.rest.service.IFeedbackPicService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.feedback.pojo.form.FeedbackPicAddForm;
import scatter.feedback.pojo.form.FeedbackPicUpdateForm;
import scatter.feedback.pojo.form.FeedbackPicPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 意见反馈图片表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class FeedbackPicServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<FeedbackPicMapper, FeedbackPic, FeedbackPicAddForm, FeedbackPicUpdateForm, FeedbackPicPageQueryForm> implements IFeedbackPicService {
    @Override
    public void preAdd(FeedbackPicAddForm addForm,FeedbackPic po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(FeedbackPicUpdateForm updateForm,FeedbackPic po) {
        super.preUpdate(updateForm,po);

    }
}
