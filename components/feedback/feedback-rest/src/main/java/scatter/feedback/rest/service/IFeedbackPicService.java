package scatter.feedback.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.feedback.pojo.po.FeedbackPic;
import scatter.common.rest.service.IBaseService;
import scatter.feedback.pojo.form.FeedbackPicAddForm;
import scatter.feedback.pojo.form.FeedbackPicUpdateForm;
import scatter.feedback.pojo.form.FeedbackPicPageQueryForm;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 意见反馈图片表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface IFeedbackPicService extends IBaseService<FeedbackPic> {


	/**
	 * 添加图片
	 * @param picUrls
	 * @param feedbackId
	 * @param isSuggestion
	 * @return
	 */
	default public boolean addPicUrls(List<String> picUrls,String feedbackId,boolean isSuggestion){
		Assert.hasText(feedbackId,"feedbackId 不能为空");
		Assert.notEmpty(picUrls,"picUrls 不能为空");

		List<FeedbackPic> collect = picUrls.stream().map(url -> new FeedbackPic().setFeedbackId(feedbackId).setPicUrl(url).setIsSuggestion(isSuggestion)).collect(Collectors.toList());
		return saveBatch(collect);
	}

	/**
	 * 删除
	 * @param feedbackId
	 * @param isSuggestion
	 * @return
	 */
	default public boolean deletePicUrls(String feedbackId, Boolean isSuggestion) {
		Assert.hasText(feedbackId,"feedbackId 不能为空");
		LambdaQueryWrapper<FeedbackPic> eq = Wrappers.<FeedbackPic>lambdaQuery().eq(FeedbackPic::getFeedbackId, feedbackId)
				.eq(isSuggestion != null,FeedbackPic::getIsSuggestion, isSuggestion);
		return remove(eq);
	}

	/**
	 * 根据feedback获取
	 * @param feedbackId
	 * @param isSuggestion
	 * @return
	 */
	default List<FeedbackPic> getByFeedbackId(String feedbackId,Boolean isSuggestion){
		Assert.hasText(feedbackId,"feedbackId 不能为空");
		LambdaQueryWrapper<FeedbackPic> eq = Wrappers.<FeedbackPic>lambdaQuery().eq(FeedbackPic::getFeedbackId, feedbackId)
				.eq(isSuggestion != null,FeedbackPic::getIsSuggestion, isSuggestion);
		return list(eq);
	}
	/**
	 * 根据feedback获取
	 * @param feedbackIds
	 * @param isSuggestion
	 * @return
	 */
	default List<FeedbackPic> getByFeedbackIds(List<String> feedbackIds,Boolean isSuggestion){
		Assert.notEmpty(feedbackIds,"feedbackIds 不能为空");
		LambdaQueryWrapper<FeedbackPic> eq = Wrappers.<FeedbackPic>lambdaQuery().in(FeedbackPic::getFeedbackId, feedbackIds)
				.eq(isSuggestion != null,FeedbackPic::getIsSuggestion, isSuggestion);
		return list(eq);
	}
}
