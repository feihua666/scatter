package scatter.feedback.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.feedback.pojo.form.*;
import scatter.feedback.pojo.po.Feedback;
import scatter.feedback.pojo.vo.FeedbackVo;

/**
 * <p>
 * 意见反馈 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FeedbackMapStruct extends IBaseVoMapStruct<Feedback, FeedbackVo>,
                                  IBaseAddFormMapStruct<Feedback,FeedbackAddForm>,
                                  IBaseUpdateFormMapStruct<Feedback,FeedbackUpdateForm>,
                                  IBaseQueryFormMapStruct<Feedback,FeedbackPageQueryForm>{
    FeedbackMapStruct INSTANCE = Mappers.getMapper( FeedbackMapStruct.class );

    Feedback  replyFormToPo(FeedbackReplyForm replyForm);

    Feedback feedbackForMePageQueryFormToPo(FeedbackForMePageQueryForm queryForm);
}
