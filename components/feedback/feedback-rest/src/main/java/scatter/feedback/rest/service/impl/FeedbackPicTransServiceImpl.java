package scatter.feedback.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.feedback.pojo.po.FeedbackPic;
import scatter.feedback.rest.service.IFeedbackPicService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 意见反馈图片翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
public class FeedbackPicTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IFeedbackPicService feedbackPicService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,FeedbackPic.TRANS_FEEDBACKPIC_BY_ID,FeedbackPic.TRANS_REPLY_FEEDBACKPIC_BY_FEEDBACK_ID,FeedbackPic.TRANS_SUGGESTION_FEEDBACKPIC_BY_FEEDBACK_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,FeedbackPic.TRANS_FEEDBACKPIC_BY_ID)) {
            FeedbackPic byId = feedbackPicService.getById(key);
            return new TransResult(byId,key);
        }else if(isEqual(type,FeedbackPic.TRANS_REPLY_FEEDBACKPIC_BY_FEEDBACK_ID)){
            List<FeedbackPic> byFeedback = feedbackPicService.getByFeedbackId(key, false);
            List<String> collect = byFeedback.stream().map(FeedbackPic::getPicUrl).collect(Collectors.toList());
            return new TransResult(collect,key);
        }else if(isEqual(type,FeedbackPic.TRANS_SUGGESTION_FEEDBACKPIC_BY_FEEDBACK_ID)){
            List<FeedbackPic> byFeedback = feedbackPicService.getByFeedbackId(key, true);
            List<String> collect = byFeedback.stream().map(FeedbackPic::getPicUrl).collect(Collectors.toList());
            return new TransResult(collect,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,FeedbackPic.TRANS_FEEDBACKPIC_BY_ID)) {
            return feedbackPicService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if(isEqual(type,FeedbackPic.TRANS_REPLY_FEEDBACKPIC_BY_FEEDBACK_ID)){
            List<FeedbackPic> byFeedback = feedbackPicService.getByFeedbackIds(new ArrayList<>(keys), false);
            return wrapPicUrlsByFeedbackId(byFeedback);
        }else if(isEqual(type,FeedbackPic.TRANS_SUGGESTION_FEEDBACKPIC_BY_FEEDBACK_ID)){
            List<FeedbackPic> byFeedback = feedbackPicService.getByFeedbackIds(new ArrayList<>(keys), true);
            return wrapPicUrlsByFeedbackId(byFeedback);
        }
        return null;
    }

    private List<TransResult<Object, String>> wrapPicUrlsByFeedbackId(List<FeedbackPic> feedbackPics){
        Map<String,List<String>> map = new HashMap<>();
        for (FeedbackPic feedbackPic : feedbackPics) {
            List<String> strings = map.get(feedbackPic.getFeedbackId());
            if(isEmpty(strings)){
                strings = new ArrayList<>();
                map.put(feedbackPic.getFeedbackId(),strings);
            }
            strings.add(feedbackPic.getPicUrl());

        }
        List<TransResult<Object, String>> results = new ArrayList<>();
        for (String key : map.keySet()) {
            results.add(new TransResult<>(map.get(key),key));
        }
        return results;
    }
}
