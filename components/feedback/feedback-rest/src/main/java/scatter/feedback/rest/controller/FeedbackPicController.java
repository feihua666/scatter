package scatter.feedback.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.feedback.rest.FeedbackConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.feedback.pojo.po.FeedbackPic;
import scatter.feedback.pojo.vo.FeedbackPicVo;
import scatter.feedback.pojo.form.FeedbackPicAddForm;
import scatter.feedback.pojo.form.FeedbackPicUpdateForm;
import scatter.feedback.pojo.form.FeedbackPicPageQueryForm;
import scatter.feedback.rest.service.IFeedbackPicService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 意见反馈图片表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "意见反馈图片相关接口")
@RestController
@RequestMapping(FeedbackConfiguration.CONTROLLER_BASE_PATH + "/feedback-pic")
public class FeedbackPicController extends BaseAddUpdateQueryFormController<FeedbackPic, FeedbackPicVo, FeedbackPicAddForm, FeedbackPicUpdateForm, FeedbackPicPageQueryForm> {
    @Autowired
    private IFeedbackPicService iFeedbackPicService;

    @ApiOperation("添加意见反馈图片")
    @PreAuthorize("hasAuthority('FeedbackPic:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public FeedbackPicVo add(@RequestBody @Valid FeedbackPicAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询意见反馈图片")
    @PreAuthorize("hasAuthority('FeedbackPic:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public FeedbackPicVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除意见反馈图片")
    @PreAuthorize("hasAuthority('FeedbackPic:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新意见反馈图片")
    @PreAuthorize("hasAuthority('FeedbackPic:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public FeedbackPicVo update(@RequestBody @Valid FeedbackPicUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询意见反馈图片")
    @PreAuthorize("hasAuthority('FeedbackPic:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<FeedbackPicVo> getList(FeedbackPicPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询意见反馈图片")
    @PreAuthorize("hasAuthority('FeedbackPic:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<FeedbackPicVo> getPage(FeedbackPicPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
