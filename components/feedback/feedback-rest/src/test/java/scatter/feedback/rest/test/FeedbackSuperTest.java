package scatter.feedback.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.feedback.pojo.po.Feedback;
import scatter.feedback.pojo.form.FeedbackAddForm;
import scatter.feedback.pojo.form.FeedbackUpdateForm;
import scatter.feedback.pojo.form.FeedbackPageQueryForm;
import scatter.feedback.rest.service.IFeedbackService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 意见反馈 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class FeedbackSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IFeedbackService feedbackService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return feedbackService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return feedbackService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Feedback mockPo() {
        return JMockData.mock(Feedback.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public FeedbackAddForm mockAddForm() {
        return JMockData.mock(FeedbackAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public FeedbackUpdateForm mockUpdateForm() {
        return JMockData.mock(FeedbackUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public FeedbackPageQueryForm mockPageQueryForm() {
        return JMockData.mock(FeedbackPageQueryForm.class, mockConfig);
    }
}