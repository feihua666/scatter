package scatter.feedback.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.feedback.pojo.po.Feedback;
import scatter.feedback.pojo.form.FeedbackAddForm;
import scatter.feedback.pojo.form.FeedbackUpdateForm;
import scatter.feedback.pojo.form.FeedbackPageQueryForm;
import scatter.feedback.rest.test.FeedbackSuperTest;
import scatter.feedback.rest.service.IFeedbackService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 意见反馈 服务测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
public class FeedbackServiceTest extends FeedbackSuperTest{

    @Autowired
    private IFeedbackService feedbackService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Feedback> pos = feedbackService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}