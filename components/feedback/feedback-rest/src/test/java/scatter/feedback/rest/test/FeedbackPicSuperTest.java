package scatter.feedback.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.feedback.pojo.po.FeedbackPic;
import scatter.feedback.pojo.form.FeedbackPicAddForm;
import scatter.feedback.pojo.form.FeedbackPicUpdateForm;
import scatter.feedback.pojo.form.FeedbackPicPageQueryForm;
import scatter.feedback.rest.service.IFeedbackPicService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 意见反馈图片 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class FeedbackPicSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IFeedbackPicService feedbackPicService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return feedbackPicService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return feedbackPicService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public FeedbackPic mockPo() {
        return JMockData.mock(FeedbackPic.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public FeedbackPicAddForm mockAddForm() {
        return JMockData.mock(FeedbackPicAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public FeedbackPicUpdateForm mockUpdateForm() {
        return JMockData.mock(FeedbackPicUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public FeedbackPicPageQueryForm mockPageQueryForm() {
        return JMockData.mock(FeedbackPicPageQueryForm.class, mockConfig);
    }
}