package scatter.feedback.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.util.List;

import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 意见反馈添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="意见反馈添加表单对象")
public class FeedbackAddForm extends BaseAddForm {

    @ApiModelProperty(value = "联系人姓名，直接填写的")
    private String contactUsername;

    @ApiModelProperty(value = "联系人手机号")
    private String contactMobile;

    @ApiModelProperty(value = "联系人邮箱")
    private String contactEmail;

    @NotEmpty(message="问题建议内容不能为空")
    @ApiModelProperty(value = "问题建议内容",required = true)
    private String suggestionContent;

    /**
     * 该字段对前端隐藏，后台处理，但这里还不能去掉，需要在controller层透传到service去
     */
    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id",hidden = true)
    private String suggestionUserId;

    @ApiModelProperty(value = "第一条意见反馈id，主要串联起一个话题")
    private String firstFeedbackId;

    @ApiModelProperty("图片地址")
    private List<String> picUrls;

}
