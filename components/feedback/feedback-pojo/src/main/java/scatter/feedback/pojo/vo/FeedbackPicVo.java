package scatter.feedback.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 意见反馈图片响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="意见反馈图片响应对象")
public class FeedbackPicVo extends BaseIdVo {

    @ApiModelProperty(value = "意见反馈id")
    private String feedbackId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "是否意见图片，1=反馈图片，0=回复图片")
    private Boolean isSuggestion;

}
