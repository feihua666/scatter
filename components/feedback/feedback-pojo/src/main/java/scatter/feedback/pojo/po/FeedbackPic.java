package scatter.feedback.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 意见反馈图片表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_feedback_pic")
@ApiModel(value="FeedbackPic对象", description="意见反馈图片表")
public class FeedbackPic extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_FEEDBACKPIC_BY_ID = "trans_feedbackpic_by_id_scatter.feedback.pojo.po";
    public static final String TRANS_SUGGESTION_FEEDBACKPIC_BY_FEEDBACK_ID = "trans_suggestion_feedbackpic_by_feedback_id_scatter.feedback.pojo.po";
    public static final String TRANS_REPLY_FEEDBACKPIC_BY_FEEDBACK_ID = "trans_reply_feedbackpic_by_feedback_id_scatter.feedback.pojo.po";

    @ApiModelProperty(value = "意见反馈id")
    private String feedbackId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "是否意见图片，1=反馈图片，0=回复图片")
    private Boolean isSuggestion;


}
