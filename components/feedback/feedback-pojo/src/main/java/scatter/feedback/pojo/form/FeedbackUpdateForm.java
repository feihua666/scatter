package scatter.feedback.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 意见反馈更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="意见反馈更新表单对象")
public class FeedbackUpdateForm extends BaseUpdateIdForm {

    @ApiModelProperty(value = "联系人姓名，直接填写的")
    private String contactUsername;

    @ApiModelProperty(value = "联系人手机号")
    private String contactMobile;

    @ApiModelProperty(value = "联系人邮箱")
    private String contactEmail;

    @NotEmpty(message="问题建议内容不能为空")
    @ApiModelProperty(value = "问题建议内容",required = true)
    private String suggestionContent;

    @NotNull(message="问题建议时间不能为空")
    @ApiModelProperty(value = "问题建议时间",required = true)
    private LocalDateTime suggestionAt;

    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id")
    private String suggestionUserId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "回复时间")
    private LocalDateTime replyAt;

    @ApiModelProperty(value = "回复用户id")
    private String replyUserId;

    @ApiModelProperty(value = "第一条意见反馈id，主要串联起一个话题")
    private String firstFeedbackId;

}
