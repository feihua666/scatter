package scatter.feedback.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.util.List;

import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.feedback.pojo.po.FeedbackPic;


/**
 * <p>
 * 意见反馈响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="意见反馈响应对象")
public class FeedbackVo extends BaseIdVo {

    @ApiModelProperty(value = "联系人姓名，直接填写的")
    private String contactUsername;

    @ApiModelProperty(value = "联系人手机号")
    private String contactMobile;

    @ApiModelProperty(value = "联系人邮箱")
    private String contactEmail;

    @ApiModelProperty(value = "问题建议内容")
    private String suggestionContent;

    @ApiModelProperty(value = "问题建议时间")
    private LocalDateTime suggestionAt;

    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id")
    private String suggestionUserId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "回复时间")
    private LocalDateTime replyAt;

    @ApiModelProperty(value = "回复用户id")
    private String replyUserId;

    @ApiModelProperty(value = "第一条意见反馈id，主要串联起一个话题")
    private String firstFeedbackId;

    @TransBy(type = FeedbackPic.TRANS_SUGGESTION_FEEDBACKPIC_BY_FEEDBACK_ID,byFieldName = "id")
    @ApiModelProperty("建议图片")
    private List<String> suggestionPicUrls;

    @TransBy(type = FeedbackPic.TRANS_REPLY_FEEDBACKPIC_BY_FEEDBACK_ID,byFieldName = "id")
    @ApiModelProperty("回复图片")
    private List<String> replyPicUrls;

}
