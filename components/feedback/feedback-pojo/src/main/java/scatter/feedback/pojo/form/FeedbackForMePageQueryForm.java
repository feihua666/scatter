package scatter.feedback.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;

import java.time.LocalDateTime;


/**
 * <p>
 * 我的意见反馈分页表单对象
 * 目前无查询参数
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="我的意见反馈分页表单对象")
public class FeedbackForMePageQueryForm extends BasePageQueryForm {


}
