package scatter.feedback.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 意见反馈表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_feedback")
@ApiModel(value="Feedback对象", description="意见反馈表")
public class Feedback extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_FEEDBACK_BY_ID = "trans_feedback_by_id_scatter.feedback.pojo.po";

    @ApiModelProperty(value = "联系人姓名，直接填写的")
    private String contactUsername;

    @ApiModelProperty(value = "联系人手机号")
    private String contactMobile;

    @ApiModelProperty(value = "联系人邮箱")
    private String contactEmail;

    @ApiModelProperty(value = "问题建议内容")
    private String suggestionContent;

    @ApiModelProperty(value = "问题建议时间")
    private LocalDateTime suggestionAt;

    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id")
    private String suggestionUserId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "回复时间")
    private LocalDateTime replyAt;

    @ApiModelProperty(value = "回复用户id")
    private String replyUserId;

    @ApiModelProperty(value = "第一条意见反馈id，主要串联起一个话题")
    private String firstFeedbackId;


}
