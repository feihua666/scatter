# captcha 验证码组件

验证码组件提供后端验证服务  
## 配置
```yaml
scatter:
  # 验证码配置
  captcha:
    prevent-configs:
      # 数组配置，配置哪些接口需要验证码验证
      - purpose: /login
```
更多配置可以查看：scatter.captcha.rest.filter.PatternConfigProperties  

## 设置需要验证码
UserLoginFailedCaptchaEventListener实现了在登录失败时设置需要验证码的时机，需要配合springSecurity登录使用  
如果其它接口需要验证码需要手动实现调用：
```java
scatter.captcha.rest.service.ICaptchaPreventService.setShouldValidate(java.lang.String, boolean, javax.servlet.http.HttpServletRequest)
```  

## 使用示例
假设有以下场景，用户下载一个报表接口/download/report  
+ **每次下载前都需要验证码，配置如下**
```yaml
scatter:
  # 验证码配置
  captcha:
    prevent-configs:
      # 数组配置，配置哪些接口需要验证码验证
      - purpose: /download/report
      - error-count: 0
```
+ **第一次下载不需要验证码，从第二次开始都需要**
```yaml
scatter:
  # 验证码配置
  captcha:
    prevent-configs:
      # 数组配置，配置哪些接口需要验证码验证
      - purpose: /download/report
      - error-count: 1
```
同时需要在下载接口中设置如下代码  
```java

@Autowired
private HttpServletRequest request;
@Autowired
private ICaptchaPreventService iCaptchaPreventService;


iCaptchaPreventService.setShouldValidate(RequestPurposeTools.resolvePurpose(request),request);

```

## 扩展
目前验证码组件仅实现了滑动验证码的支持，可以配合前端组件 scatter/components/common/web/components/pc/common/slideVerify一起使用  
如果想扩展图片验证码等其它验证码实现也很简单，步骤如下：  
+ 实现接口ICaptchaService
+ 在CaptchaController中添加对应类型的获取和验证入口

## 验证码存储
在以上示例中默认的存储是在session中存储，并不可靠，建议针对以下接口自定义实现存储  
+ ICaptchaPreventStoreService
+ ICaptchaStoreService