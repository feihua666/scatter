package scatter.captcha.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import scatter.captcha.pojo.CaptchaType;
import scatter.captcha.pojo.form.CaptchaQueryForm;
import scatter.captcha.pojo.form.CaptchaSlideValidForm;
import scatter.captcha.pojo.vo.CaptchaSlideVo;
import scatter.captcha.pojo.vo.CaptchaValidResultVo;
import scatter.captcha.pojo.vo.CaptchaVo;
import scatter.captcha.rest.CaptchaConfiguration;
import scatter.captcha.rest.service.ICaptchaService;
import scatter.common.rest.controller.SuperController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by yangwei
 * Created at 2021/1/14 17:12
 */
@RestController
@RequestMapping(CaptchaConfiguration.CONTROLLER_BASE_PATH + "/captcha")
@Api(tags = "验证码相关接口")
public class CaptchaController extends SuperController {

    @Autowired
    private ICaptchaService iCaptchaService;

    @ApiOperation("获取滑动验证码")
    @GetMapping("/slide")
    @ResponseStatus(HttpStatus.OK)
    public CaptchaVo getSlideCaptcha(@Validated CaptchaQueryForm queryForm, HttpServletRequest request) throws IOException {
        return iCaptchaService.getCaptcha(CaptchaType.slide,queryForm,request);
    }

    @ApiOperation("验证滑动验证码")
    @PostMapping("/slide")
    @ResponseStatus(HttpStatus.OK)
    public CaptchaValidResultVo validateSlideCaptcha(@Validated @RequestBody CaptchaSlideValidForm queryForm, HttpServletRequest request) {
        boolean b = iCaptchaService.validateCaptcha(CaptchaType.slide,queryForm,request);
        if(!b){
            throw new IllegalArgumentException("验证码不正确");
        }
        return CaptchaValidResultVo.ofSlide(b);
    }
}
