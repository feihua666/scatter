package scatter.captcha.rest.service;

import scatter.captcha.pojo.CaptchaStore;
import scatter.captcha.pojo.CaptchaType;
import scatter.captcha.pojo.form.CaptchaValidForm;
import scatter.captcha.pojo.vo.CaptchaVo;

import java.io.IOException;

/**
 * Created by yangwei
 * Created at 2021/1/14 17:54
 */
public interface ICaptchaProvider<R extends CaptchaStore<?,?>> {

    /**
     * 是否支持
     * @param captchaType
     * @return
     */
    boolean support(CaptchaType captchaType);

    /**
     * 获取验证码
     * @param captchaType
     * @return
     */
    R getCaptcha(CaptchaType captchaType) throws IOException;

}
