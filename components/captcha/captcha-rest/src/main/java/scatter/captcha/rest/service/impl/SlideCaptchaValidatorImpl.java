package scatter.captcha.rest.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import scatter.captcha.pojo.CaptchaStore;
import scatter.captcha.pojo.CaptchaType;
import scatter.captcha.pojo.form.CaptchaSlideValidForm;
import scatter.captcha.pojo.vo.CaptchaSlideVo;
import scatter.captcha.rest.service.ICaptchaValidator;

/**
 * Created by yangwei
 * Created at 2021/1/18 18:23
 */
@Slf4j
@Service
public class SlideCaptchaValidatorImpl implements ICaptchaValidator<CaptchaSlideValidForm, CaptchaStore<CaptchaSlideVo,Integer>> {
    @Override
    public boolean support(CaptchaType captchaType) {
        return CaptchaType.slide == captchaType;
    }

    @Override
    public boolean validate(CaptchaSlideValidForm captchaValidForm, CaptchaStore<CaptchaSlideVo, Integer> captchaStore) {
        if (captchaStore == null) {
            return false;
        }
        if(Math.abs(captchaValidForm.getSlideImageOffsetX()- ((Integer) captchaStore.getCaptchaSecretData())) <=1){
            return true;
        }
        return false;
    }
}
