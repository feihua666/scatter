package scatter.captcha.rest.service;

import scatter.captcha.pojo.CaptchaStore;
import scatter.captcha.pojo.CaptchaType;
import scatter.captcha.pojo.form.CaptchaValidForm;

import java.io.IOException;

/**
 * 验证码验证器
 * Created by yangwei
 * Created at 2021/1/14 17:54
 */
public interface ICaptchaValidator<F extends CaptchaValidForm,S extends CaptchaStore<?,?>> {

    /**
     * 是否支持
     * @param captchaType
     * @return
     */
    boolean support(CaptchaType captchaType);

    /**
     * 获取验证码
     * @param captchaValidForm
     * @param captchaStore
     * @return
     */
    boolean validate(F captchaValidForm, S captchaStore);

}
