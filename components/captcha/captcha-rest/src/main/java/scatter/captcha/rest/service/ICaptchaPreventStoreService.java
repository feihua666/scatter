package scatter.captcha.rest.service;

import scatter.captcha.pojo.CaptchaStore;

import javax.servlet.http.HttpServletRequest;

/**
 * 验证码阻止存储服务类
 * Created by yangwei
 * Created at 2021/1/15 17:08
 */
public interface ICaptchaPreventStoreService {

    /**
     * 存储
     * @param obj
     */
    void save( String purpose,Object obj, HttpServletRequest request);

    /**
     * 获取
     * @return
     */
    Object get(String purpose, HttpServletRequest request);

    /**
     * 清除存储
     * @param purpose
     */
    void clear(String purpose, HttpServletRequest request);
}
