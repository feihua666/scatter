package scatter.captcha.rest.service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

/**
 * 背景图片随机服务类
 * Created by yangwei
 * Created at 2021/1/15 16:13
 */
public interface ICaptchaBgImageRandomService {

    /**
     * 获取随机背景图片输入流
     * @return
     */
    BufferedImage getBgImageRandomInputStream() throws IOException;
}
