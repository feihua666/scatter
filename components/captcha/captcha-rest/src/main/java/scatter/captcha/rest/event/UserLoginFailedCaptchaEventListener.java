package scatter.captcha.rest.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;
import scatter.captcha.rest.service.ICaptchaPreventService;
import scatter.captcha.rest.tools.RequestPurposeTools;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户登录失败监听
 * Created by yangwei
 * Created at 2021/1/15 17:47
 */
@Component
public class UserLoginFailedCaptchaEventListener {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private ICaptchaPreventService iCaptchaPreventService;

    @EventListener
    public void method(AuthenticationFailureBadCredentialsEvent event) {
        iCaptchaPreventService.setShouldValidate(RequestPurposeTools.resolvePurpose(request),request);
    }
}
