package scatter.captcha.rest.service;

import scatter.captcha.pojo.CaptchaType;
import scatter.captcha.pojo.form.CaptchaQueryForm;
import scatter.captcha.pojo.form.CaptchaValidForm;
import scatter.captcha.pojo.vo.CaptchaVo;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by yangwei
 * Created at 2021/1/14 17:49
 */
public interface ICaptchaService {

    /**
     * 获取验证码
     * @param queryForm
     * @return
     */
    CaptchaVo getCaptcha(CaptchaType captchaType,CaptchaQueryForm queryForm,  HttpServletRequest request) throws IOException;

    /**
     * 验证验证码
     * @param captchaType
     * @param validForm
     * @return
     */
    boolean validateCaptcha(CaptchaType captchaType,CaptchaValidForm validForm,  HttpServletRequest request);

}
