package scatter.captcha.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.captcha.pojo.CaptchaStore;
import scatter.captcha.pojo.CaptchaType;
import scatter.captcha.rest.service.ICaptchaStoreService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yangwei
 * Created at 2021/1/15 17:30
 */
public class DefaultCaptchaStoreServiceImpl implements ICaptchaStoreService {

    private static String captchaStoreKeyPrefix = "DefaultCaptchaStoreServiceImpl.captchaStoreKeyPrefix";


    /**
     * 获取存储key
     * @param purpose
     * @return
     */
    private String getKey(String purpose) {
        return captchaStoreKeyPrefix + purpose;
    }

    @Override
    public void saveCaptcha(String purpose,CaptchaStore captchaStore,  HttpServletRequest request) {
        request.getSession().setAttribute(getKey(purpose),captchaStore);
    }

    @Override
    public CaptchaStore getCaptcha(String purpose,  HttpServletRequest request) {
        return (CaptchaStore) request.getSession().getAttribute(getKey(purpose));
    }

    @Override
    public void clearCaptcha(String purpose,  HttpServletRequest request) {
        request.getSession().removeAttribute(getKey(purpose));
    }
}
