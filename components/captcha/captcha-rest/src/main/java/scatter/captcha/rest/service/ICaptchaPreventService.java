package scatter.captcha.rest.service;

import javax.servlet.http.HttpServletRequest;

/**
 * 验证码阻止服务
 * 主要负责什么时候需要验证验证码
 * Created by yangwei
 * Created at 2021/1/19 9:49
 */
public interface ICaptchaPreventService {

    /**
     * 标识是否应该验证验证码
     * 注意：该方法不是幂等的
     * @param purpose
     */
    void setShouldValidate(String purpose,  HttpServletRequest request);

    /**
     * 直接设置是否应该验证
     * @param purpose
     * @param should
     * @param request
     */
    void setShouldValidate(String purpose, boolean should, HttpServletRequest request);

    /**
     * 重置
     * @param purpose
     * @param request
     */
    void reset(String purpose, HttpServletRequest request);

    /**
     * 是否应该验证验证码
     * @param purpose
     * @return
     */
    boolean isShouldValidate(String purpose,  HttpServletRequest request);
}
