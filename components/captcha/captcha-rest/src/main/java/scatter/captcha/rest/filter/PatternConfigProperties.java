package scatter.captcha.rest.filter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-01-17 22:57
 */
@Component
@Setter
@Getter
@ConfigurationProperties(prefix = "scatter.captcha")
public class PatternConfigProperties {
    private List<CaptchaPreventConfig> preventConfigs;

    /**
     * 配置类
     */
    @Setter
    @Getter
    public static class CaptchaPreventConfig{
        // 匹配的url
        private String purpose;
        // 错误次数，0为不限制，都要验证
        private int errorCount = 2;
        // 是否在重置时忽略计数
        boolean ignoreCount = false;
    }
}
