package scatter.captcha.rest.service;

import scatter.captcha.pojo.CaptchaStore;
import scatter.captcha.pojo.CaptchaType;

import javax.servlet.http.HttpServletRequest;

/**
 * 验证码存储服务类
 * Created by yangwei
 * Created at 2021/1/15 17:08
 */
public interface ICaptchaStoreService {

    /**
     * 存储验证码
     * @param captchaStore
     */
    void saveCaptcha(String purpose,CaptchaStore captchaStore,  HttpServletRequest request);

    /**
     * 获取存储的验证码
     * @return
     */
    CaptchaStore getCaptcha(String purpose,  HttpServletRequest request);

    /**
     * 清除存储
     * @param purpose
     */
    void clearCaptcha(String purpose,  HttpServletRequest request);
}
