package scatter.captcha.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import scatter.captcha.rest.service.ICaptchaPreventStoreService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yangwei
 * Created at 2021/1/19 9:57
 */
public class DefaultCaptchaPreventStoreServiceImpl implements ICaptchaPreventStoreService {

    private static String captchaPreventStoreKeyPrefix = "DefaultCaptchaPreventStoreServiceImpl.captchaPreventStoreKeyPrefix";
    /**
     * 获取存储key
     * @param purpose
     * @return
     */
    private String getKey(String purpose) {
        return captchaPreventStoreKeyPrefix + purpose;
    }

    @Override
    public void save(String purpose,Object obj,  HttpServletRequest request) {
        request.getSession().setAttribute(getKey(purpose),obj);
    }

    @Override
    public Object get(String purpose, HttpServletRequest request) {
        return request.getSession().getAttribute(getKey(purpose));
    }

    @Override
    public void clear(String purpose, HttpServletRequest request) {
        request.getSession().removeAttribute(getKey(purpose));
    }
}
