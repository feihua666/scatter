package scatter.captcha.rest.tools;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yangwei
 * Created at 2021/1/19 10:34
 */
public class RequestPurposeTools {

    /**
     * 获取请求的目的
     * @param request
     * @return
     */
    public static String resolvePurpose(HttpServletRequest request) {
        return request.getRequestURI();
    }
}
