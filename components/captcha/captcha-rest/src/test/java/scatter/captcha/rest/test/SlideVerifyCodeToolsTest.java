package scatter.captcha.rest.test;

import cn.hutool.core.img.ImgUtil;
import scatter.captcha.rest.tools.SlideVerifyCodeTools;

import java.io.File;
import java.io.IOException;

import static scatter.captcha.rest.tools.SlideVerifyCodeTools.createImage;

/**
 * Created by yangwei
 * Created at 2021/1/19 16:00
 */
public class SlideVerifyCodeToolsTest {

    public static void main(String[] args) throws IOException {
        SlideVerifyCodeTools.cutResult image = createImage("D:\\slide-verify-code-test\\img2.jpg", 45, 45, 7, 2, 100, 100);
        ImgUtil.write(image.getBlock(),new File("D:\\slide-verify-code-test\\block.png"));
        ImgUtil.write(image.getBg(),new File("D:\\slide-verify-code-test\\bgNew.jpg"));
    }
}
