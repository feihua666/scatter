package scatter.captcha.pojo;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.BasePojo;

import java.time.LocalDateTime;

/**
 * Created by yangwei
 * Created at 2021/1/15 17:10
 */
@Setter
@Getter
public class CaptchaStore<D,S> extends BasePojo {

    /**
     * 验证码类型
     */
    private CaptchaType captchaType;
    /**
     * 验证码数据对象
     */
    private D captchaData;
    /**
     * 验证码创建时间
     */
    private LocalDateTime createAt;

    /**
     * 验证码保密部分数据对象
     */
    private S captchaSecretData;

    public static CaptchaStore newCaptchaStore(CaptchaType captchaType,Object captchaData,Object captchaSecretData) {
        CaptchaStore captchaStore = new CaptchaStore();
        captchaStore.setCaptchaType(captchaType);
        captchaStore.setCaptchaData(captchaData);
        captchaStore.setCaptchaSecretData(captchaSecretData);
        captchaStore.setCreateAt(LocalDateTime.now());
        return captchaStore;
    }
}
