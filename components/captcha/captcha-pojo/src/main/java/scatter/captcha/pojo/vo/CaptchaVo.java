package scatter.captcha.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseVo;

/**
 * Created by yangwei
 * Created at 2021/1/14 17:19
 */
@Setter
@Getter
@ApiModel(value="验证码数据响应对象")
public class CaptchaVo extends BaseVo {

    @ApiModelProperty(value = "验证码类型",example = "slide、click")
    private String type;

    @ApiModelProperty(value = "背景图Base64编码数据")
    private String bgImageBase64Data;

}
