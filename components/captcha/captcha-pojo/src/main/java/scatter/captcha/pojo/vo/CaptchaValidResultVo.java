package scatter.captcha.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.captcha.pojo.CaptchaType;
import scatter.common.pojo.vo.BaseVo;

/**
 * 验证码验证结果
 * Created by yangwei
 * Created at 2021/1/14 17:19
 */
@Setter
@Getter
@ApiModel(value="验证码验证结果数据响应对象")
public class CaptchaValidResultVo extends BaseVo {

    @ApiModelProperty(value = "验证码类型",example = "slide、click")
    private String type;

    @ApiModelProperty(value = "是否成功")
    private boolean success;

    public static CaptchaValidResultVo ofSlide(boolean success) {
        CaptchaValidResultVo captchaValidResultVo = new CaptchaValidResultVo();
        captchaValidResultVo.setSuccess(success);
        captchaValidResultVo.setType(CaptchaType.slide.name());
        return captchaValidResultVo;
    }
    public static CaptchaValidResultVo ofClick(boolean success) {
        CaptchaValidResultVo captchaValidResultVo = new CaptchaValidResultVo();
        captchaValidResultVo.setSuccess(success);
        captchaValidResultVo.setType(CaptchaType.click.name());
        return captchaValidResultVo;
    }
}
