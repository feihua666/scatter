package scatter.captcha.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotEmpty;

/**
 * 验证码获取表单
 * Created by yangwei
 * Created at 2021/1/14 17:14
 */
@Setter
@Getter
@ApiModel(value="验证验证码表单对象")
public class CaptchaValidForm extends CaptchaForm {

}
