package scatter.captcha.pojo;

/**
 * Created by yangwei
 * Created at 2021/1/14 18:08
 */
public enum CaptchaType {
    slide,
    click,
    simplePlus
    ;
}
