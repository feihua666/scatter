package scatter.captcha.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by yangwei
 * Created at 2021/1/14 17:30
 */
@Setter
@Getter
@ApiModel(value="滑块验证码数据响应对象")
public class CaptchaSlideVo extends CaptchaVo {

    @ApiModelProperty(value = "滑动图Base64编码数据")
    private String slideImageBase64Data;

    @ApiModelProperty(value = "滑动图相对于背景图纵偏移量")
    private Integer slideImageOffsetY;
}
