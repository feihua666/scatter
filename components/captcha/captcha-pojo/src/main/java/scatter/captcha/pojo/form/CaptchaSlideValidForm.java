package scatter.captcha.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 滑动验证验证码表单
 * Created by yangwei
 * Created at 2021/1/14 17:14
 */
@Setter
@Getter
@ApiModel(value="滑动验证验证码表单对象")
public class CaptchaSlideValidForm extends CaptchaValidForm {


    @NotNull(message = "滑动图相对于背景图纵偏移量不能为空")
    @ApiModelProperty(value = "滑动图相对于背景图纵偏移量")
    private Integer slideImageOffsetY;

    @NotNull(message = "滑动图相对于背景图横偏移量不能为空")
    @ApiModelProperty(value = "滑动图相对于背景图横偏移量")
    private Integer slideImageOffsetX;

    @NotNull(message = "滑动的毫秒数不能为空")
    @ApiModelProperty(value = "滑动的毫秒数")
    private Integer slideDuration;
}
