package scatter.serverresource.rest.code.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.rest.ServerResourceConfiguration;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.rest.code.service.IServerResourceCodeService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 代码表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/inner/code/server-resource-code")
public class ServerResourceCodeInnerController extends BaseInnerController<ServerResourceCode> {
 @Autowired
 private IServerResourceCodeService serverResourceCodeService;

 public IServerResourceCodeService getService(){
     return serverResourceCodeService;
 }
}
