package scatter.serverresource.rest.general.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.rest.ServerResourceConfiguration;
import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.serverresource.rest.general.service.IServerResourceAccountService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 账号表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/inner/general/server-resource-account")
public class ServerResourceAccountInnerController extends BaseInnerController<ServerResourceAccount> {
 @Autowired
 private IServerResourceAccountService serverResourceAccountService;

 public IServerResourceAccountService getService(){
     return serverResourceAccountService;
 }
}
