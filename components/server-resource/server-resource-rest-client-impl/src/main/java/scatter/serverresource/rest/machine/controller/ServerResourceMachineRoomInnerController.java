package scatter.serverresource.rest.machine.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.rest.ServerResourceConfiguration;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.rest.machine.service.IServerResourceMachineRoomService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 机房表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/inner/machine/server-resource-machine-room")
public class ServerResourceMachineRoomInnerController extends BaseInnerController<ServerResourceMachineRoom> {
 @Autowired
 private IServerResourceMachineRoomService serverResourceMachineRoomService;

 public IServerResourceMachineRoomService getService(){
     return serverResourceMachineRoomService;
 }
}
