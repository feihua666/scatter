package scatter.serverresource.rest.project.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.rest.ServerResourceConfiguration;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.rest.project.service.IServerResourceProjectService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 项目表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/inner/project/server-resource-project")
public class ServerResourceProjectInnerController extends BaseInnerController<ServerResourceProject> {
 @Autowired
 private IServerResourceProjectService serverResourceProjectService;

 public IServerResourceProjectService getService(){
     return serverResourceProjectService;
 }
}
