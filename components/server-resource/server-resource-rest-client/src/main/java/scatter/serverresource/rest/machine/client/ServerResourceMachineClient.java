package scatter.serverresource.rest.machine.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 机器表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Component
@FeignClient(value = "ServerResourceMachine-client")
public interface ServerResourceMachineClient {

}
