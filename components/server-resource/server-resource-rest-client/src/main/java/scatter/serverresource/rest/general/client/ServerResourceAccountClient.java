package scatter.serverresource.rest.general.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 账号表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Component
@FeignClient(value = "ServerResourceAccount-client")
public interface ServerResourceAccountClient {

}
