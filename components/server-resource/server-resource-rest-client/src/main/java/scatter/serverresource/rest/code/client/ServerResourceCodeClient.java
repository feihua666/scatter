package scatter.serverresource.rest.code.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 代码表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Component
@FeignClient(value = "ServerResourceCode-client")
public interface ServerResourceCodeClient {

}
