package scatter.serverresource.rest.general.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 环境表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Component
@FeignClient(value = "ServerResourceEnvironment-client")
public interface ServerResourceEnvironmentClient {

}
