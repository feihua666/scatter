package scatter.serverresource.rest.general.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 代码和项目的版本控制关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Component
@FeignClient(value = "ServerResourceCodeProjectVcRel-client")
public interface ServerResourceCodeProjectVcRelClient {

}
