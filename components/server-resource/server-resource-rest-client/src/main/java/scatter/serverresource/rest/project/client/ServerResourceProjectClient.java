package scatter.serverresource.rest.project.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 项目表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Component
@FeignClient(value = "ServerResourceProject-client")
public interface ServerResourceProjectClient {

}
