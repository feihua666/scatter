package scatter.serverresource.pojo.code.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 代码分页表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="代码分页表单对象")
public class ServerResourceCodePageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "是否分组")
    private Boolean isGroup;

    @ApiModelProperty(value = "项目id，代码归属哪个项目")
    private String projectId;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
