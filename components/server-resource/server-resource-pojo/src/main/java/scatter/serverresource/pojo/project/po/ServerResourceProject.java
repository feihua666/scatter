package scatter.serverresource.pojo.project.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 项目表
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_server_resource_project")
@ApiModel(value="ServerResourceProject对象", description="项目表")
public class ServerResourceProject extends BaseTreePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SERVERRESOURCEPROJECT_BY_ID = "trans_serverresourceproject_by_id_scatter.serverresource.pojo.project.po";

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "环境id")
    private String environmentId;

    @ApiModelProperty(value = "是否分组")
    private Boolean isGroup;

    @ApiModelProperty(value = "访问链接")
    private String visitLink;

    @ApiModelProperty(value = "账号")
    private String visitLinkAccount;

    @ApiModelProperty(value = "密码")
    private String visitLinkPassword;

    @ApiModelProperty(value = "类型，自研代码，三方中间件，版本控制等")
    private String typeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
