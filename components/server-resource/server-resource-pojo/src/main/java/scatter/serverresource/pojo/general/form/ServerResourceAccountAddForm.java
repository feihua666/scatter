package scatter.serverresource.pojo.general.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 账号添加表单对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="账号添加表单对象")
public class ServerResourceAccountAddForm extends BaseAddForm {

    @NotEmpty(message="账号不能为空")
    @ApiModelProperty(value = "账号",required = true)
    private String account;

    @ApiModelProperty(value = "账号密码")
    private String password;

    @NotEmpty(message="数据id不能为空")
    @ApiModelProperty(value = "数据id",required = true)
    private String dataId;

    @NotEmpty(message="数据类型不能为空")
    @ApiModelProperty(value = "数据类型，字典id",required = true)
    private String dataTypeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;

}
