package scatter.serverresource.pojo.general.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 账号分页表单对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="账号分页表单对象")
public class ServerResourceAccountPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "账号密码")
    private String password;

    @ApiModelProperty(value = "数据id")
    private String dataId;

    @ApiModelProperty(value = "数据类型，字典id")
    private String dataTypeDictId;

    @Like
    @ApiModelProperty(value = "描述")
    private String remark;

}
