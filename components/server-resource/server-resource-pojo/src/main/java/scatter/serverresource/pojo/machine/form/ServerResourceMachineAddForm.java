package scatter.serverresource.pojo.machine.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 机器添加表单对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="机器添加表单对象")
public class ServerResourceMachineAddForm extends BaseAddForm {

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称，可填hostname",required = true)
    private String name;

    @ApiModelProperty(value = "IPV4地址")
    private String ipv4;

    @ApiModelProperty(value = "IPV6地址")
    private String ipv6;

    @ApiModelProperty(value = "IPV4内网地址")
    private String ipv4Inner;

    @ApiModelProperty(value = "IPV6内网地址")
    private String ipv6Inner;

    @NotEmpty(message="机器类型不能为空")
    @ApiModelProperty(value = "机器类型，物理机、虚拟机",required = true)
    private String typeDictId;

    @NotEmpty(message="所在机房不能为空")
    @ApiModelProperty(value = "所在机房",required = true)
    private String machineRoomId;

    @NotNull(message="cpu核数不能为空")
    @ApiModelProperty(value = "cpu核数",required = true)
    private Integer cpuNum;

    @NotNull(message="内存大小不能为空")
    @ApiModelProperty(value = "内存大小，G",required = true)
    private Integer memorySize;

    @NotNull(message="硬盘大小不能为空")
    @ApiModelProperty(value = "硬盘大小，G",required = true)
    private Integer diskSize;

    @NotNull(message="硬盘是否ssd不能为空")
    @ApiModelProperty(value = "硬盘是否ssd",required = true)
    private Boolean isDiskSsd;

    @ApiModelProperty(value = "root账号，或管理员账号")
    private String rootAccount;

    @ApiModelProperty(value = "root密码，或管理员密码")
    private String rootPassword;

    @ApiModelProperty(value = "环境id")
    private String environmentId;

}
