package scatter.serverresource.pojo.code.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代码表
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_server_resource_code")
@ApiModel(value="ServerResourceCode对象", description="代码表")
public class ServerResourceCode extends BaseTreePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SERVERRESOURCECODE_BY_ID = "trans_serverresourcecode_by_id_scatter.serverresource.pojo.code.po";

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "是否分组")
    private Boolean isGroup;

    @ApiModelProperty(value = "项目id，代码归属哪个项目")
    private String projectId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
