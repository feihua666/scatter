package scatter.serverresource.pojo.general.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 环境表
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_server_resource_environment")
@ApiModel(value="ServerResourceEnvironment对象", description="环境表")
public class ServerResourceEnvironment extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SERVERRESOURCEENVIRONMENT_BY_ID = "trans_serverresourceenvironment_by_id_scatter.serverresource.pojo.general.po";

    @ApiModelProperty(value = "名称")
    private String name;


}
