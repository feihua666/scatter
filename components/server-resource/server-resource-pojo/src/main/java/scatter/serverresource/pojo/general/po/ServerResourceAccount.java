package scatter.serverresource.pojo.general.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 账号表
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_server_resource_account")
@ApiModel(value="ServerResourceAccount对象", description="账号表")
public class ServerResourceAccount extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SERVERRESOURCEACCOUNT_BY_ID = "trans_serverresourceaccount_by_id_scatter.serverresource.pojo.general.po";

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "账号密码")
    private String password;

    @ApiModelProperty(value = "数据id")
    private String dataId;

    @ApiModelProperty(value = "数据类型，字典id")
    private String dataTypeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
