package scatter.serverresource.pojo.general.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 代码和项目的版本控制响应对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="代码和项目的版本控制响应对象")
public class ServerResourceCodeProjectVcRelVo extends BaseIdVo {

    @ApiModelProperty(value = "代码id")
    private String codeId;

    @ApiModelProperty(value = "项目的版本控制id, vc代表版本控制，项目表的版本控制类型的id，如git，svn等")
    private String projectVcId;

}
