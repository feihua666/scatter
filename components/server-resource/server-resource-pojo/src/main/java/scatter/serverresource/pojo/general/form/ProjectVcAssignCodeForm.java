package scatter.serverresource.pojo.general.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
/**
 * <p>
 * 代码分配项目的版本控制表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@PropValid
@Setter
@Getter
@ApiModel(value="项目的版本控制分配代码表单对象")
public class ProjectVcAssignCodeForm extends BaseForm {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "选择的代码id")
    private List<String> checkedCodeIds;

    @PropValid.DependCondition(message = "未选择的代码id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的代码id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedCodeIds;

    @NotEmpty(message = "项目的版本控制id不能为空")
    @ApiModelProperty(value = "项目的版本控制id")
    private String projectVcId;


    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
