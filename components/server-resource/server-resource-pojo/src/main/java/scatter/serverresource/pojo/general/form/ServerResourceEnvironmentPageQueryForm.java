package scatter.serverresource.pojo.general.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 环境分页表单对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="环境分页表单对象")
public class ServerResourceEnvironmentPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "名称")
    private String name;

}
