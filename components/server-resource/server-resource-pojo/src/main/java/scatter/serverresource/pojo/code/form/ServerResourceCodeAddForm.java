package scatter.serverresource.pojo.code.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 代码添加表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="代码添加表单对象")
public class ServerResourceCodeAddForm extends BaseAddForm {

    @NotEmpty(message="项目名称不能为空")
    @ApiModelProperty(value = "项目名称",required = true)
    private String name;

    @NotNull(message="是否分组不能为空")
    @ApiModelProperty(value = "是否分组",required = true)
    private Boolean isGroup;

    @NotEmpty(message="项目id不能为空")
    @ApiModelProperty(value = "项目id，代码归属哪个项目",required = true)
    private String projectId;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
