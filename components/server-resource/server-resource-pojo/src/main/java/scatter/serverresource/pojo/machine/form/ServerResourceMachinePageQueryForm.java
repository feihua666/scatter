package scatter.serverresource.pojo.machine.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 机器分页表单对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="机器分页表单对象")
public class ServerResourceMachinePageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "名称，可填hostname")
    private String name;

    @ApiModelProperty(value = "IPV4地址")
    private String ipv4;

    @ApiModelProperty(value = "IPV6地址")
    private String ipv6;

    @ApiModelProperty(value = "IPV4内网地址")
    private String ipv4Inner;

    @ApiModelProperty(value = "IPV6内网地址")
    private String ipv6Inner;

    @ApiModelProperty(value = "机器类型，物理机、虚拟机")
    private String typeDictId;

    @ApiModelProperty(value = "所在机房")
    private String machineRoomId;

    @ApiModelProperty(value = "cpu核数")
    private Integer cpuNum;

    @ApiModelProperty(value = "内存大小，G")
    private Integer memorySize;

    @ApiModelProperty(value = "硬盘大小，G")
    private Integer diskSize;

    @ApiModelProperty(value = "硬盘是否ssd")
    private Boolean isDiskSsd;

    @ApiModelProperty(value = "root账号，或管理员账号")
    private String rootAccount;

    @ApiModelProperty(value = "root密码，或管理员密码")
    private String rootPassword;

    @ApiModelProperty(value = "环境id")
    private String environmentId;

}
