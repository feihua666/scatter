package scatter.serverresource.pojo.machine.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 机房表
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_server_resource_machine_room")
@ApiModel(value="ServerResourceMachineRoom对象", description="机房表")
public class ServerResourceMachineRoom extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SERVERRESOURCEMACHINEROOM_BY_ID = "trans_serverresourcemachineroom_by_id_scatter.serverresource.pojo.machine.po";

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "区域位置")
    private String area;


}
