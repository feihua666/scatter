package scatter.serverresource.pojo.machine.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.common.trans.TransConstants;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;


/**
 * <p>
 * 机器响应对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="机器响应对象")
public class ServerResourceMachineVo extends BaseIdVo {

    @ApiModelProperty(value = "名称，可填hostname")
    private String name;

    @ApiModelProperty(value = "IPV4地址")
    private String ipv4;

    @ApiModelProperty(value = "IPV6地址")
    private String ipv6;

    @ApiModelProperty(value = "IPV4内网地址")
    private String ipv4Inner;

    @ApiModelProperty(value = "IPV6内网地址")
    private String ipv6Inner;

    @ApiModelProperty(value = "机器类型，物理机、虚拟机")
    private String typeDictId;

    @TransBy(type = TransConstants.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "机器类型,字典名称，物理机、虚拟机")
    private String typeDictName;

    @ApiModelProperty(value = "所在机房")
    private String machineRoomId;

    @TransBy(type = ServerResourceMachineRoom.TRANS_SERVERRESOURCEMACHINEROOM_BY_ID,byFieldName = "machineRoomId",mapValueField = "name")
    @ApiModelProperty(value = "所在机房")
    private String machineRoomName;

    @ApiModelProperty(value = "cpu核数")
    private Integer cpuNum;

    @ApiModelProperty(value = "内存大小，G")
    private Integer memorySize;

    @ApiModelProperty(value = "硬盘大小，G")
    private Integer diskSize;

    @ApiModelProperty(value = "硬盘是否ssd")
    private Boolean isDiskSsd;

    @ApiModelProperty(value = "root账号，或管理员账号")
    private String rootAccount;

    @ApiModelProperty(value = "root密码，或管理员密码")
    private String rootPassword;

    @ApiModelProperty(value = "环境id")
    private String environmentId;

    @TransBy(type = ServerResourceEnvironment.TRANS_SERVERRESOURCEENVIRONMENT_BY_ID,byFieldName = "environmentId",mapValueField = "name")
    @ApiModelProperty(value = "环境名称")
    private String environmentName;

    @ApiModelProperty(value = "描述")
    private String remark;
}
