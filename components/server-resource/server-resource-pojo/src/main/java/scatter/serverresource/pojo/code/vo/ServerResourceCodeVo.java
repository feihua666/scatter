package scatter.serverresource.pojo.code.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 代码响应对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="代码响应对象")
public class ServerResourceCodeVo extends BaseIdVo {

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "是否分组")
    private Boolean isGroup;

    @ApiModelProperty(value = "项目id，代码归属哪个项目")
    private String projectId;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
