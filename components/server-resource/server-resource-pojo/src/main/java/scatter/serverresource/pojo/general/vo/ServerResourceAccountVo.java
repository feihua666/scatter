package scatter.serverresource.pojo.general.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.common.trans.TransConstants;


/**
 * <p>
 * 账号响应对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="账号响应对象")
public class ServerResourceAccountVo extends BaseIdVo {

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "账号密码")
    private String password;

    @ApiModelProperty(value = "数据id")
    private String dataId;

    @ApiModelProperty(value = "数据类型，字典id")
    private String dataTypeDictId;

    @TransBy(type = TransConstants.TRANS_DICT_BY_ID,byFieldName = "dataTypeDictId",mapValueField = "name")
    @ApiModelProperty(value = "数据类型，字典名称")
    private String dataTypeDictName;

    @ApiModelProperty(value = "描述")
    private String remark;

}
