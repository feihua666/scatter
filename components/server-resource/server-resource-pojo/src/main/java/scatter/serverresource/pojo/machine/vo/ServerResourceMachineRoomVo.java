package scatter.serverresource.pojo.machine.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 机房响应对象
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Setter
@Getter
@ApiModel(value="机房响应对象")
public class ServerResourceMachineRoomVo extends BaseIdVo {

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "区域位置")
    private String area;

}
