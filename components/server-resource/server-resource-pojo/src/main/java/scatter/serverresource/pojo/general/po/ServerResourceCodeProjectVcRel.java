package scatter.serverresource.pojo.general.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代码和项目的版本控制关系表
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_server_resource_code_project_vc_rel")
@ApiModel(value="ServerResourceCodeProjectVcRel对象", description="代码和项目的版本控制关系表")
public class ServerResourceCodeProjectVcRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_SERVERRESOURCECODEPROJECTVCREL_BY_ID = "trans_serverresourcecodeprojectvcrel_by_id_scatter.serverresource.pojo.general.po";

    @ApiModelProperty(value = "代码id")
    private String codeId;

    @ApiModelProperty(value = "项目的版本控制id, vc代表版本控制，项目表的版本控制类型的id，如git，svn等")
    private String projectVcId;


}
