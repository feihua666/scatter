package scatter.serverresource.pojo.project.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 项目分页表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="项目分页表单对象")
public class ServerResourceProjectPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "环境id")
    private String environmentId;

    @ApiModelProperty(value = "是否分组")
    private Boolean isGroup;

    @ApiModelProperty(value = "访问链接")
    private String visitLink;

    @ApiModelProperty(value = "账号")
    private String visitLinkAccount;

    @ApiModelProperty(value = "密码")
    private String visitLinkPassword;

    @ApiModelProperty(value = "类型，自研代码，三方中间件，版本控制等")
    private String typeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
