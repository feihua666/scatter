package scatter.serverresource.pojo.general.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
/**
 * <p>
 * 代码分配项目的版本控制表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@PropValid
@Setter
@Getter
@ApiModel(value="代码分配项目的版本控制表单对象")
public class CodeAssignProjectVcForm extends BaseForm {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "代码id不能为空")
    @ApiModelProperty(value = "代码id")
    private String codeId;

    @ApiModelProperty(value = "选择的项目的版本控制id")
    private List<String> checkedProjectVcIds;

    @PropValid.DependCondition(message = "未选择的项目的版本控制id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的项目的版本控制id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedProjectVcIds;
    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
