package scatter.serverresource.pojo.project.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 项目更新表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="项目更新表单对象")
public class ServerResourceProjectUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="项目名称不能为空")
    @ApiModelProperty(value = "项目名称",required = true)
    private String name;

    @ApiModelProperty(value = "环境id")
    private String environmentId;

    @NotNull(message="是否分组不能为空")
    @ApiModelProperty(value = "是否分组",required = true)
    private Boolean isGroup;

    @ApiModelProperty(value = "访问链接")
    private String visitLink;

    @ApiModelProperty(value = "账号")
    private String visitLinkAccount;

    @ApiModelProperty(value = "密码")
    private String visitLinkPassword;

    @ApiModelProperty(value = "类型，自研代码，三方中间件，版本控制等")
    private String typeDictId;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
