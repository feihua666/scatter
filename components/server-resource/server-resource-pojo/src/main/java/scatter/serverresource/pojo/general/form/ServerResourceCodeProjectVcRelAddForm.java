package scatter.serverresource.pojo.general.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 代码和项目的版本控制添加表单对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="代码和项目的版本控制添加表单对象")
public class ServerResourceCodeProjectVcRelAddForm extends BaseAddForm {

    @NotEmpty(message="代码id不能为空")
    @ApiModelProperty(value = "代码id",required = true)
    private String codeId;

    @NotEmpty(message="项目的版本控制id不能为空")
    @ApiModelProperty(value = "项目的版本控制id, vc代表版本控制，项目表的版本控制类型的id，如git，svn等",required = true)
    private String projectVcId;

}
