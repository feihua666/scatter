package scatter.serverresource.pojo.project.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.common.trans.TransConstants;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.pojo.project.po.ServerResourceProject;


/**
 * <p>
 * 项目响应对象
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Setter
@Getter
@ApiModel(value="项目响应对象")
public class ServerResourceProjectVo extends BaseIdVo {

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "环境id")
    private String environmentId;


    @TransBy(type = ServerResourceEnvironment.TRANS_SERVERRESOURCEENVIRONMENT_BY_ID,byFieldName = "environmentId",mapValueField = "name")
    @ApiModelProperty(value = "环境名称")
    private String environmentName;

    @ApiModelProperty(value = "是否分组")
    private Boolean isGroup;

    @ApiModelProperty(value = "访问链接")
    private String visitLink;

    @ApiModelProperty(value = "账号")
    private String visitLinkAccount;

    @ApiModelProperty(value = "密码")
    private String visitLinkPassword;

    @ApiModelProperty(value = "类型，自研代码，三方中间件，版本控制等")
    private String typeDictId;

    @TransBy(type = TransConstants.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "类型，字典名称，自研代码，三方中间件，版本控制等")
    private String typeDictName;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "父级id")
    private String parentId;


    @TransBy(type = ServerResourceProject.TRANS_SERVERRESOURCEPROJECT_BY_ID,byFieldName = "parentId",mapValueField = "name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}
