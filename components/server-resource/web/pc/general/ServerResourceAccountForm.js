import ServerResourceAccountUrl from './ServerResourceAccountUrl.js'
const ServerResourceAccountForm = [
    {
        ServerResourceAccountSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'account',
        },
        element:{
            label: '账号',
            required: true,
        }
    },
    {
        ServerResourceAccountSearchList:false,
        field: {
            name: 'password',
        },
        element:{
            label: '账号密码',
        }
    },
    {
        ServerResourceAccountSearchList: false,

        field: {
            name: 'dataId',
        },
        element:{
            label: '数据id',
            required: true,
        }
    },
    {
        ServerResourceAccountSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'dataTypeDictId',
        },
        element:{
            label: '数据类型',
            type: 'selectDict',
            options: {
                groupCode: 'server_resource_account_data_type'
            },
            required: true,
        }
    },
    {

        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default ServerResourceAccountForm