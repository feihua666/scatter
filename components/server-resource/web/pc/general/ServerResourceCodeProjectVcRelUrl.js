const basePath = '' + '/general/server-resource-code-project-vc-rel'
const ServerResourceCodeProjectVcRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    codeAssignProjectVc: basePath + '/code/assign/projectvc',
    checkedProjectVcIds: basePath + '/code/{codeId}',
    projectVcAssignCode: basePath + '/projectvc/assign/code',
    checkedCodeIds: basePath + '/projectvc/{projectVcId}',
    router: {
        searchList: '/ServerResourceCodeProjectVcRelSearchList',
        add: '/ServerResourceCodeProjectVcRelAdd',
        update: '/ServerResourceCodeProjectVcRelUpdate',
        codeAssignProjectVcRel: '/codeAssignProjectVcRel',
        projectVcAssignCodeRel: '/projectVcAssignCodeRel',
    }
}
export default ServerResourceCodeProjectVcRelUrl