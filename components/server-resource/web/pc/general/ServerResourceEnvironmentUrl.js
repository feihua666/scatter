const basePath = '' + '/general/server-resource-environment'
const ServerResourceEnvironmentUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ServerResourceEnvironmentSearchList',
        add: '/ServerResourceEnvironmentAdd',
        update: '/ServerResourceEnvironmentUpdate',
    }
}
export default ServerResourceEnvironmentUrl