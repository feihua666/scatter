import ServerResourceCodeProjectVcRelUrl from './ServerResourceCodeProjectVcRelUrl.js'
const ServerResourceCodeProjectVcRelForm = [
    {
        ServerResourceCodeProjectVcRelSearchList: {
            element:{
                required: false
            }
        },

        CodeAssignProjectVcRel: false,
        ProjectVcAssignCodeRel: false,
        field: {
            name: 'codeId',
        },
        element:{
            label: '代码id',
            required: true,
        }
    },
    {
        ServerResourceCodeProjectVcRelSearchList: {
            element:{
                required: false
            }
        },

        CodeAssignProjectVcRel: false,
        ProjectVcAssignCodeRel: false,
        field: {
            name: 'projectVcId',
        },
        element:{
            label: '项目的版本控制id',
            required: true,
        }
    },

    {
        ServerResourceCodeProjectVcRelSearchList: false,
        ServerResourceCodeProjectVcRelAdd: false,
        ServerResourceCodeProjectVcRelUpdate: false,
        ProjectVcAssignCodeRel: false,

        field: {
            name: 'codeId',
        },
        element:{
            label: '代码',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo 用于回显，这里可以是一个列表地址，如：RoleUrl.list
            }

        }
    },
    {
        ServerResourceCodeProjectVcRelSearchList: false,
        ServerResourceCodeProjectVcRelAdd: false,
        ServerResourceCodeProjectVcRelUpdate: false,
        ProjectVcAssignCodeRel: false,

        field: {
            name: 'checkedProjectVcIds',
            value: []
        },
        element:{
            label: '项目的版本控制',
            type: 'tree',
            options: {
                datas: [], // todo 用于显示分配的数据，这里可以是一个列表地址，如：RoleUrl.list
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        ServerResourceCodeProjectVcRelSearchList: false,
        ServerResourceCodeProjectVcRelAdd: false,
        ServerResourceCodeProjectVcRelUpdate: false,
        CodeAssignProjectVcRel: false,

        field: {
            name: 'projectVcId',
        },
        element:{
            label: '项目的版本控制',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        ServerResourceCodeProjectVcRelSearchList: false,
        ServerResourceCodeProjectVcRelAdd: false,
        ServerResourceCodeProjectVcRelUpdate: false,
        CodeAssignProjectVcRel: false,

        field: {
            name: 'checkedCodeIds',
            value: []
        },
        element:{
            label: '代码',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default ServerResourceCodeProjectVcRelForm