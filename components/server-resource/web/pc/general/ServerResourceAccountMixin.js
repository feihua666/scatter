import ServerResourceAccountForm from './ServerResourceAccountForm.js'
import ServerResourceAccountTable from './ServerResourceAccountTable.js'
import ServerResourceAccountUrl from './ServerResourceAccountUrl.js'
const ServerResourceAccountMixin = {
    computed: {
        ServerResourceAccountFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceAccountForm,this.$options.name)
        },
        ServerResourceAccountTableOptions() {
            return ServerResourceAccountTable
        },
        ServerResourceAccountUrl(){
            return ServerResourceAccountUrl
        }
    },
}
export default ServerResourceAccountMixin