import ServerResourceAccountUrl from './ServerResourceAccountUrl.js'

const ServerResourceAccountRoute = [
    {
        path: ServerResourceAccountUrl.router.searchList,
        component: () => import('./element/ServerResourceAccountSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceAccountSearchList',
            name: '账号管理'
        }
    },
    {
        path: ServerResourceAccountUrl.router.add,
        component: () => import('./element/ServerResourceAccountAdd'),
        meta: {
            code:'ServerResourceAccountAdd',
            name: '账号添加'
        }
    },
    {
        path: ServerResourceAccountUrl.router.update,
        component: () => import('./element/ServerResourceAccountUpdate'),
        meta: {
            code:'ServerResourceAccountUpdate',
            name: '账号修改'
        }
    },
]
export default ServerResourceAccountRoute