import ServerResourceCodeProjectVcRelForm from './ServerResourceCodeProjectVcRelForm.js'
import ServerResourceCodeProjectVcRelTable from './ServerResourceCodeProjectVcRelTable.js'
import ServerResourceCodeProjectVcRelUrl from './ServerResourceCodeProjectVcRelUrl.js'
const ServerResourceCodeProjectVcRelMixin = {
    computed: {
        ServerResourceCodeProjectVcRelFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceCodeProjectVcRelForm,this.$options.name)
        },
        ServerResourceCodeProjectVcRelTableOptions() {
            return ServerResourceCodeProjectVcRelTable
        },
        ServerResourceCodeProjectVcRelUrl(){
            return ServerResourceCodeProjectVcRelUrl
        }
    },
}
export default ServerResourceCodeProjectVcRelMixin