import ServerResourceEnvironmentUrl from './ServerResourceEnvironmentUrl.js'

const ServerResourceEnvironmentRoute = [
    {
        path: ServerResourceEnvironmentUrl.router.searchList,
        component: () => import('./element/ServerResourceEnvironmentSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceEnvironmentSearchList',
            name: '环境管理'
        }
    },
    {
        path: ServerResourceEnvironmentUrl.router.add,
        component: () => import('./element/ServerResourceEnvironmentAdd'),
        meta: {
            code:'ServerResourceEnvironmentAdd',
            name: '环境添加'
        }
    },
    {
        path: ServerResourceEnvironmentUrl.router.update,
        component: () => import('./element/ServerResourceEnvironmentUpdate'),
        meta: {
            code:'ServerResourceEnvironmentUpdate',
            name: '环境修改'
        }
    },
]
export default ServerResourceEnvironmentRoute