import ServerResourceCodeProjectVcRelUrl from './ServerResourceCodeProjectVcRelUrl.js'

const ServerResourceCodeProjectVcRelRoute = [
    {
        path: ServerResourceCodeProjectVcRelUrl.router.searchList,
        component: () => import('./element/ServerResourceCodeProjectVcRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceCodeProjectVcRelSearchList',
            name: '代码和项目的版本控制管理'
        }
    },
    {
        path: ServerResourceCodeProjectVcRelUrl.router.add,
        component: () => import('./element/ServerResourceCodeProjectVcRelAdd'),
        meta: {
            code:'ServerResourceCodeProjectVcRelAdd',
            name: '代码和项目的版本控制添加'
        }
    },
    {
        path: ServerResourceCodeProjectVcRelUrl.router.update,
        component: () => import('./element/ServerResourceCodeProjectVcRelUpdate'),
        meta: {
            code:'ServerResourceCodeProjectVcRelUpdate',
            name: '代码和项目的版本控制修改'
        }
    },
    {
        path: ServerResourceCodeProjectVcRelUrl.router.codeAssignProjectVcRel,
        component: () => import('./element/CodeAssignProjectVcRel'),
        meta: {
            code:'CodeAssignProjectVcRel',
            name: '代码分配项目的版本控制'
        }
    },
    {
        path: ServerResourceCodeProjectVcRelUrl.router.projectVcAssignCodeRel,
        component: () => import('./element/ProjectVcAssignCodeRel'),
        meta: {
            code:'ProjectVcAssignCodeRel',
            name: '项目的版本控制分配代码'
        }
    },
]
export default ServerResourceCodeProjectVcRelRoute