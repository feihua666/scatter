const ServerResourceAccountTable = [
    {
        prop: 'account',
        label: '账号'
    },
    {
        prop: 'password',
        label: '账号密码'
    },
    {
        prop: 'dataId',
        label: '数据id'
    },
    {
        prop: 'dataTypeDictName',
        label: '数据类型'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ServerResourceAccountTable