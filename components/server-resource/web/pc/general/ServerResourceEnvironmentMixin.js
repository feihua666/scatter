import ServerResourceEnvironmentForm from './ServerResourceEnvironmentForm.js'
import ServerResourceEnvironmentTable from './ServerResourceEnvironmentTable.js'
import ServerResourceEnvironmentUrl from './ServerResourceEnvironmentUrl.js'
const ServerResourceEnvironmentMixin = {
    computed: {
        ServerResourceEnvironmentFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceEnvironmentForm,this.$options.name)
        },
        ServerResourceEnvironmentTableOptions() {
            return ServerResourceEnvironmentTable
        },
        ServerResourceEnvironmentUrl(){
            return ServerResourceEnvironmentUrl
        }
    },
}
export default ServerResourceEnvironmentMixin