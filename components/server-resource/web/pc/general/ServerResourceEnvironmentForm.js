import ServerResourceEnvironmentUrl from './ServerResourceEnvironmentUrl.js'
const ServerResourceEnvironmentForm = [
    {
        ServerResourceEnvironmentSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },

]
export default ServerResourceEnvironmentForm