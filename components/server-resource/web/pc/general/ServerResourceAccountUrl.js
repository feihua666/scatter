const basePath = '' + '/general/server-resource-account'
const ServerResourceAccountUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ServerResourceAccountSearchList',
        add: '/ServerResourceAccountAdd',
        update: '/ServerResourceAccountUpdate',
    }
}
export default ServerResourceAccountUrl