import ServerResourceMachineUrl from './ServerResourceMachineUrl.js'

const ServerResourceMachineRoute = [
    {
        path: ServerResourceMachineUrl.router.searchList,
        component: () => import('./element/ServerResourceMachineSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceMachineSearchList',
            name: '机器管理'
        }
    },
    {
        path: ServerResourceMachineUrl.router.add,
        component: () => import('./element/ServerResourceMachineAdd'),
        meta: {
            code:'ServerResourceMachineAdd',
            name: '机器添加'
        }
    },
    {
        path: ServerResourceMachineUrl.router.update,
        component: () => import('./element/ServerResourceMachineUpdate'),
        meta: {
            code:'ServerResourceMachineUpdate',
            name: '机器修改'
        }
    },
]
export default ServerResourceMachineRoute