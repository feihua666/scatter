import ServerResourceMachineRoomUrl from './ServerResourceMachineRoomUrl.js'
const ServerResourceMachineRoomForm = [
    {
        ServerResourceMachineRoomSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {

        field: {
            name: 'area',
        },
        element:{
            label: '区域位置',
        }
    },

]
export default ServerResourceMachineRoomForm