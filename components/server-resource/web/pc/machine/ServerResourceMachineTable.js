const ServerResourceMachineTable = [
    {
        type: 'expand'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'ipv4',
        label: 'IPV4地址'
    },
    {
        prop: 'ipv6',
        label: 'IPV6地址',
        showInDetail: true,
    },
    {
        prop: 'ipv4Inner',
        label: 'IPV4内网地址'
    },
    {
        prop: 'ipv6Inner',
        label: 'IPV6内网地址',
        showInDetail: true,
    },
    {
        prop: 'typeDictName',
        label: '机器类型'
    },
    {
        prop: 'machineRoomName',
        label: '所在机房'
    },
    {
        prop: 'cpuNum',
        label: 'cpu核数'
    },
    {
        prop: 'memorySize',
        label: '内存大小(GB)'
    },
    {
        prop: 'diskSize',
        label: '硬盘大小(GB)'
    },
    {
        prop: 'isDiskSsd',
        label: '硬盘是否ssd',
        showInDetail: true,
    },
    {
        prop: 'rootAccount',
        label: 'root账号',
        showInDetail: true,
    },
    {
        prop: 'rootPassword',
        label: 'root密码',
        showInDetail: true,
    },
    {
        prop: 'environmentName',
        label: '环境'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default ServerResourceMachineTable