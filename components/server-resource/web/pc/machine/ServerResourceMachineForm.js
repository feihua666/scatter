import ServerResourceMachineUrl from './ServerResourceMachineUrl.js'
import ServerResourceMachineRoomUrl from './ServerResourceMachineRoomUrl.js'
import ServerResourceEnvironmentUrl from '../general/ServerResourceEnvironmentUrl.js'
const ServerResourceMachineForm = [
    {
        ServerResourceMachineSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {
        ServerResourceMachineSearchList: false,
        field: {
            name: 'ipv4',
        },
        element:{
            label: 'IPV4地址',
        }
    },
    {
        ServerResourceMachineSearchList: false,
        field: {
            name: 'ipv6',
        },
        element:{
            label: 'IPV6地址',
        }
    },
    {
        ServerResourceMachineSearchList: false,
        field: {
            name: 'ipv4Inner',
        },
        element:{
            label: 'IPV4内网地址',
        }
    },
    {
        ServerResourceMachineSearchList: false,
        field: {
            name: 'ipv6Inner',
        },
        element:{
            label: 'IPV6内网地址',
        }
    },
    {
        ServerResourceMachineSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '机器类型',
            type: 'selectDict',
            options: {
                groupCode: 'server_resource_type'
            },
            required: true,
        }
    },
    {
        ServerResourceMachineSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'machineRoomId',
        },
        element:{
            label: '所在机房',
            type: 'select',
            options: {
                datas: ServerResourceMachineRoomUrl.list
            },
            required: true,
        }
    },
    {
        ServerResourceMachineSearchList: false,

        field: {
            name: 'cpuNum',
            value: 4,
        },
        element:{
            type: 'inputNumber',
            label: 'cpu核数',
            required: true,
        }
    },
    {
        ServerResourceMachineSearchList: false,

        field: {
            name: 'memorySize',
            value: 8,
        },
        element:{
            type: 'inputNumber',
            label: '内存大小(GB)',
            required: true,
        }
    },
    {
        ServerResourceMachineSearchList: false,

        field: {
            name: 'diskSize',
            value: 500,
        },
        element:{
            type: 'inputNumber',
            label: '硬盘大小(GB)',
            required: true,
        }
    },
    {
        ServerResourceMachineSearchList: false,

        field: {
            name: 'isDiskSsd',
            value: false,
        },
        element:{
            type: 'switch',
            label: '硬盘是否ssd',
            required: true,
        }
    },
    {

        field: {
            name: 'rootAccount',
        },
        element:{
            label: 'root账号',
        }
    },
    {
        ServerResourceMachineSearchList: false,
        field: {
            name: 'rootPassword',
        },
        element:{
            label: 'root密码',
        }
    },
    {

        field: {
            name: 'environmentId',
        },
        element:{
            label: '环境id',
            type: 'select',
            options: {
                datas: ServerResourceEnvironmentUrl.list
            }
        }
    },
    {
        ServerResourceMachineSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
]
export default ServerResourceMachineForm