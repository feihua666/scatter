const basePath = '' + '/machine/server-resource-machine'
const ServerResourceMachineUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ServerResourceMachineSearchList',
        add: '/ServerResourceMachineAdd',
        update: '/ServerResourceMachineUpdate',
    }
}
export default ServerResourceMachineUrl