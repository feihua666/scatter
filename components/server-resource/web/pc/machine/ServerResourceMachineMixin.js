import ServerResourceMachineForm from './ServerResourceMachineForm.js'
import ServerResourceMachineTable from './ServerResourceMachineTable.js'
import ServerResourceMachineUrl from './ServerResourceMachineUrl.js'
const ServerResourceMachineMixin = {
    computed: {
        ServerResourceMachineFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceMachineForm,this.$options.name)
        },
        ServerResourceMachineTableOptions() {
            return ServerResourceMachineTable
        },
        ServerResourceMachineUrl(){
            return ServerResourceMachineUrl
        }
    },
}
export default ServerResourceMachineMixin