import ServerResourceMachineRoomForm from './ServerResourceMachineRoomForm.js'
import ServerResourceMachineRoomTable from './ServerResourceMachineRoomTable.js'
import ServerResourceMachineRoomUrl from './ServerResourceMachineRoomUrl.js'
const ServerResourceMachineRoomMixin = {
    computed: {
        ServerResourceMachineRoomFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceMachineRoomForm,this.$options.name)
        },
        ServerResourceMachineRoomTableOptions() {
            return ServerResourceMachineRoomTable
        },
        ServerResourceMachineRoomUrl(){
            return ServerResourceMachineRoomUrl
        }
    },
}
export default ServerResourceMachineRoomMixin