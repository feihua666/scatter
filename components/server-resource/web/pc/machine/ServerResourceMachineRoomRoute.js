import ServerResourceMachineRoomUrl from './ServerResourceMachineRoomUrl.js'

const ServerResourceMachineRoomRoute = [
    {
        path: ServerResourceMachineRoomUrl.router.searchList,
        component: () => import('./element/ServerResourceMachineRoomSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceMachineRoomSearchList',
            name: '机房管理'
        }
    },
    {
        path: ServerResourceMachineRoomUrl.router.add,
        component: () => import('./element/ServerResourceMachineRoomAdd'),
        meta: {
            code:'ServerResourceMachineRoomAdd',
            name: '机房添加'
        }
    },
    {
        path: ServerResourceMachineRoomUrl.router.update,
        component: () => import('./element/ServerResourceMachineRoomUpdate'),
        meta: {
            code:'ServerResourceMachineRoomUpdate',
            name: '机房修改'
        }
    },
]
export default ServerResourceMachineRoomRoute