const basePath = '' + '/machine/server-resource-machine-room'
const ServerResourceMachineRoomUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ServerResourceMachineRoomSearchList',
        add: '/ServerResourceMachineRoomAdd',
        update: '/ServerResourceMachineRoomUpdate',
    }
}
export default ServerResourceMachineRoomUrl