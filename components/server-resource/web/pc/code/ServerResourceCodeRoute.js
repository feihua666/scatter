import ServerResourceCodeUrl from './ServerResourceCodeUrl.js'

const ServerResourceCodeRoute = [
    {
        path: ServerResourceCodeUrl.router.searchList,
        component: () => import('./element/ServerResourceCodeSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceCodeSearchList',
            name: '代码管理'
        }
    },
    {
        path: ServerResourceCodeUrl.router.add,
        component: () => import('./element/ServerResourceCodeAdd'),
        meta: {
            code:'ServerResourceCodeAdd',
            name: '代码添加'
        }
    },
    {
        path: ServerResourceCodeUrl.router.update,
        component: () => import('./element/ServerResourceCodeUpdate'),
        meta: {
            code:'ServerResourceCodeUpdate',
            name: '代码修改'
        }
    },
]
export default ServerResourceCodeRoute