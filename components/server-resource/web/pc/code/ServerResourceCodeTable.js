const ServerResourceCodeTable = [
    {
        prop: 'name',
        label: '项目名称'
    },
    {
        prop: 'isGroup',
        label: '是否分组'
    },
    {
        prop: 'projectId',
        label: '项目id'
    },
    {
        prop: 'remark',
        label: '描述'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default ServerResourceCodeTable