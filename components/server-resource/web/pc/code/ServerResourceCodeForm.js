import ServerResourceCodeUrl from './ServerResourceCodeUrl.js'
const ServerResourceCodeForm = [
    {
        ServerResourceCodeSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '项目名称',
            required: true,
        }
    },
    {
        ServerResourceCodeSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'isGroup',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否分组',
            required: true,
        }
    },
    {
        ServerResourceCodeSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'projectId',
        },
        element:{
            label: '项目id',
            required: true,
        }
    },
    {

        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: ServerResourceCodeUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },

]
export default ServerResourceCodeForm