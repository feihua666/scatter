const basePath = '' + '/code/server-resource-code'
const ServerResourceCodeUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ServerResourceCodeSearchList',
        add: '/ServerResourceCodeAdd',
        update: '/ServerResourceCodeUpdate',
    }
}
export default ServerResourceCodeUrl