import ServerResourceCodeForm from './ServerResourceCodeForm.js'
import ServerResourceCodeTable from './ServerResourceCodeTable.js'
import ServerResourceCodeUrl from './ServerResourceCodeUrl.js'
const ServerResourceCodeMixin = {
    computed: {
        ServerResourceCodeFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceCodeForm,this.$options.name)
        },
        ServerResourceCodeTableOptions() {
            return ServerResourceCodeTable
        },
        ServerResourceCodeUrl(){
            return ServerResourceCodeUrl
        }
    },
}
export default ServerResourceCodeMixin