import ServerResourceProjectUrl from './ServerResourceProjectUrl.js'

const ServerResourceProjectRoute = [
    {
        path: ServerResourceProjectUrl.router.searchList,
        component: () => import('./element/ServerResourceProjectSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ServerResourceProjectSearchList',
            name: '项目管理'
        }
    },
    {
        path: ServerResourceProjectUrl.router.add,
        component: () => import('./element/ServerResourceProjectAdd'),
        meta: {
            code:'ServerResourceProjectAdd',
            name: '项目添加'
        }
    },
    {
        path: ServerResourceProjectUrl.router.update,
        component: () => import('./element/ServerResourceProjectUpdate'),
        meta: {
            code:'ServerResourceProjectUpdate',
            name: '项目修改'
        }
    },
]
export default ServerResourceProjectRoute