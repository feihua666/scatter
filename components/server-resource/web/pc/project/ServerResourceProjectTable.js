const ServerResourceProjectTable = [
    {
        prop: 'name',
        label: '项目名称'
    },
    {
        prop: 'environmentName',
        label: '环境'
    },
    {
        prop: 'isGroup',
        label: '是否分组'
    },
    {
        prop: 'visitLink',
        label: '访问链接',
        stype: 'link'
    },
    {
        prop: 'visitLinkAccount',
        label: '账号'
    },
    {
        prop: 'visitLinkPassword',
        label: '密码'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'remark',
        label: '描述'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
]
export default ServerResourceProjectTable