const basePath = '' + '/project/server-resource-project'
const ServerResourceProjectUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ServerResourceProjectSearchList',
        add: '/ServerResourceProjectAdd',
        update: '/ServerResourceProjectUpdate',
    }
}
export default ServerResourceProjectUrl