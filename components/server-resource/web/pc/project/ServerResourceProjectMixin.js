import ServerResourceProjectForm from './ServerResourceProjectForm.js'
import ServerResourceProjectTable from './ServerResourceProjectTable.js'
import ServerResourceProjectUrl from './ServerResourceProjectUrl.js'
const ServerResourceProjectMixin = {
    computed: {
        ServerResourceProjectFormOptions() {
            return this.$stDynamicFormTools.options(ServerResourceProjectForm,this.$options.name)
        },
        ServerResourceProjectTableOptions() {
            return ServerResourceProjectTable
        },
        ServerResourceProjectUrl(){
            return ServerResourceProjectUrl
        }
    },
}
export default ServerResourceProjectMixin