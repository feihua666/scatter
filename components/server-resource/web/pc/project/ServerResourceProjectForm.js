import ServerResourceProjectUrl from './ServerResourceProjectUrl.js'
import ServerResourceEnvironmentUrl from '../general/ServerResourceEnvironmentUrl.js'
const ServerResourceProjectForm = [
    {
        ServerResourceProjectSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '项目名称',
            required: true,
        }
    },
    {

        field: {
            name: 'environmentId',
        },
        element:{
            label: '环境',
            type: 'select',
            options: {
                datas: ServerResourceEnvironmentUrl.list
            }
        }
    },
    {
        ServerResourceProjectSearchList: false,

        field: {
            name: 'isGroup',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否分组',
            required: true,
        }
    },
    {
        ServerResourceProjectSearchList: false,
        field: {
            name: 'visitLink',
        },
        element:{
            label: '访问链接',
            options: {
                placeholder: 'http://'
            },
        }
    },
    {
        ServerResourceProjectSearchList: false,
        field: {
            name: 'visitLinkAccount',
        },
        element:{
            label: '账号',
        }
    },
    {
        ServerResourceProjectSearchList: false,
        field: {
            name: 'visitLinkPassword',
        },
        element:{
            label: '密码',
        }
    },
    {
        ServerResourceProjectSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            type: 'selectDict',
            required: true,
            options: {
                groupCode: 'server_resource_project_type'
            }
        }
    },
    {
        ServerResourceProjectSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },
    {
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: ServerResourceProjectUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },

]
export default ServerResourceProjectForm