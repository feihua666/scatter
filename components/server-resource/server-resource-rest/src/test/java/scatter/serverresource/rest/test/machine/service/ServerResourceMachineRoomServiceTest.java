package scatter.serverresource.rest.test.machine.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomPageQueryForm;
import scatter.serverresource.rest.test.machine.ServerResourceMachineRoomSuperTest;
import scatter.serverresource.rest.machine.service.IServerResourceMachineRoomService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 机房 服务测试类
* </p>
*
* @author yw
* @since 2022-01-28
*/
@SpringBootTest
public class ServerResourceMachineRoomServiceTest extends ServerResourceMachineRoomSuperTest{

    @Autowired
    private IServerResourceMachineRoomService serverResourceMachineRoomService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ServerResourceMachineRoom> pos = serverResourceMachineRoomService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}