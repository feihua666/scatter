package scatter.serverresource.rest.test.code;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.pojo.code.form.ServerResourceCodeAddForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodeUpdateForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodePageQueryForm;
import scatter.serverresource.rest.code.service.IServerResourceCodeService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 代码 测试类基类
* </p>
*
* @author yw
* @since 2022-02-07
*/
public class ServerResourceCodeSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceCodeService serverResourceCodeService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceCodeService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceCodeService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceCode mockPo() {
        return JMockData.mock(ServerResourceCode.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceCodeAddForm mockAddForm() {
        return JMockData.mock(ServerResourceCodeAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceCodeUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceCodeUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceCodePageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceCodePageQueryForm.class, mockConfig);
    }
}