package scatter.serverresource.rest.test.general.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.rest.test.general.ServerResourceAccountSuperTest;
/**
* <p>
* 账号 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2022-01-28
*/
@SpringBootTest
public class ServerResourceAccountInnerControllerTest extends ServerResourceAccountSuperTest{
    @Test
    void contextLoads() {
    }
}