package scatter.serverresource.rest.test.general.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.serverresource.pojo.general.form.ServerResourceAccountAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountPageQueryForm;
import scatter.serverresource.rest.test.general.ServerResourceAccountSuperTest;
import scatter.serverresource.rest.general.service.IServerResourceAccountService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 账号 服务测试类
* </p>
*
* @author yw
* @since 2022-01-28
*/
@SpringBootTest
public class ServerResourceAccountServiceTest extends ServerResourceAccountSuperTest{

    @Autowired
    private IServerResourceAccountService serverResourceAccountService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ServerResourceAccount> pos = serverResourceAccountService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}