package scatter.serverresource.rest.test.project;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectUpdateForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectPageQueryForm;
import scatter.serverresource.rest.project.service.IServerResourceProjectService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 项目 测试类基类
* </p>
*
* @author yw
* @since 2022-02-07
*/
public class ServerResourceProjectSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceProjectService serverResourceProjectService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceProjectService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceProjectService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceProject mockPo() {
        return JMockData.mock(ServerResourceProject.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceProjectAddForm mockAddForm() {
        return JMockData.mock(ServerResourceProjectAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceProjectUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceProjectUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceProjectPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceProjectPageQueryForm.class, mockConfig);
    }
}