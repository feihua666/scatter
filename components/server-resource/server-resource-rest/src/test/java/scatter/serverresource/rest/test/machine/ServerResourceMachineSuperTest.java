package scatter.serverresource.rest.test.machine;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.machine.po.ServerResourceMachine;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachinePageQueryForm;
import scatter.serverresource.rest.machine.service.IServerResourceMachineService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 机器 测试类基类
* </p>
*
* @author yw
* @since 2022-01-28
*/
public class ServerResourceMachineSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceMachineService serverResourceMachineService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceMachineService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceMachineService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceMachine mockPo() {
        return JMockData.mock(ServerResourceMachine.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceMachineAddForm mockAddForm() {
        return JMockData.mock(ServerResourceMachineAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceMachineUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceMachineUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceMachinePageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceMachinePageQueryForm.class, mockConfig);
    }
}