package scatter.serverresource.rest.test.code.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.pojo.code.form.ServerResourceCodeAddForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodeUpdateForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodePageQueryForm;
import scatter.serverresource.rest.test.code.ServerResourceCodeSuperTest;
import scatter.serverresource.rest.code.service.IServerResourceCodeService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 代码 服务测试类
* </p>
*
* @author yw
* @since 2022-02-07
*/
@SpringBootTest
public class ServerResourceCodeServiceTest extends ServerResourceCodeSuperTest{

    @Autowired
    private IServerResourceCodeService serverResourceCodeService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ServerResourceCode> pos = serverResourceCodeService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}