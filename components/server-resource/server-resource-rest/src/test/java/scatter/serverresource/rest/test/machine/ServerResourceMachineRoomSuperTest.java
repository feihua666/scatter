package scatter.serverresource.rest.test.machine;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomPageQueryForm;
import scatter.serverresource.rest.machine.service.IServerResourceMachineRoomService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 机房 测试类基类
* </p>
*
* @author yw
* @since 2022-01-28
*/
public class ServerResourceMachineRoomSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceMachineRoomService serverResourceMachineRoomService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceMachineRoomService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceMachineRoomService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceMachineRoom mockPo() {
        return JMockData.mock(ServerResourceMachineRoom.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceMachineRoomAddForm mockAddForm() {
        return JMockData.mock(ServerResourceMachineRoomAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceMachineRoomUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceMachineRoomUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceMachineRoomPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceMachineRoomPageQueryForm.class, mockConfig);
    }
}