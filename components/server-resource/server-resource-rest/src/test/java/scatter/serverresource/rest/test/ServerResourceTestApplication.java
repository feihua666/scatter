package scatter.serverresource.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.serverresource.rest.ServerResourceConfiguration;

/**
 * Created by yangwei
 * Created at 2020/11/11 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, ServerResourceConfiguration.class})
@MapperScan("scatter.serverresource.rest.mapper")
public class ServerResourceTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerResourceTestApplication.class, args);
    }
}
