package scatter.serverresource.rest.test.general;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.serverresource.pojo.general.form.ServerResourceAccountAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountPageQueryForm;
import scatter.serverresource.rest.general.service.IServerResourceAccountService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 账号 测试类基类
* </p>
*
* @author yw
* @since 2022-01-28
*/
public class ServerResourceAccountSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceAccountService serverResourceAccountService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceAccountService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceAccountService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceAccount mockPo() {
        return JMockData.mock(ServerResourceAccount.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceAccountAddForm mockAddForm() {
        return JMockData.mock(ServerResourceAccountAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceAccountUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceAccountUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceAccountPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceAccountPageQueryForm.class, mockConfig);
    }
}