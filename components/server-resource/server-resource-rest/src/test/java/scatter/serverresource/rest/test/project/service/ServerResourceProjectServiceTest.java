package scatter.serverresource.rest.test.project.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectUpdateForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectPageQueryForm;
import scatter.serverresource.rest.test.project.ServerResourceProjectSuperTest;
import scatter.serverresource.rest.project.service.IServerResourceProjectService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 项目 服务测试类
* </p>
*
* @author yw
* @since 2022-02-07
*/
@SpringBootTest
public class ServerResourceProjectServiceTest extends ServerResourceProjectSuperTest{

    @Autowired
    private IServerResourceProjectService serverResourceProjectService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ServerResourceProject> pos = serverResourceProjectService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}