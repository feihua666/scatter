package scatter.serverresource.rest.test.general.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentPageQueryForm;
import scatter.serverresource.rest.test.general.ServerResourceEnvironmentSuperTest;
import scatter.serverresource.rest.general.service.IServerResourceEnvironmentService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 环境 服务测试类
* </p>
*
* @author yw
* @since 2022-01-28
*/
@SpringBootTest
public class ServerResourceEnvironmentServiceTest extends ServerResourceEnvironmentSuperTest{

    @Autowired
    private IServerResourceEnvironmentService serverResourceEnvironmentService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ServerResourceEnvironment> pos = serverResourceEnvironmentService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}