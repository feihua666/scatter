package scatter.serverresource.rest.test.project.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.rest.test.project.ServerResourceProjectSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.vo.ServerResourceProjectVo;
import scatter.serverresource.rest.ServerResourceConfiguration;
/**
* <p>
* 项目 前端控制器测试类
* </p>
*
* @author yw
* @since 2022-02-07
*/
@SpringBootTest
@AutoConfigureMockMvc
public class ServerResourceProjectControllerTest extends ServerResourceProjectSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/project/server-resource-project";

        // 请求表单
        ServerResourceProjectAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        ServerResourceProjectVo vo = JSONUtil.toBean(response.getContentAsString(), ServerResourceProjectVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}