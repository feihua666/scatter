package scatter.serverresource.rest.test.general;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentPageQueryForm;
import scatter.serverresource.rest.general.service.IServerResourceEnvironmentService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 环境 测试类基类
* </p>
*
* @author yw
* @since 2022-01-28
*/
public class ServerResourceEnvironmentSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceEnvironmentService serverResourceEnvironmentService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceEnvironmentService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceEnvironmentService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceEnvironment mockPo() {
        return JMockData.mock(ServerResourceEnvironment.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceEnvironmentAddForm mockAddForm() {
        return JMockData.mock(ServerResourceEnvironmentAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceEnvironmentUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceEnvironmentUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceEnvironmentPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceEnvironmentPageQueryForm.class, mockConfig);
    }
}