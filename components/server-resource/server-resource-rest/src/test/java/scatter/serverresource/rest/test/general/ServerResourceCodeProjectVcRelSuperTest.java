package scatter.serverresource.rest.test.general;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelPageQueryForm;
import scatter.serverresource.rest.general.service.IServerResourceCodeProjectVcRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 代码和项目的版本控制 测试类基类
* </p>
*
* @author yw
* @since 2022-02-07
*/
public class ServerResourceCodeProjectVcRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IServerResourceCodeProjectVcRelService serverResourceCodeProjectVcRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return serverResourceCodeProjectVcRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return serverResourceCodeProjectVcRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ServerResourceCodeProjectVcRel mockPo() {
        return JMockData.mock(ServerResourceCodeProjectVcRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ServerResourceCodeProjectVcRelAddForm mockAddForm() {
        return JMockData.mock(ServerResourceCodeProjectVcRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ServerResourceCodeProjectVcRelUpdateForm mockUpdateForm() {
        return JMockData.mock(ServerResourceCodeProjectVcRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ServerResourceCodeProjectVcRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ServerResourceCodeProjectVcRelPageQueryForm.class, mockConfig);
    }
}