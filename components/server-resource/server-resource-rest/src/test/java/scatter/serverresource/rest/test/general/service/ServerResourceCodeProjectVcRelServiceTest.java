package scatter.serverresource.rest.test.general.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelPageQueryForm;
import scatter.serverresource.rest.test.general.ServerResourceCodeProjectVcRelSuperTest;
import scatter.serverresource.rest.general.service.IServerResourceCodeProjectVcRelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 代码和项目的版本控制 服务测试类
* </p>
*
* @author yw
* @since 2022-02-07
*/
@SpringBootTest
public class ServerResourceCodeProjectVcRelServiceTest extends ServerResourceCodeProjectVcRelSuperTest{

    @Autowired
    private IServerResourceCodeProjectVcRelService serverResourceCodeProjectVcRelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ServerResourceCodeProjectVcRel> pos = serverResourceCodeProjectVcRelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}