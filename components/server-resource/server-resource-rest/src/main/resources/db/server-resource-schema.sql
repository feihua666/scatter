-- 通过自定义sql脚本可以支持形如：-- import 开头注释的依赖导入参考 scatter.common.sqlinit.CustomDataSourceScriptDatabaseInitializer 实现
-- import classpath:db/schema.component_server_resource_environment.sql
-- import classpath:db/schema.component_server_resource_account.sql
-- import classpath:db/schema.component_server_resource_code.sql
-- import classpath:db/schema.component_server_resource_code_project_vc_rel.sql
-- import classpath:db/schema.component_server_resource_machine.sql
-- import classpath:db/schema.component_server_resource_machine_room.sql
-- import classpath:db/schema.component_server_resource_project.sql