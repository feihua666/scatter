DROP TABLE IF EXISTS component_server_resource_code_project_vc_rel;
CREATE TABLE `component_server_resource_code_project_vc_rel` (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `code_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '代码id',
  `project_vc_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '项目的版本控制id, vc代表版本控制，项目表的版本控制类型的id，如git，svn等',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `code_id` (`code_id`),
  KEY `project_vc_id` (`project_vc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='代码和项目的版本控制关系表';
