package scatter.serverresource.rest.general.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.serverresource.pojo.general.vo.ServerResourceAccountVo;
import scatter.serverresource.pojo.general.form.ServerResourceAccountAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountPageQueryForm;
import scatter.serverresource.rest.general.service.IServerResourceAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 账号表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Api(tags = "账号相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/general/server-resource-account")
public class ServerResourceAccountController extends BaseAddUpdateQueryFormController<ServerResourceAccount, ServerResourceAccountVo, ServerResourceAccountAddForm, ServerResourceAccountUpdateForm, ServerResourceAccountPageQueryForm> {
    @Autowired
    private IServerResourceAccountService iServerResourceAccountService;

    @ApiOperation("添加账号")
    @PreAuthorize("hasAuthority('ServerResourceAccount:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceAccountVo add(@RequestBody @Valid ServerResourceAccountAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询账号")
    @PreAuthorize("hasAuthority('ServerResourceAccount:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceAccountVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除账号")
    @PreAuthorize("hasAuthority('ServerResourceAccount:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新账号")
    @PreAuthorize("hasAuthority('ServerResourceAccount:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceAccountVo update(@RequestBody @Valid ServerResourceAccountUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询账号")
    @PreAuthorize("hasAuthority('ServerResourceAccount:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceAccountVo> getList(ServerResourceAccountPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询账号")
    @PreAuthorize("hasAuthority('ServerResourceAccount:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceAccountVo> getPage(ServerResourceAccountPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
