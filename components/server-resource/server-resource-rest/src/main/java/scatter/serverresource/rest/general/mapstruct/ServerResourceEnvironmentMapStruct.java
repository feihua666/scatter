package scatter.serverresource.rest.general.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.pojo.general.vo.ServerResourceEnvironmentVo;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentPageQueryForm;

/**
 * <p>
 * 环境 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceEnvironmentMapStruct extends IBaseVoMapStruct<ServerResourceEnvironment, ServerResourceEnvironmentVo>,
                                  IBaseAddFormMapStruct<ServerResourceEnvironment,ServerResourceEnvironmentAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceEnvironment,ServerResourceEnvironmentUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceEnvironment,ServerResourceEnvironmentPageQueryForm>{
    ServerResourceEnvironmentMapStruct INSTANCE = Mappers.getMapper( ServerResourceEnvironmentMapStruct.class );

}
