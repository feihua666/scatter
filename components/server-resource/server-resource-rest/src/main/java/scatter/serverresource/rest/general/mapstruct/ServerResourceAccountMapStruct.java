package scatter.serverresource.rest.general.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.serverresource.pojo.general.vo.ServerResourceAccountVo;
import scatter.serverresource.pojo.general.form.ServerResourceAccountAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountPageQueryForm;

/**
 * <p>
 * 账号 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceAccountMapStruct extends IBaseVoMapStruct<ServerResourceAccount, ServerResourceAccountVo>,
                                  IBaseAddFormMapStruct<ServerResourceAccount,ServerResourceAccountAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceAccount,ServerResourceAccountUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceAccount,ServerResourceAccountPageQueryForm>{
    ServerResourceAccountMapStruct INSTANCE = Mappers.getMapper( ServerResourceAccountMapStruct.class );

}
