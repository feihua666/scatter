package scatter.serverresource.rest.general.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.pojo.general.vo.ServerResourceEnvironmentVo;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentPageQueryForm;
import scatter.serverresource.rest.general.service.IServerResourceEnvironmentService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 环境表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Api(tags = "环境相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/general/server-resource-environment")
public class ServerResourceEnvironmentController extends BaseAddUpdateQueryFormController<ServerResourceEnvironment, ServerResourceEnvironmentVo, ServerResourceEnvironmentAddForm, ServerResourceEnvironmentUpdateForm, ServerResourceEnvironmentPageQueryForm> {
    @Autowired
    private IServerResourceEnvironmentService iServerResourceEnvironmentService;

    @ApiOperation("添加环境")
    @PreAuthorize("hasAuthority('ServerResourceEnvironment:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceEnvironmentVo add(@RequestBody @Valid ServerResourceEnvironmentAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询环境")
    @PreAuthorize("hasAuthority('ServerResourceEnvironment:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceEnvironmentVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除环境")
    @PreAuthorize("hasAuthority('ServerResourceEnvironment:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新环境")
    @PreAuthorize("hasAuthority('ServerResourceEnvironment:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceEnvironmentVo update(@RequestBody @Valid ServerResourceEnvironmentUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询环境")
    @PreAuthorize("hasAuthority('ServerResourceEnvironment:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceEnvironmentVo> getList(ServerResourceEnvironmentPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询环境")
    @PreAuthorize("hasAuthority('ServerResourceEnvironment:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceEnvironmentVo> getPage(ServerResourceEnvironmentPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
