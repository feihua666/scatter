package scatter.serverresource.rest.code.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.pojo.code.vo.ServerResourceCodeVo;
import scatter.serverresource.pojo.code.form.ServerResourceCodeAddForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodeUpdateForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodePageQueryForm;

/**
 * <p>
 * 代码 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceCodeMapStruct extends IBaseVoMapStruct<ServerResourceCode, ServerResourceCodeVo>,
                                  IBaseAddFormMapStruct<ServerResourceCode,ServerResourceCodeAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceCode,ServerResourceCodeUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceCode,ServerResourceCodePageQueryForm>{
    ServerResourceCodeMapStruct INSTANCE = Mappers.getMapper( ServerResourceCodeMapStruct.class );

}
