package scatter.serverresource.rest.general.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.rest.general.service.IServerResourceCodeProjectVcRelService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 代码和项目的版本控制翻译实现
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Component
public class ServerResourceCodeProjectVcRelTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IServerResourceCodeProjectVcRelService serverResourceCodeProjectVcRelService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ServerResourceCodeProjectVcRel.TRANS_SERVERRESOURCECODEPROJECTVCREL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ServerResourceCodeProjectVcRel.TRANS_SERVERRESOURCECODEPROJECTVCREL_BY_ID)) {
            ServerResourceCodeProjectVcRel byId = serverResourceCodeProjectVcRelService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ServerResourceCodeProjectVcRel.TRANS_SERVERRESOURCECODEPROJECTVCREL_BY_ID)) {
            return serverResourceCodeProjectVcRelService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
