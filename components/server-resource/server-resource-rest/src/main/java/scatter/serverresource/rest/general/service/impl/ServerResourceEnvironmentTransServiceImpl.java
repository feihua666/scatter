package scatter.serverresource.rest.general.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.rest.general.service.IServerResourceEnvironmentService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 环境翻译实现
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Component
public class ServerResourceEnvironmentTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IServerResourceEnvironmentService serverResourceEnvironmentService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ServerResourceEnvironment.TRANS_SERVERRESOURCEENVIRONMENT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ServerResourceEnvironment.TRANS_SERVERRESOURCEENVIRONMENT_BY_ID)) {
            ServerResourceEnvironment byId = serverResourceEnvironmentService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ServerResourceEnvironment.TRANS_SERVERRESOURCEENVIRONMENT_BY_ID)) {
            return serverResourceEnvironmentService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
