package scatter.serverresource.rest.machine.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.machine.po.ServerResourceMachine;
import scatter.serverresource.pojo.machine.vo.ServerResourceMachineVo;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachinePageQueryForm;
import scatter.serverresource.rest.machine.service.IServerResourceMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 机器表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Api(tags = "机器相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/machine/server-resource-machine")
public class ServerResourceMachineController extends BaseAddUpdateQueryFormController<ServerResourceMachine, ServerResourceMachineVo, ServerResourceMachineAddForm, ServerResourceMachineUpdateForm, ServerResourceMachinePageQueryForm> {
    @Autowired
    private IServerResourceMachineService iServerResourceMachineService;

    @ApiOperation("添加机器")
    @PreAuthorize("hasAuthority('ServerResourceMachine:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceMachineVo add(@RequestBody @Valid ServerResourceMachineAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询机器")
    @PreAuthorize("hasAuthority('ServerResourceMachine:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceMachineVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除机器")
    @PreAuthorize("hasAuthority('ServerResourceMachine:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新机器")
    @PreAuthorize("hasAuthority('ServerResourceMachine:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceMachineVo update(@RequestBody @Valid ServerResourceMachineUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询机器")
    @PreAuthorize("hasAuthority('ServerResourceMachine:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceMachineVo> getList(ServerResourceMachinePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询机器")
    @PreAuthorize("hasAuthority('ServerResourceMachine:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceMachineVo> getPage(ServerResourceMachinePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
