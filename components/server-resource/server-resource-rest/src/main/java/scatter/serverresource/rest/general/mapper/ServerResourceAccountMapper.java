package scatter.serverresource.rest.general.mapper;

import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 账号表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface ServerResourceAccountMapper extends IBaseMapper<ServerResourceAccount> {

}
