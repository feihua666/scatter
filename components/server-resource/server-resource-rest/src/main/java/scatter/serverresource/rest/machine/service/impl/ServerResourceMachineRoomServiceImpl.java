package scatter.serverresource.rest.machine.service.impl;

import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.rest.machine.mapper.ServerResourceMachineRoomMapper;
import scatter.serverresource.rest.machine.service.IServerResourceMachineRoomService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 机房表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Service
@Transactional
public class ServerResourceMachineRoomServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceMachineRoomMapper, ServerResourceMachineRoom, ServerResourceMachineRoomAddForm, ServerResourceMachineRoomUpdateForm, ServerResourceMachineRoomPageQueryForm> implements IServerResourceMachineRoomService {
    @Override
    public void preAdd(ServerResourceMachineRoomAddForm addForm,ServerResourceMachineRoom po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceMachineRoomUpdateForm updateForm,ServerResourceMachineRoom po) {
        super.preUpdate(updateForm,po);

    }
}
