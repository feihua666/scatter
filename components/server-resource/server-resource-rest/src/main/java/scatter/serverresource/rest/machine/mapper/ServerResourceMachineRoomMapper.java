package scatter.serverresource.rest.machine.mapper;

import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 机房表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface ServerResourceMachineRoomMapper extends IBaseMapper<ServerResourceMachineRoom> {

}
