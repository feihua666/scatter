package scatter.serverresource.rest.general.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.serverresource.rest.ServerResourceConfiguration;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.rest.general.service.IServerResourceCodeProjectVcRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 代码和项目的版本控制关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/inner/general/server-resource-code-project-vc-rel")
public class ServerResourceCodeProjectVcRelInnerController extends BaseInnerController<ServerResourceCodeProjectVcRel> {
 @Autowired
 private IServerResourceCodeProjectVcRelService serverResourceCodeProjectVcRelService;

 public IServerResourceCodeProjectVcRelService getService(){
     return serverResourceCodeProjectVcRelService;
 }
}
