package scatter.serverresource.rest.machine.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.pojo.machine.vo.ServerResourceMachineRoomVo;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomPageQueryForm;

/**
 * <p>
 * 机房 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceMachineRoomMapStruct extends IBaseVoMapStruct<ServerResourceMachineRoom, ServerResourceMachineRoomVo>,
                                  IBaseAddFormMapStruct<ServerResourceMachineRoom,ServerResourceMachineRoomAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceMachineRoom,ServerResourceMachineRoomUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceMachineRoom,ServerResourceMachineRoomPageQueryForm>{
    ServerResourceMachineRoomMapStruct INSTANCE = Mappers.getMapper( ServerResourceMachineRoomMapStruct.class );

}
