package scatter.serverresource.rest.project.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectUpdateForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectPageQueryForm;
import java.util.List;
/**
 * <p>
 * 项目表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
public interface IServerResourceProjectService extends IBaseService<ServerResourceProject> {


}
