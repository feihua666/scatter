package scatter.serverresource.rest.code.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.code.form.ServerResourceCodeAddForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodeUpdateForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodePageQueryForm;
import java.util.List;
/**
 * <p>
 * 代码表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
public interface IServerResourceCodeService extends IBaseService<ServerResourceCode> {


}
