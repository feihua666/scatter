package scatter.serverresource.rest.project.mapper;

import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 项目表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
public interface ServerResourceProjectMapper extends IBaseMapper<ServerResourceProject> {

}
