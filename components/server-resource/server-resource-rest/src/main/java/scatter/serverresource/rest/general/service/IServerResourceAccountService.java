package scatter.serverresource.rest.general.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.general.form.ServerResourceAccountAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountPageQueryForm;
import java.util.List;
/**
 * <p>
 * 账号表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface IServerResourceAccountService extends IBaseService<ServerResourceAccount> {


}
