package scatter.serverresource.rest.code.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.rest.code.service.IServerResourceCodeService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 代码翻译实现
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Component
public class ServerResourceCodeTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IServerResourceCodeService serverResourceCodeService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ServerResourceCode.TRANS_SERVERRESOURCECODE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ServerResourceCode.TRANS_SERVERRESOURCECODE_BY_ID)) {
            ServerResourceCode byId = serverResourceCodeService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ServerResourceCode.TRANS_SERVERRESOURCECODE_BY_ID)) {
            return serverResourceCodeService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
