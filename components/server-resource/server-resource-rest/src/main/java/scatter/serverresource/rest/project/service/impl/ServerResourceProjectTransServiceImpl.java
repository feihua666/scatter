package scatter.serverresource.rest.project.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.rest.project.service.IServerResourceProjectService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 项目翻译实现
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Component
public class ServerResourceProjectTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IServerResourceProjectService serverResourceProjectService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ServerResourceProject.TRANS_SERVERRESOURCEPROJECT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ServerResourceProject.TRANS_SERVERRESOURCEPROJECT_BY_ID)) {
            ServerResourceProject byId = serverResourceProjectService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ServerResourceProject.TRANS_SERVERRESOURCEPROJECT_BY_ID)) {
            return serverResourceProjectService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
