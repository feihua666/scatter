package scatter.serverresource.rest.project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.pojo.project.vo.ServerResourceProjectVo;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectUpdateForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectPageQueryForm;
import scatter.serverresource.rest.project.service.IServerResourceProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 项目表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Api(tags = "项目相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/project/server-resource-project")
public class ServerResourceProjectController extends BaseAddUpdateQueryFormController<ServerResourceProject, ServerResourceProjectVo, ServerResourceProjectAddForm, ServerResourceProjectUpdateForm, ServerResourceProjectPageQueryForm> {
    @Autowired
    private IServerResourceProjectService iServerResourceProjectService;

    @ApiOperation("添加项目")
    @PreAuthorize("hasAuthority('ServerResourceProject:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceProjectVo add(@RequestBody @Valid ServerResourceProjectAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询项目")
    @PreAuthorize("hasAuthority('ServerResourceProject:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceProjectVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除项目")
    @PreAuthorize("hasAuthority('ServerResourceProject:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新项目")
    @PreAuthorize("hasAuthority('ServerResourceProject:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceProjectVo update(@RequestBody @Valid ServerResourceProjectUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询项目")
    @PreAuthorize("hasAuthority('ServerResourceProject:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceProjectVo> getList(ServerResourceProjectPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询项目")
    @PreAuthorize("hasAuthority('ServerResourceProject:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceProjectVo> getPage(ServerResourceProjectPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
