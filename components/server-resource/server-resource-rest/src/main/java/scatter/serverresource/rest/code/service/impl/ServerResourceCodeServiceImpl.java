package scatter.serverresource.rest.code.service.impl;

import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.rest.code.mapper.ServerResourceCodeMapper;
import scatter.serverresource.rest.code.service.IServerResourceCodeService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.code.form.ServerResourceCodeAddForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodeUpdateForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 代码表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Service
@Transactional
public class ServerResourceCodeServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceCodeMapper, ServerResourceCode, ServerResourceCodeAddForm, ServerResourceCodeUpdateForm, ServerResourceCodePageQueryForm> implements IServerResourceCodeService {
    @Override
    public void preAdd(ServerResourceCodeAddForm addForm,ServerResourceCode po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceCodeUpdateForm updateForm,ServerResourceCode po) {
        super.preUpdate(updateForm,po);

    }
}
