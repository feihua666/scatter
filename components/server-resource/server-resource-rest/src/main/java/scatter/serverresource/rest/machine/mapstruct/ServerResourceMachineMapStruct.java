package scatter.serverresource.rest.machine.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.machine.po.ServerResourceMachine;
import scatter.serverresource.pojo.machine.vo.ServerResourceMachineVo;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachinePageQueryForm;

/**
 * <p>
 * 机器 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceMachineMapStruct extends IBaseVoMapStruct<ServerResourceMachine, ServerResourceMachineVo>,
                                  IBaseAddFormMapStruct<ServerResourceMachine,ServerResourceMachineAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceMachine,ServerResourceMachineUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceMachine,ServerResourceMachinePageQueryForm>{
    ServerResourceMachineMapStruct INSTANCE = Mappers.getMapper( ServerResourceMachineMapStruct.class );

}
