package scatter.serverresource.rest.general.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 代码和项目的版本控制关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
public interface IServerResourceCodeProjectVcRelService extends IBaseService<ServerResourceCodeProjectVcRel> {

    /**
     * 根据代码id查询
     * @param codeId
     * @return
     */
    default List<ServerResourceCodeProjectVcRel> getByCodeId(String codeId) {
        Assert.hasText(codeId,"codeId不能为空");
        return list(Wrappers.<ServerResourceCodeProjectVcRel>lambdaQuery().eq(ServerResourceCodeProjectVcRel::getCodeId, codeId));
    }
    /**
     * 根据项目的版本控制id查询
     * @param projectVcId
     * @return
     */
    default List<ServerResourceCodeProjectVcRel> getByProjectVcId(String projectVcId) {
        Assert.hasText(projectVcId,"projectVcId不能为空");
        return list(Wrappers.<ServerResourceCodeProjectVcRel>lambdaQuery().eq(ServerResourceCodeProjectVcRel::getProjectVcId, projectVcId));
    }

}
