package scatter.serverresource.rest.project.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.pojo.project.vo.ServerResourceProjectVo;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectUpdateForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectPageQueryForm;

/**
 * <p>
 * 项目 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceProjectMapStruct extends IBaseVoMapStruct<ServerResourceProject, ServerResourceProjectVo>,
                                  IBaseAddFormMapStruct<ServerResourceProject,ServerResourceProjectAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceProject,ServerResourceProjectUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceProject,ServerResourceProjectPageQueryForm>{
    ServerResourceProjectMapStruct INSTANCE = Mappers.getMapper( ServerResourceProjectMapStruct.class );

}
