package scatter.serverresource.rest.general.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.pojo.general.vo.ServerResourceCodeProjectVcRelVo;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelPageQueryForm;

/**
 * <p>
 * 代码和项目的版本控制 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerResourceCodeProjectVcRelMapStruct extends IBaseVoMapStruct<ServerResourceCodeProjectVcRel, ServerResourceCodeProjectVcRelVo>,
                                  IBaseAddFormMapStruct<ServerResourceCodeProjectVcRel,ServerResourceCodeProjectVcRelAddForm>,
                                  IBaseUpdateFormMapStruct<ServerResourceCodeProjectVcRel,ServerResourceCodeProjectVcRelUpdateForm>,
                                  IBaseQueryFormMapStruct<ServerResourceCodeProjectVcRel,ServerResourceCodeProjectVcRelPageQueryForm>{
    ServerResourceCodeProjectVcRelMapStruct INSTANCE = Mappers.getMapper( ServerResourceCodeProjectVcRelMapStruct.class );

}
