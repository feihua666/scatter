package scatter.serverresource.rest.machine.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.pojo.machine.vo.ServerResourceMachineRoomVo;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomPageQueryForm;
import scatter.serverresource.rest.machine.service.IServerResourceMachineRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 机房表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Api(tags = "机房相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/machine/server-resource-machine-room")
public class ServerResourceMachineRoomController extends BaseAddUpdateQueryFormController<ServerResourceMachineRoom, ServerResourceMachineRoomVo, ServerResourceMachineRoomAddForm, ServerResourceMachineRoomUpdateForm, ServerResourceMachineRoomPageQueryForm> {
    @Autowired
    private IServerResourceMachineRoomService iServerResourceMachineRoomService;

    @ApiOperation("添加机房")
    @PreAuthorize("hasAuthority('ServerResourceMachineRoom:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceMachineRoomVo add(@RequestBody @Valid ServerResourceMachineRoomAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询机房")
    @PreAuthorize("hasAuthority('ServerResourceMachineRoom:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceMachineRoomVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除机房")
    @PreAuthorize("hasAuthority('ServerResourceMachineRoom:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新机房")
    @PreAuthorize("hasAuthority('ServerResourceMachineRoom:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceMachineRoomVo update(@RequestBody @Valid ServerResourceMachineRoomUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询机房")
    @PreAuthorize("hasAuthority('ServerResourceMachineRoom:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceMachineRoomVo> getList(ServerResourceMachineRoomPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询机房")
    @PreAuthorize("hasAuthority('ServerResourceMachineRoom:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceMachineRoomVo> getPage(ServerResourceMachineRoomPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
