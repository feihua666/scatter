package scatter.serverresource.rest.general.mapper;

import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 环境表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface ServerResourceEnvironmentMapper extends IBaseMapper<ServerResourceEnvironment> {

}
