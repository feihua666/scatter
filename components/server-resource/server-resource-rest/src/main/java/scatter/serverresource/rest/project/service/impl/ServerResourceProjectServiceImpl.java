package scatter.serverresource.rest.project.service.impl;

import scatter.serverresource.pojo.project.po.ServerResourceProject;
import scatter.serverresource.rest.project.mapper.ServerResourceProjectMapper;
import scatter.serverresource.rest.project.service.IServerResourceProjectService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.project.form.ServerResourceProjectAddForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectUpdateForm;
import scatter.serverresource.pojo.project.form.ServerResourceProjectPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 项目表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Service
@Transactional
public class ServerResourceProjectServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceProjectMapper, ServerResourceProject, ServerResourceProjectAddForm, ServerResourceProjectUpdateForm, ServerResourceProjectPageQueryForm> implements IServerResourceProjectService {
    @Override
    public void preAdd(ServerResourceProjectAddForm addForm,ServerResourceProject po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceProjectUpdateForm updateForm,ServerResourceProject po) {
        super.preUpdate(updateForm,po);

    }
}
