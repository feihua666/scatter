package scatter.serverresource.rest.machine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.serverresource.rest.machine.service.IServerResourceMachineRoomService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 机房翻译实现
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Component
public class ServerResourceMachineRoomTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IServerResourceMachineRoomService serverResourceMachineRoomService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ServerResourceMachineRoom.TRANS_SERVERRESOURCEMACHINEROOM_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ServerResourceMachineRoom.TRANS_SERVERRESOURCEMACHINEROOM_BY_ID)) {
            ServerResourceMachineRoom byId = serverResourceMachineRoomService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ServerResourceMachineRoom.TRANS_SERVERRESOURCEMACHINEROOM_BY_ID)) {
            return serverResourceMachineRoomService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
