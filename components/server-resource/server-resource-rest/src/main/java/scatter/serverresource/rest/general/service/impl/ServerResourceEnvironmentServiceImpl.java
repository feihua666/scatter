package scatter.serverresource.rest.general.service.impl;

import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.serverresource.rest.general.mapper.ServerResourceEnvironmentMapper;
import scatter.serverresource.rest.general.service.IServerResourceEnvironmentService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 环境表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Service
@Transactional
public class ServerResourceEnvironmentServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceEnvironmentMapper, ServerResourceEnvironment, ServerResourceEnvironmentAddForm, ServerResourceEnvironmentUpdateForm, ServerResourceEnvironmentPageQueryForm> implements IServerResourceEnvironmentService {
    @Override
    public void preAdd(ServerResourceEnvironmentAddForm addForm,ServerResourceEnvironment po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceEnvironmentUpdateForm updateForm,ServerResourceEnvironment po) {
        super.preUpdate(updateForm,po);

    }
}
