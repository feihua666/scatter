package scatter.serverresource.rest.general.service.impl;

import scatter.serverresource.pojo.general.po.ServerResourceAccount;
import scatter.serverresource.rest.general.mapper.ServerResourceAccountMapper;
import scatter.serverresource.rest.general.service.IServerResourceAccountService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.general.form.ServerResourceAccountAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceAccountPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 账号表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Service
@Transactional
public class ServerResourceAccountServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceAccountMapper, ServerResourceAccount, ServerResourceAccountAddForm, ServerResourceAccountUpdateForm, ServerResourceAccountPageQueryForm> implements IServerResourceAccountService {
    @Override
    public void preAdd(ServerResourceAccountAddForm addForm,ServerResourceAccount po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceAccountUpdateForm updateForm,ServerResourceAccount po) {
        super.preUpdate(updateForm,po);

    }
}
