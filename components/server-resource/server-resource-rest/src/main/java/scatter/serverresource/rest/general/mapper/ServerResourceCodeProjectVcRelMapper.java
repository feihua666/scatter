package scatter.serverresource.rest.general.mapper;

import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 代码和项目的版本控制关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
public interface ServerResourceCodeProjectVcRelMapper extends IBaseMapper<ServerResourceCodeProjectVcRel> {

}
