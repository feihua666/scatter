package scatter.serverresource.rest.general.service.impl;

import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.rest.general.mapper.ServerResourceCodeProjectVcRelMapper;
import scatter.serverresource.rest.general.service.IServerResourceCodeProjectVcRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 代码和项目的版本控制关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Service
@Transactional
public class ServerResourceCodeProjectVcRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceCodeProjectVcRelMapper, ServerResourceCodeProjectVcRel, ServerResourceCodeProjectVcRelAddForm, ServerResourceCodeProjectVcRelUpdateForm, ServerResourceCodeProjectVcRelPageQueryForm> implements IServerResourceCodeProjectVcRelService {
    @Override
    public void preAdd(ServerResourceCodeProjectVcRelAddForm addForm,ServerResourceCodeProjectVcRel po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceCodeProjectVcRelUpdateForm updateForm,ServerResourceCodeProjectVcRel po) {
        super.preUpdate(updateForm,po);

    }
}
