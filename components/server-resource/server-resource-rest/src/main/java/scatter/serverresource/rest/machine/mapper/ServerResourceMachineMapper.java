package scatter.serverresource.rest.machine.mapper;

import scatter.serverresource.pojo.machine.po.ServerResourceMachine;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 机器表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface ServerResourceMachineMapper extends IBaseMapper<ServerResourceMachine> {

}
