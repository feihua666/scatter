package scatter.serverresource.rest.machine.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.machine.po.ServerResourceMachineRoom;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineRoomPageQueryForm;
import java.util.List;
/**
 * <p>
 * 机房表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface IServerResourceMachineRoomService extends IBaseService<ServerResourceMachineRoom> {


}
