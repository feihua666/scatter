package scatter.serverresource.rest.general.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.general.po.ServerResourceCodeProjectVcRel;
import scatter.serverresource.pojo.general.vo.ServerResourceCodeProjectVcRelVo;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceCodeProjectVcRelPageQueryForm;
import scatter.serverresource.pojo.general.form.CodeAssignProjectVcForm;
import scatter.serverresource.pojo.general.form.ProjectVcAssignCodeForm;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import java.util.stream.Collectors;
import scatter.serverresource.rest.general.service.IServerResourceCodeProjectVcRelService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 代码和项目的版本控制关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Api(tags = "代码和项目的版本控制相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/general/server-resource-code-project-vc-rel")
public class ServerResourceCodeProjectVcRelController extends BaseAddUpdateQueryFormController<ServerResourceCodeProjectVcRel, ServerResourceCodeProjectVcRelVo, ServerResourceCodeProjectVcRelAddForm, ServerResourceCodeProjectVcRelUpdateForm, ServerResourceCodeProjectVcRelPageQueryForm> {
    @Autowired
    private IServerResourceCodeProjectVcRelService iServerResourceCodeProjectVcRelService;

    @ApiOperation("添加代码和项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceCodeProjectVcRelVo add(@RequestBody @Valid ServerResourceCodeProjectVcRelAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询代码和项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceCodeProjectVcRelVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除代码和项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新代码和项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceCodeProjectVcRelVo update(@RequestBody @Valid ServerResourceCodeProjectVcRelUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询代码和项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceCodeProjectVcRelVo> getList(ServerResourceCodeProjectVcRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询代码和项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceCodeProjectVcRelVo> getPage(ServerResourceCodeProjectVcRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("代码分配项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:single:codeAssignProjectVc')")
    @PostMapping("/code/assign/projectvc")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean codeAssignProjectVc(@RequestBody @Valid CodeAssignProjectVcForm cf) {
        return iBaseService.removeAndAssignRel(cf.getCodeId(),cf.getCheckedProjectVcIds(),cf.getUncheckedProjectVcIds(),cf.getIsLazyLoad(),ServerResourceCodeProjectVcRel::getCodeId,ServerResourceCodeProjectVcRel::getProjectVcId,(relDto)->new ServerResourceCodeProjectVcRel().setCodeId(relDto.getMainId()).setProjectVcId(relDto.getOtherId()));
    }

    @ApiOperation("根据代码ID查询已分配的项目的版本控制id")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:single:queryByCodeId')")
    @GetMapping("/code/{codeId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByCodeId(@PathVariable String codeId) {
        List<ServerResourceCodeProjectVcRel> rels = iServerResourceCodeProjectVcRelService.getByCodeId(codeId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }

        return rels.stream().map(item -> item.getProjectVcId()).collect(Collectors.toList());
    }

    @ApiOperation("清空代码下的所有项目的版本控制")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:single:deleteByCodeId')")
    @DeleteMapping("/code/{codeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByCodeId(@PathVariable String codeId) {
        return iBaseService.removeAssignRel(codeId,ServerResourceCodeProjectVcRel::getCodeId);
    }


    @ApiOperation("项目的版本控制分配代码")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:single:projectVcAssignCode')")
    @PostMapping("/projectvc/assign/code")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean projectVcAssignCode(@RequestBody @Valid ProjectVcAssignCodeForm cf) {
        return iServerResourceCodeProjectVcRelService.removeAndAssignRel(cf.getProjectVcId(),cf.getCheckedCodeIds(),cf.getUncheckedCodeIds(),cf.getIsLazyLoad(),ServerResourceCodeProjectVcRel::getProjectVcId,ServerResourceCodeProjectVcRel::getCodeId,(relDto)->new ServerResourceCodeProjectVcRel().setProjectVcId(relDto.getMainId()).setCodeId(relDto.getOtherId()));
    }

    @ApiOperation("根据项目的版本控制ID查询已分配的代码id")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:single:queryByProjectVcId')")
    @GetMapping("/projectvc/{projectVcId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByProjectVcId(@PathVariable String projectVcId) {
        List<ServerResourceCodeProjectVcRel> rels = iServerResourceCodeProjectVcRelService.getByProjectVcId(projectVcId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }
        return rels.stream().map(item -> item.getProjectVcId()).collect(Collectors.toList());
    }

    @ApiOperation("清空项目的版本控制下的所有代码")
    @PreAuthorize("hasAuthority('ServerResourceCodeProjectVcRel:single:deleteByProjectVcId')")
    @DeleteMapping("/projectvc/{projectVcId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByProjectVcId(@PathVariable String projectVcId) {
        return iServerResourceCodeProjectVcRelService.removeAssignRel(projectVcId,ServerResourceCodeProjectVcRel::getProjectVcId);
    }
}
