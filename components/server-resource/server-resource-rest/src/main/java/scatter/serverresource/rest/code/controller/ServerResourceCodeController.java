package scatter.serverresource.rest.code.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.serverresource.rest.ServerResourceConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.serverresource.pojo.code.vo.ServerResourceCodeVo;
import scatter.serverresource.pojo.code.form.ServerResourceCodeAddForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodeUpdateForm;
import scatter.serverresource.pojo.code.form.ServerResourceCodePageQueryForm;
import scatter.serverresource.rest.code.service.IServerResourceCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 代码表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
@Api(tags = "代码相关接口")
@RestController
@RequestMapping(ServerResourceConfiguration.CONTROLLER_BASE_PATH + "/code/server-resource-code")
public class ServerResourceCodeController extends BaseAddUpdateQueryFormController<ServerResourceCode, ServerResourceCodeVo, ServerResourceCodeAddForm, ServerResourceCodeUpdateForm, ServerResourceCodePageQueryForm> {
    @Autowired
    private IServerResourceCodeService iServerResourceCodeService;

    @ApiOperation("添加代码")
    @PreAuthorize("hasAuthority('ServerResourceCode:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceCodeVo add(@RequestBody @Valid ServerResourceCodeAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询代码")
    @PreAuthorize("hasAuthority('ServerResourceCode:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ServerResourceCodeVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除代码")
    @PreAuthorize("hasAuthority('ServerResourceCode:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新代码")
    @PreAuthorize("hasAuthority('ServerResourceCode:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ServerResourceCodeVo update(@RequestBody @Valid ServerResourceCodeUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询代码")
    @PreAuthorize("hasAuthority('ServerResourceCode:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ServerResourceCodeVo> getList(ServerResourceCodePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询代码")
    @PreAuthorize("hasAuthority('ServerResourceCode:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ServerResourceCodeVo> getPage(ServerResourceCodePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
