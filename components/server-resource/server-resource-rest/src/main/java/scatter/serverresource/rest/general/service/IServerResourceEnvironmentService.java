package scatter.serverresource.rest.general.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.general.po.ServerResourceEnvironment;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentAddForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentUpdateForm;
import scatter.serverresource.pojo.general.form.ServerResourceEnvironmentPageQueryForm;
import java.util.List;
/**
 * <p>
 * 环境表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface IServerResourceEnvironmentService extends IBaseService<ServerResourceEnvironment> {


}
