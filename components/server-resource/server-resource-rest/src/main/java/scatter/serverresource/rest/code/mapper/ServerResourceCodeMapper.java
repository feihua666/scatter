package scatter.serverresource.rest.code.mapper;

import scatter.serverresource.pojo.code.po.ServerResourceCode;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 代码表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2022-02-07
 */
public interface ServerResourceCodeMapper extends IBaseMapper<ServerResourceCode> {

}
