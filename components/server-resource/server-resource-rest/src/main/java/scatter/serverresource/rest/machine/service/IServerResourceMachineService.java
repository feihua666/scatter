package scatter.serverresource.rest.machine.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.serverresource.pojo.machine.po.ServerResourceMachine;
import scatter.common.rest.service.IBaseService;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachinePageQueryForm;
import java.util.List;
/**
 * <p>
 * 机器表 服务类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
public interface IServerResourceMachineService extends IBaseService<ServerResourceMachine> {


}
