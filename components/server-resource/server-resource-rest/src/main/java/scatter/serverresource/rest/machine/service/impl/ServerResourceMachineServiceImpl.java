package scatter.serverresource.rest.machine.service.impl;

import scatter.serverresource.pojo.machine.po.ServerResourceMachine;
import scatter.serverresource.rest.machine.mapper.ServerResourceMachineMapper;
import scatter.serverresource.rest.machine.service.IServerResourceMachineService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineAddForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachineUpdateForm;
import scatter.serverresource.pojo.machine.form.ServerResourceMachinePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 机器表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2022-01-28
 */
@Service
@Transactional
public class ServerResourceMachineServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ServerResourceMachineMapper, ServerResourceMachine, ServerResourceMachineAddForm, ServerResourceMachineUpdateForm, ServerResourceMachinePageQueryForm> implements IServerResourceMachineService {
    @Override
    public void preAdd(ServerResourceMachineAddForm addForm,ServerResourceMachine po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ServerResourceMachineUpdateForm updateForm,ServerResourceMachine po) {
        super.preUpdate(updateForm,po);

    }
}
