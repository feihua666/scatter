import WxMaMsgReplyForm from './WxMaMsgReplyForm.js'
import WxMaMsgReplyTable from './WxMaMsgReplyTable.js'
import WxMaMsgReplyUrl from './WxMaMsgReplyUrl.js'
const WxMaMsgReplyMixin = {
    computed: {
        WxMaMsgReplyFormOptions() {
            return this.$stDynamicFormTools.options(WxMaMsgReplyForm,this.$options.name)
        },
        WxMaMsgReplyTableOptions() {
            return WxMaMsgReplyTable
        },
        WxMaMsgReplyUrl(){
            return WxMaMsgReplyUrl
        }
    },
}
export default WxMaMsgReplyMixin