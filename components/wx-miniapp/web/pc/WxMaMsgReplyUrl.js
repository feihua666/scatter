const basePath = '' + '/wx-ma-msg-reply'
const WxMaMsgReplyUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/WxMaMsgReplySearchList',
        add: '/WxMaMsgReplyAdd',
        update: '/WxMaMsgReplyUpdate',
    }
}
export default WxMaMsgReplyUrl