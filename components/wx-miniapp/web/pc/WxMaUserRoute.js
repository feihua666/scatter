import WxMaUserUrl from './WxMaUserUrl.js'

const WxMaUserRoute = [
    {
        path: WxMaUserUrl.router.searchList,
        component: () => import('./element/WxMaUserSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'WxMaUserSearchList',
            name: '小程序微信用户管理'
        }
    },
    {
        path: WxMaUserUrl.router.add,
        component: () => import('./element/WxMaUserAdd'),
        meta: {
            code:'WxMaUserAdd',
            name: '小程序微信用户添加'
        }
    },
    {
        path: WxMaUserUrl.router.update,
        component: () => import('./element/WxMaUserUpdate'),
        meta: {
            code:'WxMaUserUpdate',
            name: '小程序微信用户修改'
        }
    },
]
export default WxMaUserRoute