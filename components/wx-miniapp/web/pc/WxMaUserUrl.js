const basePath = '' + '/wx-ma-user'
const WxMaUserUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/WxMaUserSearchList',
        add: '/WxMaUserAdd',
        update: '/WxMaUserUpdate',
    }
}
export default WxMaUserUrl