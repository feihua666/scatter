const WxMaMsgReplyTable = [
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'appCode',
        label: '小程序编码'
    },
    {
        prop: 'appName',
        label: '小程序名称'
    },
    {
        prop: 'matchFromUser',
        label: '匹配接收人'
    },
    {
        prop: 'matchMsgTypeDictId',
        label: '匹配消息类型'
    },
    {
        prop: 'matchEventTypeDictId',
        label: '匹配事件类型'
    },
    {
        prop: 'matchEventKey',
        label: '匹配事件key'
    },
    {
        prop: 'matchEventKeyMatchDictId',
        label: '匹配event_key的匹配方式'
    },
    {
        prop: 'matchContent',
        label: '匹配内容'
    },
    {
        prop: 'matchContentMatchDictId',
        label: '匹配内容的匹配方式'
    },
    {
        prop: 'replyMsgTypeDictId',
        label: '回复的消息类型'
    },
    {
        prop: 'replyContent',
        label: '回复内容'
    },
    {
        prop: 'priority',
        label: '优先级'
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default WxMaMsgReplyTable