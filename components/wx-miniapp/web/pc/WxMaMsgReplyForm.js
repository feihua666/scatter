import WxMaMsgReplyUrl from './WxMaMsgReplyUrl.js'
const WxMaMsgReplyForm = [
    {

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
        }
    },
    {
        WxMaMsgReplySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'appCode',
        },
        element:{
            label: '小程序编码',
            required: true,
        }
    },
    {
        WxMaMsgReplySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'appName',
        },
        element:{
            label: '小程序名称',
            required: true,
        }
    },
    {

        field: {
            name: 'matchFromUser',
        },
        element:{
            label: '匹配接收人',
        }
    },
    {

        field: {
            name: 'matchMsgTypeDictId',
        },
        element:{
            label: '匹配消息类型',
        }
    },
    {

        field: {
            name: 'matchEventTypeDictId',
        },
        element:{
            label: '匹配事件类型',
        }
    },
    {

        field: {
            name: 'matchEventKey',
        },
        element:{
            label: '匹配事件key',
        }
    },
    {

        field: {
            name: 'matchEventKeyMatchDictId',
        },
        element:{
            label: '匹配event_key的匹配方式',
        }
    },
    {

        field: {
            name: 'matchContent',
        },
        element:{
            label: '匹配内容',
        }
    },
    {

        field: {
            name: 'matchContentMatchDictId',
        },
        element:{
            label: '匹配内容的匹配方式',
        }
    },
    {
        WxMaMsgReplySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'replyMsgTypeDictId',
        },
        element:{
            label: '回复的消息类型',
            required: true,
        }
    },
    {
        WxMaMsgReplySearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'replyContent',
        },
        element:{
            label: '回复内容',
            required: true,
        }
    },
    {

        field: {
            name: 'priority',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '优先级',
        }
    },
    {

        field: {
            name: 'remark',
        },
        element:{
            label: '备注',
        }
    },

]
export default WxMaMsgReplyForm