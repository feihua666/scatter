import WxMaUserUrl from './WxMaUserUrl.js'
const WxMaUserForm = [
    {
        WxMaUserSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'appCode',
        },
        element:{
            label: '小程序编码',
            required: true,
        }
    },
    {
        WxMaUserSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'appName',
        },
        element:{
            label: '小程序名称',
            required: true,
        }
    },
    {
        WxMaUserSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'openId',
        },
        element:{
            label: '微信openId',
            required: true,
        }
    },
    {

        field: {
            name: 'unionId',
        },
        element:{
            label: '微信全局id',
        }
    },
    {

        field: {
            name: 'nickname',
        },
        element:{
            label: '微信昵称',
        }
    },
    {

        field: {
            name: 'genderDictId',
        },
        element:{
            label: '性别',
        }
    },
    {

        field: {
            name: 'cityAreaId',
        },
        element:{
            label: '城市区域id',
        }
    },
    {

        field: {
            name: 'cityName',
        },
        element:{
            label: '城市名称',
        }
    },
    {

        field: {
            name: 'provinceAreaId',
        },
        element:{
            label: '省区域id',
        }
    },
    {

        field: {
            name: 'provinceName',
        },
        element:{
            label: '省名称',
        }
    },
    {

        field: {
            name: 'countryAreaId',
        },
        element:{
            label: '国家区域id',
        }
    },
    {

        field: {
            name: 'countryName',
        },
        element:{
            label: '国家名称',
        }
    },
    {

        field: {
            name: 'avatar',
        },
        element:{
            label: '头像地址',
        }
    },
    {

        field: {
            name: 'language',
        },
        element:{
            label: '语言',
        }
    },
    {

        field: {
            name: 'longitude',
        },
        element:{
            label: '经度',
        }
    },
    {

        field: {
            name: 'latitude',
        },
        element:{
            label: '纬度',
        }
    },
    {

        field: {
            name: 'precisions',
        },
        element:{
            label: '经纬度精度',
        }
    },
    {

        field: {
            name: 'locationDesc',
        },
        element:{
            label: '地理位置描述',
        }
    },

]
export default WxMaUserForm