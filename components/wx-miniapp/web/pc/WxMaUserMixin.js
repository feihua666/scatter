import WxMaUserForm from './WxMaUserForm.js'
import WxMaUserTable from './WxMaUserTable.js'
import WxMaUserUrl from './WxMaUserUrl.js'
const WxMaUserMixin = {
    computed: {
        WxMaUserFormOptions() {
            return this.$stDynamicFormTools.options(WxMaUserForm,this.$options.name)
        },
        WxMaUserTableOptions() {
            return WxMaUserTable
        },
        WxMaUserUrl(){
            return WxMaUserUrl
        }
    },
}
export default WxMaUserMixin