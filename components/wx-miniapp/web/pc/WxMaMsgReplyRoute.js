import WxMaMsgReplyUrl from './WxMaMsgReplyUrl.js'

const WxMaMsgReplyRoute = [
    {
        path: WxMaMsgReplyUrl.router.searchList,
        component: () => import('./element/WxMaMsgReplySearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'WxMaMsgReplySearchList',
            name: '小程序消息回复管理'
        }
    },
    {
        path: WxMaMsgReplyUrl.router.add,
        component: () => import('./element/WxMaMsgReplyAdd'),
        meta: {
            code:'WxMaMsgReplyAdd',
            name: '小程序消息回复添加'
        }
    },
    {
        path: WxMaMsgReplyUrl.router.update,
        component: () => import('./element/WxMaMsgReplyUpdate'),
        meta: {
            code:'WxMaMsgReplyUpdate',
            name: '小程序消息回复修改'
        }
    },
]
export default WxMaMsgReplyRoute