const WxMaUserTable = [
    {
        prop: 'appCode',
        label: '小程序编码'
    },
    {
        prop: 'appName',
        label: '小程序名称'
    },
    {
        prop: 'openId',
        label: '微信openId'
    },
    {
        prop: 'unionId',
        label: '微信全局id'
    },
    {
        prop: 'nickname',
        label: '微信昵称'
    },
    {
        prop: 'genderDictId',
        label: '性别'
    },
    {
        prop: 'cityAreaId',
        label: '城市区域id'
    },
    {
        prop: 'cityName',
        label: '城市名称'
    },
    {
        prop: 'provinceAreaId',
        label: '省区域id'
    },
    {
        prop: 'provinceName',
        label: '省名称'
    },
    {
        prop: 'countryAreaId',
        label: '国家区域id'
    },
    {
        prop: 'countryName',
        label: '国家名称'
    },
    {
        prop: 'avatar',
        label: '头像地址'
    },
    {
        prop: 'language',
        label: '语言'
    },
    {
        prop: 'longitude',
        label: '经度'
    },
    {
        prop: 'latitude',
        label: '纬度'
    },
    {
        prop: 'precisions',
        label: '经纬度精度'
    },
    {
        prop: 'locationDesc',
        label: '地理位置描述'
    },
]
export default WxMaUserTable