package scatter.wxminiapp.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 小程序微信用户表
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_wx_ma_user")
@ApiModel(value="WxMaUser对象", description="小程序微信用户表")
public class WxMaUser extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_WXMAUSER_BY_ID = "trans_wxmauser_by_id_scatter.wxminiapp.pojo.po";

    @ApiModelProperty(value = "小程序编码")
    private String appCode;

    @ApiModelProperty(value = "小程序名称")
    private String appName;

    @ApiModelProperty(value = "微信openId")
    private String openId;

    @ApiModelProperty(value = "微信全局id")
    private String unionId;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "手机号区号")
    private String mobileCountryCode;

    @ApiModelProperty(value = "微信昵称")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "城市区域id")
    private String cityAreaId;

    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @ApiModelProperty(value = "省区域id")
    private String provinceAreaId;

    @ApiModelProperty(value = "省名称")
    private String provinceName;

    @ApiModelProperty(value = "国家区域id")
    private String countryAreaId;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "语言")
    private String language;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;


}
