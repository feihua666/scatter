package scatter.wxminiapp.pojo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.LoginUser;
import scatter.wxminiapp.pojo.po.WxMaUser;

/**
 * Created by yangwei
 * Created at 2021/11/22 20:38
 */
@Getter
@Setter
@ApiModel("登录用户信息")
public class WxMaLoginUser extends LoginUser  {

    @ApiModelProperty(value = "用户登录信息")
    private WxMaUser wxMaUser;

    /**
     * 可以用来获取手机号，微信旧版本
     */
    @ApiModelProperty(value = "登录后的sessionKey")
    private String sessionKey;
}
