package scatter.wxminiapp.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 小程序微信用户响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Setter
@Getter
@ApiModel(value="小程序微信用户响应对象")
public class WxMaUserVo extends BaseIdVo {

    @ApiModelProperty(value = "小程序编码")
    private String appCode;

    @ApiModelProperty(value = "小程序名称")
    private String appName;

    @ApiModelProperty(value = "微信openId")
    private String openId;

    @ApiModelProperty(value = "微信全局id")
    private String unionId;

    @ApiModelProperty(value = "微信昵称")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "城市区域id")
    private String cityAreaId;

    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @ApiModelProperty(value = "省区域id")
    private String provinceAreaId;

    @ApiModelProperty(value = "省名称")
    private String provinceName;

    @ApiModelProperty(value = "国家区域id")
    private String countryAreaId;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "语言")
    private String language;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经纬度精度")
    private String precisions;

    @ApiModelProperty(value = "地理位置描述")
    private String locationDesc;

}
