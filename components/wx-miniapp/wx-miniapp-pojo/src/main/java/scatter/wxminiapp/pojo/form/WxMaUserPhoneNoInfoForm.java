package scatter.wxminiapp.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * code方式获取小程序手机号
 * </p>
 *
 * @author yw
 * @since 2022-3-22
 */
@Setter
@Getter
@ApiModel(value="小程序微信用户添加表单对象")
public class WxMaUserPhoneNoInfoForm extends BaseAddForm {

    @NotEmpty(message="小程序编码不能为空")
    @ApiModelProperty(value = "小程序编码，标识是哪一个小程序",required = true)
    private String appCode;

    @NotEmpty(message="code 不能为空")
    @ApiModelProperty(value = "小程序的code",required = true)
    private String code;

    @NotEmpty(message="加密内容不能为空")
    @ApiModelProperty("加密内容")
    private String encryptedData;

    @NotEmpty(message="加密算法的初始向量不能为空")
    @ApiModelProperty("加密算法的初始向量")
    private String iv;
}
