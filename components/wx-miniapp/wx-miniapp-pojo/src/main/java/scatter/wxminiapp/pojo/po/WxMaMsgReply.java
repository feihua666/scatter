package scatter.wxminiapp.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.dict.IDictGroup;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 小程序消息回复表
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_wx_ma_msg_reply")
@ApiModel(value="WxMaMsgReply对象", description="小程序消息回复表")
public class WxMaMsgReply extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_WXMAMSGREPLY_BY_ID = "trans_wxmamsgreply_by_id_scatter.wxminiapp.pojo.po";



    /**
     * xml消息类型字典项的前缀，也就是说建立的字典项必须以该前缀开始，且以 me.chanjar.weixin.common.api.WxConsts.XmlMsgType 中的值结尾
     */
    public static final String WX_MA_PUSH_XML_MSG_TYPE = "wx_ma_push_xml_msg_type_";
    /**
     * 消息类型字典项的前缀，也就是说建立的字典项必须以该前缀开始，且以 me.chanjar.weixin.common.api.WxConsts.EventType 中的值结尾
     */
    public static final String EVENT_TYPE_DICT_ITEM_PREFIX = "wx_ma_push_event_type_";


    public enum XmlMsgTypeDictGroup implements IDictGroup {
        wx_ma_push_xml_msg_type;

        @Override
        public String groupCode() {
            return this.name();
        }
    }
    public enum EventTypeDictGroup implements IDictGroup{
        wx_ma_push_event_type;

        @Override
        public String groupCode() {
            return this.name();
        }
    }

    @ApiModelProperty(value = "名称，仅标识是什么消息，模糊查询")
    private String name;

    @ApiModelProperty(value = "小程序编码，模糊查询")
    private String appCode;

    @ApiModelProperty(value = "小程序名称，模糊查询")
    private String appName;

    @ApiModelProperty(value = "匹配接收人")
    private String matchFromUser;

    @ApiModelProperty(value = "匹配消息类型")
    private String matchMsgTypeDictId;

    @ApiModelProperty(value = "匹配事件类型")
    private String matchEventTypeDictId;

    @ApiModelProperty(value = "匹配事件key")
    private String matchEventKey;

    @ApiModelProperty(value = "匹配event_key的匹配方式")
    private String matchEventKeyMatchDictId;

    @ApiModelProperty(value = "匹配内容")
    private String matchContent;

    @ApiModelProperty(value = "匹配内容的匹配方式")
    private String matchContentMatchDictId;

    @ApiModelProperty(value = "回复的消息类型")
    private String replyMsgTypeDictId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "优先级，数值越大优先级越高")
    private Integer priority;

    @ApiModelProperty(value = "备注")
    private String remark;


}
