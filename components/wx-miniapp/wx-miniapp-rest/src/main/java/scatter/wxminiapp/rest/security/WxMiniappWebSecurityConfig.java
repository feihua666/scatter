package scatter.wxminiapp.rest.security;

import cn.binarywang.wx.miniapp.api.WxMaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import scatter.common.rest.config.CommonRestSecurityConfigure;
import scatter.common.rest.security.DefaultAuthenticationFailureHandler;
import scatter.common.rest.security.DefaultAuthenticationSuccessHandler;
import scatter.wxminiapp.rest.security.mobile.WxMiniappMobileUserDetailsServiceImpl;
import scatter.wxminiapp.rest.security.mobile.WxMiniappMobileAuthenticationProvider;

import scatter.wxminiapp.rest.security.normal.WxMiniappCodeAuthenticationFilter;
import scatter.wxminiapp.rest.security.normal.WxMiniappCodeAuthenticationProvider;
import scatter.wxminiapp.rest.security.normal.WxMiniappUserDetailsServiceImpl;

/**
 * Created by yangwei
 * Created at 2021/1/6 16:46
 */
public class WxMiniappWebSecurityConfig implements CommonRestSecurityConfigure {

    /**
     * 小程序登录太方式
     */
    public static String wxMiniappLoginUrl = "/wxMiniapp/login";
    /**
     * 手机号登录地址
     */
    public static String wxMiniappLogin_by_mobile_Url = "/wxMiniapp/login-by-mobile";

    @Autowired
    private WxMiniappUserDetailsServiceImpl wxMaUserDetailsService;
    @Autowired
    private WxMiniappMobileUserDetailsServiceImpl wxMiniappMobileUserDetailsService;
    @Autowired
    private WxMaService wxMaService;

    @Override
    public void configure(HttpSecurity http, AuthenticationManager authenticationManager) throws Exception {

        // 登录成功处理器
        DefaultAuthenticationSuccessHandler defaultAuthenticationSuccessHandler = new DefaultAuthenticationSuccessHandler();
        // 登录失败处理器
        DefaultAuthenticationFailureHandler defaultAuthenticationFailureHandler = new DefaultAuthenticationFailureHandler();
        // 基础登录态登录
        WxMiniappCodeAuthenticationFilter miniappCodeAuthenticationFilter = new WxMiniappCodeAuthenticationFilter();
        miniappCodeAuthenticationFilter.setFilterProcessesUrl(wxMiniappLoginUrl);
        miniappCodeAuthenticationFilter.setAuthenticationManager(authenticationManager);
        miniappCodeAuthenticationFilter.setAuthenticationSuccessHandler(defaultAuthenticationSuccessHandler);
        miniappCodeAuthenticationFilter.setAuthenticationFailureHandler(defaultAuthenticationFailureHandler);

        http.addFilterAfter(miniappCodeAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

        // 基础登录态登录
        WxMiniappCodeAuthenticationFilter mobileAuthenticationFilter = new WxMiniappCodeAuthenticationFilter();
        mobileAuthenticationFilter.setFilterProcessesUrl(wxMiniappLogin_by_mobile_Url);
        mobileAuthenticationFilter.setAuthenticationManager(authenticationManager);
        mobileAuthenticationFilter.setAuthenticationSuccessHandler(defaultAuthenticationSuccessHandler);
        mobileAuthenticationFilter.setAuthenticationFailureHandler(defaultAuthenticationFailureHandler);

        http.addFilterAfter(mobileAuthenticationFilter, WxMiniappCodeAuthenticationFilter.class);


        //http.authorizeRequests().mvcMatchers(HttpMethod.POST,loginUrl).permitAll();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {

        // 基础登录态登录
        WxMiniappCodeAuthenticationProvider codeAuthenticationProvider = new WxMiniappCodeAuthenticationProvider();
        codeAuthenticationProvider.setUserDetailsService(wxMaUserDetailsService);
        codeAuthenticationProvider.setWxMaService(wxMaService);
        auth.authenticationProvider(codeAuthenticationProvider);

        // 手机号登录
        WxMiniappMobileAuthenticationProvider mobileAuthenticationProvider = new WxMiniappMobileAuthenticationProvider();
        mobileAuthenticationProvider.setUserDetailsService(wxMiniappMobileUserDetailsService);
        mobileAuthenticationProvider.setWxMaService(wxMaService);
        auth.authenticationProvider(mobileAuthenticationProvider);


    }
}
