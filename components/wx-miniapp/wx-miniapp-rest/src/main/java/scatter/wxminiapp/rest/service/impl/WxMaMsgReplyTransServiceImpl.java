package scatter.wxminiapp.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.wxminiapp.rest.service.IWxMaMsgReplyService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 小程序消息回复翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Component
public class WxMaMsgReplyTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IWxMaMsgReplyService wxMaMsgReplyService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,WxMaMsgReply.TRANS_WXMAMSGREPLY_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,WxMaMsgReply.TRANS_WXMAMSGREPLY_BY_ID)) {
            WxMaMsgReply byId = wxMaMsgReplyService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,WxMaMsgReply.TRANS_WXMAMSGREPLY_BY_ID)) {
            return wxMaMsgReplyService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
