package scatter.wxminiapp.rest.service;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import me.chanjar.weixin.common.session.WxSessionManager;
import scatter.common.rest.service.IBaseService;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;

import java.util.Map;

/**
 * <p>
 * 小程序消息回复表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
public interface IWxMaMsgReplyService extends IBaseService<WxMaMsgReply> {


	/**
	 * 获取匹配的消息回复内容
	 * @param wxMessage
	 * @param context
	 * @param wxMaService
	 * @param sessionManager
	 * @return
	 */
	public WxMaMsgReply getMatched(WxMaMessage wxMessage, Map<String, Object> context, WxMaService wxMaService, WxSessionManager sessionManager);
}
