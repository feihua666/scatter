package scatter.wxminiapp.rest.security;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import scatter.common.LoginUser;
import scatter.wxminiapp.pojo.login.WxMaLoginUser;
import scatter.wxminiapp.pojo.po.WxMaUser;

/**
 * Created by yangwei
 * Created at 2021/1/6 18:26
 */
public interface WxMiniappUserDetailsServiceListener {

    /**
     * 微信小程序登录获取code2Session信息后调用
     * @param maUser
     * @param appId
     * @return 返回登录信息
     */
    LoginUser onUserInfoReady(WxMaJscode2SessionResult session,WxMaUser maUser, String appId, WxMaLoginUser wxMaLoginUser);

    /**
     * 微信小程序手机号登录后调用
     * @param phoneNoInfo
     * @param maUser
     * @param appId
     * @param wxMaLoginUser
     * @return
     */
    LoginUser onUserInfoReady(WxMaPhoneNumberInfo phoneNoInfo, WxMaUser maUser, String appId, WxMaLoginUser wxMaLoginUser);
}
