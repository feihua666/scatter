package scatter.wxminiapp.rest.service.impl;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import scatter.common.dict.PublicDictEnums;
import scatter.common.rest.validation.DictService;
import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.wxminiapp.rest.basic.config.WxMaProperties;
import scatter.wxminiapp.rest.componentext.WxMaCityIdResolveDto;
import scatter.wxminiapp.rest.componentext.WxMaCityIdResolveParam;
import scatter.wxminiapp.rest.componentext.WxMaCityIdResolver;
import scatter.wxminiapp.rest.mapper.WxMaUserMapper;
import scatter.wxminiapp.rest.service.IWxMaUserService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.wxminiapp.pojo.form.WxMaUserAddForm;
import scatter.wxminiapp.pojo.form.WxMaUserUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaUserPageQueryForm;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * <p>
 * 小程序微信用户表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Service
@Transactional
public class WxMaUserServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<WxMaUserMapper, WxMaUser, WxMaUserAddForm, WxMaUserUpdateForm, WxMaUserPageQueryForm> implements IWxMaUserService {

    @Autowired
    private DictService dictService;
    @Autowired
    private WxMaCityIdResolver wxMaCityIdResolver;


    @Override
    public void preAdd(WxMaUserAddForm addForm,WxMaUser po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(WxMaUserUpdateForm updateForm,WxMaUser po) {
        super.preUpdate(updateForm,po);

    }


    @Override
    public WxMaUser insert(WxMaJscode2SessionResult session, WxMaProperties.MaConfig mpConfig) {

        WxMaUser maUserForInsert = new WxMaUser();
        maUserForInsert = map(session, maUserForInsert, mpConfig);
        // 用户来源
        save(maUserForInsert);
        return maUserForInsert;
    }

    @Override
    public WxMaUser insert(WxMaPhoneNumberInfo phoneNoInfo, WxMaProperties.MaConfig mpConfig) {
        WxMaUser maUserForInsert = new WxMaUser();
        maUserForInsert = map(phoneNoInfo, maUserForInsert, mpConfig);
        // 用户来源
        save(maUserForInsert);
        return maUserForInsert;
    }


    @Override
    public WxMaUser updateById(WxMaJscode2SessionResult session, WxMaProperties.MaConfig mpConfig, WxMaUser maUser) {
        Assert.notNull(maUser,"maUser不能为空");
        Assert.hasText(maUser.getId(),"maUser.id不能为空");
        maUser = map(session, maUser, mpConfig);

        updateById(maUser);
        return maUser;
    }



    /**
     * 实体转换
     * @param session
     * @param maUser
     * @param mpConfig
     * @return
     */
    private WxMaUser map(WxMaJscode2SessionResult session,WxMaUser maUser, WxMaProperties.MaConfig mpConfig) {
        maUser.setAppCode(mpConfig.getAppCode());
        maUser.setAppName(mpConfig.getAppName());

        maUser.setOpenId(session.getOpenid());
        maUser.setUnionId(session.getUnionid());

        return maUser;
    }
    /**
     * 实体转换
     * @param phoneNoInfo
     * @param maUser
     * @param mpConfig
     * @return
     */
    private WxMaUser map(WxMaPhoneNumberInfo phoneNoInfo,WxMaUser maUser, WxMaProperties.MaConfig mpConfig) {
        maUser.setAppCode(mpConfig.getAppCode());
        maUser.setAppName(mpConfig.getAppName());

        maUser.setMobile(phoneNoInfo.getPurePhoneNumber());
        maUser.setMobileCountryCode(phoneNoInfo.getCountryCode());

        return maUser;
    }
}
