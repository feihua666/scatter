package scatter.wxminiapp.rest.basic.config;

import cn.binarywang.wx.miniapp.message.WxMaMessageRouter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.wxminiapp.rest.basic.service.handler.IMaDefaultHandler;
import scatter.wxminiapp.rest.basic.service.handler.IMaLogHandler;
import scatter.wxminiapp.rest.basic.service.handler.IMaSubscribeHandler;

/**
 * Created by yangwei
 * Created at 2021/11/22 11:44
 */
@Component
public class DefaultMaHandlerConfigure implements IMaHandlerConfigure {

    @Autowired
    private IMaDefaultHandler iMaDefaultHandler;
    @Autowired
    private IMaLogHandler iMaLogHandler;
    @Autowired
    private IMaSubscribeHandler iMaSubscribeHandler;
    @Override
    public void configure(WxMaMessageRouter newRouter) {
        // 记录所有事件的日志 （异步执行）
        newRouter.rule().handler(this.iMaLogHandler).next();

        // 默认
        newRouter.rule().async(false).handler(this.iMaDefaultHandler).end();
    }
}
