package scatter.wxminiapp.rest.basic.service.impl.handler;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.message.WxMaXmlOutMessage;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import org.springframework.stereotype.Service;
import scatter.wxminiapp.rest.basic.service.handler.IMaKfCreateSessionHandler;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2021/11/22 18:39
 */
@Service
public class MaKfSessionHandlerImpl implements IMaKfCreateSessionHandler {
    @Override
    public WxMaXmlOutMessage handle(WxMaMessage wxMessage, Map<String, Object> context, WxMaService wxMaService, WxSessionManager sessionManager) throws WxErrorException {
        return null;
    }
}
