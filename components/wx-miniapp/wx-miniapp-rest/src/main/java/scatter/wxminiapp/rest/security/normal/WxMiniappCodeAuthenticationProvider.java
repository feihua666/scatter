package scatter.wxminiapp.rest.security.normal;

import cn.binarywang.wx.miniapp.api.WxMaService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 自定义一个provide目前主要为了微信登录
 * Created by yangwei
 * Created at 2021/11/22 13:37
 */

public class WxMiniappCodeAuthenticationProvider extends DaoAuthenticationProvider {

    private WxMaService wxMaService;

    @Override
    public boolean supports(Class<?> authentication) {
        return (WxMiniappCodeAuthenticationToken.class
                .isAssignableFrom(authentication));
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String appId = authentication.getPrincipal().toString();
        String code = authentication.getCredentials().toString();
        wxMaService.switchover(appId);
        // 偷梁换柱 这里把两个交换一下,因为我们后台userDetailsService是根据userName
        WxMiniappCodeAuthenticationToken wxMaCodeAuthenticationToken = new WxMiniappCodeAuthenticationToken(code, appId);
        return super.authenticate(wxMaCodeAuthenticationToken);
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        // 不需要额外验证 主要是去除密码验证
    }

    public void setWxMaService(WxMaService wxMaService) {
        this.wxMaService = wxMaService;
    }
}
