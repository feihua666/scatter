package scatter.wxminiapp.rest.security.normal;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by yangwei
 * Created at 2021/11/22 13:46
 */
public class WxMiniappCodeAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public WxMiniappCodeAuthenticationToken(Authentication authenticationToken) {
        this(authenticationToken.getPrincipal(), authenticationToken.getCredentials());
    }
    public WxMiniappCodeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public WxMiniappCodeAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
