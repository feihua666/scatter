package scatter.wxminiapp.rest.service.impl;

import scatter.common.LoginUser;
import scatter.wxminiapp.pojo.login.WxMaLoginUser;
import scatter.wxminiapp.rest.componentext.WxMiniSessionKeyResolver;

/**
 * <p>
 * 默认session key处理
 * </p>
 *
 * @author yangwei
 * @since 2022-03-22 21:00
 */
public class DefaultWxMiniSessionKeyResolverImpl implements WxMiniSessionKeyResolver {
	@Override
	public String resolveSessionKey(String appCode, LoginUser loginUser) {
		if (loginUser instanceof WxMaLoginUser) {
			return ((WxMaLoginUser) loginUser).getSessionKey();
		}

		throw new RuntimeException("可能你需要自己实现 WxMiniSessionKeyResolver 来获取sessionKey");
	}
}
