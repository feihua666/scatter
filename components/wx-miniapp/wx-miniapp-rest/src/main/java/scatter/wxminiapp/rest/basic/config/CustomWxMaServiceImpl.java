package scatter.wxminiapp.rest.basic.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.config.WxMaConfig;
import cn.hutool.json.JSONUtil;
import com.google.gson.JsonObject;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.util.json.GsonParser;
import org.apache.commons.lang3.StringUtils;

/**
 * 重写了几个方法，目的是兼容appCode，建议用appCode替代appId做为参数，这样换了小程序不影响配置或代码
 * Created by yangwei
 * Created at 2021/11/22 12:49
 */
public class CustomWxMaServiceImpl extends WxMaServiceImpl {

    /**
     * 获取手机号
     * https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=%s
     * access_token 部分微信包中自动处理了
     */
    private static String phone_numer_url = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=%s";

    private WxMaProperties mpProperties;

    public CustomWxMaServiceImpl(WxMaProperties mpProperties){
        this.mpProperties = mpProperties;
    }

    @Override
    public void addConfig(String miniappId, WxMaConfig configStorages) {
        super.addConfig(miniappId, configStorages);
    }


    @Override
    public void removeConfig(String mpId) {
        super.removeConfig(getByAppCode(mpId));
    }

    @Override
    public WxMaService switchoverTo(String mpId) {
        return super.switchoverTo(getByAppCode(mpId));
    }

    @Override
    public boolean switchover(String mpId) {
        return super.switchover(getByAppCode(mpId));
    }

    /**
     * 根据appCode查找配置的appId,如果没有找到，返回appCode
     * @param appCode
     * @return 返回appCode如果没有找到，否则返回appId
     */
    private String getByAppCode(String appCode){

        if (mpProperties.getConfigs() != null) {
            return mpProperties.getConfigs().stream()
                    .filter(item -> StringUtils.equals(appCode, item.getAppCode()))
                    .map(WxMaProperties.MaConfig::getAppId)
                    .findFirst().orElse(appCode);
        }

        return appCode;
    }

    /**
     * 获取手机号信息
     * 参见：https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/phonenumber/phonenumber.getPhoneNumber.html
     * 小程序新处理改了，在这里添加一个支持，区别于maUserService中的获取手机号，这个是以前的版本
     * @return
     */
    public WxMaPhoneNumberInfo getPhoneNoInfo(String code) throws WxErrorException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("code",code);
        String responseContent = post(phone_numer_url, jsonObject);
        String phone_info = GsonParser.parse(responseContent).get("phone_info").getAsString();
        return WxMaPhoneNumberInfo.fromJson(phone_info);
    }
}
