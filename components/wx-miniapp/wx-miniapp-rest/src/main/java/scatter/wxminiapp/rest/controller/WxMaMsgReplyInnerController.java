package scatter.wxminiapp.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxminiapp.rest.WxMiniappConfiguration;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.wxminiapp.rest.service.IWxMaMsgReplyService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 小程序消息回复表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@RestController
@RequestMapping(WxMiniappConfiguration.CONTROLLER_BASE_PATH + "/inner/wx-ma-msg-reply")
public class WxMaMsgReplyInnerController extends BaseInnerController<WxMaMsgReply> {
 @Autowired
 private IWxMaMsgReplyService wxMaMsgReplyService;

 public IWxMaMsgReplyService getService(){
     return wxMaMsgReplyService;
 }
}
