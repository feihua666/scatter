package scatter.wxminiapp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.wxminiapp.pojo.vo.WxMaUserVo;
import scatter.wxminiapp.pojo.form.WxMaUserAddForm;
import scatter.wxminiapp.pojo.form.WxMaUserUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaUserPageQueryForm;

/**
 * <p>
 * 小程序微信用户 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxMaUserMapStruct extends IBaseVoMapStruct<WxMaUser, WxMaUserVo>,
                                  IBaseAddFormMapStruct<WxMaUser,WxMaUserAddForm>,
                                  IBaseUpdateFormMapStruct<WxMaUser,WxMaUserUpdateForm>,
                                  IBaseQueryFormMapStruct<WxMaUser,WxMaUserPageQueryForm>{
    WxMaUserMapStruct INSTANCE = Mappers.getMapper( WxMaUserMapStruct.class );

}
