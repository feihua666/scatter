package scatter.wxminiapp.rest.basic.mvc;


import cn.binarywang.wx.miniapp.api.WxMaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.SuperController;
import scatter.wxminiapp.rest.WxMiniappConfiguration;
import scatter.wxminiapp.rest.basic.config.WxMaConfiguration;
import scatter.wxminiapp.rest.basic.config.WxMaProperties;
import scatter.wxminiapp.rest.basic.mapstruct.MaConfigWebMapper;
import scatter.wxminiapp.rest.basic.vo.MaConfigVo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 小程序配置相关接口
 * Created by yangwei
 * Created at 2021/11/22 20:48
 */
@RestController
@RequestMapping(WxMiniappConfiguration.CONTROLLER_BASE_PATH +"/wx/ma/config")
@Api(tags = "小程序配置相关接口")
public class MaConfigController extends SuperController {

    @Autowired
    private WxMaService wxMaService;
    @Autowired
    private WxMaProperties maProperties;
    @Autowired
    private MaConfigWebMapper maConfigWebMapper;

    /**
     * 小程序配置列表，主要用来下拉选择或列表查看
     * @return
     */
    @ApiOperation("小程序配置列表")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/list")
    public List<MaConfigVo> configList() {
        return maProperties.getConfigs().stream().map(maConfigWebMapper::configToVo).collect(Collectors.toList());
    }
}
