package scatter.wxminiapp.rest.basic.service.handler;

import cn.binarywang.wx.miniapp.message.WxMaMessageHandler;

/**
 * 客服接入会话handler
 * Created by yangwei
 * Created at 2021/11/22 20:24
 */
public interface IMaKfCreateSessionHandler extends WxMaMessageHandler {
}
