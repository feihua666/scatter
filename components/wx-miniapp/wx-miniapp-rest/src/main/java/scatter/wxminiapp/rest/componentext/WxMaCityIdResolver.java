package scatter.wxminiapp.rest.componentext;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 13:31
 */
public interface WxMaCityIdResolver {

	/**
	 * 根据给定的名字换取id
	 * @param param
	 * @return
	 */
	WxMaCityIdResolveDto resolve(WxMaCityIdResolveParam param);
}
