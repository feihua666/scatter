package scatter.wxminiapp.rest.basic.service.handler;

import cn.binarywang.wx.miniapp.message.WxMaMessageHandler;

/**
 * 用来打印日志的handler
 * Created by yangwei
 * Created at 2021/11/22 20:24
 */
public interface IMaLogHandler extends WxMaMessageHandler {
}
