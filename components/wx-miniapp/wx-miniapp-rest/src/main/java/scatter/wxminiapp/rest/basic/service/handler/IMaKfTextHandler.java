package scatter.wxminiapp.rest.basic.service.handler;


import cn.binarywang.wx.miniapp.message.WxMaMessageHandler;

/**
 * 客服会话handler，收到用户的文本消息处理器
 * Created by yangwei
 * Created at 2021/11/22 20:24
 */
public interface IMaKfTextHandler extends WxMaMessageHandler {
}
