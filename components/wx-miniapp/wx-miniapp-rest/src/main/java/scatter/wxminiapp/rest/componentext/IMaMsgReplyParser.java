package scatter.wxminiapp.rest.componentext;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.message.WxMaXmlOutMessage;
import me.chanjar.weixin.common.session.WxSessionManager;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;

import java.util.Map;

/**
 * 消息回复输入消息解析
 * Created by yangwei
 * Created at 2021/11/22 20:53
 */
public interface IMaMsgReplyParser {
    /**
     * 是否支持
     * @param reply
     * @return
     */
    boolean support(WxMaMsgReply reply);

    /**
     * 解析
     * @param reply
     * @param wxMessage
     * @param context
     * @param wxMaService
     * @param sessionManager
     * @return
     */
    WxMaXmlOutMessage parse(WxMaMsgReply reply, WxMaMessage wxMessage, Map<String, Object> context, WxMaService wxMaService, WxSessionManager sessionManager);
}
