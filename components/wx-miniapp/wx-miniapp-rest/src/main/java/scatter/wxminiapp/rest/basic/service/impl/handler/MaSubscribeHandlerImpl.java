package scatter.wxminiapp.rest.basic.service.impl.handler;


import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.message.WxMaXmlOutMessage;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import org.springframework.stereotype.Service;
import scatter.wxminiapp.rest.basic.service.handler.IMaSubscribeHandler;

import java.util.Map;

/**
 * 订阅处理
 * Created by yangwei
 * Created at 2021/11/22 18:43
 */
@Service
public class MaSubscribeHandlerImpl implements IMaSubscribeHandler {

    @Override
    public WxMaXmlOutMessage handle(WxMaMessage wxMessage, Map<String, Object> context, WxMaService wxMaService, WxSessionManager sessionManager) throws WxErrorException {

        return null;
    }
}
