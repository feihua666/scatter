package scatter.wxminiapp.rest.basic.service.handler;

import cn.binarywang.wx.miniapp.message.WxMaMessageHandler;

/**
 * 默认的处理handler
 * Created by yangwei
 * Created at 2021/11/22 20:24
 */
public interface IMaDefaultHandler extends WxMaMessageHandler {
}
