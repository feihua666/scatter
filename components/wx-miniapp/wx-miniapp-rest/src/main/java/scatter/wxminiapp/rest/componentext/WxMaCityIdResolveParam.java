package scatter.wxminiapp.rest.componentext;

import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.param.BaseParam;

/**
 * <p>
 * 根据这名称，解决他们的id问题
 * </p>
 *
 * @author yangwei
 * @since 2021-11-05 13:34
 */
@Getter
@Setter
public class WxMaCityIdResolveParam extends BaseParam {

	/**
	 * 区id
	 */
	private String districtName;
	/**
	 * 城市id
	 */
	private String cityName;
	/**
	 * 省id
	 */
	private String provinceName;
	/**
	 * 国家名称
	 */
	private String countryName;
}
