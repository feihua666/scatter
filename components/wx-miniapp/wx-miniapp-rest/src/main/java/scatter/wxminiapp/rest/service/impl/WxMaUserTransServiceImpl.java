package scatter.wxminiapp.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.wxminiapp.rest.service.IWxMaUserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 小程序微信用户翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Component
public class WxMaUserTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IWxMaUserService wxMaUserService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,WxMaUser.TRANS_WXMAUSER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,WxMaUser.TRANS_WXMAUSER_BY_ID)) {
            WxMaUser byId = wxMaUserService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,WxMaUser.TRANS_WXMAUSER_BY_ID)) {
            return wxMaUserService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
