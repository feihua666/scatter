package scatter.wxminiapp.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import scatter.area.rest.service.IAreaService;
import scatter.wxminiapp.rest.basic.config.WxMaConfiguration;
import scatter.wxminiapp.rest.componentimpl.DefaultWxMaCityIdResolverImpl;
import scatter.wxminiapp.rest.security.WxMiniappWebSecurityConfig;
import scatter.wxminiapp.rest.service.impl.DefaultMaMsgReplyParserImpl;
import scatter.wxminiapp.rest.service.impl.DefaultWxMiniSessionKeyResolverImpl;

/**
 * <p>
 * 微信小程序组件配置入口类
 * </p>
 *
 * @author yangwei
 * @since 2021/11/22
 */
@ComponentScan
@Import({WxMaConfiguration.class, WxMiniappWebSecurityConfig.class})
@MapperScan("scatter.wxminiapp.rest.**.mapper")
public class WxMiniappConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "wxminiapp";

    @Bean
    @ConditionalOnMissingBean
    public DefaultMaMsgReplyParserImpl maMsgReplyParser(){
        return new DefaultMaMsgReplyParserImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(IAreaService.class)
    public DefaultWxMaCityIdResolverImpl defaultWxMaCityIdResolver(){
        return new DefaultWxMaCityIdResolverImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public DefaultWxMiniSessionKeyResolverImpl defaultWxMiniSessionKeyResolver(){
        return new DefaultWxMiniSessionKeyResolverImpl();
    }
}
