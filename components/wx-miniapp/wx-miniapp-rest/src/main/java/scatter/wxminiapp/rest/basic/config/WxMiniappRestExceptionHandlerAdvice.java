package scatter.wxminiapp.rest.basic.config;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import scatter.common.pojo.vo.ErrorVo;
import scatter.common.rest.advise.GlobalExceptionAdvice;
import scatter.common.rest.tools.InterfaceTool;

import javax.servlet.http.HttpServletRequest;

/**
 * 统一异常处理
 * Created by yangwei
 * Created at 2021/11/22 11:42
 */
@RestControllerAdvice
@Slf4j
@Order(8)
public class WxMiniappRestExceptionHandlerAdvice implements InterfaceTool {

    /**
     * 微信请求出现的异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(WxErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorVo handleWxErrorException(HttpServletRequest request, WxErrorException ex) {
        return GlobalExceptionAdvice.createRM(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getError().getErrorMsg(), ex.getError(), ex);
    }
}
