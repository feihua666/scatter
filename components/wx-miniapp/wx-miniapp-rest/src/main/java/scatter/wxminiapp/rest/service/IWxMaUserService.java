package scatter.wxminiapp.rest.service;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import org.springframework.util.Assert;
import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.common.rest.service.IBaseService;
import scatter.wxminiapp.pojo.form.WxMaUserAddForm;
import scatter.wxminiapp.pojo.form.WxMaUserUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaUserPageQueryForm;
import scatter.wxminiapp.rest.basic.config.WxMaProperties;

import java.util.List;
/**
 * <p>
 * 小程序微信用户表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
public interface IWxMaUserService extends IBaseService<WxMaUser> {

	/**
	 * 根据openId和appCode判断是否存在
	 * @param openId
	 * @param appCode
	 * @return
	 */
	default boolean existByOpenidAndAppCode(String openId,String appCode) {
		return getByOpenidAndAppCode(openId,appCode) != null;
	}

	/**
	 * 根据openId和appCode获取
	 * @param openId
	 * @param appCode
	 * @return
	 */
	default WxMaUser getByOpenidAndAppCode(String openId,String appCode) {
		Assert.hasText(openId,"openId不能为空");
		Assert.hasText(appCode,"appCode不能为空");
		return getOne(Wrappers.<WxMaUser>lambdaQuery().eq(WxMaUser::getAppCode, appCode).eq(WxMaUser::getOpenId, openId));
	}

	/**
	 * 根据 mobile 和appCode获取
	 * @param mobile
	 * @param appCode
	 * @return
	 */
	default WxMaUser getByMobileAndAppCode(String mobile,String appCode) {
		Assert.hasText(mobile,"mobile 不能为空");
		Assert.hasText(appCode,"appCode 不能为空");
		return getOne(Wrappers.<WxMaUser>lambdaQuery().eq(WxMaUser::getAppCode, appCode).eq(WxMaUser::getMobile, mobile));
	}

	/**
	 * 添加用户，根据session
	 * @param session
	 * @param mpConfig
	 * @return
	 */
	WxMaUser insert(WxMaJscode2SessionResult session, WxMaProperties.MaConfig mpConfig);

	/**
	 * 添加用户，根据手机号
	 * @param phoneNoInfo
	 * @param mpConfig
	 * @return
	 */
	WxMaUser insert(WxMaPhoneNumberInfo phoneNoInfo, WxMaProperties.MaConfig mpConfig);
	/**
	 * 更新用户
	 * @param session
	 * @param mpConfig
	 * @param mpUser
	 * @return
	 */
	WxMaUser updateById(WxMaJscode2SessionResult session, WxMaProperties.MaConfig mpConfig, WxMaUser mpUser);
}
