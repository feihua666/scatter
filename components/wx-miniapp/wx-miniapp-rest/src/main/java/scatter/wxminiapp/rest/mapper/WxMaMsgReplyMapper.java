package scatter.wxminiapp.rest.mapper;

import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 小程序消息回复表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
public interface WxMaMsgReplyMapper extends IBaseMapper<WxMaMsgReply> {

}
