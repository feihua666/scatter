package scatter.wxminiapp.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxminiapp.rest.WxMiniappConfiguration;
import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.wxminiapp.rest.service.IWxMaUserService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 小程序微信用户表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@RestController
@RequestMapping(WxMiniappConfiguration.CONTROLLER_BASE_PATH + "/inner/wx-ma-user")
public class WxMaUserInnerController extends BaseInnerController<WxMaUser> {
 @Autowired
 private IWxMaUserService wxMaUserService;

 public IWxMaUserService getService(){
     return wxMaUserService;
 }
}
