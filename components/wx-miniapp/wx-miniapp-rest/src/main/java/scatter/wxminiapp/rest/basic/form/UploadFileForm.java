package scatter.wxminiapp.rest.basic.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotNull;

/**
 * Created by yangwei
 * Created at 2021/11/22 15:21
 */
@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel("文件上传表单")
public class UploadFileForm extends BaseForm {

    @NotNull(message = "上传文件不能为空")
    @ApiModelProperty(value = "上传的文件",required = true,dataType = "org.springframework.web.multipart.MultipartFile")
    private MultipartFile file;
}
