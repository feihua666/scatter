package scatter.wxminiapp.rest.basic.service.impl.handler;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.message.WxMaXmlOutMessage;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import org.springframework.stereotype.Service;
import scatter.wxminiapp.rest.basic.service.handler.IMaLogHandler;

import java.util.Map;

/**
 * 只是一个打印日志handler
 * Created by yangwei
 * Created at 2021/11/22 18:40
 */
@Slf4j
@Service
public class MaLogHandlerImpl implements IMaLogHandler {
    @Override
    public WxMaXmlOutMessage handle(WxMaMessage wxMessage, Map<String, Object> context, WxMaService wxMaService, WxSessionManager sessionManager) throws WxErrorException {
        log.info("wxMessage={},wxMessage={}", JSONUtil.toJsonStr(wxMessage),context);
        return null;
    }
}
