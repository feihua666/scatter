package scatter.wxminiapp.rest.basic.mapstruct;


import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.wxminiapp.rest.basic.config.WxMaProperties;
import scatter.wxminiapp.rest.basic.vo.MaConfigVo;

/**
 * Created by yangwei
 * Created at 2021/11/22 12:41
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MaConfigWebMapper {
    MaConfigWebMapper INSTANCE = Mappers.getMapper( MaConfigWebMapper.class );
    MaConfigVo configToVo(WxMaProperties.MaConfig config);
}
