package scatter.wxminiapp.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.wxminiapp.rest.WxMiniappConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.wxminiapp.pojo.vo.WxMaMsgReplyVo;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyAddForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyPageQueryForm;
import scatter.wxminiapp.rest.service.IWxMaMsgReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 小程序消息回复表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Api(tags = "小程序消息回复相关接口")
@RestController
@RequestMapping(WxMiniappConfiguration.CONTROLLER_BASE_PATH + "/wx-ma-msg-reply")
public class WxMaMsgReplyController extends BaseAddUpdateQueryFormController<WxMaMsgReply, WxMaMsgReplyVo, WxMaMsgReplyAddForm, WxMaMsgReplyUpdateForm, WxMaMsgReplyPageQueryForm> {
    @Autowired
    private IWxMaMsgReplyService iWxMaMsgReplyService;

    @ApiOperation("添加小程序消息回复")
    @PreAuthorize("hasAuthority('WxMaMsgReply:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public WxMaMsgReplyVo add(@RequestBody @Valid WxMaMsgReplyAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询小程序消息回复")
    @PreAuthorize("hasAuthority('WxMaMsgReply:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public WxMaMsgReplyVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除小程序消息回复")
    @PreAuthorize("hasAuthority('WxMaMsgReply:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新小程序消息回复")
    @PreAuthorize("hasAuthority('WxMaMsgReply:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public WxMaMsgReplyVo update(@RequestBody @Valid WxMaMsgReplyUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询小程序消息回复")
    @PreAuthorize("hasAuthority('WxMaMsgReply:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<WxMaMsgReplyVo> getList(WxMaMsgReplyPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询小程序消息回复")
    @PreAuthorize("hasAuthority('WxMaMsgReply:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<WxMaMsgReplyVo> getPage(WxMaMsgReplyPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
