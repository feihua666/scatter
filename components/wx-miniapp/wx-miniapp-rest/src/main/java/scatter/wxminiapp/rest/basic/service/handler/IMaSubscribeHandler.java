package scatter.wxminiapp.rest.basic.service.handler;


import cn.binarywang.wx.miniapp.message.WxMaMessageHandler;

/**
 * 订阅消息
 * 什么也不做的handler
 * Created by yangwei
 * Created at 2021/11/22 20:24
 */
public interface IMaSubscribeHandler extends WxMaMessageHandler {
}
