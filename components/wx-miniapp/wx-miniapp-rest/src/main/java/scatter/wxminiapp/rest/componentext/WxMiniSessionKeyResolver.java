package scatter.wxminiapp.rest.componentext;

import scatter.common.LoginUser;

/**
 * <p>
 * sessionKey处理
 * 添加这个主要是为了获取手机号使用，方便操作sessionKey处理
 * </p>
 *
 * @author yangwei
 * @since 2022-03-22 20:58
 */
public interface WxMiniSessionKeyResolver {

	String resolveSessionKey(String appCode, LoginUser loginUser);
}
