package scatter.wxminiapp.rest.componentext;

import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import scatter.common.LoginUser;

/**
 * <p>
 * 获取到微信手机号后调用
 * </p>
 *
 * @author yangwei
 * @since 2022-03-22 20:50
 */
public interface WxMiniPhoneNoInfoListener {

	void phoneNoInfoReady(WxMaPhoneNumberInfo phoneNoInfo, LoginUser loginUser);
}
