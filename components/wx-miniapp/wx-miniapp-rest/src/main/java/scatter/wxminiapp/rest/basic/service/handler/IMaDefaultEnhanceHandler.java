package scatter.wxminiapp.rest.basic.service.handler;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.message.WxMaXmlOutMessage;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2021/11/22 12:28
 */
public interface IMaDefaultEnhanceHandler {

    /**
     * 是否支持该handler处理
     * @param wxMessage
     * @param context
     * @param wxMaService
     * @param sessionManager
     * @return
     */
    boolean support(WxMaMessage wxMessage,
                    Map<String, Object> context,
                    WxMaService wxMaService,
                    WxSessionManager sessionManager);

    /**
     * 处理微信推送消息.
     *
     * @param wxMessage      微信推送消息
     * @param context        上下文，如果handler或interceptor之间有信息要传递，可以用这个
     * @param wxMaService    服务类
     * @param sessionManager session管理器
     * @return xml格式的消息，如果在异步规则里处理的话，可以返回null
     * @throws WxErrorException 异常
     */
    WxMaXmlOutMessage handle(WxMaMessage wxMessage,
                             Map<String, Object> context,
                             WxMaService wxMaService,
                             WxSessionManager sessionManager) throws WxErrorException;
}
