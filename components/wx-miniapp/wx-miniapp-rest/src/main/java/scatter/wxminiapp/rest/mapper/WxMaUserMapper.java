package scatter.wxminiapp.rest.mapper;

import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 小程序微信用户表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
public interface WxMaUserMapper extends IBaseMapper<WxMaUser> {

}
