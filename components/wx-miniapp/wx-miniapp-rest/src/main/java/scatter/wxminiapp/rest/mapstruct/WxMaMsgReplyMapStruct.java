package scatter.wxminiapp.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.wxminiapp.pojo.vo.WxMaMsgReplyVo;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyAddForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyPageQueryForm;

/**
 * <p>
 * 小程序消息回复 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxMaMsgReplyMapStruct extends IBaseVoMapStruct<WxMaMsgReply, WxMaMsgReplyVo>,
                                  IBaseAddFormMapStruct<WxMaMsgReply,WxMaMsgReplyAddForm>,
                                  IBaseUpdateFormMapStruct<WxMaMsgReply,WxMaMsgReplyUpdateForm>,
                                  IBaseQueryFormMapStruct<WxMaMsgReply,WxMaMsgReplyPageQueryForm>{
    WxMaMsgReplyMapStruct INSTANCE = Mappers.getMapper( WxMaMsgReplyMapStruct.class );

}
