package scatter.wxminiapp.rest.security.mobile;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by yangwei
 * Created at 2022/3/22 13:46
 */
public class WxMiniappMobileAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public WxMiniappMobileAuthenticationToken(Authentication authenticationToken) {
        this(authenticationToken.getPrincipal(), authenticationToken.getCredentials());
    }
    public WxMiniappMobileAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public WxMiniappMobileAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
