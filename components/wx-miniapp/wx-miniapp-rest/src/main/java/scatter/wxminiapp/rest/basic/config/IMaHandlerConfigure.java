package scatter.wxminiapp.rest.basic.config;

import cn.binarywang.wx.miniapp.message.WxMaMessageRouter;

/**
 * Created by yangwei
 * Created at 2021/11/22 11:33
 */
public interface IMaHandlerConfigure {

    public void configure(WxMaMessageRouter newRouter);
}
