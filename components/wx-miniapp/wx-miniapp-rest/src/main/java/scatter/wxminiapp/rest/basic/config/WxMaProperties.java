package scatter.wxminiapp.rest.basic.config;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by yangwei
 * Created at 2021/11/22 20:01
 */
@Data
@Component
@ConfigurationProperties(prefix = "scatter.wx.miniapp")
public class WxMaProperties {

    private List<MaConfig> configs;

    @Data
    public static class MaConfig {
        /**
         * 自定义编码
         */
        private String appCode;
        /**
         * 自定义名称
         */
        private String appName;


        /**
         * 设置微信小程序的appId
         */
        private String appId;

        /**
         * 设置微信小程序的app secret
         */
        private String secret;

        /**
         * 设置微信小程序的token
         */
        private String token;

        /**
         * 设置微信小程序的EncodingAESKey
         */
        private String aesKey;
        /**
         * 消息格式，XML或者JSON
         */
        private String msgDataFormat;

    }

    /**
     * 获取appname
     * @param appCodeOrAppId
     * @return
     */
    public String abtainAppNameByAppCodeOrAppId(String appCodeOrAppId){
        return Optional.ofNullable(abtainConfigByAppCodeOrAppId(appCodeOrAppId)).map(MaConfig::getAppName).orElse(null);
    }
    /**
     * 获取appname
     * @param appCodeOrAppId
     * @return
     */
    public MaConfig abtainConfigByAppCodeOrAppId(String appCodeOrAppId){
        return this.getConfigs().stream()
                .filter(item -> StringUtils.equals(appCodeOrAppId, item.getAppCode()) || StringUtils.equals(appCodeOrAppId, item.getAppId()))
                .findFirst()
                .orElse(null);
    }
}
