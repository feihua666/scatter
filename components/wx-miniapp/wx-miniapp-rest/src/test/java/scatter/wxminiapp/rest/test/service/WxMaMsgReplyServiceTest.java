package scatter.wxminiapp.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyAddForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyPageQueryForm;
import scatter.wxminiapp.rest.test.WxMaMsgReplySuperTest;
import scatter.wxminiapp.rest.service.IWxMaMsgReplyService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 小程序消息回复 服务测试类
* </p>
*
* @author yw
* @since 2021-11-23
*/
@SpringBootTest
public class WxMaMsgReplyServiceTest extends WxMaMsgReplySuperTest{

    @Autowired
    private IWxMaMsgReplyService wxMaMsgReplyService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<WxMaMsgReply> pos = wxMaMsgReplyService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}