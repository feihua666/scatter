package scatter.wxminiapp.rest.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import scatter.common.rest.config.CommonRestConfig;
import scatter.wxminiapp.rest.WxMiniappConfiguration;

/**
 * Created by yangwei
 * Created at 2021/11/22 10:38
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Import({CommonRestConfig.class, WxMiniappConfiguration.class})
@MapperScan("scatter.wxminiapp.rest.mapper")
public class WxMiniappTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(WxMiniappTestApplication.class, args);
    }
}
