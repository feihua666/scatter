package scatter.wxminiapp.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxminiapp.rest.test.WxMaUserSuperTest;
/**
* <p>
* 小程序微信用户 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-11-23
*/
@SpringBootTest
public class WxMaUserInnerControllerTest extends WxMaUserSuperTest{
    @Test
    void contextLoads() {
    }
}