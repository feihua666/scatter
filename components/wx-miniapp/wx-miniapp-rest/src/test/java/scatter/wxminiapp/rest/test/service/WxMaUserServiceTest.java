package scatter.wxminiapp.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.wxminiapp.pojo.po.WxMaUser;
import scatter.wxminiapp.pojo.form.WxMaUserAddForm;
import scatter.wxminiapp.pojo.form.WxMaUserUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaUserPageQueryForm;
import scatter.wxminiapp.rest.test.WxMaUserSuperTest;
import scatter.wxminiapp.rest.service.IWxMaUserService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 小程序微信用户 服务测试类
* </p>
*
* @author yw
* @since 2021-11-23
*/
@SpringBootTest
public class WxMaUserServiceTest extends WxMaUserSuperTest{

    @Autowired
    private IWxMaUserService wxMaUserService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<WxMaUser> pos = wxMaUserService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}