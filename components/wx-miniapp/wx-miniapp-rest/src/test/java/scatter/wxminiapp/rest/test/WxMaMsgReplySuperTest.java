package scatter.wxminiapp.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.wxminiapp.pojo.po.WxMaMsgReply;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyAddForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyUpdateForm;
import scatter.wxminiapp.pojo.form.WxMaMsgReplyPageQueryForm;
import scatter.wxminiapp.rest.service.IWxMaMsgReplyService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 小程序消息回复 测试类基类
* </p>
*
* @author yw
* @since 2021-11-23
*/
public class WxMaMsgReplySuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IWxMaMsgReplyService wxMaMsgReplyService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return wxMaMsgReplyService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return wxMaMsgReplyService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public WxMaMsgReply mockPo() {
        return JMockData.mock(WxMaMsgReply.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public WxMaMsgReplyAddForm mockAddForm() {
        return JMockData.mock(WxMaMsgReplyAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public WxMaMsgReplyUpdateForm mockUpdateForm() {
        return JMockData.mock(WxMaMsgReplyUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public WxMaMsgReplyPageQueryForm mockPageQueryForm() {
        return JMockData.mock(WxMaMsgReplyPageQueryForm.class, mockConfig);
    }
}