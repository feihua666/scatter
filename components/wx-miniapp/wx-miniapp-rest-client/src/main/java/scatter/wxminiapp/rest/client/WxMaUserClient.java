package scatter.wxminiapp.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 小程序微信用户表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-23
 */
@Component
@FeignClient(value = "WxMaUser-client")
public interface WxMaUserClient {

}
