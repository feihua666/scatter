import UserForm from './UserForm.js'
import UserTable from './UserTable.js'
import UserUrl from './UserUrl.js'

const UserMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserForm,this.$options.name)
        },
        computedTableOptions() {
            return UserTable
        },
        computedUrl(){
            return UserUrl
        }
    },
}
export default UserMixin