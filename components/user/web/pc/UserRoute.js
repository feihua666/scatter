import UserUrl from './UserUrl.js'

const UserRoute = [
    {
        path: UserUrl.router.searchList,
        component: () => import('./element/UserSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'UserSearchList',
            name: '后台管理用户管理'
        }
    },
    {
        path: UserUrl.router.add,
        component: () => import('./element/UserAdd'),
        meta: {
            code:'UserAdd',
            name: '后台管理用户添加'
        }
    },
    {
        path: UserUrl.router.update,
        component: () => import('./element/UserUpdate'),
        meta: {
            code:'UserUpdate',
            name: '后台管理用户修改'
        }
    },
]
export default UserRoute