const UserTable = [
    {
        prop: 'nickname',
        label: '昵称'
    },
    {
        prop: 'genderDictName',
        label: '性别'
    },
    {
        prop: 'avatar',
        label: '头像',
        stype: 'image'
    },
    {
        prop: 'serialNo',
        label: '用户编号'
    },
    {
        prop: 'compName',
        label: '公司'
    },
    {
        prop: 'deptName',
        label: '部门'
    },
    {
        prop: 'isVirtual',
        label: '是否虚拟用户'
    },
    {
        prop: 'isLock',
        label: '锁定状态'
    },
    {
        prop: 'lockReason',
        label: '锁定原因'
    },
    {
        prop: 'categoryDictName',
        label: '用户分类'
    },
    {
        prop: 'sourceFromDictName',
        label: '用户来源'
    },
]
export default UserTable