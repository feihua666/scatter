import CompUrl from '../../../comp/web/pc/CompUrl.js'
import DeptUrl from '../../../dept/web/pc/DeptUrl.js'
const UserForm = [

    {
        UserSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'nickname'
        },
        element:{
            label: '昵称',
            required: true,
        }
    },
    {
        field: {
            name: 'genderDictId'
        },
        element:{
            label: '性别',
            type: 'selectDict',
            options: {
                groupCode: 'gender'
            }
        }
    },

    {
        field: {
            name: 'serialNo'
        },
        element:{
            label: '用户编号',
        }
    },
    {
        UserSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'compId'
        },
        element:{
            label: '公司',
            required: true,
            type: 'cascader',
            options: {
                datas: CompUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false,
                },
            },
        }
    },
    {
        UserSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'deptId'
        },
        element:{
            label: '部门',
            required: true,
            type: 'cascader',
            options: {
                datas: DeptUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false,
                },
            },
        }
    },
    {

        UserSearchList: false,
        field: {
            name: 'isVirtual',
            valueDefault: false
        },
        element:{
            label: '是否虚拟用户',
            required: true,
            type: 'switch'
        }
    },
    {

        UserSearchList: false,
        field: {
            name: 'isLock',
            valueDefault: false
        },
        element:{
            label: '锁定状态',
            required: true,
            type: 'switch'
        }
    },
    {
        UserSearchList: false,
        field: {
            name: 'lockReason'
        },
        element:{
            label: '锁定原因',
            required: ({form})=>{
                return form.isLock
            }
        }
    },
    {
        UserSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'categoryDictId'
        },
        element:{
            label: '用户分类',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'user_category'
            }
        }
    },
    {
        UserSearchList: {
            element:{
                required: false,
            }
        },
        field: {
            name: 'sourceFromDictId'
        },
        element:{
            label: '用户来源',
            type: 'selectDict',
            options: {
                groupCode: 'user_source_from'
            }
        }
    },
]
export default UserForm