const basePath = '' + '/user'
const UserUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserSearchList',
        add: '/UserAdd',
        update: '/UserUpdate',
    }
}
export default UserUrl