package scatter.user.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.comp.pojo.po.Comp;
import scatter.dept.pojo.po.Dept;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 后台管理用户响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="后台管理用户响应对象")
public class UserVo extends BaseIdVo {

    @ApiModelProperty(value = "昵称，姓名，模糊查询")
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "genderDictId",mapValueField="name")
    @ApiModelProperty(value = "类型，字典名称")
    private String genderDictName;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @ApiModelProperty(value = "用户编号，可以做为员工编号")
    private String serialNo;

    @ApiModelProperty(value = "公司id，冗余字段，由dept_id对应公司派生")
    private String compId;

    @TransBy(type = Comp.TRANS_COMP_BY_ID,byFieldName = "compId",mapValueField="name")
    @ApiModelProperty(value = "归属公司名称")
    private String compName;

    @ApiModelProperty(value = "部门id")
    private String deptId;


    @TransBy(type = Dept.TRANS_DEPT_BY_ID,byFieldName = "deptId",mapValueField="name")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "是否虚拟用户，虚拟用户代表不是一个真正存在的用户")
    private Boolean isVirtual;

    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定")
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @ApiModelProperty(value = "用户分类字典，标识是哪一类用户，比如后台用户等")
    private String categoryDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "categoryDictId",mapValueField="name")
    @ApiModelProperty(value = "用户分类，字典名称")
    private String categoryDictName;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceFromDictId",mapValueField="name")
    @ApiModelProperty(value = "用户来源，字典名称")
    private String sourceFromDictName;
}
