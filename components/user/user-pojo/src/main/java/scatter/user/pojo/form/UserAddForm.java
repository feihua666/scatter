package scatter.user.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 后台管理用户添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="后台管理用户添加表单对象")
public class UserAddForm extends BaseAddForm {

    @NotEmpty(message="昵称不能为空")
    @ApiModelProperty(value = "昵称，姓名，模糊查询",required = true)
    private String nickname;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "头像，图片绝对路径")
    private String avatar;

    @ApiModelProperty(value = "用户编号，可以做为员工编号")
    private String serialNo;

    @NotEmpty(message="公司id不能为空")
    @ApiModelProperty(value = "公司id，冗余字段，由dept_id对应公司派生",required = true)
    private String compId;

    @NotEmpty(message="部门id不能为空")
    @ApiModelProperty(value = "部门id",required = true)
    private String deptId;

    @NotNull(message="是否虚拟用户不能为空")
    @ApiModelProperty(value = "是否虚拟用户，虚拟用户代表不是一个真正存在的用户",required = true)
    private Boolean isVirtual;

    @NotNull(message="锁定状态不能为空")
    @ApiModelProperty(value = "锁定状态，0=未锁定；1=锁定",required = true)
    private Boolean isLock;

    @ApiModelProperty(value = "锁定原因")
    private String lockReason;

    @NotEmpty(message="用户分类字典不能为空")
    @ApiModelProperty(value = "用户分类字典，标识是哪一类用户，比如后台用户等",required = true)
    private String categoryDictId;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

}
