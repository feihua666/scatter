package scatter.user.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.user.pojo.po.User;
import scatter.user.pojo.form.UserAddForm;
import scatter.user.pojo.form.UserUpdateForm;
import scatter.user.pojo.form.UserPageQueryForm;
import scatter.user.rest.test.UserSuperTest;
import scatter.user.rest.service.IUserService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 后台管理用户 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserServiceTest extends UserSuperTest{

    @Autowired
    private IUserService userService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<User> pos = userService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }

    /**
     * 测试mybatis plus id手动输入是否有效
     */
    @Test
    void idTest(){
        User user = new User();
        user.setId("555");
        user.setNickname("555");
        user.setCompId("555");
        user.setDeptId("555");
        user.setIsVirtual(true);
        user.setIsLock(true);
        user.setCategoryDictId("555");
        userService.save(user);
        User byId = userService.getById("555");
        Assertions.assertEquals("555",byId.getId());

    }
    @Test
    void idsTest(){
        User user = new User();
        user.setId("333");
        user.setNickname("333");
        user.setCompId("333");
        user.setDeptId("333");
        user.setIsVirtual(true);
        user.setIsLock(true);
        user.setCategoryDictId("333");

        User user1 = new User();
        user1.setId("444");
        user1.setNickname("444");
        user1.setCompId("444");
        user1.setDeptId("444");
        user1.setIsVirtual(true);
        user1.setIsLock(true);
        user1.setCategoryDictId("444");

        userService.saveBatch(userService.newArrayList(user,user1));
        User byId = userService.getById("333");
        Assertions.assertEquals("333",byId.getId());
        User byId1 = userService.getById("444");
        Assertions.assertEquals("444",byId1.getId());

    }
}