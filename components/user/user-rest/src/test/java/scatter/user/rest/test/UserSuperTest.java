package scatter.user.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.user.pojo.po.User;
import scatter.user.pojo.form.UserAddForm;
import scatter.user.pojo.form.UserUpdateForm;
import scatter.user.pojo.form.UserPageQueryForm;
import scatter.user.rest.service.IUserService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 后台管理用户 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserService userService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public User mockPo() {
        return JMockData.mock(User.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserAddForm mockAddForm() {
        return JMockData.mock(UserAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserUpdateForm mockUpdateForm() {
        return JMockData.mock(UserUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserPageQueryForm.class, mockConfig);
    }
}