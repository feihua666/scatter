package scatter.user.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.dataconstraint.DataConstraint;
import scatter.user.rest.UserConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.user.pojo.po.User;
import scatter.user.pojo.vo.UserVo;
import scatter.user.pojo.form.UserAddForm;
import scatter.user.pojo.form.UserUpdateForm;
import scatter.user.pojo.form.UserPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 后台管理用户表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@DataConstraint(doc = "user",name = "用户")
@RestController
@RequestMapping(UserConfiguration.CONTROLLER_BASE_PATH + "/user")
@Api(tags = "后台管理用户")
public class UserController extends BaseAddUpdateQueryFormController<User, UserVo, UserAddForm, UserUpdateForm, UserPageQueryForm> {


     @Override
	 @ApiOperation("添加后台管理用户")
     @PreAuthorize("hasAuthority('User:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserVo add(@RequestBody @Valid UserAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询后台管理用户")
     @PreAuthorize("hasAuthority('User:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除后台管理用户")
     @PreAuthorize("hasAuthority('User:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新后台管理用户")
     @PreAuthorize("hasAuthority('User:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserVo update(@RequestBody @Valid UserUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询后台管理用户")
    @PreAuthorize("hasAuthority('User:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserVo> getList(UserPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询后台管理用户")
    @PreAuthorize("hasAuthority('User:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserVo> getPage(UserPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
