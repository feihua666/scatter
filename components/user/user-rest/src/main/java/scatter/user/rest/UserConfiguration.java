package scatter.user.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.user.rest.ext.UserLoginUserExtPutServiceImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.user.rest.**.mapper")
public class UserConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "user";

    /**
     * 添加登录用户额外信息
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public UserLoginUserExtPutServiceImpl userLoginUserExtPutServiceImpl() {
        return new UserLoginUserExtPutServiceImpl();
    }
}
