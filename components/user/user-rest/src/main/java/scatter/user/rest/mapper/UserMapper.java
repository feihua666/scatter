package scatter.user.rest.mapper;

import scatter.user.pojo.po.User;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 后台管理用户表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserMapper extends IBaseMapper<User> {

}
