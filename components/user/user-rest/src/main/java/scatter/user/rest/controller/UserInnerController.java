package scatter.user.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.user.pojo.po.User;
import scatter.user.rest.service.IUserService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 后台管理用户表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user")
public class UserInnerController extends BaseInnerController<User> {
 @Autowired
 private IUserService userService;

 public IUserService getService(){
     return userService;
 }
}
