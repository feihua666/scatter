package scatter.user.rest.ext;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.LoginUser;
import scatter.common.rest.security.LoginUserExtPutService;
import scatter.user.pojo.po.User;
import scatter.user.rest.service.IUserService;

/** 添加
 * Created by yangwei
 * Created at 2021/2/8 11:09
 */
@Slf4j
public class UserLoginUserExtPutServiceImpl implements LoginUserExtPutService {

    public static String login_user_ext_key_user = "user";

    @Autowired
    private IUserService iUserService;

    @Override
    public void addExt(LoginUser user) {
        User byId = iUserService.getById(user.getId());
        if (byId == null) {
            log.warn("用户登录填充额外信息失败，因为未根据 userId 获取到 User 实体，userId={}",user.getId());
            return;
        }
        user.setAvatar(byId.getAvatar());
        user.setNickname(byId.getNickname());
        user.addExt(login_user_ext_key_user,byId);
    }
}
