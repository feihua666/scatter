package scatter.user.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.common.rest.validation.DictService;
import scatter.common.trans.TransConstants;
import scatter.common.trans.UserForTrans;
import scatter.user.pojo.po.User;
import scatter.user.rest.service.IUserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 后台管理用户翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private DictService dictService;
    @Autowired
    private IUserService userService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,User.TRANS_USER_BY_ID, TransConstants.TRANS_USER_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,User.TRANS_USER_BY_ID)) {
            User byId = userService.getById(key);
            return new TransResult(byId,key);
        }
        if (isEqual(type, TransConstants.TRANS_USER_BY_ID)) {
            User byId = userService.getById(key);
            return new TransResult(userMapUserForTrans(byId),key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,User.TRANS_USER_BY_ID)) {
            return userService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        if (isEqual(type,TransConstants.TRANS_USER_BY_ID)) {
            return userService.listByIds(keys).stream().map(item->new TransResult<Object, String>(userMapUserForTrans(item),item.getId())).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * 转换实体
     * @param user
     * @return
     */
    private UserForTrans userMapUserForTrans(User user){
        UserForTrans userForTrans = new UserForTrans();
        userForTrans.setId(user.getId());
        // user 不支持真实姓名
        userForTrans.setName(null);
        userForTrans.setNickname(user.getNickname());
        userForTrans.setAvatar(user.getAvatar());
        String valueById = dictService.getValueById(user.getGenderDictId());
        String nameById = dictService.getNameById(user.getGenderDictId());
        userForTrans.setGenderName(nameById);
        userForTrans.setGenderValue(valueById);
        return userForTrans;
    }
}
