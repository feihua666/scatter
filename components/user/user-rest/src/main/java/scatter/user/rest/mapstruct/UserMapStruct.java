package scatter.user.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.user.pojo.form.UserAddForm;
import scatter.user.pojo.form.UserPageQueryForm;
import scatter.user.pojo.form.UserUpdateForm;
import scatter.user.pojo.po.User;
import scatter.user.pojo.vo.UserVo;

/**
 * <p>
 * 后台管理用户 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapStruct extends IBaseVoMapStruct<User, UserVo>,
        IBaseAddFormMapStruct<User,UserAddForm>,
        IBaseUpdateFormMapStruct<User,UserUpdateForm>,
        IBaseQueryFormMapStruct<User,UserPageQueryForm> {
    UserMapStruct INSTANCE = Mappers.getMapper( UserMapStruct.class );

}
