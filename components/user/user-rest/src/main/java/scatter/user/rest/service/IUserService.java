package scatter.user.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.user.pojo.po.User;
/**
 * <p>
 * 后台管理用户表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserService extends IBaseService<User> {

    /**
     * 根据编码查询
     * @param serialNo
     * @return
     */
    default User getBySerialNo(String serialNo) {
        Assert.hasText(serialNo,"serialNo不能为空");
        return getOne(Wrappers.<User>lambdaQuery().eq(User::getSerialNo, serialNo));
    }

}
