package scatter.user.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.user.pojo.form.UserAddForm;
import scatter.user.pojo.form.UserPageQueryForm;
import scatter.user.pojo.form.UserUpdateForm;
import scatter.user.pojo.po.User;
import scatter.user.rest.mapper.UserMapper;
import scatter.user.rest.service.IUserService;
/**
 * <p>
 * 后台管理用户表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserMapper, User, UserAddForm, UserUpdateForm, UserPageQueryForm> implements IUserService {
    @Override
    public void preAdd(UserAddForm addForm,User po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getSerialNo())) {
            // 用户编号已存在不能添加
            assertByColumn(addForm.getSerialNo(),User::getSerialNo,false);
        }

    }

    @Override
    public void preUpdate(UserUpdateForm updateForm,User po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getSerialNo())) {
            User byId = getById(updateForm.getId());
            // 如果用户编号有改动
            if (!isEqual(updateForm.getSerialNo(), byId.getSerialNo())) {
                // 用户编号已存在不能修改
                assertByColumn(updateForm.getSerialNo(),User::getSerialNo,false);
            }
        }

    }
}
