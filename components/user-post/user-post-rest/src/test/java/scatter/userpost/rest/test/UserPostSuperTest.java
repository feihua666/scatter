package scatter.userpost.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.pojo.form.UserPostAddForm;
import scatter.userpost.pojo.form.UserPostUpdateForm;
import scatter.userpost.pojo.form.UserPostPageQueryForm;
import scatter.userpost.rest.service.IUserPostService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户岗位 测试类基类
* </p>
*
* @author yw
* @since 2020-12-08
*/
public class UserPostSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IUserPostService userPostService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return userPostService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return userPostService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public UserPost mockPo() {
        return JMockData.mock(UserPost.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public UserPostAddForm mockAddForm() {
        return JMockData.mock(UserPostAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public UserPostUpdateForm mockUpdateForm() {
        return JMockData.mock(UserPostUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public UserPostPageQueryForm mockPageQueryForm() {
        return JMockData.mock(UserPostPageQueryForm.class, mockConfig);
    }
}