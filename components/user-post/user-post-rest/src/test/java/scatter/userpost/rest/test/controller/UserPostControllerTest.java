package scatter.userpost.rest.test.controller;

import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import scatter.userpost.pojo.form.UserPostAddForm;
import scatter.userpost.pojo.vo.UserPostVo;
import scatter.userpost.rest.UserPostConfiguration;
import scatter.userpost.rest.test.UserPostSuperTest;
/**
* <p>
* 用户岗位 前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
@AutoConfigureMockMvc
public class UserPostControllerTest extends UserPostSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = UserPostConfiguration.CONTROLLER_BASE_PATH + "/user-post";

        // 请求表单
        UserPostAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        UserPostVo vo = JSONUtil.toBean(response.getContentAsString(), UserPostVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}