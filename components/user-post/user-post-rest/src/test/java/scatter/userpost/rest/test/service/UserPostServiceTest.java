package scatter.userpost.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.pojo.form.UserPostAddForm;
import scatter.userpost.pojo.form.UserPostUpdateForm;
import scatter.userpost.pojo.form.UserPostPageQueryForm;
import scatter.userpost.rest.test.UserPostSuperTest;
import scatter.userpost.rest.service.IUserPostService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户岗位 服务测试类
* </p>
*
* @author yw
* @since 2020-12-08
*/
@SpringBootTest
public class UserPostServiceTest extends UserPostSuperTest{

    @Autowired
    private IUserPostService userPostService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<UserPost> pos = userPostService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}