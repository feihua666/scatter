package scatter.userpost.rest.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.userpost.pojo.form.UserPostAddForm;
import scatter.userpost.pojo.form.UserPostPageQueryForm;
import scatter.userpost.pojo.form.UserPostUpdateForm;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.rest.mapper.UserPostMapper;
import scatter.userpost.rest.service.IUserPostService;
/**
 * <p>
 * 用户岗位表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Service
@Transactional
public class UserPostServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<UserPostMapper, UserPost, UserPostAddForm, UserPostUpdateForm, UserPostPageQueryForm> implements IUserPostService {
    @Override
    public void preAdd(UserPostAddForm addForm,UserPost po) {
        // 主岗只能有一个
        if (po.getIsMain()) {
            int count = count(Wrappers.<UserPost>query().lambda().eq(UserPost::getUserId, po.getUserId()).eq(UserPost::getIsMain, true));
            Assert.isTrue(count == 0,"同一个用户只能有一个主岗");
        }
        // 一个用户在同一个部门下只能有一个岗位
        int count = count(Wrappers.<UserPost>query().lambda().eq(UserPost::getUserId, po.getUserId()).eq(UserPost::getDeptId, po.getDeptId()));
        Assert.isTrue(count == 0,"一个用户在同一个部门下只能有一个岗位");
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(UserPostUpdateForm updateForm,UserPost po) {
        // 主岗只能有一个
        if (po.getIsMain()) {
            UserPost userPost = getOne(Wrappers.<UserPost>query().lambda().eq(UserPost::getUserId, po.getUserId()).eq(UserPost::getIsMain, true));
            Assert.isTrue(userPost != null && isEqual(userPost.getId(),po.getId()),"同一个用户只能有一个主岗");
        }
        // 一个用户在同一个部门下只能有一个岗位
        UserPost userPost = getOne(Wrappers.<UserPost>query().lambda().eq(UserPost::getUserId, po.getUserId()).eq(UserPost::getDeptId, po.getDeptId()));
        Assert.isTrue(userPost != null && isEqual(userPost.getId(),po.getId()),"一个用户在同一个部门下只能有一个岗位");
        super.preUpdate(updateForm,po);

    }
}
