package scatter.userpost.rest.mapper;

import scatter.userpost.pojo.po.UserPost;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户岗位表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface UserPostMapper extends IBaseMapper<UserPost> {

}
