package scatter.userpost.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.rest.service.IUserPostService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户岗位表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/inner/user-post")
public class UserPostInnerController extends BaseInnerController<UserPost> {
 @Autowired
 private IUserPostService userPostService;

 public IUserPostService getService(){
     return userPostService;
 }
}
