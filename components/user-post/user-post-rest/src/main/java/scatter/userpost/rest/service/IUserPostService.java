package scatter.userpost.rest.service;

import scatter.common.rest.service.IBaseService;
import scatter.userpost.pojo.po.UserPost;
/**
 * <p>
 * 用户岗位表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
public interface IUserPostService extends IBaseService<UserPost> {


}
