package scatter.userpost.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.rest.service.IUserPostService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户岗位翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Component
public class UserPostTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IUserPostService userPostService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,UserPost.TRANS_USERPOST_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,UserPost.TRANS_USERPOST_BY_ID)) {
            UserPost byId = userPostService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,UserPost.TRANS_USERPOST_BY_ID)) {
            return userPostService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
