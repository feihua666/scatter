package scatter.userpost.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.userpost.rest.UserPostConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.pojo.vo.UserPostVo;
import scatter.userpost.pojo.form.UserPostAddForm;
import scatter.userpost.pojo.form.UserPostUpdateForm;
import scatter.userpost.pojo.form.UserPostPageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 用户岗位表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@RestController
@RequestMapping(UserPostConfiguration.CONTROLLER_BASE_PATH + "/user-post")
@Api(tags = "用户岗位")
public class UserPostController extends BaseAddUpdateQueryFormController<UserPost, UserPostVo, UserPostAddForm, UserPostUpdateForm, UserPostPageQueryForm> {


     @Override
	 @ApiOperation("添加用户岗位")
     @PreAuthorize("hasAuthority('UserPost:add')")
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserPostVo add(@RequestBody @Valid UserPostAddForm addForm) {
         return super.add(addForm);
     }

     @Override
	 @ApiOperation("根据ID查询用户岗位")
     @PreAuthorize("hasAuthority('UserPost:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public UserPostVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除用户岗位")
     @PreAuthorize("hasAuthority('UserPost:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

     @Override
	 @ApiOperation("根据ID更新用户岗位")
     @PreAuthorize("hasAuthority('UserPost:update')")
     @PutMapping
     @ResponseStatus(HttpStatus.CREATED)
     public UserPostVo update(@RequestBody @Valid UserPostUpdateForm updateForm) {
         return super.update(updateForm);
     }

    @Override
	@ApiOperation("不分页查询用户岗位")
    @PreAuthorize("hasAuthority('UserPost:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<UserPostVo> getList(UserPostPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询用户岗位")
    @PreAuthorize("hasAuthority('UserPost:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<UserPostVo> getPage(UserPostPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
