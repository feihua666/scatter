package scatter.userpost.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.userpost.pojo.form.UserPostAddForm;
import scatter.userpost.pojo.form.UserPostPageQueryForm;
import scatter.userpost.pojo.form.UserPostUpdateForm;
import scatter.userpost.pojo.po.UserPost;
import scatter.userpost.pojo.vo.UserPostVo;

/**
 * <p>
 * 用户岗位 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserPostMapStruct extends IBaseVoMapStruct<UserPost, UserPostVo>,
        IBaseAddFormMapStruct<UserPost,UserPostAddForm>,
        IBaseUpdateFormMapStruct<UserPost,UserPostUpdateForm>,
        IBaseQueryFormMapStruct<UserPost,UserPostPageQueryForm> {
    UserPostMapStruct INSTANCE = Mappers.getMapper( UserPostMapStruct.class );

}
