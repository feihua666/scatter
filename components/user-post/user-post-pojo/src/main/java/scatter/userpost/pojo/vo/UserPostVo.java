package scatter.userpost.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.comp.pojo.po.Comp;
import scatter.dept.pojo.po.Dept;
import scatter.dept.pojo.po.DeptTreeName;
import scatter.job.pojo.po.Job;
import scatter.job.pojo.po.JobLevel;
import scatter.post.pojo.po.Post;


/**
 * <p>
 * 用户岗位响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户岗位响应对象")
public class UserPostVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "公司id，冗余字段，由dept_id派生，")
    private String compId;

    @TransBy(type = Comp.TRANS_COMP_BY_ID,byFieldName = "compId",mapValueField = "name")
    @ApiModelProperty(value = "公司名称")
    private String compName;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @TransBy(type = Dept.TRANS_DEPT_BY_ID,byFieldName = "deptId",mapValueField = "name")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "岗位id")
    private String postId;

    @TransBy(type = Post.TRANS_POST_BY_ID,byFieldName = "postId",mapValueField = "name")
    @ApiModelProperty(value = "岗位id")
    private String postName;

    @ApiModelProperty(value = "职务id，冗余字段，由job_level_id对应的job派生")
    private String jobId;


    @TransBy(type = Job.TRANS_JOB_BY_ID,byFieldName = "jobId",mapValueField = "name")
    @ApiModelProperty(value = "职务名称")
    private String jobName;

    @ApiModelProperty(value = "职级id")
    private String jobLevelId;

    @TransBy(type = JobLevel.TRANS_JOBLEVEL_BY_ID,byFieldName = "jobLevelId",mapValueField = "name")
    @ApiModelProperty(value = "职级名称")
    private String jobLevelName;

    @ApiModelProperty(value = "是否有效")
    private Boolean isEffect;

    @ApiModelProperty(value = "无效原因")
    private String ineffectReason;

    @ApiModelProperty(value = "生效的时间")
    private Long effcetAt;

    @ApiModelProperty(value = "是否主岗")
    private Boolean isMain;

}
