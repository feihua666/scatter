package scatter.userpost.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户岗位表
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_user_post")
@ApiModel(value="UserPost对象", description="用户岗位表")
public class UserPost extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_USERPOST_BY_ID = "trans_userpost_by_id_scatter.userpost.pojo.po";

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "公司id，冗余字段，由dept_id派生，")
    private String compId;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "岗位id")
    private String postId;

    @ApiModelProperty(value = "职务id，冗余字段，由job_level_id对应的job派生")
    private String jobId;

    @ApiModelProperty(value = "职级id")
    private String jobLevelId;

    @ApiModelProperty(value = "是否有效")
    private Boolean isEffect;

    @ApiModelProperty(value = "无效原因")
    private String ineffectReason;

    @ApiModelProperty(value = "生效的时间")
    private Long effcetAt;

    @ApiModelProperty(value = "是否主岗")
    private Boolean isMain;


}
