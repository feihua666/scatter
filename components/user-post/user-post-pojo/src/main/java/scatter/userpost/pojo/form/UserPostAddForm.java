package scatter.userpost.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户岗位添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户岗位添加表单对象")
public class UserPostAddForm extends BaseAddForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotEmpty(message="公司id不能为空")
    @ApiModelProperty(value = "公司id，冗余字段，由dept_id派生，",required = true)
    private String compId;

    @NotEmpty(message="部门id不能为空")
    @ApiModelProperty(value = "部门id",required = true)
    private String deptId;


    @NotEmpty(message="岗位id不能为空")
    @ApiModelProperty(value = "岗位id",required = true)
    private String postId;

    @NotEmpty(message="职务id不能为空")
    @ApiModelProperty(value = "职务id，冗余字段，由job_level_id对应的job派生",required = true)
    private String jobId;

    @NotEmpty(message="职级id不能为空")
    @ApiModelProperty(value = "职级id",required = true)
    private String jobLevelId;

    @NotNull(message="是否有效不能为空")
    @ApiModelProperty(value = "是否有效",required = true)
    private Boolean isEffect;

    @ApiModelProperty(value = "无效原因")
    private String ineffectReason;

    @ApiModelProperty(value = "生效的时间")
    private Long effcetAt;

    @NotNull(message="是否主岗不能为空")
    @ApiModelProperty(value = "是否主岗",required = true)
    private Boolean isMain;

}
