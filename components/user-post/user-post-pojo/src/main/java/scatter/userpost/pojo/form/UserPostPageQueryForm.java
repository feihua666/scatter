package scatter.userpost.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户岗位分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-08
 */
@Setter
@Getter
@ApiModel(value="用户岗位分页表单对象")
public class UserPostPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "公司id，冗余字段，由dept_id派生，")
    private String compId;

    @ApiModelProperty(value = "部门id")
    private String deptId;


    @ApiModelProperty(value = "岗位id")
    private String postId;

    @ApiModelProperty(value = "职务id，冗余字段，由job_level_id对应的job派生")
    private String jobId;

    @ApiModelProperty(value = "职级id")
    private String jobLevelId;

    @ApiModelProperty(value = "是否有效")
    private Boolean isEffect;

    @ApiModelProperty(value = "无效原因")
    private String ineffectReason;

    @ApiModelProperty(value = "生效的时间")
    private Long effcetAt;

    @ApiModelProperty(value = "是否主岗")
    private Boolean isMain;

}
