const basePath = '' + '/user-post'
const UserPostUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/UserPostSearchList',
        add: '/UserPostAdd',
        update: '/UserPostUpdate',
    }
}
export default UserPostUrl