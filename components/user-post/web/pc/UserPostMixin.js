import UserPostForm from './UserPostForm.js'
import UserPostTable from './UserPostTable.js'
import UserPostUrl from './UserPostUrl.js'

const UserPostMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(UserPostForm,this.$options.name)
        },
        computedTableOptions() {
            return UserPostTable
        },
        computedUrl(){
            return UserPostUrl
        }
    },
}
export default UserPostMixin