import UserPostUrl from './UserPostUrl.js'

const UserPostRoute = [
    {
        path: UserPostUrl.router.searchList,
        component: () => import('./element/UserPostSearchList'),
        meta: {
            code:'UserPostSearchList',
            name: '用户岗位管理'
        }
    },
    {
        path: UserPostUrl.router.add,
        component: () => import('./element/UserPostAdd'),
        meta: {
            code:'UserPostAdd',
            name: '用户岗位添加'
        }
    },
    {
        path: UserPostUrl.router.update,
        component: () => import('./element/UserPostUpdate'),
        meta: {
            code:'UserPostUpdate',
            name: '用户岗位修改'
        }
    },
]
export default UserPostRoute