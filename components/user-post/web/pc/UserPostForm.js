import CompUrl from '../../../comp/web/pc/CompUrl.js'
import DeptUrl from '../../../dept/web/pc/DeptUrl.js'
import DeptTreeUrl from '../../../dept/web/pc/DeptTreeUrl.js'
import PostUrl from '../../../post/web/pc/PostUrl.js'
import JobUrl from '../../../job/web/pc/JobUrl.js'
import JobLevelUrl from '../../../job/web/pc/JobLevelUrl.js'
const UserPostForm = [
    {
        UserPostSearchList:{
            element:{
                required: false
            }
        },
        field: {
            name: 'userId'
        },
        element:{
            label: '用户',
            required: true,
            disabled: true,
            type: 'select',
            options: ({$vm})=>{
                return {
                    datas: [
                        {
                            id: $vm.$route.query.userId,
                            name: $vm.$route.query.userNickname
                        }
                    ]
                }
            }
        }
    },
    {
        UserPostSearchList:{
            element:{
                required: false
            }
        },
        field: {
            name: 'compId'
        },
        element:{
            label: '公司',
            required: true,
            type: 'cascader',
            options: {
                datas: CompUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
        }
    },
    {
        UserPostSearchList:{
            element:{
                required: false
            }
        },
        field: {
            name: 'deptId'
        },
        element:{
            label: '部门',
            required: true,
            type: 'cascader',
            options: ({form})=>{
                let urlQuery = ''
                if (form.compId) {
                    urlQuery = '?compId=' + form.compId
                }
                return {
                    datas: DeptUrl.list + urlQuery,
                    originProps:{
                        checkStrictly: true,
                        emitPath: false
                    },
                }
            }
        }
    },
    {
        UserPostSearchList:{
            element:{
                required: false
            }
        },
        field: {
            name: 'postId'
        },
        element:{
            label: '岗位',
            required: true,
            type: 'select',
            options: ({form})=>{
                let urlQuery = '?isIncludePublic=true'
                if (form.deptId) {
                    urlQuery += '&deptId=' + form.deptId
                }
                return {
                    datas: PostUrl.list + urlQuery
                }
            }
        }
    },
    {
        UserPostSearchList:{
            element:{
                required: false
            }
        },
        field: {
            name: 'jobId'
        },
        element:{
            label: '职务',
            required: true,
            type: 'select',
            options: ({form})=>{
                let urlQuery = '?isIncludePublic=true'
                if (form.deptId) {
                    urlQuery += '&deptId=' + form.deptId
                }
                return {
                    datas: JobUrl.list + urlQuery
                }
            }
        }
    },
    {
        UserPostSearchList:{
            element:{
                required: false
            }
        },
        field: {
            name: 'jobLevelId'
        },
        element:{
            label: '职级',
            required: true,
            type: 'select',
            options: ({form})=>{
                let urlQuery = ''
                if (form.jobId) {
                    urlQuery = '?jobId=' + form.jobId
                }
                return {
                    datas: JobLevelUrl.list + urlQuery
                }
            }
        }
    },
    {
        UserPostSearchList: false,
        field: {
            name: 'isEffect',
            valueDefault: true
        },
        element:{
            label: '是否有效',
            required: true,
            type: 'switch'
        }
    },
    {
        UserPostSearchList: false,
        field: {
            name: 'ineffectReason'
        },
        element:{
            label: '无效原因',
            required: ({form})=>{
                return !form.isEffect
            }
        }
    },
    {
        UserPostSearchList: false,
        field: {
            name: 'effcetAt'
        },
        element:{
            label: '生效的时间',
            type: 'datetime',
            required: ({form})=>{
                return !form.isEffect
            }
        }
    },
    {
        UserPostSearchList: false,
        field: {
            name: 'isMain',
            valueDefault: false
        },
        element:{
            label: '是否主岗',
            required: true,
            type: 'switch'
        }
    },
]
export default UserPostForm