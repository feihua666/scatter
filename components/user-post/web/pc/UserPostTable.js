const UserPostTable = [

    {
        prop: 'compName',
        label: '公司'
    },
    {
        prop: 'deptName',
        label: '部门'
    },
    {
        prop: 'postName',
        label: '岗位'
    },
    {
        prop: 'jobName',
        label: '职务'
    },
    {
        prop: 'jobLevelName',
        label: '职级'
    },
    {
        prop: 'isEffect',
        label: '是否有效'
    },
    {
        prop: 'ineffectReason',
        label: '无效原因'
    },
    {
        prop: 'effcetAt',
        label: '生效的时间'
    },
    {
        prop: 'isMain',
        label: '是否主岗'
    },
]
export default UserPostTable