package scatter.file.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.form.FileNeedDeleteUpdateForm;
import scatter.file.pojo.form.FileNeedDeletePageQueryForm;
import scatter.file.rest.service.IFileNeedDeleteService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 文件需要删除 测试类基类
* </p>
*
* @author yw
* @since 2021-10-21
*/
public class FileNeedDeleteSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IFileNeedDeleteService fileNeedDeleteService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return fileNeedDeleteService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return fileNeedDeleteService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public FileNeedDelete mockPo() {
        return JMockData.mock(FileNeedDelete.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public FileNeedDeleteAddForm mockAddForm() {
        return JMockData.mock(FileNeedDeleteAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public FileNeedDeleteUpdateForm mockUpdateForm() {
        return JMockData.mock(FileNeedDeleteUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public FileNeedDeletePageQueryForm mockPageQueryForm() {
        return JMockData.mock(FileNeedDeletePageQueryForm.class, mockConfig);
    }
}