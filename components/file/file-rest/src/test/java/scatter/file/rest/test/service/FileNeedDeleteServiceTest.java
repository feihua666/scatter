package scatter.file.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.form.FileNeedDeleteUpdateForm;
import scatter.file.pojo.form.FileNeedDeletePageQueryForm;
import scatter.file.rest.test.FileNeedDeleteSuperTest;
import scatter.file.rest.service.IFileNeedDeleteService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 文件需要删除 服务测试类
* </p>
*
* @author yw
* @since 2021-10-21
*/
@SpringBootTest
public class FileNeedDeleteServiceTest extends FileNeedDeleteSuperTest{

    @Autowired
    private IFileNeedDeleteService fileNeedDeleteService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<FileNeedDelete> pos = fileNeedDeleteService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }

    /**
     * 测试数据是否初始化
     */
    @Test
    void testTool() {
        List<FileNeedDelete> pos = fileNeedDeleteService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}