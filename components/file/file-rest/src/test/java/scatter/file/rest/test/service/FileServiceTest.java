package scatter.file.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.file.pojo.po.File;
import scatter.file.pojo.form.FileAddForm;
import scatter.file.pojo.form.FileUpdateForm;
import scatter.file.pojo.form.FilePageQueryForm;
import scatter.file.rest.test.FileSuperTest;
import scatter.file.rest.service.IFileService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 文件上传 服务测试类
* </p>
*
* @author yw
* @since 2020-12-15
*/
@SpringBootTest
public class FileServiceTest extends FileSuperTest{

    @Autowired
    private IFileService fileService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<File> pos = fileService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}