package scatter.file.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.file.rest.test.FileSuperTest;
/**
* <p>
* 文件上传 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2020-12-15
*/
@SpringBootTest
public class FileInnerControllerTest extends FileSuperTest{
    @Test
    void contextLoads() {
    }
}