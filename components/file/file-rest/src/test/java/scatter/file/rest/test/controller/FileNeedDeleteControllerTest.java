package scatter.file.rest.test.controller;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.file.rest.test.FileNeedDeleteSuperTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.vo.FileNeedDeleteVo;
import scatter.file.rest.FileConfiguration;
/**
* <p>
* 文件需要删除 前端控制器测试类
* </p>
*
* @author yw
* @since 2021-10-21
*/
@SpringBootTest
@AutoConfigureMockMvc
public class FileNeedDeleteControllerTest extends FileNeedDeleteSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = FileConfiguration.CONTROLLER_BASE_PATH + "/file-need-delete";

        // 请求表单
        FileNeedDeleteAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        FileNeedDeleteVo vo = JSONUtil.toBean(response.getContentAsString(), FileNeedDeleteVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}