package scatter.file.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.file.pojo.po.File;
import scatter.file.pojo.form.FileAddForm;
import scatter.file.pojo.form.FileUpdateForm;
import scatter.file.pojo.form.FilePageQueryForm;
import scatter.file.rest.service.IFileService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 文件上传 测试类基类
* </p>
*
* @author yw
* @since 2020-12-15
*/
public class FileSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IFileService fileService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return fileService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return fileService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public File mockPo() {
        return JMockData.mock(File.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public FileAddForm mockAddForm() {
        return JMockData.mock(FileAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public FileUpdateForm mockUpdateForm() {
        return JMockData.mock(FileUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public FilePageQueryForm mockPageQueryForm() {
        return JMockData.mock(FilePageQueryForm.class, mockConfig);
    }
}