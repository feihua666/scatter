DROP TABLE IF EXISTS component_file;
CREATE TABLE `component_file` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `name` varchar(255) NOT NULL COMMENT '文件名称，存储的文件名称',
  `origin_name` varchar(500) NOT NULL COMMENT '原始文件名称',
  `extension` varchar(20) NOT NULL COMMENT '文件扩展名,带点',
  `file_size` varchar(50) NOT NULL COMMENT '文件大小，单位字节',
  `type_dict_id` varchar(20) NOT NULL COMMENT '类型，字典',
  `storage_type` varchar(255) NOT NULL COMMENT '存储类型',
  `storage_type_memo` varchar(255) NOT NULL COMMENT '存储类型,备忘',
  `root_path` varchar(500) DEFAULT NULL COMMENT '存储根路径',
  `path` varchar(255) DEFAULT NULL COMMENT '存放相对路径，相对于根路径',
  `object_name` varchar(255) NOT NULL COMMENT '存储对象key',
  `url` varchar(500) NOT NULL COMMENT '文件地址',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `is_disabled` (`file_size`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文件上传表';
