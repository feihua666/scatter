DROP TABLE IF EXISTS component_file_need_delete;
CREATE TABLE `component_file_need_delete` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `url` varchar(500) NOT NULL COMMENT '需要删除的地址',
  `file_id` varchar(20) DEFAULT NULL COMMENT '文件表id',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `file_id` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件需要删除表';
