package scatter.file.rest.cloudstorage.config.impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 腾讯云
 * Created by yangwei
 * Created at 2019/3/14 11:30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ConfigurationProperties(prefix = "scatter.file.cloudstorage.txyun")
@Component
public class QcloudStorageConfig extends AbstractCloudStorageConfig{
    public QcloudStorageConfig() {
        super("txyun","腾讯云存储");
    }

    //腾讯云AppId

    private Long appId;
    //腾讯云SecretId

    private String secretId;
    //腾讯云SecretKey

    private String secretKey;
    //腾讯云BucketName

    private String bucketName;
    //腾讯云COS所属地区

    private String region;

}
