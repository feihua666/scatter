package scatter.file.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.pojo.vo.FileNeedDeleteVo;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.form.FileNeedDeleteUpdateForm;
import scatter.file.pojo.form.FileNeedDeletePageQueryForm;

/**
 * <p>
 * 文件需要删除 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FileNeedDeleteMapStruct extends IBaseVoMapStruct<FileNeedDelete, FileNeedDeleteVo>,
                                  IBaseAddFormMapStruct<FileNeedDelete,FileNeedDeleteAddForm>,
                                  IBaseUpdateFormMapStruct<FileNeedDelete,FileNeedDeleteUpdateForm>,
                                  IBaseQueryFormMapStruct<FileNeedDelete,FileNeedDeletePageQueryForm>{
    FileNeedDeleteMapStruct INSTANCE = Mappers.getMapper( FileNeedDeleteMapStruct.class );

}
