package scatter.file.rest.cloudstorage.config.impl;

import lombok.Data;
import scatter.file.rest.cloudstorage.config.CloudStorageConfig;

/**
 * Created by yangwei
 * Created at 2020/3/11 17:18
 */
@Data
public abstract class AbstractCloudStorageConfig implements CloudStorageConfig {

    private String uploadType;
    private String uploadTypeMemo;

    private String prefix;

    private String domain;

    public AbstractCloudStorageConfig(String uploadType,String uploadTypeMemo) {
        this.uploadType = uploadType;
        this.uploadTypeMemo = uploadTypeMemo;
    }

    @Override
    public String uploadType() {
        return uploadType;
    }

    @Override
    public String prefix() {
        return prefix;
    }

    @Override
    public String domain() {
        return domain;
    }

    @Override
    public boolean isLocalStorage() {
        return false;
    }
}
