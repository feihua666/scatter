package scatter.file.rest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.dict.rest.service.IDictService;
import scatter.file.rest.cloudstorage.service.ICloudStorageUploadFileTypeDictResolver;
import scatter.file.rest.cloudstorage.service.impl.ICloudStorageUploadFileTypeDictResolverImpl;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.file.rest.**.mapper")
public class FileConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "file";

    @Bean
    @ConditionalOnClass(IDictService.class)
    public ICloudStorageUploadFileTypeDictResolver iCloudStorageUploadFileTypeDictResolver() {
        return new ICloudStorageUploadFileTypeDictResolverImpl();
    }
}
