package scatter.file.rest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.file.pojo.form.FileAddForm;
import scatter.file.pojo.form.FilePageQueryForm;
import scatter.file.pojo.form.FileUpdateForm;
import scatter.file.pojo.po.File;
import scatter.file.rest.mapper.FileMapper;
import scatter.file.rest.service.IFileService;
/**
 * <p>
 * 文件上传表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Service
@Transactional
public class FileServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<FileMapper, File, FileAddForm, FileUpdateForm, FilePageQueryForm> implements IFileService {

    @Override
    public boolean deleteByObjectNameAndStorageType(String objectName, String storageType) {
        return remove(queryWrapperByObjectNameAndStorageType(objectName,storageType));
    }

    @Override
    public File getByObjectNameAndStorageType(String objectName, String storageType) {
        return getOne(queryWrapperByObjectNameAndStorageType(objectName,storageType));
    }

    private LambdaQueryWrapper<File> queryWrapperByObjectNameAndStorageType(String objectName, String storageType) {
        Assert.hasText(objectName,"objectName不能为空");
        Assert.hasText(storageType,"storageType不能为空");
        LambdaQueryWrapper<File> queryWrapper = Wrappers.<File>lambdaQuery().eq(File::getObjectName, objectName).eq(File::getStorageType, storageType);
        return queryWrapper;
    }
    @Override
    public void preAdd(FileAddForm addForm,File po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(FileUpdateForm updateForm,File po) {
        super.preUpdate(updateForm,po);

    }
}
