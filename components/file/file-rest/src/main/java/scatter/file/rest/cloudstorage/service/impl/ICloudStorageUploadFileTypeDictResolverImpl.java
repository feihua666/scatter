package scatter.file.rest.cloudstorage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;
import scatter.file.pojo.po.File;
import scatter.file.rest.cloudstorage.service.ICloudStorageUploadFileTypeDictResolver;

import java.util.List;

/**
 * Created by yangwei
 * Created at 2020/12/17 15:36
 */
@Component
public class ICloudStorageUploadFileTypeDictResolverImpl implements ICloudStorageUploadFileTypeDictResolver, InterfaceTool {

    @Autowired(required = false)
    private IDictService iDictService;

    @Override
    public String resolve(String dictValue, String extention) {
        Dict byCode = iDictService.getByCode(File.TypeDictGroup.file_type.groupCode());
        List<Dict> children = iDictService.getChildren(byCode.getId());
        String value = dictValue;
        if (isStrEmpty(value)) {
            // 根据扩展名匹配字典id

            File.TypeDictItem byExtention = File.TypeDictItem.getByExtention(extention);
            value = byExtention.itemValue();
        }
        if (isStrEmpty(value)) {
            value = File.TypeDictItem.other.itemValue();
        }
        String finalValue = value;
        return children.stream().filter(item -> isEqual(finalValue, item.getValue())).findFirst().map(Dict::getId).orElse(null);
    }
}
