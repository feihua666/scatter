package scatter.file.rest.cloudstorage.config.impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 七牛云
 * Created by yangwei
 * Created at 2019/3/14 11:30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ConfigurationProperties(prefix = "scatter.file.cloudstorage.qiniuyun")
@Component
public class QiniuStorageConfig extends AbstractCloudStorageConfig {

    public QiniuStorageConfig() {
        super("qiniuyun","七牛云存储");
    }

    //七牛ACCESS_KEY

    private String accessKey;
    //七牛SECRET_KEY

    private String secretKey;
    //七牛存储空间名

    private String bucketName;

}
