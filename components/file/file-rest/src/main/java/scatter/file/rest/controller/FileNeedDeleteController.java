package scatter.file.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.file.rest.FileConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.pojo.vo.FileNeedDeleteVo;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.form.FileNeedDeleteUpdateForm;
import scatter.file.pojo.form.FileNeedDeletePageQueryForm;
import scatter.file.rest.service.IFileNeedDeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 文件需要删除表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Api(tags = "文件需要删除相关接口")
@RestController
@RequestMapping(FileConfiguration.CONTROLLER_BASE_PATH + "/file-need-delete")
public class FileNeedDeleteController extends BaseAddUpdateQueryFormController<FileNeedDelete, FileNeedDeleteVo, FileNeedDeleteAddForm, FileNeedDeleteUpdateForm, FileNeedDeletePageQueryForm> {
    @Autowired
    private IFileNeedDeleteService iFileNeedDeleteService;

    @ApiOperation("添加文件需要删除")
    @PreAuthorize("hasAuthority('FileNeedDelete:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public FileNeedDeleteVo add(@RequestBody @Valid FileNeedDeleteAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询文件需要删除")
    @PreAuthorize("hasAuthority('FileNeedDelete:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public FileNeedDeleteVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除文件需要删除")
    @PreAuthorize("hasAuthority('FileNeedDelete:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新文件需要删除")
    @PreAuthorize("hasAuthority('FileNeedDelete:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public FileNeedDeleteVo update(@RequestBody @Valid FileNeedDeleteUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询文件需要删除")
    @PreAuthorize("hasAuthority('FileNeedDelete:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<FileNeedDeleteVo> getList(FileNeedDeletePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询文件需要删除")
    @PreAuthorize("hasAuthority('FileNeedDelete:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<FileNeedDeleteVo> getPage(FileNeedDeletePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
