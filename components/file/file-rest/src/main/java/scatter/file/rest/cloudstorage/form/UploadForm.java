package scatter.file.rest.cloudstorage.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;
import scatter.common.pojo.form.BaseForm;

import javax.validation.constraints.NotNull;

/**
 * Created by yangwei
 * Created at 2020/3/11 18:01
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel(value="文件上传表单对象")
public class UploadForm extends BaseForm {

    @NotNull(message = "请选择要上传的文件")
    @ApiModelProperty(value = "上传的文件",required = true,dataType = "org.springframework.web.multipart.MultipartFile")
    private MultipartFile file;

    @ApiModelProperty(value = "上传的路径，如：header/photo")
    private String path;

    @ApiModelProperty(value = "文件类型字典值，表示是图片还是视频等")
    private String fileTypeDictValue;
}
