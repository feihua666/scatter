package scatter.file.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.file.pojo.form.FileAddForm;
import scatter.file.pojo.form.FilePageQueryForm;
import scatter.file.pojo.form.FileUpdateForm;
import scatter.file.pojo.po.File;
import scatter.file.pojo.vo.FileVo;
import scatter.file.rest.cloudstorage.vo.UploadVo;

/**
 * <p>
 * 文件上传 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FileMapStruct extends IBaseVoMapStruct<File, FileVo>,
        IBaseAddFormMapStruct<File,FileAddForm>,
        IBaseUpdateFormMapStruct<File,FileUpdateForm>,
        IBaseQueryFormMapStruct<File,FilePageQueryForm> {
    FileMapStruct INSTANCE = Mappers.getMapper( FileMapStruct.class );

    UploadVo fileVoToUploadVo(FileVo vo);
    UploadVo fileToUploadVo(File f);
}
