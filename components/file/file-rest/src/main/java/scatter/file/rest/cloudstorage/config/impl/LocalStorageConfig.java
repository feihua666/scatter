package scatter.file.rest.cloudstorage.config.impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 本地存储
 * Created by yangwei
 * Created at 2019/3/14 11:39
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ConfigurationProperties(prefix = "scatter.file.cloudstorage.local")
@Component
public class LocalStorageConfig extends AbstractCloudStorageConfig {

    public LocalStorageConfig() {
        super("local","本地存储");
    }
    // 文件的存放位置，绝对路径，D:/base-rest/images
    private String location;

    @Override
    public boolean isLocalStorage() {
        return true;
    }
}
