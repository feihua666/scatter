package scatter.file.rest.mapper;

import scatter.file.pojo.po.File;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 文件上传表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
public interface FileMapper extends IBaseMapper<File> {

}
