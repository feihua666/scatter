package scatter.file.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.rest.service.IFileNeedDeleteService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 文件需要删除翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Component
public class FileNeedDeleteTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IFileNeedDeleteService fileNeedDeleteService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,FileNeedDelete.TRANS_FILENEEDDELETE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,FileNeedDelete.TRANS_FILENEEDDELETE_BY_ID)) {
            FileNeedDelete byId = fileNeedDeleteService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,FileNeedDelete.TRANS_FILENEEDDELETE_BY_ID)) {
            return fileNeedDeleteService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
