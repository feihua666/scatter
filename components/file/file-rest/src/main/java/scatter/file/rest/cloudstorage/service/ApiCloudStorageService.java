package scatter.file.rest.cloudstorage.service;

import java.io.InputStream;

/**
 * 云存储接口
 * Created by yangwei
 * Created at 2019/3/14 10:47
 */
public interface ApiCloudStorageService {
    /**
     * 是否支持，主要是根据配置查找责任实现类
     * @param type
     * @return
     */
    boolean support(String type);

    /**
     * 文件上传
     * @param data    文件字节数组
     * @param filepath    文件路径，包含文件名 /head/yw/xxxx.png
     * @return        返回http地址
     */
    public String upload(byte[] data, String filepath);

    /**
     * 文件上传
     * @param data     文件字节数组
     * @param suffix   后缀 .png
     * @param prefixPath   前缀路径 photo,test/photo
     * @return         返回http地址
     */
    public String uploadSuffix(byte[] data, String prefixPath, String suffix);

    /**
     * 文件上传
     * @param inputStream   字节流
     * @param filepath          文件路径，包含文件名，包含文件名 /head/yw/xxxx.png
     * @return              返回http地址
     */
    public String upload(InputStream inputStream, String filepath);

    /**
     * 文件上传
     * @param inputStream  字节流
     * @param suffix       后缀 .png
     * @param prefixPath   前缀路径 photo,test/photo
     * @return             返回http地址
     */
    public String uploadSuffix(InputStream inputStream, String prefixPath, String suffix);

    /**
     * 下载文件
     * @param objectName  文件路径
     */
    public byte[] download(String objectName) throws Throwable;

    /**
     * 删除文件
     * @param objectName
     * @return
     */
    public boolean delete(String objectName);

}
