package scatter.file.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.common.rest.service.IBaseService;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.form.FileNeedDeleteUpdateForm;
import scatter.file.pojo.form.FileNeedDeletePageQueryForm;
import java.util.List;
/**
 * <p>
 * 文件需要删除表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
public interface IFileNeedDeleteService extends IBaseService<FileNeedDelete> {


    /**
     * 根据文件表id查询
     * @param fileId
     * @return
     */
    default FileNeedDelete getByFileId(String fileId) {
        Assert.hasText(fileId,"fileId不能为空");
        return getOne(Wrappers.<FileNeedDelete>lambdaQuery().eq(FileNeedDelete::getFileId, fileId));
    }

    /**
     * 添加需要删除的数据
     * @param url 需要删除的已上传文件的全路径，必填
     * @param fileId file表的id，可选
     * @param remark 备注，必填
     * @return
     */
    public FileNeedDelete addData(String url,String fileId,String remark);

}
