package scatter.file.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.file.pojo.po.File;
import scatter.file.rest.service.IFileService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 文件上传翻译实现
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Component
public class FileTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IFileService fileService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,File.TRANS_FILE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,File.TRANS_FILE_BY_ID)) {
            File byId = fileService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,File.TRANS_FILE_BY_ID)) {
            return fileService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
