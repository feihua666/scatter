package scatter.file.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.file.rest.FileConfiguration;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.rest.service.IFileNeedDeleteService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 文件需要删除表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@RestController
@RequestMapping(FileConfiguration.CONTROLLER_BASE_PATH + "/inner/file-need-delete")
public class FileNeedDeleteInnerController extends BaseInnerController<FileNeedDelete> {
 @Autowired
 private IFileNeedDeleteService fileNeedDeleteService;

 public IFileNeedDeleteService getService(){
     return fileNeedDeleteService;
 }
}
