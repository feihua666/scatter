package scatter.file.rest.cloudstorage.config;

/**
 * 云存储配置
 * @Auther: yw
 * @Date: 2019/2/20 15:21
 * @Description:
 */
public interface CloudStorageConfig {

    /**
     * 获取上传类型
     * @return
     */
    String uploadType();

    /**
     * 存储前缀
     * @return
     */
    String prefix();

    /**
     * 访问域名
     * @return
     */
    String domain();

    /**
     * 是否本地存储
     * @return
     */
    boolean isLocalStorage();
}
