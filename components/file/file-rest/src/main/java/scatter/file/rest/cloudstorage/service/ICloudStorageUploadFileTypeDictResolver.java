package scatter.file.rest.cloudstorage.service;

/**
 * 上传文件类型字典解析服务
 * Created by yangwei
 * Created at 2020/12/16 10:57
 */
public interface ICloudStorageUploadFileTypeDictResolver {

    /**
     * 根据字典值和文件扩展名获取字典id
     * @param dictValue 字典值
     * @param extention 文件扩展名
     * @return 返回字典id
     */
    String resolve(String dictValue, String extention);
}
