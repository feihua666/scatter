package scatter.file.rest.cloudstorage.config.impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 阿里云
 * Created by yangwei
 * Created at 2019/3/14 11:28
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ConfigurationProperties(prefix = "scatter.file.cloudstorage.aliyun")
@Component
public class AliyunStorageConfig extends AbstractCloudStorageConfig {
    public AliyunStorageConfig() {
        super("aliyun","阿里云oss存储");
    }

    //阿里云EndPoint
    private String endPoint;
    //阿里云AccessKeyId
    private String accessKeyId;
    //阿里云AccessKeySecret
    private String accessKeySecret;
    //阿里云BucketName
    private String bucketName;

}
