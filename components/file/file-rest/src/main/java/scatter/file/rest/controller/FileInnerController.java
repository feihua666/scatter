package scatter.file.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.file.rest.FileConfiguration;
import scatter.file.pojo.po.File;
import scatter.file.rest.service.IFileService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 文件上传表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@RestController
@RequestMapping(FileConfiguration.CONTROLLER_BASE_PATH + "/inner/file")
public class FileInnerController extends BaseInnerController<File> {
 @Autowired
 private IFileService fileService;

 public IFileService getService(){
     return fileService;
 }
}
