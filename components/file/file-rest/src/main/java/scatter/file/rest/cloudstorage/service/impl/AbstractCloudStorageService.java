package scatter.file.rest.cloudstorage.service.impl;

import cn.hutool.core.date.DateUtil;
import org.springframework.beans.factory.annotation.Value;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.tools.PathTool;
import scatter.file.rest.cloudstorage.service.ApiCloudStorageService;

/**
 * 云存储抽象实现
 * Created by yangwei
 * Created at 2019/3/14 11:21
 */
public abstract class AbstractCloudStorageService implements ApiCloudStorageService, InterfaceTool {


    @Value("${scatter.file.cloudstorage.result-concat-domain:true}")
    private boolean resultConcatDomain;

    /**
     * 新文件路径
     * @param prefix 前缀
     * @param suffix 后缀 扩展名不带点
     * @return 返回上传路径
     */
    public String newPath(String prefix, String suffix) {
        //文件路径
        String path = DateUtil.formatDate(DateUtil.date()) + PathTool.SLASH + uuid()
                + (isStrEmpty(suffix)? "" : PathTool.ensureBeginDot(suffix));
        if(!isStrEmpty(prefix)){
            path = PathTool.ensureNotEndSlash(prefix) + PathTool.SLASH + path;
        }
        return path;
    }

    /**
     * 上传后的结果处理
     * @param domain
     * @param prefix
     * @param filePath
     * @return
     */
    public String getResult(String domain,String prefix,String filePath,String paramUploadType){
        String uploadTypeParam = "?uploadType=" + paramUploadType;
        if (!resultConcatDomain) {
            return PathTool.concat(prefix , filePath + uploadTypeParam);
        }
        return PathTool.concat(domain ,prefix , filePath + uploadTypeParam);
    }
}
