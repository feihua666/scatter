package scatter.file.rest.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import scatter.file.pojo.po.File;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.rest.mapper.FileNeedDeleteMapper;
import scatter.file.rest.service.IFileNeedDeleteService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.file.pojo.form.FileNeedDeleteAddForm;
import scatter.file.pojo.form.FileNeedDeleteUpdateForm;
import scatter.file.pojo.form.FileNeedDeletePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
import scatter.file.rest.service.IFileService;

/**
 * <p>
 * 文件需要删除表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Slf4j
@Service
@Transactional
public class FileNeedDeleteServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<FileNeedDeleteMapper, FileNeedDelete, FileNeedDeleteAddForm, FileNeedDeleteUpdateForm, FileNeedDeletePageQueryForm> implements IFileNeedDeleteService {

    @Lazy
    @Autowired
    private IFileService iFileService;

    @Override
    public void preAdd(FileNeedDeleteAddForm addForm,FileNeedDelete po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getFileId())) {
            // 文件表id已存在不能添加
            assertByColumn(addForm.getFileId(),FileNeedDelete::getFileId,false);
        }

    }

    @Override
    public void preUpdate(FileNeedDeleteUpdateForm updateForm,FileNeedDelete po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getFileId())) {
            FileNeedDelete byId = getById(updateForm.getId());
            // 如果文件表id有改动
            if (!isEqual(updateForm.getFileId(), byId.getFileId())) {
                // 文件表id已存在不能修改
                assertByColumn(updateForm.getFileId(),FileNeedDelete::getFileId,false);
            }
        }
    }

    @Override
    protected void postDeleteById(String id, FileNeedDelete po) {
        // 删除后需要将文件也删除
        if (isStrEmpty(po.getFileId())) {
            File byUrl = iFileService.getByUrl(po.getUrl());
            if (byUrl == null) {
                log.warn("删除需要删除的文件，未能根据url找到file数据,fileNeedDelete={}",toJsonStr(po));
            }else {
                iFileService.deleteById(byUrl.getId());
            }
        }else {
            iFileService.deleteById(po.getFileId());
        }
    }

    @Override
    public FileNeedDelete addData(String url, String fileId, String remark) {

        if(isStrEmpty(fileId)){

            File byUrl = iFileService.getByUrl(url);
            if (byUrl != null) {
                fileId = byUrl.getId();
            }else {
                log.warn("添加需要删除的数据，未根据url获取到fileid，url={},remark={}",url,remark);
            }
        }
        FileNeedDelete fileNeedDelete = new FileNeedDelete().setUrl(url).setFileId(fileId).setRemark(remark);


        save(fileNeedDelete);
        return fileNeedDelete;
    }
}
