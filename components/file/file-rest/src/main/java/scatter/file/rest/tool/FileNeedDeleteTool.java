package scatter.file.rest.tool;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import scatter.common.rest.tools.SpringContextHolder;
import scatter.file.pojo.po.FileNeedDelete;
import scatter.file.rest.service.IFileNeedDeleteService;

import java.util.concurrent.ExecutorService;

/**
 * <p>
 * 文件需要删除记录工具
 * </p>
 *
 * @author yangwei
 * @since 2021-10-21 14:38
 */
@Slf4j
public class FileNeedDeleteTool {
	/**
	 * 添加
	 * @param url
	 * @param remark
	 */
	public static void add(String url,String remark){
		if (StrUtil.isEmpty(url)) {
			log.warn("添加需要删除的文件，url为空，直接返回");
			return;
		}
		SpringContextHolder.getBean(IFileNeedDeleteService.class).addData(url,null,remark);
	}

	/**
	 * 添加,异步
	 * @param url
	 * @param remark
	 */
	public static void addAsync(String url,String remark){
		ExecutorService commonDbTaskExecutor = null;
		try {
			commonDbTaskExecutor = SpringContextHolder.getBean("commonDbTaskExecutor");
		} catch (Exception e) {
			log.error("获取名为 commonDbTaskExecutor 线程池实例失败，将使用同步逻辑执行，可能原因排除了 common 组件的配置类 scatter.common.rest.config.CommonExecutorsConfig");
		}
		if (commonDbTaskExecutor != null) {
			commonDbTaskExecutor.execute(() -> {
				add(url,remark);
			});

		}else {
			add(url, remark);
		}
	}
}
