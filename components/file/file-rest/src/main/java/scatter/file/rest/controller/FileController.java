package scatter.file.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.file.rest.FileConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.file.pojo.po.File;
import scatter.file.pojo.vo.FileVo;
import scatter.file.pojo.form.FileAddForm;
import scatter.file.pojo.form.FileUpdateForm;
import scatter.file.pojo.form.FilePageQueryForm;

import javax.validation.Valid;
/**
 * <p>
 * 文件上传表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Api(tags = "文件上传相关接口")
@RestController
@RequestMapping(FileConfiguration.CONTROLLER_BASE_PATH + "/file")
public class FileController extends BaseAddUpdateQueryFormController<File, FileVo, FileAddForm, FileUpdateForm, FilePageQueryForm> {


     @Override
	 @ApiOperation("根据ID查询文件上传")
     @PreAuthorize("hasAuthority('File:queryById')")
     @GetMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     public FileVo queryById(@PathVariable String id) {
         return super.queryById(id);
     }

     @Override
	 @ApiOperation("根据ID删除文件上传")
     @PreAuthorize("hasAuthority('File:deleteById')")
     @DeleteMapping("/{id}")
     @ResponseStatus(HttpStatus.NO_CONTENT)
     public boolean deleteById(@PathVariable String id) {
         return super.deleteById(id);
     }

    @Override
	@ApiOperation("不分页查询文件上传")
    @PreAuthorize("hasAuthority('File:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    public List<FileVo> getList(FilePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @Override
	@ApiOperation("分页查询文件上传")
    @PreAuthorize("hasAuthority('File:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<FileVo> getPage(FilePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
