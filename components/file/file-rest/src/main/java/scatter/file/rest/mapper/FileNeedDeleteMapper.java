package scatter.file.rest.mapper;

import scatter.file.pojo.po.FileNeedDelete;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 文件需要删除表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
public interface FileNeedDeleteMapper extends IBaseMapper<FileNeedDelete> {

}
