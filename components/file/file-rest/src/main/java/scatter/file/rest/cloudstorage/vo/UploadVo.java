package scatter.file.rest.cloudstorage.vo;

import io.swagger.annotations.ApiModel;
import scatter.file.pojo.vo.FileVo;

/**
 * Created by yangwei
 * Created at 2020/3/12 13:43
 */
@ApiModel(value="文件上传数据响应对象")
public class UploadVo extends FileVo {
}
