package scatter.file.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.file.pojo.po.File;
/**
 * <p>
 * 文件上传表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
public interface IFileService extends IBaseService<File> {

    /**
     * 根据对象名称和上传类型删除文件数据
     * @param objectName
     * @param storageType
     * @return
     */
    boolean deleteByObjectNameAndStorageType(String objectName, String storageType);

    /**
     *
     * @param objectName
     * @param storageType
     * @return
     */
    File getByObjectNameAndStorageType(String objectName, String storageType);

    /**
     * 根据 url地址获取
     * 经分析，一个url 不管使用哪种存储方式，全表应该只有一条数据
     * @param url
     * @return
     */
    default File getByUrl(String url){
        Assert.hasText(url,"url 不能为空");
        LambdaQueryWrapper<File> eq = Wrappers.<File>lambdaQuery().eq(File::getUrl, url);
        return getOne(eq);
    }
}
