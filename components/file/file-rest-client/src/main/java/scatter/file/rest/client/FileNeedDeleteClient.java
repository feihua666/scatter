package scatter.file.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 文件需要删除表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Component
@FeignClient(value = "FileNeedDelete-client")
public interface FileNeedDeleteClient {

}
