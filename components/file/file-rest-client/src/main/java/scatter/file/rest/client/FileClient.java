package scatter.file.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 文件上传表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Component
@FeignClient(value = "File-client")
public interface FileClient {

}
