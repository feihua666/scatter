package scatter.file.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 文件上传响应对象
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Setter
@Getter
@ApiModel(value="文件上传响应对象")
public class FileVo extends BaseIdVo {

    @ApiModelProperty(value = "文件名称，存储的文件名称")
    private String name;

    @ApiModelProperty(value = "原始文件名称")
    private String originName;

    @ApiModelProperty(value = "文件扩展名,带点")
    private String extension;

    @ApiModelProperty(value = "文件大小，单位字节")
    private String fileSize;

    @ApiModelProperty(value = "类型，字典")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="value")
    @ApiModelProperty(value = "类型，字典值")
    private String typeDictValue;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField="name")
    @ApiModelProperty(value = "类型，字典名称")
    private String typeDictName;



    @ApiModelProperty(value = "存储类型")
    private String storageType;

    @ApiModelProperty(value = "存储类型,备忘")
    private String storageTypeMemo;

    @ApiModelProperty(value = "存储根路径")
    private String rootPath;

    @ApiModelProperty(value = "存放相对路径，相对于根路径")
    private String path;

    @ApiModelProperty(value = "存储对象key")
    private String objectName;

    @ApiModelProperty(value = "文件地址")
    private String url;

    @ApiModelProperty(value = "描述")
    private String remark;

}
