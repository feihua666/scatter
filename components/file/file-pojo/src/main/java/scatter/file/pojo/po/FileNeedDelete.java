package scatter.file.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文件需要删除表
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_file_need_delete")
@ApiModel(value="FileNeedDelete对象", description="文件需要删除表")
public class FileNeedDelete extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_FILENEEDDELETE_BY_ID = "trans_fileneeddelete_by_id_scatter.file.pojo.po";

    @ApiModelProperty(value = "需要删除的地址")
    private String url;

    @ApiModelProperty(value = "文件表id")
    private String fileId;

    @ApiModelProperty(value = "描述")
    private String remark;


}
