package scatter.file.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 文件需要删除分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-21
 */
@Setter
@Getter
@ApiModel(value="文件需要删除分页表单对象")
public class FileNeedDeletePageQueryForm extends BasePageQueryForm {
    @Like
    @ApiModelProperty(value = "需要删除的地址")
    private String url;

    @ApiModelProperty(value = "文件表id")
    private String fileId;

    @Like
    @ApiModelProperty(value = "描述")
    private String remark;

}
