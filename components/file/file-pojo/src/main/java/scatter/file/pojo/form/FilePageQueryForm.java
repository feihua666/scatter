package scatter.file.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 文件上传分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Setter
@Getter
@ApiModel(value="文件上传分页表单对象")
public class FilePageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "文件名称，存储的文件名称")
    private String name;
    @Like
    @ApiModelProperty(value = "原始文件名称")
    private String originName;
    @Like
    @ApiModelProperty(value = "文件扩展名,带点")
    private String extension;

    @ApiModelProperty(value = "类型，字典")
    private String typeDictId;

    @Like
    @ApiModelProperty(value = "存储类型")
    private String storageType;
    @Like
    @ApiModelProperty(value = "存储对象key")
    private String objectName;


}
