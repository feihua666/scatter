package scatter.file.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 文件上传添加表单对象
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Setter
@Getter
@ApiModel(value="文件上传添加表单对象")
public class FileAddForm extends BaseAddForm {

    @NotEmpty(message="文件名称不能为空")
    @ApiModelProperty(value = "文件名称，存储的文件名称",required = true)
    private String name;

    @NotEmpty(message="原始文件名称不能为空")
    @ApiModelProperty(value = "原始文件名称",required = true)
    private String originName;

    @NotEmpty(message="文件扩展名不能为空")
    @ApiModelProperty(value = "文件扩展名,带点",required = true)
    private String extension;

    @NotEmpty(message="文件大小不能为空")
    @ApiModelProperty(value = "文件大小，单位字节",required = true)
    private String fileSize;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典",required = true)
    private String typeDictId;

    @NotEmpty(message="存储类型不能为空")
    @ApiModelProperty(value = "存储类型",required = true)
    private String storageType;

    @NotEmpty(message="存储类型不能为空")
    @ApiModelProperty(value = "存储类型,备忘",required = true)
    private String storageTypeMemo;

    @ApiModelProperty(value = "存储根路径")
    private String rootPath;

    @ApiModelProperty(value = "存放相对路径，相对于根路径")
    private String path;

    @NotEmpty(message="存储对象key不能为空")
    @ApiModelProperty(value = "存储对象key",required = true)
    private String objectName;

    @NotEmpty(message="文件地址不能为空")
    @ApiModelProperty(value = "文件地址",required = true)
    private String url;

    @ApiModelProperty(value = "描述")
    private String remark;

}
