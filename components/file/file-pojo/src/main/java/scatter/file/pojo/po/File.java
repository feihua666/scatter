package scatter.file.pojo.po;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * 文件上传表
 * </p>
 *
 * @author yw
 * @since 2020-12-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_file")
@ApiModel(value="File对象", description="文件上传表")
public class File extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_FILE_BY_ID = "trans_file_by_id_scatter.file.pojo.po";

    /**
     * 区域类型字典组编码
     */
    public enum TypeDictGroup implements IDictGroup {
        file_type;
        @Override
        public String groupCode() {
            return this.name();
        }}

    /**
     * 区域类型字典项编码
     */
    public enum TypeDictItem implements IDictItem {
        image,
        video,
        word,
        excel,
        ppt,
        exe,
        txt,
        other,
        ;

        private static Map<TypeDictItem, Set<String>> extention = new HashMap<>();
        static {
            extention.put(image, Stream.of(".png", ".jpg",".jpeg", ".gif").collect(Collectors.toSet()));
            extention.put(video, Stream.of(".mp4", ".rmvb",".vob", ".flv", ".mkv").collect(Collectors.toSet()));
            extention.put(word, Stream.of(".doc", ".docx").collect(Collectors.toSet()));
            extention.put(excel, Stream.of(".xls", ".xlsx").collect(Collectors.toSet()));
            extention.put(ppt, Stream.of(".pptx").collect(Collectors.toSet()));
            extention.put(exe, Stream.of(".exe").collect(Collectors.toSet()));
            extention.put(txt, Stream.of(".txt").collect(Collectors.toSet()));
            extention.put(other, new HashSet<>());
        }


        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return TypeDictGroup.file_type.groupCode();
        }

        /**
         * 匹配扩展名
         * @param extention
         * @return
         */
        public boolean matchExtention(String extention) {
            if (StrUtil.isEmpty(extention) && this == other) {
                return true;
            }
            Set<String> strings = TypeDictItem.extention.get(this);
            boolean r = strings.contains(extention);
            if (r) {
                return r;
            }
            return this == other;
        }

        /**
         * 根据扩展名获取文件的类型
         * @param extention
         * @return
         */
        public static TypeDictItem getByExtention(String extention) {
            for (TypeDictItem value : TypeDictItem.values()) {
                if (value.matchExtention(extention)) {
                    return value;
                }
            }
            return null;
        }
    }
    @ApiModelProperty(value = "文件名称，存储的文件名称")
    private String name;

    @ApiModelProperty(value = "原始文件名称")
    private String originName;

    @ApiModelProperty(value = "文件扩展名,带点")
    private String extension;

    @ApiModelProperty(value = "文件大小，单位字节")
    private String fileSize;

    @ApiModelProperty(value = "类型，字典")
    private String typeDictId;

    @ApiModelProperty(value = "存储类型")
    private String storageType;

    @ApiModelProperty(value = "存储类型,备忘")
    private String storageTypeMemo;

    @ApiModelProperty(value = "存储根路径")
    private String rootPath;

    @ApiModelProperty(value = "存放相对路径，相对于根路径")
    private String path;

    @ApiModelProperty(value = "存储对象key")
    private String objectName;

    @ApiModelProperty(value = "文件地址")
    private String url;

    @ApiModelProperty(value = "描述")
    private String remark;


}
