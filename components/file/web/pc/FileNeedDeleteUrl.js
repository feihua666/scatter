const basePath = '' + '/file-need-delete'
const FileNeedDeleteUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/FileNeedDeleteSearchList',

    }
}
export default FileNeedDeleteUrl