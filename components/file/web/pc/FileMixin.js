import FileForm from './FileForm.js'
import FileTable from './FileTable.js'
import FileUrl from './FileUrl.js'

const FileMixin = {
    computed: {
        FileFormOptions() {
            return this.$stDynamicFormTools.options(FileForm,this.$options.name)
        },
        FileTableOptions() {
            return FileTable
        },
        FileUrl(){
            return FileUrl
        }
    },
}
export default FileMixin