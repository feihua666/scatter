import FileUrl from './FileUrl.js'

const FileRoute = [
    {
        path: FileUrl.router.searchList,
        component: () => import('./element/FileSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'FileSearchList',
            name: '文件上传管理'
        }
    },
]
export default FileRoute