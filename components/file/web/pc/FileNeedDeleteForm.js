import FileNeedDeleteUrl from './FileNeedDeleteUrl.js'
const FileNeedDeleteForm = [
    {
        FileNeedDeleteSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'url',
        },
        element:{
            label: '文件地址',
            required: true,
        }
    },
    {

        field: {
            name: 'fileId',
        },
        element:{
            label: '文件表id',
        }
    },
    {

        field: {
            name: 'remark',
        },
        element:{
            label: '描述',
        }
    },

]
export default FileNeedDeleteForm