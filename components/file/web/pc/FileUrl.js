const basePath = '' + '/file'
const FileUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/FileSearchList',
        add: '/FileAdd',
        update: '/FileUpdate',
    }
}
export default FileUrl