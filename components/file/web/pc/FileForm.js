const FileForm = [
    {
        FileSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'name',
        },
        element:{
            label: '文件名称',
            required: true,
        }
    },
    {
        FileSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'originName',
        },
        element:{
            label: '原始文件名称',
            required: true,
        }
    },
    {
        FileSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'extension',
        },
        element:{
            label: '文件扩展名',
            required: true,
        }
    },
    {
        FileSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'typeDictId',
        },
        element:{
            type: 'selectDict',
            options: {
                groupCode: 'file_type'
            },
            label: '类型',
            required: true,
        }
    },
    {
        FileSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'storageType',
        },
        element:{
            label: '存储类型',
            required: true,
        }
    },
    {
        FileSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'objectName',
        },
        element:{
            label: '存储对象key',
            required: true,
        }
    },
]
export default FileForm