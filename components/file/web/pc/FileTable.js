const FileTable = [
    {
        type: 'expand',
    },
    {
        prop: 'name',
        label: '文件名称',
        showOverflowTooltip: true
    },
    {
        prop: 'originName',
        label: '原始文件名称'
    },
    {
        prop: 'extension',
        label: '文件扩展名'
    },
    {
        prop: 'fileSize',
        label: '文件大小'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
    {
        prop: 'storageType',
        label: '存储类型'
    },
    {
        prop: 'storageTypeMemo',
        label: '存储类型备注'
    },
    {
        prop: 'rootPath',
        label: '存储根路径'
    },
    {
        prop: 'path',
        label: '存放相对路径'
    },
    {
        prop: 'objectName',
        label: '存储对象key',
        showOverflowTooltip: true
    },
    {
        prop: 'url',
        label: '文件地址',
        showOverflowTooltip: true
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default FileTable