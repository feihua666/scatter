const FileNeedDeleteTable = [
    {
        prop: 'url',
        label: '需要删除的地址'
    },
    {
        prop: 'fileId',
        label: '文件表id'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default FileNeedDeleteTable