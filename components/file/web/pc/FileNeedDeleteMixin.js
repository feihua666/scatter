import FileNeedDeleteForm from './FileNeedDeleteForm.js'
import FileNeedDeleteTable from './FileNeedDeleteTable.js'
import FileNeedDeleteUrl from './FileNeedDeleteUrl.js'
const FileNeedDeleteMixin = {
    computed: {
        FileNeedDeleteFormOptions() {
            return this.$stDynamicFormTools.options(FileNeedDeleteForm,this.$options.name)
        },
        FileNeedDeleteTableOptions() {
            return FileNeedDeleteTable
        },
        FileNeedDeleteUrl(){
            return FileNeedDeleteUrl
        }
    },
}
export default FileNeedDeleteMixin