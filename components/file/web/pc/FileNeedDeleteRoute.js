import FileNeedDeleteUrl from './FileNeedDeleteUrl.js'

const FileNeedDeleteRoute = [
    {
        path: FileNeedDeleteUrl.router.searchList,
        component: () => import('./element/FileNeedDeleteSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'FileNeedDeleteSearchList',
            name: '文件需要删除管理'
        }
    },
]
export default FileNeedDeleteRoute