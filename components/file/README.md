# file 文件上传下载组件

一般系统中都会有图片或文档的上传和下载功能，该组件旨在统一系统的上传和下载的出入口，具有以下功能  

+ 支持本地存储
+ 支持阿里云oss存储
+ 支持腾讯云存储
+ 支持七牛云存储
+ 其它可自定义扩展

# 配置

## 本地存储配置
```yaml
scatter:
  file:
    cloudstorage:
      # 配置使用的上传类型 默认local
      upload-type: local
      local:
        # 上传位置，前缀
        prefix: test
        # 上传成功后拼接的域名
        domain: http://localhost
        # 存储位置
        location: d:/upload 
```

## 阿里云oss配置
```yaml
scatter:
  file:
    cloudstorage:
      # 配置使用的上传类型 默认local
      upload-type: aliyun
      aliyun:
        # 上传位置，前缀
        prefix: test
        # 上传成功后拼接的域名
        domain: http://xxxxx.oss-cn-beijing.aliyuncs.com
        #end-point: http://oss-cn-beijing-internal.aliyuncs.com
        # 上传位置
        end-point: http://oss-cn-beijing.aliyuncs.com
        access-key-id: access-key-id
        access-key-secret: access-key-secret
        bucket-name: bucket-name
```

## 其它存储配置
具体配置请参见AbstractCloudStorageConfig的子类  

::: warning 注意
文件上传目前只支持一种配置生效，也就是说多种存储配置不能并存生效
:::