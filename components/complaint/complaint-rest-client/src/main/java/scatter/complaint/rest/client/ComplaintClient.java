package scatter.complaint.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 举报投诉表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Component
@FeignClient(value = "Complaint-client")
public interface ComplaintClient {

}
