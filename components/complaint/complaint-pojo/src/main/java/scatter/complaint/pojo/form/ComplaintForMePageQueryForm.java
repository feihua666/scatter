package scatter.complaint.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BasePageQueryForm;

import java.time.LocalDateTime;


/**
 * <p>
 * 举报投诉分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Setter
@Getter
@ApiModel(value="举报投诉分页表单对象")
public class ComplaintForMePageQueryForm extends BasePageQueryForm {


}
