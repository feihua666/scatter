package scatter.complaint.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 举报图片添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Setter
@Getter
@ApiModel(value="举报图片添加表单对象")
public class ComplaintPicAddForm extends BaseAddForm {

    @NotEmpty(message="举报id不能为空")
    @ApiModelProperty(value = "举报id",required = true)
    private String complaintId;

    @NotEmpty(message="图片地址不能为空")
    @ApiModelProperty(value = "图片地址",required = true)
    private String picUrl;

    @NotNull(message="是否举报图片不能为空")
    @ApiModelProperty(value = "是否举报图片，1=举报图片，0=回复图片",required = true)
    private Boolean isComplaint;

}
