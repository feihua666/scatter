package scatter.complaint.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.complaint.pojo.po.ComplaintPic;

import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 举报投诉响应对象
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Setter
@Getter
@ApiModel(value="举报投诉响应对象")
public class ComplaintVo extends BaseIdVo {

    @ApiModelProperty(value = "数据id")
    private String dataId;

    @ApiModelProperty(value = "数据类型，用于区分数据id")
    private String dataType;

    @ApiModelProperty(value = "举报内容")
    private String complaintContent;

    @ApiModelProperty(value = "举报时间")
    private LocalDateTime complaintAt;

    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id")
    private String complaintUserId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "回复时间")
    private LocalDateTime replyAt;

    @ApiModelProperty(value = "回复用户id")
    private String replyUserId;

    @ApiModelProperty(value = "第一条举报id，主要串联起一个话题")
    private String firstComplaintId;

    @TransBy(type = ComplaintPic.TRANS_COMPLAINT_COMPLAINTPIC_BY_COMPLAINT_ID,byFieldName = "id")
    @ApiModelProperty("建议图片")
    private List<String> complaintPicUrls;

    @TransBy(type = ComplaintPic.TRANS_REPLY_COMPLAINTPIC_BY_COMPLAINT_ID,byFieldName = "id")
    @ApiModelProperty("回复图片")
    private List<String> replyPicUrls;

}
