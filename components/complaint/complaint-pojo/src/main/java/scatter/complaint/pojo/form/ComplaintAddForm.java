package scatter.complaint.pojo.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.util.List;

import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 举报投诉添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Setter
@Getter
@ApiModel(value="举报投诉添加表单对象")
public class ComplaintAddForm extends BaseAddForm {

    @NotEmpty(message="数据id不能为空")
    @ApiModelProperty(value = "数据id",required = true)
    private String dataId;

    @NotEmpty(message="数据类型不能为空")
    @ApiModelProperty(value = "数据类型，用于区分数据id",required = true)
    private String dataType;

    @NotEmpty(message="举报内容不能为空")
    @ApiModelProperty(value = "举报内容",required = true)
    private String complaintContent;
    /**
     * 该字段不需要前端传
     */
    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id",hidden = true)
    private String complaintUserId;

    @ApiModelProperty("图片地址")
    private List<String> picUrls;


}
