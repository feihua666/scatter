package scatter.complaint.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 举报投诉分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Setter
@Getter
@ApiModel(value="举报投诉分页表单对象")
public class ComplaintPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "数据id")
    private String dataId;

    @ApiModelProperty(value = "数据类型，用于区分数据id")
    private String dataType;

    @ApiModelProperty(value = "举报内容")
    private String complaintContent;

    @ApiModelProperty(value = "举报时间")
    private LocalDateTime complaintAt;

    @ApiModelProperty(value = "用户id，如果存在登录用户，记录提建议的用户id")
    private String complaintUserId;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "回复时间")
    private LocalDateTime replyAt;

    @ApiModelProperty(value = "回复用户id")
    private String replyUserId;

    @ApiModelProperty(value = "第一条意见反馈id，主要串联起一个话题")
    private String firstComplaintId;

}
