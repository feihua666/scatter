package scatter.complaint.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseUpdateIdForm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 举报投诉回复表单对象
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Setter
@Getter
@ApiModel(value="举报投诉回复表单对象")
public class ComplaintReplyForm extends BaseUpdateIdForm {


    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    /**
     * 隐藏掉该字段到前端，后台处理
     */
    @ApiModelProperty(value = "回复用户id",hidden = true)
    private String replyUserId;

    @ApiModelProperty("图片地址")
    private List<String> picUrls;

}
