package scatter.complaint.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 举报图片表
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_complaint_pic")
@ApiModel(value="ComplaintPic对象", description="举报图片表")
public class ComplaintPic extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_COMPLAINTPIC_BY_ID = "trans_complaintpic_by_id_scatter.complaint.pojo.po";

    public static final String TRANS_COMPLAINT_COMPLAINTPIC_BY_COMPLAINT_ID = "trans_complaint_complaintpic_by_complaint_id_scatter.complaint.pojo.po";
    public static final String TRANS_REPLY_COMPLAINTPIC_BY_COMPLAINT_ID = "trans_reply_complaintpic_by_complaint_id_scatter.complaint.pojo.po";


    @ApiModelProperty(value = "举报id")
    private String complaintId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "是否举报图片，1=举报图片，0=回复图片")
    private Boolean isComplaint;


}
