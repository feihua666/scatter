package scatter.complaint.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.pojo.form.ComplaintPicAddForm;
import scatter.complaint.pojo.form.ComplaintPicUpdateForm;
import scatter.complaint.pojo.form.ComplaintPicPageQueryForm;
import scatter.complaint.rest.service.IComplaintPicService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 举报图片 测试类基类
* </p>
*
* @author yw
* @since 2021-11-10
*/
public class ComplaintPicSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IComplaintPicService complaintPicService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return complaintPicService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return complaintPicService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ComplaintPic mockPo() {
        return JMockData.mock(ComplaintPic.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ComplaintPicAddForm mockAddForm() {
        return JMockData.mock(ComplaintPicAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ComplaintPicUpdateForm mockUpdateForm() {
        return JMockData.mock(ComplaintPicUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ComplaintPicPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ComplaintPicPageQueryForm.class, mockConfig);
    }
}