package scatter.complaint.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.complaint.pojo.po.Complaint;
import scatter.complaint.pojo.form.ComplaintAddForm;
import scatter.complaint.pojo.form.ComplaintUpdateForm;
import scatter.complaint.pojo.form.ComplaintPageQueryForm;
import scatter.complaint.rest.service.IComplaintService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 举报投诉 测试类基类
* </p>
*
* @author yw
* @since 2021-11-15
*/
public class ComplaintSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IComplaintService complaintService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return complaintService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return complaintService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Complaint mockPo() {
        return JMockData.mock(Complaint.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ComplaintAddForm mockAddForm() {
        return JMockData.mock(ComplaintAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ComplaintUpdateForm mockUpdateForm() {
        return JMockData.mock(ComplaintUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ComplaintPageQueryForm mockPageQueryForm() {
        return JMockData.mock(ComplaintPageQueryForm.class, mockConfig);
    }
}