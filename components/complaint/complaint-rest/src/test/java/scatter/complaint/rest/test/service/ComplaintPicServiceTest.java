package scatter.complaint.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.pojo.form.ComplaintPicAddForm;
import scatter.complaint.pojo.form.ComplaintPicUpdateForm;
import scatter.complaint.pojo.form.ComplaintPicPageQueryForm;
import scatter.complaint.rest.test.ComplaintPicSuperTest;
import scatter.complaint.rest.service.IComplaintPicService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 举报图片 服务测试类
* </p>
*
* @author yw
* @since 2021-11-10
*/
@SpringBootTest
public class ComplaintPicServiceTest extends ComplaintPicSuperTest{

    @Autowired
    private IComplaintPicService complaintPicService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<ComplaintPic> pos = complaintPicService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}