DROP TABLE IF EXISTS component_complaint_pic;
CREATE TABLE `component_complaint_pic` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `complaint_id` varchar(20) NOT NULL COMMENT '举报id',
  `pic_url` varchar(300) NOT NULL COMMENT '图片地址',
  `is_complaint` tinyint(1) NOT NULL COMMENT '是否举报图片，1=举报图片，0=回复图片',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `complaint_id` (`complaint_id`,`is_complaint`),
  KEY `complaint_id_2` (`complaint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='举报图片表';
