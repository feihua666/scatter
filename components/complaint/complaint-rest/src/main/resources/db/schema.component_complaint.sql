DROP TABLE IF EXISTS component_complaint;
CREATE TABLE `component_complaint` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `data_id` varchar(20) NOT NULL COMMENT '数据id',
  `data_type` varchar(50) NOT NULL COMMENT '数据类型，用于区分数据id',
  `complaint_content` varchar(2000) NOT NULL COMMENT '举报内容',
  `complaint_at` datetime NOT NULL COMMENT '举报时间',
  `complaint_user_id` varchar(20) DEFAULT NULL COMMENT '用户id，如果存在登录用户，记录提建议的用户id',
  `reply_content` varchar(2000) DEFAULT NULL COMMENT '回复内容',
  `reply_at` datetime DEFAULT NULL COMMENT '回复时间',
  `reply_user_id` varchar(20) DEFAULT NULL COMMENT '回复用户id',
  `first_complaint_id` varchar(20) DEFAULT NULL COMMENT '第一条意见反馈id，主要串联起一个话题',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `first_complaint_id` (`first_complaint_id`),
  KEY `data_type` (`data_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='举报投诉表';
