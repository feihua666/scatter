package scatter.complaint.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.complaint.pojo.form.*;
import scatter.complaint.pojo.po.Complaint;
import scatter.complaint.pojo.vo.ComplaintVo;

/**
 * <p>
 * 举报投诉 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComplaintMapStruct extends IBaseVoMapStruct<Complaint, ComplaintVo>,
                                  IBaseAddFormMapStruct<Complaint,ComplaintAddForm>,
                                  IBaseUpdateFormMapStruct<Complaint,ComplaintUpdateForm>,
                                  IBaseQueryFormMapStruct<Complaint,ComplaintPageQueryForm>{
    ComplaintMapStruct INSTANCE = Mappers.getMapper( ComplaintMapStruct.class );

    Complaint complaintForMePageQueryFormToPo(ComplaintForMePageQueryForm form);

    Complaint replyFormToPo(ComplaintReplyForm form);
}
