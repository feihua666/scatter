package scatter.complaint.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.common.rest.service.IBaseService;
import scatter.complaint.pojo.form.ComplaintPicAddForm;
import scatter.complaint.pojo.form.ComplaintPicUpdateForm;
import scatter.complaint.pojo.form.ComplaintPicPageQueryForm;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 举报图片表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface IComplaintPicService extends IBaseService<ComplaintPic> {


	/**
	 * 添加图片
	 * @param picUrls
	 * @param complaintId
	 * @param isComplaint
	 * @return
	 */
	default public boolean addPicUrls(List<String> picUrls,String complaintId,boolean isComplaint){
		Assert.hasText(complaintId,"complaintId 不能为空");
		Assert.notEmpty(picUrls,"picUrls 不能为空");

		List<ComplaintPic> collect = picUrls.stream().map(url -> new ComplaintPic().setComplaintId(complaintId).setPicUrl(url).setIsComplaint(isComplaint)).collect(Collectors.toList());
		return saveBatch(collect);
	}

	/**
	 * 删除
	 * @param complaintId
	 * @param isComplaint
	 * @return
	 */
	default public boolean deletePicUrls(String complaintId, Boolean isComplaint) {
		Assert.hasText(complaintId,"complaintId 不能为空");
		LambdaQueryWrapper<ComplaintPic> eq = Wrappers.<ComplaintPic>lambdaQuery().eq(ComplaintPic::getComplaintId, complaintId)
				.eq(isComplaint != null,ComplaintPic::getIsComplaint, isComplaint);
		return remove(eq);
	}

	/**
	 * 根据complaint获取
	 * @param complaintId
	 * @param isComplaint
	 * @return
	 */
	default List<ComplaintPic> getByComplaintId(String complaintId,Boolean isComplaint){
		Assert.hasText(complaintId,"complaintId 不能为空");
		LambdaQueryWrapper<ComplaintPic> eq = Wrappers.<ComplaintPic>lambdaQuery().eq(ComplaintPic::getComplaintId, complaintId)
				.eq(isComplaint != null,ComplaintPic::getIsComplaint, isComplaint);
		return list(eq);
	}
	/**
	 * 根据complaint获取
	 * @param complaintIds
	 * @param isComplaint
	 * @return
	 */
	default List<ComplaintPic> getByComplaintIds(List<String> complaintIds,Boolean isComplaint){
		Assert.notEmpty(complaintIds,"complaintIds 不能为空");
		LambdaQueryWrapper<ComplaintPic> eq = Wrappers.<ComplaintPic>lambdaQuery().in(ComplaintPic::getComplaintId, complaintIds)
				.eq(isComplaint != null,ComplaintPic::getIsComplaint, isComplaint);
		return list(eq);
	}
}
