package scatter.complaint.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.pojo.vo.ComplaintPicVo;
import scatter.complaint.pojo.form.ComplaintPicAddForm;
import scatter.complaint.pojo.form.ComplaintPicUpdateForm;
import scatter.complaint.pojo.form.ComplaintPicPageQueryForm;

/**
 * <p>
 * 举报图片 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComplaintPicMapStruct extends IBaseVoMapStruct<ComplaintPic, ComplaintPicVo>,
                                  IBaseAddFormMapStruct<ComplaintPic,ComplaintPicAddForm>,
                                  IBaseUpdateFormMapStruct<ComplaintPic,ComplaintPicUpdateForm>,
                                  IBaseQueryFormMapStruct<ComplaintPic,ComplaintPicPageQueryForm>{
    ComplaintPicMapStruct INSTANCE = Mappers.getMapper( ComplaintPicMapStruct.class );

}
