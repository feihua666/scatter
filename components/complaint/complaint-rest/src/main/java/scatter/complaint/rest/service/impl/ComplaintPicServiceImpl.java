package scatter.complaint.rest.service.impl;

import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.rest.mapper.ComplaintPicMapper;
import scatter.complaint.rest.service.IComplaintPicService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.complaint.pojo.form.ComplaintPicAddForm;
import scatter.complaint.pojo.form.ComplaintPicUpdateForm;
import scatter.complaint.pojo.form.ComplaintPicPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 举报图片表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Service
@Transactional
public class ComplaintPicServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComplaintPicMapper, ComplaintPic, ComplaintPicAddForm, ComplaintPicUpdateForm, ComplaintPicPageQueryForm> implements IComplaintPicService {
    @Override
    public void preAdd(ComplaintPicAddForm addForm,ComplaintPic po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(ComplaintPicUpdateForm updateForm,ComplaintPic po) {
        super.preUpdate(updateForm,po);

    }
}
