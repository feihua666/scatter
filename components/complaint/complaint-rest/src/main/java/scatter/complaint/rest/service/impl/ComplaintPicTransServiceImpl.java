package scatter.complaint.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.rest.service.IComplaintPicService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 举报图片翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Component
public class ComplaintPicTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IComplaintPicService complaintPicService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,ComplaintPic.TRANS_COMPLAINTPIC_BY_ID,ComplaintPic.TRANS_REPLY_COMPLAINTPIC_BY_COMPLAINT_ID,ComplaintPic.TRANS_COMPLAINT_COMPLAINTPIC_BY_COMPLAINT_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,ComplaintPic.TRANS_COMPLAINTPIC_BY_ID)) {
            ComplaintPic byId = complaintPicService.getById(key);
            return new TransResult(byId,key);
        }else if(isEqual(type,ComplaintPic.TRANS_REPLY_COMPLAINTPIC_BY_COMPLAINT_ID)){
            List<ComplaintPic> byComplaint = complaintPicService.getByComplaintId(key, false);
            List<String> collect = byComplaint.stream().map(ComplaintPic::getPicUrl).collect(Collectors.toList());
            return new TransResult(collect,key);
        }else if(isEqual(type,ComplaintPic.TRANS_COMPLAINT_COMPLAINTPIC_BY_COMPLAINT_ID)){
            List<ComplaintPic> byComplaint = complaintPicService.getByComplaintId(key, true);
            List<String> collect = byComplaint.stream().map(ComplaintPic::getPicUrl).collect(Collectors.toList());
            return new TransResult(collect,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,ComplaintPic.TRANS_COMPLAINTPIC_BY_ID)) {
            return complaintPicService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }else if(isEqual(type,ComplaintPic.TRANS_REPLY_COMPLAINTPIC_BY_COMPLAINT_ID)){
            List<ComplaintPic> byComplaint = complaintPicService.getByComplaintIds(new ArrayList<>(keys), false);
            return wrapPicUrlsByComplaintId(byComplaint);
        }else if(isEqual(type,ComplaintPic.TRANS_COMPLAINT_COMPLAINTPIC_BY_COMPLAINT_ID)){
            List<ComplaintPic> byComplaint = complaintPicService.getByComplaintIds(new ArrayList<>(keys), true);
            return wrapPicUrlsByComplaintId(byComplaint);
        }
        return null;
    }

    private List<TransResult<Object, String>> wrapPicUrlsByComplaintId(List<ComplaintPic> complaintPics){
        Map<String,List<String>> map = new HashMap<>();
        for (ComplaintPic complaintPic : complaintPics) {
            List<String> strings = map.get(complaintPic.getComplaintId());
            if(isEmpty(strings)){
                strings = new ArrayList<>();
                map.put(complaintPic.getComplaintId(),strings);
            }
            strings.add(complaintPic.getPicUrl());

        }
        List<TransResult<Object, String>> results = new ArrayList<>();
        for (String key : map.keySet()) {
            results.add(new TransResult<>(map.get(key),key));
        }
        return results;
    }
}
