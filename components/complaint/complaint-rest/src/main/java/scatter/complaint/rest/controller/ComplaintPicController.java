package scatter.complaint.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.complaint.rest.ComplaintConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.pojo.vo.ComplaintPicVo;
import scatter.complaint.pojo.form.ComplaintPicAddForm;
import scatter.complaint.pojo.form.ComplaintPicUpdateForm;
import scatter.complaint.pojo.form.ComplaintPicPageQueryForm;
import scatter.complaint.rest.service.IComplaintPicService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 举报图片表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@Api(tags = "举报图片相关接口")
@RestController
@RequestMapping(ComplaintConfiguration.CONTROLLER_BASE_PATH + "/complaint-pic")
public class ComplaintPicController extends BaseAddUpdateQueryFormController<ComplaintPic, ComplaintPicVo, ComplaintPicAddForm, ComplaintPicUpdateForm, ComplaintPicPageQueryForm> {
    @Autowired
    private IComplaintPicService iComplaintPicService;

    @ApiOperation("添加举报图片")
    @PreAuthorize("hasAuthority('ComplaintPic:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ComplaintPicVo add(@RequestBody @Valid ComplaintPicAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询举报图片")
    @PreAuthorize("hasAuthority('ComplaintPic:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ComplaintPicVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除举报图片")
    @PreAuthorize("hasAuthority('ComplaintPic:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新举报图片")
    @PreAuthorize("hasAuthority('ComplaintPic:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ComplaintPicVo update(@RequestBody @Valid ComplaintPicUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询举报图片")
    @PreAuthorize("hasAuthority('ComplaintPic:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ComplaintPicVo> getList(ComplaintPicPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询举报图片")
    @PreAuthorize("hasAuthority('ComplaintPic:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ComplaintPicVo> getPage(ComplaintPicPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
