package scatter.complaint.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.complaint.pojo.po.Complaint;
import scatter.complaint.rest.service.IComplaintService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 举报投诉翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Component
public class ComplaintTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IComplaintService complaintService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Complaint.TRANS_COMPLAINT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,Complaint.TRANS_COMPLAINT_BY_ID)) {
            Complaint byId = complaintService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,Complaint.TRANS_COMPLAINT_BY_ID)) {
            return complaintService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
