package scatter.complaint.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.complaint.rest.ComplaintConfiguration;
import scatter.complaint.pojo.po.ComplaintPic;
import scatter.complaint.rest.service.IComplaintPicService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 举报图片表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
@RestController
@RequestMapping(ComplaintConfiguration.CONTROLLER_BASE_PATH + "/inner/complaint-pic")
public class ComplaintPicInnerController extends BaseInnerController<ComplaintPic> {
 @Autowired
 private IComplaintPicService complaintPicService;

 public IComplaintPicService getService(){
     return complaintPicService;
 }
}
