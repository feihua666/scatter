package scatter.complaint.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.exception.BusinessException;
import scatter.complaint.pojo.form.*;
import scatter.complaint.rest.ComplaintConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.complaint.pojo.po.Complaint;
import scatter.complaint.pojo.vo.ComplaintVo;
import scatter.complaint.rest.service.IComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
/**
 * <p>
 * 举报投诉表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Api(tags = "举报投诉相关接口")
@RestController
@RequestMapping(ComplaintConfiguration.CONTROLLER_BASE_PATH + "/complaint")
public class ComplaintController extends BaseAddUpdateQueryFormController<Complaint, ComplaintVo, ComplaintAddForm, ComplaintUpdateForm, ComplaintPageQueryForm> {
    @Autowired
    private IComplaintService iComplaintService;

    @ApiOperation("添加举报投诉")
    @PreAuthorize("hasAuthority('Complaint:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ComplaintVo add(@RequestBody @Valid ComplaintAddForm addForm, @ApiIgnore LoginUser loginUser) {
        addForm.setComplaintUserId(Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询举报投诉")
    @PreAuthorize("hasAuthority('Complaint:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ComplaintVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除举报投诉")
    @PreAuthorize("hasAuthority('Complaint:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("不分页查询举报投诉")
    @PreAuthorize("hasAuthority('Complaint:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<ComplaintVo> getList(ComplaintPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询举报投诉")
    @PreAuthorize("hasAuthority('Complaint:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<ComplaintVo> getPage(ComplaintPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    /**
     * 后台回复
     * @param replyForm
     * @return
     */
    @ApiOperation("回复举报")
    @PreAuthorize("hasAuthority('Complaint:reply')")
    @PostMapping("/reply")
    @ResponseStatus(HttpStatus.CREATED)
    public ComplaintVo reply(@RequestBody @Valid ComplaintReplyForm replyForm, @ApiIgnore LoginUser loginUser) {
        replyForm.setReplyUserId(Optional.ofNullable(loginUser).map(l->l.getId()).orElse(null));
        boolean reply = iComplaintService.reply(replyForm);
        if (!reply) {
            throw new BusinessException("回复失败，请刷新后尝试");
        }
        return queryById(replyForm.getId());
    }

    @ApiOperation("获取我的举报")
    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/getMyComplaintPage")
    @ResponseStatus(HttpStatus.OK)
    public IPage<ComplaintVo> myComplaint(ComplaintForMePageQueryForm listPageForm, @ApiIgnore LoginUser loginUser) {
        return super.pagePoToVo(iComplaintService.getPageBySuggestUserId(listPageForm,loginUser.getId()));
    }
}
