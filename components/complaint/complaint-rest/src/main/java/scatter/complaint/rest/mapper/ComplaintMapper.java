package scatter.complaint.rest.mapper;

import scatter.complaint.pojo.po.Complaint;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 举报投诉表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
public interface ComplaintMapper extends IBaseMapper<Complaint> {

}
