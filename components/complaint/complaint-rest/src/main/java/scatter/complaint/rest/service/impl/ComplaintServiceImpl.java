package scatter.complaint.rest.service.impl;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import scatter.common.rest.notify.NotifyParam;
import scatter.common.rest.notify.NotifyTool;
import scatter.complaint.pojo.form.ComplaintReplyForm;
import scatter.complaint.pojo.po.Complaint;
import scatter.complaint.rest.mapper.ComplaintMapper;
import scatter.complaint.rest.mapstruct.ComplaintMapStruct;
import scatter.complaint.rest.service.IComplaintPicService;
import scatter.complaint.rest.service.IComplaintService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.complaint.pojo.form.ComplaintAddForm;
import scatter.complaint.pojo.form.ComplaintUpdateForm;
import scatter.complaint.pojo.form.ComplaintPageQueryForm;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p>
 * 举报投诉表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@Service
@Transactional
public class ComplaintServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<ComplaintMapper, Complaint, ComplaintAddForm, ComplaintUpdateForm, ComplaintPageQueryForm> implements IComplaintService {

    @Lazy
    @Autowired
    private IComplaintPicService iComplaintPicService;

    @Override
    public void preAdd(ComplaintAddForm addForm,Complaint po) {
        po.setComplaintAt(LocalDateTime.now());
        super.preAdd(addForm,po);
    }

    @Override
    public void postAdd(ComplaintAddForm complaintAddForm, Complaint po) {
        // 设置首次举报id，以中联起一个话题
        if (isStrEmpty(po.getFirstComplaintId())) {
            po.setFirstComplaintId(po.getId());
            updateById(po);
        }
        /**
         * 如果图片不为空，添加图片
         */
        if(!isEmpty(complaintAddForm.getPicUrls())){
            iComplaintPicService.addPicUrls(complaintAddForm.getPicUrls(), po.getId(),true);
        }

        // 添加完举报，发一下消息提醒回复
        NotifyParam notifyParam = NotifyParam.business().setContentType("complaint.add")
                .setTitle("举报提醒")
                .setContent(StrUtil.format("有新的举报，请尽快回复，内容={}", po.getComplaintContent()));
        NotifyTool.notify(notifyParam);
    }

    @Override
    protected void postDeleteById(String id, Complaint po) {
        iComplaintPicService.deletePicUrls(id,null);
    }

    @Override
    public boolean reply(ComplaintReplyForm replyForm) {
        Complaint complaint = ComplaintMapStruct.INSTANCE.replyFormToPo(replyForm);
        complaint.setReplyAt(LocalDateTime.now());
        boolean b = updateById(complaint);
        if (b) {
            if (!isEmpty(replyForm.getPicUrls())) {
                iComplaintPicService.addPicUrls(replyForm.getPicUrls(),replyForm.getId(),false);
            }
            // 添加完举报，发一下消息提醒回复
            NotifyParam notifyParam = NotifyParam.business().setContentType("complaint.reply")
                    .setTitle("举报回复提醒")
                    .setContent(StrUtil.format("举报已回复，请知悉，内容={}", replyForm.getReplyContent()));
            NotifyTool.notify(notifyParam);
        }
        return b;
    }
}
