package scatter.complaint.rest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.Assert;
import scatter.complaint.pojo.form.*;
import scatter.complaint.pojo.po.Complaint;
import scatter.common.rest.service.IBaseService;
import scatter.complaint.rest.mapstruct.ComplaintMapStruct;

import java.util.List;
/**
 * <p>
 * 举报投诉表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
public interface IComplaintService extends IBaseService<Complaint> {


	/**
	 * 回复
	 * @param replyForm
	 * @return
	 */
	public boolean reply(ComplaintReplyForm replyForm);

	/**
	 * 查询用户发起的意见反馈
	 * @param queryForm
	 * @param complaintUserId
	 * @return
	 */
	default public IPage<Complaint> getPageBySuggestUserId(ComplaintForMePageQueryForm queryForm, String complaintUserId){
		Assert.hasText(complaintUserId,"complaintUserId 不能为空");
		Page page = convertPage(queryForm);
		Complaint complaint = ComplaintMapStruct.INSTANCE.complaintForMePageQueryFormToPo(queryForm);
		complaint.setComplaintUserId(complaintUserId);
		return page(page,Wrappers.<Complaint>query(complaint).lambda().orderByDesc(Complaint::getComplaintAt));
	}
}
