package scatter.complaint.rest.mapper;

import scatter.complaint.pojo.po.ComplaintPic;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 举报图片表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-11-10
 */
public interface ComplaintPicMapper extends IBaseMapper<ComplaintPic> {

}
