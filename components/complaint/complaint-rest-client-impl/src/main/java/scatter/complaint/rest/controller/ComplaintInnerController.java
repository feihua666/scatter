package scatter.complaint.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.complaint.rest.ComplaintConfiguration;
import scatter.complaint.pojo.po.Complaint;
import scatter.complaint.rest.service.IComplaintService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 举报投诉表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-11-15
 */
@RestController
@RequestMapping(ComplaintConfiguration.CONTROLLER_BASE_PATH + "/inner/complaint")
public class ComplaintInnerController extends BaseInnerController<Complaint> {
 @Autowired
 private IComplaintService complaintService;

 public IComplaintService getService(){
     return complaintService;
 }
}
