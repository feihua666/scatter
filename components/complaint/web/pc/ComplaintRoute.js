import ComplaintUrl from './ComplaintUrl.js'

const ComplaintRoute = [
    {
        path: ComplaintUrl.router.searchList,
        component: () => import('./element/ComplaintSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComplaintSearchList',
            name: '举报投诉管理'
        }
    },
    {
        path: ComplaintUrl.router.add,
        component: () => import('./element/ComplaintAdd'),
        meta: {
            code:'ComplaintAdd',
            name: '举报投诉添加'
        }
    },
    {
        path: ComplaintUrl.router.reply,
        component: () => import('./element/ComplaintReply'),
        meta: {
            code:'ComplaintReply',
            name: '举报投诉回复'
        }
    },
]
export default ComplaintRoute