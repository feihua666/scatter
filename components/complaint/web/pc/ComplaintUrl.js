const basePath = '' + '/complaint'
const ComplaintUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',

    reply: basePath + '/reply',
    router: {
        searchList: '/ComplaintSearchList',
        add: '/ComplaintAdd',
        reply: '/ComplaintReply',
    }
}
export default ComplaintUrl