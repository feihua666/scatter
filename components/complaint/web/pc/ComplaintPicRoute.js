import ComplaintPicUrl from './ComplaintPicUrl.js'

const ComplaintPicRoute = [
    {
        path: ComplaintPicUrl.router.searchList,
        component: () => import('./element/ComplaintPicSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'ComplaintPicSearchList',
            name: '举报图片管理'
        }
    },
    {
        path: ComplaintPicUrl.router.add,
        component: () => import('./element/ComplaintPicAdd'),
        meta: {
            code:'ComplaintPicAdd',
            name: '举报图片添加'
        }
    },
    {
        path: ComplaintPicUrl.router.update,
        component: () => import('./element/ComplaintPicUpdate'),
        meta: {
            code:'ComplaintPicUpdate',
            name: '举报图片修改'
        }
    },
]
export default ComplaintPicRoute