import ComplaintPicUrl from './ComplaintPicUrl.js'
const ComplaintPicForm = [
    {
        ComplaintPicSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'complaintId',
        },
        element:{
            label: '举报id',
            required: true,
        }
    },
    {
        ComplaintPicSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'picUrl',
        },
        element:{
            label: '图片地址',
            required: true,
        }
    },
    {
        ComplaintPicSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'isComplaint',
            value: false,
        },
        element:{
            type: 'switch',
            label: '是否举报图片',
            required: true,
        }
    },

]
export default ComplaintPicForm