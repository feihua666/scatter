const ComplaintTable = [
    {
        prop: 'dataId',
        label: '数据id'
    },
    {
        prop: 'dataType',
        label: '数据类型'
    },
    {
        prop: 'complaintContent',
        label: '举报内容'
    },
    {
        prop: 'complaintPicUrls',
        label: '举报配图',
        stype: 'image'
    },
    {
        prop: 'complaintAt',
        label: '举报时间'
    },
    {
        prop: 'complaintUserId',
        label: '用户id'
    },
    {
        prop: 'replyContent',
        label: '回复内容'
    },
    {
        prop: 'replyPicUrls',
        label: '回复配图',
        stype: 'image'
    },
    {
        prop: 'replyAt',
        label: '回复时间'
    },
    {
        prop: 'replyUserId',
        label: '回复用户id'
    },
    {
        prop: 'firstComplaintId',
        label: '第一条意见反馈id'
    },
]
export default ComplaintTable