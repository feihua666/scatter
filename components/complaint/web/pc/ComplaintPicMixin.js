import ComplaintPicForm from './ComplaintPicForm.js'
import ComplaintPicTable from './ComplaintPicTable.js'
import ComplaintPicUrl from './ComplaintPicUrl.js'
const ComplaintPicMixin = {
    computed: {
        ComplaintPicFormOptions() {
            return this.$stDynamicFormTools.options(ComplaintPicForm,this.$options.name)
        },
        ComplaintPicTableOptions() {
            return ComplaintPicTable
        },
        ComplaintPicUrl(){
            return ComplaintPicUrl
        }
    },
}
export default ComplaintPicMixin