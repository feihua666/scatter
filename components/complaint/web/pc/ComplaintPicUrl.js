const basePath = '' + '/complaint-pic'
const ComplaintPicUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/ComplaintPicSearchList',
        add: '/ComplaintPicAdd',
        update: '/ComplaintPicUpdate',
    }
}
export default ComplaintPicUrl