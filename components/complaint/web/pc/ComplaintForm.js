import ComplaintUrl from './ComplaintUrl.js'
const ComplaintForm = [
    {
        ComplaintSearchList: {
            element:{
                required: false
            }
        },
        ComplaintReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'dataId',
        },
        element:{
            label: '数据id',
            required: true,
        }
    },
    {
        ComplaintSearchList: {
            element:{
                required: false
            }
        },
        ComplaintReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'dataType',
        },
        element:{
            label: '数据类型',
            required: true,
        }
    },
    {
        ComplaintSearchList: false,
        ComplaintAdd: {
            element: {
                type: 'textarea',
                isBlock: true
            }
        },
        ComplaintReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'complaintContent',
        },
        element:{
            label: '举报内容',
            required: true,
        }
    },
    {
        // 该字段添加举报时用
        ComplaintSearchList:false,
        ComplaintReply: false,
        field: {
            name: 'picUrls',
        },
        element:{
            label: '举报配图',
            type: 'uploadFile',
            options: {
                triggerType: 'card',
                accept: 'image/*',
                listType: 'picture-card'
            },
        }
    },
    {
        ComplaintAdd: false,
        ComplaintReply: {
            element: {
                type: 'txt'
            }
        },
        field: {
            name: 'complaintUserId',
        },
        element:{
            label: '举报用户id',
        }
    },
    {
        // 该字段回复用，仅做展示用
        ComplaintSearchList:false,
        ComplaintAdd: false,
        field: {
            name: 'complaintPicUrls',
        },
        element:{
            label: '举报配图',
            type: 'image',
        }
    },
    {
        ComplaintSearchList: false,
        ComplaintAdd: false,
        field: {
            name: 'replyContent',
        },
        element:{
            label: '回复内容',
            type: 'textarea',
            required: true,
            isBlock: true,
        }
    },
    {
        // 该字段回复用配图
        ComplaintSearchList:false,
        ComplaintAdd: false,
        field: {
            name: 'picUrls',
        },
        element:{
            label: '回复配图',
            type: 'uploadFile',
            options: {
                triggerType: 'card',
                accept: 'image/*',
                listType: 'picture-card',
                tips: '请等待图片上传完成后再提交'
            },
        }
    },
    {
        ComplaintAdd: false,
        ComplaintReply: false,
        field: {
            name: 'firstComplaintId',
        },
        element:{
            label: '第一条意见反馈id',
        }
    },

]
export default ComplaintForm