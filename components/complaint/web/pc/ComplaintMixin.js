import ComplaintForm from './ComplaintForm.js'
import ComplaintTable from './ComplaintTable.js'
import ComplaintUrl from './ComplaintUrl.js'
const ComplaintMixin = {
    computed: {
        ComplaintFormOptions() {
            return this.$stDynamicFormTools.options(ComplaintForm,this.$options.name)
        },
        ComplaintTableOptions() {
            return ComplaintTable
        },
        ComplaintUrl(){
            return ComplaintUrl
        }
    },
}
export default ComplaintMixin