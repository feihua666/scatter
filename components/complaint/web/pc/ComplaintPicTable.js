const ComplaintPicTable = [
    {
        prop: 'complaintId',
        label: '举报id'
    },
    {
        prop: 'picUrl',
        label: '图片地址'
    },
    {
        prop: 'isComplaint',
        label: '是否举报图片'
    },
]
export default ComplaintPicTable