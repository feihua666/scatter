package scatter.test.rest.test.temp.yd;

import cn.hutool.json.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-06-23 11:55
 */
public class JsonTest {
	public static void main(String[] args) {
		Map map = JSONUtil.toBean("{\n" +
				"\t\"code\": 11431,\n" +
				"\t\"message\": \"固定价房价代码关联酒店时，没有配置价格，不会关联该酒店下的任何房型。若需关联，请先配置价格\",\n" +
				"\t\"result\": null\n" +
				"}", Map.class);

		System.out.println(map);
	}
}
