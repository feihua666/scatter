package scatter.test.rest.test.temp.yd;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcelFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import scatter.common.rest.tools.HttpClientTool;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 房价代码导入生成sql
 * </p>
 *
 * @author yangwei
 * @since 2021-06-22 14:29
 */
@Slf4j
public class RateCodeImportTest {


	// 测试环境
	//private String domain = "http://beta-mix2.api.crs.corp.at-our.com";
	//生产环境
	private String domain = "http://api.crs.corp.yaduo.com";

	// 请求的cookie 需登录后获取手动拷贝到这里来
	// 测试环境
	//private static String cookie = "asso_token=ebad092f7db24e5aae6e74fe68d38c0e; service_ticket_CRS=5b9e11792e494875a130f1a57344e85a; global_token=";
	// 生产环境
	private static String cookie = "asso_token=4c84e22d940840cb88f2cfe1c0cbfe56; service_ticket_CRS=9e6bf6a9076b427b9fc57dd93f821921; service_ticket_CRM=365149e0a3574871a185d0c6ac1e4414; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22J4Pr387j%22%2C%22first_id%22%3A%2217bdeae7a5e382-0e2354d6d2464a8-455f6c-2007040-17bdeae7a5fc0b%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%2217bdeae7a5e382-0e2354d6d2464a8-455f6c-2007040-17bdeae7a5fc0b%22%7D; service_ticket_TMC=ea3c0a0b4f174dc29f5c1c5067604fbe; global_token=9e6bf6a9076b427b9fc57dd93f821921";







	// 周末定义更新删除接口
	private String weekendUpdataUrl = domain + "/weekend/update?source=CRS";
	// 周末定义查询接口
	private String weekendQueryUrl = domain + "/weekend/query?chainId=${chainId}&rateCode=${rateCode}&source=CRS";

	//添加房价酒店关联接口
	private String addRalatedChain = domain + "/rateCode/fixedPrice/addRelatedChain?source=CRS";
	private String updateRalatedChain = domain + "/rateCode/fixedPrice/updateRelatedChain?source=CRS";
	// 删除关联
	private String deleteRelatedChain = domain + "/rateCode/fixedPrice/deleteRelatedChain?chainId=${chainId}&rateCode=${rateCode}&source=CRS";


	private boolean importOnly = false;

	// 待上传的数据
	//private String excelPath = "/Users/yw/temp/test固定价上传.xlsx";
	//private String excelPath = "/Users/yw/temp/副本test固定价上传.xlsx";
	// String excelPath = "/Users/yw/temp/副本test固定价上传的副本.xlsx";
	private String excelPath = "/Users/yw/yaduo/导入/价格代码导入-2021年携程尊享价.xlsx";

	private String weekendSuccessPath = "/Users/yw/yaduo/导入/weekendSuccess.txt";
	private String rateCodeSuccessPath = "/Users/yw/yaduo/导入/rateCodeSuccess.txt";
	private String rateCodeErrorPath = "/Users/yw/yaduo/导入/rateCodeError.txt";

	//
	private String excelRoomTypePath = "/Users/yw/yaduo/导入/roomtype.xlsx";
	private List<List<Object>> datas = new ArrayList<>(3);
	public static void main(String[] args) throws FileNotFoundException {
		RateCodeImportTest rateCodeImportTest = new RateCodeImportTest();
		rateCodeImportTest.readExcel(null);


		// 添加周末定义
		rateCodeImportTest.addRateCodeHotelWeekend();
		// 添加价格
		rateCodeImportTest.addRateCode();

		//rateCodeImportTest.deleteRateCodeHotelWeekend("114","haha13");
		//rateCodeImportTest.deleteRelatedChain("114","haha13");

	}



	private List<Header> getHeader(){
		Header header = new BasicHeader("Cookie",cookie);

		return Arrays.asList(header);
	}

	/**
	 *
	 * @param excelPath
	 * @throws FileNotFoundException
	 */
	private void readExcel(String excelPath) throws FileNotFoundException {
		if (excelPath == null) {
			excelPath = this.excelPath;
		}
		for (int i = 0; i < 3; i++) {
			// sheet从1开始
			List<Object> read =	EasyExcelFactory.read(excelPath).sheet(i).doReadSync();
			datas.add(read);
		}
		System.out.println();
	}

	private Map<String,String> roomType = new HashMap<>();
	// 根据房型编码获取id
	private String getRoomTypeIdByType(String Type){

		if (roomType.isEmpty()) {

			List<Object> read =	EasyExcelFactory.read(excelRoomTypePath).sheet(0).doReadSync();
			for (Object o : read) {
				LinkedHashMap map = (LinkedHashMap)o;
				if (map.get(0) == null) {
					continue;
				}
				String type = map.get(0).toString().trim();
				String id = map.get(1).toString().trim();
				roomType.put(type,id);
			}

		}
		return roomType.get(Type);
	}

	/**
	 * post数据样例
	 * {
	 * 	"chainId": 114,
	 * 	"rateCode": "haha13",
	 * 	"source": "CRS",
	 * 	"weekends": [
	 *                {
	 * 			"dayOfWeek": [
	 * 				2,
	 * 				3,
	 * 				6
	 * 			],
	 * 			"deleted": 0,
	 * 			"id": 1744,
	 * 			"name": "周末定义0"
	 *        },
	 *        {
	 * 			"dayOfWeek": [
	 * 				1
	 * 			],
	 * 			"deleted": 0,
	 * 			"id": 0,
	 * 			"name": "周末定义1"
	 *        }
	 * 	]
	 * }
	 * @return
	 */
	// 周末定义
	private static String hotelWeekendPostTemp = "{\"chainId\":${chainId},\"rateCode\":\"${rateCode}\",\"source\":\"CRS\",\"weekends\":[${weekends}]}";
	private static String hotelWeekendWeekendsTemp = "{\"dayOfWeek\":[${dayOfWeek}],\"deleted\":0,\"id\":0,\"name\":\"周末定义${name}\"}";

	/**
	 * 添加周末定义
	 */
	private void addRateCodeHotelWeekend(){
		List<Object> objects = datas.get(1);
		Map<String,String> weekendsMap = new HashMap<>();
		// 从1开始，0是标题
		for (int i = 0; i < objects.size(); i++) {
			LinkedHashMap row = ((LinkedHashMap) objects.get(i));

			String chainId = row.get(0).toString().trim();
			String rateCode = row.get(1).toString().trim();
			String weekNum = row.get(2).toString().trim();
			String weekDays = row.get(3).toString().trim();
			if ("0".equals(weekNum)) {
				log.warn("周末定义忽略，chainId={}，rateCode={}，weekNum={}",chainId,rateCode,weekNum);
				continue;
			}
			String key = chainId + "_" + rateCode;
			weekNum = (NumberUtil.parseInt(weekNum) - 1) +"";
			if (!weekendsMap.containsKey(key)) {
				weekendsMap.put(key,hotelWeekendWeekendsTemp.replace("${dayOfWeek}",weekDays).replace("${name}",weekNum));
			}else {
				String value = weekendsMap.get(key);
				weekendsMap.put(key,value + "," + hotelWeekendWeekendsTemp.replace("${dayOfWeek}",weekDays).replace("${name}",weekNum));

			}

		}// end for
		Map<String,String> postDataMap = new HashMap<>();
		weekendsMap.forEach((key,value)->{
			String[] k = key.split("_");
			String chainId = k[0].toString().trim();
			String rateCode = k[1].toString().trim();
			postDataMap.put(key,hotelWeekendPostTemp.replace("${chainId}",chainId).replace("${rateCode}",rateCode).replace("${weekends}",value));
		});

		if (postDataMap.isEmpty()) {
			log.info("无周末定义可添加");
		}


		// 添加之前清空记录的成功内容
		FileUtil.del(weekendSuccessPath);

		postDataMap.forEach((key,value)->{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("sleep失败，key={},value={}",key,value,e);
			}
			String[] k = key.split("_");
			String chainId = k[0].toString().trim();
			String rateCode = k[1].toString().trim();
			String url = weekendUpdataUrl.replace("${chainId}",chainId).replace("${rateCode}",rateCode);
			System.out.println("---------------------------------------------------");
			log.info("添加周末定义 请求url={},请求参数={}",url,value);

			try {
				String json = HttpClientTool.httpPostJson(url, value, getHeader());
				log.info("添加周末定义 请求结束，请求结果={}",json);
				Map map = JSONUtil.toBean(json, Map.class);
				if (!"0".equals(map.get("code").toString())) {
					log.error("添加周末定义 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));
					throw new RuntimeException("添加周末定义出错，请求结果 code不是0");
				}else {
					//添加成功的记录到文件
					FileUtil.appendUtf8String(chainId+ "_" + rateCode +"\n",weekendSuccessPath);
				}
			} catch (IOException e) {
				log.error("添加周末定义 请求异常",e);
				throw new RuntimeException(e);
			}

		});
	}

	/**
	 * 删除周末定义
	 * @param chainId
	 * @param rateCode
	 */
	private static String hotelWeekendWeekendsDeleteTemp = "{\"dayOfWeek\":[${dayOfWeek}],\"deleted\":1,\"id\":${id},\"name\":\"${name}\"}";

	/**
	 * 删除周末定义
	 * @param chainId
	 * @param rateCode
	 */
	private void deleteRateCodeHotelWeekend(String chainId,String rateCode){
		List rateCodeHotelWeekend = getRateCodeHotelWeekend(chainId, rateCode);
		if (rateCodeHotelWeekend == null || rateCodeHotelWeekend.isEmpty()) {
			log.info("删除周末定义，未获取到数据");
			return;
		}
		List<String> weekends = new ArrayList<>();
		for (Object o : rateCodeHotelWeekend) {
			HashMap map = ((HashMap) o);
			JSONArray daysOfWeek = (JSONArray) map.get("daysOfWeek");
			String daysOfWeekStr = daysOfWeek.stream().map(item -> item.toString()).collect(Collectors.joining(",")).toString();
			weekends.add(
					hotelWeekendWeekendsDeleteTemp.replace("${name}",map.get("name").toString())
					.replace("${id}",map.get("id").toString())
					.replace("${dayOfWeek}",daysOfWeekStr)
			);
		}
		String weekendsStr = weekends.stream().collect(Collectors.joining(","));
		String postJson = hotelWeekendPostTemp.replace("${chainId}",chainId).replace("${rateCode}",rateCode).replace("${weekends}",weekendsStr);

		String url = weekendUpdataUrl.replace("${chainId}",chainId).replace("${rateCode}",rateCode);
		System.out.println("---------------------------------------------------");
		log.info("删除周末定义 请求url={},请求参数={}",url,postJson);

		try {
			String json = HttpClientTool.httpPostJson(url, postJson, getHeader());
			log.info("删除周末定义 请求结束，请求结果={}",json);
			Map map = JSONUtil.toBean(json, Map.class);
			if (!"0".equals(map.get("code").toString())) {
				log.error("删除周末定义 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));
			}
		} catch (IOException e) {
			log.error("删除周末定义 请求异常",e);
		}

	}

	private	Map<String,List<Map>> weekendCacheMap = new HashMap<>();

	/**
	 * 请求周末定义
	 * @param chainId
	 * @param rateCode
	 * @return
	 */
	private List getRateCodeHotelWeekend(String chainId,String rateCode){
		List<Map> maps = weekendCacheMap.get(chainId + rateCode);
		if (maps != null) {
			return maps;
		}
		String url = weekendQueryUrl.replace("${chainId}",chainId).replace("${rateCode}",rateCode);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			log.error("getRateCodeHotelWeekend sleep失败，chainId={},rateCode={}",chainId,rateCode,e);
		}
		try {
			String s = HttpClientTool.httpGet(url, getHeader());
			Map map = JSONUtil.toBean(s, Map.class);
			if (!"0".equals(map.get("code").toString())) {
				log.error("请求周末定义 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));
				return null;
			}
			JSONObject result = (JSONObject) map.get("result");
			maps = JSONUtil.toList((JSONArray) result.get("weekends"),Map.class);
			weekendCacheMap.put(chainId + rateCode,maps);
			return maps;
		} catch (IOException e) {
			log.error("请求周末定义 异常，url={}",url);
		}
		return null;
	}

	/**
	 *
	 */
	// 非永久生效
	private static String validDatesTempP = "{\"durationKey\":\"${durationKey}\",\"end\":\"${end}\",\"invalidDates\":[${invalidDates}],\"start\":\"${start}\"}";
	// 永久生效
	private static String validDatesTemp = "{\"durationKey\":\"${durationKey}\",\"invalidDates\":[]}";

	/**
	 * 添加房价代码和酒店关联
	 * addRelatedChain
	 */
	private void addRateCode(){

		// 生效时段
		List<Object> objects = datas.get(0);
		Map<String,List<Map<String,String>>> validDatesMap = new HashMap<>();

		Map<String,String> permanent = new HashMap<>();

		for (int i = 0; i < objects.size(); i++) {
			LinkedHashMap row = ((LinkedHashMap) objects.get(i));
			String chainId = row.get(0).toString().trim();
			String rateCode = row.get(1).toString().trim();
			String key = chainId + "_" + rateCode;

			permanent.put(key,"0");
			List<Map<String, String>> mapsList = validDatesMap.get(key);
			if (mapsList == null) {
				mapsList = new ArrayList<>();
				validDatesMap.put(key,mapsList);
			}

			Map<String,String> m = new HashMap<>();

			String index = row.get(2).toString().trim();
			m.put("index",index);
			String valid = row.get(3).toString().trim();
			if ("0".equals(valid)) {
				// 永久生效
				permanent.put(key,"1");
			}else {

				String[] validSplit = valid.split("-");
				String validStart = formatDate(validSplit[0]);
				String validEnd = formatDate(validSplit[1]);

				Object invalidObj = row.get(4);
				String invalid = invalidObj == null ? null: invalidObj.toString().trim();

				String invalids = null;

				if (StrUtil.isNotEmpty(invalid)) {
					List<String> t = new ArrayList<>();
					invalids = Arrays.stream(invalid.split(",")).map(item->{
						return "\"" + formatDate(item) + "\"";
					}).collect(Collectors.joining(","));
				}

				m.put("start",validStart);
				m.put("end",validEnd);
				m.put("durationKey", IdUtil.randomUUID());
				m.put("invalidDates", StrUtil.nullToEmpty(invalids));
			}

			mapsList.add(m);

		}// end for 生效时段 完毕

		System.out.println(validDatesMap);



		// 房型价格
		Map<String,List<Map<String,Object>>> priceMap = new HashMap<>();
		List<Object> objects2 = datas.get(2);
		for (Object o : objects2) {
			LinkedHashMap row = ((LinkedHashMap) o);
			String chainId = row.get(1).toString().trim();
			String rateCode = row.get(0).toString().trim();
			String oldKey = chainId + "_" + rateCode;

			String roomType = row.get(3).toString().trim();
			String roomTypeId = getRoomTypeIdByType(roomType);

			String key = chainId + "_" + rateCode + "_" + roomTypeId;

			String validTimeIndex = row.get(5).toString().trim();

			String weekendIndex = row.get(6).toString().trim();
			String price = row.get(7).toString().trim();

			// 价格时段
			List<Map<String, Object>> mapsList = priceMap.get(key);
			if (mapsList == null) {
				mapsList = new ArrayList<>();
				priceMap.put(key,mapsList);
			}


			Map<String,Object> priceTimeIndexMap = new HashMap<>();

			Map<String, Object> isExist = mapsList.stream().filter(m -> m.get("index").equals(validTimeIndex)).findFirst().orElse(null);
			//不存在
			if (isExist != null) {
				priceTimeIndexMap =  isExist;
			}else {
				mapsList.add(priceTimeIndexMap);
			}
			// weekendPrices
			List<Map<String, Object>> weekendPrices = (List<Map<String, Object>>) priceTimeIndexMap.get("weekendPrices");
			if (weekendPrices == null) {
				weekendPrices = new ArrayList<>();
				priceTimeIndexMap.put("weekendPrices",weekendPrices);
			}

			priceTimeIndexMap.put("index",validTimeIndex);

			//todo
			List<Map<String, String>> maps = validDatesMap.get(oldKey);
			Map<String, String> index = maps.stream().filter(m -> m.get("index").equals(validTimeIndex)).findFirst().orElse(null);
			if (index == null) {
				log.error("未匹配到时段，oldkey={},validTimeIndex={}",oldKey,validTimeIndex);
			}else {
				priceTimeIndexMap.put("durationKey",index.get("durationKey"));

			}
			if("0".equals(weekendIndex)){
				priceTimeIndexMap.put("normalPrice",price);
			}else {
				//获取周末定义
				List<Map> weekend = getRateCodeHotelWeekend(chainId, rateCode);

				Map<String,Object> weekendPricesItemMap = new HashMap<>();
				weekendPricesItemMap.put("price",price);
				Map name = weekend.stream().filter(m -> m.get("name").equals("周末定义" + (NumberUtil.parseInt(weekendIndex) - 1))).findFirst().orElse(null);
				if (name == null) {
					log.error("未匹配到周末定义 key={},weekend={}",key,JSONUtil.toJsonStr(weekend));
				}
				weekendPricesItemMap.put("weekendId",name.get("id"));
				weekendPrices.add(weekendPricesItemMap);
			}

		}// end for

		// 构造请求参数


		List<Map<String,Object>> r = new ArrayList<>();
		for (String k : priceMap.keySet()) {
			String[] keys = k.split("_");
			String chainId = keys[0].toString();
			String rateCode = keys[1].toString();
			String roomTypeId = keys[2].toString();
			String key = chainId +"_" + rateCode;
			Map<String, Object> stringObjectMap = r.stream().filter(m -> m.get("chainId").equals(chainId) && m.get("rateCode").equals(rateCode)).findFirst().orElse(null);
			if (stringObjectMap == null) {
				stringObjectMap = new HashMap<>();
				r.add(stringObjectMap);
				stringObjectMap.put("chainId",chainId);
				stringObjectMap.put("rateCode",rateCode);
				stringObjectMap.put("permanent",NumberUtil.parseInt(permanent.get(key)));
				stringObjectMap.put("priceConfig",new ArrayList<>());
				stringObjectMap.put("validDates","${validDates}");
				stringObjectMap.put("remark","");
				stringObjectMap.put("source","CRS");

			}
			List<Map> priceConfig = (List) stringObjectMap.get("priceConfig");
			Map roomTypeId1 = priceConfig.stream().filter(m -> m.get("roomTypeId").equals(roomTypeId)).findFirst().orElse(null);
			if (roomTypeId1 == null) {
				roomTypeId1 = new HashMap();
				priceConfig.add(roomTypeId1);
				roomTypeId1.put("roomTypeId",roomTypeId);
				roomTypeId1.put("price",new ArrayList<>());
			}

			List<Map> price = (List<Map>) roomTypeId1.get("price");
			List<Map<String, Object>> maps = priceMap.get(k);
			for (Map<String, Object> map : maps) {
				Map<String,Object> priceItem = new HashMap<>();
				price.add(priceItem);
				priceItem.put("durationKey",map.get("durationKey"));
				priceItem.put("normalPrice",map.get("normalPrice"));
				priceItem.put("weekendPrices",map.get("weekendPrices"));

			}


		}// end for

		// 添加之前清空记录的成功内容
		FileUtil.del(rateCodeSuccessPath);
		FileUtil.del(rateCodeErrorPath);
		// 重新拼装最终参数
		for (Map<String, Object> stringObjectMap : r) {
			stringObjectMap.put("version",1);
			String s = JSONUtil.toJsonStr(stringObjectMap);
			// 替换validdays
			List<Map<String, String>> maps = validDatesMap.get(stringObjectMap.get("chainId") + "_" + stringObjectMap.get("rateCode"));
			List<String> validays = new ArrayList<>();
			for (Map<String, String> map : maps) {
				if ("0".equals(stringObjectMap.get("permanent").toString())) {
					validays.add(validDatesTempP.replace("${durationKey}",map.get("durationKey"))
							.replace("${end}",map.get("end"))
							.replace("${start}",map.get("start"))
							.replace("${invalidDates}",map.get("invalidDates"))
					);
				}else {
					// 永久生效
					validays.add(validDatesTemp.replace("${durationKey}",map.get("durationKey"))
					);
				}

			}
			s = s.replace("\"${validDates}\"","["+validays.stream().collect(Collectors.joining(",")) + "]");


			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("addRateCode sleep失败，chainId={},rateCode={}",stringObjectMap.get("chainId"),stringObjectMap.get("rateCode"),e);
			}
			// 发起请求
			String url = addRalatedChain;
			System.out.println("---------------------------------------------------");
			log.info("添加房价 请求url={},请求参数={}",url,s);

			try {
				String json = HttpClientTool.httpPostJson(url, s, getHeader());
				log.info("添加房价 请求结束，请求结果={}",json);
				Map map = JSONUtil.toBean(json, Map.class);
				if (!"0".equals(map.get("code").toString())) {
					log.error("添加房价 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));

					String msg = map.get("message").toString();
					// 更新操作可能会有问题，只更新版本为1的数据慎用！
					if (!importOnly && msg.contains("房价代码和酒店已关联, 请勿重复关联")) {
						// 如果有重复直接更新
						updateRelatedChain(stringObjectMap,s);
					}else {
						FileUtil.appendUtf8String(stringObjectMap.get("chainId") + "_" + stringObjectMap.get("rateCode")+ " "+ map.get("code")+ "_" + map.get("message") + "\n",rateCodeErrorPath);
					}

				}else {
					FileUtil.appendUtf8String(stringObjectMap.get("chainId") + "_" + stringObjectMap.get("rateCode") + "\n",rateCodeSuccessPath);
				}
			} catch (IOException e) {
				log.error("添加房价 请求异常",e);
			}

		}

	}

	/**
	 * 更新酒店房价关联，主要是用于在有添加时提示已存在，进行更新处理
	 * @param stringObjectMap
	 * @param data
	 */
	private void updateRelatedChain(Map<String, Object> stringObjectMap,String data ){
		String url = updateRalatedChain;
		try {
			stringObjectMap.put("version",1);
			String json = HttpClientTool.httpPostJson(url, data, getHeader());
			log.info("更新房价 请求结束，请求结果={}",json);
			Map map = JSONUtil.toBean(json, Map.class);
			if (!"0".equals(map.get("code").toString())) {
				log.error("更新房价 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));
				FileUtil.appendUtf8String(stringObjectMap.get("chainId") + "_" + stringObjectMap.get("rateCode")+ " "+ map.get("code")+ "_" + map.get("message") + " 更新" + "\n",rateCodeErrorPath);

			}else {
				FileUtil.appendUtf8String(stringObjectMap.get("chainId") + "_" + stringObjectMap.get("rateCode") + " 更新" + "\n",rateCodeSuccessPath);
			}
		} catch (IOException e) {
			log.error("更新房价 请求异常",e);
		}
	}

	/**
	 * 删除房价代码和酒店关联
	 * @param chainId
	 * @param rateCode
	 */
	private void deleteRelatedChain(String chainId,String rateCode){
		// 发起请求
		String url = deleteRelatedChain.replace("${chainId}",chainId).replace("${rateCode}",rateCode);
		System.out.println("---------------------------------------------------");
		log.info("删除房价代码和酒店关联 请求url={}",url);

		try {
			String json = HttpClientTool.httpGet(url, getHeader());
			log.info("删除房价代码和酒店关联 请求结束，请求结果={}",json);
			Map map = JSONUtil.toBean(json, Map.class);
			if (!"0".equals(map.get("code").toString())) {
				log.error("删除房价代码和酒店关联 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));
			}
		} catch (IOException e) {
			log.error("删除房价代码和酒店关联 请求异常",e);
		}
	}
	/**
	 * 日期格式化 20210109 -> 2021-01-09
	 * @param d
	 * @return
	 */
	private String formatDate(String d){
		d = d.trim();
		return d.substring(0,4) +  "-" + d.substring(4,6) + "-" + d.substring(6).trim();
	}
}
