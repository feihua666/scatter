package scatter.test.rest.test.temp.yd;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcelFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import scatter.common.rest.tools.HttpClientTool;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 批量冻结协议客户卡号
 * </p>
 *
 * @author yangwei
 * @since 2021-09-16 10:48
 */
@Slf4j
public class CorpCustomTest {


	// 测试环境
	//private String domain = "http://beta-mix2.api.crm.corp.at-our.com";
	//生产环境
	private String domain = "https://crm.corp.yaduo.com/api";

	// 请求的cookie 需登录后获取手动拷贝到这里来
	// 测试环境
	//private static String cookie = "Token=4cb54dd027cf4ad5a732a04410ccdb20; xiaonengToken=xn-78ace812-c7b3-4d33-a6fd-abd1726961c2; asso_token=9e7bed92575644c08332076de64fd8e4; service_ticket_CRS=9b6cb736c85348a6a000abf60dacac79; service_ticket_CRM=bb59e4748c73419eacd074534411d413; service_ticket_OPS=74c65cc325be42babec1fd6fe3fac34c; service_ticket_PMS=4cb54dd027cf4ad5a732a04410ccdb20; service_ticket_RBAC=a0d6a20796a441fa9d88a3bc38771d23";
	// 生产环境
	private static String cookie = "asso_token=2a05a4a04ab7453a982025aeec4f8900; service_ticket_CRS=9e6bf6a9076b427b9fc57dd93f821921; service_ticket_CRM=2cad5458457549ea8296766d4c22de49; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22J4Pr387j%22%2C%22first_id%22%3A%2217bdeae7a5e382-0e2354d6d2464a8-455f6c-2007040-17bdeae7a5fc0b%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%2217bdeae7a5e382-0e2354d6d2464a8-455f6c-2007040-17bdeae7a5fc0b%22%7D; service_ticket_TMC=ea3c0a0b4f174dc29f5c1c5067604fbe; global_token=9e6bf6a9076b427b9fc57dd93f821921; acw_tc=2f61f27b16352132015712220e10e490d29fdf31f29d43ad91eef69b88b5ad";


	// 协议客户冻结接口
	private String closeUrl = domain + "/corp/updateState?r=cfaf67b65a9098&mebid={{mebid}}&state=5";

	private String excelPath = "/Users/yw/yaduo/冻结协卡号/美团飞猪三次清理1019.xlsx";
	private String closeErrorPath = "/Users/yw/yaduo/冻结协卡号/closeCorpMebidError.txt";

	// mebid所在的列 从0开始
	int mebidCol = 1;

	public static void main(String[] args) {
		CorpCustomTest corpCustomTest = new CorpCustomTest();
		corpCustomTest.closeCorp(null);
	}

	/**
	 * 读取excel形式的卡号
	 * @param excelPath
	 * @throws FileNotFoundException
	 */
	private List<String> readExcel(String excelPath) {
		log.info("读取数据开始");
		if (excelPath == null) {
			excelPath = this.excelPath;
		}
		List<Object> read =	EasyExcelFactory.read(excelPath).sheet(0).headRowNumber(0).doReadSync();
		List<String> result = read.stream().map(map -> ((Map)map).get(mebidCol)).filter(Objects::nonNull).map(Objects::toString).collect(Collectors.toList());
		log.info("读取数据结束总条数={}",read.size());
		return result;
	}

	/**
	 * 执行冻结逻辑
	 * @param excelPath
	 */
	public void closeCorp(String excelPath){
		log.info("冻结协议客户开始");

		List<String> strings = readExcel(excelPath);
		if (CollectionUtil.isEmpty(strings)) {
			log.warn("excel 没有数据可用，直接返回");
			return;
		}

		// 将error的删除
		FileUtil.del(closeErrorPath);
		int total = strings.size();
		int index = 0;
		for (String mebid : strings) {
			String url = closeUrl.replace("{{mebid}}",mebid);
			log.info("url={}",url);
			try {
				String json = HttpClientTool.httpGet(url, getHeader());
				log.info("冻结协议客户 请求结束，{}/{},请求结果={}",index++,total,json);
				Map map = JSONUtil.toBean(json, Map.class);
				if (!"0".equals(map.get("code").toString())) {
					log.error("冻结协议客户 请求结果有错，code={}，message={}",map.get("code"),map.get("message"));
					//添加失败的记录到文件
					FileUtil.appendUtf8String(mebid +" " + map.get("message") +"\n",closeErrorPath);
				}
			} catch (Exception e) {
				log.error("冻结协议客户 请求异常",e);
				//添加失败的记录到文件
				FileUtil.appendUtf8String(mebid +" " + e.getMessage() +"\n",closeErrorPath);
			}

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		log.info("冻结协议客户结束");
	}

	private List<Header> getHeader(){
		Header header = new BasicHeader("Cookie",cookie);

		return Arrays.asList(header);
	}
}
