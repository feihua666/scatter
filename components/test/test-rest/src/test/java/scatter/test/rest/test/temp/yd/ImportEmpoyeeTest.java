package scatter.test.rest.test.temp.yd;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcelFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-09 15:40
 */
public class ImportEmpoyeeTest {
	public static void main(String[] args) {
		String s = FileUtil.readUtf8String("/Users/yw/temp/importEmployee.json");
		Map map = JSONUtil.toBean(s, Map.class);
		Object result = map.get("result");
		Object list = ((JSONObject) result).get("list");
		List<String> stringList = new ArrayList<>();
		for (Object o : ((JSONArray) list)) {
			Object employeePhone = ((JSONObject) o).get("employeePhone");
			stringList.add(employeePhone.toString());
		}


		String path = "/Users/yw/temp/批量添加员工 1.xlsx";
		List<Object> read =	EasyExcelFactory.read(path).sheet(0).doReadSync();
		for (Object o : read) {
			Object o0 = ((Map) o).get(0);
			Object o1 = ((Map) o).get(2);

			if (!stringList.contains(o1.toString())) {
				System.out.println(o0 + " " +o1);
			}
		}


	}
}
