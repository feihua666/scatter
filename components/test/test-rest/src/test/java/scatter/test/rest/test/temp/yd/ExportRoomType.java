package scatter.test.rest.test.temp.yd;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONNull;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import lombok.Data;
import scatter.common.rest.tools.HttpClientTool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 导出房型图片信息
 * </p>
 *
 * @author yangwei
 * @since 2021-10-15 10:15
 */
public class ExportRoomType {


	static String urlTemp = "http://openapi.yaduo.com/atour/openapi/baoku/hotel/getRoomTypeList?appId=#{appId}&hotelId=#{hotelId}&sign=#{sign}";


	static Integer[] hotelIds = new Integer[]{

			610132,
			1301002,
			1301003,
			1401005,
			2101014,
			3100071,
			3201043,
			3201044,
			3202010,
			3202012,
			3206009,
			4101011,
			4201024,
			4201029,
			4201030,
			4304002,
			4403051,
			4404005,
			4404012,
			5101037,
			5101040,
			5114001
	};

	public static void main(String[] args) throws IOException {

		String appId = "169";
		// signKey需要从数据库获取
		String signKey = "c0c4c2fab2894fdcbcd3468719c7c3d7";

		// 随便找一个 appId 如：169
		List<ExportResult> results = new ArrayList<>();

		for (Integer hotelId : hotelIds) {
			Map<String,String> m = new HashMap<>();
			m.put("appId",appId);
			m.put("hotelId",hotelId.toString());
			String sign = SignUtil.sign(m, signKey);
			String url = urlTemp.replace("#{appId}",appId).replace("#{hotelId}",hotelId.toString()).replace("#{sign}",sign);

			String response = HttpClientTool.httpGet(url);

			JSONObject jsonObject = JSONUtil.parseObj(response);
			Object result = jsonObject.get("result");
			if (result == null|| result instanceof JSONNull) {
				continue;
			}
			JSONArray resultArray = (JSONArray) result;


			for (Object o : resultArray) {
				JSONObject room = (JSONObject) o;
				ExportResult exportResult = new ExportResult();
				results.add(exportResult);
				exportResult.setHotelId(hotelId);

				exportResult.setRoomTypeId(room.get("roomTypeId",Integer.class));
				exportResult.setRoomTypeName(room.get("roomTypeName",String.class));
				List<String> list = room.get("pictures", List.class);
				String collect = list.stream().map(Object::toString).collect(Collectors.joining("\n"));
				exportResult.setPictures(collect);
			}

			System.out.println();

		}


		// 写excel
		EasyExcel.write("/Users/yw/yaduo/temp/exportRoomType.xlsx")
				.sheet().doWrite(results);


	}


	@Data
	public static class ExportResult{
		@ExcelProperty(value = "hotelId")
		private Integer hotelId;
		private String hotelName;

		private Integer roomTypeId;
		private String roomTypeName;

		private String pictures;
	}


}
