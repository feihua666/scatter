import AreaRoute from '../../../../../../components/area/web/pc/AreaRoute.js'
import DictRoute from '../../../../../../components/dict/web/pc/DictRoute.js'
import FuncRoute from '../../../../../../components/func/web/pc/FuncRoute.js'
import RoleRoutesEntry from '../../../../../../components/role/web/pc/RoleRoutesEntry.js'
import UserSimpleRoute from '../../../../../../components/usersimple/web/pc/UserSimpleRoute.js'
import FuncGroupRoute from '../../../../../../components/func/web/pc/FuncGroupRoute.js'

let indexChildren = [].concat(AreaRoute)
.concat(DictRoute)
.concat(FuncRoute)
.concat(RoleRoutesEntry)
.concat(UserSimpleRoute)
.concat(FuncGroupRoute)

let routes = [
    {
        path: '/login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/index',
        component: () => import('../views/Index.vue'),
        children: indexChildren
    },
]

export default routes