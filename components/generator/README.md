# generator 代码生成器组件

基于mybatis plus generator实现的代码生成器组件

+ 支持单表常用crud的代码生成
+ 支持单表vue前端页面的生成

单表代码生成后，只需要对表单稍作修改即可使用  
具体使用方式请移步[这里](/guide/project/backend/components/component-create/)