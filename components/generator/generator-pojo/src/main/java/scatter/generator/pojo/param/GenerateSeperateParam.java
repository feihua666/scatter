package scatter.generator.pojo.param;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 代码生成参数
 * Created by yangwei
 * Created at 2020/11/4 13:26
 */
@Setter
@Getter
@Accessors(chain = true)
public class GenerateSeperateParam extends GenerateParam {

    /**
     * pojo 模块路径
     */
    private String pojoComponentPath = "";
    /**
     * rest 模块路径
     */
    private String restComponentPath = "";
    /**
     * rest-client 模块路径
     */
    private String restClientComponentPath = "";
    /**
     * rest-client-impl 模块路径
     */
    private String restClientImplComponentPath = "";
    /**
     * web页面路径
     */
    private String webPath = "";
}
