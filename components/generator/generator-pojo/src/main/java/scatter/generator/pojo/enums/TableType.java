package scatter.generator.pojo.enums;

/**
 * Created by yangwei
 * Created at 2020/11/4 13:36
 */
public enum TableType {
    /**
     * 常规表
     */
    normal,
    // 关系表
    rel,
    // 树表
    tree
}
