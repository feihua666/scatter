package scatter.generator.pojo.enums;

/**
 * Created by yangwei
 * Created at 2020/11/9 12:33
 */
public enum  CustomeTemplateType {

    ENTITY,
    SERVICE,
    CONTROLLER,
    MAPPER,
    XML,
    ADD_FORM,
    UPDATE_FORM,
    PAGE_QUERY_FORM,
    VO,
    MAP_STRUCT,
    ONE_ASSIGN_TWO_FORM,
    TWO_ASSIGN_ONE_FORM,
    // feiclient 相关
    INNER_CONTROLLER,
    CLIENT,
    // 数据表建表语句
    SCHEMA,
    // 测试 相关
    SERVICE_TEST,
    CONTROLLER_TEST,
    INNER_CONTROLLER_TEST,
    SUPER_TEST,
    // web 相关
    ADD,
    UPDATE,
    SEARCH_LIST,
    FORM,
    MIXIN,
    ROUTE,
    TABLE,
    URL,
    ONE_ASSIGN_TWO_VUE,
    TWO_ASSIGN_ONE_VUE,
}
