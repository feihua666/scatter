package scatter.generator.pojo.param;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import scatter.generator.pojo.enums.CustomeTemplateType;
import scatter.generator.pojo.enums.TableType;

/**
 * 代码生成参数
 * Created by yangwei
 * Created at 2020/11/4 13:26
 */
@Setter
@Getter
@Accessors(chain = true)
public class GenerateParam {

    /**
     * 作者
     */
    private String author;
    /**
     * 模块名称
     */
    private String moduleName;
    /**
     * 模块名称
     */
    private String componentName;

    /**
     * 表名
     */
    private String tableName;
    /**
     * 父包路径
     */
    private String packageParent;
    /**
     * 实体的父包路径
     */
    private String entityPackageParent;

    /**
     * 模块路径
     */
    private String componentPath = "";

    /**
     * 表类型
     */
    private TableType tableType;

    /**
     * 需要生成的模板
     */
    private CustomeTemplateType[] templates;
    /**
     * 表前缀
     */
    private String tablePrefix;

    /**
     * 是否覆盖已有文件
     */
    private boolean fileOverride = false;
    /**
     * 是否删除生成的文件
     */
    private boolean deleteFile = false;



}
