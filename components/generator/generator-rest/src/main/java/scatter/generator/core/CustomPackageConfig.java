package scatter.generator.core;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by yangwei
 * Created at 2021/2/9 10:44
 */
@Data
@Accessors(chain = true)
public class CustomPackageConfig extends PackageConfig {

    /**
     * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
     */
    private String parent = "com.baomidou";

    @Override
    public String getParent() {
        // 这里重写 不同时指定父级和和模块不拼接，这样可以留出moduleName字段来做它用
        if (StringUtils.isNotBlank(parent) && StringUtils.isNotBlank(getModuleName())) {
            return parent + StringPool.DOT + getModuleName();
        }
        return parent;
    }
}
