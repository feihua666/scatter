package scatter.generator.core;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-02-27 16:24
 */
public class CustomDataSourceConfig extends DataSourceConfig {

    private List<Connection> connectionList = new ArrayList<>();

    @Override
    public Connection getConn() {
        Connection connection =  super.getConn();
        connectionList.add(connection);
        return  connection;
    }

    // 关闭所有链接
    public void closeAllConnection() {
        for (Connection connection : connectionList) {
            try {
                connection.close();
            } catch (SQLException throwables) {
            }
        }
    }
}
