package scatter.generator.core;

/**
 * Created by yangwei
 * Created at 2021/2/24 12:26
 */
public class Utils {

    /**
     *
     * @param componentName
     * @return 返回小写转换为下划线
     */
    public static String componentNameConvertToUnderLine(String componentName) {
        String newComponentName = (componentName).replace("-", "_").toLowerCase();
        return newComponentName;
    }
    /**
     *
     * @param componentName
     * @return 返回小写并去掉连接线
     */
    public static String componentNameConvertToNoLine(String componentName) {
        String newComponentName = (componentName).replace("-", "_").toLowerCase();
        return newComponentName.replace("_","");
    }
}
