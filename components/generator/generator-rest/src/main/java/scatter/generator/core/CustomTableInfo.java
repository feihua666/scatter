package scatter.generator.core;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 扩展
 * Created by yangwei
 * Created at 2021/2/24 10:18
 */
@Data
@Accessors(chain = true)
public class CustomTableInfo extends TableInfo  {

    private String commentSimple;
    // 在表类型为rel是其注释应该是xxx和xxxx关系表

    // 对应 xxx
    private String commentRel1;
    // 对应 xxxx
    private String commentRel2;
}
