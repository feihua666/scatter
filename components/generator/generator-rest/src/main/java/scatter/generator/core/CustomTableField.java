package scatter.generator.core;

import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2020/11/5 11:13
 */
@Setter
@Getter
@Accessors(chain = true)
public class CustomTableField extends TableField {
    /**
     * 是否必填
     */
    private boolean required;
    /**
     * 是否唯一
     */
    private boolean unique;

    /**
     * 简化的列提示，主要用于验证提示，提取列注释的第一个逗号前面的
     */
    private String commentSimple;

    /**
     * 查询是是否模糊查询，如果列中注释包含 模糊查询
     */
    private boolean queryLike;
    /**
     * 是否排序，如果列中注释包含 排序
     */
    private boolean orderBy;
    /**
     * 排序方向，如果列中注释包含 升序或降序
     */
    private boolean orderByAsc = true;
    // 外键
    private boolean foreignKey = false;
    /**
     * 字段长度
     */
    private Integer length;
    /**
     * 小数位长度
     */
    private Integer fractionLength;
}
