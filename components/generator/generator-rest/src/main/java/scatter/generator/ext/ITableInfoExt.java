package scatter.generator.ext;

import scatter.generator.core.CustomTableField;
import scatter.generator.core.CustomTableInfo;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-02-26 17:02
 */
public interface ITableInfoExt {

     public void tableInfoExt(CustomTableInfo customTableInfo);

     public void tableColumnExt(CustomTableField customTableField);
}
