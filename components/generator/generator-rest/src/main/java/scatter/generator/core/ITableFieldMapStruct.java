package scatter.generator.core;

import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * Created by yangwei
 * Created at 2020/11/5 13:42
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ITableFieldMapStruct {
    ITableFieldMapStruct INSTANCE = Mappers.getMapper( ITableFieldMapStruct.class );

    CustomTableField map(TableField field);

    @Mapping(target = "importPackages",ignore = true)
    CustomTableInfo map(TableInfo tableInfo);
}
