package scatter.generator.core;

import com.baomidou.mybatisplus.generator.InjectionConfig;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * Created by yangwei
 * Created at 2021/2/24 12:32
 */
@Data
@Accessors(chain = true)
public class CustomInjectionConfig extends InjectionConfig {
    @Override
    public void initMap() {
        
    }
}
