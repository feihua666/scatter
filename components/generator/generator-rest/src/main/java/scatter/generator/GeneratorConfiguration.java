package scatter.generator;

import org.springframework.context.annotation.ComponentScan;

/**
 * Created by yangwei
 * Created at 2021/2/24 11:26
 */
@ComponentScan
public class GeneratorConfiguration {
}
