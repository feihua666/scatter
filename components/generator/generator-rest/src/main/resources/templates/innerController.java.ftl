package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import ${cfg.configurationPkg}.${cfg.configuration};
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import ${cfg.innerControllerSuperClass};

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 内部调用前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping(${cfg.configuration}.CONTROLLER_BASE_PATH + "/inner<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${cfg.innerController} extends ${cfg.innerControllerSuperClassName}<${entity}> {
<#else>
public class ${table.controllerName} {
</#if>
 @Autowired
 private ${table.serviceName} ${entity?uncap_first}Service;

 public ${table.serviceName} getService(){
     return ${entity?uncap_first}Service;
 }
}
</#if>
