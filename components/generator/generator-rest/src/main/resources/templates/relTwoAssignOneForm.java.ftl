package ${cfg.formPkg};

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
<#list table.importPackages as pkg>
import ${pkg};
</#list>
<#if swagger2>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
</#if>

import java.util.List;
/**
 * <p>
 * ${table.commentRel1}分配${table.commentRel2}表单对象
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@PropValid
@Setter
@Getter
</#if>
<#if swagger2>
@ApiModel(value="${table.commentRel2}分配${table.commentRel1}表单对象")
</#if>
public class ${cfg.twoAssignOneForm} extends BaseForm {
    private static final long serialVersionUID = 1L;
<#list table.fields as field>

    <#if field.propertyName == (cfg.relTwoPropertyName)>
    @NotEmpty(message = "${field.commentSimple!}不能为空")
        <#if field.comment!?length gt 0>
            <#if swagger2>
    @ApiModelProperty(value = "${field.commentSimple!}")
            <#else>
                /**
                * ${field.comment}
                */
            </#if>
        </#if>
    private ${field.propertyType} ${field.propertyName};

    </#if>

    <#if field.propertyName == (cfg.relOnePropertyName)>
        <#if field.comment!?length gt 0>
            <#if swagger2>
    @ApiModelProperty(value = "选择的${field.commentSimple!}")
            <#else>
                /**
                * ${field.comment}
                */
            </#if>
        </#if>
    private List<${field.propertyType}> checked${field.propertyName?cap_first}s;

    @PropValid.DependCondition(message = "未选择的${field.commentSimple!}不能为空",dependProp = "isLazyLoad",ifEqual = "true")
      <#if field.comment!?length gt 0>
            <#if swagger2>
    @ApiModelProperty(value = "未选择的${field.commentSimple!}",notes = "如果为懒加载请传该值")
            <#else>
                /**
                * ${field.comment}
                */
            </#if>
        </#if>
    private List<${field.propertyType}> unchecked${field.propertyName?cap_first}s;
    </#if>
</#list>
    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
