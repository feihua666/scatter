package ${package.ServiceImpl};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * ${cfg.tableComment}翻译实现
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Component
public class ${entity}TransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private ${table.serviceName} ${entity?uncap_first}Service;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,${entity}.TRANS_${entity?upper_case}_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,${entity}.TRANS_${entity?upper_case}_BY_ID)) {
            ${entity} byId = ${entity?uncap_first}Service.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,${entity}.TRANS_${entity?upper_case}_BY_ID)) {
            return ${entity?uncap_first}Service.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
