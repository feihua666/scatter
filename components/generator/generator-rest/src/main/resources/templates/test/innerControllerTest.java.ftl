package ${cfg.testInnerControllerPkg};
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ${cfg.testPkg}.${cfg.superTest};
/**
* <p>
* ${cfg.tableComment} 内部调用前端控制器测试类
* </p>
*
* @author ${author}
* @since ${date}
*/
@SpringBootTest
public class ${cfg.innerControllerTest} extends ${cfg.superTest}{
    @Test
    void contextLoads() {
    }
}