package ${cfg.testServicePkg};
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ${package.Entity}.${entity};
import ${cfg.formPkg}.${cfg.addForm};
import ${cfg.formPkg}.${cfg.updateForm};
import ${cfg.formPkg}.${cfg.pageQueryForm};
import ${cfg.testPkg}.${cfg.superTest};
import ${package.Service}.${table.serviceName};
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* ${cfg.tableComment} 服务测试类
* </p>
*
* @author ${author}
* @since ${date}
*/
@SpringBootTest
public class ${cfg.serviceTest} extends ${cfg.superTest}{

    @Autowired
    private ${table.serviceName} ${entity?uncap_first}Service;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<${entity}> pos = ${entity?uncap_first}Service.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}