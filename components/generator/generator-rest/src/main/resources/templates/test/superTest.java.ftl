package ${cfg.testPkg};

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import ${package.Entity}.${entity};
import ${cfg.formPkg}.${cfg.addForm};
import ${cfg.formPkg}.${cfg.updateForm};
import ${cfg.formPkg}.${cfg.pageQueryForm};
import ${package.Service}.${table.serviceName};
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* ${cfg.tableComment} 测试类基类
* </p>
*
* @author ${author}
* @since ${date}
*/
public class ${cfg.superTest} {
    protected static MockConfig mockConfig = null;

    @Autowired
    private ${table.serviceName} ${entity?uncap_first}Service;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return ${entity?uncap_first}Service.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return ${entity?uncap_first}Service.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public ${entity} mockPo() {
        return JMockData.mock(${entity}.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public ${cfg.addForm} mockAddForm() {
        return JMockData.mock(${cfg.addForm}.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public ${cfg.updateForm} mockUpdateForm() {
        return JMockData.mock(${cfg.updateForm}.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public ${cfg.pageQueryForm} mockPageQueryForm() {
        return JMockData.mock(${cfg.pageQueryForm}.class, mockConfig);
    }
}