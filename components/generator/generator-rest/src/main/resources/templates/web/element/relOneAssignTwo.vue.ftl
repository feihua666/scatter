<template>
    <StElDynamicForm :submit-props="{method: 'post',url:${entity}Url.${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first},label: '保存'}"
                     :datas="$stStringTools.replaceb(${entity}Url.checked${cfg.relTwoPropertyName?cap_first}s,{${cfg.relOnePropertyName?uncap_first}: $route.query.${cfg.relOnePropertyName?uncap_first}})"
                     :datas-format="datasFormat"
                     :options="${entity}FormOptions" :origin-prop="{inline: true,labelWidth: '100px'}" :helper-form="{${cfg.relOnePropertyName?uncap_first}: $route.query.${cfg.relOnePropertyName?uncap_first}}">

    </StElDynamicForm>
</template>

<script>
    import ${entity}Mixin from '../${entity}Mixin.js'

    export default {
        name: "${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel",
        mixins: [${entity}Mixin],
        data() {
            return {}
        },
        methods: {
            datasFormat(data) {
                return {
                    checked${cfg.relTwoPropertyName?cap_first}s: data
                }
            }
        }
    }
</script>

<style scoped>

</style>