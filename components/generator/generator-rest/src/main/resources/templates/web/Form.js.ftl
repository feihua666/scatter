import ${entity}Url from './${entity}Url.js'
const ${entity}Form = [
<#list table.fields as field>
    {
    <#if field.required>
        ${entity}SearchList: {
            element:{
                required: false
            }
        },
    </#if>

<#if cfg.tableType = 'rel' && table.commentRel2??>
        ${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel: false,
        ${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel: false,
        </#if>
        field: {
            name: '${field.propertyName}',
            <#if field.propertyType == 'Boolean'>
            value: false,
            </#if>
            <#if field.propertyType == 'Integer'>
            value: 10,
            </#if>
        },
        element:{
            <#if field.propertyType == 'Boolean'>
            type: 'switch',
            </#if>
            <#if field.propertyType == 'Integer'>
            type: 'inputNumber',
            </#if>
            label: '${field.commentSimple!}',
            <#if field.required>
            required: true,
            </#if>
        }
    },
</#list>
<#if cfg.tableType = 'tree'>
    {
    <#if cfg.tableType = 'rel' && table.commentRel2??>
        ${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel: false,
        ${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel: false,
    </#if>
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: ${entity}Url.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            label: '父级',
        }
    },
</#if>

<#if cfg.tableType = 'rel' && table.commentRel2??>
    {
        ${entity}SearchList: false,
        ${entity}Add: false,
        ${entity}Update: false,
        ${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel: false,

        field: {
            name: '${cfg.relOnePropertyName?uncap_first}',
        },
        element:{
            label: '${table.commentRel1}',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo 用于回显，这里可以是一个列表地址，如：RoleUrl.list
            }

        }
    },
    {
        ${entity}SearchList: false,
        ${entity}Add: false,
        ${entity}Update: false,
        ${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel: false,

        field: {
            name: 'checked${cfg.relTwoPropertyName?cap_first}s',
            value: []
        },
        element:{
            label: '${table.commentRel2}',
            type: 'tree',
            options: {
                datas: [], // todo 用于显示分配的数据，这里可以是一个列表地址，如：RoleUrl.list
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        ${entity}SearchList: false,
        ${entity}Add: false,
        ${entity}Update: false,
        ${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel: false,

        field: {
            name: '${cfg.relTwoPropertyName?uncap_first}',
        },
        element:{
            label: '${table.commentRel2}',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        ${entity}SearchList: false,
        ${entity}Add: false,
        ${entity}Update: false,
        ${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel: false,

        field: {
            name: 'checked${cfg.relOnePropertyName?cap_first}s',
            value: []
        },
        element:{
            label: '${table.commentRel1}',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
</#if>
]
export default ${entity}Form