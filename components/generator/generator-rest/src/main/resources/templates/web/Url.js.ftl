const basePath = '' + '<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>'
const ${entity}Url = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
<#if cfg.tableType = 'rel' && table.commentRel2??>
    ${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}: basePath + '/${cfg.relOnePropertyNameSimple?lower_case}/assign/${cfg.relTwoPropertyNameSimple?lower_case}',
    checked${cfg.relTwoPropertyName?cap_first}s: basePath + '/${cfg.relOnePropertyNameSimple?lower_case}/{${cfg.relOnePropertyName?uncap_first}}',
    ${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}: basePath + '/${cfg.relTwoPropertyNameSimple?lower_case}/assign/${cfg.relOnePropertyNameSimple?lower_case}',
    checked${cfg.relOnePropertyName?cap_first}s: basePath + '/${cfg.relTwoPropertyNameSimple?lower_case}/{${cfg.relTwoPropertyName?uncap_first}}',
</#if>
    router: {
        searchList: '/${entity}SearchList',
        add: '/${entity}Add',
        update: '/${entity}Update',
<#if cfg.tableType = 'rel' && table.commentRel2??>
        ${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel: '/${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel',
        ${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel: '/${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel',
</#if>
    }
}
export default ${entity}Url