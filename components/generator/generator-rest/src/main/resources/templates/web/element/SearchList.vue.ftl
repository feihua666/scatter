<template>
    <el-container>
        <#if cfg.tableType = 'tree'>
        <el-aside width="200px">
            <StElTree :datas="${entity}Url.list" @nodeClick="nodeClick" ></StElTree>
        </el-aside>
        </#if>
        <el-main>
        <StElDynamicForm :helper-form="helperForm" :submit-props="{method: 'get',loading: searchLoading,onSubmit:(form,data)=>{queryParams = $stObjectTools.clone(data)}}"
                         :options="${entity}FormOptions" :origin-prop="{inline: true,labelWidth: '100px'}">
            <template #buttons>
                <el-button v-bind="$stElStyleTools.aiButtonStyle('添加')"
                           @click="$router.push(${entity}Url.router.add)" size="mini">添加
                </el-button>
            </template>
        </StElDynamicForm>
        <StElTable ref="steltable" :options="${entity}TableOptions" @loading="(val)=>{searchLoading = val}" :datas="${entity}Url.searchList" :query-params="queryParams">
            <template #append>
                <el-table-column
                        label="操作"
                        width="200">
                    <template #default="{row,column,$index}">
                        <StElButtonGroup :group-param="{row,column,$index}" :operationMoreButtonOptions="{stype: 'link'}" :operations="tableOperations">
                        </StElButtonGroup>
                    </template>
                </el-table-column>

            </template>
        </StElTable>
        </el-main>
    </el-container>
</template>

<script>
    import ${entity}Mixin from '../${entity}Mixin.js'
    export default {
        name: "${entity}SearchList",
        mixins: [${entity}Mixin],
        props: {
        },
        computed: {
        },
        data() {
            return {
                queryParams: {},
                searchLoading:  false,

                helperForm: {
                    <#if cfg.tableType = 'tree'>
                    parentId: ''
                    </#if>
                },

                tableOperations: [{
                    label: '删除',
                    stype: 'link',
                    loading: false,
                    requestSuccess: ({groupParam}) => {
                        // 删除当前行，不重新请求
                        this.$refs.steltable.getTableData().splice(groupParam.$index, 1)
                    },
                    action: ({groupParam}) => {
                        return {
                            method: 'delete',
                            url: this.$stStringTools.replaceb(this.${entity}Url.delete, {id: groupParam.row.id}),
                            confirmText: `确定删除 ${"$"}{groupParam.row.name}吗`
                        }
                    }
                },
                    {
                        label: '修改',
                        stype: 'link',
                        action: ({groupParam}) => {
                            this.$router.push({path: this.${entity}Url.router.update, query: {id: groupParam.row.id}})
                        }
                    },
                    <#if cfg.tableType = 'tree'>
                    {
                        label: '添加子级',
                        stype: 'link',
                        action: ({groupParam}) => {
                            this.$router.push({path: this.${entity}Url.router.add, query: {parentId: groupParam.row.id}})
                        }
                    },
                    </#if>
                ]
            }
        },
        methods: {
            <#if cfg.tableType = 'tree'>
            nodeClick(data) {
                this.helperForm.parentId = data.id
            },
            </#if>
        }
    }
</script>

<style scoped>

</style>