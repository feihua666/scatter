<template>
    <StElDynamicForm :submit-props="{method: 'post',url:${entity}Url.${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first},label: '保存'}"
                     :datas="$stStringTools.replaceb(${entity}Url.checked${cfg.relTwoPropertyName?cap_first}s,{${cfg.relTwoPropertyName?uncap_first}: $route.query.${cfg.relTwoPropertyName?uncap_first}})"
                     :datas-format="datasFormat"
                     :options="${entity}FormOptions" :origin-prop="{inline: true,labelWidth: '100px'}" :helper-form="{${cfg.relTwoPropertyName?uncap_first}: $route.query.${cfg.relTwoPropertyName?uncap_first}}">

    </StElDynamicForm>
</template>

<script>
    import ${entity}Mixin from '../${entity}Mixin.js'

    export default {
        name: "${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel",
        mixins: [${entity}Mixin],
        data() {
            return {}
        },
        methods: {
            datasFormat(data) {
                return {
                    checked${cfg.relOnePropertyName?cap_first}s: data
                }
            }
        }
    }
</script>

<style scoped>

</style>