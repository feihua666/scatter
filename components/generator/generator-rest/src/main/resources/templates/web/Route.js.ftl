import ${entity}Url from './${entity}Url.js'

const ${entity}Route = [
    {
        path: ${entity}Url.router.searchList,
        component: () => import('./element/${entity}SearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'${entity}SearchList',
            name: '${cfg.tableComment}管理'
        }
    },
    {
        path: ${entity}Url.router.add,
        component: () => import('./element/${entity}Add'),
        meta: {
            code:'${entity}Add',
            name: '${cfg.tableComment}添加'
        }
    },
    {
        path: ${entity}Url.router.update,
        component: () => import('./element/${entity}Update'),
        meta: {
            code:'${entity}Update',
            name: '${cfg.tableComment}修改'
        }
    },
<#if cfg.tableType = 'rel' && table.commentRel2??>
    {
        path: ${entity}Url.router.${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel,
        component: () => import('./element/${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel'),
        meta: {
            code:'${cfg.relOnePropertyNameSimple?cap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}Rel',
            name: '${table.commentRel1}分配${table.commentRel2}'
        }
    },
    {
        path: ${entity}Url.router.${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel,
        component: () => import('./element/${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel'),
        meta: {
            code:'${cfg.relTwoPropertyNameSimple?cap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}Rel',
            name: '${table.commentRel2}分配${table.commentRel1}'
        }
    },
</#if>
]
export default ${entity}Route