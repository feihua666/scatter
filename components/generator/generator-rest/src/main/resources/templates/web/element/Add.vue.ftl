<template>
    <StElDynamicForm :submit-props="{method: 'post',url:${entity}Url.add}"
                    <#if cfg.tableType = 'tree'>
                     :helper-form="{parentId: $route.query.parentId}"
                     </#if>
                     :options="${entity}FormOptions" :origin-prop="{inline: true,labelWidth: '100px'}">

    </StElDynamicForm>
</template>

<script>
    import ${entity}Mixin from '../${entity}Mixin.js'
    export default {
        name: "${entity}Add",
        mixins: [${entity}Mixin],
        data() {
            return {}
        },
        methods: {}
    }
</script>

<style scoped>

</style>