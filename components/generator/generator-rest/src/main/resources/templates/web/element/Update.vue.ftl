<template>
    <StElDynamicForm :submit-props="{method: 'put',url:${entity}Url.update}"
                     :datas="$stStringTools.replaceb(${entity}Url.queryById,{id:  $route.query.id})"
                     :options="${entity}FormOptions" :origin-prop="{inline: true,labelWidth: '100px'}">

    </StElDynamicForm>
</template>

<script>
    import ${entity}Mixin from '../${entity}Mixin.js'
    export default {
        name: "${entity}Update",
        mixins: [${entity}Mixin],
        data() {
            return {}
        },
        methods: {}
    }
</script>

<style scoped>

</style>