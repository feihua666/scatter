const ${entity}Table = [
<#list table.fields as field>
    {
        prop: '${field.propertyName}',
        label: '${field.commentSimple!}'
    },
</#list>
<#if cfg.tableType = 'tree'>
    {
        prop: 'parentName',
        label: '父级名称'
    },
</#if>
]
export default ${entity}Table