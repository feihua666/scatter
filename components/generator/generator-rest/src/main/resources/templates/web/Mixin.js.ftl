import ${entity}Form from './${entity}Form.js'
import ${entity}Table from './${entity}Table.js'
import ${entity}Url from './${entity}Url.js'
const ${entity}Mixin = {
    computed: {
        ${entity}FormOptions() {
            return this.$stDynamicFormTools.options(${entity}Form,this.$options.name)
        },
        ${entity}TableOptions() {
            return ${entity}Table
        },
        ${entity}Url(){
            return ${entity}Url
        }
    },
}
export default ${entity}Mixin