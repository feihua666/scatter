package ${package.Service};

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import ${cfg.formPkg}.${cfg.addForm};
import ${cfg.formPkg}.${cfg.updateForm};
import ${cfg.formPkg}.${cfg.pageQueryForm};
import java.util.List;
/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

  <#list table.fields as field>
    <#if field.propertyType == "boolean">
        <#assign getprefix="is"/>
    <#else>
        <#assign getprefix="get"/>
    </#if>
    <#assign getMethodStr =  "${getprefix +field.capitalName}">
    <#if field.unique>

    /**
     * 根据${field.commentSimple!}查询
     * @param ${field.propertyName}
     * @return
     */
    default ${entity} getBy${field.capitalName}(String ${field.propertyName}) {
        Assert.hasText(${field.propertyName},"${field.propertyName}不能为空");
        return getOne(Wrappers.<${entity}>lambdaQuery().eq(${entity}::${getMethodStr}, ${field.propertyName}));
    }
    </#if>
    <#if (cfg.tableType = 'rel' || field.comment ? contains("外键") || field.foreignKey) && !field.unique>
    /**
     * 根据${field.commentSimple!}查询
     * @param ${field.propertyName}
     * @return
     */
    default List<${entity}> getBy${field.capitalName}(String ${field.propertyName}) {
        Assert.hasText(${field.propertyName},"${field.propertyName}不能为空");
        return list(Wrappers.<${entity}>lambdaQuery().eq(${entity}::${getMethodStr}, ${field.propertyName}));
    }
    </#if>
 </#list>

}
</#if>
