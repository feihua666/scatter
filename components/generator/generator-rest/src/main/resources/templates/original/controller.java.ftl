package ${package.Controller};

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ${cfg.configurationPkg}.${cfg.configuration};
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import ${package.Entity}.${entity};
import ${cfg.voPkg}.${cfg.vo};
import ${cfg.formPkg}.${cfg.addForm};
import ${cfg.formPkg}.${cfg.updateForm};
import ${cfg.formPkg}.${cfg.pageQueryForm};
<#if cfg.tableType = 'rel' && table.commentRel2??>
import ${cfg.formPkg}.${cfg.oneAssignTwoForm};
import ${cfg.formPkg}.${cfg.twoAssignOneForm};
import scatter.common.rest.exception.BusinessDataNotFoundException;
import java.util.stream.Collectors;
</#if>
import ${package.Service}.${table.serviceName};
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Api(tags = "${cfg.tableComment}相关接口")
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping(${cfg.configuration}.CONTROLLER_BASE_PATH + "<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass}<${entity}, ${cfg.vo}, ${cfg.addForm}, ${cfg.updateForm}, ${cfg.pageQueryForm}> {
<#else>
public class ${table.controllerName} {
</#if>
    @Autowired
    private ${table.serviceName} ${table.serviceName?uncap_first};

    @ApiOperation("添加${cfg.tableComment}")
    @PreAuthorize("hasAuthority('${entity}:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ${cfg.vo} add(@RequestBody @Valid ${cfg.addForm} addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询${cfg.tableComment}")
    @PreAuthorize("hasAuthority('${entity}:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public ${cfg.vo} queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除${cfg.tableComment}")
    @PreAuthorize("hasAuthority('${entity}:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新${cfg.tableComment}")
    @PreAuthorize("hasAuthority('${entity}:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public ${cfg.vo} update(@RequestBody @Valid ${cfg.updateForm} updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询${cfg.tableComment}")
    @PreAuthorize("hasAuthority('${entity}:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<${cfg.vo}> getList(${cfg.pageQueryForm} listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询${cfg.tableComment}")
    @PreAuthorize("hasAuthority('${entity}:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<${cfg.vo}> getPage(${cfg.pageQueryForm} listPageForm) {
         return super.getPage(listPageForm);
    }
    <#if cfg.tableType = 'rel' && table.commentRel2??>

    @ApiOperation("${table.commentRel1}分配${table.commentRel2}")
    @PreAuthorize("hasAuthority('${entity}:single:${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}')")
    @PostMapping("/${cfg.relOnePropertyNameSimple?lower_case}/assign/${cfg.relTwoPropertyNameSimple?lower_case}")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean ${cfg.relOnePropertyNameSimple?uncap_first}Assign${cfg.relTwoPropertyNameSimple?cap_first}(@RequestBody @Valid ${cfg.oneAssignTwoForm} cf) {
        return iBaseService.removeAndAssignRel(cf.get${cfg.relOnePropertyName?cap_first}(),cf.getChecked${cfg.relTwoPropertyName?cap_first}s(),cf.getUnchecked${cfg.relTwoPropertyName?cap_first}s(),cf.getIsLazyLoad(),${entity}::get${cfg.relOnePropertyName?cap_first},${entity}::get${cfg.relTwoPropertyName?cap_first},(relDto)->new ${entity}().set${cfg.relOnePropertyName?cap_first}(relDto.getMainId()).set${cfg.relTwoPropertyName?cap_first}(relDto.getOtherId()));
    }

    @ApiOperation("根据${table.commentRel1}ID查询已分配的${table.commentRel2}id")
    @PreAuthorize("hasAuthority('${entity}:single:queryBy${cfg.relOnePropertyName?cap_first}')")
    @GetMapping("/${cfg.relOnePropertyNameSimple?lower_case}/{${cfg.relOnePropertyName?uncap_first}}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryBy${cfg.relOnePropertyName?cap_first}(@PathVariable String ${cfg.relOnePropertyName?uncap_first}) {
        List<${entity}> rels = ${table.serviceName?uncap_first}.getBy${cfg.relOnePropertyName?cap_first}(${cfg.relOnePropertyName?uncap_first});
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }

        return rels.stream().map(item -> item.get${cfg.relTwoPropertyName?cap_first}()).collect(Collectors.toList());
    }

    @ApiOperation("清空${table.commentRel1}下的所有${table.commentRel2}")
    @PreAuthorize("hasAuthority('${entity}:single:deleteBy${cfg.relOnePropertyName?cap_first}')")
    @DeleteMapping("/${cfg.relOnePropertyNameSimple?lower_case}/{${cfg.relOnePropertyName?uncap_first}}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteBy${cfg.relOnePropertyName?cap_first}(@PathVariable String ${cfg.relOnePropertyName?uncap_first}) {
        return iBaseService.removeAssignRel(${cfg.relOnePropertyName?uncap_first},${entity}::get${cfg.relOnePropertyName?cap_first});
    }


    @ApiOperation("${table.commentRel2}分配${table.commentRel1}")
    @PreAuthorize("hasAuthority('${entity}:single:${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}')")
    @PostMapping("/${cfg.relTwoPropertyNameSimple?lower_case}/assign/${cfg.relOnePropertyNameSimple?lower_case}")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean ${cfg.relTwoPropertyNameSimple?uncap_first}Assign${cfg.relOnePropertyNameSimple?cap_first}(@RequestBody @Valid ${cfg.twoAssignOneForm} cf) {
        return ${table.serviceName?uncap_first}.removeAndAssignRel(cf.get${cfg.relTwoPropertyName?cap_first}(),cf.getChecked${cfg.relOnePropertyName?cap_first}s(),cf.getUnchecked${cfg.relOnePropertyName?cap_first}s(),cf.getIsLazyLoad(),${entity}::get${cfg.relTwoPropertyName?cap_first},${entity}::get${cfg.relOnePropertyName?cap_first},(relDto)->new ${entity}().set${cfg.relTwoPropertyName?cap_first}(relDto.getMainId()).set${cfg.relOnePropertyName?cap_first}(relDto.getOtherId()));
    }

    @ApiOperation("根据${table.commentRel2}ID查询已分配的${table.commentRel1}id")
    @PreAuthorize("hasAuthority('${entity}:single:queryBy${cfg.relTwoPropertyName?cap_first}')")
    @GetMapping("/${cfg.relTwoPropertyNameSimple?lower_case}/{${cfg.relTwoPropertyName?uncap_first}}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryBy${cfg.relTwoPropertyName?cap_first}(@PathVariable String ${cfg.relTwoPropertyName?uncap_first}) {
        List<${entity}> rels = ${table.serviceName?uncap_first}.getBy${cfg.relTwoPropertyName?cap_first}(${cfg.relTwoPropertyName?uncap_first});
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }
        return rels.stream().map(item -> item.get${cfg.relTwoPropertyName?cap_first}()).collect(Collectors.toList());
    }

    @ApiOperation("清空${table.commentRel2}下的所有${table.commentRel1}")
    @PreAuthorize("hasAuthority('${entity}:single:deleteBy${cfg.relTwoPropertyName?cap_first}')")
    @DeleteMapping("/${cfg.relTwoPropertyNameSimple?lower_case}/{${cfg.relTwoPropertyName?uncap_first}}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteBy${cfg.relTwoPropertyName?cap_first}(@PathVariable String ${cfg.relTwoPropertyName?uncap_first}) {
        return ${table.serviceName?uncap_first}.removeAssignRel(${cfg.relTwoPropertyName?uncap_first},${entity}::get${cfg.relTwoPropertyName?cap_first});
    }
    </#if>
}
</#if>
