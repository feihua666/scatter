package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import ${cfg.formPkg}.${cfg.addForm};
import ${cfg.formPkg}.${cfg.updateForm};
import ${cfg.formPkg}.${cfg.pageQueryForm};
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
@Transactional
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}, ${cfg.addForm}, ${cfg.updateForm}, ${cfg.pageQueryForm}> implements ${table.serviceName} {
    @Override
    public void preAdd(${cfg.addForm} addForm,${entity} po) {
        super.preAdd(addForm,po);
 <#list table.fields as field>
    <#if field.unique>
        <#if field.propertyType == "boolean">
            <#assign getprefix="is"/>
        <#else>
            <#assign getprefix="get"/>
        </#if>
        <#assign getMethodStr =  "${getprefix +field.capitalName}">
        if (!isStrEmpty(addForm.${getMethodStr}())) {
            // ${field.commentSimple!}已存在不能添加
            assertByColumn(addForm.${getMethodStr}(),${entity}::${getMethodStr},false);
        }
    </#if>
 </#list>

    }

    @Override
    public void preUpdate(${cfg.updateForm} updateForm,${entity} po) {
        super.preUpdate(updateForm,po);
 <#list table.fields as field>
    <#if field.unique>
        <#if field.propertyType == "boolean">
            <#assign getprefix="is"/>
        <#else>
            <#assign getprefix="get"/>
        </#if>
        <#assign getMethodStr =  "${getprefix +field.capitalName}">
        if (!isStrEmpty(updateForm.${getMethodStr}())) {
            ${entity} byId = getById(updateForm.getId());
            // 如果${field.commentSimple!}有改动
            if (!isEqual(updateForm.${getMethodStr}(), byId.${getMethodStr}())) {
                // ${field.commentSimple!}已存在不能修改
                assertByColumn(updateForm.${getMethodStr}(),${entity}::${getMethodStr},false);
            }
        }
    </#if>
 </#list>

    }
}
</#if>
