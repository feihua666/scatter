package ${cfg.mapStructPkg};

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import ${package.Entity}.${entity};
import ${cfg.voPkg}.${cfg.vo};
import ${cfg.formPkg}.${cfg.addForm};
import ${cfg.formPkg}.${cfg.updateForm};
import ${cfg.formPkg}.${cfg.pageQueryForm};

/**
 * <p>
 * ${cfg.tableComment} mapStruct 转换接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ${cfg.mapStruct} extends IBaseVoMapStruct<${entity}, ${cfg.vo}>,
                                  IBaseAddFormMapStruct<${entity},${cfg.addForm}>,
                                  IBaseUpdateFormMapStruct<${entity},${cfg.updateForm}>,
                                  IBaseQueryFormMapStruct<${entity},${cfg.pageQueryForm}>{
    ${cfg.mapStruct} INSTANCE = Mappers.getMapper( ${cfg.mapStruct}.class );

}
