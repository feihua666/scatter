package ${cfg.clientPkg};

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * ${table.comment!} 服务客户端
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Component
@FeignClient(value = "${entity}-client")
public interface ${cfg.client} {

}
