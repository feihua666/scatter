package ${cfg.voPkg};

import scatter.common.pojo.vo.BaseIdVo;
<#list table.importPackages as pkg>
import ${pkg};
</#list>
<#if swagger2>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
</#if>


/**
 * <p>
 * ${cfg.tableComment}响应对象
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Setter
@Getter
</#if>
<#if swagger2>
@ApiModel(value="${cfg.tableComment}响应对象")
</#if>
public class ${cfg.vo} extends BaseIdVo {
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>

    <#if field.comment!?length gt 0>
        <#if swagger2>
    @ApiModelProperty(value = "${field.comment}")
        <#else>
    /**
     * ${field.comment}
     */
        </#if>
    </#if>
    private ${field.propertyType} ${field.propertyName};
</#list>

<#if cfg.tableType = 'tree'>
    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "父级名称")
    private String parentName;
</#if>
<#------------  END 字段循环遍历  ---------->
}
