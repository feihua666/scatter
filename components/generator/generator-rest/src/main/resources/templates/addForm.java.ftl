package ${cfg.formPkg};

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
<#list table.importPackages as pkg>
import ${pkg};
</#list>
<#if swagger2>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
</#if>

/**
 * <p>
 * ${cfg.tableComment}添加表单对象
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Setter
@Getter
</#if>
<#if swagger2>
@ApiModel(value="${cfg.tableComment}添加表单对象")
</#if>
public class ${cfg.addForm} extends BaseAddForm {
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>

    <#if field.required>
    <#if field.propertyType == "String"><#assign nullValid="NotEmpty"/><#else><#assign nullValid="NotNull"/></#if>
    @${nullValid}(message="${field.commentSimple!}不能为空")
    </#if>
    <#if field.comment!?length gt 0>
        <#if swagger2>
    @ApiModelProperty(value = "${field.comment}"<#if field.required>,required = true</#if>)
        <#else>
    /**
     * ${field.comment}
     */
        </#if>
    </#if>
    private ${field.propertyType} ${field.propertyName};
</#list>

<#if cfg.tableType = 'tree'>
    @ApiModelProperty(value = "父级id")
    private String parentId;
</#if>
<#------------  END 字段循环遍历  ---------->
}
