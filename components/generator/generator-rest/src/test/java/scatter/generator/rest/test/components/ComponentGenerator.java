package scatter.generator.rest.test.components;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by yangwei
 * Created at 2020/8/20 12:46
 */
@Slf4j
public class ComponentGenerator {

    public static void main(String[] args) {
        // position可选两个值：components modules
        // components 代码生成到scatter/components下
        // modules 代码生成到scatter/modules
        String position = "modules";
        // 生成的组件名称
        String componentName = "scatter-server-resource";

        scatter.generator.component.ComponentGenerator componentGenerator = new scatter.generator.component.ComponentGenerator(componentName, position);
        componentGenerator.setIncludeServer(true);
        componentGenerator.generate();
    }
}
