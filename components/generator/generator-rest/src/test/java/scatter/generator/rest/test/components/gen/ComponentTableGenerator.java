package scatter.generator.rest.test.components.gen;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import scatter.generator.core.BaseGenerator;
import scatter.generator.core.CustomDataSourceConfig;
import scatter.generator.core.Utils;
import scatter.generator.pojo.enums.TableType;
import scatter.generator.pojo.param.GenerateSeperateParam;

/**
 * Created by yangwei
 * Created at 2020/11/4 13:12
 */
public class ComponentTableGenerator extends BaseGenerator {

    public static void main(String[] args) {

        //String tableNames = "component_user_simple";
        //String tableNames = "wwd_activity_content,wwd_activity";
        //String tableNames = "component_identifier_pwd,component_identifier";
        //String tableNames = "component_role_user_rel";
        //String tableNames = "component_role_func_rel";
        //String tableNames = "component_role";
        //String tableNames = "component_file";
        //String tableNames = "component_wx_mp_user_location_record,component_wx_mp_user,component_wx_mp_msg_reply";
        //String tableNames = "wwd_user,wwd_user_identifier,wwd_user_identifier_pwd";
        //String tableNames = "component_banner";
        //String tableNames = "component_error_log";
        //String tableNames = "component_comment_subject,component_comment_subject_pic,component_comment_subject_star";
        //String tableNames = "component_comment_hierarchical";
        //String tableNames = "component_comment_hierarchical_pic,component_comment_hierarchical_star";
        // String tableNames = "component_point_detail,component_point";
        //String tableNames = "scatter_design_component_frontend_tag_rel,scatter_design_component_frontend_tag,scatter_design_component_frontend_endpoint_rel,scatter_design_component_frontend_endpoint,scatter_design_component_frontend_backend_rel,scatter_design_component_frontend";
        //String tableNames = "scatter_design_component_frontend_tag_rel";

        //String tableNames = "scatter_design_component_backend_tag_rel,scatter_design_component_backend_tag,scatter_design_component_backend,scatter_design_component_backend_backend_rel";
        //String tableNames = "scatter_generator_table_column,scatter_generator_table";
        //String tableNames = "scatter_generator_config_template,scatter_generator_config_params,scatter_generator_config_datasource";
        //String tableNames = "scatter_generator_table_gen_history";
        // String tableNames = "component_dept";
        //String tableNames = "component_data_object,component_data_scope";
        //String tableNames = "component_data_scope_custom_rel";
        //String tableNames = "component_order_wx";
        //String tableNames = "component_message,component_message_template,component_message_user_state";
        //String tableNames = "component_recd_user,component_recd_user_behavior,component_recd_user_features,component_recd_user_tags";
        //String tableNames = "component_bank_card,component_bank";
        //String tableNames = "component_recd_item,component_recd_item_features,component_recd_item_tags";
        //String tableNames = "component_recd_user,component_recd_user_behavior,component_recd_user_features,component_recd_user_tags";
        //String tableNames = "component_portrait_user,component_portrait_user_behavior,component_portrait_user_features,component_portrait_user_tags";
        //String tableNames = "component_portrait_user_features_rel,component_portrait_user_tags_rel";
        //String tableNames = "component_portrait_item_features_rel,component_portrait_item_tags_rel";
        //String tableNames = "component_portrait_item,component_portrait_item_features,component_portrait_item_tags";
        //String tableNames = "component_scheduler_record";
        //String tableNames = "component_server_resource_environment,component_server_resource_account";
        String tableNames = "component_server_resource_code";
        //String tableNames = "component_server_resource_machine,component_server_resource_machine_room";
        //String tableNames = "component_server_resource_project";
        //String tableNames = "component_server_resource_code_project_vc_rel";


        String position = "components";// modules components
        String tablePrefix = "component";
        String moduleName = "code"; // 单个项目下的包分组可以不填
        String componentName = "server-resource";
        // 在表类型为rel是其注释应该是 xxx和xxxx关系表
        TableType tableType = TableType.tree;
        boolean fileOverride = true;
        boolean deleteFile = false;
        for (String name : tableNames.split(",")) {
            ComponentTableGenerator generator = new ComponentTableGenerator();
            GenerateSeperateParam generateParam = new GenerateSeperateParam();

            String tableName = name;

            String packageParent = "scatter." + generator.componentNameConvertToNoLine(componentName);
            generateParam
                    .setPojoComponentPath(StrUtil.format("/{}/{}/{}-pojo",position,componentName,componentName))
                    .setRestComponentPath(StrUtil.format("/{}/{}/{}-rest",position,componentName,componentName))
                    .setRestClientComponentPath(StrUtil.format("/{}/{}/{}-rest-client",position,componentName,componentName))
                    .setRestClientImplComponentPath(StrUtil.format("/{}/{}/{}-rest-client-impl",position,componentName,componentName))
                    .setWebPath(StrUtil.format("/{}/{}/web/pc",position,componentName))
                    .setAuthor("yw")
                    .setTableName(tableName)
                    .setTablePrefix(tablePrefix)
                    .setTableType(tableType)
                    .setModuleName(moduleName)
                    .setComponentName(componentName)

                    .setEntityPackageParent(packageParent + ".pojo")
                    .setPackageParent(packageParent + ".rest")
                    .setFileOverride(fileOverride)
                    .setDeleteFile(deleteFile)
            ;
            // 生成 rest
            generator.doGenerateSeperate(generateParam);
        }

    }

    @Override
    public String componentNameConvertToNoLine(String componentName) {
        return Utils.componentNameConvertToNoLine(componentName);
    }

    @Override
    public String componentNameConvertToUnderLine(String componentName) {
        return Utils.componentNameConvertToUnderLine(componentName);
    }
    /**
     * 数据源配置
     * @return
     */
    public CustomDataSourceConfig dataSourceConfig() {
        // 数据源配置
        CustomDataSourceConfig dsc = new CustomDataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/scatter_dev?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("rootroot");
        return dsc;
    }
}
