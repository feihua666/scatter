package scatter.generator.rest.test.components;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-09-17 22:36
 */
@Slf4j
public class Test {
	public static void main(String[] args) {
		String userDir = System.getProperty("user.dir");
		log.info("userDir = [" + userDir + "]");

		String starterPath = userDir + "/components";

		File[] ls = FileUtil.ls(starterPath);
		for (File l : ls) {

			if (!l.isDirectory()) {
				continue;
			}
			String name = l.getName();
			String dbPath = starterPath + "/" + name + "/" + name +"-rest/src/main/resources/db";
			if (!FileUtil.exist(dbPath)) {
				continue;
			}
			File[] ls1 = FileUtil.ls(dbPath);
			if (ls1 != null && ls1.length > 0) {
				File file = Arrays.stream(ls1).filter(f -> f.getName().endsWith("-schema.sql")).findFirst().orElse(null);
				if (file != null) {
					String s = FileUtil.readUtf8String(file);
					if (s != null) {
						if (!s.contains("-- import ")) {
							//逻辑
							List<File> files = Arrays.stream(ls1).filter(f -> f.getName().startsWith("schema")).collect(Collectors.toList());
							if (CollectionUtil.isNotEmpty(files)) {
								List<String> resultStr = new ArrayList<>();
								for (File file1 : files) {
									resultStr.add("-- import classpath:db/"+ file1.getName());
								}
								System.out.println(file.getName());
								System.out.println(resultStr);
								FileUtil.writeUtf8Lines(resultStr,file);
							}
						}
					}
				}
			}
		}
	}
}
