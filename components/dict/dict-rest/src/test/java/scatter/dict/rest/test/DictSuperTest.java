package scatter.dict.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dict.pojo.po.Dict;
import scatter.dict.pojo.form.DictAddForm;
import scatter.dict.pojo.form.DictUpdateForm;
import scatter.dict.pojo.form.DictPageQueryForm;
import scatter.dict.rest.service.IDictService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 字典 测试类基类
* </p>
*
* @author yw
* @since 2020-11-23
*/
public class DictSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IDictService dictService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return dictService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return dictService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Dict mockPo() {
        return JMockData.mock(Dict.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public DictAddForm mockAddForm() {
        return JMockData.mock(DictAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public DictUpdateForm mockUpdateForm() {
        return JMockData.mock(DictUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public DictPageQueryForm mockPageQueryForm() {
        return JMockData.mock(DictPageQueryForm.class, mockConfig);
    }
}