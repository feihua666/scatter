package scatter.dict.rest.test.controller;

import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import scatter.dict.pojo.form.DictAddForm;
import scatter.dict.pojo.vo.DictVo;
import scatter.dict.rest.DictConfiguration;
import scatter.dict.rest.test.DictSuperTest;
/**
* <p>
* 字典 前端控制器测试类
* </p>
*
* @author yw
* @since 2020-11-23
*/
@SpringBootTest
@AutoConfigureMockMvc
public class DictControllerTest extends DictSuperTest{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testAddForm() throws Exception {
        // 请求url
        String url = DictConfiguration.CONTROLLER_BASE_PATH + "/dict";

        // 请求表单
        DictAddForm addForm = mockAddForm();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                     .content(JSONUtil.toJsonStr(addForm))
                // 断言返回结果是json
                .accept(MediaType.APPLICATION_JSON))
                // 得到返回结果
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        // 添加成功返回vo
        DictVo vo = JSONUtil.toBean(response.getContentAsString(), DictVo.class);
        Assertions.assertEquals(201,response.getStatus());
        // 删除数据
        removeById(vo.getId());
    }
}