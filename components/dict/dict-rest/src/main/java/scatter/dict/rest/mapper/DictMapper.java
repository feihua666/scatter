package scatter.dict.rest.mapper;

import scatter.dict.pojo.po.Dict;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2020-11-23
 */
public interface DictMapper extends IBaseMapper<Dict> {

}
