package scatter.dict.rest.mapstruct;

import cn.hutool.core.collection.CollectionUtil;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.dict.pojo.dto.DictGroupItemDto;
import scatter.dict.pojo.form.DictAddForm;
import scatter.dict.pojo.form.DictPageQueryForm;
import scatter.dict.pojo.form.DictUpdateForm;
import scatter.dict.pojo.po.Dict;
import scatter.dict.pojo.vo.AppDictVo;
import scatter.dict.pojo.vo.DictGroupItemVo;
import scatter.dict.pojo.vo.DictVo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 字典 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2020-11-23
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DictMapStruct extends IBaseVoMapStruct<Dict, DictVo>,
        IBaseAddFormMapStruct<Dict,DictAddForm>,
        IBaseUpdateFormMapStruct<Dict,DictUpdateForm>,
        IBaseQueryFormMapStruct<Dict,DictPageQueryForm> {
    DictMapStruct INSTANCE = Mappers.getMapper( DictMapStruct.class );

    AppDictVo poToAppVo(Dict dict);

    List<AppDictVo> posToAppVos(List<Dict> dicts);

    DictGroupItemDto poToDictGroupItemDto(Dict dict);

    DictGroupItemVo dictGroupItemDtoToDictGroupItemVo(DictGroupItemDto dictGroupItemDto);

    default List<DictGroupItemVo> dictGroupItemDtosToDictGroupItemVos(List<DictGroupItemDto> dictGroupItemDtos){
        if (CollectionUtil.isEmpty(dictGroupItemDtos)) {
            return null;
        }
        return dictGroupItemDtos.stream().map(item -> dictGroupItemDtoToDictGroupItemVo(item)).collect(Collectors.toList());
    }
}
