package scatter.dict.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.common.rest.service.IBaseService;
import scatter.common.rest.validation.DictService;
import scatter.dict.pojo.dto.DictGroupItemDto;
import scatter.dict.pojo.form.DictGroupsQueryForm;
import scatter.dict.pojo.form.DictItemsQueryForm;
import scatter.dict.pojo.po.Dict;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author yw
 * @since 2020-11-23
 */
public interface IDictService extends IBaseService<Dict>, DictService {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default Dict getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<Dict>lambdaQuery().eq(Dict::getCode, code));
    }

    /**
     * 根据字典组编码查询字典项
     * @param queryForm
     * @return 公共字典项 + 私有字典
     */
    List<Dict> getItemsByGroupCode(DictItemsQueryForm queryForm);

    /**
     * 根据字典组编码查询带分组的字典项
     * @param queryForm
     * @return
     */
    List<DictGroupItemDto> getGroupItemsByGroupCode(DictItemsQueryForm queryForm);

    /**
     * 根据字典组编码查询字典组
     * @param queryForm
     * @return 公共字典组 + 私有字典组
     */
    List<Dict> getGroupsByGroupCodes(DictGroupsQueryForm queryForm);

    /*
     * 根据字典组编码查询字典组
     * @param code
     * @param value
     * @return 公共字典组 + 私有字典组
     */
    Dict getDictByCodeAndValue(String code, String value);

}
