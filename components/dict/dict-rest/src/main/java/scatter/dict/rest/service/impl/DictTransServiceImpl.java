package scatter.dict.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.common.trans.TransConstants;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by yangwei
 * Created at 2020/11/26 18:49
 */
@Component
public class DictTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IDictService iDictService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,Dict.TRANS_DICT_BY_ID, TransConstants.TRANS_DICT_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqualAny(type,Dict.TRANS_DICT_BY_ID,TransConstants.TRANS_DICT_BY_ID)) {
            Dict byId = iDictService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqualAny(type,Dict.TRANS_DICT_BY_ID,TransConstants.TRANS_DICT_BY_ID)) {
            return iDictService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
