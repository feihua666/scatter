package scatter.dict.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.dict.pojo.po.Dict;
import scatter.dict.rest.service.IDictService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 字典表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2020-11-23
 */
@RestController
@RequestMapping("/inner/dict")
public class DictInnerController extends BaseInnerController<Dict> {
 @Autowired
 private IDictService dictService;

 public IDictService getService(){
     return dictService;
 }
}
