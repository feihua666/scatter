import DictForm from './DictForm.js'
import DictTable from './DictTable.js'
import DictUrl from './DictUrl.js'

const DictMixin = {
    computed: {
        computedFormOptions() {
            return this.$stDynamicFormTools.options(DictForm,this.$options.name)
        },
        computedTableOptions() {
            return DictTable
        },
        computedUrl(){
            return DictUrl
        }
    },
}
export default DictMixin