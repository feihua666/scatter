import DictUrl from './DictUrl.js'

const DictRoute = [
    {
        path: DictUrl.router.searchList,
        component: () => import('./element/DictSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'DictSearchList',
            name: '字典管理'
        }
    },
    {
        path: DictUrl.router.add,
        component: () => import('./element/DictAdd'),
        meta: {
            code:'DictAdd',
            name: '字典添加'
        }
    },
    {
        path: DictUrl.router.update,
        component: () => import('./element/DictUpdate'),
        meta: {
            code:'DictUpdate',
            name: '字典修改'
        }
    },
]
export default DictRoute