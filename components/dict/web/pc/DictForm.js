import DictUrl from './DictUrl.js'

const DictForm = [
    {
        field: {
            name: 'isGroup',
            value: false
        },
        element:{
            type: 'switch',
            options:{
                activeText: '字典组',
                inactiveText: '字典项'
            },
            label: '是否为字典组',
        },
        DictSearchList: false
    },
    {
        field: {
            name: 'code'
        },
        element:{
            label: '字典编码',
            disabled: ({form})=>{
                let r = !form.isGroup
                if(r){
                    form.code = null
                }
                return r
            },
            required: ({form})=>{
                let r = form.isGroup
                return r
            }
        },
        DictSearchList: {
            element: {
                disabled: false
            }
        }
    },
    {
        field: {
            name: 'name'
        },
        element:{
            label: '字典名称',
            required: true,
        },
        DictSearchList: {
            element: {
                required: false
            }
        }
    },
    {
        field: {
            name: 'value'
        },
        element:{
            label: '字典值',
            disabled: ({form})=>{
                let r = form.isGroup
                if(r){
                    form.value = null
                }
                return r
            },
            required: ({form})=>{
                let r = !form.isGroup
                return r
            }
        },
        DictSearchList: {
            element: {
                required: false
            }
        }
    },
    {
        DictSearchList: {
            element: {
                required: false
            }
        },
        field: {
            name: 'parentId',
        },
        element:{
            type: 'cascader',
            options: {
                datas: DictUrl.list,
                originProps:{
                    checkStrictly: true,
                    emitPath: false
                },
            },
            required: ({form})=>{
                return !form.isGroup
            },
            label: '父级',
        }
    },
    {
        field: {
            name: 'isSystem',
            value: true
        },
        element:{
            type: 'switch',
            options:{
                activeText: '系统',
                inactiveText: '非系统'
            },
            label: '是否为系统字典',

        },
        DictSearchList: false
    },
    {
        field: {
            name: 'isPublic',
            value: true
        },
        element:{
            type: 'switch',
            label: '是否为公共字典',
            options:{
                activeText: '公共',
                inactiveText: '私有'
            },
            disabled: ({form})=>{
                let r = form.isGroup
                if(r){
                    form.isPublic = true
                }
                return r
            }
        },
        DictSearchList: false
    },

    {
        field: {
            name: 'isDisabled',
            value: false
        },
        element:{
            type: 'switch',
            label: '是否禁用',
            options:{
                activeText: '禁用',
                inactiveText: '启用'
            },
            disabled: ({form})=>{
                let r = form.isPublic
                if(r){
                    form.isDisabled = false
                }
                return r
            },
        },
        DictSearchList: false,
        DictAdd: false
    },
    {
        field: {
            name: 'disabledReason'
        },
        element:{
            label: '禁用原因',
            disabled: ({form})=>{
                let r = !form.isDisabled
                if(r){
                    form.disabledReason = null
                }
                return r
            },
            required: ({form})=>{
                let r = form.isDisabled
                return r
            }
        },
        DictSearchList: false,
        DictAdd: false
    },
    {
        field: {
            name: 'privateFlag'
        },
        element:{
            label: '私有标识',
            disabled: ({form})=>{
                let r = form.isPublic
                if(r){
                    form.privateFlag = ''
                }
                return r
            },
            required: ({form})=>{
                let r = !form.isPublic
                return r
            }
        },
        DictSearchList: {
            element: {
                required: false
            }
        }
    },
    {
        field: {
            name: 'privateFlagMemo'
        },
        element:{
            label: '私有标识备忘',
            disabled: ({form})=>{
                let r = form.isPublic
                if(r){
                    form.privateFlagMemo = ''
                }
                return r
            },
            required: ({form})=>{
                let r = !form.isPublic
                return r
            }
        },
        DictSearchList: false
    },
    {
        field: {
            name: 'groupFlag'
        },
        element:{
            label: '分组标识',
            disabled: ({form})=>{
                let r = form.isPublic
                if(r){
                    form.groupFlag = ''
                }
                return r
            }
        }
    },
    {
        field: {
            name: 'groupFlagMemo'
        },
        element:{
            label: '分组标识备忘',
            disabled: ({form})=>{
                let r = form.isPublic
                if(r){
                    form.groupFlag = ''
                }
                return r
            }
        },
        DictSearchList: false
    },
    {
        field: {
            name: 'tags'
        },
        element:{
            label: '标签',
            disabled: ({form})=>{
                let r = !!form.isGroup
                if(r){
                    form.tags = null
                }
                return r
            },
        },
        DictSearchList: false
    },
    {
        field: {
            name: 'seq',
            value: 10
        },
        element:{
            type: 'inputNumber',
            label: '排序',
            required: true,
        },
        DictSearchList: false
    },
    {
        field: {
            name: 'remark'
        },
        element:{
            label: '描述',
        },
        DictSearchList: false
    },

]
export default DictForm