const basePath = '' + '/dict'
const DictUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    getItems: basePath + '/getItems',
    generateJavaEnum: basePath + '/generateJavaEnum/{dictId}',
    router: {
        searchList: '/DictSearchList',
        add: '/DictAdd',
        update: '/DictUpdate',
    }
}
export default DictUrl