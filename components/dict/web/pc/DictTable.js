const DictTable = [
    {
        prop: 'id',
        label: 'id'
    },
    {
        prop: 'code',
        label: '字典编码'
    },
    {
        prop: 'name',
        label: '字典名称'
    },
    {
        prop: 'value',
        label: '字典值'
    },
    {
        prop: 'parentName',
        label: '父级名称'
    },
    {
        prop: 'isSystem',
        label: '是否系统',
    },
    {
        prop: 'isPublic',
        label: '是否公共'
    },
    {
        prop: 'isGroup',
        label: '是否字典组'
    },
    {
        prop: 'isDisabled',
        label: '是否禁用',
        formatter(row){
            if(row.isDisabled){
                let r = '是'
                r += `(${row.disabledReason})`
                return r
            }else {
                return row.isDisabled
            }
        }
    },
    {
        prop: 'privateFlag',
        label: '私有标识'
    },
    {
        prop: 'groupFlag',
        label: '分组标识'
    },
    {
        prop: 'seq',
        label: '排序'
    },
    {
        prop: 'remark',
        label: '描述'
    },
]
export default DictTable