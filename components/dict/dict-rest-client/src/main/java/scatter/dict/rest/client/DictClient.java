package scatter.dict.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 字典表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2020-11-23
 */
@Component
@FeignClient(value = "Dict-client")
public interface DictClient {

}
