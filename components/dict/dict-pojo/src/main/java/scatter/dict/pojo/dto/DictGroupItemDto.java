package scatter.dict.pojo.dto;

import lombok.Getter;
import lombok.Setter;
import scatter.dict.pojo.po.Dict;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2021-10-13 14:42
 */
@Setter
@Getter
public class DictGroupItemDto extends Dict {

	private List<Dict> items;
}
