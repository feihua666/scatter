package scatter.dict.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 字典分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-11-25
 */
@Setter
@Getter
@ApiModel(value="字典分页表单对象")
public class DictPageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "字典编码,模糊查询，字典组时必填")
    private String code;

    @Like
    @ApiModelProperty(value = "字典名称,模糊查询")
    private String name;

    @Like
    @ApiModelProperty(value = "字典值,模糊查询")
    private String value;

    @ApiModelProperty(value = "是否为系统字典，一般系统字典代码中会做判断，不能修改或删除")
    private Boolean isSystem;

    @ApiModelProperty(value = "是否为公共字典，如果为公共字典不限制使用，否则按相应数据权限查询")
    private Boolean isPublic;

    @ApiModelProperty(value = "是否包含公共，如果isPublic有值则忽略该条件")
    private Boolean isIncludePublic;

    @ApiModelProperty(value = "是否为字典组，不是字典组就是字典项目，没有其它的")
    private Boolean isGroup;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @Like
    @ApiModelProperty(value = "私有标识,模糊查询")
    private String privateFlag;

    @ApiModelProperty(value = "私有标识备忘")
    private String privateFlagMemo;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "分组标识备忘")
    private String groupFlagMemo;

    @ApiModelProperty(value = "描述")
    private String remark;

    @OrderBy(asc = true)
    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;

}
