package scatter.dict.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseQueryForm;

import javax.validation.constraints.NotEmpty;

/**
 * 字典项查询表单
 * Created by yangwei
 * Created at 2020/11/26 15:43
 */
@Setter
@Getter
@ApiModel(value="字典组查询表单对象")
public class DictGroupsQueryForm extends BaseQueryForm {

    @NotEmpty(message="字典组编码不能为空")
    @ApiModelProperty(value = "字典组编码,多个以逗号分隔",required = true)
    private String groupCodes;

    @ApiModelProperty(value = "私有标识")
    private String privateFlag;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;
}
