package scatter.dict.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.form.SetNullWhenNull;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.validation.props.PropValid;

/**
 * <p>
 * 字典更新表单对象
 * </p>
 *
 * @author yw
 * @since 2020-11-25
 */
@PropValid
@Setter
@Getter
@ApiModel(value="字典更新表单对象")
public class DictUpdateForm extends BaseUpdateIdForm {

    @ApiModelProperty(value = "字典编码,模糊查询，字典组时必填")
    private String code;

    @NotEmpty(message="字典名称不能为空")
    @ApiModelProperty(value = "字典名称,模糊查询",required = true)
    private String name;

    @ApiModelProperty(value = "字典值,模糊查询")
    private String value;

    @NotNull(message="是否为系统字典不能为空")
    @ApiModelProperty(value = "是否为系统字典，一般系统字典代码中会做判断，不能修改或删除",required = true)
    private Boolean isSystem;

    @NotNull(message="是否为公共字典不能为空")
    @ApiModelProperty(value = "是否为公共字典，如果为公共字典不限制使用，否则按相应数据权限查询",required = true)
    private Boolean isPublic;

    @NotNull(message="是否为字典组不能为空")
    @ApiModelProperty(value = "是否为字典组，不是字典组就是字典项目，没有其它的",required = true)
    private Boolean isGroup;

    @NotNull(message="是否禁用不能为空")
    @ApiModelProperty(value = "是否禁用",required = true)
    private Boolean isDisabled;

    @PropValid.DependCondition(message = "禁用不能为空",dependProp = "isDisabled",ifEqual = "true")
    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "私有标识,模糊查询")
    private String privateFlag;

    @ApiModelProperty(value = "私有标识备忘")
    private String privateFlagMemo;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "分组标识备忘")
    private String groupFlagMemo;

    @ApiModelProperty(value = "标签，多个以逗号分隔，用来区分字典项")
    private String tags;

    @ApiModelProperty(value = "描述")
    private String remark;

    @NotNull(message="排序不能为空")
    @ApiModelProperty(value = "排序,默认按该字段升序排序",required = true)
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
