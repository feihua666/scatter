package scatter.dict.pojo.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>
 * 包含组的字典项
 * </p>
 *
 * @author yangwei
 * @since 2021-10-13 14:33
 */
@Setter
@Getter
public class DictGroupItemVo extends DictVo{

	List<DictVo> items;

}
