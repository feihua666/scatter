package scatter.dict.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.vo.BaseIdVo;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 字典响应对象
 * </p>
 *
 * @author yw
 * @since 2020-11-25
 */
@Setter
@Getter
@ApiModel(value="字典响应对象")
public class AppDictVo extends BaseIdVo {

    @ApiModelProperty(value = "字典编码,模糊查询，字典组时必填")
    private String code;

    @ApiModelProperty(value = "字典名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "字典值,模糊查询")
    private String value;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}
