package scatter.dict.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author yw
 * @since 2020-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_dict")
@ApiModel(value="Dict对象", description="字典表")
public class Dict extends BaseTreePo {

    private static final long serialVersionUID = 1L;


    public static final String TRANS_DICT_BY_ID = "trans_dict_by_id_scatter.dict.pojo.po";

    @ApiModelProperty(value = "字典编码,模糊查询，字典组时必填")
    private String code;

    @ApiModelProperty(value = "字典名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "字典值,模糊查询")
    private String value;

    @ApiModelProperty(value = "是否为系统字典，一般系统字典代码中会做判断，不能修改或删除")
    private Boolean isSystem;

    @ApiModelProperty(value = "是否为公共字典，如果为公共字典不限制使用，否则按相应数据权限查询")
    private Boolean isPublic;

    @ApiModelProperty(value = "是否为字典组，不是字典组就是字典项目，没有其它的")
    private Boolean isGroup;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "私有标识,模糊查询")
    private String privateFlag;

    @ApiModelProperty(value = "私有标识备忘")
    private String privateFlagMemo;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "分组标识备忘")
    private String groupFlagMemo;

    @ApiModelProperty(value = "标签，多个以逗号分隔，用来区分字典项")
    private String tags;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;


}
