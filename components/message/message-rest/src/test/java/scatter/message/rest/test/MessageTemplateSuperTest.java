package scatter.message.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.message.pojo.po.MessageTemplate;
import scatter.message.pojo.form.MessageTemplateAddForm;
import scatter.message.pojo.form.MessageTemplateUpdateForm;
import scatter.message.pojo.form.MessageTemplatePageQueryForm;
import scatter.message.rest.service.IMessageTemplateService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 消息内容模板 测试类基类
* </p>
*
* @author yw
* @since 2021-08-11
*/
public class MessageTemplateSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IMessageTemplateService messageTemplateService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return messageTemplateService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return messageTemplateService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public MessageTemplate mockPo() {
        return JMockData.mock(MessageTemplate.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public MessageTemplateAddForm mockAddForm() {
        return JMockData.mock(MessageTemplateAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public MessageTemplateUpdateForm mockUpdateForm() {
        return JMockData.mock(MessageTemplateUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public MessageTemplatePageQueryForm mockPageQueryForm() {
        return JMockData.mock(MessageTemplatePageQueryForm.class, mockConfig);
    }
}