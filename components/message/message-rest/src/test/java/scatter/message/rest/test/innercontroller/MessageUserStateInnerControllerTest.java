package scatter.message.rest.test.innercontroller;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.message.rest.test.MessageUserStateSuperTest;
/**
* <p>
* 用户消息读取状态 内部调用前端控制器测试类
* </p>
*
* @author yw
* @since 2021-08-11
*/
@SpringBootTest
public class MessageUserStateInnerControllerTest extends MessageUserStateSuperTest{
    @Test
    void contextLoads() {
    }
}