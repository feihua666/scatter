package scatter.message.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.message.pojo.po.Message;
import scatter.message.pojo.form.MessageAddForm;
import scatter.message.pojo.form.MessageUpdateForm;
import scatter.message.pojo.form.MessagePageQueryForm;
import scatter.message.rest.service.IMessageService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 消息 测试类基类
* </p>
*
* @author yw
* @since 2021-08-11
*/
public class MessageSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IMessageService messageService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return messageService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return messageService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public Message mockPo() {
        return JMockData.mock(Message.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public MessageAddForm mockAddForm() {
        return JMockData.mock(MessageAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public MessageUpdateForm mockUpdateForm() {
        return JMockData.mock(MessageUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public MessagePageQueryForm mockPageQueryForm() {
        return JMockData.mock(MessagePageQueryForm.class, mockConfig);
    }
}