package scatter.message.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.message.pojo.po.MessageTemplate;
import scatter.message.pojo.form.MessageTemplateAddForm;
import scatter.message.pojo.form.MessageTemplateUpdateForm;
import scatter.message.pojo.form.MessageTemplatePageQueryForm;
import scatter.message.rest.test.MessageTemplateSuperTest;
import scatter.message.rest.service.IMessageTemplateService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 消息内容模板 服务测试类
* </p>
*
* @author yw
* @since 2021-08-11
*/
@SpringBootTest
public class MessageTemplateServiceTest extends MessageTemplateSuperTest{

    @Autowired
    private IMessageTemplateService messageTemplateService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<MessageTemplate> pos = messageTemplateService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}