package scatter.message.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.message.pojo.po.MessageUserState;
import scatter.message.pojo.form.MessageUserStateAddForm;
import scatter.message.pojo.form.MessageUserStateUpdateForm;
import scatter.message.pojo.form.MessageUserStatePageQueryForm;
import scatter.message.rest.test.MessageUserStateSuperTest;
import scatter.message.rest.service.IMessageUserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户消息读取状态 服务测试类
* </p>
*
* @author yw
* @since 2021-08-11
*/
@SpringBootTest
public class MessageUserStateServiceTest extends MessageUserStateSuperTest{

    @Autowired
    private IMessageUserStateService messageUserStateService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<MessageUserState> pos = messageUserStateService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}