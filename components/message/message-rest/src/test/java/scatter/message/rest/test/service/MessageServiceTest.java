package scatter.message.rest.test.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.message.pojo.po.Message;
import scatter.message.pojo.form.MessageAddForm;
import scatter.message.pojo.form.MessageUpdateForm;
import scatter.message.pojo.form.MessagePageQueryForm;
import scatter.message.rest.test.MessageSuperTest;
import scatter.message.rest.service.IMessageService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 消息 服务测试类
* </p>
*
* @author yw
* @since 2021-08-11
*/
@SpringBootTest
public class MessageServiceTest extends MessageSuperTest{

    @Autowired
    private IMessageService messageService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<Message> pos = messageService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}