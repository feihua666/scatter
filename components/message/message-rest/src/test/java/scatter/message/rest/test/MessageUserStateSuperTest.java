package scatter.message.rest.test;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.message.pojo.po.MessageUserState;
import scatter.message.pojo.form.MessageUserStateAddForm;
import scatter.message.pojo.form.MessageUserStateUpdateForm;
import scatter.message.pojo.form.MessageUserStatePageQueryForm;
import scatter.message.rest.service.IMessageUserStateService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户消息读取状态 测试类基类
* </p>
*
* @author yw
* @since 2021-08-11
*/
public class MessageUserStateSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IMessageUserStateService messageUserStateService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return messageUserStateService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return messageUserStateService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public MessageUserState mockPo() {
        return JMockData.mock(MessageUserState.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public MessageUserStateAddForm mockAddForm() {
        return JMockData.mock(MessageUserStateAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public MessageUserStateUpdateForm mockUpdateForm() {
        return JMockData.mock(MessageUserStateUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public MessageUserStatePageQueryForm mockPageQueryForm() {
        return JMockData.mock(MessageUserStatePageQueryForm.class, mockConfig);
    }
}