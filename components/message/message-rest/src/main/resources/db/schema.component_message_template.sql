DROP TABLE IF EXISTS component_message_template;
CREATE TABLE `component_message_template` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `code` varchar(255) NOT NULL COMMENT '编码，不能重复',
  `name` varchar(255) NOT NULL COMMENT '模板名称',
  `title_tpl` varchar(255) DEFAULT NULL COMMENT '消息标题模板',
  `content_tpl` varchar(2000) NOT NULL COMMENT '消息内容模板',
  `type_dict_id` varchar(20) NOT NULL COMMENT '消息模板分类，字典id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) NOT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`),
  KEY `type_dict_id` (`type_dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='消息内容模板表';
