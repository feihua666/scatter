DROP TABLE IF EXISTS component_message;
CREATE TABLE `component_message` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `title` varchar(255) NOT NULL COMMENT '消息标题',
  `short_content` varchar(255) DEFAULT NULL COMMENT '消息简短内容',
  `content` varchar(2000) NOT NULL COMMENT '消息内容',
  `business_data` varchar(2000) DEFAULT NULL COMMENT '业务内容',
  `business_id` varchar(20) DEFAULT NULL COMMENT '业务数据id',
  `type_dict_id` varchar(20) DEFAULT NULL COMMENT '消息分类，字典',
  `send_status_dict_id` varchar(20) NOT NULL COMMENT '发送状态，字典id，未发送，发送中，已发送',
  `send_user_id` varchar(20) DEFAULT NULL COMMENT '发送人id',
  `send_at` datetime DEFAULT NULL COMMENT '发送时间',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) NOT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type_dict_id` (`type_dict_id`),
  KEY `send_status_dict_id` (`send_status_dict_id`),
  KEY `business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='消息表';
