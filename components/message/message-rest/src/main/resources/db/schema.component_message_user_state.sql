DROP TABLE IF EXISTS component_message_user_state;
CREATE TABLE `component_message_user_state` (
  `id` varchar(20) NOT NULL COMMENT '表主键',
  `message_id` varchar(20) NOT NULL COMMENT '消息表主键',
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `is_read` tinyint(1) NOT NULL COMMENT '是否已读，1=已读，0=未读',
  `read_at` datetime DEFAULT NULL COMMENT '读取时间',
  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` bigint(20) NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) NOT NULL COMMENT '创建人',
  `update_at` bigint(20) DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `message_id`(`message_id`, `user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户消息读取状态表';
