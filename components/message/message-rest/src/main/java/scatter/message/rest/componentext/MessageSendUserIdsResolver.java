package scatter.message.rest.componentext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import scatter.common.pojo.form.BasePageQueryForm;

import java.util.List;

/**
 * <p>
 * 消息发送，用户id获取接口
 * </p>
 *
 * @author yangwei
 * @since 2021-11-25 19:07
 */
public interface MessageSendUserIdsResolver {

	public static enum DataType{
		comp,
		dept,
		role,
		post;
	}


	/**
	 * 根据支持的类型，获取用户id
	 * @param dataIds
	 * @param dataType {@link DataType}
	 * @param basePageQueryForm
	 * @return
	 */
	IPage<String> resolverUserIds(List<String> dataIds,String dataType, BasePageQueryForm basePageQueryForm);
}
