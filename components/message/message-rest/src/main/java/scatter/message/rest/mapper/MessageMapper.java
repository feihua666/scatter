package scatter.message.rest.mapper;

import scatter.message.pojo.po.Message;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 消息表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface MessageMapper extends IBaseMapper<Message> {

}
