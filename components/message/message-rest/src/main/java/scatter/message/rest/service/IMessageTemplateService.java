package scatter.message.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.message.pojo.po.MessageTemplate;
import scatter.common.rest.service.IBaseService;
import scatter.message.pojo.form.MessageTemplateAddForm;
import scatter.message.pojo.form.MessageTemplateUpdateForm;
import scatter.message.pojo.form.MessageTemplatePageQueryForm;
import java.util.List;
/**
 * <p>
 * 消息内容模板表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface IMessageTemplateService extends IBaseService<MessageTemplate> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default MessageTemplate getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<MessageTemplate>lambdaQuery().eq(MessageTemplate::getCode, code));
    }

}
