package scatter.message.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.message.rest.MessageConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.message.pojo.po.MessageUserState;
import scatter.message.pojo.vo.MessageUserStateVo;
import scatter.message.pojo.form.MessageUserStateAddForm;
import scatter.message.pojo.form.MessageUserStateUpdateForm;
import scatter.message.pojo.form.MessageUserStatePageQueryForm;
import scatter.message.rest.service.IMessageUserStateService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 用户消息读取状态表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Api(tags = "用户消息读取状态相关接口")
@RestController
@RequestMapping(MessageConfiguration.CONTROLLER_BASE_PATH + "/message-user-state")
public class MessageUserStateController extends BaseAddUpdateQueryFormController<MessageUserState, MessageUserStateVo, MessageUserStateAddForm, MessageUserStateUpdateForm, MessageUserStatePageQueryForm> {
    @Autowired
    private IMessageUserStateService iMessageUserStateService;

    @ApiOperation("添加用户消息读取状态")
    @PreAuthorize("hasAuthority('MessageUserState:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MessageUserStateVo add(@RequestBody @Valid MessageUserStateAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询用户消息读取状态")
    @PreAuthorize("hasAuthority('MessageUserState:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public MessageUserStateVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除用户消息读取状态")
    @PreAuthorize("hasAuthority('MessageUserState:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新用户消息读取状态")
    @PreAuthorize("hasAuthority('MessageUserState:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MessageUserStateVo update(@RequestBody @Valid MessageUserStateUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询用户消息读取状态")
    @PreAuthorize("hasAuthority('MessageUserState:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<MessageUserStateVo> getList(MessageUserStatePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询用户消息读取状态")
    @PreAuthorize("hasAuthority('MessageUserState:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<MessageUserStateVo> getPage(MessageUserStatePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
