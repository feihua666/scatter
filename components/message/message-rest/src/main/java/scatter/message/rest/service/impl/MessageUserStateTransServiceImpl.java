package scatter.message.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.message.pojo.po.MessageUserState;
import scatter.message.rest.service.IMessageUserStateService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户消息读取状态翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Component
public class MessageUserStateTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IMessageUserStateService messageUserStateService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,MessageUserState.TRANS_MESSAGEUSERSTATE_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,MessageUserState.TRANS_MESSAGEUSERSTATE_BY_ID)) {
            MessageUserState byId = messageUserStateService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,MessageUserState.TRANS_MESSAGEUSERSTATE_BY_ID)) {
            return messageUserStateService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
