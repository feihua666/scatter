package scatter.message.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.message.pojo.param.MessageSendParam;
import scatter.message.pojo.po.Message;
import scatter.common.rest.service.IBaseService;
import scatter.message.pojo.form.MessageAddForm;
import scatter.message.pojo.form.MessageUpdateForm;
import scatter.message.pojo.form.MessagePageQueryForm;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消息表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface IMessageService extends IBaseService<Message> {

	/**
	 * 消息发送
	 * @param sendParam
	 */
	void send(MessageSendParam sendParam);

	/**
	 * 修改发送状态
	 * @param messageId
	 * @param sendUserId 发送人用户id，只在从未发送到发送中时需要传
	 * @param sendStatusDictItem
	 * @return
	 */
	boolean updateSendStatus(String messageId,String sendUserId, Message.SendStatusDictItem sendStatusDictItem);

	/**
	 * 根据模板编码，映射消息游离态实体
	 * @param code
	 * @param titleParams
	 * @param contentParams
	 * @return
	 */
	Message mapMessageByTemplateCode(String code, Map<String,Object> titleParams,Map<String,Object> contentParams);

}
