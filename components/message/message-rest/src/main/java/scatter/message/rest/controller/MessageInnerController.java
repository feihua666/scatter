package scatter.message.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.message.rest.MessageConfiguration;
import scatter.message.pojo.po.Message;
import scatter.message.rest.service.IMessageService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 消息表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@RestController
@RequestMapping(MessageConfiguration.CONTROLLER_BASE_PATH + "/inner/message")
public class MessageInnerController extends BaseInnerController<Message> {
 @Autowired
 private IMessageService messageService;

 public IMessageService getService(){
     return messageService;
 }
}
