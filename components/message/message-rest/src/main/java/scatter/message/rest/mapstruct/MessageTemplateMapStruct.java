package scatter.message.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.message.pojo.po.MessageTemplate;
import scatter.message.pojo.vo.MessageTemplateVo;
import scatter.message.pojo.form.MessageTemplateAddForm;
import scatter.message.pojo.form.MessageTemplateUpdateForm;
import scatter.message.pojo.form.MessageTemplatePageQueryForm;

/**
 * <p>
 * 消息内容模板 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MessageTemplateMapStruct extends IBaseVoMapStruct<MessageTemplate, MessageTemplateVo>,
                                  IBaseAddFormMapStruct<MessageTemplate,MessageTemplateAddForm>,
                                  IBaseUpdateFormMapStruct<MessageTemplate,MessageTemplateUpdateForm>,
                                  IBaseQueryFormMapStruct<MessageTemplate,MessageTemplatePageQueryForm>{
    MessageTemplateMapStruct INSTANCE = Mappers.getMapper( MessageTemplateMapStruct.class );

}
