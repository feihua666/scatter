package scatter.message.rest.service.impl;

import scatter.message.pojo.po.MessageTemplate;
import scatter.message.rest.mapper.MessageTemplateMapper;
import scatter.message.rest.service.IMessageTemplateService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.message.pojo.form.MessageTemplateAddForm;
import scatter.message.pojo.form.MessageTemplateUpdateForm;
import scatter.message.pojo.form.MessageTemplatePageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 消息内容模板表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Service
@Transactional
public class MessageTemplateServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<MessageTemplateMapper, MessageTemplate, MessageTemplateAddForm, MessageTemplateUpdateForm, MessageTemplatePageQueryForm> implements IMessageTemplateService {
    @Override
    public void preAdd(MessageTemplateAddForm addForm,MessageTemplate po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),MessageTemplate::getCode,false);
        }

    }

    @Override
    public void preUpdate(MessageTemplateUpdateForm updateForm,MessageTemplate po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            MessageTemplate byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),MessageTemplate::getCode,false);
            }
        }

    }
}
