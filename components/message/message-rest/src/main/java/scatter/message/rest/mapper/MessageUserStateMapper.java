package scatter.message.rest.mapper;

import scatter.message.pojo.po.MessageUserState;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户消息读取状态表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface MessageUserStateMapper extends IBaseMapper<MessageUserState> {

}
