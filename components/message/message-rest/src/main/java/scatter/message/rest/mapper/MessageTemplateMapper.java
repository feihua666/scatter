package scatter.message.rest.mapper;

import scatter.message.pojo.po.MessageTemplate;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 消息内容模板表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface MessageTemplateMapper extends IBaseMapper<MessageTemplate> {

}
