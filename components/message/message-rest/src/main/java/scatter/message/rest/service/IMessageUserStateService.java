package scatter.message.rest.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.message.pojo.po.Message;
import scatter.message.pojo.po.MessageUserState;
import scatter.common.rest.service.IBaseService;
import scatter.message.pojo.form.MessageUserStateAddForm;
import scatter.message.pojo.form.MessageUserStateUpdateForm;
import scatter.message.pojo.form.MessageUserStatePageQueryForm;
import java.util.List;
/**
 * <p>
 * 用户消息读取状态表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
public interface IMessageUserStateService extends IBaseService<MessageUserState> {

	/**
	 * 根据消息id和用户id获取
	 * @param messageId 不能为空
	 * @param userIds 不能为空
	 * @return
	 */
	default List<MessageUserState> getByMessageIdAndUserIds(String messageId,List<String> userIds){

		return list(Wrappers.<MessageUserState>lambdaQuery().eq(MessageUserState::getMessageId,messageId).in(MessageUserState::getUserId,userIds));
	}

	/**
	 * 发送数据消息
	 * @param message
	 * @param userIdsList
	 * @param checkHasUserIds 是否检查给定的 userIdsList 参数中是否已在数据中存在，如果存在不处理已存在的数据，不检查如果存在重复可能会报唯一索引异常，所以要求在使用时自己有个判断是否需要
	 */
	List<MessageUserState> sendMessageByUserIds(Message message, List<String> userIdsList,boolean checkHasUserIds);

}
