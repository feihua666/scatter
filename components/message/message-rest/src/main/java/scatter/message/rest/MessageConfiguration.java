package scatter.message.rest;

import io.micrometer.core.instrument.MeterRegistry;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import scatter.common.rest.concurrency.CustomExecutors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * <p>
 *
 * </p>
 *
 * @author yangwei
 * @since 2020-11-10 22:41
 */
@ComponentScan
@MapperScan("scatter.message.rest.**.mapper")
public class MessageConfiguration {
    /**
     * controller访问基础路径
     */
    public static final String CONTROLLER_BASE_PATH = "";
    /**
     * FeignClient
     */
    public static final String FEIGN_CLIENT = "message";

    /**
     * 通用数据库查询线程池
     * @param beanFactory
     * @return
     */
    @Bean(name = "messageSendExecutor", destroyMethod = "shutdown")
    public ExecutorService messageSendExecutor(BeanFactory beanFactory, MeterRegistry meterRegistry) {
        return CustomExecutors.newExecutorService(beanFactory,
                "messageSendExecutor",
                5,
                100,
                1000,
                new LinkedBlockingQueue<>(1000),
                // 如果拒绝自己执行
                new ThreadPoolExecutor.CallerRunsPolicy(),
                true,meterRegistry);
    }
}
