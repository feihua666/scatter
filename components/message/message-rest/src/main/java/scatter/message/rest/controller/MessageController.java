package scatter.message.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.message.pojo.form.MessageSendForm;
import scatter.message.rest.MessageConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.message.pojo.po.Message;
import scatter.message.pojo.vo.MessageVo;
import scatter.message.pojo.form.MessageAddForm;
import scatter.message.pojo.form.MessageUpdateForm;
import scatter.message.pojo.form.MessagePageQueryForm;
import scatter.message.rest.mapstruct.MessageMapStruct;
import scatter.message.rest.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 消息表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Api(tags = "消息相关接口")
@RestController
@RequestMapping(MessageConfiguration.CONTROLLER_BASE_PATH + "/message")
public class MessageController extends BaseAddUpdateQueryFormController<Message, MessageVo, MessageAddForm, MessageUpdateForm, MessagePageQueryForm> {
    @Autowired
    private IMessageService iMessageService;

    @ApiOperation("添加消息")
    @PreAuthorize("hasAuthority('Message:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MessageVo add(@RequestBody @Valid MessageAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询消息")
    @PreAuthorize("hasAuthority('Message:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public MessageVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除消息")
    @PreAuthorize("hasAuthority('Message:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新消息")
    @PreAuthorize("hasAuthority('Message:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MessageVo update(@RequestBody @Valid MessageUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询消息")
    @PreAuthorize("hasAuthority('Message:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<MessageVo> getList(MessagePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询消息")
    @PreAuthorize("hasAuthority('Message:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<MessageVo> getPage(MessagePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }


    /**
     * 该功能目前开发主要用于后台群发等
     * @param sendForm
     * @return
     */
    @ApiOperation("发送消息")
    @PreAuthorize("hasAuthority('Message:send')")
    @PostMapping("/send")
    @ResponseStatus(HttpStatus.CREATED)
    public MessageVo send(@RequestBody @Valid MessageSendForm sendForm) {
        MessageMapStruct.INSTANCE.messageSendFormToParam(sendForm);
        return this.queryById(sendForm.getMessageId());
    }
}
