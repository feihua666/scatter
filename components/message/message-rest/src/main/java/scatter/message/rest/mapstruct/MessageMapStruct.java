package scatter.message.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.message.pojo.form.MessageSendForm;
import scatter.message.pojo.param.MessageSendParam;
import scatter.message.pojo.po.Message;
import scatter.message.pojo.vo.MessageVo;
import scatter.message.pojo.form.MessageAddForm;
import scatter.message.pojo.form.MessageUpdateForm;
import scatter.message.pojo.form.MessagePageQueryForm;

/**
 * <p>
 * 消息 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MessageMapStruct extends IBaseVoMapStruct<Message, MessageVo>,
                                  IBaseAddFormMapStruct<Message,MessageAddForm>,
                                  IBaseUpdateFormMapStruct<Message,MessageUpdateForm>,
                                  IBaseQueryFormMapStruct<Message,MessagePageQueryForm>{
    MessageMapStruct INSTANCE = Mappers.getMapper( MessageMapStruct.class );

    MessageSendParam messageSendFormToParam(MessageSendForm sendForm);
}
