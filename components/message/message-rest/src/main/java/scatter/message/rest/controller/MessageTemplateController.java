package scatter.message.rest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.message.rest.MessageConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.message.pojo.po.MessageTemplate;
import scatter.message.pojo.vo.MessageTemplateVo;
import scatter.message.pojo.form.MessageTemplateAddForm;
import scatter.message.pojo.form.MessageTemplateUpdateForm;
import scatter.message.pojo.form.MessageTemplatePageQueryForm;
import scatter.message.rest.service.IMessageTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 消息内容模板表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Api(tags = "消息内容模板相关接口")
@RestController
@RequestMapping(MessageConfiguration.CONTROLLER_BASE_PATH + "/message-template")
public class MessageTemplateController extends BaseAddUpdateQueryFormController<MessageTemplate, MessageTemplateVo, MessageTemplateAddForm, MessageTemplateUpdateForm, MessageTemplatePageQueryForm> {
    @Autowired
    private IMessageTemplateService iMessageTemplateService;

    @ApiOperation("添加消息内容模板")
    @PreAuthorize("hasAuthority('MessageTemplate:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MessageTemplateVo add(@RequestBody @Valid MessageTemplateAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询消息内容模板")
    @PreAuthorize("hasAuthority('MessageTemplate:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public MessageTemplateVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除消息内容模板")
    @PreAuthorize("hasAuthority('MessageTemplate:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新消息内容模板")
    @PreAuthorize("hasAuthority('MessageTemplate:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public MessageTemplateVo update(@RequestBody @Valid MessageTemplateUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询消息内容模板")
    @PreAuthorize("hasAuthority('MessageTemplate:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<MessageTemplateVo> getList(MessageTemplatePageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询消息内容模板")
    @PreAuthorize("hasAuthority('MessageTemplate:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<MessageTemplateVo> getPage(MessageTemplatePageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
