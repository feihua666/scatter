package scatter.message.rest.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.message.pojo.po.MessageUserState;
import scatter.message.pojo.vo.MessageUserStateVo;
import scatter.message.pojo.form.MessageUserStateAddForm;
import scatter.message.pojo.form.MessageUserStateUpdateForm;
import scatter.message.pojo.form.MessageUserStatePageQueryForm;

/**
 * <p>
 * 用户消息读取状态 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MessageUserStateMapStruct extends IBaseVoMapStruct<MessageUserState, MessageUserStateVo>,
                                  IBaseAddFormMapStruct<MessageUserState,MessageUserStateAddForm>,
                                  IBaseUpdateFormMapStruct<MessageUserState,MessageUserStateUpdateForm>,
                                  IBaseQueryFormMapStruct<MessageUserState,MessageUserStatePageQueryForm>{
    MessageUserStateMapStruct INSTANCE = Mappers.getMapper( MessageUserStateMapStruct.class );

}
