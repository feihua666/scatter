package scatter.message.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户消息读取状态更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="用户消息读取状态更新表单对象")
public class MessageUserStateUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="消息表主键不能为空")
    @ApiModelProperty(value = "消息表主键",required = true)
    private String messageId;

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;

    @NotNull(message="是否已读不能为空")
    @ApiModelProperty(value = "是否已读，1=已读，0=未读",required = true)
    private Boolean isRead;

    @ApiModelProperty(value = "读取时间")
    private LocalDateTime readAt;

}
