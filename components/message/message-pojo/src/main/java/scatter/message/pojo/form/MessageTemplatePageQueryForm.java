package scatter.message.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 消息内容模板分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="消息内容模板分页表单对象")
public class MessageTemplatePageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "编码，不能重复")
    private String code;

    @Like
    @ApiModelProperty(value = "模板名称")
    private String name;

    @Like
    @ApiModelProperty(value = "消息标题模板")
    private String titleTpl;

    @ApiModelProperty(value = "消息模板分类，字典id")
    private String typeDictId;
}
