package scatter.message.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息内容模板表
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_message_template")
@ApiModel(value="MessageTemplate对象", description="消息内容模板表")
public class MessageTemplate extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_MESSAGETEMPLATE_BY_ID = "trans_messagetemplate_by_id_scatter.message.pojo.po";

    @ApiModelProperty(value = "编码，不能重复")
    private String code;

    @ApiModelProperty(value = "模板名称")
    private String name;

    @ApiModelProperty(value = "消息标题模板")
    private String titleTpl;

    @ApiModelProperty(value = "消息内容模板")
    private String contentTpl;

    @ApiModelProperty(value = "消息模板分类，字典id")
    private String typeDictId;

    @ApiModelProperty(value = "备注")
    private String remark;


}
