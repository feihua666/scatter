package scatter.message.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 消息内容模板更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="消息内容模板更新表单对象")
public class MessageTemplateUpdateForm extends BaseUpdateIdForm {

    @ApiModelProperty(value = "编码，不能重复")
    private String code;

    @NotEmpty(message="模板名称不能为空")
    @ApiModelProperty(value = "模板名称",required = true)
    private String name;

    @ApiModelProperty(value = "消息标题模板")
    private String titleTpl;

    @NotEmpty(message="消息内容模板不能为空")
    @ApiModelProperty(value = "消息内容模板",required = true)
    private String contentTpl;

    @NotEmpty(message="消息模板分类不能为空")
    @ApiModelProperty(value = "消息模板分类，字典id",required = true)
    private String typeDictId;

    @ApiModelProperty(value = "备注")
    private String remark;

}
