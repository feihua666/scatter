package scatter.message.pojo.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;

import scatter.common.pojo.form.SetNullWhenNull;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 消息更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="消息更新表单对象")
public class MessageUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="消息标题不能为空")
    @ApiModelProperty(value = "消息标题",required = true)
    private String title;

    @SetNullWhenNull
    @ApiModelProperty(value = "消息简短内容")
    private String shortContent;

    @NotEmpty(message="消息内容不能为空")
    @ApiModelProperty(value = "消息内容",required = true)
    private String content;

    @SetNullWhenNull
    @ApiModelProperty(value = "业务内容")
    private String businessData;

    @SetNullWhenNull
    @ApiModelProperty(value = "业务数据id")
    private String businessId;

    @ApiModelProperty(value = "消息分类，字典")
    private String typeDictId;

}
