package scatter.message.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 消息响应对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="消息响应对象")
public class MessageVo extends BaseIdVo {
    /**
     * 预留翻译，因为用户体系不确定
     */
    public final static String TRANS_MESSAGE_VO_SEND_USER_NICKNAME_BY_USERID = "trans_message_vo_send_user_nickname_by_userid";

    @ApiModelProperty(value = "消息标题")
    private String title;

    @ApiModelProperty(value = "消息简短内容")
    private String shortContent;

    @ApiModelProperty(value = "消息内容")
    private String content;

    @ApiModelProperty(value = "业务内容")
    private String businessData;

    @ApiModelProperty(value = "业务数据id")
    private String businessId;

    @ApiModelProperty(value = "消息分类，字典")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "消息分类，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "发送状态，字典id，未发送，发送中，已发送")
    private String sendStatusDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sendStatusDictId",mapValueField = "code")
    @ApiModelProperty(value = "发送状态，字典编码")
    private String sendStatusDictCode;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sendStatusDictId",mapValueField = "name")
    @ApiModelProperty(value = "发送状态，字典名称")
    private String sendStatusDictName;

    @ApiModelProperty(value = "发送人id")
    private String sendUserId;

    @TransBy(type = TRANS_MESSAGE_VO_SEND_USER_NICKNAME_BY_USERID,byFieldName = "sendUserId")
    @ApiModelProperty(value = "发送人昵称")
    private String sendUserNickname;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime sendAt;

}
