package scatter.message.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 消息分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="消息分页表单对象")
public class MessagePageQueryForm extends BasePageQueryForm {

    @Like
    @ApiModelProperty(value = "消息标题")
    private String title;

    @Like
    @ApiModelProperty(value = "消息简短内容")
    private String shortContent;

    @ApiModelProperty(value = "业务数据id")
    private String businessId;

    @ApiModelProperty(value = "消息分类，字典")
    private String typeDictId;

    @ApiModelProperty(value = "发送状态，字典id，未发送，发送中，已发送")
    private String sendStatusDictId;
}
