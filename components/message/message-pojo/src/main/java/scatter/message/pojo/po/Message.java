package scatter.message.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;

import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_message")
@ApiModel(value="Message对象", description="消息表")
public class Message extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_MESSAGE_BY_ID = "trans_message_by_id_scatter.message.pojo.po";


    /**
     * 消息类型字典组编码
     */
    public enum TypeDictGroup implements IDictGroup {
        /**
         * 消息类型
         */
        message_type;

        @Override
        public String groupCode() {
            return this.name();
        }}

    /**
     * 消息类型字典项编码
     */
    public enum TypeDictItem implements IDictItem {
        /**
         * 系统消息
         */
        system,
        /**
         * 点赞消息
         */
        star,
        /**
         * 评论消息
         */
        comment,
        /**
         * 审批消息
         */
        audit,
        /**
         * 访客消息
         */
        visitor;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return TypeDictGroup.message_type.groupCode();
        }

    }

    /**
     * 消息发送状态字典组编码
     */
    public enum SendStatusDictGroup implements IDictGroup {
        /**
         * 消息发送状态
         */
        message_send_status;

        @Override
        public String groupCode() {
            return this.name();
        }}

    /**
     * 消息发送状态字典项编码
     */
    public enum SendStatusDictItem implements IDictItem {
        /**
         * 未发送
         */
        not_send,
        /**
         * 发送中
         */
        sending,
        /**
         * 已发送
         */
        sent,
        /**
         * 发送失败
         */
        send_fail;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return SendStatusDictGroup.message_send_status.groupCode();
        }

    }

    @ApiModelProperty(value = "消息标题")
    private String title;

    @ApiModelProperty(value = "消息简短内容")
    private String shortContent;

    @ApiModelProperty(value = "消息内容")
    private String content;

    @ApiModelProperty(value = "业务内容")
    private String businessData;

    @ApiModelProperty(value = "业务数据id")
    private String businessId;

    @ApiModelProperty(value = "消息分类，字典")
    private String typeDictId;

    @ApiModelProperty(value = "发送状态，字典id，未发送，发送中，已发送")
    private String sendStatusDictId;

    @ApiModelProperty(value = "发送人id")
    private String sendUserId;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime sendAt;


}
