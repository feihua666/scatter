package scatter.message.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户消息读取状态响应对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="用户消息读取状态响应对象")
public class MessageUserStateVo extends BaseIdVo {

    @ApiModelProperty(value = "消息表主键")
    private String messageId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "是否已读，1=已读，0=未读")
    private Boolean isRead;

    @ApiModelProperty(value = "读取时间")
    private LocalDateTime readAt;

}
