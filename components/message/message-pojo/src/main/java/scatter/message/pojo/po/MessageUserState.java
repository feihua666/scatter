package scatter.message.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户消息读取状态表
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_message_user_state")
@ApiModel(value="MessageUserState对象", description="用户消息读取状态表")
public class MessageUserState extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_MESSAGEUSERSTATE_BY_ID = "trans_messageuserstate_by_id_scatter.message.pojo.po";

    @ApiModelProperty(value = "消息表主键")
    private String messageId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "是否已读，1=已读，0=未读")
    private Boolean isRead;

    @ApiModelProperty(value = "读取时间")
    private LocalDateTime readAt;


}
