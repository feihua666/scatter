package scatter.message.pojo.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseForm;
import scatter.common.pojo.param.BaseParam;
import scatter.common.rest.notify.NotifyParam;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消息发送参数对象
 * </p>
 *
 * @author yw
 * @since 2021-11-25
 */
@Setter
@Getter
@ApiModel(value="消息发送参数对象")
public class MessageSendParam extends BaseParam {

    @NotEmpty(message="消息id不能为空")
    @ApiModelProperty(value = "消息id",required = true)
    private String messageId;

    @ApiModelProperty(value = "发送人用户id")
    private String sendUserId;

    @ApiModelProperty(value = "要发送的用户ids")
    private List<String> userIds;

    @ApiModelProperty(value = "要发送的公司ids")
    private List<String> compIds;

    @ApiModelProperty(value = "要发送的部门ids")
    private List<String> deptIds;

    @ApiModelProperty(value = "要发送的角色ids")
    private List<String> roleIds;

    @ApiModelProperty(value = "要发送的岗位ids")
    private List<String> postIds;

    /**
	 * 类型参数 {@link NotifyParam.Type}
     */
    @ApiModelProperty(value = "要通知的类型")
    private List<String> notifyTypes;

    /**
	 * 主要用于，通知细节
     * 如：要发微信公众号消息，这里可以定义是哪个公众号，具体怎么用，以实现为准
     */
    @ApiModelProperty(value = "通知详情")
    private Map<String,Object> notifyDetail;


}
