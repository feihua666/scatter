package scatter.message.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 消息内容模板响应对象
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Setter
@Getter
@ApiModel(value="消息内容模板响应对象")
public class MessageTemplateVo extends BaseIdVo {

    @ApiModelProperty(value = "编码，不能重复")
    private String code;

    @ApiModelProperty(value = "模板名称")
    private String name;

    @ApiModelProperty(value = "消息标题模板")
    private String titleTpl;

    @ApiModelProperty(value = "消息内容模板")
    private String contentTpl;

    @ApiModelProperty(value = "消息模板分类，字典id")
    private String typeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "typeDictId",mapValueField = "name")
    @ApiModelProperty(value = "消息模板分类，字典名称")
    private String typeDictName;

    @ApiModelProperty(value = "备注")
    private String remark;

}
