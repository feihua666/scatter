const basePath = '' + '/message'
const MessageUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/MessageSearchList',
        add: '/MessageAdd',
        update: '/MessageUpdate',
    }
}
export default MessageUrl