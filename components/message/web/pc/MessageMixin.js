import MessageForm from './MessageForm.js'
import MessageTable from './MessageTable.js'
import MessageUrl from './MessageUrl.js'
const MessageMixin = {
    computed: {
        MessageFormOptions() {
            return this.$stDynamicFormTools.options(MessageForm,this.$options.name)
        },
        MessageTableOptions() {
            return MessageTable
        },
        MessageUrl(){
            return MessageUrl
        }
    },
}
export default MessageMixin