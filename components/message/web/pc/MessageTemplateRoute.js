import MessageTemplateUrl from './MessageTemplateUrl.js'

const MessageTemplateRoute = [
    {
        path: MessageTemplateUrl.router.searchList,
        component: () => import('./element/MessageTemplateSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MessageTemplateSearchList',
            name: '消息内容模板管理'
        }
    },
    {
        path: MessageTemplateUrl.router.add,
        component: () => import('./element/MessageTemplateAdd'),
        meta: {
            code:'MessageTemplateAdd',
            name: '消息内容模板添加'
        }
    },
    {
        path: MessageTemplateUrl.router.update,
        component: () => import('./element/MessageTemplateUpdate'),
        meta: {
            code:'MessageTemplateUpdate',
            name: '消息内容模板修改'
        }
    },
]
export default MessageTemplateRoute