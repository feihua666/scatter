const basePath = '' + '/message-template'
const MessageTemplateUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/MessageTemplateSearchList',
        add: '/MessageTemplateAdd',
        update: '/MessageTemplateUpdate',
    }
}
export default MessageTemplateUrl