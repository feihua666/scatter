import MessageTemplateUrl from './MessageTemplateUrl.js'
const MessageTemplateForm = [
    {
        MessageTemplateSearchList: {
            element:{
                required: false
            }
        },
        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        MessageTemplateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '模板名称',
            required: true,
        }
    },
    {

        field: {
            name: 'titleTpl',
        },
        element:{
            label: '消息标题',
        }
    },
    {
        MessageTemplateSearchList: false,

        field: {
            name: 'contentTpl',
        },
        element:{
            label: '消息内容模板',
            isBlock: true,
            type: 'textarea',
            required: true,
        }
    },
    {
        MessageTemplateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '消息模板分类',
            type: 'selectDict',
            options: {
                groupCode: 'message_template_type'
            },
            required: true,
        }
    },
    {
        MessageTemplateSearchList: false,
        field: {
            name: 'remark',
        },
        element:{
            label: '备注',
        }
    },

]
export default MessageTemplateForm