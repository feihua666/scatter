const MessageTable = [
    {
        prop: 'title',
        label: '消息标题'
    },
    {
        prop: 'shortContent',
        label: '消息简短内容'
    },
    {
        prop: 'content',
        label: '消息内容'
    },
    {
        prop: 'businessData',
        label: '业务内容'
    },
    {
        prop: 'businessId',
        label: '业务数据id'
    },
    {
        prop: 'typeDictName',
        label: '消息分类'
    },
    {
        prop: 'sendStatusDictName',
        label: '发送状态'
    },
    {
        prop: 'sendUserNickname',
        label: '发送人'
    },
    {
        prop: 'sendAt',
        label: '发送时间'
    },
]
export default MessageTable