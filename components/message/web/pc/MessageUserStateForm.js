import MessageUserStateUrl from './MessageUserStateUrl.js'
const MessageUserStateForm = [
    {
        MessageUserStateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'messageId',
        },
        element:{
            label: '消息主键',
            required: true,
        }
    },
    {
        MessageUserStateSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        MessageUserStateSearchList: {
            field: {
                name: 'isRead',
                value: null,
            },
            element:{
                required: false
            }
        },

        field: {
            name: 'isRead',
            value: false,
        },
        element:{
            type: 'select',
            options: {
                datas: 'yesNoBool'
            },
            label: '是否已读',
            required: true,
        }
    },
    {
        MessageUserStateSearchList: false,
        field: {
            name: 'readAt',
        },
        element:{
            label: '读取时间',
        }
    },

]
export default MessageUserStateForm