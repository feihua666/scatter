import MessageTemplateForm from './MessageTemplateForm.js'
import MessageTemplateTable from './MessageTemplateTable.js'
import MessageTemplateUrl from './MessageTemplateUrl.js'
const MessageTemplateMixin = {
    computed: {
        MessageTemplateFormOptions() {
            return this.$stDynamicFormTools.options(MessageTemplateForm,this.$options.name)
        },
        MessageTemplateTableOptions() {
            return MessageTemplateTable
        },
        MessageTemplateUrl(){
            return MessageTemplateUrl
        }
    },
}
export default MessageTemplateMixin