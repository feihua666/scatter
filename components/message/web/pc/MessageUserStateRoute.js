import MessageUserStateUrl from './MessageUserStateUrl.js'

const MessageUserStateRoute = [
    {
        path: MessageUserStateUrl.router.searchList,
        component: () => import('./element/MessageUserStateSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MessageUserStateSearchList',
            name: '用户消息读取状态管理'
        }
    },

]
export default MessageUserStateRoute