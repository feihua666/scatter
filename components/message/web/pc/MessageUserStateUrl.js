const basePath = '' + '/message-user-state'
const MessageUserStateUrl = {
    searchList: basePath + '/getPage',
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/MessageUserStateSearchList',
    }
}
export default MessageUserStateUrl