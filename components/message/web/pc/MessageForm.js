import MessageUrl from './MessageUrl.js'
const MessageForm = [
    {
        MessageSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'title',
        },
        element:{
            label: '消息标题',
            required: true,
        }
    },
    {

        field: {
            name: 'shortContent',
        },
        element:{
            label: '消息简短内容',
        }
    },
    {
        MessageSearchList: false,

        field: {
            name: 'content',
        },
        element:{
            label: '消息内容',
            type: 'textarea',
            isBlock: true,
            required: true,
        }
    },
    {

        field: {
            name: 'businessId',
        },
        element:{
            label: '业务数据id',
        }
    },
    {
        MessageSearchList: false,
        field: {
            name: 'businessData',
        },
        element:{
            label: '业务内容',
            type: 'textarea',
            isBlock: true,
        }
    },

    {

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '消息分类',
            type: 'selectDict',
            options: {
                groupCode: 'message_type'
            }
        }
    },
    {
        MessageSearchList: {
            element:{
                required: false
            }
        },
        MessageAdd: false,
        MessageUpdate: false,
        field: {
            name: 'sendStatusDictId',
        },
        element:{
            label: '发送状态',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'message_send_status'
            }
        }
    },
]
export default MessageForm