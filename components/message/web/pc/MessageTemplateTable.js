const MessageTemplateTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '模板名称'
    },
    {
        prop: 'titleTpl',
        label: '消息标题模板'
    },
    {
        prop: 'contentTpl',
        label: '消息内容模板'
    },
    {
        prop: 'typeDictName',
        label: '消息模板分类'
    },
    {
        prop: 'remark',
        label: '备注'
    },
]
export default MessageTemplateTable