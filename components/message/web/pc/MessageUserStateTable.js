const MessageUserStateTable = [
    {
        prop: 'messageId',
        label: '消息表主键'
    },
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'isRead',
        label: '是否已读'
    },
    {
        prop: 'readAt',
        label: '读取时间'
    },
]
export default MessageUserStateTable