import MessageUrl from './MessageUrl.js'

const MessageRoute = [
    {
        path: MessageUrl.router.searchList,
        component: () => import('./element/MessageSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'MessageSearchList',
            name: '消息管理'
        }
    },
    {
        path: MessageUrl.router.add,
        component: () => import('./element/MessageAdd'),
        meta: {
            code:'MessageAdd',
            name: '消息添加'
        }
    },
    {
        path: MessageUrl.router.update,
        component: () => import('./element/MessageUpdate'),
        meta: {
            code:'MessageUpdate',
            name: '消息修改'
        }
    },
]
export default MessageRoute