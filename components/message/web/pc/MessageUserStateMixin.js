import MessageUserStateForm from './MessageUserStateForm.js'
import MessageUserStateTable from './MessageUserStateTable.js'
import MessageUserStateUrl from './MessageUserStateUrl.js'
const MessageUserStateMixin = {
    computed: {
        MessageUserStateFormOptions() {
            return this.$stDynamicFormTools.options(MessageUserStateForm,this.$options.name)
        },
        MessageUserStateTableOptions() {
            return MessageUserStateTable
        },
        MessageUserStateUrl(){
            return MessageUserStateUrl
        }
    },
}
export default MessageUserStateMixin