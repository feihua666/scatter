import MessageRoute from './MessageRoute.js'
import MessageTemplateRoute from './MessageTemplateRoute.js'
import MessageUserStateRoute from './MessageUserStateRoute.js'

const MessageRoutesEntry = [
]
.concat(MessageRoute)
.concat(MessageTemplateRoute)
.concat(MessageUserStateRoute)

export default MessageRoutesEntry