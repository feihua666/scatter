package scatter.message.rest.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户消息读取状态表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@Component
@FeignClient(value = "MessageUserState-client")
public interface MessageUserStateClient {

}
