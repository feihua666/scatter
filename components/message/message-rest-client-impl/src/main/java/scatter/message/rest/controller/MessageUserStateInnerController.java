package scatter.message.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.message.rest.MessageConfiguration;
import scatter.message.pojo.po.MessageUserState;
import scatter.message.rest.service.IMessageUserStateService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户消息读取状态表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-08-11
 */
@RestController
@RequestMapping(MessageConfiguration.CONTROLLER_BASE_PATH + "/inner/message-user-state")
public class MessageUserStateInnerController extends BaseInnerController<MessageUserState> {
 @Autowired
 private IMessageUserStateService messageUserStateService;

 public IMessageUserStateService getService(){
     return messageUserStateService;
 }
}
