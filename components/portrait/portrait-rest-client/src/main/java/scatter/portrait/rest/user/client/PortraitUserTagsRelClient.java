package scatter.portrait.rest.user.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户和标签关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Component
@FeignClient(value = "PortraitUserTagsRel-client")
public interface PortraitUserTagsRelClient {

}
