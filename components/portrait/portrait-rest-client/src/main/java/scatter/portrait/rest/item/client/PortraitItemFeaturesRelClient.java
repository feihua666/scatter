package scatter.portrait.rest.item.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * item和特征关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
@FeignClient(value = "PortraitItemFeaturesRel-client")
public interface PortraitItemFeaturesRelClient {

}
