package scatter.portrait.rest.item.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * item表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
@FeignClient(value = "PortraitItem-client")
public interface PortraitItemClient {

}
