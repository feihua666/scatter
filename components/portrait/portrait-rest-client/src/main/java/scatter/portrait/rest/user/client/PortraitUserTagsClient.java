package scatter.portrait.rest.user.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户标签字典表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
@FeignClient(value = "PortraitUserTags-client")
public interface PortraitUserTagsClient {

}
