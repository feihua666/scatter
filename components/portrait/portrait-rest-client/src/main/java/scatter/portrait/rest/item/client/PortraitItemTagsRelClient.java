package scatter.portrait.rest.item.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * item和标签关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
@FeignClient(value = "PortraitItemTagsRel-client")
public interface PortraitItemTagsRelClient {

}
