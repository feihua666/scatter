package scatter.portrait.rest.user.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 画像用户表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
@FeignClient(value = "PortraitUser-client")
public interface PortraitUserClient {

}
