package scatter.portrait.rest.user.client;

import org.springframework.stereotype.Component;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * <p>
 * 用户和特征关系表 服务客户端
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Component
@FeignClient(value = "PortraitUserFeaturesRel-client")
public interface PortraitUserFeaturesRelClient {

}
