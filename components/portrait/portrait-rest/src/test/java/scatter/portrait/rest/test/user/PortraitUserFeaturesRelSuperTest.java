package scatter.portrait.rest.test.user;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.user.po.PortraitUserFeaturesRel;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户和特征 测试类基类
* </p>
*
* @author yw
* @since 2021-10-14
*/
public class PortraitUserFeaturesRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitUserFeaturesRelService portraitUserFeaturesRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitUserFeaturesRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitUserFeaturesRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitUserFeaturesRel mockPo() {
        return JMockData.mock(PortraitUserFeaturesRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitUserFeaturesRelAddForm mockAddForm() {
        return JMockData.mock(PortraitUserFeaturesRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitUserFeaturesRelUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitUserFeaturesRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitUserFeaturesRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitUserFeaturesRelPageQueryForm.class, mockConfig);
    }
}