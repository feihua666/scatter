package scatter.portrait.rest.test.item;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.portrait.pojo.item.form.PortraitItemTagsAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemTagsService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* Item标签字典 测试类基类
* </p>
*
* @author yw
* @since 2021-10-09
*/
public class PortraitItemTagsSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitItemTagsService portraitItemTagsService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitItemTagsService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitItemTagsService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitItemTags mockPo() {
        return JMockData.mock(PortraitItemTags.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitItemTagsAddForm mockAddForm() {
        return JMockData.mock(PortraitItemTagsAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitItemTagsUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitItemTagsUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitItemTagsPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitItemTagsPageQueryForm.class, mockConfig);
    }
}