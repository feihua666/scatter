package scatter.portrait.rest.test.user;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户特征字典 测试类基类
* </p>
*
* @author yw
* @since 2021-10-09
*/
public class PortraitUserFeaturesSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitUserFeaturesService portraitUserFeaturesService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitUserFeaturesService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitUserFeaturesService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitUserFeatures mockPo() {
        return JMockData.mock(PortraitUserFeatures.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitUserFeaturesAddForm mockAddForm() {
        return JMockData.mock(PortraitUserFeaturesAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitUserFeaturesUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitUserFeaturesUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitUserFeaturesPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitUserFeaturesPageQueryForm.class, mockConfig);
    }
}