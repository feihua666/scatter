package scatter.portrait.rest.test.user.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.user.po.PortraitUserTags;
import scatter.portrait.pojo.user.form.PortraitUserTagsAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsPageQueryForm;
import scatter.portrait.rest.test.user.PortraitUserTagsSuperTest;
import scatter.portrait.rest.user.service.IPortraitUserTagsService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户标签字典 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitUserTagsServiceTest extends PortraitUserTagsSuperTest{

    @Autowired
    private IPortraitUserTagsService portraitUserTagsService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitUserTags> pos = portraitUserTagsService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}