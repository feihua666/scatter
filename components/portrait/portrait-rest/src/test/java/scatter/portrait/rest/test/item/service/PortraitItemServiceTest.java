package scatter.portrait.rest.test.item.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.portrait.pojo.item.form.PortraitItemAddForm;
import scatter.portrait.pojo.item.form.PortraitItemUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemPageQueryForm;
import scatter.portrait.rest.test.item.PortraitItemSuperTest;
import scatter.portrait.rest.item.service.IPortraitItemService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* item 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitItemServiceTest extends PortraitItemSuperTest{

    @Autowired
    private IPortraitItemService portraitItemService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitItem> pos = portraitItemService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}