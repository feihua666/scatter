package scatter.portrait.rest.test.user;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserTagsRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 用户和标签 测试类基类
* </p>
*
* @author yw
* @since 2021-10-14
*/
public class PortraitUserTagsRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitUserTagsRelService portraitUserTagsRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitUserTagsRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitUserTagsRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitUserTagsRel mockPo() {
        return JMockData.mock(PortraitUserTagsRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitUserTagsRelAddForm mockAddForm() {
        return JMockData.mock(PortraitUserTagsRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitUserTagsRelUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitUserTagsRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitUserTagsRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitUserTagsRelPageQueryForm.class, mockConfig);
    }
}