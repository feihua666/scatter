package scatter.portrait.rest.test.item;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemTagsRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* item和标签 测试类基类
* </p>
*
* @author yw
* @since 2021-10-09
*/
public class PortraitItemTagsRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitItemTagsRelService portraitItemTagsRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitItemTagsRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitItemTagsRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitItemTagsRel mockPo() {
        return JMockData.mock(PortraitItemTagsRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitItemTagsRelAddForm mockAddForm() {
        return JMockData.mock(PortraitItemTagsRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitItemTagsRelUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitItemTagsRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitItemTagsRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitItemTagsRelPageQueryForm.class, mockConfig);
    }
}