package scatter.portrait.rest.test.user.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.pojo.user.form.PortraitUserAddForm;
import scatter.portrait.pojo.user.form.PortraitUserUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserPageQueryForm;
import scatter.portrait.rest.test.user.PortraitUserSuperTest;
import scatter.portrait.rest.user.service.IPortraitUserService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 画像用户 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitUserServiceTest extends PortraitUserSuperTest{

    @Autowired
    private IPortraitUserService portraitUserService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitUser> pos = portraitUserService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}