package scatter.portrait.rest.test.user;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.pojo.user.form.PortraitUserAddForm;
import scatter.portrait.pojo.user.form.PortraitUserUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* 画像用户 测试类基类
* </p>
*
* @author yw
* @since 2021-10-09
*/
public class PortraitUserSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitUserService portraitUserService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitUserService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitUserService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitUser mockPo() {
        return JMockData.mock(PortraitUser.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitUserAddForm mockAddForm() {
        return JMockData.mock(PortraitUserAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitUserUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitUserUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitUserPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitUserPageQueryForm.class, mockConfig);
    }
}