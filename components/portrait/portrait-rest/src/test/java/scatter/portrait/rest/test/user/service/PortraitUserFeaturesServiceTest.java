package scatter.portrait.rest.test.user.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesPageQueryForm;
import scatter.portrait.rest.test.user.PortraitUserFeaturesSuperTest;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户特征字典 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitUserFeaturesServiceTest extends PortraitUserFeaturesSuperTest{

    @Autowired
    private IPortraitUserFeaturesService portraitUserFeaturesService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitUserFeatures> pos = portraitUserFeaturesService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}