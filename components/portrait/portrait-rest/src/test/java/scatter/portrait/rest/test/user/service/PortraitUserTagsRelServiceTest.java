package scatter.portrait.rest.test.user.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelPageQueryForm;
import scatter.portrait.rest.test.user.PortraitUserTagsRelSuperTest;
import scatter.portrait.rest.user.service.IPortraitUserTagsRelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户和标签 服务测试类
* </p>
*
* @author yw
* @since 2021-10-14
*/
@SpringBootTest
public class PortraitUserTagsRelServiceTest extends PortraitUserTagsRelSuperTest{

    @Autowired
    private IPortraitUserTagsRelService portraitUserTagsRelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitUserTagsRel> pos = portraitUserTagsRelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}