package scatter.portrait.rest.test.item;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.portrait.pojo.item.form.PortraitItemAddForm;
import scatter.portrait.pojo.item.form.PortraitItemUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* item 测试类基类
* </p>
*
* @author yw
* @since 2021-10-09
*/
public class PortraitItemSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitItemService portraitItemService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitItemService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitItemService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitItem mockPo() {
        return JMockData.mock(PortraitItem.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitItemAddForm mockAddForm() {
        return JMockData.mock(PortraitItemAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitItemUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitItemUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitItemPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitItemPageQueryForm.class, mockConfig);
    }
}