package scatter.portrait.rest.test.item.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.item.po.PortraitItemFeatures;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesPageQueryForm;
import scatter.portrait.rest.test.item.PortraitItemFeaturesSuperTest;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* Item特征字典 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitItemFeaturesServiceTest extends PortraitItemFeaturesSuperTest{

    @Autowired
    private IPortraitItemFeaturesService portraitItemFeaturesService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitItemFeatures> pos = portraitItemFeaturesService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}