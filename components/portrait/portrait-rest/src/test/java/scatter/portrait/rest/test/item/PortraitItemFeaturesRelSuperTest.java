package scatter.portrait.rest.test.item;

import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeAll;
import com.github.jsonzou.jmockdata.MockConfig;
import com.github.jsonzou.jmockdata.JMockData;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesRelService;
import java.util.ArrayList;
import java.util.List;
import scatter.common.pojo.po.BaseTreePo;
/**
* <p>
* item和特征 测试类基类
* </p>
*
* @author yw
* @since 2021-10-09
*/
public class PortraitItemFeaturesRelSuperTest {
    protected static MockConfig mockConfig = null;

    @Autowired
    private IPortraitItemFeaturesRelService portraitItemFeaturesRelService;

    @BeforeAll
    public static void mockConfig() {
        // 排除serialVersionUID字段

        mockConfig = new MockConfig();
        List<String> excludes = new ArrayList();
        excludes.add("serialVersionUID");
        excludes.add("id");
        Field[] fields = ReflectUtil.getFields(BaseTreePo.class);
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                excludes.add(field.getName());
            }
        }
        mockConfig.excludes(excludes.toArray(new String[0]));
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public boolean removeById(String id) {
        return portraitItemFeaturesRelService.removeById(id);
    }
    /**
     * 根据ids 批量删除删除
     * @param ids
     * @return
     */
    public boolean removeByIds(List<String> ids) {
        return portraitItemFeaturesRelService.removeByIds(ids);
    }
    /**
    * 模拟数据po
    * @return
    */
    public PortraitItemFeaturesRel mockPo() {
        return JMockData.mock(PortraitItemFeaturesRel.class, mockConfig);
    }

    /**
    * 模拟数据addForm
    * @return
    */
    public PortraitItemFeaturesRelAddForm mockAddForm() {
        return JMockData.mock(PortraitItemFeaturesRelAddForm.class, mockConfig);
    }

    /**
    * 模拟数据updateForm
    * @return
    */
    public PortraitItemFeaturesRelUpdateForm mockUpdateForm() {
        return JMockData.mock(PortraitItemFeaturesRelUpdateForm.class, mockConfig);
    }

    /**
    * 模拟数据pageQueryForm
    * @return
    */
    public PortraitItemFeaturesRelPageQueryForm mockPageQueryForm() {
        return JMockData.mock(PortraitItemFeaturesRelPageQueryForm.class, mockConfig);
    }
}