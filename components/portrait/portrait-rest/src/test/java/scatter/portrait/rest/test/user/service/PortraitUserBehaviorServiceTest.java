package scatter.portrait.rest.test.user.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorAddForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorPageQueryForm;
import scatter.portrait.rest.test.user.PortraitUserBehaviorSuperTest;
import scatter.portrait.rest.user.service.IPortraitUserBehaviorService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* 用户行为 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitUserBehaviorServiceTest extends PortraitUserBehaviorSuperTest{

    @Autowired
    private IPortraitUserBehaviorService portraitUserBehaviorService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitUserBehavior> pos = portraitUserBehaviorService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}