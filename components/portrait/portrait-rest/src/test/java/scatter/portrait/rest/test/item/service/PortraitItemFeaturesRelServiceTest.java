package scatter.portrait.rest.test.item.service;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelPageQueryForm;
import scatter.portrait.rest.test.item.PortraitItemFeaturesRelSuperTest;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesRelService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
* <p>
* item和特征 服务测试类
* </p>
*
* @author yw
* @since 2021-10-09
*/
@SpringBootTest
public class PortraitItemFeaturesRelServiceTest extends PortraitItemFeaturesRelSuperTest{

    @Autowired
    private IPortraitItemFeaturesRelService portraitItemFeaturesRelService;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
    }


    /**
     * 测试数据是否初始化
     */
    @Test
    void initDbTest() {
        List<PortraitItemFeaturesRel> pos = portraitItemFeaturesRelService.list();
        Assertions.assertTrue( pos.size() >= 0);
    }
}