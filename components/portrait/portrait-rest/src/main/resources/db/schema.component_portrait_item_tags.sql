DROP TABLE IF EXISTS component_portrait_item_tags;
CREATE TABLE `component_portrait_item_tags` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `type_dict_id` varchar(20) NOT NULL COMMENT '类型，字典id',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `item_id` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Item标签字典表';
