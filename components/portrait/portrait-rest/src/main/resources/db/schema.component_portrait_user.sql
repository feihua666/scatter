DROP TABLE IF EXISTS component_portrait_user;
CREATE TABLE `component_portrait_user` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `user_id` varchar(20) DEFAULT NULL COMMENT '用户id，已注册用户必填',
  `regist_type_dict_id` varchar(20) DEFAULT NULL COMMENT '用户注册类型,如：app注册账号，手机号等，字典id',
  `imei` varchar(50) DEFAULT NULL COMMENT '设备串号，安卓:（imei），IOS:（idfa），未注册用户必填',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号',
  `gender_dict_id` varchar(20) DEFAULT NULL COMMENT '性别，字典id',
  `age` int DEFAULT NULL COMMENT '年龄',
  `age_group_dict_id` varchar(20) DEFAULT NULL COMMENT '年龄段，如：20-25，字典id',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `city` varchar(255) DEFAULT NULL COMMENT '城市名称',
  `device_model_dict_id` varchar(20) DEFAULT NULL COMMENT '设备型号，如：iphoneX，字典id',
  `source_from_dict_id` varchar(20) DEFAULT NULL COMMENT '用户来源，字典id',
  `description` varchar(255) DEFAULT NULL COMMENT '用户描述',
  `regist_at` datetime DEFAULT NULL COMMENT '注册时间',
  `last_login_at` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `last_login_ip` varchar(255) DEFAULT NULL COMMENT '最后一次登录ip',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='画像用户表';
