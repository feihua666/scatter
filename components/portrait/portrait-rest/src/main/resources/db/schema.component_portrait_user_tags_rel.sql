DROP TABLE IF EXISTS component_portrait_user_tags_rel;
CREATE TABLE `component_portrait_user_tags_rel` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `user_id` varchar(20) DEFAULT NULL COMMENT '用户id，外键',
  `user_tags_id` varchar(20) NOT NULL COMMENT '用户标签id，外键',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `source_dict_id` varchar(20) NOT NULL COMMENT '来源标识，用来标识该关系来源',
  `version` int NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_id` (`user_id`,`user_tags_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和标签关系表';
