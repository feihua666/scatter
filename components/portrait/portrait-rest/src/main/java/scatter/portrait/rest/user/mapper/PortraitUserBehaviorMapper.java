package scatter.portrait.rest.user.mapper;

import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户行为表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitUserBehaviorMapper extends IBaseMapper<PortraitUserBehavior> {

}
