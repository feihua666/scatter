package scatter.portrait.rest.item.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.portrait.pojo.item.vo.PortraitItemTagsVo;
import scatter.portrait.pojo.item.form.PortraitItemTagsAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemTagsService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * Item标签字典表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "Item标签字典相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/item/portrait-item-tags")
public class PortraitItemTagsController extends BaseAddUpdateQueryFormController<PortraitItemTags, PortraitItemTagsVo, PortraitItemTagsAddForm, PortraitItemTagsUpdateForm, PortraitItemTagsPageQueryForm> {
    @Autowired
    private IPortraitItemTagsService iPortraitItemTagsService;

    @ApiOperation("添加Item标签字典")
    @PreAuthorize("hasAuthority('PortraitItemTags:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemTagsVo add(@RequestBody @Valid PortraitItemTagsAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询Item标签字典")
    @PreAuthorize("hasAuthority('PortraitItemTags:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitItemTagsVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除Item标签字典")
    @PreAuthorize("hasAuthority('PortraitItemTags:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新Item标签字典")
    @PreAuthorize("hasAuthority('PortraitItemTags:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemTagsVo update(@RequestBody @Valid PortraitItemTagsUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询Item标签字典")
    @PreAuthorize("hasAuthority('PortraitItemTags:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitItemTagsVo> getList(PortraitItemTagsPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询Item标签字典")
    @PreAuthorize("hasAuthority('PortraitItemTags:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitItemTagsVo> getPage(PortraitItemTagsPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
