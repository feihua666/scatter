package scatter.portrait.rest.item.mapper;

import scatter.portrait.pojo.item.po.PortraitItemFeatures;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * Item特征字典表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitItemFeaturesMapper extends IBaseMapper<PortraitItemFeatures> {

}
