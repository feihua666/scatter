package scatter.portrait.rest.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import scatter.common.LoginUser;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorTrackingForm;
import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorAddForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorPageQueryForm;

import javax.validation.Valid;
import java.util.List;
/**
 * <p>
 * 用户行为表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitUserBehaviorService extends IBaseService<PortraitUserBehavior> {

	/**
	 * 添加埋点数据
	 * @param addForm
	 * @return
	 */
	Boolean tracking(PortraitUserBehaviorTrackingForm addForm, LoginUser loginUser);
}
