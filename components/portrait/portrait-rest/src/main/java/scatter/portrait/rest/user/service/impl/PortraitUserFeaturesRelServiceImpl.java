package scatter.portrait.rest.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import scatter.portrait.pojo.user.po.PortraitUserFeaturesRel;
import scatter.portrait.rest.user.mapper.PortraitUserFeaturesRelMapper;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 用户和特征关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Service
@Transactional
public class PortraitUserFeaturesRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitUserFeaturesRelMapper, PortraitUserFeaturesRel, PortraitUserFeaturesRelAddForm, PortraitUserFeaturesRelUpdateForm, PortraitUserFeaturesRelPageQueryForm> implements IPortraitUserFeaturesRelService {
    @Override
    public void preAdd(PortraitUserFeaturesRelAddForm addForm,PortraitUserFeaturesRel po) {
        assertByWrapper(Wrappers.<PortraitUserFeaturesRel>lambdaQuery()
                .eq(PortraitUserFeaturesRel::getUserId,addForm.getUserId())
                .eq(PortraitUserFeaturesRel::getUserFeaturesId,addForm.getUserFeaturesId()),
                false,"该特征已设置，请不要重复设置");
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PortraitUserFeaturesRelUpdateForm updateForm,PortraitUserFeaturesRel po) {

        assertByWrapper(Wrappers.<PortraitUserFeaturesRel>lambdaQuery()
                        .eq(PortraitUserFeaturesRel::getUserId,updateForm.getUserId())
                        .eq(PortraitUserFeaturesRel::getUserFeaturesId,updateForm.getUserFeaturesId()).ne(PortraitUserFeaturesRel::getId,updateForm.getId()),
                false,"该特征已设置，请不要重复设置");
        super.preUpdate(updateForm,po);

    }
}
