package scatter.portrait.rest.item.mapper;

import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * item和标签关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitItemTagsRelMapper extends IBaseMapper<PortraitItemTagsRel> {

}
