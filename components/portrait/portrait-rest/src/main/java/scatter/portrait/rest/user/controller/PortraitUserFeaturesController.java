package scatter.portrait.rest.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.portrait.pojo.user.vo.PortraitUserFeaturesVo;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 用户特征字典表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "用户特征字典相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/user/portrait-user-features")
public class PortraitUserFeaturesController extends BaseAddUpdateQueryFormController<PortraitUserFeatures, PortraitUserFeaturesVo, PortraitUserFeaturesAddForm, PortraitUserFeaturesUpdateForm, PortraitUserFeaturesPageQueryForm> {
    @Autowired
    private IPortraitUserFeaturesService iPortraitUserFeaturesService;

    @ApiOperation("添加用户特征字典")
    @PreAuthorize("hasAuthority('PortraitUserFeatures:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserFeaturesVo add(@RequestBody @Valid PortraitUserFeaturesAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询用户特征字典")
    @PreAuthorize("hasAuthority('PortraitUserFeatures:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitUserFeaturesVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除用户特征字典")
    @PreAuthorize("hasAuthority('PortraitUserFeatures:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新用户特征字典")
    @PreAuthorize("hasAuthority('PortraitUserFeatures:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserFeaturesVo update(@RequestBody @Valid PortraitUserFeaturesUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询用户特征字典")
    @PreAuthorize("hasAuthority('PortraitUserFeatures:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitUserFeaturesVo> getList(PortraitUserFeaturesPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询用户特征字典")
    @PreAuthorize("hasAuthority('PortraitUserFeatures:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitUserFeaturesVo> getPage(PortraitUserFeaturesPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
