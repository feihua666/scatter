package scatter.portrait.rest.user.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.user.po.PortraitUserFeaturesRel;
import scatter.portrait.pojo.user.vo.PortraitUserFeaturesRelVo;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelPageQueryForm;

/**
 * <p>
 * 用户和特征 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitUserFeaturesRelMapStruct extends IBaseVoMapStruct<PortraitUserFeaturesRel, PortraitUserFeaturesRelVo>,
                                  IBaseAddFormMapStruct<PortraitUserFeaturesRel,PortraitUserFeaturesRelAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitUserFeaturesRel,PortraitUserFeaturesRelUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitUserFeaturesRel,PortraitUserFeaturesRelPageQueryForm>{
    PortraitUserFeaturesRelMapStruct INSTANCE = Mappers.getMapper( PortraitUserFeaturesRelMapStruct.class );

}
