package scatter.portrait.rest.user.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.portrait.pojo.user.vo.PortraitUserFeaturesVo;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesPageQueryForm;

/**
 * <p>
 * 用户特征字典 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitUserFeaturesMapStruct extends IBaseVoMapStruct<PortraitUserFeatures, PortraitUserFeaturesVo>,
                                  IBaseAddFormMapStruct<PortraitUserFeatures,PortraitUserFeaturesAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitUserFeatures,PortraitUserFeaturesUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitUserFeatures,PortraitUserFeaturesPageQueryForm>{
    PortraitUserFeaturesMapStruct INSTANCE = Mappers.getMapper( PortraitUserFeaturesMapStruct.class );

}
