package scatter.portrait.rest.user.mapper;

import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 画像用户表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitUserMapper extends IBaseMapper<PortraitUser> {

}
