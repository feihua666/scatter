package scatter.portrait.rest.item.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.validation.DictService;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.portrait.pojo.item.vo.PortraitItemFeaturesRelVo;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelPageQueryForm;
import scatter.portrait.pojo.item.form.ItemAssignItemFeaturesForm;
import scatter.portrait.pojo.item.form.ItemFeaturesAssignItemForm;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import java.util.stream.Collectors;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesRelService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * item和特征关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "item和特征相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/item/portrait-item-features-rel")
public class PortraitItemFeaturesRelController extends BaseAddUpdateQueryFormController<PortraitItemFeaturesRel, PortraitItemFeaturesRelVo, PortraitItemFeaturesRelAddForm, PortraitItemFeaturesRelUpdateForm, PortraitItemFeaturesRelPageQueryForm> {
    @Autowired
    private IPortraitItemFeaturesRelService iPortraitItemFeaturesRelService;

    @Autowired
    private DictService dictService;

    @ApiOperation("添加item和特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemFeaturesRelVo add(@RequestBody @Valid PortraitItemFeaturesRelAddForm addForm) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        addForm.setSourceDictId(idByGroupCodeAndValue);
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询item和特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitItemFeaturesRelVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除item和特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新item和特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemFeaturesRelVo update(@RequestBody @Valid PortraitItemFeaturesRelUpdateForm updateForm) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        updateForm.setSourceDictId(idByGroupCodeAndValue);
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询item和特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitItemFeaturesRelVo> getList(PortraitItemFeaturesRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询item和特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitItemFeaturesRelVo> getPage(PortraitItemFeaturesRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("item分配特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:single:itemAssignItemFeatures')")
    @PostMapping("/item/assign/itemfeatures")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean itemAssignItemFeatures(@RequestBody @Valid ItemAssignItemFeaturesForm cf) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        return iBaseService.removeAndAssignRel(cf.getItemId(),
                cf.getCheckedItemFeaturesIds(),
                cf.getUncheckedItemFeaturesIds(),
                cf.getIsLazyLoad(),
                PortraitItemFeaturesRel::getItemId,
                PortraitItemFeaturesRel::getItemFeaturesId,
                (relDto)->new PortraitItemFeaturesRel().setItemId(relDto.getMainId()).setItemFeaturesId(relDto.getOtherId()).setSourceDictId(idByGroupCodeAndValue));
    }

    @ApiOperation("根据itemID查询已分配的特征id")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:single:queryByItemId')")
    @GetMapping("/item/{itemId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByItemId(@PathVariable String itemId) {
        List<PortraitItemFeaturesRel> rels = iPortraitItemFeaturesRelService.getByItemId(itemId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }

        return rels.stream().map(item -> item.getItemFeaturesId()).collect(Collectors.toList());
    }

    @ApiOperation("清空item下的所有特征")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:single:deleteByItemId')")
    @DeleteMapping("/item/{itemId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByItemId(@PathVariable String itemId) {
        return iBaseService.removeAssignRel(itemId,PortraitItemFeaturesRel::getItemId);
    }


    @ApiOperation("特征分配item")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:single:itemFeaturesAssignItem')")
    @PostMapping("/itemfeatures/assign/item")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean itemFeaturesAssignItem(@RequestBody @Valid ItemFeaturesAssignItemForm cf) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        return iPortraitItemFeaturesRelService.removeAndAssignRel(cf.getItemFeaturesId(),
                cf.getCheckedItemIds(),
                cf.getUncheckedItemIds(),
                cf.getIsLazyLoad(),
                PortraitItemFeaturesRel::getItemFeaturesId,
                PortraitItemFeaturesRel::getItemId,
                (relDto)->new PortraitItemFeaturesRel().setItemFeaturesId(relDto.getMainId()).setItemId(relDto.getOtherId()).setSourceDictId(idByGroupCodeAndValue));
    }

    @ApiOperation("根据特征ID查询已分配的itemid")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:single:queryByItemFeaturesId')")
    @GetMapping("/itemfeatures/{itemFeaturesId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByItemFeaturesId(@PathVariable String itemFeaturesId) {
        List<PortraitItemFeaturesRel> rels = iPortraitItemFeaturesRelService.getByItemFeaturesId(itemFeaturesId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }
        return rels.stream().map(item -> item.getItemFeaturesId()).collect(Collectors.toList());
    }

    @ApiOperation("清空特征下的所有item")
    @PreAuthorize("hasAuthority('PortraitItemFeaturesRel:single:deleteByItemFeaturesId')")
    @DeleteMapping("/itemfeatures/{itemFeaturesId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByItemFeaturesId(@PathVariable String itemFeaturesId) {
        return iPortraitItemFeaturesRelService.removeAssignRel(itemFeaturesId,PortraitItemFeaturesRel::getItemFeaturesId);
    }
}
