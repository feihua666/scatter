package scatter.portrait.rest.item.mapper;

import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * item和特征关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitItemFeaturesRelMapper extends IBaseMapper<PortraitItemFeaturesRel> {

}
