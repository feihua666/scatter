package scatter.portrait.rest.user.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorTrackingForm;
import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.portrait.pojo.user.vo.PortraitUserBehaviorVo;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorAddForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorPageQueryForm;

/**
 * <p>
 * 用户行为 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitUserBehaviorMapStruct extends IBaseVoMapStruct<PortraitUserBehavior, PortraitUserBehaviorVo>,
                                  IBaseAddFormMapStruct<PortraitUserBehavior,PortraitUserBehaviorAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitUserBehavior,PortraitUserBehaviorUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitUserBehavior,PortraitUserBehaviorPageQueryForm>{
    PortraitUserBehaviorMapStruct INSTANCE = Mappers.getMapper( PortraitUserBehaviorMapStruct.class );


    PortraitUserBehavior map(PortraitUserBehaviorTrackingForm trackingForm);

}
