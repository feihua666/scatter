package scatter.portrait.rest.user.service.impl;

import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.portrait.rest.user.mapper.PortraitUserFeaturesMapper;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 用户特征字典表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitUserFeaturesServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitUserFeaturesMapper, PortraitUserFeatures, PortraitUserFeaturesAddForm, PortraitUserFeaturesUpdateForm, PortraitUserFeaturesPageQueryForm> implements IPortraitUserFeaturesService {
    @Override
    public void preAdd(PortraitUserFeaturesAddForm addForm,PortraitUserFeatures po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),PortraitUserFeatures::getCode,false);
        }

    }

    @Override
    public void preUpdate(PortraitUserFeaturesUpdateForm updateForm,PortraitUserFeatures po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            PortraitUserFeatures byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),PortraitUserFeatures::getCode,false);
            }
        }

    }
}
