package scatter.portrait.rest.user.service.impl;

import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.rest.user.mapper.PortraitUserMapper;
import scatter.portrait.rest.user.service.IPortraitUserService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.user.form.PortraitUserAddForm;
import scatter.portrait.pojo.user.form.PortraitUserUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 画像用户表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitUserServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitUserMapper, PortraitUser, PortraitUserAddForm, PortraitUserUpdateForm, PortraitUserPageQueryForm> implements IPortraitUserService {
    @Override
    public void preAdd(PortraitUserAddForm addForm,PortraitUser po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getUserId())) {
            // 用户id已存在不能添加
            assertByColumn(addForm.getUserId(),PortraitUser::getUserId,false);
        }

    }

    @Override
    public void preUpdate(PortraitUserUpdateForm updateForm,PortraitUser po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getUserId())) {
            PortraitUser byId = getById(updateForm.getId());
            // 如果用户id有改动
            if (!isEqual(updateForm.getUserId(), byId.getUserId())) {
                // 用户id已存在不能修改
                assertByColumn(updateForm.getUserId(),PortraitUser::getUserId,false);
            }
        }

    }
}
