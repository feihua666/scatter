package scatter.portrait.rest.item.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.portrait.pojo.item.vo.PortraitItemTagsVo;
import scatter.portrait.pojo.item.form.PortraitItemTagsAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsPageQueryForm;

/**
 * <p>
 * Item标签字典 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitItemTagsMapStruct extends IBaseVoMapStruct<PortraitItemTags, PortraitItemTagsVo>,
                                  IBaseAddFormMapStruct<PortraitItemTags,PortraitItemTagsAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitItemTags,PortraitItemTagsUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitItemTags,PortraitItemTagsPageQueryForm>{
    PortraitItemTagsMapStruct INSTANCE = Mappers.getMapper( PortraitItemTagsMapStruct.class );

}
