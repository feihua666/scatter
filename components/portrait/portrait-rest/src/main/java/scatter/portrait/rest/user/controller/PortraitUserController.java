package scatter.portrait.rest.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.pojo.user.vo.PortraitUserVo;
import scatter.portrait.pojo.user.form.PortraitUserAddForm;
import scatter.portrait.pojo.user.form.PortraitUserUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 画像用户表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "画像用户相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/user/portrait-user")
public class PortraitUserController extends BaseAddUpdateQueryFormController<PortraitUser, PortraitUserVo, PortraitUserAddForm, PortraitUserUpdateForm, PortraitUserPageQueryForm> {
    @Autowired
    private IPortraitUserService iPortraitUserService;

    @ApiOperation("添加画像用户")
    @PreAuthorize("hasAuthority('PortraitUser:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserVo add(@RequestBody @Valid PortraitUserAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询画像用户")
    @PreAuthorize("hasAuthority('PortraitUser:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitUserVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除画像用户")
    @PreAuthorize("hasAuthority('PortraitUser:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新画像用户")
    @PreAuthorize("hasAuthority('PortraitUser:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserVo update(@RequestBody @Valid PortraitUserUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询画像用户")
    @PreAuthorize("hasAuthority('PortraitUser:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitUserVo> getList(PortraitUserPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询画像用户")
    @PreAuthorize("hasAuthority('PortraitUser:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitUserVo> getPage(PortraitUserPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
