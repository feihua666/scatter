package scatter.portrait.rest.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.user.form.PortraitUserAddForm;
import scatter.portrait.pojo.user.form.PortraitUserUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserPageQueryForm;
import java.util.List;
/**
 * <p>
 * 画像用户表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitUserService extends IBaseService<PortraitUser> {


    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default PortraitUser getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return getOne(Wrappers.<PortraitUser>lambdaQuery().eq(PortraitUser::getUserId, userId));
    }

}
