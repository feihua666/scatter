package scatter.portrait.rest.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelPageQueryForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelUpdateForm;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.portrait.rest.user.mapper.PortraitUserTagsRelMapper;
import scatter.portrait.rest.user.service.IPortraitUserTagsRelService;
/**
 * <p>
 * 用户和标签关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Service
@Transactional
public class PortraitUserTagsRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitUserTagsRelMapper, PortraitUserTagsRel, PortraitUserTagsRelAddForm, PortraitUserTagsRelUpdateForm, PortraitUserTagsRelPageQueryForm> implements IPortraitUserTagsRelService {
    @Override
    public void preAdd(PortraitUserTagsRelAddForm addForm,PortraitUserTagsRel po) {
        assertByWrapper(Wrappers.<PortraitUserTagsRel>lambdaQuery()
                        .eq(PortraitUserTagsRel::getUserId,addForm.getUserId())
                        .eq(PortraitUserTagsRel::getUserTagsId,addForm.getUserTagsId()),
                false,"该标签已设置，请不要重复设置");
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PortraitUserTagsRelUpdateForm updateForm,PortraitUserTagsRel po) {
        assertByWrapper(Wrappers.<PortraitUserTagsRel>lambdaQuery()
                        .eq(PortraitUserTagsRel::getUserId,updateForm.getUserId())
                        .eq(PortraitUserTagsRel::getUserTagsId,updateForm.getUserTagsId()).ne(PortraitUserTagsRel::getId,updateForm.getId()),
                false,"该标签已设置，请不要重复设置");
        super.preUpdate(updateForm,po);

    }
}
