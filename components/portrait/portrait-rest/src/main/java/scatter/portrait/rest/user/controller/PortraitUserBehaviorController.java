package scatter.portrait.rest.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.LoginUser;
import scatter.common.rest.tools.RequestTool;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorTrackingForm;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.portrait.pojo.user.vo.PortraitUserBehaviorVo;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorAddForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserBehaviorService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
/**
 * <p>
 * 用户行为表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "用户行为相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/user/portrait-user-behavior")
public class PortraitUserBehaviorController extends BaseAddUpdateQueryFormController<PortraitUserBehavior, PortraitUserBehaviorVo, PortraitUserBehaviorAddForm, PortraitUserBehaviorUpdateForm, PortraitUserBehaviorPageQueryForm> {
    @Autowired
    private IPortraitUserBehaviorService iPortraitUserBehaviorService;

    @ApiOperation("添加用户行为")
    @PreAuthorize("hasAuthority('PortraitUserBehavior:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserBehaviorVo add(@RequestBody @Valid PortraitUserBehaviorAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询用户行为")
    @PreAuthorize("hasAuthority('PortraitUserBehavior:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitUserBehaviorVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除用户行为")
    @PreAuthorize("hasAuthority('PortraitUserBehavior:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新用户行为")
    @PreAuthorize("hasAuthority('PortraitUserBehavior:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserBehaviorVo update(@RequestBody @Valid PortraitUserBehaviorUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询用户行为")
    @PreAuthorize("hasAuthority('PortraitUserBehavior:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitUserBehaviorVo> getList(PortraitUserBehaviorPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询用户行为")
    @PreAuthorize("hasAuthority('PortraitUserBehavior:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitUserBehaviorVo> getPage(PortraitUserBehaviorPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("添加埋点数据")
    @PreAuthorize("hasAuthority('user')")
    @PostMapping("/tracking")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean tracking(@RequestBody @Valid PortraitUserBehaviorTrackingForm addForm, LoginUser loginUser, HttpServletRequest request) {
        addForm.setClientIp(RequestTool.getRemoteAddr(request));
        return iPortraitUserBehaviorService.tracking(addForm,loginUser);
    }
}
