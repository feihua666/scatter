package scatter.portrait.rest.item.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.portrait.rest.item.mapper.PortraitItemTagsRelMapper;
import scatter.portrait.rest.item.service.IPortraitItemTagsRelService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * item和标签关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitItemTagsRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitItemTagsRelMapper, PortraitItemTagsRel, PortraitItemTagsRelAddForm, PortraitItemTagsRelUpdateForm, PortraitItemTagsRelPageQueryForm> implements IPortraitItemTagsRelService {
    @Override
    public void preAdd(PortraitItemTagsRelAddForm addForm,PortraitItemTagsRel po) {
        assertByWrapper(Wrappers.<PortraitItemTagsRel>lambdaQuery()
                        .eq(PortraitItemTagsRel::getItemId,addForm.getItemId())
                        .eq(PortraitItemTagsRel::getItemTagsId,addForm.getItemTagsId()),
                false,"该标签已设置，请不要重复设置");
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PortraitItemTagsRelUpdateForm updateForm,PortraitItemTagsRel po) {
        assertByWrapper(Wrappers.<PortraitItemTagsRel>lambdaQuery()
                        .eq(PortraitItemTagsRel::getItemId,updateForm.getItemId())
                        .eq(PortraitItemTagsRel::getItemTagsId,updateForm.getItemTagsId()).ne(PortraitItemTagsRel::getId,updateForm.getId()),
                false,"该标签已设置，请不要重复设置");
        super.preUpdate(updateForm,po);

    }
}
