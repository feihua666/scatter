package scatter.portrait.rest.item.service.impl;

import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.portrait.rest.item.mapper.PortraitItemMapper;
import scatter.portrait.rest.item.service.IPortraitItemService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.item.form.PortraitItemAddForm;
import scatter.portrait.pojo.item.form.PortraitItemUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * item表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitItemServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitItemMapper, PortraitItem, PortraitItemAddForm, PortraitItemUpdateForm, PortraitItemPageQueryForm> implements IPortraitItemService {
    @Override
    public void preAdd(PortraitItemAddForm addForm,PortraitItem po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getItemId())) {
            // Item_id已存在不能添加
            assertByColumn(addForm.getItemId(),PortraitItem::getItemId,false);
        }

    }

    @Override
    public void preUpdate(PortraitItemUpdateForm updateForm,PortraitItem po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getItemId())) {
            PortraitItem byId = getById(updateForm.getId());
            // 如果Item_id有改动
            if (!isEqual(updateForm.getItemId(), byId.getItemId())) {
                // Item_id已存在不能修改
                assertByColumn(updateForm.getItemId(),PortraitItem::getItemId,false);
            }
        }

    }
}
