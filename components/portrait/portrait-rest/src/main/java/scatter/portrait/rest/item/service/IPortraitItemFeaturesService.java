package scatter.portrait.rest.item.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.item.po.PortraitItemFeatures;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesPageQueryForm;
import java.util.List;
/**
 * <p>
 * Item特征字典表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitItemFeaturesService extends IBaseService<PortraitItemFeatures> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default PortraitItemFeatures getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<PortraitItemFeatures>lambdaQuery().eq(PortraitItemFeatures::getCode, code));
    }

}
