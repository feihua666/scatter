package scatter.portrait.rest.item.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.portrait.pojo.item.vo.PortraitItemTagsRelVo;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelPageQueryForm;

/**
 * <p>
 * item和标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitItemTagsRelMapStruct extends IBaseVoMapStruct<PortraitItemTagsRel, PortraitItemTagsRelVo>,
                                  IBaseAddFormMapStruct<PortraitItemTagsRel,PortraitItemTagsRelAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitItemTagsRel,PortraitItemTagsRelUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitItemTagsRel,PortraitItemTagsRelPageQueryForm>{
    PortraitItemTagsRelMapStruct INSTANCE = Mappers.getMapper( PortraitItemTagsRelMapStruct.class );

}
