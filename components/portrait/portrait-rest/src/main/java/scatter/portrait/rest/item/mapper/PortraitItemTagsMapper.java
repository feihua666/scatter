package scatter.portrait.rest.item.mapper;

import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * Item标签字典表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitItemTagsMapper extends IBaseMapper<PortraitItemTags> {

}
