package scatter.portrait.rest.item.service.impl;

import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.portrait.rest.item.mapper.PortraitItemTagsMapper;
import scatter.portrait.rest.item.service.IPortraitItemTagsService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.item.form.PortraitItemTagsAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * Item标签字典表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitItemTagsServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitItemTagsMapper, PortraitItemTags, PortraitItemTagsAddForm, PortraitItemTagsUpdateForm, PortraitItemTagsPageQueryForm> implements IPortraitItemTagsService {
    @Override
    public void preAdd(PortraitItemTagsAddForm addForm,PortraitItemTags po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),PortraitItemTags::getCode,false);
        }

    }

    @Override
    public void preUpdate(PortraitItemTagsUpdateForm updateForm,PortraitItemTags po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            PortraitItemTags byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),PortraitItemTags::getCode,false);
            }
        }

    }
}
