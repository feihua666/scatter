package scatter.portrait.rest.scheduler;

/**
 * <p>
 * 画像信息同步任务
 * </p>
 *
 * @author yangwei
 * @since 2021-09-29 13:21
 */
public interface IPortraitSynchronizeJob {
	/**
	 * 全量同步
	 */
	public void synchronizeAll();

	/**
	 * 增量同步
	 */
	public void synchronizeIncrement();
}
