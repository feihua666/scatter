package scatter.portrait.rest.item.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.portrait.pojo.item.vo.PortraitItemVo;
import scatter.portrait.pojo.item.form.PortraitItemAddForm;
import scatter.portrait.pojo.item.form.PortraitItemUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * item表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "item相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/item/portrait-item")
public class PortraitItemController extends BaseAddUpdateQueryFormController<PortraitItem, PortraitItemVo, PortraitItemAddForm, PortraitItemUpdateForm, PortraitItemPageQueryForm> {
    @Autowired
    private IPortraitItemService iPortraitItemService;

    @ApiOperation("添加item")
    @PreAuthorize("hasAuthority('PortraitItem:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemVo add(@RequestBody @Valid PortraitItemAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询item")
    @PreAuthorize("hasAuthority('PortraitItem:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitItemVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除item")
    @PreAuthorize("hasAuthority('PortraitItem:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新item")
    @PreAuthorize("hasAuthority('PortraitItem:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemVo update(@RequestBody @Valid PortraitItemUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询item")
    @PreAuthorize("hasAuthority('PortraitItem:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitItemVo> getList(PortraitItemPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询item")
    @PreAuthorize("hasAuthority('PortraitItem:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitItemVo> getPage(PortraitItemPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
