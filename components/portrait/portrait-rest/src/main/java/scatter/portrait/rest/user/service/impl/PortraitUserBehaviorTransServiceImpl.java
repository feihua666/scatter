package scatter.portrait.rest.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.portrait.rest.user.service.IPortraitUserBehaviorService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户行为翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
public class PortraitUserBehaviorTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IPortraitUserBehaviorService portraitUserBehaviorService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,PortraitUserBehavior.TRANS_PORTRAITUSERBEHAVIOR_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,PortraitUserBehavior.TRANS_PORTRAITUSERBEHAVIOR_BY_ID)) {
            PortraitUserBehavior byId = portraitUserBehaviorService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,PortraitUserBehavior.TRANS_PORTRAITUSERBEHAVIOR_BY_ID)) {
            return portraitUserBehaviorService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
