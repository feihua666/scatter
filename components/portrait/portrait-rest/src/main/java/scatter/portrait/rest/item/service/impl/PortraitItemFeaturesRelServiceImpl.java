package scatter.portrait.rest.item.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelPageQueryForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelUpdateForm;
import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.portrait.rest.item.mapper.PortraitItemFeaturesRelMapper;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesRelService;
/**
 * <p>
 * item和特征关系表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitItemFeaturesRelServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitItemFeaturesRelMapper, PortraitItemFeaturesRel, PortraitItemFeaturesRelAddForm, PortraitItemFeaturesRelUpdateForm, PortraitItemFeaturesRelPageQueryForm> implements IPortraitItemFeaturesRelService {
    @Override
    public void preAdd(PortraitItemFeaturesRelAddForm addForm,PortraitItemFeaturesRel po) {
        assertByWrapper(Wrappers.<PortraitItemFeaturesRel>lambdaQuery()
                        .eq(PortraitItemFeaturesRel::getItemId,addForm.getItemId())
                        .eq(PortraitItemFeaturesRel::getItemFeaturesId,addForm.getItemFeaturesId()),
                false,"该特征已设置，请不要重复设置");
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PortraitItemFeaturesRelUpdateForm updateForm,PortraitItemFeaturesRel po) {
        assertByWrapper(Wrappers.<PortraitItemFeaturesRel>lambdaQuery()
                        .eq(PortraitItemFeaturesRel::getItemId,updateForm.getItemId())
                        .eq(PortraitItemFeaturesRel::getItemFeaturesId,updateForm.getItemFeaturesId()).ne(PortraitItemFeaturesRel::getId,updateForm.getId()),
                false,"该特征已设置，请不要重复设置");
        super.preUpdate(updateForm,po);

    }
}
