package scatter.portrait.rest.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.user.po.PortraitUserTags;
import scatter.portrait.pojo.user.vo.PortraitUserTagsVo;
import scatter.portrait.pojo.user.form.PortraitUserTagsAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsPageQueryForm;
import scatter.portrait.rest.user.service.IPortraitUserTagsService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * 用户标签字典表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "用户标签字典相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/user/portrait-user-tags")
public class PortraitUserTagsController extends BaseAddUpdateQueryFormController<PortraitUserTags, PortraitUserTagsVo, PortraitUserTagsAddForm, PortraitUserTagsUpdateForm, PortraitUserTagsPageQueryForm> {
    @Autowired
    private IPortraitUserTagsService iPortraitUserTagsService;

    @ApiOperation("添加用户标签字典")
    @PreAuthorize("hasAuthority('PortraitUserTags:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserTagsVo add(@RequestBody @Valid PortraitUserTagsAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询用户标签字典")
    @PreAuthorize("hasAuthority('PortraitUserTags:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitUserTagsVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除用户标签字典")
    @PreAuthorize("hasAuthority('PortraitUserTags:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新用户标签字典")
    @PreAuthorize("hasAuthority('PortraitUserTags:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitUserTagsVo update(@RequestBody @Valid PortraitUserTagsUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询用户标签字典")
    @PreAuthorize("hasAuthority('PortraitUserTags:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitUserTagsVo> getList(PortraitUserTagsPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询用户标签字典")
    @PreAuthorize("hasAuthority('PortraitUserTags:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitUserTagsVo> getPage(PortraitUserTagsPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
