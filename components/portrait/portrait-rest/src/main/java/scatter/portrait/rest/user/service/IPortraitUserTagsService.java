package scatter.portrait.rest.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.user.po.PortraitUserTags;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.user.form.PortraitUserTagsAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsPageQueryForm;
import java.util.List;
/**
 * <p>
 * 用户标签字典表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitUserTagsService extends IBaseService<PortraitUserTags> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default PortraitUserTags getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<PortraitUserTags>lambdaQuery().eq(PortraitUserTags::getCode, code));
    }

}
