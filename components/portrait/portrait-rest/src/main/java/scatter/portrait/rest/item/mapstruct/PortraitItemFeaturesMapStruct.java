package scatter.portrait.rest.item.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.item.po.PortraitItemFeatures;
import scatter.portrait.pojo.item.vo.PortraitItemFeaturesVo;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesPageQueryForm;

/**
 * <p>
 * Item特征字典 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitItemFeaturesMapStruct extends IBaseVoMapStruct<PortraitItemFeatures, PortraitItemFeaturesVo>,
                                  IBaseAddFormMapStruct<PortraitItemFeatures,PortraitItemFeaturesAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitItemFeatures,PortraitItemFeaturesUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitItemFeatures,PortraitItemFeaturesPageQueryForm>{
    PortraitItemFeaturesMapStruct INSTANCE = Mappers.getMapper( PortraitItemFeaturesMapStruct.class );

}
