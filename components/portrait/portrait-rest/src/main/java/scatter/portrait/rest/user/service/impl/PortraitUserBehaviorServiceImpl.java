package scatter.portrait.rest.user.service.impl;

import cn.hutool.core.util.ReflectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.common.LoginUser;
import scatter.common.rest.validation.DictService;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorTrackingForm;
import scatter.portrait.pojo.user.po.PortraitUserBehavior;
import scatter.portrait.rest.user.mapper.PortraitUserBehaviorMapper;
import scatter.portrait.rest.user.mapstruct.PortraitUserBehaviorMapStruct;
import scatter.portrait.rest.user.service.IPortraitUserBehaviorService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorAddForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserBehaviorPageQueryForm;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.Optional;

/**
 * <p>
 * 用户行为表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitUserBehaviorServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitUserBehaviorMapper, PortraitUserBehavior, PortraitUserBehaviorAddForm, PortraitUserBehaviorUpdateForm, PortraitUserBehaviorPageQueryForm> implements IPortraitUserBehaviorService {

    @Autowired
    private DictService dictService;

    @Override
    public void preAdd(PortraitUserBehaviorAddForm addForm,PortraitUserBehavior po) {
        super.preAdd(addForm,po);

    }

    @Override
    public void preUpdate(PortraitUserBehaviorUpdateForm updateForm,PortraitUserBehavior po) {
        super.preUpdate(updateForm,po);

    }

    @Override
    public Boolean tracking(PortraitUserBehaviorTrackingForm addForm, LoginUser loginUser) {

        // 填充字典数据

        String dictId = null;
        if (!isStrEmpty(addForm.getItemTypeDictValue())) {
            dictId = dictService.getIdByGroupCodeWithValue(addForm.getItemTypeDictValue());
            addForm.setItemTypeDictId(Optional.ofNullable(dictId).orElse(addForm.getItemTypeDictId()));
        }

        if (!isStrEmpty(addForm.getItemPositionDictValue())) {
            dictId = dictService.getIdByGroupCodeWithValue(addForm.getItemPositionDictValue());
            addForm.setItemPositionDictId(Optional.ofNullable(dictId).orElse(addForm.getItemPositionDictId()));
        }

        if (!isStrEmpty(addForm.getBehaviorTypeDictValue())) {
            dictId = dictService.getIdByGroupCodeWithValue(addForm.getBehaviorTypeDictValue());
            addForm.setBehaviorTypeDictId(Optional.ofNullable(dictId).orElse(addForm.getBehaviorTypeDictId()));
        }

        if (!isStrEmpty(addForm.getClientPlatformDictValue())) {
            dictId = dictService.getIdByGroupCodeWithValue(addForm.getClientPlatformDictValue());
            addForm.setClientPlatformDictId(Optional.ofNullable(dictId).orElse(addForm.getClientPlatformDictId()));
        }

        if (!isStrEmpty(addForm.getNetTypeDictValue())) {
            dictId = dictService.getIdByGroupCodeWithValue(addForm.getNetTypeDictValue());
            addForm.setNetTypeDictId(Optional.ofNullable(dictId).orElse(addForm.getNetTypeDictId()));
        }

        if (!isStrEmpty(addForm.getDeviceModelDictValue())) {
            dictId = dictService.getIdByGroupCodeWithValue(addForm.getDeviceModelDictValue());
            addForm.setDeviceModelDictId(Optional.ofNullable(dictId).orElse(addForm.getDeviceModelDictId()));
        }

        PortraitUserBehavior behavior = PortraitUserBehaviorMapStruct.INSTANCE.map(addForm);
        String userId = Optional.ofNullable(behavior.getUserId()).orElse(Optional.ofNullable(loginUser).map(i->i.getId()).orElse(null));
        behavior.setUserId(userId);

        behavior.setIsLogin(loginUser != null);

        return save(behavior);

    }
}
