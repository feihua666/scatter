package scatter.portrait.rest.item.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.portrait.rest.item.service.IPortraitItemTagsRelService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * item和标签翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
public class PortraitItemTagsRelTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IPortraitItemTagsRelService portraitItemTagsRelService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,PortraitItemTagsRel.TRANS_PORTRAITITEMTAGSREL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,PortraitItemTagsRel.TRANS_PORTRAITITEMTAGSREL_BY_ID)) {
            PortraitItemTagsRel byId = portraitItemTagsRelService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,PortraitItemTagsRel.TRANS_PORTRAITITEMTAGSREL_BY_ID)) {
            return portraitItemTagsRelService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
