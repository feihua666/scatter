package scatter.portrait.rest.item.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.validation.DictService;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.portrait.pojo.item.vo.PortraitItemTagsRelVo;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelPageQueryForm;
import scatter.portrait.pojo.item.form.ItemAssignItemTagsForm;
import scatter.portrait.pojo.item.form.ItemTagsAssignItemForm;
import scatter.common.rest.exception.BusinessDataNotFoundException;
import java.util.stream.Collectors;
import scatter.portrait.rest.item.service.IPortraitItemTagsRelService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * item和标签关系表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "item和标签相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/item/portrait-item-tags-rel")
public class PortraitItemTagsRelController extends BaseAddUpdateQueryFormController<PortraitItemTagsRel, PortraitItemTagsRelVo, PortraitItemTagsRelAddForm, PortraitItemTagsRelUpdateForm, PortraitItemTagsRelPageQueryForm> {
    @Autowired
    private IPortraitItemTagsRelService iPortraitItemTagsRelService;

    @Autowired
    private DictService dictService;


    @ApiOperation("添加item和标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemTagsRelVo add(@RequestBody @Valid PortraitItemTagsRelAddForm addForm) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        addForm.setSourceDictId(idByGroupCodeAndValue);
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询item和标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitItemTagsRelVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除item和标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新item和标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemTagsRelVo update(@RequestBody @Valid PortraitItemTagsRelUpdateForm updateForm) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        updateForm.setSourceDictId(idByGroupCodeAndValue);
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询item和标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitItemTagsRelVo> getList(PortraitItemTagsRelPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询item和标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitItemTagsRelVo> getPage(PortraitItemTagsRelPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }

    @ApiOperation("item分配标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:single:itemAssignItemTags')")
    @PostMapping("/item/assign/itemtags")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean itemAssignItemTags(@RequestBody @Valid ItemAssignItemTagsForm cf) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        return iBaseService.removeAndAssignRel(cf.getItemId(),
                cf.getCheckedItemTagsIds(),
                cf.getUncheckedItemTagsIds(),
                cf.getIsLazyLoad(),
                PortraitItemTagsRel::getItemId,PortraitItemTagsRel::getItemTagsId,
                (relDto)->new PortraitItemTagsRel().setItemId(relDto.getMainId()).setItemTagsId(relDto.getOtherId()).setSourceDictId(idByGroupCodeAndValue));
    }

    @ApiOperation("根据itemID查询已分配的标签id")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:single:queryByItemId')")
    @GetMapping("/item/{itemId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByItemId(@PathVariable String itemId) {
        List<PortraitItemTagsRel> rels = iPortraitItemTagsRelService.getByItemId(itemId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }

        return rels.stream().map(item -> item.getItemTagsId()).collect(Collectors.toList());
    }

    @ApiOperation("清空item下的所有标签")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:single:deleteByItemId')")
    @DeleteMapping("/item/{itemId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByItemId(@PathVariable String itemId) {
        return iBaseService.removeAssignRel(itemId,PortraitItemTagsRel::getItemId);
    }


    @ApiOperation("标签分配item")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:single:itemTagsAssignItem')")
    @PostMapping("/itemtags/assign/item")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean itemTagsAssignItem(@RequestBody @Valid ItemTagsAssignItemForm cf) {
        // 这里是手动配置
        String idByGroupCodeAndValue = dictService.getIdByGroupCodeAndValue(PortraitUser.FeatureTagSourceGroupCode.feature_tag_source.groupCode(),
                PortraitUser.FeatureTagSourceDictItem.manual.itemValue());
        return iPortraitItemTagsRelService.removeAndAssignRel(cf.getItemTagsId(),
                cf.getCheckedItemIds(),
                cf.getUncheckedItemIds(),
                cf.getIsLazyLoad(),
                PortraitItemTagsRel::getItemTagsId,
                PortraitItemTagsRel::getItemId,
                (relDto)->new PortraitItemTagsRel().setItemTagsId(relDto.getMainId()).setItemId(relDto.getOtherId()).setSourceDictId(idByGroupCodeAndValue));
    }

    @ApiOperation("根据标签ID查询已分配的itemid")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:single:queryByItemTagsId')")
    @GetMapping("/itemtags/{itemTagsId}")
    @ResponseStatus(HttpStatus.OK)
    public List<String> queryByItemTagsId(@PathVariable String itemTagsId) {
        List<PortraitItemTagsRel> rels = iPortraitItemTagsRelService.getByItemTagsId(itemTagsId);
        if (isEmpty(rels)) {
            throw new BusinessDataNotFoundException("数据不存在");
        }
        return rels.stream().map(item -> item.getItemTagsId()).collect(Collectors.toList());
    }

    @ApiOperation("清空标签下的所有item")
    @PreAuthorize("hasAuthority('PortraitItemTagsRel:single:deleteByItemTagsId')")
    @DeleteMapping("/itemtags/{itemTagsId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteByItemTagsId(@PathVariable String itemTagsId) {
        return iPortraitItemTagsRelService.removeAssignRel(itemTagsId,PortraitItemTagsRel::getItemTagsId);
    }
}
