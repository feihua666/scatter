package scatter.portrait.rest.user.service.impl;

import scatter.portrait.pojo.user.po.PortraitUserTags;
import scatter.portrait.rest.user.mapper.PortraitUserTagsMapper;
import scatter.portrait.rest.user.service.IPortraitUserTagsService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.user.form.PortraitUserTagsAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * 用户标签字典表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitUserTagsServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitUserTagsMapper, PortraitUserTags, PortraitUserTagsAddForm, PortraitUserTagsUpdateForm, PortraitUserTagsPageQueryForm> implements IPortraitUserTagsService {
    @Override
    public void preAdd(PortraitUserTagsAddForm addForm,PortraitUserTags po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),PortraitUserTags::getCode,false);
        }

    }

    @Override
    public void preUpdate(PortraitUserTagsUpdateForm updateForm,PortraitUserTags po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            PortraitUserTags byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),PortraitUserTags::getCode,false);
            }
        }

    }
}
