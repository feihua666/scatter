package scatter.portrait.rest.user.scheduler.job;

import scatter.portrait.rest.scheduler.IPortraitSynchronizeJob;

/**
 * <p>
 * 画像用户同步任务
 * </p>
 *
 * @author yangwei
 * @since 2021-09-29 13:28
 */
public interface IPortraitUserSynchronizeJob extends IPortraitSynchronizeJob {
}
