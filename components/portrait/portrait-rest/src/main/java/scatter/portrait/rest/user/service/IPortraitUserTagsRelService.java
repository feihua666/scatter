package scatter.portrait.rest.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 用户和标签关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
public interface IPortraitUserTagsRelService extends IBaseService<PortraitUserTagsRel> {

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<PortraitUserTagsRel> getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return list(Wrappers.<PortraitUserTagsRel>lambdaQuery().eq(PortraitUserTagsRel::getUserId, userId));
    }
    /**
     * 根据用户标签id查询
     * @param userTagsId
     * @return
     */
    default List<PortraitUserTagsRel> getByUserTagsId(String userTagsId) {
        Assert.hasText(userTagsId,"userTagsId不能为空");
        return list(Wrappers.<PortraitUserTagsRel>lambdaQuery().eq(PortraitUserTagsRel::getUserTagsId, userTagsId));
    }
    /**
     * 根据租户id查询
     * @param tenantId
     * @return
     */
    default List<PortraitUserTagsRel> getByTenantId(String tenantId) {
        Assert.hasText(tenantId,"tenantId不能为空");
        return list(Wrappers.<PortraitUserTagsRel>lambdaQuery().eq(PortraitUserTagsRel::getTenantId, tenantId));
    }
    /**
     * 根据来源标识查询
     * @param sourceDictId
     * @return
     */
    default List<PortraitUserTagsRel> getBySourceDictId(String sourceDictId) {
        Assert.hasText(sourceDictId,"sourceDictId不能为空");
        return list(Wrappers.<PortraitUserTagsRel>lambdaQuery().eq(PortraitUserTagsRel::getSourceDictId, sourceDictId));
    }

}
