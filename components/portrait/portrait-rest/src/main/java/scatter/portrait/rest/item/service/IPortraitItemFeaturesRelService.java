package scatter.portrait.rest.item.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * item和特征关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitItemFeaturesRelService extends IBaseService<PortraitItemFeaturesRel> {

    /**
     * 根据item id查询
     * @param itemId
     * @return
     */
    default List<PortraitItemFeaturesRel> getByItemId(String itemId) {
        Assert.hasText(itemId,"itemId不能为空");
        return list(Wrappers.<PortraitItemFeaturesRel>lambdaQuery().eq(PortraitItemFeaturesRel::getItemId, itemId));
    }
    /**
     * 根据item特征id查询
     * @param itemFeaturesId
     * @return
     */
    default List<PortraitItemFeaturesRel> getByItemFeaturesId(String itemFeaturesId) {
        Assert.hasText(itemFeaturesId,"itemFeaturesId不能为空");
        return list(Wrappers.<PortraitItemFeaturesRel>lambdaQuery().eq(PortraitItemFeaturesRel::getItemFeaturesId, itemFeaturesId));
    }
    /**
     * 根据租户id查询
     * @param tenantId
     * @return
     */
    default List<PortraitItemFeaturesRel> getByTenantId(String tenantId) {
        Assert.hasText(tenantId,"tenantId不能为空");
        return list(Wrappers.<PortraitItemFeaturesRel>lambdaQuery().eq(PortraitItemFeaturesRel::getTenantId, tenantId));
    }
    /**
     * 根据来源标识查询
     * @param sourceDictId
     * @return
     */
    default List<PortraitItemFeaturesRel> getBySourceDictId(String sourceDictId) {
        Assert.hasText(sourceDictId,"sourceDictId不能为空");
        return list(Wrappers.<PortraitItemFeaturesRel>lambdaQuery().eq(PortraitItemFeaturesRel::getSourceDictId, sourceDictId));
    }

}
