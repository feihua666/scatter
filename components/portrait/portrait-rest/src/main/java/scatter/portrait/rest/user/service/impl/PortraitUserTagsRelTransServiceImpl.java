package scatter.portrait.rest.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.portrait.rest.user.service.IPortraitUserTagsRelService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户和标签翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Component
public class PortraitUserTagsRelTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IPortraitUserTagsRelService portraitUserTagsRelService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,PortraitUserTagsRel.TRANS_PORTRAITUSERTAGSREL_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,PortraitUserTagsRel.TRANS_PORTRAITUSERTAGSREL_BY_ID)) {
            PortraitUserTagsRel byId = portraitUserTagsRelService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,PortraitUserTagsRel.TRANS_PORTRAITUSERTAGSREL_BY_ID)) {
            return portraitUserTagsRelService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
