package scatter.portrait.rest.item.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import scatter.portrait.rest.PortraitConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;
import scatter.portrait.pojo.item.po.PortraitItemFeatures;
import scatter.portrait.pojo.item.vo.PortraitItemFeaturesVo;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesPageQueryForm;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
/**
 * <p>
 * Item特征字典表 前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Api(tags = "Item特征字典相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/item/portrait-item-features")
public class PortraitItemFeaturesController extends BaseAddUpdateQueryFormController<PortraitItemFeatures, PortraitItemFeaturesVo, PortraitItemFeaturesAddForm, PortraitItemFeaturesUpdateForm, PortraitItemFeaturesPageQueryForm> {
    @Autowired
    private IPortraitItemFeaturesService iPortraitItemFeaturesService;

    @ApiOperation("添加Item特征字典")
    @PreAuthorize("hasAuthority('PortraitItemFeatures:add')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemFeaturesVo add(@RequestBody @Valid PortraitItemFeaturesAddForm addForm) {
        return super.add(addForm);
    }
    @ApiOperation("根据ID查询Item特征字典")
    @PreAuthorize("hasAuthority('PortraitItemFeatures:queryById')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public PortraitItemFeaturesVo queryById(@PathVariable String id) {
        return super.queryById(id);
    }

    @ApiOperation("根据ID删除Item特征字典")
    @PreAuthorize("hasAuthority('PortraitItemFeatures:deleteById')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public boolean deleteById(@PathVariable String id) {
        return super.deleteById(id);
    }

    @ApiOperation("根据ID更新Item特征字典")
    @PreAuthorize("hasAuthority('PortraitItemFeatures:update')")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public PortraitItemFeaturesVo update(@RequestBody @Valid PortraitItemFeaturesUpdateForm updateForm) {
        return super.update(updateForm);
    }

    @ApiOperation("不分页查询Item特征字典")
    @PreAuthorize("hasAuthority('PortraitItemFeatures:getList')")
    @GetMapping("/getList")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public List<PortraitItemFeaturesVo> getList(PortraitItemFeaturesPageQueryForm listPageForm) {
         return super.getList(listPageForm);
    }

    @ApiOperation("分页查询Item特征字典")
    @PreAuthorize("hasAuthority('PortraitItemFeatures:getPage')")
    @GetMapping("/getPage")
    @ResponseStatus(HttpStatus.OK)
    @Override
    public IPage<PortraitItemFeaturesVo> getPage(PortraitItemFeaturesPageQueryForm listPageForm) {
         return super.getPage(listPageForm);
    }
}
