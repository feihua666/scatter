package scatter.portrait.rest.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesPageQueryForm;
import java.util.List;
/**
 * <p>
 * 用户特征字典表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitUserFeaturesService extends IBaseService<PortraitUserFeatures> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default PortraitUserFeatures getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<PortraitUserFeatures>lambdaQuery().eq(PortraitUserFeatures::getCode, code));
    }

}
