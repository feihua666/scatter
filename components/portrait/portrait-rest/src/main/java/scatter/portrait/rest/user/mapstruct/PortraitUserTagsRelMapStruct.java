package scatter.portrait.rest.user.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.portrait.pojo.user.vo.PortraitUserTagsRelVo;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsRelPageQueryForm;

/**
 * <p>
 * 用户和标签 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitUserTagsRelMapStruct extends IBaseVoMapStruct<PortraitUserTagsRel, PortraitUserTagsRelVo>,
                                  IBaseAddFormMapStruct<PortraitUserTagsRel,PortraitUserTagsRelAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitUserTagsRel,PortraitUserTagsRelUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitUserTagsRel,PortraitUserTagsRelPageQueryForm>{
    PortraitUserTagsRelMapStruct INSTANCE = Mappers.getMapper( PortraitUserTagsRelMapStruct.class );

}
