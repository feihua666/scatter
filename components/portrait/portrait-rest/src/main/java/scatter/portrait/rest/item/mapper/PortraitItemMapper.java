package scatter.portrait.rest.item.mapper;

import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * item表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface PortraitItemMapper extends IBaseMapper<PortraitItem> {

}
