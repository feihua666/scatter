package scatter.portrait.rest.user.mapper;

import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.common.rest.service.IBaseMapper;

/**
 * <p>
 * 用户和标签关系表 Mapper 接口
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
public interface PortraitUserTagsRelMapper extends IBaseMapper<PortraitUserTagsRel> {

}
