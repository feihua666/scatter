package scatter.portrait.rest.item.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.item.po.PortraitItemTagsRel;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * item和标签关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitItemTagsRelService extends IBaseService<PortraitItemTagsRel> {

    /**
     * 根据item id查询
     * @param itemId
     * @return
     */
    default List<PortraitItemTagsRel> getByItemId(String itemId) {
        Assert.hasText(itemId,"itemId不能为空");
        return list(Wrappers.<PortraitItemTagsRel>lambdaQuery().eq(PortraitItemTagsRel::getItemId, itemId));
    }
    /**
     * 根据item标签id查询
     * @param itemTagsId
     * @return
     */
    default List<PortraitItemTagsRel> getByItemTagsId(String itemTagsId) {
        Assert.hasText(itemTagsId,"itemTagsId不能为空");
        return list(Wrappers.<PortraitItemTagsRel>lambdaQuery().eq(PortraitItemTagsRel::getItemTagsId, itemTagsId));
    }
    /**
     * 根据租户id查询
     * @param tenantId
     * @return
     */
    default List<PortraitItemTagsRel> getByTenantId(String tenantId) {
        Assert.hasText(tenantId,"tenantId不能为空");
        return list(Wrappers.<PortraitItemTagsRel>lambdaQuery().eq(PortraitItemTagsRel::getTenantId, tenantId));
    }
    /**
     * 根据来源标识查询
     * @param sourceDictId
     * @return
     */
    default List<PortraitItemTagsRel> getBySourceDictId(String sourceDictId) {
        Assert.hasText(sourceDictId,"sourceDictId不能为空");
        return list(Wrappers.<PortraitItemTagsRel>lambdaQuery().eq(PortraitItemTagsRel::getSourceDictId, sourceDictId));
    }

}
