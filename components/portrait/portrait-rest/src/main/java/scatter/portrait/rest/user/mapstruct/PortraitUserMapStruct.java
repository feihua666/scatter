package scatter.portrait.rest.user.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.user.po.PortraitUser;
import scatter.portrait.pojo.user.vo.PortraitUserVo;
import scatter.portrait.pojo.user.form.PortraitUserAddForm;
import scatter.portrait.pojo.user.form.PortraitUserUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserPageQueryForm;

/**
 * <p>
 * 画像用户 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitUserMapStruct extends IBaseVoMapStruct<PortraitUser, PortraitUserVo>,
                                  IBaseAddFormMapStruct<PortraitUser,PortraitUserAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitUser,PortraitUserUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitUser,PortraitUserPageQueryForm>{
    PortraitUserMapStruct INSTANCE = Mappers.getMapper( PortraitUserMapStruct.class );

}
