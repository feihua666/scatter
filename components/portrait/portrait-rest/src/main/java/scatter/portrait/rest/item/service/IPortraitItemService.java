package scatter.portrait.rest.item.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.item.form.PortraitItemAddForm;
import scatter.portrait.pojo.item.form.PortraitItemUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemPageQueryForm;
import java.util.List;
/**
 * <p>
 * item表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitItemService extends IBaseService<PortraitItem> {


    /**
     * 根据Item_id查询
     * @param itemId
     * @return
     */
    default PortraitItem getByItemId(String itemId) {
        Assert.hasText(itemId,"itemId不能为空");
        return getOne(Wrappers.<PortraitItem>lambdaQuery().eq(PortraitItem::getItemId, itemId));
    }

}
