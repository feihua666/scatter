package scatter.portrait.rest.user.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.user.po.PortraitUserTags;
import scatter.portrait.pojo.user.vo.PortraitUserTagsVo;
import scatter.portrait.pojo.user.form.PortraitUserTagsAddForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserTagsPageQueryForm;

/**
 * <p>
 * 用户标签字典 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitUserTagsMapStruct extends IBaseVoMapStruct<PortraitUserTags, PortraitUserTagsVo>,
                                  IBaseAddFormMapStruct<PortraitUserTags,PortraitUserTagsAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitUserTags,PortraitUserTagsUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitUserTags,PortraitUserTagsPageQueryForm>{
    PortraitUserTagsMapStruct INSTANCE = Mappers.getMapper( PortraitUserTagsMapStruct.class );

}
