package scatter.portrait.rest.item.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.item.form.PortraitItemTagsAddForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemTagsPageQueryForm;
import java.util.List;
/**
 * <p>
 * Item标签字典表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
public interface IPortraitItemTagsService extends IBaseService<PortraitItemTags> {


    /**
     * 根据编码查询
     * @param code
     * @return
     */
    default PortraitItemTags getByCode(String code) {
        Assert.hasText(code,"code不能为空");
        return getOne(Wrappers.<PortraitItemTags>lambdaQuery().eq(PortraitItemTags::getCode, code));
    }

}
