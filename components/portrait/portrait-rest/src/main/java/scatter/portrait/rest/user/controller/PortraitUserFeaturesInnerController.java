package scatter.portrait.rest.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.rest.PortraitConfiguration;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;
import scatter.portrait.rest.user.service.IPortraitUserFeaturesService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户特征字典表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/inner/user/portrait-user-features")
public class PortraitUserFeaturesInnerController extends BaseInnerController<PortraitUserFeatures> {
 @Autowired
 private IPortraitUserFeaturesService portraitUserFeaturesService;

 public IPortraitUserFeaturesService getService(){
     return portraitUserFeaturesService;
 }
}
