package scatter.portrait.rest.item.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.item.po.PortraitItemFeaturesRel;
import scatter.portrait.pojo.item.vo.PortraitItemFeaturesRelVo;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesRelPageQueryForm;

/**
 * <p>
 * item和特征 mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitItemFeaturesRelMapStruct extends IBaseVoMapStruct<PortraitItemFeaturesRel, PortraitItemFeaturesRelVo>,
                                  IBaseAddFormMapStruct<PortraitItemFeaturesRel,PortraitItemFeaturesRelAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitItemFeaturesRel,PortraitItemFeaturesRelUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitItemFeaturesRel,PortraitItemFeaturesRelPageQueryForm>{
    PortraitItemFeaturesRelMapStruct INSTANCE = Mappers.getMapper( PortraitItemFeaturesRelMapStruct.class );

}
