package scatter.portrait.rest.item.service.impl;

import scatter.portrait.pojo.item.po.PortraitItemFeatures;
import scatter.portrait.rest.item.mapper.PortraitItemFeaturesMapper;
import scatter.portrait.rest.item.service.IPortraitItemFeaturesService;
import scatter.common.rest.service.IBaseAddUpdateQueryFormServiceImpl;
import org.springframework.stereotype.Service;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesAddForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemFeaturesPageQueryForm;
import org.springframework.transaction.annotation.Transactional;
/**
 * <p>
 * Item特征字典表 服务实现类
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Service
@Transactional
public class PortraitItemFeaturesServiceImpl extends IBaseAddUpdateQueryFormServiceImpl<PortraitItemFeaturesMapper, PortraitItemFeatures, PortraitItemFeaturesAddForm, PortraitItemFeaturesUpdateForm, PortraitItemFeaturesPageQueryForm> implements IPortraitItemFeaturesService {
    @Override
    public void preAdd(PortraitItemFeaturesAddForm addForm,PortraitItemFeatures po) {
        super.preAdd(addForm,po);
        if (!isStrEmpty(addForm.getCode())) {
            // 编码已存在不能添加
            assertByColumn(addForm.getCode(),PortraitItemFeatures::getCode,false);
        }

    }

    @Override
    public void preUpdate(PortraitItemFeaturesUpdateForm updateForm,PortraitItemFeatures po) {
        super.preUpdate(updateForm,po);
        if (!isStrEmpty(updateForm.getCode())) {
            PortraitItemFeatures byId = getById(updateForm.getId());
            // 如果编码有改动
            if (!isEqual(updateForm.getCode(), byId.getCode())) {
                // 编码已存在不能修改
                assertByColumn(updateForm.getCode(),PortraitItemFeatures::getCode,false);
            }
        }

    }
}
