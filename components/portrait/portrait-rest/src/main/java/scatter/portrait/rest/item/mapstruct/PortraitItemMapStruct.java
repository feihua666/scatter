package scatter.portrait.rest.item.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import scatter.common.rest.mapstruct.IBaseAddFormMapStruct;
import scatter.common.rest.mapstruct.IBaseQueryFormMapStruct;
import scatter.common.rest.mapstruct.IBaseUpdateFormMapStruct;
import scatter.common.rest.mapstruct.IBaseVoMapStruct;
import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.portrait.pojo.item.vo.PortraitItemVo;
import scatter.portrait.pojo.item.form.PortraitItemAddForm;
import scatter.portrait.pojo.item.form.PortraitItemUpdateForm;
import scatter.portrait.pojo.item.form.PortraitItemPageQueryForm;

/**
 * <p>
 * item mapStruct 转换接口
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PortraitItemMapStruct extends IBaseVoMapStruct<PortraitItem, PortraitItemVo>,
                                  IBaseAddFormMapStruct<PortraitItem,PortraitItemAddForm>,
                                  IBaseUpdateFormMapStruct<PortraitItem,PortraitItemUpdateForm>,
                                  IBaseQueryFormMapStruct<PortraitItem,PortraitItemPageQueryForm>{
    PortraitItemMapStruct INSTANCE = Mappers.getMapper( PortraitItemMapStruct.class );

}
