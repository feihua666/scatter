package scatter.portrait.rest.user.scheduler.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import scatter.common.rest.controller.SuperController;
import scatter.portrait.rest.PortraitConfiguration;
import scatter.portrait.rest.user.scheduler.job.IPortraitUserSynchronizeJob;

/**
 * <p>
 * 画像用户计划任务控制器，主要供计划任务调度器以http请求方式调用
 * </p>
 *
 * @author yangwei
 * @since 2021-09-29 13:31
 */
@Api(tags = "画像用户计划任务相关接口")
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/user/scheduler")
public class RecdUserSchedulerController extends SuperController {

	@Autowired
	private IPortraitUserSynchronizeJob iRecdUserSynchronizeJob;

	@ApiOperation("同步全量用户数据")
	@PostMapping("/user/synchronize-all")
	@ResponseStatus(HttpStatus.CREATED)
	public Boolean synchronizeAll() {
		iRecdUserSynchronizeJob.synchronizeAll();
		return true;
	}
	@ApiOperation("同步增量用户数据")
	@PostMapping("/user/synchronize-increment")
	@ResponseStatus(HttpStatus.CREATED)
	public Boolean synchronizeIncrement() {
		iRecdUserSynchronizeJob.synchronizeIncrement();
		return true;
	}
}
