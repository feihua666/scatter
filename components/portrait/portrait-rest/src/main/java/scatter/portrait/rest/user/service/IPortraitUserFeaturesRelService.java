package scatter.portrait.rest.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.util.Assert;
import scatter.portrait.pojo.user.po.PortraitUserFeaturesRel;
import scatter.common.rest.service.IBaseService;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelAddForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelUpdateForm;
import scatter.portrait.pojo.user.form.PortraitUserFeaturesRelPageQueryForm;
import java.util.List;
/**
 * <p>
 * 用户和特征关系表 服务类
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
public interface IPortraitUserFeaturesRelService extends IBaseService<PortraitUserFeaturesRel> {

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    default List<PortraitUserFeaturesRel> getByUserId(String userId) {
        Assert.hasText(userId,"userId不能为空");
        return list(Wrappers.<PortraitUserFeaturesRel>lambdaQuery().eq(PortraitUserFeaturesRel::getUserId, userId));
    }
    /**
     * 根据特征id查询
     * @param userFeaturesId
     * @return
     */
    default List<PortraitUserFeaturesRel> getByUserFeaturesId(String userFeaturesId) {
        Assert.hasText(userFeaturesId,"userFeaturesId不能为空");
        return list(Wrappers.<PortraitUserFeaturesRel>lambdaQuery().eq(PortraitUserFeaturesRel::getUserFeaturesId, userFeaturesId));
    }
    /**
     * 根据租户id查询
     * @param tenantId
     * @return
     */
    default List<PortraitUserFeaturesRel> getByTenantId(String tenantId) {
        Assert.hasText(tenantId,"tenantId不能为空");
        return list(Wrappers.<PortraitUserFeaturesRel>lambdaQuery().eq(PortraitUserFeaturesRel::getTenantId, tenantId));
    }
    /**
     * 根据来源标识查询
     * @param sourceDictId
     * @return
     */
    default List<PortraitUserFeaturesRel> getBySourceDictId(String sourceDictId) {
        Assert.hasText(sourceDictId,"sourceDictId不能为空");
        return list(Wrappers.<PortraitUserFeaturesRel>lambdaQuery().eq(PortraitUserFeaturesRel::getSourceDictId, sourceDictId));
    }

}
