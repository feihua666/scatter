package scatter.portrait.rest.item.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scatter.common.rest.tools.InterfaceTool;
import scatter.common.rest.trans.ITransService;
import scatter.common.rest.trans.TransResult;
import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.portrait.rest.item.service.IPortraitItemTagsService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * Item标签字典翻译实现
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Component
public class PortraitItemTagsTransServiceImpl implements ITransService<Object,String>, InterfaceTool {

    @Autowired
    private IPortraitItemTagsService portraitItemTagsService;

    @Override
    public boolean support(String type) {
        return isEqualAny(type,PortraitItemTags.TRANS_PORTRAITITEMTAGS_BY_ID);
    }

    @Override
    public TransResult<Object, String> trans(String type, String key) {
        if (isEqual(type,PortraitItemTags.TRANS_PORTRAITITEMTAGS_BY_ID)) {
            PortraitItemTags byId = portraitItemTagsService.getById(key);
            return new TransResult(byId,key);
        }
        return null;
    }

    @Override
    public boolean supportBatch(String type) {
        return support(type);
    }

    @Override
    public List<TransResult<Object, String>> transBatch(String type, Set<String> keys) {
        if (isEqual(type,PortraitItemTags.TRANS_PORTRAITITEMTAGS_BY_ID)) {
            return portraitItemTagsService.listByIds(keys).stream().map(item->new TransResult<Object, String>(item,item.getId())).collect(Collectors.toList());
        }
        return null;
    }
}
