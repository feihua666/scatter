package scatter.portrait.rest.item.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.rest.PortraitConfiguration;
import scatter.portrait.pojo.item.po.PortraitItemTags;
import scatter.portrait.rest.item.service.IPortraitItemTagsService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * Item标签字典表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/inner/item/portrait-item-tags")
public class PortraitItemTagsInnerController extends BaseInnerController<PortraitItemTags> {
 @Autowired
 private IPortraitItemTagsService portraitItemTagsService;

 public IPortraitItemTagsService getService(){
     return portraitItemTagsService;
 }
}
