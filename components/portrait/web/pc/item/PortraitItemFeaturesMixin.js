import PortraitItemFeaturesForm from './PortraitItemFeaturesForm.js'
import PortraitItemFeaturesTable from './PortraitItemFeaturesTable.js'
import PortraitItemFeaturesUrl from './PortraitItemFeaturesUrl.js'
const PortraitItemFeaturesMixin = {
    computed: {
        PortraitItemFeaturesFormOptions() {
            return this.$stDynamicFormTools.options(PortraitItemFeaturesForm,this.$options.name)
        },
        PortraitItemFeaturesTableOptions() {
            return PortraitItemFeaturesTable
        },
        PortraitItemFeaturesUrl(){
            return PortraitItemFeaturesUrl
        }
    },
}
export default PortraitItemFeaturesMixin