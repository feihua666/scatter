import PortraitItemRoute from './PortraitItemRoute.js'
import PortraitItemTagsRoute from './PortraitItemTagsRoute.js'
import PortraitItemFeaturesRoute from './PortraitItemFeaturesRoute.js'
import PortraitItemFeaturesRelRoute from './PortraitItemFeaturesRelRoute.js'
import PortraitItemTagsRelRoute from './PortraitItemTagsRelRoute.js'

const PortraitItemRoutesEntry = [
]
.concat(PortraitItemRoute)
.concat(PortraitItemTagsRoute)
.concat(PortraitItemFeaturesRoute)
.concat(PortraitItemFeaturesRelRoute)
.concat(PortraitItemTagsRelRoute)
export default PortraitItemRoutesEntry