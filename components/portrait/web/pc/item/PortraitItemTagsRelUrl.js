const basePath = '' + '/item/portrait-item-tags-rel'
const PortraitItemTagsRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    itemAssignItemTags: basePath + '/item/assign/itemtags',
    checkedItemTagsIds: basePath + '/item/{itemId}',
    itemTagsAssignItem: basePath + '/itemtags/assign/item',
    checkedItemIds: basePath + '/itemtags/{itemTagsId}',
    router: {
        searchList: '/PortraitItemTagsRelSearchList',
        add: '/PortraitItemTagsRelAdd',
        update: '/PortraitItemTagsRelUpdate',
        itemAssignItemTagsRel: '/itemAssignItemTagsRel',
        itemTagsAssignItemRel: '/itemTagsAssignItemRel',
    }
}
export default PortraitItemTagsRelUrl