import PortraitItemFeaturesUrl from './PortraitItemFeaturesUrl.js'

const PortraitItemFeaturesRoute = [
    {
        path: PortraitItemFeaturesUrl.router.searchList,
        component: () => import('./element/PortraitItemFeaturesSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitItemFeaturesSearchList',
            name: 'Item特征字典管理'
        }
    },
    {
        path: PortraitItemFeaturesUrl.router.add,
        component: () => import('./element/PortraitItemFeaturesAdd'),
        meta: {
            code:'PortraitItemFeaturesAdd',
            name: 'Item特征字典添加'
        }
    },
    {
        path: PortraitItemFeaturesUrl.router.update,
        component: () => import('./element/PortraitItemFeaturesUpdate'),
        meta: {
            code:'PortraitItemFeaturesUpdate',
            name: 'Item特征字典修改'
        }
    },
]
export default PortraitItemFeaturesRoute