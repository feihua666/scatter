import PortraitItemUrl from './PortraitItemUrl.js'
const PortraitItemForm = [
    {
        PortraitItemSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'itemId',
        },
        element:{
            label: 'Item_id',
            required: true,
        }
    },
    {
        PortraitItemSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'itemTypeDictId',
        },
        element:{
            label: '商品类型',
            type: 'selectDict',
            options: {
                groupCode: 'portrait_item_type'
            },
            required: true,
        }
    },

    {

        field: {
            name: 'title',
        },
        element:{
            label: '标题',
        }
    },

    {

        field: {
            name: 'author',
        },
        element:{
            label: '作者',
        }
    },
    {

        field: {
            name: 'content',
        },
        element:{
            label: '内容摘要',
        }
    },

    {

        field: {
            name: 'country',
        },
        element:{
            label: '国家',
        }
    },
    {

        field: {
            name: 'city',
        },
        element:{
            label: '城市名称',
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },

]
export default PortraitItemForm