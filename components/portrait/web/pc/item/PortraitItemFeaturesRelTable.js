const PortraitItemFeaturesRelTable = [
    {
        prop: 'itemId',
        label: 'item id'
    },
    {
        prop: 'itemFeaturesName',
        label: 'item特征'
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
    {
        prop: 'sourceDictName',
        label: '来源标识'
    },
]
export default PortraitItemFeaturesRelTable