const basePath = '' + '/item/portrait-item'
const PortraitItemUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitItemSearchList',
    }
}
export default PortraitItemUrl