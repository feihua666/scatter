const PortraitItemTable = [
    {
        type: 'expand'
    },
    {
        prop: 'itemId',
        label: 'Item_id'
    },
    {
        prop: 'itemTypeDictName',
        label: '商品类型'
    },
    {
        prop: 'isValid',
        label: '是否有效'
    },
    {
        prop: 'sceneId',
        label: '场景id',
        showInDetail: true
    },
    {
        prop: 'videoDuration',
        label: '视频时长',
        showInDetail: true
    },
    {
        prop: 'publishAt',
        label: '发布时间'
    },
    {
        prop: 'expireAt',
        label: '失效时间'
    },
    {
        prop: 'lastModifyAt',
        label: '最后更新时间',
        showInDetail: true
    },
    {
        prop: 'title',
        label: '标题'
    },
    {
        prop: 'weight',
        label: 'item的加权',
        showInDetail: true
    },
    {
        prop: 'categoryLevel',
        label: '类目层级数',
        showInDetail: true
    },
    {
        prop: 'categoryPath',
        label: '类目路径',
        showInDetail: true
    },
    {
        prop: 'author',
        label: '作者'
    },
    {
        prop: 'content',
        label: '内容摘要',
        showInDetail: true
    },
    {
        prop: 'exposeCount',
        label: '一个月内爆光次数',
        showInDetail: true
    },
    {
        prop: 'clickCount',
        label: '一个月内点击次数',
        showInDetail: true
    },
    {
        prop: 'likeCount',
        label: '一个月内点赞次数',
        showInDetail: true
    },
    {
        prop: 'unlikeCount',
        label: '一个月内踩次数',
        showInDetail: true
    },
    {
        prop: 'commentCount',
        label: '一个月内评论次数',
        showInDetail: true
    },
    {
        prop: 'collectCount',
        label: '一个月内收藏次数',
        showInDetail: true
    },
    {
        prop: 'shareCount',
        label: '一个月内分享次数',
        showInDetail: true
    },
    {
        prop: 'downloadCount',
        label: '一个月内下载次数',
        showInDetail: true
    },
    {
        prop: 'tipCount',
        label: '一个月内打赏次数',
        showInDetail: true
    },
    {
        prop: 'subscribeCount',
        label: '一个月内关注次数',
        showInDetail: true
    },
    {
        prop: 'country',
        label: '国家',
        showInDetail: true
    },
    {
        prop: 'city',
        label: '城市名称',
        showInDetail: true
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
]
export default PortraitItemTable