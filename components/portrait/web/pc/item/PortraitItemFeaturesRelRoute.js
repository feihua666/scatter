import PortraitItemFeaturesRelUrl from './PortraitItemFeaturesRelUrl.js'

const PortraitItemFeaturesRelRoute = [
    {
        path: PortraitItemFeaturesRelUrl.router.searchList,
        component: () => import('./element/PortraitItemFeaturesRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitItemFeaturesRelSearchList',
            name: 'item和特征管理'
        }
    },
    {
        path: PortraitItemFeaturesRelUrl.router.add,
        component: () => import('./element/PortraitItemFeaturesRelAdd'),
        meta: {
            code:'PortraitItemFeaturesRelAdd',
            name: 'item和特征添加'
        }
    },
    {
        path: PortraitItemFeaturesRelUrl.router.update,
        component: () => import('./element/PortraitItemFeaturesRelUpdate'),
        meta: {
            code:'PortraitItemFeaturesRelUpdate',
            name: 'item和特征修改'
        }
    },
    {
        path: PortraitItemFeaturesRelUrl.router.itemAssignItemFeaturesRel,
        component: () => import('./element/ItemAssignItemFeaturesRel'),
        meta: {
            code:'ItemAssignItemFeaturesRel',
            name: 'item分配特征'
        }
    },
    {
        path: PortraitItemFeaturesRelUrl.router.itemFeaturesAssignItemRel,
        component: () => import('./element/ItemFeaturesAssignItemRel'),
        meta: {
            code:'ItemFeaturesAssignItemRel',
            name: '特征分配item'
        }
    },
]
export default PortraitItemFeaturesRelRoute