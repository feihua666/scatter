import PortraitItemFeaturesRelUrl from './PortraitItemFeaturesRelUrl.js'
import PortraitItemFeaturesUrl from './PortraitItemFeaturesUrl.js'
const PortraitItemFeaturesRelForm = [
    {
        PortraitItemFeaturesRelSearchList: {
            element:{
                required: false
            }
        },

        ItemAssignItemFeaturesRel: false,
        ItemFeaturesAssignItemRel: false,
        field: {
            name: 'itemId',
        },
        element:{
            label: 'item id',
            required: true,
        }
    },
    {
        PortraitItemFeaturesRelSearchList: {
            element:{
                required: false
            }
        },

        ItemAssignItemFeaturesRel: false,
        ItemFeaturesAssignItemRel: false,
        field: {
            name: 'itemFeaturesId',
        },
        element:{
            label: 'item特征',
            type: 'select',
            options: {
                datas: PortraitItemFeaturesUrl.list
            },
            required: true,
        }
    },
    {

        ItemAssignItemFeaturesRel: false,
        ItemFeaturesAssignItemRel: false,
        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitItemFeaturesRelSearchList: {
            element:{
                required: false
            }
        },

        ItemAssignItemFeaturesRel: false,
        ItemFeaturesAssignItemRel: false,
        PortraitItemFeaturesRelAdd: false,
        PortraitItemFeaturesRelUpdate: false,
        field: {
            name: 'sourceDictId',
        },
        element:{
            label: '来源标识',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'feature_tag_source'
            },
        }
    },

    {
        PortraitItemFeaturesRelSearchList: false,
        PortraitItemFeaturesRelAdd: false,
        PortraitItemFeaturesRelUpdate: false,
        ItemFeaturesAssignItemRel: false,

        field: {
            name: 'itemId',
        },
        element:{
            label: 'item',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo 用于回显，这里可以是一个列表地址，如：RoleUrl.list
            }

        }
    },
    {
        PortraitItemFeaturesRelSearchList: false,
        PortraitItemFeaturesRelAdd: false,
        PortraitItemFeaturesRelUpdate: false,
        ItemFeaturesAssignItemRel: false,

        field: {
            name: 'checkedItemFeaturesIds',
            value: []
        },
        element:{
            label: '特征',
            type: 'tree',
            options: {
                datas: PortraitItemFeaturesUrl.list,
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        PortraitItemFeaturesRelSearchList: false,
        PortraitItemFeaturesRelAdd: false,
        PortraitItemFeaturesRelUpdate: false,
        ItemAssignItemFeaturesRel: false,

        field: {
            name: 'itemFeaturesId',
        },
        element:{
            label: '特征',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        PortraitItemFeaturesRelSearchList: false,
        PortraitItemFeaturesRelAdd: false,
        PortraitItemFeaturesRelUpdate: false,
        ItemAssignItemFeaturesRel: false,

        field: {
            name: 'checkedItemIds',
            value: []
        },
        element:{
            label: 'item',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default PortraitItemFeaturesRelForm