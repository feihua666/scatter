import PortraitItemTagsRelUrl from './PortraitItemTagsRelUrl.js'

const PortraitItemTagsRelRoute = [
    {
        path: PortraitItemTagsRelUrl.router.searchList,
        component: () => import('./element/PortraitItemTagsRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitItemTagsRelSearchList',
            name: 'item和标签管理'
        }
    },
    {
        path: PortraitItemTagsRelUrl.router.add,
        component: () => import('./element/PortraitItemTagsRelAdd'),
        meta: {
            code:'PortraitItemTagsRelAdd',
            name: 'item和标签添加'
        }
    },
    {
        path: PortraitItemTagsRelUrl.router.update,
        component: () => import('./element/PortraitItemTagsRelUpdate'),
        meta: {
            code:'PortraitItemTagsRelUpdate',
            name: 'item和标签修改'
        }
    },
    {
        path: PortraitItemTagsRelUrl.router.itemAssignItemTagsRel,
        component: () => import('./element/ItemAssignItemTagsRel'),
        meta: {
            code:'ItemAssignItemTagsRel',
            name: 'item分配标签'
        }
    },
    {
        path: PortraitItemTagsRelUrl.router.itemTagsAssignItemRel,
        component: () => import('./element/ItemTagsAssignItemRel'),
        meta: {
            code:'ItemTagsAssignItemRel',
            name: '标签分配item'
        }
    },
]
export default PortraitItemTagsRelRoute