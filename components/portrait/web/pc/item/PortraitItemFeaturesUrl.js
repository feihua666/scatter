const basePath = '' + '/item/portrait-item-features'
const PortraitItemFeaturesUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitItemFeaturesSearchList',
        add: '/PortraitItemFeaturesAdd',
        update: '/PortraitItemFeaturesUpdate',
    }
}
export default PortraitItemFeaturesUrl