import PortraitItemTagsRelForm from './PortraitItemTagsRelForm.js'
import PortraitItemTagsRelTable from './PortraitItemTagsRelTable.js'
import PortraitItemTagsRelUrl from './PortraitItemTagsRelUrl.js'
const PortraitItemTagsRelMixin = {
    computed: {
        PortraitItemTagsRelFormOptions() {
            return this.$stDynamicFormTools.options(PortraitItemTagsRelForm,this.$options.name)
        },
        PortraitItemTagsRelTableOptions() {
            return PortraitItemTagsRelTable
        },
        PortraitItemTagsRelUrl(){
            return PortraitItemTagsRelUrl
        }
    },
}
export default PortraitItemTagsRelMixin