import PortraitItemForm from './PortraitItemForm.js'
import PortraitItemTable from './PortraitItemTable.js'
import PortraitItemUrl from './PortraitItemUrl.js'
const PortraitItemMixin = {
    computed: {
        PortraitItemFormOptions() {
            return this.$stDynamicFormTools.options(PortraitItemForm,this.$options.name)
        },
        PortraitItemTableOptions() {
            return PortraitItemTable
        },
        PortraitItemUrl(){
            return PortraitItemUrl
        }
    },
}
export default PortraitItemMixin