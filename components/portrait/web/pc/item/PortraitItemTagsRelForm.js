import PortraitItemTagsRelUrl from './PortraitItemTagsRelUrl.js'
import PortraitItemTagsUrl from './PortraitItemTagsUrl.js'
const PortraitItemTagsRelForm = [
    {
        PortraitItemTagsRelSearchList: {
            element:{
                required: false
            }
        },

        ItemAssignItemTagsRel: false,
        ItemTagsAssignItemRel: false,
        field: {
            name: 'itemId',
        },
        element:{
            label: 'item id',
            required: true,
        }
    },
    {
        PortraitItemTagsRelSearchList: {
            element:{
                required: false
            }
        },

        ItemAssignItemTagsRel: false,
        ItemTagsAssignItemRel: false,
        field: {
            name: 'itemTagsId',
        },
        element:{
            label: 'item标签',
            type: 'select',
            options: {
                datas: PortraitItemTagsUrl.list
            },
            required: true,
        }
    },
    {

        ItemAssignItemTagsRel: false,
        ItemTagsAssignItemRel: false,
        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitItemTagsRelSearchList: {
            element:{
                required: false
            }
        },

        ItemAssignItemTagsRel: false,
        ItemTagsAssignItemRel: false,
        PortraitItemTagsRelAdd: false,
        PortraitItemTagsRelUpdate: false,
        field: {
            name: 'sourceDictId',
        },
        element:{
            label: '来源标识',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'feature_tag_source'
            },
        }
    },

    {
        PortraitItemTagsRelSearchList: false,
        PortraitItemTagsRelAdd: false,
        PortraitItemTagsRelUpdate: false,
        ItemTagsAssignItemRel: false,

        field: {
            name: 'itemId',
        },
        element:{
            label: 'item',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo 用于回显，这里可以是一个列表地址，如：RoleUrl.list
            }

        }
    },
    {
        PortraitItemTagsRelSearchList: false,
        PortraitItemTagsRelAdd: false,
        PortraitItemTagsRelUpdate: false,
        ItemTagsAssignItemRel: false,

        field: {
            name: 'checkedItemTagsIds',
            value: []
        },
        element:{
            label: '标签',
            type: 'tree',
            options: {
                datas: PortraitItemTagsUrl.list,
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        PortraitItemTagsRelSearchList: false,
        PortraitItemTagsRelAdd: false,
        PortraitItemTagsRelUpdate: false,
        ItemAssignItemTagsRel: false,

        field: {
            name: 'itemTagsId',
        },
        element:{
            label: '标签',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        PortraitItemTagsRelSearchList: false,
        PortraitItemTagsRelAdd: false,
        PortraitItemTagsRelUpdate: false,
        ItemAssignItemTagsRel: false,

        field: {
            name: 'checkedItemIds',
            value: []
        },
        element:{
            label: 'item',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default PortraitItemTagsRelForm