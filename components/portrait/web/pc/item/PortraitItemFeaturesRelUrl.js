const basePath = '' + '/item/portrait-item-features-rel'
const PortraitItemFeaturesRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    itemAssignItemFeatures: basePath + '/item/assign/itemfeatures',
    checkedItemFeaturesIds: basePath + '/item/{itemId}',
    itemFeaturesAssignItem: basePath + '/itemfeatures/assign/item',
    checkedItemIds: basePath + '/itemfeatures/{itemFeaturesId}',
    router: {
        searchList: '/PortraitItemFeaturesRelSearchList',
        add: '/PortraitItemFeaturesRelAdd',
        update: '/PortraitItemFeaturesRelUpdate',
        itemAssignItemFeaturesRel: '/itemAssignItemFeaturesRel',
        itemFeaturesAssignItemRel: '/itemFeaturesAssignItemRel',
    }
}
export default PortraitItemFeaturesRelUrl