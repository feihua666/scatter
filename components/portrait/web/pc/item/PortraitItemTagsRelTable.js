const PortraitItemTagsRelTable = [
    {
        prop: 'itemId',
        label: 'item id'
    },
    {
        prop: 'itemTagsName',
        label: 'item标签'
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
    {
        prop: 'sourceDictName',
        label: '来源标识'
    },
]
export default PortraitItemTagsRelTable