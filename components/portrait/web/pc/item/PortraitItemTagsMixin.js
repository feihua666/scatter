import PortraitItemTagsForm from './PortraitItemTagsForm.js'
import PortraitItemTagsTable from './PortraitItemTagsTable.js'
import PortraitItemTagsUrl from './PortraitItemTagsUrl.js'
const PortraitItemTagsMixin = {
    computed: {
        PortraitItemTagsFormOptions() {
            return this.$stDynamicFormTools.options(PortraitItemTagsForm,this.$options.name)
        },
        PortraitItemTagsTableOptions() {
            return PortraitItemTagsTable
        },
        PortraitItemTagsUrl(){
            return PortraitItemTagsUrl
        }
    },
}
export default PortraitItemTagsMixin