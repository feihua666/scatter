const PortraitItemTagsTable = [
    {
        prop: 'code',
        label: '编码'
    },
    {
        prop: 'name',
        label: '名称'
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
    {
        prop: 'typeDictName',
        label: '类型'
    },
]
export default PortraitItemTagsTable