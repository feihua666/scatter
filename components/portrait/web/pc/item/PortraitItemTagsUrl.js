const basePath = '' + '/item/portrait-item-tags'
const PortraitItemTagsUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitItemTagsSearchList',
        add: '/PortraitItemTagsAdd',
        update: '/PortraitItemTagsUpdate',
    }
}
export default PortraitItemTagsUrl