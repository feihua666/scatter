import PortraitItemTagsUrl from './PortraitItemTagsUrl.js'
const PortraitItemTagsForm = [
    {
        PortraitItemTagsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        PortraitItemTagsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitItemTagsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'portrait_item_feature_tag_type'
            },
        }
    },

]
export default PortraitItemTagsForm