import PortraitItemTagsUrl from './PortraitItemTagsUrl.js'

const PortraitItemTagsRoute = [
    {
        path: PortraitItemTagsUrl.router.searchList,
        component: () => import('./element/PortraitItemTagsSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitItemTagsSearchList',
            name: 'Item标签字典管理'
        }
    },
    {
        path: PortraitItemTagsUrl.router.add,
        component: () => import('./element/PortraitItemTagsAdd'),
        meta: {
            code:'PortraitItemTagsAdd',
            name: 'Item标签字典添加'
        }
    },
    {
        path: PortraitItemTagsUrl.router.update,
        component: () => import('./element/PortraitItemTagsUpdate'),
        meta: {
            code:'PortraitItemTagsUpdate',
            name: 'Item标签字典修改'
        }
    },
]
export default PortraitItemTagsRoute