import PortraitItemUrl from './PortraitItemUrl.js'

const PortraitItemRoute = [
    {
        path: PortraitItemUrl.router.searchList,
        component: () => import('./element/PortraitItemSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitItemSearchList',
            name: 'item管理'
        }
    },

]
export default PortraitItemRoute