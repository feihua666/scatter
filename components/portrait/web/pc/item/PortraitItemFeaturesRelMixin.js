import PortraitItemFeaturesRelForm from './PortraitItemFeaturesRelForm.js'
import PortraitItemFeaturesRelTable from './PortraitItemFeaturesRelTable.js'
import PortraitItemFeaturesRelUrl from './PortraitItemFeaturesRelUrl.js'
const PortraitItemFeaturesRelMixin = {
    computed: {
        PortraitItemFeaturesRelFormOptions() {
            return this.$stDynamicFormTools.options(PortraitItemFeaturesRelForm,this.$options.name)
        },
        PortraitItemFeaturesRelTableOptions() {
            return PortraitItemFeaturesRelTable
        },
        PortraitItemFeaturesRelUrl(){
            return PortraitItemFeaturesRelUrl
        }
    },
}
export default PortraitItemFeaturesRelMixin