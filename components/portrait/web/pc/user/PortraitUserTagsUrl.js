const basePath = '' + '/user/portrait-user-tags'
const PortraitUserTagsUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitUserTagsSearchList',
        add: '/PortraitUserTagsAdd',
        update: '/PortraitUserTagsUpdate',
    }
}
export default PortraitUserTagsUrl