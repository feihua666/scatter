import PortraitUserBehaviorForm from './PortraitUserBehaviorForm.js'
import PortraitUserBehaviorTable from './PortraitUserBehaviorTable.js'
import PortraitUserBehaviorUrl from './PortraitUserBehaviorUrl.js'
const PortraitUserBehaviorMixin = {
    computed: {
        PortraitUserBehaviorFormOptions() {
            return this.$stDynamicFormTools.options(PortraitUserBehaviorForm,this.$options.name)
        },
        PortraitUserBehaviorTableOptions() {
            return PortraitUserBehaviorTable
        },
        PortraitUserBehaviorUrl(){
            return PortraitUserBehaviorUrl
        }
    },
}
export default PortraitUserBehaviorMixin