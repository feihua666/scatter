import PortraitUserTagsForm from './PortraitUserTagsForm.js'
import PortraitUserTagsTable from './PortraitUserTagsTable.js'
import PortraitUserTagsUrl from './PortraitUserTagsUrl.js'
const PortraitUserTagsMixin = {
    computed: {
        PortraitUserTagsFormOptions() {
            return this.$stDynamicFormTools.options(PortraitUserTagsForm,this.$options.name)
        },
        PortraitUserTagsTableOptions() {
            return PortraitUserTagsTable
        },
        PortraitUserTagsUrl(){
            return PortraitUserTagsUrl
        }
    },
}
export default PortraitUserTagsMixin