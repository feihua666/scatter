import PortraitUserTagsRelUrl from './PortraitUserTagsRelUrl.js'
import PortraitUserTagsUrl from './PortraitUserTagsUrl.js'
const PortraitUserTagsRelForm = [
    {

        UserAssignUserTagsRel: false,
        UserTagsAssignUserRel: false,
        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
        }
    },
    {
        PortraitUserTagsRelSearchList: false,

        UserAssignUserTagsRel: false,
        UserTagsAssignUserRel: false,
        field: {
            name: 'userTagsId',
        },
        element:{
            label: '用户标签',
            type: 'select',
            options: {
                datas: PortraitUserTagsUrl.list
            },
            required: true,
        }
    },
    {

        UserAssignUserTagsRel: false,
        UserTagsAssignUserRel: false,
        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitUserTagsRelSearchList: {
            element:{
                required: false
            }
        },

        UserAssignUserTagsRel: false,
        UserTagsAssignUserRel: false,
        PortraitUserTagsRelAdd:false,
        PortraitUserTagsRelUpdate:false,
        field: {
            name: 'sourceDictId',
        },
        element:{
            label: '来源标识',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'feature_tag_source'
            },
        }
    },

    {
        PortraitUserTagsRelSearchList: false,
        PortraitUserTagsRelAdd: false,
        PortraitUserTagsRelUpdate: false,
        UserTagsAssignUserRel: false,

        field: {
            name: 'userId',
        },
        element:{
            label: '用户',
            required: true,
            type: 'select',
            readonly: true,
            options: ({$route,$vm})=>{
                let datas = []
                if ($route.query.userId) {
                    datas.push({
                        id: $route.query.userId,
                        nickname: $route.query.userNickname || $route.query.userId
                    })
                }
                return {
                    datas: datas,
                    optionProp:{
                        value: 'id', // 选中的值属性
                        label: 'nickname', // 显示的值属性
                    },
                    originProp: {
                        placeholder: '请按用户昵称搜索',
                        remote: true,
                    },
                    // 因为本项目存在两个用户后端组件，这是可以通过全局配置的方式决定使用哪个用户体系
                    remoteUrl: $vm.$stObjectTools.getValue($vm.$scatterConfig,'PortraitUserFeaturesRelForm.userRemoteSearchUrl') || '/user-simple/getPage',
                    remoteQueryProp: 'nickname'
                }
            }

        }
    },
    {
        PortraitUserTagsRelSearchList: false,
        PortraitUserTagsRelAdd: false,
        PortraitUserTagsRelUpdate: false,
        UserTagsAssignUserRel: false,

        field: {
            name: 'checkedUserTagsIds',
            value: []
        },
        element:{
            label: '标签',
            type: 'tree',
            options: {
                datas: PortraitUserTagsUrl.list,
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        PortraitUserTagsRelSearchList: false,
        PortraitUserTagsRelAdd: false,
        PortraitUserTagsRelUpdate: false,
        UserAssignUserTagsRel: false,

        field: {
            name: 'userTagsId',
        },
        element:{
            label: '标签',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        PortraitUserTagsRelSearchList: false,
        PortraitUserTagsRelAdd: false,
        PortraitUserTagsRelUpdate: false,
        UserAssignUserTagsRel: false,

        field: {
            name: 'checkedUserIds',
            value: []
        },
        element:{
            label: '用户',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default PortraitUserTagsRelForm