const PortraitUserFeaturesRelTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'userFeaturesName',
        label: '特征'
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
    {
        prop: 'sourceDictName',
        label: '来源标识'
    },
]
export default PortraitUserFeaturesRelTable