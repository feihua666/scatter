const PortraitUserTagsRelTable = [
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'userTagsName',
        label: '用户标签'
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
    {
        prop: 'sourceDictName',
        label: '来源标识'
    },
]
export default PortraitUserTagsRelTable