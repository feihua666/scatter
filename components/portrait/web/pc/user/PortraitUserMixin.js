import PortraitUserForm from './PortraitUserForm.js'
import PortraitUserTable from './PortraitUserTable.js'
import PortraitUserUrl from './PortraitUserUrl.js'
const PortraitUserMixin = {
    computed: {
        PortraitUserFormOptions() {
            return this.$stDynamicFormTools.options(PortraitUserForm,this.$options.name)
        },
        PortraitUserTableOptions() {
            return PortraitUserTable
        },
        PortraitUserUrl(){
            return PortraitUserUrl
        }
    },
}
export default PortraitUserMixin