import PortraitUserFeaturesRelForm from './PortraitUserFeaturesRelForm.js'
import PortraitUserFeaturesRelTable from './PortraitUserFeaturesRelTable.js'
import PortraitUserFeaturesRelUrl from './PortraitUserFeaturesRelUrl.js'
const PortraitUserFeaturesRelMixin = {
    computed: {
        PortraitUserFeaturesRelFormOptions() {
            return this.$stDynamicFormTools.options(PortraitUserFeaturesRelForm,this.$options.name)
        },
        PortraitUserFeaturesRelTableOptions() {
            return PortraitUserFeaturesRelTable
        },
        PortraitUserFeaturesRelUrl(){
            return PortraitUserFeaturesRelUrl
        }
    },
}
export default PortraitUserFeaturesRelMixin