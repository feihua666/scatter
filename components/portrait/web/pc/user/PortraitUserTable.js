const PortraitUserTable = [
    {
        type: 'expand'
    },
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'registTypeDictName',
        label: '用户注册类型'
    },
    {
        prop: 'imei',
        label: '设备串号'
    },
    {
        prop: 'mobile',
        label: '手机号',
        showInDetail: true
    },
    {
        prop: 'genderDictName',
        label: '性别'
    },
    {
        prop: 'age',
        label: '年龄'
    },
    {
        prop: 'ageGroupDictName',
        label: '年龄段'
    },
    {
        prop: 'country',
        label: '国家',
        showInDetail: true
    },
    {
        prop: 'city',
        label: '城市名称',
        showInDetail: true
    },
    {
        prop: 'deviceModelDictName',
        label: '设备型号',
        showInDetail: true
    },
    {
        prop: 'sourceFromDictName',
        label: '用户来源'
    },
    {
        prop: 'description',
        label: '用户描述'
    },
    {
        prop: 'registAt',
        label: '注册时间'
    },
    {
        prop: 'lastLoginAt',
        label: '最后一次登录时间',
        showInDetail: true
    },
    {
        prop: 'lastLoginIp',
        label: '最后一次登录ip',
        showInDetail: true
    },
    {
        prop: 'tenantId',
        label: '租户id',
        showInDetail: true
    },
]
export default PortraitUserTable