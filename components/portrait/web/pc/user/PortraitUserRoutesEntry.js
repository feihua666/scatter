import PortraitUserRoute from './PortraitUserRoute.js'
import PortraitUserBehaviorRoute from './PortraitUserBehaviorRoute.js'
import PortraitUserTagsRoute from './PortraitUserTagsRoute.js'
import PortraitUserFeaturesRoute from './PortraitUserFeaturesRoute.js'
import PortraitUserFeaturesRelRoute from './PortraitUserFeaturesRelRoute.js'
import PortraitUserTagsRelRoute from './PortraitUserTagsRelRoute.js'

const PortraitUserRoutesEntry = [
]
.concat(PortraitUserRoute)
.concat(PortraitUserBehaviorRoute)
.concat(PortraitUserTagsRoute)
.concat(PortraitUserFeaturesRoute)
.concat(PortraitUserFeaturesRelRoute)
.concat(PortraitUserTagsRelRoute)
export default PortraitUserRoutesEntry