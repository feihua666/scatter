const basePath = '' + '/user/portrait-user'
const PortraitUserUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitUserSearchList',
        add: '/PortraitUserAdd',
        update: '/PortraitUserUpdate',
    }
}
export default PortraitUserUrl