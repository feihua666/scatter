const basePath = '' + '/user/portrait-user-tags-rel'
const PortraitUserTagsRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    userAssignUserTags: basePath + '/user/assign/usertags',
    checkedUserTagsIds: basePath + '/user/{userId}',
    userTagsAssignUser: basePath + '/usertags/assign/user',
    checkedUserIds: basePath + '/usertags/{userTagsId}',
    router: {
        searchList: '/PortraitUserTagsRelSearchList',
        add: '/PortraitUserTagsRelAdd',
        update: '/PortraitUserTagsRelUpdate',
        userAssignUserTagsRel: '/userAssignUserTagsRel',
        userTagsAssignUserRel: '/userTagsAssignUserRel',
    }
}
export default PortraitUserTagsRelUrl