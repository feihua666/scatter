const basePath = '' + '/user/portrait-user-features-rel'
const PortraitUserFeaturesRelUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    userAssignUserFeatures: basePath + '/user/assign/userfeatures',
    checkedUserFeaturesIds: basePath + '/user/{userId}',
    userFeaturesAssignUser: basePath + '/userfeatures/assign/user',
    checkedUserIds: basePath + '/userfeatures/{userFeaturesId}',
    router: {
        searchList: '/PortraitUserFeaturesRelSearchList',
        add: '/PortraitUserFeaturesRelAdd',
        update: '/PortraitUserFeaturesRelUpdate',
        userAssignUserFeaturesRel: '/userAssignUserFeaturesRel',
        userFeaturesAssignUserRel: '/userFeaturesAssignUserRel',
    }
}
export default PortraitUserFeaturesRelUrl