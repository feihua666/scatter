import PortraitUserFeaturesUrl from './PortraitUserFeaturesUrl.js'

const PortraitUserFeaturesRoute = [
    {
        path: PortraitUserFeaturesUrl.router.searchList,
        component: () => import('./element/PortraitUserFeaturesSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitUserFeaturesSearchList',
            name: '用户特征字典管理'
        }
    },
    {
        path: PortraitUserFeaturesUrl.router.add,
        component: () => import('./element/PortraitUserFeaturesAdd'),
        meta: {
            code:'PortraitUserFeaturesAdd',
            name: '用户特征字典添加'
        }
    },
    {
        path: PortraitUserFeaturesUrl.router.update,
        component: () => import('./element/PortraitUserFeaturesUpdate'),
        meta: {
            code:'PortraitUserFeaturesUpdate',
            name: '用户特征字典修改'
        }
    },
]
export default PortraitUserFeaturesRoute