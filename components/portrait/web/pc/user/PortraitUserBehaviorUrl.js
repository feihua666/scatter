const basePath = '' + '/user/portrait-user-behavior'
const PortraitUserBehaviorUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitUserBehaviorSearchList',
        add: '/PortraitUserBehaviorAdd',
        update: '/PortraitUserBehaviorUpdate',
    }
}
export default PortraitUserBehaviorUrl