import PortraitUserFeaturesUrl from './PortraitUserFeaturesUrl.js'
const PortraitUserFeaturesForm = [
    {
        PortraitUserFeaturesSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        PortraitUserFeaturesSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitUserFeaturesSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            type: 'selectDict',
            options: {
                groupCode: 'portrait_user_feature_tag_type'
            },
            required: true,
        }
    },

]
export default PortraitUserFeaturesForm