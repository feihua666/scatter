const basePath = '' + '/user/portrait-user-features'
const PortraitUserFeaturesUrl = {
    searchList: basePath + '/getPage',
    add: basePath,
    update: basePath,
    list: basePath + '/getList',
    delete: basePath + '/{id}',
    queryById: basePath + '/{id}',
    router: {
        searchList: '/PortraitUserFeaturesSearchList',
        add: '/PortraitUserFeaturesAdd',
        update: '/PortraitUserFeaturesUpdate',
    }
}
export default PortraitUserFeaturesUrl