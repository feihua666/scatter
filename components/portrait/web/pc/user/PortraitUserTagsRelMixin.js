import PortraitUserTagsRelForm from './PortraitUserTagsRelForm.js'
import PortraitUserTagsRelTable from './PortraitUserTagsRelTable.js'
import PortraitUserTagsRelUrl from './PortraitUserTagsRelUrl.js'
const PortraitUserTagsRelMixin = {
    computed: {
        PortraitUserTagsRelFormOptions() {
            return this.$stDynamicFormTools.options(PortraitUserTagsRelForm,this.$options.name)
        },
        PortraitUserTagsRelTableOptions() {
            return PortraitUserTagsRelTable
        },
        PortraitUserTagsRelUrl(){
            return PortraitUserTagsRelUrl
        }
    },
}
export default PortraitUserTagsRelMixin