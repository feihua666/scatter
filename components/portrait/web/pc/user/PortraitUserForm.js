import PortraitUserUrl from './PortraitUserUrl.js'
const PortraitUserForm = [
    {

        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
        }
    },
    {
        PortraitUserSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'registTypeDictId',
        },
        element:{
            label: '用户注册类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'regist_type'
            }
        }
    },
    {

        field: {
            name: 'imei',
        },
        element:{
            label: '设备串号',
        }
    },
    {

        field: {
            name: 'mobile',
        },
        element:{
            label: '手机号',
        }
    },
    {
        PortraitUserSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'genderDictId',
        },
        element:{
            label: '性别',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'gender'
            }
        }
    },
    {

        PortraitUserSearchList: false,
        field: {
            name: 'age',
            value: 10,
        },
        element:{
            type: 'inputNumber',
            label: '年龄',
        }
    },
    {

        field: {
            name: 'ageGroupDictId',
        },
        element:{
            label: '年龄段',
            type: 'selectDict',
            options: {
                groupCode: 'age_group'
            }

        }
    },
    {

        field: {
            name: 'country',
        },
        element:{
            label: '国家',
        }
    },
    {

        field: {
            name: 'city',
        },
        element:{
            label: '城市名称',
        }
    },
    {

        field: {
            name: 'deviceModelDictId',
        },
        element:{
            label: '设备型号',
            type: 'selectDict',
            options: {
                groupCode: 'device_model',
                groupType: 'groupItem'
            }
        }
    },
    {

        field: {
            name: 'sourceFromDictId',
        },
        element:{
            label: '用户来源',
            type: 'selectDict',
            options: {
                groupCode: 'user_source_from'
            }
        }
    },
    {

        PortraitUserSearchList: false,
        field: {
            name: 'description',
        },
        element:{
            label: '用户描述',
        }
    },
    {
        PortraitUserSearchList:{
            element: {
                type: "datepicker",
                options: {
                    type: 'datetimerange',
                    valueFormat: 'yyyy-MM-dd HH:mm:ss'
                }
            }
        },
        field: {
            name: 'registAt',
        },
        element:{
            label: '注册时间',
            type: "datetime"
        }
    },
    {
        PortraitUserSearchList: false,
        field: {
            name: 'lastLoginAt',
        },
        element:{
            label: '最后一次登录时间',
        }
    },
    {
        PortraitUserSearchList: false,
        field: {
            name: 'lastLoginIp',
        },
        element:{
            label: '最后一次登录ip',
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },

]
export default PortraitUserForm