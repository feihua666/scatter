import PortraitUserUrl from './PortraitUserUrl.js'

const PortraitUserRoute = [
    {
        path: PortraitUserUrl.router.searchList,
        component: () => import('./element/PortraitUserSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitUserSearchList',
            name: '画像用户管理'
        }
    },
    {
        path: PortraitUserUrl.router.add,
        component: () => import('./element/PortraitUserAdd'),
        meta: {
            code:'PortraitUserAdd',
            name: '画像用户添加'
        }
    },
    {
        path: PortraitUserUrl.router.update,
        component: () => import('./element/PortraitUserUpdate'),
        meta: {
            code:'PortraitUserUpdate',
            name: '画像用户修改'
        }
    },
]
export default PortraitUserRoute