import PortraitUserBehaviorUrl from './PortraitUserBehaviorUrl.js'

const PortraitUserBehaviorRoute = [
    {
        path: PortraitUserBehaviorUrl.router.searchList,
        component: () => import('./element/PortraitUserBehaviorSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitUserBehaviorSearchList',
            name: '用户行为管理'
        }
    },

]
export default PortraitUserBehaviorRoute