import PortraitUserTagsUrl from './PortraitUserTagsUrl.js'
const PortraitUserTagsForm = [
    {
        PortraitUserTagsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'code',
        },
        element:{
            label: '编码',
            required: true,
        }
    },
    {
        PortraitUserTagsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'name',
        },
        element:{
            label: '名称',
            required: true,
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitUserTagsSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'typeDictId',
        },
        element:{
            label: '类型',
            type: 'selectDict',
            options: {
                groupCode: 'portrait_user_feature_tag_type'
            },
            required: true,
        }
    },

]
export default PortraitUserTagsForm