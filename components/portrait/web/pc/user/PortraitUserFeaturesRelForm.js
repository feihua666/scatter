import PortraitUserFeaturesRelUrl from './PortraitUserFeaturesRelUrl.js'
import PortraitUserFeaturesUrl from './PortraitUserFeaturesUrl.js'
const PortraitUserFeaturesRelForm = [
    {
        PortraitUserFeaturesRelSearchList: {
            element:{
                required: false
            }
        },

        UserAssignUserFeaturesRel: false,
        UserFeaturesAssignUserRel: false,
        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
            required: true,
        }
    },
    {
        PortraitUserFeaturesRelSearchList: false,

        UserAssignUserFeaturesRel: false,
        UserFeaturesAssignUserRel: false,
        field: {
            name: 'userFeaturesId',
        },
        element:{
            label: '特征',
            type: 'select',
            options: {
                datas: PortraitUserFeaturesUrl.list
            },
            required: true,
        }
    },
    {

        UserAssignUserFeaturesRel: false,
        UserFeaturesAssignUserRel: false,
        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },
    {
        PortraitUserFeaturesRelSearchList: {
            element:{
                required: false
            }
        },

        UserAssignUserFeaturesRel: false,
        UserFeaturesAssignUserRel: false,
        PortraitUserFeaturesRelAdd: false,
        PortraitUserFeaturesRelUpdate: false,
        field: {
            name: 'sourceDictId',
        },
        element:{
            label: '来源标识',
            type: 'selectDict',
            options: {
                groupCode: 'feature_tag_source'
            },
            required: true,
        }
    },

    {
        PortraitUserFeaturesRelSearchList: false,
        PortraitUserFeaturesRelAdd: false,
        PortraitUserFeaturesRelUpdate: false,
        UserFeaturesAssignUserRel: false,

        field: {
            name: 'userId',
        },
        element:{
            label: '用户',
            required: true,
            type: 'select',
            readonly: true,
            options:  ({$route,$vm})=>{
                let datas = []
                if ($route.query.userId) {
                    datas.push({
                        id: $route.query.userId,
                        nickname: $route.query.userNickname || $route.query.userId
                    })
                }
                return {
                    datas: datas,
                    optionProp:{
                        value: 'id', // 选中的值属性
                        label: 'nickname', // 显示的值属性
                    },
                    originProp: {
                        placeholder: '请按用户昵称搜索',
                        remote: true,
                    },
                    // 因为本项目存在两个用户后端组件，这是可以通过全局配置的方式决定使用哪个用户体系
                    remoteUrl: $vm.$stObjectTools.getValue($vm.$scatterConfig,'PortraitUserFeaturesRelForm.userRemoteSearchUrl') || '/user-simple/getPage',
                    remoteQueryProp: 'nickname'
                }
            }

        }
    },
    {
        PortraitUserFeaturesRelSearchList: false,
        PortraitUserFeaturesRelAdd: false,
        PortraitUserFeaturesRelUpdate: false,
        UserFeaturesAssignUserRel: false,

        field: {
            name: 'checkedUserFeaturesIds',
            value: []
        },
        element:{
            label: '特征',
            type: 'tree',
            options: {
                datas: PortraitUserFeaturesUrl.list,
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
    {
        PortraitUserFeaturesRelSearchList: false,
        PortraitUserFeaturesRelAdd: false,
        PortraitUserFeaturesRelUpdate: false,
        UserAssignUserFeaturesRel: false,

        field: {
            name: 'userFeaturesId',
        },
        element:{
            label: '特征',
            required: true,
            type: 'select',
            readonly: true,
            options: {
                datas: [] // todo
            }

        }
    },
    {
        PortraitUserFeaturesRelSearchList: false,
        PortraitUserFeaturesRelAdd: false,
        PortraitUserFeaturesRelUpdate: false,
        UserAssignUserFeaturesRel: false,

        field: {
            name: 'checkedUserIds',
            value: []
        },
        element:{
            label: '用户',
            type: 'tree',
            options: {
                datas: [], // todo
                originProp:{
                    showCheckbox: true
                }
            }
        }
    },
]
export default PortraitUserFeaturesRelForm