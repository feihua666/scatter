import PortraitUserFeaturesRelUrl from './PortraitUserFeaturesRelUrl.js'

const PortraitUserFeaturesRelRoute = [
    {
        path: PortraitUserFeaturesRelUrl.router.searchList,
        component: () => import('./element/PortraitUserFeaturesRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitUserFeaturesRelSearchList',
            name: '用户和特征管理'
        }
    },
    {
        path: PortraitUserFeaturesRelUrl.router.add,
        component: () => import('./element/PortraitUserFeaturesRelAdd'),
        meta: {
            code:'PortraitUserFeaturesRelAdd',
            name: '用户和特征添加'
        }
    },
    {
        path: PortraitUserFeaturesRelUrl.router.update,
        component: () => import('./element/PortraitUserFeaturesRelUpdate'),
        meta: {
            code:'PortraitUserFeaturesRelUpdate',
            name: '用户和特征修改'
        }
    },
    {
        path: PortraitUserFeaturesRelUrl.router.userAssignUserFeaturesRel,
        component: () => import('./element/UserAssignUserFeaturesRel'),
        meta: {
            code:'UserAssignUserFeaturesRel',
            name: '用户分配特征'
        }
    },
    {
        path: PortraitUserFeaturesRelUrl.router.userFeaturesAssignUserRel,
        component: () => import('./element/UserFeaturesAssignUserRel'),
        meta: {
            code:'UserFeaturesAssignUserRel',
            name: '特征分配用户'
        }
    },
]
export default PortraitUserFeaturesRelRoute