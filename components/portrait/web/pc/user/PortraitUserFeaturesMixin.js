import PortraitUserFeaturesForm from './PortraitUserFeaturesForm.js'
import PortraitUserFeaturesTable from './PortraitUserFeaturesTable.js'
import PortraitUserFeaturesUrl from './PortraitUserFeaturesUrl.js'
const PortraitUserFeaturesMixin = {
    computed: {
        PortraitUserFeaturesFormOptions() {
            return this.$stDynamicFormTools.options(PortraitUserFeaturesForm,this.$options.name)
        },
        PortraitUserFeaturesTableOptions() {
            return PortraitUserFeaturesTable
        },
        PortraitUserFeaturesUrl(){
            return PortraitUserFeaturesUrl
        }
    },
}
export default PortraitUserFeaturesMixin