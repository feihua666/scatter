import PortraitUserTagsRelUrl from './PortraitUserTagsRelUrl.js'

const PortraitUserTagsRelRoute = [
    {
        path: PortraitUserTagsRelUrl.router.searchList,
        component: () => import('./element/PortraitUserTagsRelSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitUserTagsRelSearchList',
            name: '用户和标签管理'
        }
    },
    {
        path: PortraitUserTagsRelUrl.router.add,
        component: () => import('./element/PortraitUserTagsRelAdd'),
        meta: {
            code:'PortraitUserTagsRelAdd',
            name: '用户和标签添加'
        }
    },
    {
        path: PortraitUserTagsRelUrl.router.update,
        component: () => import('./element/PortraitUserTagsRelUpdate'),
        meta: {
            code:'PortraitUserTagsRelUpdate',
            name: '用户和标签修改'
        }
    },
    {
        path: PortraitUserTagsRelUrl.router.userAssignUserTagsRel,
        component: () => import('./element/UserAssignUserTagsRel'),
        meta: {
            code:'UserAssignUserTagsRel',
            name: '用户分配标签'
        }
    },
    {
        path: PortraitUserTagsRelUrl.router.userTagsAssignUserRel,
        component: () => import('./element/UserTagsAssignUserRel'),
        meta: {
            code:'UserTagsAssignUserRel',
            name: '标签分配用户'
        }
    },
]
export default PortraitUserTagsRelRoute