import PortraitUserBehaviorUrl from './PortraitUserBehaviorUrl.js'
const PortraitUserBehaviorForm = [
    {

        field: {
            name: 'userId',
        },
        element:{
            label: '用户id',
        }
    },
    {

        field: {
            name: 'imei',
        },
        element:{
            label: '设备串号',
        }
    },
    {
        PortraitUserBehaviorSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'itemId',
        },
        element:{
            label: 'Item_id',
            required: true,
        }
    },
    {
        PortraitUserBehaviorSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'itemTypeDictId',
        },
        element:{
            label: '商品类型',
            type: 'selectDict',
            options: {
                groupCode: 'portrait_item_type'
            },
            required: true,
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'itemPositionDictId',
        },
        element:{
            label: '商品位置',
            type: 'selectDict',
            options: {
                groupCode: 'portrait_item_position'
            }
        }
    },
    {
        PortraitUserBehaviorSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'behaviorTypeDictId',
        },
        element:{
            label: '行为类型',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'portrait_behavior_type'
            }
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'sceneId',
        },
        element:{
            label: '场景id',
        }
    },
    {
        PortraitUserBehaviorSearchList: false,

        field: {
            name: 'behaviorAt',
        },
        element:{
            label: '行为产生时间',
            required: true,
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'behaviorValue',
        },
        element:{
            label: '行为值',
        }
    },
    {
        PortraitUserBehaviorSearchList: {
            element:{
                required: false
            }
        },

        field: {
            name: 'clientPlatformDictId',
        },
        element:{
            label: '客户端平台',
            required: true,
            type: 'selectDict',
            options: {
                groupCode: 'client_platform'
            }
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'clientVersion',
        },
        element:{
            label: '客户端版本',
        }
    },
    {

        field: {
            name: 'netTypeDictId',
        },
        element:{
            label: '网络类型',
            type: 'selectDict',
            options: {
                groupCode: 'net_type'
            }
        }
    },
    {

        field: {
            name: 'deviceModelDictId',
        },
        element:{
            label: '设备型号',
            type: 'selectDict',
            options: {
                groupCode: 'device_model',
                groupType: 'groupItem'
            }
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'clientIp',
        },
        element:{
            label: '客户端ip',
        }
    },
    {
        PortraitUserBehaviorSearchList: false,

        field: {
            name: 'isLogin',
            value: false,
        },
        element:{
            type: 'switch',
            label: '用户是否登录',
            required: true,
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'moduleId',
        },
        element:{
            label: '模块信息id',
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'pageId',
        },
        element:{
            label: '页面信息id',
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'longitude',
        },
        element:{
            label: '位置经度',
        }
    },
    {
        PortraitUserBehaviorSearchList: false,
        field: {
            name: 'latitude',
        },
        element:{
            label: '位置纬度',
        }
    },
    {

        field: {
            name: 'tenantId',
        },
        element:{
            label: '租户id',
        }
    },

]
export default PortraitUserBehaviorForm