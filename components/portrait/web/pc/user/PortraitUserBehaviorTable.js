const PortraitUserBehaviorTable = [
    {
        type: 'expand'
    },
    {
        prop: 'userId',
        label: '用户id'
    },
    {
        prop: 'imei',
        label: '设备串号'
    },
    {
        prop: 'itemId',
        label: 'Item_id'
    },
    {
        prop: 'itemTypeDictId',
        label: '商品类型'
    },
    {
        prop: 'itemPositionDictId',
        label: '商品位置'
    },
    {
        prop: 'behaviorTypeDictId',
        label: '行为类型'
    },
    {
        prop: 'sceneId',
        label: '场景id',
        showInDetail: true
    },
    {
        prop: 'behaviorAt',
        label: '行为时间'
    },
    {
        prop: 'behaviorValue',
        label: '行为值'
    },
    {
        prop: 'clientPlatformDictId',
        label: '客户端平台'
    },
    {
        prop: 'clientVersion',
        label: '客户端版本'
    },
    {
        prop: 'netTypeDictId',
        label: '网络类型'
    },
    {
        prop: 'deviceModelDictId',
        label: '设备型号'
    },
    {
        prop: 'clientIp',
        label: '客户端ip'
    },
    {
        prop: 'isLogin',
        label: '是否登录'
    },
    {
        prop: 'moduleId',
        label: '模块信息id',
        showInDetail: true
    },
    {
        prop: 'pageId',
        label: '页面信息id',
        showInDetail: true
    },
    {
        prop: 'longitude',
        label: '位置经度',
        showInDetail: true
    },
    {
        prop: 'latitude',
        label: '位置纬度',
        showInDetail: true
    },
    {
        prop: 'tenantId',
        label: '租户id'
    },
]
export default PortraitUserBehaviorTable