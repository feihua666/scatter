import PortraitUserTagsUrl from './PortraitUserTagsUrl.js'

const PortraitUserTagsRoute = [
    {
        path: PortraitUserTagsUrl.router.searchList,
        component: () => import('./element/PortraitUserTagsSearchList'),
        meta: {
            root: true,
            keepAlive: true,
            code:'PortraitUserTagsSearchList',
            name: '用户标签字典管理'
        }
    },
    {
        path: PortraitUserTagsUrl.router.add,
        component: () => import('./element/PortraitUserTagsAdd'),
        meta: {
            code:'PortraitUserTagsAdd',
            name: '用户标签字典添加'
        }
    },
    {
        path: PortraitUserTagsUrl.router.update,
        component: () => import('./element/PortraitUserTagsUpdate'),
        meta: {
            code:'PortraitUserTagsUpdate',
            name: '用户标签字典修改'
        }
    },
]
export default PortraitUserTagsRoute