package scatter.portrait.rest.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.rest.PortraitConfiguration;
import scatter.portrait.pojo.user.po.PortraitUserTagsRel;
import scatter.portrait.rest.user.service.IPortraitUserTagsRelService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * 用户和标签关系表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/inner/user/portrait-user-tags-rel")
public class PortraitUserTagsRelInnerController extends BaseInnerController<PortraitUserTagsRel> {
 @Autowired
 private IPortraitUserTagsRelService portraitUserTagsRelService;

 public IPortraitUserTagsRelService getService(){
     return portraitUserTagsRelService;
 }
}
