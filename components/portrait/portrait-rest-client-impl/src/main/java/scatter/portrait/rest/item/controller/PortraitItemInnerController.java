package scatter.portrait.rest.item.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import scatter.portrait.rest.PortraitConfiguration;
import scatter.portrait.pojo.item.po.PortraitItem;
import scatter.portrait.rest.item.service.IPortraitItemService;
import scatter.common.rest.controller.BaseInnerController;

import org.springframework.web.bind.annotation.RestController;
import scatter.common.rest.controller.BaseAddUpdateQueryFormController;

/**
 * <p>
 * item表 内部调用前端控制器
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@RestController
@RequestMapping(PortraitConfiguration.CONTROLLER_BASE_PATH + "/inner/item/portrait-item")
public class PortraitItemInnerController extends BaseInnerController<PortraitItem> {
 @Autowired
 private IPortraitItemService portraitItemService;

 public IPortraitItemService getService(){
     return portraitItemService;
 }
}
