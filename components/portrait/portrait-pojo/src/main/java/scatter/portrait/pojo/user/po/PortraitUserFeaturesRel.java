package scatter.portrait.pojo.user.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;

/**
 * <p>
 * 用户和特征关系表
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_portrait_user_features_rel")
@ApiModel(value="PortraitUserFeaturesRel对象", description="用户和特征关系表")
public class PortraitUserFeaturesRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_PORTRAITUSERFEATURESREL_BY_ID = "trans_portraituserfeaturesrel_by_id_scatter.portrait.pojo.user.po";

    @ApiModelProperty(value = "用户id，外键")
    private String userId;

    @ApiModelProperty(value = "特征id，外键")
    private String userFeaturesId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源")
    private String sourceDictId;

}
