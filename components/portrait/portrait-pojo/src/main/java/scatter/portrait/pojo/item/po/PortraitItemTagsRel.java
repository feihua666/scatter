package scatter.portrait.pojo.item.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * item和标签关系表
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_portrait_item_tags_rel")
@ApiModel(value="PortraitItemTagsRel对象", description="item和标签关系表")
public class PortraitItemTagsRel extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_PORTRAITITEMTAGSREL_BY_ID = "trans_portraititemtagsrel_by_id_scatter.portrait.pojo.item.po";

    @ApiModelProperty(value = "item id，外键")
    private String itemId;

    @ApiModelProperty(value = "item标签id，外键")
    private String itemTagsId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源")
    private String sourceDictId;


}
