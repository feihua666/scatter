package scatter.portrait.pojo.item.form;

import scatter.common.pojo.form.BaseAddForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * Item标签字典添加表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="Item标签字典添加表单对象")
public class PortraitItemTagsAddForm extends BaseAddForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码",required = true)
    private String code;

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String name;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典id",required = true)
    private String typeDictId;

}
