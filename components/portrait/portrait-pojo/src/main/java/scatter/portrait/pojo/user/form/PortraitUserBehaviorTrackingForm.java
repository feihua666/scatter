package scatter.portrait.pojo.user.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.form.BaseAddForm;
import scatter.common.rest.validation.props.PropValid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户行为埋点表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="用户行为埋点表单对象")
public class PortraitUserBehaviorTrackingForm extends BaseAddForm {

    @ApiModelProperty(value = "用户id，已注册用户必填")
    private String userId;

    @ApiModelProperty(value = "设备串号，安卓:（imei），IOS:（idfa），未注册用户必填")
    private String imei;

    @NotEmpty(message="Item_id不能为空")
    @ApiModelProperty(value = "Item_id,商品id",required = true)
    private String itemId;

    @PropValid.Depend(message = "商品类型字典值或id至少填一个",dependProp = "itemTypeDictValue",dependPropHasValue = false)
    @ApiModelProperty(value = "商品类型，字典id，视频、图片等类型")
    private String itemTypeDictId;

    @PropValid.Depend(message = "商品类型字典值或id至少填一个",dependProp = "itemTypeDictId",dependPropHasValue = false)
    @ApiModelProperty(value = "商品类型，字典编码，视频、图片等类型，itemTypeDictId 二选1")
    private String itemTypeDictValue;

    @PropValid.Depend(message = "商品位置字典值或id至少填一个",dependProp = "itemPositionDictValue",dependPropHasValue = false)
    @ApiModelProperty(value = "商品位置，字典id")
    private String itemPositionDictId;

    @PropValid.Depend(message = "商品位置字典值或id至少填一个",dependProp = "itemPositionDictId",dependPropHasValue = false)
    @ApiModelProperty(value = "商品位置，字典id,itemPositionDictId 二选1")
    private String itemPositionDictValue;

    @PropValid.Depend(message = "行为类型字典值或id至少填一个",dependProp = "behaviorTypeDictValue",dependPropHasValue = false)
    @ApiModelProperty(value = "行为类型，字典id，行为类型，例如曝光、停留、点击、收藏、下载、购买、加购、评价等")
    private String behaviorTypeDictId;

    @PropValid.Depend(message = "行为类型字典值或id至少填一个",dependProp = "behaviorTypeDictId",dependPropHasValue = false)
    @ApiModelProperty(value = "行为类型，字典 值，行为类型，例如曝光、停留、点击、收藏、下载、购买、加购、评价等，behaviorTypeDictId 二选1")
    private String behaviorTypeDictValue;

    @ApiModelProperty(value = "场景id，1、用户产生行为的场景，在对应的item的场景ID中，单值. 2、行为表的scene_id要包含于item表中的scene_id。")
    private String sceneId;

    @ApiModelProperty(value = "行为产生时间, 无值系统默认生成当前时间")
    private LocalDateTime behaviorAt;

    @ApiModelProperty(value = "行为值，例如点击次数，停留时长，购买件数、购买金额等")
    private String behaviorValue;

    @PropValid.Depend(message = "客户端平台字典值或id至少填一个",dependProp = "clientPlatformDictValue",dependPropHasValue = false)
    @ApiModelProperty(value = "客户端平台，字典id，ios/andriod/h5/pc")
    private String clientPlatformDictId;

    @PropValid.Depend(message = "客户端平台字典值或id至少填一个",dependProp = "clientPlatformDictId",dependPropHasValue = false)
    @ApiModelProperty(value = "客户端平台，字典id，ios/andriod/h5/pc，clientPlatformDictId 二选1")
    private String clientPlatformDictValue;

    @ApiModelProperty(value = "客户端版本")
    private String clientVersion;

    @PropValid.Depend(message = "网络类型字典值或id至少填一个",dependProp = "netTypeDictValue",dependPropHasValue = false)
    @ApiModelProperty(value = "网络类型，字典id，2G/3G/4G/WIFI")
    private String netTypeDictId;

    @PropValid.Depend(message = "网络类型字典值或id至少填一个",dependProp = "netTypeDictId",dependPropHasValue = false)
    @ApiModelProperty(value = "网络类型，字典id，2G/3G/4G/WIFI")
    private String netTypeDictValue;

    @PropValid.Depend(message = "设备型号字典值或id至少填一个",dependProp = "deviceModelDictValue",dependPropHasValue = false)
    @ApiModelProperty(value = "设备型号，如：iphoneX，字典id")
    private String deviceModelDictId;

    @PropValid.Depend(message = "设备型号字典值或id至少填一个",dependProp = "deviceModelDictId",dependPropHasValue = false)
    @ApiModelProperty(value = "设备型号，如：iphoneX，字典id")
    private String deviceModelDictValue;

    @ApiModelProperty(value = "客户端ip",hidden = true)
    private String clientIp;

    @ApiModelProperty(value = "模块信息id，目前不佣对应的表可以用数据替代")
    private String moduleId;

    @ApiModelProperty(value = "页面信息id，目前没有对应的表可以用数据替代")
    private String pageId;

    @ApiModelProperty(value = "位置经度")
    private String longitude;

    @ApiModelProperty(value = "位置纬度")
    private String latitude;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
