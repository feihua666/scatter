package scatter.portrait.pojo.item.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * Item特征字典分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="Item特征字典分页表单对象")
public class PortraitItemFeaturesPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;

}
