package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 画像用户分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="画像用户分页表单对象")
public class PortraitUserPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id，已注册用户必填")
    private String userId;

    @ApiModelProperty(value = "用户注册类型,如：app注册账号，手机号等，字典id")
    private String registTypeDictId;

    @ApiModelProperty(value = "设备串号，安卓:（imei），IOS:（idfa），未注册用户必填")
    private String imei;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "年龄段，如：20-25，字典id")
    private String ageGroupDictId;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "城市名称")
    private String city;

    @ApiModelProperty(value = "设备型号，如：iphoneX，字典id")
    private String deviceModelDictId;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

    @ApiModelProperty(value = "注册时间")
    private LocalDateTime registAt;

    @Gt(value = "registAt",eq = true)
    @ApiModelProperty(value = "注册时间开始")
    private LocalDateTime registAtStart;

    @Lt(value = "registAt",eq = true)
    @ApiModelProperty(value = "注册时间结束")
    private LocalDateTime registAtEnd;


    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
