package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
/**
 * <p>
 * 用户分配特征表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="用户分配特征表单对象")
public class UserAssignUserFeaturesForm extends BaseForm {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "用户id不能为空")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "选择的特征id")
    private List<String> checkedUserFeaturesIds;

    @PropValid.DependCondition(message = "未选择的特征id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的特征id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedUserFeaturesIds;


    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
