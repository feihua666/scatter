package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户和标签更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Setter
@Getter
@ApiModel(value="用户和标签更新表单对象")
public class PortraitUserTagsRelUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="用户id不能为空")
    @ApiModelProperty(value = "用户id，外键")
    private String userId;

    @NotEmpty(message="用户标签id不能为空")
    @ApiModelProperty(value = "用户标签id，外键",required = true)
    private String userTagsId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源",hidden = true)
    private String sourceDictId;

}
