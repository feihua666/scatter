package scatter.portrait.pojo.item.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * item分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="item分页表单对象")
public class PortraitItemPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "Item_id,商品id")
    private String itemId;

    @ApiModelProperty(value = "商品类型，字典id，视频、图片等类型")
    private String itemTypeDictId;

    @ApiModelProperty(value = "是否有效，有效可以被推荐")
    private Boolean isValid;

    @ApiModelProperty(value = "场景id，物品投放的不同地方，可以理解成是对物品的一个分类的id。不同的页面，用户访问类型不一样，都可以做成不同的场景id。")
    private String sceneId;

    @ApiModelProperty(value = "视频时长，单位秒，视频必填")
    private Integer videoDuration;

    @ApiModelProperty(value = "发布时间，用于判断内容是否为最新发布或商品是否为新品")
    private LocalDateTime publishAt;

    @ApiModelProperty(value = "失效时间，失效不会被推荐出来")
    private LocalDateTime expireAt;

    @ApiModelProperty(value = "最后更新时间，如果有大量更新可以更新该字段，权重会增加")
    private LocalDateTime lastModifyAt;

    @ApiModelProperty(value = "标题，用于语义层面深入分析，置空会损失部分算法效果")
    private String title;

    @ApiModelProperty(value = "item的加权。注意： 1、要加权的商品填：100、不加权的商品填：1。 2、仅支持填入100或1，其他值是非法的。 3、建议加权的item数量<=item总数的10%。 ")
    private Integer weight;

    @ApiModelProperty(value = "类目层级数，例如3级类目")
    private Integer categoryLevel;

    @ApiModelProperty(value = "类目路径，下划线联接")
    private String categoryPath;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "内容摘要，通常为正文关键片段，最长5000个中英文字符，主要用于语义分析")
    private String content;

    @ApiModelProperty(value = "一个月内爆光次数")
    private Integer exposeCount;

    @ApiModelProperty(value = "一个月内点击次数")
    private Integer clickCount;

    @ApiModelProperty(value = "一个月内点赞次数")
    private Integer likeCount;

    @ApiModelProperty(value = "一个月内踩次数")
    private Integer unlikeCount;

    @ApiModelProperty(value = "一个月内评论次数")
    private Integer commentCount;

    @ApiModelProperty(value = "一个月内收藏次数")
    private Integer collectCount;

    @ApiModelProperty(value = "一个月内分享次数")
    private Integer shareCount;

    @ApiModelProperty(value = "一个月内下载次数")
    private Integer downloadCount;

    @ApiModelProperty(value = "一个月内打赏次数")
    private Integer tipCount;

    @ApiModelProperty(value = "一个月内关注次数")
    private Integer subscribeCount;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "城市名称")
    private String city;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
