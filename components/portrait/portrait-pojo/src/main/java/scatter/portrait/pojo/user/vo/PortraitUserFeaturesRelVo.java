package scatter.portrait.pojo.user.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.portrait.pojo.user.po.PortraitUserFeatures;


/**
 * <p>
 * 用户和特征响应对象
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Setter
@Getter
@ApiModel(value="用户和特征响应对象")
public class PortraitUserFeaturesRelVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id，外键")
    private String userId;

    @ApiModelProperty(value = "特征id，外键")
    private String userFeaturesId;

    @TransBy(type = PortraitUserFeatures.TRANS_PORTRAITUSERFEATURES_BY_ID,byFieldName = "userFeaturesId",mapValueField = "name")
    @ApiModelProperty(value = "特征名称，外键")
    private String userFeaturesName;


    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源")
    private String sourceDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceDictId",mapValueField = "name")
    @ApiModelProperty(value = "来源标识，字典名称，用来标识该关系来源")
    private String sourceDictName;
}
