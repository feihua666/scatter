package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户行为分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="用户行为分页表单对象")
public class PortraitUserBehaviorPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id，已注册用户必填")
    private String userId;

    @ApiModelProperty(value = "设备串号，安卓:（imei），IOS:（idfa），未注册用户必填")
    private String imei;

    @ApiModelProperty(value = "Item_id,商品id")
    private String itemId;

    @ApiModelProperty(value = "商品类型，字典id，视频、图片等类型")
    private String itemTypeDictId;

    @ApiModelProperty(value = "商品位置，字典id")
    private String itemPositionDictId;

    @ApiModelProperty(value = "行为类型，字典id，行为类型，例如曝光、停留、点击、收藏、下载、购买、加购、评价等")
    private String behaviorTypeDictId;

    @ApiModelProperty(value = "场景id，1、用户产生行为的场景，在对应的item的场景ID中，单值. 2、行为表的scene_id要包含于item表中的scene_id。")
    private String sceneId;

    @ApiModelProperty(value = "行为产生时间")
    private LocalDateTime behaviorAt;

    @ApiModelProperty(value = "行为值，例如点击次数，停留时长，购买件数、购买金额等")
    private String behaviorValue;

    @ApiModelProperty(value = "客户端平台，字典id，ios/andriod/h5/pc")
    private String clientPlatformDictId;

    @ApiModelProperty(value = "客户端版本")
    private String clientVersion;

    @ApiModelProperty(value = "网络类型，字典id，2G/3G/4G/WIFI")
    private String netTypeDictId;

    @ApiModelProperty(value = "设备型号，如：iphoneX，字典id")
    private String deviceModelDictId;

    @ApiModelProperty(value = "客户端ip")
    private String clientIp;

    @ApiModelProperty(value = "用户是否登录")
    private Boolean isLogin;

    @ApiModelProperty(value = "模块信息id，目前不佣对应的表可以用数据替代")
    private String moduleId;

    @ApiModelProperty(value = "页面信息id，目前没有对应的表可以用数据替代")
    private String pageId;

    @ApiModelProperty(value = "位置经度")
    private String longitude;

    @ApiModelProperty(value = "位置纬度")
    private String latitude;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
