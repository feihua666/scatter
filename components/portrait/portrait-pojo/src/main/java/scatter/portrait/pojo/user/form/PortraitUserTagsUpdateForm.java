package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户标签字典更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="用户标签字典更新表单对象")
public class PortraitUserTagsUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="编码不能为空")
    @ApiModelProperty(value = "编码",required = true)
    private String code;

    @NotEmpty(message="名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String name;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @NotEmpty(message="类型不能为空")
    @ApiModelProperty(value = "类型，字典id",required = true)
    private String typeDictId;

}
