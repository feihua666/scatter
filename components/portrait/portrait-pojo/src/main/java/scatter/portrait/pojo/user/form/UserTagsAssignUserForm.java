package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.BaseForm;
import scatter.common.rest.validation.props.PropValid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
/**
 * <p>
 * 用户分配标签表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@PropValid
@Setter
@Getter
@ApiModel(value="标签分配用户表单对象")
public class UserTagsAssignUserForm extends BaseForm {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "选择的用户id")
    private List<String> checkedUserIds;

    @PropValid.DependCondition(message = "未选择的用户id不能为空",dependProp = "isLazyLoad",ifEqual = "true")
    @ApiModelProperty(value = "未选择的用户id",notes = "如果为懒加载请传该值")
    private List<String> uncheckedUserIds;

    @NotEmpty(message = "用户标签id不能为空")
    @ApiModelProperty(value = "用户标签id")
    private String userTagsId;

    @ApiModelProperty(value = "页面可选择的数据是否为懒加载")
    private Boolean isLazyLoad = false;
}
