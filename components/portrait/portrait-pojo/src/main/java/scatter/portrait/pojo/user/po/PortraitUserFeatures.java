package scatter.portrait.pojo.user.po;

import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户特征字典表
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_portrait_user_features")
@ApiModel(value="PortraitUserFeatures对象", description="用户特征字典表")
public class PortraitUserFeatures extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_PORTRAITUSERFEATURES_BY_ID = "trans_portraituserfeatures_by_id_scatter.portrait.pojo.user.po";

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "类型，字典id")
    private String typeDictId;


}
