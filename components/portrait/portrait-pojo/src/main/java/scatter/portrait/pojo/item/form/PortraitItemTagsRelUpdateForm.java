package scatter.portrait.pojo.item.form;

import scatter.common.pojo.form.BaseUpdateIdForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * item和标签更新表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="item和标签更新表单对象")
public class PortraitItemTagsRelUpdateForm extends BaseUpdateIdForm {

    @NotEmpty(message="item id不能为空")
    @ApiModelProperty(value = "item id，外键",required = true)
    private String itemId;

    @NotEmpty(message="item标签id不能为空")
    @ApiModelProperty(value = "item标签id，外键",required = true)
    private String itemTagsId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源",hidden = true)
    private String sourceDictId;

}
