package scatter.portrait.pojo.user.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 画像用户响应对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="画像用户响应对象")
public class PortraitUserVo extends BaseIdVo {

    @ApiModelProperty(value = "用户id，已注册用户必填")
    private String userId;

    @ApiModelProperty(value = "用户注册类型,如：app注册账号，手机号等，字典id")
    private String registTypeDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "registTypeDictId",mapValueField = "name")
    @ApiModelProperty(value = "用户注册类型,如：app注册账号，手机号等，字典名称")
    private String registTypeDictName;


    @ApiModelProperty(value = "设备串号，安卓:（imei），IOS:（idfa），未注册用户必填")
    private String imei;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "genderDictId",mapValueField = "name")
    @ApiModelProperty(value = "性别，字典名称")
    private String genderDictName;


    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "年龄段，如：20-25，字典id")
    private String ageGroupDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "ageGroupDictId",mapValueField = "name")
    @ApiModelProperty(value = "年龄段，如：20-25，字典名称")
    private String ageGroupDictName;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "城市名称")
    private String city;

    @ApiModelProperty(value = "设备型号，如：iphoneX，字典id")
    private String deviceModelDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "deviceModelDictId",mapValueField = "name")
    @ApiModelProperty(value = "设备型号，如：iphoneX，字典名称")
    private String deviceModelDictName;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceFromDictId",mapValueField = "name")
    @ApiModelProperty(value = "用户来源，字典名称")
    private String sourceFromDictName;

    @ApiModelProperty(value = "用户描述")
    private String description;

    @ApiModelProperty(value = "注册时间")
    private LocalDateTime registAt;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDateTime lastLoginAt;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

}
