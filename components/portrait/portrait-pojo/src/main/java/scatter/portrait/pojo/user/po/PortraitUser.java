package scatter.portrait.pojo.user.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;

import scatter.common.dict.IDictGroup;
import scatter.common.dict.IDictItem;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 画像用户表
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("component_portrait_user")
@ApiModel(value="PortraitUser对象", description="画像用户表")
public class PortraitUser extends BasePo {

    private static final long serialVersionUID = 1L;
    public static final String TRANS_PORTRAITUSER_BY_ID = "trans_portraituser_by_id_scatter.portrait.pojo.user.po";


    /**
     * 特征标签来源
     */
    public enum FeatureTagSourceGroupCode implements IDictGroup {
        feature_tag_source;

        @Override
        public String groupCode() {
            return this.name();
        }
    }

    /**
     * 特征标签来源字典项
     */
    public enum FeatureTagSourceDictItem implements IDictItem {
        manual,     // 手动添加
        auto,     // 机器学习自动添加

        ;

        @Override
        public String itemValue() {
            return this.name();
        }

        @Override
        public String groupCode() {
            return FeatureTagSourceGroupCode.feature_tag_source.groupCode();
        }
    }

    @ApiModelProperty(value = "用户id，已注册用户必填")
    private String userId;

    @ApiModelProperty(value = "用户注册类型,如：app注册账号，手机号等，字典id")
    private String registTypeDictId;

    @ApiModelProperty(value = "设备串号，安卓:（imei），IOS:（idfa），未注册用户必填")
    private String imei;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "性别，字典id")
    private String genderDictId;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "年龄段，如：20-25，字典id")
    private String ageGroupDictId;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "城市名称")
    private String city;

    @ApiModelProperty(value = "设备型号，如：iphoneX，字典id")
    private String deviceModelDictId;

    @ApiModelProperty(value = "用户来源，字典id")
    private String sourceFromDictId;

    @ApiModelProperty(value = "用户描述")
    private String description;

    @ApiModelProperty(value = "注册时间")
    private LocalDateTime registAt;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDateTime lastLoginAt;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


}
