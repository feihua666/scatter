package scatter.portrait.pojo.item.vo;

import scatter.common.pojo.vo.BaseIdVo;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;
import scatter.portrait.pojo.item.po.PortraitItemFeatures;


/**
 * <p>
 * item和特征响应对象
 * </p>
 *
 * @author yw
 * @since 2021-10-09
 */
@Setter
@Getter
@ApiModel(value="item和特征响应对象")
public class PortraitItemFeaturesRelVo extends BaseIdVo {

    @ApiModelProperty(value = "item id，外键")
    private String itemId;

    @ApiModelProperty(value = "item特征id，外键")
    private String itemFeaturesId;

    @TransBy(type = PortraitItemFeatures.TRANS_PORTRAITITEMFEATURES_BY_ID,byFieldName = "itemFeaturesId",mapValueField = "name")
    @ApiModelProperty(value = "item特征名称，外键")
    private String itemFeaturesName;


    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源")
    private String sourceDictId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "sourceDictId",mapValueField = "name")
    @ApiModelProperty(value = "来源标识，字典名称，用来标识该关系来源")
    private String sourceDictName;
}
