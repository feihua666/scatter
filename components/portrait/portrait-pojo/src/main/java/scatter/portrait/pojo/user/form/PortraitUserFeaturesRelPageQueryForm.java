package scatter.portrait.pojo.user.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BasePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 用户和特征分页表单对象
 * </p>
 *
 * @author yw
 * @since 2021-10-14
 */
@Setter
@Getter
@ApiModel(value="用户和特征分页表单对象")
public class PortraitUserFeaturesRelPageQueryForm extends BasePageQueryForm {

    @ApiModelProperty(value = "用户id，外键")
    private String userId;

    @ApiModelProperty(value = "特征id，外键")
    private String userFeaturesId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "来源标识，用来标识该关系来源")
    private String sourceDictId;

}
