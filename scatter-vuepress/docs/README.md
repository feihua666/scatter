---
home: true
heroImage: /scatter-logo.png
heroText: Scatter
tagline: 你要的轮子就在这里
actionText: 快速上手 →
actionLink: /guide/
features:
- title: 解藕
  details: 各个组件独立开发，除了基础公共部分，相互独立。
- title: 贴近具体功能
  details: 每个组件是单个具体功能的实现CRUD。
- title: 可重用
  details: 组件可搭配组装，组成模块或项目。
footer: MIT Licensed | Copyright © 2018-present Evan You
---
