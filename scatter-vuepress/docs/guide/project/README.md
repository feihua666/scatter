## 项目目录结构
Scatter 整体目录结构分为三大部分 components,modules和其它 
```
├── scatter
│   ├── components (可复用的组件根目录)
│   │── modules (可执行的服务模块根目录)
│   │── scatter-vuepress (本文档vuepress项目 属于其它)
│   │── tools (工具目录 属于其它)
│   ├── pom.xml (scatter pom.xml)
```

## 后台组件目录结构
下面以dict(字典组件)为列进行说明  
```
├── dict (dict组件根目录  maven module)
│   ├── dict-pojo (dict实体 maven module)
│   │── dict-rest (mvc功能处理  maven module)
│   │── dict-rest-client (spring cloud feign client  maven module)
│   │── dict-rest-client-impl (spring cloud feign client 实现模块 主要用于将微服务合并为单体项目使用 maven module)
│   │── web (前端目录)
│   │   │── pc (pc使用的目录)
│   │   │   │── element (基本element ui 实现的前端组件目录)
│   │   │   │── DictForm.js (pc通用js文件)
│   │   │   │── ... 
│   │── pom.xml (dict pom.xml)

```

