# 前端通用基础组件
位置：scatter/components/common/web  
scatter项目针对前端封装了部分组件以方便使用，主要分为以下几大部分：  
+ 基于element ui的封装，对element ui的封装都是以STEl开头 位置：components/common/web/components/pc/element
+ 常用前端工具集合 位置：components/common/web/tools
+ 通用的前端组件（仅依赖vue） 位置：components/common/web/components/pc/common

## 动态表单
StElDynamicForm.vue封装了大部分的表单项，以json的参数形式进行配置可以实现绝大部分表单  
如果我们有一个用户查询的表单需求，只需要配置如下：  

<<< @/docs/.vuepress/components/StElDynamicFormUserDemo.vue

会渲染出如下表单：  

<StElDynamicFormUserDemo></StElDynamicFormUserDemo>

::: tip
动态表单支持表单验证的响应绑定、支持自定义插槽、支持布局，更多使用方式可以查看源码或其它组件的使用  
:::

## 动态表单示例  

<StElDynamicFormDemo></StElDynamicFormDemo>

::: details 动态表单示例代码
<<< @/docs/.vuepress/components/StElDynamicFormDemo.vue
:::

## 动态表单布局参数
动态表单布局依赖于element ui的 Layout 布局，所有的值的定义都会转化为 Layout 布局的span  

参数名称：layout  

值类型为数字或数组  

| layout值类型   | layout值示例    | 布局结果  | 备注  |
| ------------- | -------------  | -----    | ----- |
| 数字 | 1           | 每行1个表单项  | |
| 数字 | 3           | 每行3个表单项（默认） | |
| 数组 | [2,3]       | 第1行2个表单项，第2行3个表单项 | 如果实际表单项多于5个，多余的默认每行1个表单项   | |
| 数组 | [2,3,5]     | 第1行2个表单项，第2行3个表单项，第3行5个表单项 | 如果实际表单项少于10个，将会报错，配置布局最好按实际表单项数量配置 |
| 数组 | [2,3,5]     | 第1行2个表单项，第2行3个表单项，第3行5个表单项 | 如果实际表单项少于10个，将会报错，配置布局最好按实际表单项数量配置 |
| 数组 | [2,3,[8,8]] | 第1行2个表单项，第2行3个表单项，第3行2个表单项并且每一个表单项占一个span=8 | 嵌套的数组长度是表单项的个数，数组元素是span的值 |

## 表格

StElTable.vue封装了一些常用的列表功能，以json的参数形式进行配置可以实现简单的列表需求    

## 表格示例

<StElTableDemo></StElTableDemo>

::: details 表格示例代码
<<< @/docs/.vuepress/components/StElTableDemo.vue
:::

::: tip
表格封装支持分页、支持自定义插槽、支持内容自定义格式化，更多使用方式可以查看源码或其它组件的使用  
:::

