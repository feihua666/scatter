# 后端组件的使用
这里仅说明以代码生成器生成的组件使用方式，因不同的组件实现方式不一样可能使用方式也不一样  
用代码生成器生成的组件一般会生成一个xxx-rest模块，其包含如下代码或文件，以banner组件为例：  
+ springboot 配置类，如banner组件的配置类：scatter.banner.rest.BannerConfiguration
+ 数据库建表语句 db/xxx-schema.sql 如：db/banner-schema.sql
+ 组件的初始化数据sql db/xxx-data.sql 如：db/banner-data.sql

对于熟悉springboot的同学肯定会知道只需要在使用banner的模块/项目中引入如下依赖：  
```xml
<dependency>
    <groupId>scatter</groupId>
    <artifactId>banner-rest</artifactId>
    <version>${scatter.version}</version>
<!-- <version>${project.version}</version> -->
</dependency>
```
然后在配置类中引入 scatter.banner.rest.BannerConfiguration即可  
```java
@Import({CommonRestConfig.class, BannerConfiguration.class})
```
如果需要在模块/项目启动的时候自动初始化表，需要添加如下配置：  
```yaml
spring:
  datasource:
    schema:
      - classpath:db/banner-schema.sql
    data:
      - classpath:db/banner-data.sql
    initialization-mode: ALWAYS #注意这里是ALWAYS
```
::: tip
将组件封装成springboot starter，自动配置会更加方便，但显示配置更直观
:::