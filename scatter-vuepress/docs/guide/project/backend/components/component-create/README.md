# 后端自定义组件
当我们在开发中，我们可以考虑将需求中的一小部分拆分出来做为公共部分存在，如果可以将一个组件抽出来做为可重复使用的通用组件，其通常业务紧密度不高  
比如app的版本管理，做为一个能用组件一般情况下也挺合适的  
这样做的好处是，如果我们再有这样的其它项目的需求，则可以很快的重复利用！  

## 组件代码生成
::: tip 建议
建议使用代码生成器来生成新的组件，在生成的基础上进行细节完善，关于组件代码生成一节参考 [快速上手](/guide/quick/quick-start)
:::

## crud代码生成
好，假设我们已经生成了一个组件如：app-version(app版本管理)，接下来需要先建表，因为接下来需要根据表来生成crud的代码  
::: warning
注意，因为用代码生成器生成基础的crud代码，其po的基类是 BasePo，所以在建表时，必须存在的字段为id，version,create_at,create_by,update_at,update_by
:::
BasePo代码内容如下：  
```java
package scatter.common.pojo.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;
import scatter.common.pojo.BasePojo;

import java.time.LocalDateTime;

/**
 * 所有数据库实体基类
 * Created by yangwei
 * Created at 2020/10/27 13:13
 */
@Getter
@Setter
public class BasePo extends BasePojo {

    public static final String PROPERTY_ID = "id";
    public static final String COLUMN_ID = "id";
    public static final String PROPERTY_CREATE_AT = "createAt";
    public static final String PROPERTY_CREATE_BY = "createBy";
    public static final String PROPERTY_UPDATE_AT = "updateAt";
    public static final String PROPERTY_UPDATE_BY = "updateBy";
    public static final String PROPERTY_VERSION = "version";
    // 初始化版本
    public static final int INIT_VERSION = 1;

    @TableId(type = IdType.ASSIGN_ID)
    private String id;


    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateAt;

    /**
     * 修改人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 乐观锁字段
     */
    @Version
    @TableField(fill = FieldFill.INSERT)
    private Integer version;
}

```

基本建表语句如下：  
```sql
DROP TABLE IF EXISTS component_app_version;
CREATE TABLE `component_app_version` (
  `id` varchar(20) NOT NULL COMMENT '表主键',

   /*...*/

  `version` int(11) NOT NULL COMMENT '乐观锁字段',
  `create_at` datetime NOT NULL COMMENT '创建时间的时间戳',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间的时间戳',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='app版本管理表';

```

建好表后，和模块生成一样，依然有两种生成方式  
+ 通过代码生成器生成
代码生成器位置：scatter.generator.rest.test.components.gen.ComponentTableGenerator  
请先找到 最下面的数据源配置以配置数据源,表配置如下：  
```java
String tableNames = "scatter_generator_table_gen_history";

String position = "modules"; // modules components
String tablePrefix = "scatter_generator";
String moduleName = "history";
String componentName = "scatter-generator";
TableType tableType = TableType.normal;
boolean fileOverride = true;
boolean deleteFile = false;
```
+ 通过模块代码服务生成
服务启动参见 快速上手 通过模块服务生成器生成部分  

点击导航的数据源配置，来添加一个数据源，不再详述  
添加完数据源，点击代码生成，如下  
![代码生成](/guide/project/component-create-generate.png)
代码生成分为四列，  
在 表信息列 输入表名并选择配置的数据源，点击下面的加载表信息按钮可以将表信息加载到字段信息  
可以对表信息做适当的修改  
代码生成列 输入作者 表前缀 组件名 模块名 位置 表类型点击代码生成即可
::: tip
导航的[参数配置]暂时没用到,模板配置不到修改和删除
:::

::: tip
在建表时，表注释、列注释（如：模糊查询、排序、外键等关键字）、和唯一索引都会影响代码的生成  
建议通过代码服务可视化生成
:::
## 生成后完善
在代码生成后，除了生成后端代码还生成了pc前端代码  
要做的其实基本上就是修改添加、修改、查询的表单字段和表单验证，不再详述    

## 代码生成表类型说明
表类型分三种
+ 常规表 常规表即一般类型的表，其实体基类为BasePo
+ 树表 即表示树的表，典型的例子为部门表，其含有parentId,在scatter项目中树表基类为BaseTreePo  
除了parentId还添加了level，parent{x（x取值范围为1-10）}，parent{x}为冗余字段
，parent1表示level为1的父级id，parent2表示level为2的父级id，依此类推，这样做的好处是在查询父级和子级时在不使用递归的情况下可以方便的查询某一个节点的任一个父级和任一层子级
+ 关系表 即两个实体的关系，如：用户和角色关系表，scatter中关系表都是以rel结尾 其实体基类为BasePo