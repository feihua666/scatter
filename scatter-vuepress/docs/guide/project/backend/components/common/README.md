# 后端通用基础组件
位置：scatter/components/common  
通用基础组件是所有组件或模块的基础依赖组件，其提供了基础的模板类和通用配置等，可以节省大量工作  
提供的基础功能封装如下：  


## 通用java配置
位置： scatter.common.rest.config  
由于WebSecurityConfigurerAdapter的配置实现只能有一个，所以建议在对security进行配置时使用扩展实现scatter.common.rest.config.CommonRestSecurityConfigure  

## 登录用户
位置： scatter.common.LoginUser 登录用户基类  
建议当前登录用户继承该基类或直接使用，因为基础组件提供了对该类型的支持，比如scatter.common.rest.security.LoginUserTool工具，controller方法中注入当前登录用户信息  

## 安全 spring security
位置： scatter.common.rest.security  
关于 spring security的一些封装  

## pojo基类
common-pojo 提供了mvc各个层的基础实体基类、注解等，遵循这样的规则可以方便扩展和统一  

+ 数据库表实体基类： scatter.common.pojo.po.BasePo  
+ controller传递到service层的参数基类： scatter.common.pojo.param.BaseParam(添加或个性)、scatter.common.pojo.param.BaseQuery(查询)  
+ http接口请求基类： scatter.common.pojo.form.BaseForm
+ service返回业务实体基类： scatter.common.pojo.dto.BaseDto  
+ controller响应数据实体基类： scatter.common.pojo.vo.BaseVo

## crud模板基类

common-rest 提供了常用crud的模板基类

### 顶级service模板类
+ scatter.common.rest.service.IBaseService： 顶级service模版接口，提供最基础的常用操作
+ scatter.common.rest.service.IBaseServiceImpl： IBaseService的实现

### 添加service模板类
+ scatter.common.rest.service.IBaseAddFormService： 添加表单模板接口
+ scatter.common.rest.service.IBaseAddFormServiceImpl： IBaseAddFormService实现
+ scatter.common.rest.service.IAddServiceListener：添加表单监听接口，目的是松散解藕，在不改变原组件代码情况下，以实现一个接口并放到spring容器中以做额外的处理

### 修改service模板类
修改的模板类，同添加模板类，不再详述  

修改支持的自定义注解：@SetNullWhenNull
::: tip 背景
在使用mybatis-plus时，一般是根据主键更新一个实体，如果是默认的配置，在实现某个字段为null的情况下会忽略该字段的更新;  
但有时我们确实需要将该字段设置为null，并且设置为null的条件并不是固定的，而是根据数据而定的（比如：在字段A为1是设置字段b为null）;  
在这种情况下，有了这个注解我们只需要在更新表单中为字段（b字段，自行判断如果字段A为1时设置b为null）添加一个注解@SetNullWhenNull，当该字段为null时（确保更新表单的字段名和对应的实体字段名一致）会更新该字段null值到表中；  
:::

### 查询service模板类
+ scatter.common.rest.service.IBaseQueryFormService： 查询service模版接口
+ scatter.common.rest.service.IBaseQueryFormServiceImpl： IBaseQueryFormService的实现

查询模板接口支持分页和不分页两种方法，但使用的查询参数是同一个，也就是说调用分页查询接口会返回分页数据，调用不分页查询接口会返回全部数据  

查询支持的自定义注解，以字典分页查询为例说明（参见下面@Like、@OrderBy注释），定义如下查询：
``` java
package scatter.dict.pojo.form;

import scatter.common.pojo.form.BasePageQueryForm;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import scatter.common.pojo.form.Like;
import scatter.common.pojo.form.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import scatter.common.pojo.po.BaseTreePo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 字典分页表单对象
 * </p>
 *
 * @author yw
 * @since 2020-11-25
 */
@Setter
@Getter
@ApiModel(value="字典分页表单对象")
public class DictPageQueryForm extends BasePageQueryForm {

    @Like // 这里只需要标标注like注解即可实现模板查询
    @ApiModelProperty(value = "字典编码,模糊查询，字典组时必填")
    private String code;

    @Like
    @ApiModelProperty(value = "字典名称,模糊查询")
    private String name;

    @Like
    @ApiModelProperty(value = "字典值,模糊查询")
    private String value;

    @ApiModelProperty(value = "是否为系统字典，一般系统字典代码中会做判断，不能修改或删除")
    private Boolean isSystem;

    @ApiModelProperty(value = "是否为公共字典，如果为公共字典不限制使用，否则按相应数据权限查询")
    private Boolean isPublic;

    @ApiModelProperty(value = "是否为字典组，不是字典组就是字典项目，没有其它的")
    private Boolean isGroup;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @Like
    @ApiModelProperty(value = "私有标识,模糊查询")
    private String privateFlag;

    @ApiModelProperty(value = "私有标识备忘")
    private String privateFlagMemo;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "分组标识备忘")
    private String groupFlagMemo;

    @ApiModelProperty(value = "描述")
    private String remark;

    @OrderBy(asc = true) // 默认按该字段升序排序
    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;

}

```
::: tip
自定义查询注解：@Gt（大于）、@Lt（小于）、@In（范围）、@OrderBy（排序）、@Like（模糊查询）、@Or(或)
:::

## controller基类
位置：scatter.common.rest.controller  
这里就不再详细说明了，道理同service模板基类相同  
::: tip
scatter.common.rest.controller.BaseInnerController 基类是供feign client使用的远程调用基类
:::

## 数据翻译
位置： scatter.common.rest.trans  
::: tip 背景
在开发中会经常用到在vo层返回到前端时根据id换名称的场景，比如查询用户列表，但用户表中有一个角色id需要转换为名称；  
我们通常的做法是遍历这个用户列表数据，循环查询出角色名称并设置角色名称，如果想效率高一点就是先将所有角色id取出，再批量查询，再设置名称；  
如果我们的代码中到处存在这种转换，总觉得不是太优雅，为了一个名字写了一堆重复的样板代码！
:::
基于以上背景，scatter提供了翻译服务，以减少大量的重复工作，下面以dictVo为例简单说明使用方式：  
字典是一个树表，其存在父级id，如果在查询字典的时候需要展示其父级名称，这和上面的背景是一个道理，dictVo类如下：  
``` java
package scatter.dict.pojo.vo;

import scatter.common.pojo.vo.BaseIdVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import scatter.common.rest.trans.TransBy;
import scatter.dict.pojo.po.Dict;


/**
 * <p>
 * 字典响应对象
 * </p>
 *
 * @author yw
 * @since 2020-11-25
 */
@Setter
@Getter
@ApiModel(value="字典响应对象")
public class DictVo extends BaseIdVo {

    @ApiModelProperty(value = "字典编码,模糊查询，字典组时必填")
    private String code;

    @ApiModelProperty(value = "字典名称,模糊查询")
    private String name;

    @ApiModelProperty(value = "字典值,模糊查询")
    private String value;

    @ApiModelProperty(value = "是否为系统字典，一般系统字典代码中会做判断，不能修改或删除")
    private Boolean isSystem;

    @ApiModelProperty(value = "是否为公共字典，如果为公共字典不限制使用，否则按相应数据权限查询")
    private Boolean isPublic;

    @ApiModelProperty(value = "是否为字典组，不是字典组就是字典项目，没有其它的")
    private Boolean isGroup;

    @ApiModelProperty(value = "是否禁用")
    private Boolean isDisabled;

    @ApiModelProperty(value = "禁用原因")
    private String disabledReason;

    @ApiModelProperty(value = "私有标识,模糊查询")
    private String privateFlag;

    @ApiModelProperty(value = "私有标识备忘")
    private String privateFlagMemo;

    @ApiModelProperty(value = "分组标识")
    private String groupFlag;

    @ApiModelProperty(value = "分组标识备忘")
    private String groupFlagMemo;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "排序,默认按该字段升序排序")
    private Integer seq;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "parentId",mapValueField = "name")
    @ApiModelProperty(value = "父级名称")
    private String parentName;
}

```

我们只需要在dictVo中 添加一个字段 private String parentName  
并添加上注解： @TransBy(type = Dict.TRANS_DICT_BY_ID,byFieldName = "parentId",mapValueField = "name")即可  

这一切归功于字典翻译对接口scatter.common.rest.trans.ITransService的实现，并注入到spring容器中  
相当于每一个需要翻译的表都需要实现 ITransService 接口，来达到要翻译的目的，当然大部分情况下可以这么做，在generator组件代码生成中也默认生成了翻译服务实现类  

::: tip
ITransService接口支持批量翻译，也就是说如果一个vo列表中有多个同一种类型的翻译，只会发起一次数据库查询
:::

### 翻译默认实现
位置： scatter.common.rest.trans.impl  
目前有两个实现：  
+ DatetimeTransServiceImpl： 对日期的转换实现
+ TableNameTransServiceImpl： 对依赖数据库表的实现，
  这一实现完全可以替代上面讲到的自定义翻译，也就是说我们可以不再自定义实现ITransService接口来翻译，而是使用该翻译替代，
  使用方式可以参见scatter.moment.pojo.vo.MomentVo类  

## 表单验证
位置：scatter.common.rest.validation  
后端的表单验证是必不可少的一个环节，如果表单验证仅仅是依赖前端，我们暂且不考虑数据安全的问题，那些需要接口方法的接口可以随意传值，这将会导致数据混乱！！，输入数据校验是对系统的自我保护。  

正常来说我们使用springboot的validate组件可以完全方便的实现表单验证，但在一些场景下显示不是那么方便：  
+ 在一个表单项依赖另一个表单项时比较难处理
+ 需要复杂的验证逻辑时，我们不得不把校验逻辑下探到controller层去处理或写一些字符串拼接的脚本验证（有利于可读，需要js语法）

鉴于此也封装了一些常用的字段验证器以供使用，目的是在应该完成校验的地方去校验：  
+ 属性依赖验证 scatter.common.rest.validation.props.IPropValidator，scatter.common.rest.validation.cross.depend.DependField  
+ 复杂逻辑验证 scatter.common.rest.validation.form.Form  
+ 手机号验证 scatter.common.rest.validation.Mobile  
+ 自定义脚本验证 scatter.common.rest.validation.Script  


