module.exports = {
    title: 'Scatter 技术文档',
    // 网站的描述，它将会以 <meta> 标签渲染到当前页面的 HTML 中
    description: 'Scatter 技术文档',
    port: 9090,
    plugins: [
        require('../plugins/BackendComponentsAdditionalPages.js')
    ],
    themeConfig: {
        displayAllHeaders: true, // 默认值：false
        logo: '/scatter-logo.png',
        nav: [
            { text: '介绍', link: '/guide/profile/' },
            {
                text: '其它',
                items: [
                    { text: 'copy-relative-path', link: 'https://gitee.com/feihua666/copy-relative-path' }
                ]
            },
            { text: 'Gitee', link: 'https://gitee.com/feihua666/scatter' },
        ],
        sidebar: [
            '/guide/profile/',
            '/guide/quick/quick-start',
            {
                title: '项目',   // 必要的
                path: '/guide/project/',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
                collapsable: false, // 可选的, 默认值是 true,
                children: [
                    '/guide/project/backend/components/common/',
                    '/guide/project/backend/components/component-use/',
                    '/guide/project/backend/components/component-create/',
                    '/guide/project/frontend/components/common/',
                ]
            },
            {
                title: '组件',   // 必要的
                collapsable: false, // 可选的, 默认值是 true,
                children: [
                ]
            },
        ]
    }
}