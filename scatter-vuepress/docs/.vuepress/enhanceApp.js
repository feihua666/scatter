
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '../../../components/common/web/st.css'
import ToolsPlugin from '../../../components/common/web/tools/ToolsPlugin.js'
import ElementPlugin from '../../../components/common/web/components/pc/element/ElementPlugin.js'
import CommonPlugin from '../../../components/common/web/components/pc/common/CommonPlugin.js'

export default ({ Vue, options, router }) => {
    Vue.use(Element)
// 自定义工具插件
    Vue.use(ToolsPlugin)
// 自定义基于element组件
    Vue.use(ElementPlugin)
// 自定义基于通用组件
    Vue.use(CommonPlugin)
    console.log(11111)
};
