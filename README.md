# scatter

#### 介绍
基于SpringBoot2.x、vuejs，前后端分离。后端springboot业务组件拆分，旨在快速开发新模块。主要技术点：swagger接口文档、表单验证、MyBatisPlus、springSecurity、lombok、mapstruct；前端vuejs、element-ui。

#### 软件架构
本项目旨在开发基础的、常用的业务功能，使其组件化的curd操作，以快速组装可用的新模块或项目。
整体分为两部分：

1.  组件
组件是组装模块的最小单位
2.  模块
模块是服务运行的最小单位，可以单独发布或组装更大的模块发布

#### 安装教程

1.  后端springboot
暂无
2.  前端vue
暂无

#### 使用说明

暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
